<?php global $realty, $helper, $wpdb; 
if(!empty($_GET)){
	foreach($_GET as $key=>$item)$suburb=str_replace("_"," ",$key);
	if(strpos($suburb,"'")!==false){
		$end_link=strpos($suburb,"'")+1;
		$suburb=substr($suburb,$end_link);
	}
}
else{
	global $post;if(!empty($post->post_title))$suburb=str_replace("-"," ",trim($post->post_title));
}
?>
<!-- CURRENT SALE-->
<?php $current_sale = $realty->properties(array('list' =>'sale','limit'=>'1','suburb'=>$suburb, 'order'=>'created_at' , 'order_direction'=>'desc'));if($current_sale['results_count'] >0):?>

<div id="current_sale" class="sim_content">
    <h2 class="section_title">Latest For Sale</h2>
    <div class="block_content">
        <?php
            $count=0; 
            foreach ($current_sale as $property):
            	if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned			
            	$currsale_count++;
		?>
                <div class="block<?php echo " block".$currsale_count;?>">
                    <div class="image"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>"><img src="<?php echo $property['thumbnail']; ?>" alt="<?php echo $property['headline']; ?>" /></a></div>
                    <div class="similar_property_info">
                        <ul>
                            <li class="sim_suburb suburb"><span class="field">Suburb</span><span class="colon">:</span><?php echo $property['suburb']; ?></li>
                            <li class="sim_type type"><span class="field">Type</span><span class="colon">:</span><?php echo $property['type']; ?></li>
                            <li class="sim_price price"><span class="field">Price</span><span class="colon">:</span><?php echo $property['price']; ?></li>
                            <li class="sim_view"><p class="sim_view-link"><a href="<?php echo $property['url']; ?>" title="View Property">View Property <span>&raquo;</span></a></p></li>
                        </ul>
                    </div>
                    <div class="clearer"></div>
                </div>
		<?php endforeach; ?>
    </div>
	<p class="view-listings"><a href="<?php echo $realty->siteUrl.'search-results/?list=sale&suburb[]='.strtoupper($suburb); ?>">View all listings</a></p>
</div><!-- end #current_sale -->
<?php endif; ?>



<!-- CURRENT LEASE-->
<?php $current_lease = $realty->properties(array('list' =>'lease','limit'=>'1', 'suburb'=>$suburb, 'order'=>'created_at' , 'order_direction'=>'desc'));if($current_lease['results_count'] >0):?>
<div id="current_lease" class="sim_content">
    <h2 class="section_title">Latest For Lease</h2>
    <div class="block_content">
        <?php
            $count=0; 
            foreach ($current_lease as $property):
            if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned			
            $currlease_count++;
		?>
            <div class="block<?php echo " block".$currlease_count;?>">
                <div class="image"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>"><img src="<?php echo $property['thumbnail']; ?>" alt="<?php echo $property['headline']; ?>" /></a></div>
                <div class="similar_property_info">
                    <ul>
                        <li class="sim_suburb suburb"><span class="field">Suburb</span><span class="colon">:</span><?php echo $property['suburb']; ?></li>
                        <li class="sim_type type"><span class="field">Type</span><span class="colon">:</span><?php echo $property['type']; ?></li>
                        <li class="sim_price price"><span class="field">Price</span><span class="colon">:</span><?php echo $property['price']; ?></li>
                        <li class="sim_view"><p class="sim_view-link"><a href="<?php echo $property['url']; ?>" title="View Property">View Property <span>&raquo;</span></a></p></li>
                    </ul>
                </div>
            	<div class="clearer"></div>
            </div>
		<?php	endforeach; ?>
    </div>
    <p class="view-listings"><a href="<?php echo $realty->siteUrl.'search-results/?list=lease&suburb[]='.strtoupper($suburb); ?>">View all listings</a></p>
</div><!-- end #current_lease -->
<?php endif;?>


<?php
$extra_condition="where suburb like '%".esc_sql(trim($suburb))."%'"; 
$location = $wpdb->get_results("SELECT suburb_id, postcode FROM LocationInfo_suburbs $extra_condition", ARRAY_A);
$postcode=$location[0]['postcode'];
$suburb_id=$location[0]['suburb_id'];
if(empty($postcode))return;

$location = $wpdb->get_results("SELECT distinct suburb FROM LocationInfo_suburbs where postcode in($postcode) and suburb_id not in($suburb_id) and suburb not in('".esc_sql(trim($suburb))."')", ARRAY_A);
if(!empty($location)){ ?>
<div id="other_properties">
	<h2 class="section_title">Other Suburbs in This Postcode</h2>
    <div class="block_content">
        <p>
		<?php foreach($location as $item): $page_name=preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "-",strtolower($item['suburb']))); ?>
        	<a href="<?php echo $realty->siteUrl."location-info/$page_name/"; ?>"><?php echo $item['suburb']; ?></a>
        <?php endforeach; ?>
        </p>
    </div>
</div>
<?php } ?>