<?php
$property_types=array('Accommodation/Tourism','Automotive','Beauty/Health','Education/Training','Food/Hospitality','Franchise','Home/Garden','Import/Export/Whole','Industrial/Manufacturing','Leisure/Entertainment','Professional','Retail','Rural','Services','Transport/Distribution');
if(!empty($property['property_type']))$categories=$helper->get_column("select distinct name from property_types where category in ('".$property['property_type']."') order by name");
?>
<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div class="content-left-column">
		<input type="hidden" name="type" value="BusinessSale">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div id="address_div" class="block_content">
			<h4>Property Address</h4>
			<table>
				<tr>
					<td class="label"><strong>Country<span class="red"> *</span></strong></td>
					<td>
						<select name="country" id="country" class="full">
						<?php foreach($countries as $country){ ?>
						<option value="<?php echo $country; ?>" <?php if($property['country']==$country)echo 'selected="selected"'; ?>><?php echo $country; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Suburb<span class="red"> *</span></strong></td>
					<td><input onkeyup="reloads_addres(this.value);" type="text" name="suburb" id="suburb" class="full" value="<?php echo $property['suburb']; ?>">
					<div class="suggestionsBoxSuburb" id="suggestionsBoxSuburb" style="display: none;">
						<div class="autoSuggestionsListSuburb" id="autoSuggestionsListSuburb">
							&nbsp;
						</div>
					</div>  
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Region<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="town_village" name="town_village"  value="<?php echo $property['town_village']; ?>">
						<span id="town_village_span"><?php echo $property['town_village']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>State<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="state" name="state"  value="<?php echo $property['state']; ?>">
						<span id="state_span"><?php echo $property['state']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Postcode<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="postcode" name="postcode"  value="<?php echo $property['postcode']; ?>">
						<span id="postcode_span"><?php echo $property['postcode']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label">Unit Number</td>
					<td><input type="text" name="unit_number" class="full"  value="<?php echo $property['unit_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Street Number<span class="red"> *</span></strong></td>
					<td><input type="text" id="street_number" name="street_number" class="full"  value="<?php echo $property['street_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Street Name/Type<span class="red"> *</span></strong></td>
					<td><input type="text" id="street" name="street" class="full"  value="<?php echo $property['street']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Display Address:</td>
					<td>
						<input type="radio" name="display_address" <?php if ($property['display_address']=='1' || !isset($property['display_address'])) echo 'checked="checked"'; ?> value="1">All Address Details
						<span style="margin-left:10px"><input type="radio" name="display_address" <?php if ($property['display_address']=='0') echo 'checked="checked"'; ?> value="0">Suburb Only</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		<div id="datas_div" class="block_content">
			<h4>Property Data</h4>
			<table>
				<tr>
					<td class="label"><strong>Category:<span class="red"> *</span></strong></td>
					<td colspan="2">
						<select name="property_type" class="full" onChange="change_category(this.value);">
						<option value="">Please Select</option>
						<?php foreach($property_types as $property_type){ ?>
							<option value="<?php echo $property_type; ?>" <?php if($property_type==$property['property_type']) echo 'selected="selected"'; ?>><?php echo $property_type; ?></option>
						<?php } ?>
						</select>					
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Sub Category:<span class="red"> *</span></strong></td>
					<td colspan="2">
						<select name="category" id="category" class="full">
						<option value="">Please Select</option>
						<?php if(!empty($categories)){ foreach($categories as $category){ ?>
							<option value="<?php echo $category; ?>" <?php if($category==$property['category']) echo 'selected="selected"'; ?>><?php echo $category; ?></option>
						<?php } } ?>
						</select>					
					</td>
				</tr>
				<?php /*<tr>
					<td class="label">Unique Property ID#:</td>
					<td colspan="2"><input type="text" class="full"  name="unique_id" value="<?php echo $property['unique_id']; ?>"></td>
				</tr>*/ ?>
				<tr>
					<td class="label"><strong>Property Headline:<span class="red"> *</span></strong></td>
					<td colspan="2"><input type="text" class="full" name="headline" value="<?php echo $property['headline']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Property Description:<span class="red"> *</span></strong></td>
					<td colspan="2"><textarea name="description" cols="39" rows="15"><?php echo $property['description']; ?></textarea></td>
				</tr>
				<tr>
					<td class="label">Year built:</td>
					<td colspan="2"><input type="text" class="half" name="year_built"  value="<?php echo $property['year_built']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Tax Rates:</td>
					<td><input type="text" class="half" name="tax_rate"  value="<?php echo $property['tax_rate']; ?>"></td>
					<td>
						<select name="tax_rate_period">
							<option value="">Please Select</option>
							<?php foreach($periods as $period){ ?>
							<option value="<?php echo $period; ?>" <?php if($property['tax_rate_period']==$period)echo 'selected';?>><?php echo $period; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Water Rates:</td>
					<td><input type="text" class="half" name="water_rate"  value="<?php echo $property['water_rate']; ?>"></td>
					<td>
						<select name="water_rate_period">
							<option value="">Please Select</option>
							<?php foreach($periods as $period){ ?>
							<option value="<?php echo $period; ?>" <?php if($property['water_rate_period']==$period)echo 'selected';?>><?php echo $period; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Strata Fees:</td>
					<td><input type="text" class="half" name="condo_strata_fee"  value="<?php echo $property['condo_strata_fee']; ?>"></td>
					<td>
						<select name="condo_strata_fee_period">
							<option value="">Please Select</option>
							<?php foreach($periods as $period){ ?>
							<option value="<?php echo $period; ?>" <?php if($property['condo_strata_fee_period']==$period)echo 'selected';?>><?php echo $period; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Estimated Rental Return:</td>
					<td><input type="text" class="half" name="estimate_rental_return"  value="<?php if(!empty($property['estimate_rental_return']))echo $property['estimate_rental_return']; ?>"></td>
					<td>
						<select name="estimate_rental_return_period">
							<option value="">Please Select</option>
							<?php foreach($periods as $period){ ?>
							<option value="<?php echo $period; ?>" <?php if($property['estimate_rental_return_period']==$period)echo 'selected';?>><?php echo $period; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		<div id="rooms_div" class="block_content">
			<h4>Rooms</h4>
			<table>
				<tr>
					<td class="label">Bedrooms</td>
					<td>
						<select name="bedrooms" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['bedrooms'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Half Bedroom:</td>
					<td><input type="checkbox" name="half_bedroom" value="1" <?php if(strpos($property['bedrooms'],'.5')!==false)echo 'cehcked';?>></td>
				</tr>
				<tr>
					<td class="label">Bathrooms</td>
					<td>
						<select name="bathrooms" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['bathrooms'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Half Bathroom:</td>
					<td><input type="checkbox" name="half_bathrooms" value="1" <?php if(strpos($property['bathrooms'],'.5')!==false)echo 'cehcked';?>></td>
				</tr>
				<tr>
					<td class="label">Carport Spaces:</td>
					<td>
						<select name="carport" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['carport'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Garage Spaces:</td>
					<td>
						<select name="garage" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['garage'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Off Street Spaces:</td>
					<td>
						<select name="off_street_park" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['off_street_park'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		<div id="eer_div"class="block_content">
			<h4>Energy Efficiency Rating </h4>
			<table>
				<tr>
					<td class="label">Select Rating:</td>
					<td>
						<select name="energy_efficiency_rating" class="full">
							<option value="">Please Select</option>
							<?php foreach($eer as $i){ ?>
							<option value="<?php echo $i; ?>" <?php if($property['energy_efficiency_rating']==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		
	</div>

	<div class="content-right-column">
		<div id="areas_div" class="block_content">
			<h4>Land/Block Floor Areas (Whole Numbers Only)</h4>
			<table>
				<tr>
					<td class="label">Land Area: </td>
					<td><input type="text" class="half"  name="land_area" value="<?php echo $property['land_area']; ?>"></td>
					<td>
						<select name="land_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['land_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Floor Area:</td>
					<td><input type="text"  class="half" name="floor_area" value="<?php echo $property['floor_area']; ?>"></td>
					<td>
						<select name="floor_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['floor_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Number of Floors/Levels:</td>
					<td colspan="2">
						<select name="number_of_floors">						
							<?php for($i=1;$i<6;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if($property['number_of_floors']==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		<div id="others_div" class="block_content">
			<h4>External Links</h4>
			<table>
				<?php /*<tr>
					<td class="label">Property Url:</td>
					<td><input type="text" class="full"  name="property_url" value="<?php echo $property['property_url']; ?>"></td>
				</tr>*/ ?>
				<tr>
					<td class="label">Virtual Tour 1 Url:</td>
					<td><input type="text" class="full"  name="virtual_tour" value="<?php echo $property['virtual_tour']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Virtual Tour 2 Url:</td>
					<td><input type="text" class="full"  name="ext_link_1" value="<?php echo $property['ext_link_1']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Video Url:</td>
					<td><input type="text" class="full"  name="ext_link_2" value="<?php echo $property['ext_link_2']; ?>"></td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		<div id="auction" class="block_content">
			<h4>Auction Date & Time (Only if for auction)</h4>
			<table>
				<tr>
					<td class="label">Forthcoming Auction:</td>
					<td colspan="3"><input type="checkbox" value="1" name="forthcoming_auction" <?php if($property['forthcoming_auction'])echo 'checked'; ?>></td>
				</tr>
				<tr>
					<td class="label">Auction Place:</td>
					<td colspan="3"><input class="full" type="text" name="auction_place" value="<?php echo $property['auction_place']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Auction Date/Time:</td>
					<td><input class="half" type="text"  id="auction_date" name="auction_date" value="<?php if(!empty($property['auction_date']) && $property['auction_date']!='0000-00-00')echo date("d-m-Y",strtotime($property['auction_date'])); ?>"></td>
					<td>&nbsp;@&nbsp;</td>
					<td><input type="text"  class="half" id="auction_time" name="auction_time" value="<?php if(!empty($property['auction_time']) && $property['auction_time']!='00:00:00')echo date("H:i",strtotime($property['auction_time'])); ?>"></td>
				</tr>
			</table>
		</div>
		
		<div id="price_div" class="block_content">	
			<h4>Pricing</h4>
			<table>
				<tr>
					<td class="label"><strong>Price/Search Price:<span class="red"> *</span></strong></td>
					<td><input type="text" class="half"  name="price" value="<?php if(!empty($property['price']))echo $property['price']; ?>"></td>
					<td>&nbsp;to&nbsp;</td>
					<td><input type="text" class="half"  name="to_price" value="<?php  if(!empty($property['to_price']))echo $property['to_price']; ?>"></td>
				</tr>
			</table>
			
			<p><strong>Displaying Price</strong></p>
			<table>
				<tr>
					<td class="label">Display Above Price:</td>
					<td colspan="2"><input type="radio" name="display_price"  <?php if ($property['display_price']=='1' || !isset($property['display_price'])) echo 'checked="checked"'; ?> value="1"></td>	
				</tr>
				<tr>
					<td class="label">Display text in place of Price:</td>
					<td><input type="radio" name="display_price" <?php if ($property['display_price']=='2') echo 'checked="checked"'; ?> value="2"></td>
					<td><input type="text"  class="full display-price"  name="display_price_text" value="<?php echo $property['display_price_text']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Not display Price:</td>
					<td><input type="radio" name="display_price" <?php if ($property['display_price']=='0') echo 'checked="checked"'; ?> value="0"></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>
		
		<div id="users_div" class="block_content">
			<h4>Property Contacts </h4>
			<table>
				<tr>
					<td class="label"><strong>Primary Contact:*<span class="red"> *</span></strong></td>
					<td>
						<select name="user_id" class="full">
							<option value="">Please Select</option>
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['user_id']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Secondary Contact:</td>
					<td>
						<select name="secondary_user" class="full">
							<option value="">Please Select</option>	
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['secondary_user']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Third Contact:</td>
					<td>
						<select name="third_user" class="full">
							<option value="">Please Select</option>
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['third_user']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="clearer"></div>		
		
		<div id="map_div"class="block_content">
			<h4>Mapping Coordinates</h4>
			<p><strong><a class="submit_geocode" href="javascript:submit_geocode();">Map Above Address</a></strong></p>
			<p>
				<strong>Longitude:<span id="longitude_span" class="value"><?php echo ($property['longitude']=='')?'&nbsp':$property['longitude']; ?></span></strong>
				<strong>Latitude:<span id="latitude_span" class="value"><?php echo ($property['latitude']=='')?'&nbsp':$property['latitude']; ?></span></strong>
				<input type="hidden" name="longitude" id="longitude" value="<?php echo $property['longitude']; ?>">
				<input type="hidden" name="latitude" id="latitude" value="<?php echo $property['latitude']; ?>">
			</p>
			<div class="map-wrapper">
			<div id="map_canvas" style="width:380px !important;height:350px !important;"></div>
			<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
			<?php if(!empty($property['latitude'])){ ?>
			<script type="text/javascript">
			jQuery(document).ready(function () {
				load();
				});	
			</script>
			<?php } ?>
		</div>
		</div>
		<div class="clearer"></div>
		
	</div>	

	<div class="clear"></div>	
     <div class="block_content">
		<input type="submit" name="submit" value="Save & Next" class="btn" id="submit_btn" >
	</div>
</form>