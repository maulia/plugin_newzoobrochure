<div id="stamp_duty_calculator" class="clc">
<?php if ($before_title != '') { echo $before_title . "<span>Stamp Duty</span> Calculator" . $after_title; } else { echo "<h4><span>Stamp Duty</span> Calculator</h4>"; } ?>

<table class="calc_table" cellpadding="0" cellspacing="0">
    <tr>
    	<td class="calc-td-form">
        	<table cellpadding="0" cellspacing="0">
            	<tr>
                    <td class="calc_field">State</td>
                    <td class="calc_value">
                           <select name="state_id" id="state_id">
							<option value="">Choose a state</option>
							<option value="7" >Australian Capital Territory ( ACT )</option>
							<option value="1" >New South Wales ( NSW )</option>
							<option value="8" >Northern Territory ( NT )</option>
							<option value="3" >Queensland ( QLD )</option>
							<option value="4" >South Australia ( SA )</option>
							<option value="6" >Tasmania ( TAS )</option>							
							<option value="2" >Victoria ( VIC )</option>
							<option value="5" >Western Australia ( WA )</option>
						</select>
                    </td>
                </tr>
                <tr>
                    <td class="calc_field">Price</td>
                    <td class="calc_value"><input type="text" size="20" name="price" id="price" class="textbox" /></td>
                </tr>
               <tr>
					<td>Property is:</td>
					<td>
						<input type="radio" name="property_is" id="living_property" checked="checked" /> to live in<br />
						<input type="radio" name="property_is" id="investment_property" /> an investment
					</td>
				</tr>
				<tr>
					<td>First home buyer?</td>
					<td>
						<input type="radio" name="first_home_buyer" id="first_home_buyer" checked="checked"/> Yes<br />
						<input type="radio" name="first_home_buyer" id="not_first_home_buyer"   /> No
					</td>
				</tr>
				<tr>
					<td>Are you purchasing:</td>
					<td>
						<input type="radio" name="property_type" id="establish" checked="checked" /> An established home<br />
						<input type="radio" name="property_type" id="new_home" /> A newly constructed home<br/>
						<input type="radio" name="property_type" id="land" /> Vacant land
					</td>
				</tr>
				<tr>
                    <td class="calc_btn" colspan="2"><p class="submit_btn button"><a href="javascript:void(0)" onClick="javascript:loadresult_stamp();" class="btn" >Calculate</a></p></td>
                </tr>
            </table>
        </td>
        <td class="calc-td-result">
			<table id="stamp_result_tr" style="display:none">
				<tr>			
					<td class="calc_field">Stamp Duty:</td>
					<td class="calc_value"><input id="stamp_result" type="text" class="textbox" /></td>
				</tr>
				<tr>			
					<td class="calc_field">Mortgage Registration Fee:</td>
					<td class="calc_value"><input id="registration_fee" type="text" class="textbox" /></td>
				</tr>
				<tr>			
					<td class="calc_field">Transfer Fee:</td>
					<td class="calc_value"><input id="transfer_fee" type="text" class="textbox" /></td>
				</tr>
				<tr>			
					<td class="calc_field">Total:</td>
					<td class="calc_value"><input id="total" type="text" class="textbox" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<span id="notes"></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>

<div id="mort_calc" class="clc">
<?php if ($before_title != '') { echo $before_title . "<span>Mortgage</span> Calculator" . $after_title; } else { echo "<h4><span>Mortgage</span> Calculator</h4>"; } ?>

<div class="block_content">
    <table class="calc_table">
        <tr>
            <td class="calc-td-form">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="calc_field">Sales Price:</td>
                        <td class="calc_value"><input id="sales_price" type="text" size="6" value="<?php echo $_POST['sales_price']; ?>" name="sales_price" /></td>
                    </tr>
                    <tr style="display:none;">
                        <td class="calc_field">Down Payment(%):</td>
                        <td class="calc_value"><input id="down_payment" type="text" size="2" maxlength="2" value="<?php echo $_POST['down_payment']; ?>" name="down_payment" /></td>
                    </tr>
                    <tr>
                        <td class="calc_field">Interest Rate(%):</td>
                        <td class="calc_value"><input id="interest_rate" type="text" size="5" maxlength="5" value="<?php echo $_POST['interest_rate']; ?>" name="interest_rate" /></td>
                    </tr>
                    <tr>
                        <td class="calc_field">Term (years):</td>
                        <td class="calc_value"><input id="term" type="text" size="2" maxlength="2" value="<?php echo $_POST['term']; ?>" name="term" /></td>
                    </tr>
                    <tr>
                        <td class="calc_btn" colspan="2"><p class="submit_btn button"><a href="javascript:void(0)" onClick="javascript:loadresult();" name="calculate_mortgage" class="btn" >Calculate</a></p></td>
                    </tr>
                </table>
            </td>
            <td class="calc-td-result"><div id="calc_results"></div></td>
        </tr>
    </table>
</div>
</div><!-- end #mort_calc -->
<p>These calculators are a guide only and should not replace professional financial advice.</p>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>display/pages/js/stamp_duty.js"></script>
<script type="text/javascript"> 
function loadresult(){
	var sales_price = document.getElementById('sales_price').value;
	var interest_rate = document.getElementById('interest_rate').value;
	var term = document.getElementById('term').value;
	jQuery('#calc_results').show();
	var url = "<?php echo $this->pluginUrl; ?>display/pages/js/calculate.php?sales_price=" + escape(sales_price)+"&interest_rate=" + escape(interest_rate)+"&term=" + escape(term)+"&down_payment=0";
	jQuery('#calc_results').load(url);
}
</script>