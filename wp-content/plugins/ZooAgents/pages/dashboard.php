<?php
global $helper, $current_user, $realty, $wpdb;
if(!current_user_can('administrator')){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }

$office_id_all=$realty->settings['general_settings']['office_id'];
if(!empty($_GET['office_id'])){
	$settings=get_option('realty_general_settings');	
	
	if($_GET['deactivate']=='true')$settings['office_id']=array_diff($settings['office_id'], array($_GET['office_id']));
	else if($_GET['activate']=='true')array_push($settings['office_id'], $_GET['office_id']);
	
	$office_id_all=$settings['office_id'];
	update_option('realty_general_settings',$settings);
}


$offices=$wpdb->get_results("select id, name, created_at from office where contributor=0 order by name", ARRAY_A);
?>
<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/table_sorter.css" type="text/css" media="all" />
<script type="text/javascript">jQuery(function() { jQuery("#properties_table").tablesorter({ widgets: ['zebra'] }); });</script>	

<?php include('menu_admin.php'); ?>

<table class="tablesorter" cellpadding="0" cellspacing="0" id="properties_table">
	<thead>
		<tr class="th">
			<th class="th_date">Date Added</th>
			<th class="th_id">ID</th>
			<th class="th_type">Name</th>			
			<th class="th_listings">Listings</th>			
			<th class="th_team">Team</th>			
			<th class="th_status">Status</th>			
			<th class="th_edit">System</th>
			<th class="th_del">Delete</th>
		</tr>
	</thead>
	<?php 
	foreach($offices as $office): 
		$listings=$wpdb->get_var("select count(id) from properties where office_id=".$office['id']);
		$team=$wpdb->get_var("select count(id) from users where office_id=".$office['id']);
	?>
	<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
		<td class="td_date"><?php echo date("d-m-Y",strtotime($office['created_at'])); ?></td>
		<td class="td_id"><?php echo $office['id']; ?></a></td>
		<td class="td_type"><?php echo $office['name']; ?></td>		
		<td class="th_listings"><?php echo $listings; ?></td>		
		<td class="th_team"><?php echo $team; ?></td>		
		<td class="th_status">
		<?php if(in_array($office['id'], $office_id_all)){ ?>		
		<a href="<?php echo $realty->siteUrl.'dashboard/?deactivate=true&office_id='.$office['id']; ?>" onClick="return confirm('You are about to deactivate the this office.\n\'OK\' to delete, \'Cancel\' to stop.' );">Active</a>
		<?php } else { ?>
		<a href="<?php echo $realty->siteUrl.'dashboard/?activate=true&office_id='.$office['id']; ?>" onClick="return confirm('You are about to activate the this office.\n\'OK\' to delete, \'Cancel\' to stop.' );">Not Active</a>
		<?php } ?>
		</td>		
		<td class="td_edit"><a href="<?php echo $realty->siteUrl.'dashboard/listings/?office_id='.$office['id']; ?>" title="View Detail" target="_blank">System</a></td>		
		<td class="td_del"><a class="delete" onClick="return confirm('Are you sure to delete this office along with the listings and agents belong to this office.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl.'dashboard/?task=delete&id='.$office['id'];?>">Delete</a></td>
	</tr>
	<?php endforeach; ?>
</table>