<?php
/*
Template Name: Property page
*/
global $options;
foreach ($options as $value) {if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }}
get_header();
global $realty;?>

<div id="content" class="narrowcolumn">
	<div class="img-media">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Page Media') ) {} ?>
    </div>
	<div class="property-main">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Page Main') ) {} ?>
    </div>
	<div class="property-side">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Page Side') ) {} ?>
    </div>
</div>

<?php get_footer(); ?>