<script type="text/javascript">
// <![CDATA[
function open_query_form(element){
	if(element.value != 'sale' && element.value != 'lease')
		return;
	if(element.checked==1)
		document.getElementById(element.value + '_search').style.display='block';
	else
		document.getElementById(element.value + '_search').style.display='none';
}
// ]]>
</script>
<?php if (get_option('spam_filter')=='1')require_once(dirname(dirname(dirname(__FILE__))).'/zoorealty/display/elements/cryptographp.php'); ?>
<?php if (!empty($content))echo "$content"; ?>
<?php if (!empty($return)) echo "<div id='return'>$return</div>"; ?>

<?php if(!$this->is_logged_in() || $this->is_logged_in()): ?>
<form method="post" action="" name="form_email_subscription" class="contact_form form_email_subscription">
	<input type="hidden" name="subscriber_id" value="<?php echo $subscriber['subscriber_id']; ?>" />
	<input type="hidden" name="task" value="register_or_edit" />
	<input type="hidden" name="post_title" value="<?php global $post; echo $post->post_title; ?>" />
	<div id="user_management_page">
		<div id="manage_profile">
			<ol class="cf-ol">
				<?php if(get_option('management_appraisal_title')=='1') : ?>
				<li><label><span>Title</span></label>
				<select name="title">
					<option value="">Select</option>		
					<option value="Miss"<?php if($subscriber['title']=='Miss') echo ' selected="selected"'; ?>>Miss</option>
					<option value="Mr"<?php if($subscriber['title']=='Mr') echo ' selected="selected"'; ?>>Mr</option>
					<option value="Mrs"<?php if($subscriber['title']=='Mrs') echo ' selected="selected"'; ?>>Mrs</option>
					<option value="Ms"<?php if($subscriber['title']=='Ms') echo ' selected="selected"'; ?>>Ms</option>
					<option value="Dr"<?php if($subscriber['title']=='Dr') echo ' selected="selected"'; ?>>Dr</option>
				</select></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_first_name')=='1') : ?>
				<li><label><span>First Name *</span></label><input type="text" name="first_name" class="searchbox" size="45" value="<?php echo $subscriber['first_name']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_last_name')=='1') : ?>
				<li><label><span>Last Name *</span></label><input type="text" name="last_name" class="searchbox" size="45" value="<?php echo $subscriber['last_name']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_email')=='1') : ?>
				<li><label><span>E-mail *</span></label><input type="text" name="email" class="searchbox" size="45" value="<?php echo $subscriber['email']; ?>" onClick="if(this.value=='enter email address') this.value='';" /></li>
				<?php endif; ?>

				<?php if(get_option('management_appraisal_home_phone')=='1') : ?>	
				<li><label><span>Home Number</span></label><input type="text" name="home_phone" class="searchbox" size="45" value="<?php echo $subscriber['home_phone']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_mobile_phone')=='1') : ?>
				<li><label><span>Mobile Number</span></label><input type="text" name="mobile_phone" class="searchbox" size="45" value="<?php echo $subscriber['mobile_phone']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_work_phone')=='1') : ?>
				<li><label><span>Work Number</span></label><input type="text" name="work_phone" class="searchbox" size="45" value="<?php echo $subscriber['work_phone']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_fax')=='1') : ?>
				<li><label><span>Fax Number</span></label><input type="text" name="fax" class="searchbox" size="45" value="<?php echo $subscriber['fax']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_country')=='1') : ?>			
					<li><label><span>Country</span></label>
					<select id="country" name="country">
						<option value="Australia">Australia</option>
						<?php foreach ($list_countries as $item): ?>	
							<option value="<?php echo $item; ?>" <?php if($item == $subscriber['country']) echo 'selected="selected"'; ?>><?php echo $item; ?></option>
						<?php endforeach; ?>
						</select></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_referrer')=='1') : ?>	
					<li class="subscribe_referrer"><label><span>How did you find us?</span></label>
					<select name="referrer">
						<option value="">Please select</option>
					<?php foreach($all_list as $item_value): ?>
						<option value="<?php echo $item_value['name']; ?>"<?php if ($subscriber['referrer'] == $item_value['name']) echo 'selected="selected"'; ?>><?php echo $item_value['name']; ?></option>
					<?php endforeach; ?>
					</select></li>
				<?php endif; ?>
				
				
				<li class="site_alerts">
				<?php if(get_option('management_appraisal_property_alert')=='1') : ?>	
				<label><span>Property Alerts</span></label>
					<?php $property_alert=array();$property_alert=array_diff($list_alerts, $news_alerts);?>
					<?php foreach($property_alert as $key=>$item_value):?>
						<span><input type="checkbox" class="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?> onclick="open_query_form(this)">
						<?php echo $item_value; ?></span><br/>
						<?php echo $this->get_form_query_string($key);
				endforeach; ?>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_news_alert')=='1') : ?>	
				<label><span>News Alerts</span></label>
					<?php $news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);?>
					<?php foreach($news_alert as $key=>$item_value):?>
						<input type="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?>><?php echo $item_value; ?><br/>
					<?php endforeach; ?>
				<?php endif; ?>
				</li>
					
				<?php if(get_option('management_appraisal_property_address')=='1') : ?>
				<li><label><span>Property Address</span></label><input type="text" name="property_address" class="searchbox" size="45" value="<?php echo $subscriber['property_address']; ?>" /></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_comments')=='1') : ?>
				<li class="subscribe_comments"><label><span>Comments</span></label><textarea name="comments" cols="30"><?php echo $subscriber['comments']; ?></textarea></li>
				<?php endif; ?>
				
				<?php if (get_option('spam_filter')=='1'):?>
				<li><label><span>Spam Code</span><img id="captcha-image" src="<?php echo $realty->pluginUrl.'display/elements/CaptchaSecurityImages.php?width=100&height=40&characters=5'; ?>" alt="Anti-Spam Image" style="vertical-align:top;" /><span class="reload-captcha"><a href="javascript:captchas_image_reload('captcha-image')" class='btn' title="Reload Image">Reload Image</a></span>
			<br/><br/><label>Enter the above code</label><input type="text" name="securitycode" id="securitycode" size="20" />
			<script type="text/javascript">
			  function captchas_image_reload (imgId) 
			  {
				var image_url = document.getElementById(imgId).src;
				image_url+= "&";
				document.getElementById(imgId).src = image_url;
			  }
			</script></li>
				<?php endif; ?>
				
				<?php if(get_option('management_appraisal_issubscribe')=='1') : ?>
				<li><label><span>Subscribe to be kept informed?</span></label><input type="checkbox" class="checkbox" name="keep_informed" value="1" <?php if($subscriber['keep_informed']=='1') echo "checked";?>>Yes</li>
				<?php endif; ?>
			</ol>
			</div><!-- end #manage_profile-->
	</div><!-- end #user_management_page-->
	<input type="hidden" name="user_type" value="management_appraisal">
	<div class="clearer"></div>
	<div class="buttons signup_button">
	   <p class="button email_btn"><input class="btn" type="submit" value="Submit"></p>
	</div>
</form>
<?php endif; ?>