<?php
/*
  Template Name: Template Homepage
 */

global $options, $realty, $helper, $wp_query, $wpdb;
foreach ($options as $value) {
    if (get_option($value['id']) === FALSE) {
        $$value['id'] = $value['std'];
    } else {
        $$value['id'] = get_option($value['id']);
    }
}
get_header();
$images = get_option('homepage_images');

$users = $realty->users();
?>
<div class="content">
    <?php //get_sidebar(); ?>
    <div class="homepage-header">
        <div class="homepage-header-wrap">
            <div class="homepage-image">
                <div class="block home-image">
                    <div class="wrap">
                        <?php ?>
                        <div class="slideshow">
                            <?php
                            if (!empty($images)) {
                                $video = array(
                                    'text' => '',
                                    'link' => '',
                                    'image' => '',
                                    'thumbnail' => '',
                                );                                
                                $desc = get_option('blogname') . " - " . get_option('blogdescription');
                                $count = 1;
                                
                                foreach ($images as $image) {
                                    /*
                                    if ($count == 1) {
                                        ?>
                                        <iframe width="690" height="301" src="//www.youtube.com/embed/x_7_YOcM5Ao" frameborder="0" allowfullscreen></iframe> 
                                        <?php
                                    } */
                                    if (!empty($image['image'])) {
                                        ?>
                                        <img src="<?php echo $image['image']; ?>" alt="<?php echo $desc; ?>" title="<?php echo $desc; ?>" />
                                        <?php
                                    }
                                    $count++;
                                }
                            }
                            ?>
                        </div>
                        <?php ?>                        
                    </div>
                </div>
            </div>
            <div class="homepage-search">
                <div class="col-md-3 sections footer-icons">
                    <div class="social left">
                        <?php include('includes/follow.php'); ?>
                    </div>
                    <!--/.social-->
                </div>
                <?php  (dynamic_sidebar('Static Sidebar')) ? '' : '';  ?>
                <?php //get_sidebar(); ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        
    </div>      
    <div class="homepage-footer">
        <div class="homepage-contact">
            <ul class="contact-list">
            <?php  (dynamic_sidebar('Homepage Sidebar')) ? '' : '';  ?>
               
            </ul>
        </div>
        <div class="clear"></div>
    </div> 
</div>
<?php
get_footer();
?>
