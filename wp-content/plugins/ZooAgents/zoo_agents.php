<?php
/*
Plugin Name: Zoo Agents
Version: 1.7
Plugin URI: http://www.agentpoint.com.au/
Description: Allow new user to signup. Manage listings, agents & profiles. Also make a payment.
Author: Agentpoint
Author URI: http://www.agentpoint.com.au

*/
if(!class_exists('form_handler_ex'))
{
	require_once(dirname(__FILE__).'/includes/form_handler.class.php');
}
class zoo_agents  extends form_handler_ex{		
	function zoo_agents(){
		$this->site_url = get_option('siteurl');
		if ( '/' != $this->site_url[strlen($this->site_url)-1] )$this->site_url .= '/';
		$this->plugin_folder = substr( dirname( __FILE__ ), strlen(ABSPATH . PLUGINDIR) + 1 ) . '/';
		$this->pluginUrl = $this->site_url . PLUGINDIR . '/' . $this->plugin_folder;
		$this->form_action = "?page=" . $_GET['page'];
		$this->blogname = str_replace("&amp;","&",get_option('blogname'));
		add_action('wp_head', array($this, 'head'),0);
		add_action('login_form', array($this, 'redirect_to_dashboard'));
		add_filter('the_content',array($this, 'the_content'));
		add_action('admin_menu', array($this, 'admin_menu'));
		register_activation_hook(__FILE__, array($this, 'install'));
	}
	
	function admin_menu(){
		add_menu_page('Private Listings', 'Private Listings', 8, __FILE__, array($this, 'setting_page'));
	}	

	function setting_page(){ ?>
		<script type="text/javascript">
			window.location="<?php echo $this->site_url."dashboard/setting/"; ?>";
		</script>
	<?php
	}
	
	function install(){
		global $wpdb;		
		$wpdb->query("alter table properties add (active varchar(20) default 'Active')");
		$wpdb->query("alter table properties add (is_zoo tinyint(1) default '1')");
		$wpdb->query("alter table properties add (expire_at date)");
		$wpdb->query("alter table properties add (listingpaid  tinyint(1) default '0')");
		$wpdb->query("alter table properties add (share  tinyint(1) default '0')");
		$wpdb->query("alter table users add (contributor tinyint(1) default '0')");
		//$wpdb->query("alter table users add (`main_office_id` int(11))");
		$wpdb->query("alter table office add (contributor tinyint(1) default '0')");
		//$wpdb->query("alter table office add (`main_office_id` int(11))");
		//$wpdb->query("alter table office add (`extra_office_id` varchar(50))");
		//$wpdb->query("alter table office add (`page_id` int(11))");
		//$wpdb->query("alter table office add (`page_name` varchar(64))");
		$wpdb->query("alter table office add (`ip_address` varchar(50))");
		$wpdb->query("alter table office add (`wp_user_id` int(11))");
		
		$sql = "CREATE TABLE IF NOT EXISTS `payment_type` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`type` varchar(50) NOT NULL ,
		`price` varchar(50)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
		$wpdb->query($sql);
		
		$check=$wpdb->get_var("select id from payment_type limit 1");
		if(!$check){
			$wpdb->query("insert into payment_type values('','ResidentialSale',0), ('','ResidentialLease',0), ('','Commercial',0), ('','BusinessSale',0), ('','Share',0)");		
		}
		
		$sql = "CREATE TABLE IF NOT EXISTS `agent_account` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`officename` varchar(50),
		`unit_number` varchar(50),
		`street_number` varchar(50),
		`street` varchar(100),
		`suburb` varchar(50),
		`state` varchar(20),
		`zipcode` varchar(20),
		`firstname` varchar(50),
		`lastname` varchar(50),
		`email` varchar(50),
		`phone` varchar(50),
		`ip_address` varchar(50),
		`created_at` datetime,
		`password` varchar(50),
		`portal` varchar(50)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
		$wpdb->query($sql);
		
		$q = "CREATE TABLE IF NOT EXISTS `property_payment` (
			  `id` int(11) NOT NULL auto_increment,
			  `property_id` int(11) NOT NULL,
			  `order_amount` decimal(16,2) NOT NULL,
			  `card_number` varchar(50) default NULL,
			  `card_verification_number` varchar(50) default NULL,
			  `card_expiry_month` varchar(5) default NULL,
			  `card_expiry_year` varchar(5) default NULL,
			  `receipt_number` varchar(100) NOT NULL,
			  `paypal_account` varchar(50) default NULL,
			  `verify_sign` varchar(200) default NULL,
			  `txn_id` varchar(200) default NULL,
			  `type` varchar(10) COMMENT 'paypal or cc',
			  `created_at` datetime default NULL,
			  PRIMARY KEY  (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;";
		$r = $wpdb->query( $q );
		
		$default_add_page = array(				
		'Agent Login'=>'<!--agents_plugin: template:agent_login-->',
		'Advertise'=>'<!--agents_plugin: template:advertise-->',
		'Agent Account'=>'<!--agents_plugin: template:agent_account-->',
		'Sign Up'=>'<!--agents_plugin: template:sign_up-->',		
		'Dashboard'=>'<!--agents_plugin: template:dashboard-->',
		'Setting'=>'<!--agents_plugin: template:payment_setting-->',
		'Private Listings'=>'<!--agents_plugin: template:private_listings-->',
		'Agent Accounts'=>'<!--agents_plugin: template:agent_accounts-->',
		'Property Payment'=>'<!--agents_plugin: template:admin_payment-->',
		'Search Listings'=>'<!--agents_plugin: template:search_listings-->',
		'Listings'=>'<!--agents_plugin: template:listings-->',
		'Agents'=>'<!--agents_plugin: template:agents-->',
		'Profile'=>'<!--agents_plugin: template:profile-->',
		'Offices'=>'<!--agents_plugin: template:offices-->',
		'New Listing'=>'<!--agents_plugin: template:new_listing-->',
		'Detail'=>'<!--agents_plugin: template:detail-->',
		'Media'=>'<!--agents_plugin: template:media-->',
		'Status'=>'<!--agents_plugin: template:status-->',
		'Opens'=>'<!--agents_plugin: template:opens-->',
		'Active'=>'<!--agents_plugin: template:active-->',
		'Payment'=>'<!--agents_plugin: template:payment-->',
		'New Agent'=>'<!--agents_plugin: template:new_agent-->',
		'Edit Agent'=>'<!--agents_plugin: template:edit_agent-->',
		'New Office'=>'<!--agents_plugin: template:new_office-->',
		'Edit Office'=>'<!--agents_plugin: template:edit_office-->',
		);
		foreach($default_add_page as $post_title=>$post_content):
			$check=$wpdb->get_var("select ID from $wpdb->posts where post_content='$post_content'");
			if(!$check){
				$post_parent=$wpdb->get_var("select ID from $wpdb->posts where post_content='<!--agents_plugin: template:dashboard-->'");
				if(!$post_parent)$post_parent=0;
				$post_name=sanitize_title($post_title);
				$guid=($post_title=='Dashboard')?$this->site_url."$post_name/":$this->site_url."dashboard/$post_name/";
				$wpdb->query("insert into $wpdb->posts (post_author, post_date, post_date_gmt, post_content, post_title, post_status, comment_status, ping_status, post_name, post_modified, post_modified_gmt, post_parent, guid, menu_order, post_type, comment_count) values ('1', now(), now(), '$post_content', '$post_title', 'publish', 'open', 'open', '$post_name', now(), now(), '$post_parent', '$guid', '0', 'page', '0')");
			}
			//if(!$check)wp_insert_post(array( 'post_status' => 'publish', 'post_type' => 'page', 'post_title'=>$post_title, 'post_content'=>$post_content));
		endforeach;	
	}

	function head(){
		global $post, $wp_query;		
		if(preg_match ('/<!--agents_plugin:.*?(.*?)template:(.*?)-->/is', $post->post_content, $matches)){
			wp_print_scripts('jquery');
			$this->renderer = trim($matches[2]);
			?>
			<link href="<?php echo $this->pluginUrl; ?>css/style.css?v=0.02" media="screen" rel="stylesheet" type="text/css" />
			<?php
		}
	}	
	
	function redirect_to_dashboard(){
		global $redirect_to;
		$redirect_to=$this->site_url."dashboard/setting/";
	}	
	
	function the_content($content){
		if(empty($this->renderer))return $content;
		if(!file_exists($renderer = dirname(__FILE__).'/pages/' .$this->renderer .'.php'))return 'Error: Renderer does not exist.';		
		require ($renderer);	
	}
	
	function authorized_dir($dir){
		if(file_exists($dir))
			return @chmod($dir, 0777);
		else
			return @mkdir($dir, 0777);			
	}
	
	function authorized_dir_recursive($dir){
		if(file_exists($dir))
			@chmod($dir, 0777);
		else{
			$this->authorized_dir_recursive(dirname($dir));
			@mkdir($dir, 0777);
		}
		
		return true;
	}

	function remove_folder_contents($dir, $extension=NULL, $exceptions=array()){
		if(file_exists($dir)){
			@chmod($dir, 0777);
			$pattern = $dir .'/*'. ($extension==NULL ? '' : ".".$extension);
			$files = glob($pattern);
			if(is_array($files) && count($files)){
				foreach($files AS $file){
					@chmod($file, 0777);
					if(is_dir($file)){
						$this->remove_folder_contents($file, $extension, $exceptions);
						@rmdir($file);
					}else{			
						if(!in_array(basename($file), $exceptions))
							@unlink($file);
					}
				}
			}
		}
	}
	
	function save_file($url, $file){
		if(!@file_exists(dirname($file)))
			@mkdir(dirname($file), 0777);
		else{
			if(@file_exists($file)){
				@chmod($file);
				@unlink($file);
			}
		}
		
		$content = @file_get_contents($url);
		if($content)
			$result = @file_put_contents($file, $content);
		else
			$result = false;
		
		if($result)
			@chmod($file, 0777);
		
		return $result;
	}
	
	function text_email($to, $subject, $message, $from='', $bcc='', $flag=true){
		global $realty;
		if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
		if(empty($from)) $from = get_option('admin_email');
		if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\r\n";		
		if($flag){
			if(empty($bcc)) $bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
			if(!empty($bcc)) $bcc = "bcc: $bcc\r\n";		
		}	
		return @wp_mail($to, $subject, $message, $from . $bcc . "Content-Type: text/plain\n");		
	}
	
	function add_date($givendate,$day=0,$mth=0,$yr=0) {
		$cd = strtotime($givendate);
		$newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),    date('i',$cd), date('s',$cd), date('m',$cd)+$mth,    date('d',$cd)+$day, date('Y',$cd)+$yr));
		return $newdate;
	}
	
	function check_login($email, $password=''){
		global $wpdb;	
		$za_user_id = $wpdb->get_var("SELECT id from agent_account WHERE `email`='$email' and password='$password' LIMIT 1");		
		if($za_user_id){
			$this->set_session($za_user_id);
			return $za_user_id;
		}
		else return false;
	}

	function is_logged_in(){
		return (!empty($_SESSION['za_user_id']));			
	}
	
	function set_session($za_user_id ='') {
		if(empty($za_user_id))unset($_SESSION['za_user_id']);
		$_SESSION['za_user_id'] = $za_user_id;
	}
	
	function logout(){
		$_SESSION['za_user_id'] = '';
		//if (isset($_COOKIE[session_name()]))setcookie(session_name(), '', time()-42000, '/');		
		session_destroy();
	}
	
	function reset_password($email){	
		global $wpdb, $realty; 
		$check_email=$wpdb->get_var("select id from agent_account where email='$email'");
		if(!$check_email)return "This e-mail address is not registered.<br />";
		else{			
			$new_password = wp_generate_password(8, false);
			$wpdb->query("UPDATE `agent_account` SET `password`='$new_password' WHERE `email`= '$email'");
			$login=$realty->siteUrl.'agent-login/';
			$message="Your new password is: $new_password\n\r\n\rPlease log in here: $login\n\r\n\rBest Regards,\n\r\n\r".$this->blogname;
			if($this->text_email($email, "Your password to $this->blogname",$message, '', '', false))return "An e-mail has been sent to you with your new password.";	
		}
	}
}

$zoo_agents = new zoo_agents();
?>