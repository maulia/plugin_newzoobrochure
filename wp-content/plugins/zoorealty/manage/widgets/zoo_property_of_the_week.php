<p><label>Number</label> <br><input value="<?php echo $settings['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Title]" type="text"></p>

<h4>Image Size:</h4>
<p><input value="small" name="realty_widget_<?php echo $caption; ?>[image_size]" type="radio"<?php if($settings['image_size']=='small' || empty($settings['image_size'])) echo ' checked="checked"'; ?>> <label>Small</label> </p>
<p><input value="medium" name="realty_widget_<?php echo $caption; ?>[image_size]" type="radio"<?php if($settings['image_size']=='medium') echo ' checked="checked"'; ?>> <label>Medium</label> </p>	
<p><input value="large" name="realty_widget_<?php echo $caption; ?>[image_size]" type="radio"<?php if($settings['image_size']=='large') echo ' checked="checked"'; ?>> <label>Large</label> </p>	
<p><input value="original" name="realty_widget_<?php echo $caption; ?>[image_size]" type="radio"<?php if($settings['image_size']=='original') echo ' checked="checked"'; ?>> <label>Original</label></p>

<h4>Information to Display</h4>
<?php 
$items = array (
   'full_address', 'street_suburb','suburb','street_address', 'property_type','price', 'type', 'bedrooms', 'bathrooms','carspaces');
global $helper;
foreach ($items as $key=>$item):
if(is_array($item)): ?>
<p><input value="<?php echo (empty($settings[$key]['title']))? $helper->humanize($key):$settings[$key]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $key; ?>][title]" type="text">
<?php foreach ($item as $subitem): ?>
<br /><input value="<?php echo $subitem; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $key; ?>][display][]" type="checkbox"<?php if(is_array($settings[$key]['display']) and in_array($subitem, $settings[$key]['display'])) echo ' checked="checked"'; ?>><label> <?php echo $helper->humanize($subitem); ?></label> 
<?php endforeach;
else: ?>
<label><?php echo (empty($settings[$item]['title']) || $settings[$item]['title'] =="1")? $helper->humanize($item):$settings[$item]['title']; ?></label>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][display]" type="checkbox"<?php if($settings[$item]['display']) echo ' checked="checked"'; ?>> 
<input value="<?php echo $settings[$item]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][title]" type="text"></p>
<?php endif; endforeach; ?>

<br/>
<p>
	Load random listings from Office ID<br/>
	<input value="<?php echo $settings['office_id']; ?>" name="realty_widget_<?php echo $caption; ?>[office_id]" type="text">
</p>