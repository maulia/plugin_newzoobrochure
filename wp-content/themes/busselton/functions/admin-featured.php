<?php
$themes_folder=get_bloginfo('stylesheet_directory') .'/';
global $realty, $helper;
$office_id=implode(",",$realty->settings['general_settings']['office_id']);
$properties=$helper->get_results("select id, unit_number, street_number, street, suburb, state, type, deal_type from properties where office_id in ($office_id) and status in (1,4) order by suburb, street");
$feature_settings=get_option('realty_featured_listings');

if(empty($feature_settings['featured_listings']['sale']))$feature_settings['featured_listings']['sale']=array();
if(empty($feature_settings['featured_listings']['lease']))$feature_settings['featured_listings']['lease']=array();
$feature_settings['featured_listings']['sale']=array_unique($feature_settings['featured_listings']['sale']);
$feature_settings['featured_listings']['lease']=array_unique($feature_settings['featured_listings']['lease']);

if(!empty($feature_settings['featured_listings']['lease'])){
	$exist_ids=$helper->get_column("select id from properties where office_id in($office_id) and status in (1,4) and id in(".implode(',',$feature_settings['featured_listings']['lease']).")");	
	$delete_ids=array_diff($feature_settings['featured_listings']['lease'],$exist_ids);
	if(!empty($delete_ids)){
		$update_flag=1;
		$feature_settings['featured_listings']['lease']=array_diff($feature_settings['featured_listings']['lease'],$delete_ids);	
	}
}
if(!empty($feature_settings['featured_listings']['sale'])){
	$exist_ids=$helper->get_column("select id from properties where office_id in($office_id) and status in (1,4) and id in(".implode(',',$feature_settings['featured_listings']['sale']).")");	
	$delete_ids=array_diff($feature_settings['featured_listings']['sale'],$exist_ids);
	if(!empty($delete_ids)){
		$update_flag=1;
		$feature_settings['featured_listings']['sale']=array_diff($feature_settings['featured_listings']['sale'],$delete_ids);		
	}
}
if(!empty($update_flag))update_option('realty_featured_listings',$feature_settings);

$featured_listings=array_merge($feature_settings['featured_listings']['sale'], $feature_settings['featured_listings']['lease']);
?>
<script type="text/javascript">
function addFeatured(id, action){
	jQuery("#listings_"+id).load("<?php echo $themes_folder; ?>functions/featured_listings.php?action="+action+"&id="+id);
}
function delFeatured(id, action){
	jQuery("#listings_"+id).load("<?php echo $themes_folder; ?>functions/featured_listings.php?action="+action+"&id="+id);
}
</script>
<table class="widefat maintable" cellspacing="0">
	<thead>
	<tr>
		<th colspan="2" scope="col" class="manage-column column-title">Featured Listings</th>
	</tr>
	</thead> 
	<?php foreach($properties as $property){ 
	$property['is_for_sale'] = (stripos($property['type'],"Lease")=== false && strtolower($property['deal_type'])!='lease');
	extract ($property); 
	?>
	<tr>
		<td><a href="<?php echo $realty->siteUrl."$id/"; ?>" target="_blank"><?php echo ($unit_number!='')?"$unit_number/$street_number $street, $suburb, $state":"$street_number $street, $suburb, $state"; ?></a></td>	
		<td>
			<div id="listings_<?php echo $id; ?>">
				<?php if(!in_array($id,$featured_listings)){ 
					if($property['is_for_sale']){ ?>
					<a href="javascript:addFeatured('<?php echo $id; ?>','add_sale');">+ Mark As Featured Sale Listings</a>
				<?php }else if(!$property['is_for_sale']){ ?>
					<a href="javascript:addFeatured('<?php echo $id; ?>','add_lease');">+ Mark As Featured Lease Listings</a>
				<?php }
				}else if(in_array($id,$feature_settings['featured_listings']['sale'])){  ?>
				<a href="javascript:delFeatured('<?php echo $id; ?>','del_sale');">- Remove from Featured Sale Listings</a>
				<?php }
				else if(in_array($id,$feature_settings['featured_listings']['lease'])){  ?>
				<a href="javascript:delFeatured('<?php echo $id; ?>','del_lease');">- Remove from Featured Lease Listings</a>
				<?php } ?>
			</div>
		</td>	
	</tr>
	<?php } ?>
</table>