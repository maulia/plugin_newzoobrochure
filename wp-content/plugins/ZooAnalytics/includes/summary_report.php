<h2>Property Performance Report</h2>
Statistics for <?php echo $office_name; ?> - <a href="<?php echo $office_website; ?>"><?php echo $office_website; ?></a><br/>
Generated For: <?php echo $email; ?><br/>
Generated: <?php echo $date; ?><br/>

<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:15px 0; padding:0; font-family:Arial, Helvetica, sans-serif; line-height:normal; color:#444; font-size:12px;">
	<tr style="background-color:#003D7D ; color:#fff; font-style:bold;">
		<td style="width:140px; padding:10px 5px; border-bottom:solid 1px #eee;">Suburb</td>
		<td style="width:200px; padding:10px 5px; border-bottom:solid 1px #eee;">Address</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Num<br/>Hits</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Num<br/>Emails</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Brochure<br/>Views</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Email to<br/>Friend</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Email to<br/>Self</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Share on<br/>Twitter</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Share on<br/>Facebook</td>
		<td style="width:70px; padding:10px 5px; border-bottom:solid 1px #eee; text-align:center">Shortlisted</td>					
	</tr>
	<?php $i=0;foreach ($properties as $property): if(!is_array($property))continue; ?>
	<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
		<td style="padding:5px 0; border-bottom:solid 1px #eee;"><a href="<?php echo $property['url']; ?>" style="text-decoration:none; color:#003D7D;" title="<?php echo $property['address']; ?>"><?php echo $property['suburb']; ?></a></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee;"><?php echo $property['address']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['visit']; ?> </td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['enquiry']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['brochure']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['email_friend']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['email_self']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['share_on_twitter']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['share_on_facebook']; ?></td>
		<td style="padding:5px 0; border-bottom:solid 1px #eee; text-align:center"><?php echo $property['favorite']; ?></td>				
	</tr>
	<?php $i++; endforeach; ?>
</table>