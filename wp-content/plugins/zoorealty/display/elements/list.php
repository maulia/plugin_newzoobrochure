<?php $listings = $params; ?>
<?php global $realty; 
$src=str_replace('wp-content/plugins/Realty/style/stickers/','',$realty->settings['property_stickers']['sticker_src']);
$style="style".(str_replace("sticker","",str_replace(".png","",$src))+1);
switch($realty->settings['property_stickers']['sticker_colour']){
case '#2b497d';$colors='dk_blue';break;
case '#2b7d36';$colors='dk_green';break;
case '#857a82';$colors='grey';break;
case '#70c141';$colors='lite_green';break;
case '#4cb2c2';$colors='lite_blue';break;
case '#d78c23';$colors='orange';break;
case '#7e2a1d';$colors='red';break;
case '#7d5f2b';$colors='tan';break;
case '#d7ce31';$colors='yellow';break;
}
?>
<div id="list_format">
<table class="property" width="100%" border="0" cellspacing="0" cellpadding="0">
<?php			
foreach($listings as $listing):
	if(!is_array($listing)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned			
	$printedTd = false;
	$count = 0;
	?>
	<tr <?php echo ($alt_class = ($alt_class==' class="alt"') ? '':' class="alt"'); ?>>
		<td class="listCont">
    	<div class="listWrap">
		<?php
		 if(is_array($realty->settings['search_results'])) 
			foreach($realty->settings['search_results'] as $key=>$values):
				if($key =='thumbnail'):					
					if( $count > 0 ):
						echo '</div>';
						$printedTd = false;
					endif;
				?>
					<div class="contImage">
				<?php
					elseif($key !='thumbnail' and !$printedTd):
				?>	
					<div class="contDesc">
				<?php 
					$printedTd = true;
				endif;	
				switch($key):
					case 'thumbnail': ?>
					<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>">	
						<img src="<?php echo $listing['thumbnail']; ?>" alt="<?php echo $listing['headline']; ?>" />
                    </a>
						<?php  if ($listing['new']):?>
						<div class="fresh_place">
						</div>
						<?php elseif ($listing['is_sold']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker sold ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo $realty->settings['property_stickers']['sticker_text']['sold'];?></span>
							</div>
						</div>
						<?php elseif ($listing['is_leased']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker leased ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo $realty->settings['property_stickers']['sticker_text']['leased'];?></span>
							</div>
						</div>
						<?php elseif ($listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker under_offer ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo $realty->settings['property_stickers']['sticker_text']['under_offer'];?></span>
							</div>
						</div>
						<?php elseif (!empty($listing['is_open_home']) && !$listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker open_home ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo ($realty->settings['property_stickers']['sticker_text']['open_home']=='')?'Open Home':$realty->settings['property_stickers']['sticker_text']['open_home'];?></span>
							</div>
						</div>
						<?php endif;  
					break;
					case 'address':
						if(is_array($values)) foreach($values as $addressPart)${$addressPart} = $listing[$addressPart];
						$address='';
						if(!empty($street_address))$address=$street_address.", ";
						if(!empty($suburb))$address.=$suburb.", ";
						if(!empty($state))$address.=$state.", "; ?>
						<p class="street-address"><a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>"><?php echo substr($address,0,-2) ;?></a></p>						<?php
					break;
					case 'description':?>
					<p class="description"><?php echo $helper->_substr($listing['description'], $values); ?>&hellip;<a href="<?php echo $listing['url']; ?>" title="View Property">More &raquo;</a></p>
					<?php
					break;
					case 'building_size':
						if(!empty($listing[$key])): ?>
							<div class="land_building_size">
								<p>Building: <span class="building_size"><?php echo $listing[$key]." ".$listing['floor_area_metric']; ?></span></p>
							</div>
							<?php
                            endif;				
					break;
					case 'land_size':						
						if(!empty($listing[$key])): ?>
								<div class="land_building_size">
									<p class="land_size">LAND: <span class="land_size"><?php echo $listing[$key]." ".$listing['land_area_metric']; ?></span></p>
								</div>
							<?php
                            endif;          			
					break;
					case 'current_rent':?>
						<p class="current_rent">Current Rent : <?php echo '$'.number_format($listing['current_rent']); ?></p>
					<?php
					break;
					case 'auction':
						if (!empty($listing[$key]) && $listing[$key]!='00:00:00'):	?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php
						endif;
					break;
					case 'price':
						if (!$listing["is_auction"] && $listing["status"]!='2'):?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php elseif($listing["status"]=='2'): ?>
							<p class="<?php echo $key; ?>"><?php echo ($listing['display_sold_price'])? $listing['last_price']:'Undisclosed'; ?></p>
						<?php else : ?>
							<p class="<?php echo $key; ?>">Auction: <?php 
								if(!empty($listing["auction_place"]))echo $listing["auction_place"].", ";							
								echo $listing["auction_date"]; 
								if(!empty($listing["auction_time"]))echo " at ".$listing["auction_time"]; ?>
							</p>
						<?php endif;
					break;
					case 'action_buttons': ?>
						<div class="more-buttons">
							<ul class="action-buttons">
								<?php 
								foreach($values as $param){
									switch($param){
										case 'photos':
											$photo = $realty->photos($listing['id'], "photos", array('800x600'));
											if(!empty($photo)){
												$photos = array();
												$j='0';$k='0';$l='0';
												for ($i=0;$i<count($photo);$i++){
													if($photo[$i]['small']!=""){ $photos[$j]['small']=$photo[$i]['small']; $j++; }
													if($photo[$i]['medium']!=""){ $photos[$k]['medium']=$photo[$i]['medium']; $k++; }
													if($photo[$i]['large']!=""){ $photos[$l]['large']=$photo[$i]['large']; $l++; }
												} 
												 ?>
												<li class="button photo_btn">
													<a class="link-lightbox view_photos" href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[photo_gallery<?php echo $listing['id']?>]" title="View SLIDE SHOW">SLIDE SHOW</a>
													<?php for ($i=1;$i<count($photos);$i++){ ?>
													<a href="<?php echo $photos[$i]['large']; ?>" onclick="return false;" rel="prettyPhoto[photo_gallery<?php echo $listing['id']?>]"></a>
													<?php } ?>
												</li>				
											<?php
											}
										break;
										case 'floorplans':
											$floorplans = $realty->photos($listing['id'], "floorplans", array('800x600'));	
											if(!empty($floorplans)){
												$floorplan = array();
												$j='0';$k='0';$l='0';
												for ($i=0;$i<count($floorplans);$i++){
													if($floorplans[$i]['small']!=""){ $floorplan[$j]['small']=$floorplans[$i]['small']; $j++; }
													if($floorplans[$i]['medium']!=""){ $floorplan[$k]['medium']=$floorplans[$i]['medium']; $k++; }
													if($floorplans[$i]['large']!=""){ $floorplan[$l]['large']=$floorplans[$i]['large']; $l++; }
												}
												 ?>
												<li class="button floorplan">
													<a class="link-lightbox view_photos" href="<?php echo $floorplan[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[floorplan<?php echo $listing['id']?>]" title="View FLOOR PLAN">FLOOR PLAN</a>
													<?php for ($i=1;$i<count($floorplan);$i++){ ?>
													<a href="<?php echo $floorplan[$i]['large']; ?>" onclick="return false;" rel="prettyPhoto[floorplan<?php echo $listing['id']?>]"></a>
													<?php } ?>
												</li>					
											<?php
											}
										break;
										case 'map':
											if(!empty($listing['latitude']) && !empty($listing['longitude'])){ ?>								
												<li class="button map">
													<a class="link-lightbox" href="#" onclick="openbox('Map', 'filter_map', 'box_map', 'boxtitle_map', 'load_form_map', '<?php echo $realty->pluginUrl.'display/elements/lightbox_map.php?property_id='.$listing['id'].'&siteurl='.$realty->siteUrl; ?>')" title="MAP">MAP</a>
												</li>								
											<?php }
										break;		
										case 'video':
											if(!empty($listing['property_video'])){ ?>								
												<li class="button video">
													<a class="link-lightbox" href="#" onclick="openbox('Video', 'filter_video', 'box_video', 'boxtitle_video', 'load_form_video', '<?php echo $realty->pluginUrl.'display/elements/lightbox_video.php?property_id='.$listing['id']; ?>')" title="VIDEO">VIDEO</a>
												</li>								
											<?php }
										break;											
									}
								} ?>
							</ul>
						</div>
					<?php
					break;
					case 'number_of_rooms': 
						if ( !$listing['is_land'] and is_array($values) and !empty($values)): 
						?>
						
						<div class="rooms-wrap">							
							<ul class="rooms">
							<?php  
							foreach($values as $param):
								if(!empty($listing[$param])): ?>
								<li class="<?php echo $param; ?>">
									<span class="room_count"><?php echo $listing[$param]; ?></span> <span class="room_type"><?php //echo $param; ?></span>
								</li>
								<?php
								endif;
							endforeach; ?>
							</ul>
						</div>
						<?php 
						endif;	
					break;
					case 'primary_contact': 
						$user = $realty->user($listing['user_id']);
						if(!empty($user)):?>
						<p>Contact</p>
						<?php
							foreach($values as $param):
								if(!empty($user[$param])): ?>
								<p>
								<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $user[$param]; ?>"><?php echo $user[$param]; ?></a>
								<?php else: ?>
								<?php echo $user[$param]; ?>
								<?php endif; ?>
								</span>
								</p>
								<?php
								endif;
							endforeach;
						endif;						
					break;
					case 'opentimes':
						$opentimes = $this->opentimes($listing['id']); 
						if(!empty($opentimes)) :?>	
						<p><span>Open</span> Times</p>
						<ul>
						<?php foreach($opentimes as $opentime): ?>
							<li><span class="open_date"><strong>
							<?php echo date('l, j M Y', strtotime($opentime['date'])); ?></strong></span><br />
							<span class="open_time"><?php echo $opentime['start_time']; ?> - <?php echo $opentime['end_time']; ?></span></li>
						<?php endforeach; ?>
						</ul>
						<?php
						endif;
					break;
					case 'available_at':
						if(!empty($listing[$key]) && $listing[$key]!='0000-00-00'):	?>	
							<p>Available on: </p>						
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
						endif;					
					break;	
					case 'secondary_contact':
						if(!empty($listing['secondary_user']) && $listing['secondary_user']!= $listing['user_id'])$secondary_user = $this->user($listing['secondary_user']);
						if(!empty($secondary_user)):
							foreach($values as $param):
								if(!empty($user[$param])):?>
								<p><span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $secondary_user[$param]; ?>"><?php echo $secondary_user[$param]; ?></a>
								<?php else: ?>
								<?php echo $secondary_user[$param]; ?>
								<?php endif; ?>
								</span>
								</p>
								<?php
								endif;
							endforeach;
						endif;					
					break;							
					default:
						if(!empty($listing[$key])):		?>							
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
						endif;
					break;
				endswitch;
				if($key =='thumbnail')
					echo '</div>';
				++$count;
			endforeach; 
			if($key !='thumbnail') ?>
				</div><!--ends .tdContent-->
				<div class="clear"></div>
				</div><!--ends .listWrap-->            
			</td>
		</tr><!-- ends property <?php //echo $listing['id']; ?> TR -->
	<?php endforeach; ?>	
	</table>
</div>