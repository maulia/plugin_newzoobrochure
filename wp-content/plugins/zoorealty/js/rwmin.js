heroIMages();
function heroIMages(){
	hideAll();
	setFirstVisible();
	setLinks();
	setThumbnailLinks();
	preLoadCurrentImage(0);
	preLoadImages();
	toggleArrows()
}
function hideAll(){
	jQuery.each(jQuery("li.gMainImg"),
	function(a,b){if(a>0){jQuery(b).hide()}});
	jQuery("li.gMainFloorplan").hide()
}
function setFirstVisible(){
	jQuery(jQuery("li.gMainImg")[0]).show();
	jQuery(jQuery("li.gThumbImg")[0]).addClass("active")
}
function nextImage(){
	var c=getCurrentImage();
	var b=c;
	var a=getNextId();
	hideShowImages(b,a)
}
function previousImage(){
	var c=getCurrentImage();
	var b=c;
	var a=getPreviousId();
	hideShowImages(b,a)
}
function getNextId(){
	var b=getCurrentImage();
	var a=(b>=(jQuery("li.gMainImg").length-1))?0:b+1;return a
}
function getPreviousId(){
	var b=getCurrentImage();
	var a=(b==0)?jQuery("li.gMainImg").length-1:b-1;
	return a
}
function hideShowImages(b,a){
	preLoadCurrentImage(a);
	jQuery(jQuery("li.gMainImg")[b]).hide();
	jQuery(jQuery("li.gThumbImg")[b]).removeClass("active");
	jQuery(jQuery("li.gMainImg")[a]).show();
	jQuery(jQuery("li.gThumbImg")[a]).addClass("active");
	preLoadImages()
}
function getCurrentImage(){
	var a=0;jQuery.each(jQuery("li.gMainImg"),function(b,c){if(jQuery(jQuery("li.gMainImg")[b]).is(":visible")){a=b}});
	return a
}
function setLinks(){
	jQuery(".rwOHero-next").click(function(){nextImage();return false});
	jQuery(".rwOHero-prev").click(function(){previousImage();return false})
}
function setThumbnailLinks(){
	jQuery.each(jQuery("li.gThumbImg"),function(a,b){jQuery(b).click(function(){
	var e=getCurrentImage();
	var d=e;
	var c=a;
	hideShowImages(d,c)
	});
	jQuery(b).hover(function(){jQuery(this).css({cursor:"pointer"})},function(){jQuery(this).css({cursor:"default"})})})
}
function preLoadCurrentImage(b){
	var a=b;try{jQuery(jQuery(".gMainImg img")[a]).attr("src",jQuery(jQuery(".gMainImg img")[a]).attr("load-src"))}catch(c){}
}
function preLoadImages(){
	var a=getNextId();var b=getPreviousId();try{jQuery(jQuery(".gMainImg img")[a]).attr("src",jQuery(jQuery(".gMainImg img")[a]).attr("load-src"));jQuery(jQuery(".gMainImg img")[b]).attr("src",jQuery(jQuery(".gMainImg img")[b]).attr("load-src"))}catch(c){}toggleArrows()
}
function toggleArrows(){
	try{if(jQuery("li.gMainImg.video").is(":visible")){jQuery(".rwOHero-prev").hide();jQuery(".rwOHero-next").hide()}else{jQuery(".rwOHero-prev").show();jQuery(".rwOHero-next").show()}}catch(a){}
}	