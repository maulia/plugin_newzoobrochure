<?php
global $realty;

if($_GET['action']=='logout'){ $this->logout(); }
if($_GET['action']=='forgot_password'){
	if(isset($_POST['reset_password']))echo $this->reset_password($_POST['user_email']); ?>
	<p>Enter your e-mail address and we will send you a new password.</p>
    <form method="post" action="?action=forgot_password" class="form-agent-log">
        <ol class="cf-ol">
        	<li><label>Email <span class="red">*</span></label><input name="user_email" type="text"  value="<?php echo $_POST['user_email']; ?>" maxlength="200" /></label></li>
        	<li><label>&nbsp;</label><input type="submit" name="reset_password" class="button" value="Submit" /></li>
        </ol>
        <p><a href="?action=login" title="Log In">Click here to Log In</a></p>
	</form>
<?php }
elseif(!$this->is_logged_in()){
	if(isset($_POST['submit'])){
		extract ($_POST);	
		if(empty($user_email))$error.="Please fill your email<br/>";
		if(empty($password))$error.="Please fill your password<br/>";
		if(!empty($error))echo "<p class='red'>$error</p>";
		
		if(empty($error)){		
			$za_user_id=$this->check_login($user_email, $password);
			if(!$za_user_id)echo "<p class='red'>The email or password entered is incorrect.</p>";
			else{
				 ?>
				<script type="text/javascript">
				location.href='<?php echo $realty->siteUrl.'dashboard/listings/'; ?>';
				</script>
				<?php
			}
		}
	}  ?>
	<form method="post" action="" class="form-agent-log">
    	<ol class="cf-ol">
        	<li><label>Email <span class="required">*</span></label> <input name="user_email" type="text"  value="<?php echo $_POST['user_email']; ?>" maxlength="200" /></li>
            <li><label>Password <span class="required">*</span></label> <input name="password" type="password"  value="" maxlength="200" /></li>
            <li><label>&nbsp;</label><input type="submit" name="submit" class="button" value="Login" /></li>
        </ol>
		<p>Forgot your password? <a href="?action=forgot_password" title="Forgot your password?">Click here.</a><br />Having Trouble to Log in ? Press CTRL + F5 to refresh Cache of your Browser.</p>
	</form>
<?php }else{ ?>
<script type="text/javascript">
location.href='<?php echo $realty->siteUrl.'dashboard/listings/'; ?>';
</script>
<?php } ?>