<?php
if(!empty($realty->params['list']))$list=$realty->params['list'];
else $list=($_GET['list']=='')?'sale':$_GET['list'];
$type = (strpos($list, 'lease') !== false || strpos($list, 'Lease') !== false)? 'lease': 'sale';
$price_options = $settings[$type];
$max_price = end($price_options);

if($realty->renderer=='sold'){ $status='2,6'; }
else {
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']=='1')$status='1,2,4,6';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']!='1')$status='1,2,4';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']=='1')$status='1,4,6';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']!='1')$status='1,4';
}


$property_types = $realty->property_search('property_type',$list,'',$status);
$suburbs = $realty->property_search('suburb',$list,'',$status);

global $post;
$post_action=($realty->params['list']=='')?get_option('siteurl').'/search-results/':get_permalink($post->ID);
?>
<div id="quick_search" style="width: 280px;">
	<h2 class="section_title" style="float:none">Property Search</h2>
	<form id="search_quick" name="search_quick" action="<?php echo $post_action; ?>" method="get">		
		<fieldset>
			<div class="search_quick-wrap">
				<ul class="qs-ul">
					<?php if(empty($realty->params['list'])){ ?>
					<input type="hidden" id="list" name="list" value="<?php echo $type;?>">
					<li>
						<div class="search_selection"><label>Search Type:</label><label>Buy</label><input type="radio" name="nill" class="radio" onclick="javascript:showtab('sale');document.getElementById('list').value='sale';" <?php if($type=='sale') echo 'checked="checked"'; ?>>
							<label>Rent</label><input type="radio" name="nill" class="radio" onclick="javascript:showtab('lease');document.getElementById('list').value='lease';" <?php if($type=='lease') echo 'checked="checked"'; ?>> 
							<div class="clear"></div>
						</div>
					</li>
					<?php } ?>
					<li class="location">
						<div class="suburb_select">
							<select name="suburb[]" multiple="multiple" id="select_suburb" class="select_suburb"  style="margin-top:5px;">
								<option value="">All Suburbs</option>
								<?php foreach ($suburbs as $suburb): if(!is_array($suburb)) continue; ?>
									<option value="<?php echo $suburb['tag']; ?>"<?php if(is_array($_GET['suburb']) and in_array($suburb['tag'], $_GET['suburb'])) echo ' selected="selected"'; ?>><?php echo $suburb['tag']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<p class="instruction">To select multiple, hold the Ctrl key and click.</p>
					</li>					
					<li class="search_property_types">
						<div class="property_type">
							<select name="property_type" id="select_property_type">
								<option value="">Property Type</option>
								<?php foreach($property_types as $property_type): if(!is_array($property_type)) continue; ?>
								<option value="<?php echo $property_type['tag']; ?>"<?php if( $property_type['tag']==$_GET['property_type']) echo ' selected="selected"'; ?>><?php echo $property_type['tag']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="bedrooms numberofrooms">
							<select name="bedrooms" class="bedrooms">
								<option value="">Beds</option>
								<?php for ( $i = 1 ; $i <= $settings['max_beds']; ++$i ) : ?>
								<option value="<?php echo $i ?>"<?php if( $i==$_GET['bedrooms']) echo ' selected="selected"'; ?>><?php echo $i; if ($i == $settings['max_beds']) echo '+';?></option>
								<?php endfor;	?>
							</select>
						</div>
					</li>					
					<li id="price_for_sale" class="price">
						<div class="price_min prices">
							<select name="price_min">
								<option value="">Min Price</option>
								<?php foreach($price_options as $price_option): 
								$delimiter = strpos($price_option, ':'); 
								$value = substr($price_option, 0, $delimiter); ?>
								<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_min']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1); //if($price_option ==$max_price) echo '+'; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					
						<div class="price_max prices">
							<select name="price_max">
								<option value="">Max Price</option>
								<?php foreach($price_options as $price_option): 
								$delimiter = strpos($price_option, ':'); 
								$value = substr($price_option, 0, $delimiter); ?>
								<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_max']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1); if($price_option ==$max_price) echo '+'; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</li>
				</ul>

				<p class="button quick_search_btn"><input type="submit" value="Search" class="btn" /></p>
			</div>
			
		</fieldset>
	</form>
	<div class="clearer"></div>
</div>

<div class="clearer"></div>
<script type="text/javascript">
function showtab(type) {
	var url2 = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?type="+escape( type )+"&cat=property_type";	
	jQuery('#select_property_type').load(url2);
	
	var url = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?type="+escape( type )+"&cat=price";
	jQuery('#price_for_sale').load(url);
	
	var url1 = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?type="+escape( type )+"&cat=suburb";
	jQuery('#select_suburb').load(url1);	
 }
</script>