<?php global $options; foreach ($options as $value) {if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }} ?>

		<div class="clear"></div>
	</div><!-- #main_body -->

</div><!-- .wrap -->

<?php if (!is_page('print-property')){?>
	<div id="footer">
        <div id="footerwrap">
            <div class="footer-menu">
                <?php wp_nav_menu(array('menu' => 'Main Nav', 'container_class' => 'nav', 'after' => '<span class="separator"></span>')); ?>
                <div class="footer-logo"><img src="<?php echo $logo_footer; ?>" alt="<?php bloginfo('name'); ?>" /></div>
                <div class="clear"></div>
            </div>
            <div class="footer-credit">
                <p>&copy; Copyright <?php echo date("Y"); ?> <?php bloginfo('name'); ?>.  Site by <a href="http://wwww.agentpoint.com.au/" target="_blank">Agent Point</a></p>
            </div>
	    
            <div class="clear"></div>
        </div>
	</div>
<?php } ?>

<?php if(is_page('property')) { ?>
	<div id="filter_map" class="filter_lightbox"></div>
	<div id="box_map" class="box_lightbox">
		<span id="boxtitle_map" class="boxtitle_lightbox"></span>
		<div id="load_form_map" class="frame"></div>    
	</div>
	<div id="filter_walk" class="filter_lightbox"></div>
	<div id="box_walk" class="box_lightbox">
		<span id="boxtitle_walk" class="boxtitle_lightbox"></span>
		<div id="load_form_walk" class="frame"></div>    
	</div>
    <div id="filter_video" class="filter_lightbox"></div>
	<div id="box_video" class="box_lightbox">
		<span id="boxtitle_video" class="boxtitle_lightbox"></span>
		<div id="load_form_video" class="frame"></div>
	</div>

<?php }?>

<?php wp_footer(); ?>

<?php //echo $analytics_tracking_code; 
	$ga_code=get_option('ga_code');
if ( !empty( $ga_code ) ) { 
	?>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<?php echo $ga_code; ?>']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
   <?php }  ?>
<!-- <?php echo get_num_queries(); ?> queries in <?php timer_stop(1); ?>  seconds. -->
</body>
</html>
