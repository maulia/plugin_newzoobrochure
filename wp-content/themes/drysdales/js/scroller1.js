function getLeft() {
	// get the left of the content
	var left = jQuery('#scroll-content').css('left');
	return trimPx(left);
}

function getWidth(id) {
	// get the width, including padding
	var width = jQuery(id).width();
	var paddingTop = trimPx(jQuery(id).css("padding-left"));
	var paddingBottom = trimPx(jQuery(id).css("padding-right"));

	return width + paddingTop + paddingBottom;
}

function trimPx(value) {
	// remove "px" from values
	var pos = value.indexOf("px");
	if (pos != 0)
		return parseInt(value.substring(0, pos));
	else
		return 0;
}

var container;
var content;
var hidden;	// # of pixels hidden by the container

function setScrollerDimensions() {
	container = getWidth("#scroll-container");
	content = getWidth("#scroll-content");
	hidden = content - container;
}

function resetScroller() {
	setScrollerDimensions();
	jQuery('#scroll-content').css('left', 0);
}

jQuery(document).ready(function() {

	setScrollerDimensions();

	jQuery('#scroll-controls a.up-arrow').click(function() {
		return false;
	});

	jQuery('#scroll-controls a.down-arrow').click(function() {
		return false;
	});

	jQuery('#scroll-controls a.down-arrow').hover(
		function() {
			if (hidden > 0) {
				var current = getLeft();
				jQuery('#scroll-content').animate({ left: -hidden }, Math.abs(current - hidden) * 5);
			}
		},
		function() {
			jQuery('#scroll-content').stop();
		}
	);

	jQuery('#scroll-controls a.up-arrow').hover(
		function() {
			if (hidden > 0) {
				var current = getLeft();
				jQuery('#scroll-content').animate({ left: "0" }, Math.abs(current) * 5);
			}
		},
		function() {
			jQuery('#scroll-content').stop();
		}
	);
});