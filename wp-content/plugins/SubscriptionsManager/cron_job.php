<?php
$path = dirname(dirname(dirname(dirname(__FILE__))));
require($path.'/wp-load.php');
ignore_user_abort(true);
set_time_limit( 0 ); 
ini_set('memory_limit', '-1');
$subscriptions_manager = new subscriptions_manager;
$subscriptions_manager->send_alerts_by_subscribers();
?>