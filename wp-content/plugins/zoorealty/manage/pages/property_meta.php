<?php
//metadata: access_level=8
if(isset($_POST['submit'])):
	update_option('realty_property_meta_title', str_replace('\"','',str_replace("\\","",$_POST['realty_property_meta_title'])));
	update_option('realty_property_meta_description', str_replace('\"','',str_replace("\\","",$_POST['realty_property_meta_description'])));
	update_option('realty_property_meta_num_of_desc', $_POST['realty_property_meta_num_of_desc']);
	update_option('realty_property_meta_keywords', str_replace('\"','',str_replace("\\","",$_POST['realty_property_meta_keywords'])));
	update_option('realty_property_meta_other', str_replace('\"','',str_replace("\\","",$_POST['realty_property_meta_other'])));
	$return = 'Settings Updated.';
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
?>
<div class="wrap">
<form name="" method="post" action="">
<h2>Property Pages Meta</h2> 
<table class="widefat"> 
<tr class="alternate">
	<td>Title</td>
	<td colspan="2"><textarea name="realty_property_meta_title" cols="90"><?php echo get_option('realty_property_meta_title');?></textarea></td>
</tr>
<tr>
	<td>Description</td>
	<td><textarea name="realty_property_meta_description" cols="90"><?php echo get_option('realty_property_meta_description');?></textarea></td>
	<td align="right"><input type="text" name="realty_property_meta_num_of_desc" size="5" value="<?php echo get_option('realty_property_meta_num_of_desc');?>">(Number of Characters)</td>
</tr>
<tr class="alternate">
	<td>Keywords</td>
	<td colspan="2"><textarea name="realty_property_meta_keywords" cols="90"><?php echo get_option('realty_property_meta_keywords');?></textarea></td>	
</tr>
<tr>
	<td>Other</td>
	<td colspan="2"><textarea name="realty_property_meta_other" cols="90"><?php echo get_option('realty_property_meta_other');?></textarea></td>
</tr>
</table> 
<p>Below are a list of tags you can use.</p>
<textarea name="realty_property_meta_list" cols="90"><?php echo get_option('realty_property_meta_list');?></textarea>

 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->