<div id="location_info">
	<?php
	$post_parent=$wpdb->get_var("select ID from $wpdb->posts where post_content='[location_info_plugin]' and post_status='publish' and post_type='page' limit 1");
	$suburb_profiles_page=get_permalink($post_parent);
	echo str_replace('[location_info_plugin]', '',$post->post_content); 
	echo $location['pagination_above']; ?>
	<div class="location-list">
	<?php 
	$count=0;
	foreach($location as $item):
		if(!is_array($item) || empty($item['suburb']))continue; 
		$permalink=get_permalink($item['page_id']);		
		?>
			<div class="location-wrap<?php echo ($count==0 ? ' first-item' : ''); ?>">               
				<?php if(!empty($item['photos'])) { ?>
				<div class="image">
					<a href="<?php echo $permalink; ?>" title="<?php echo ucwords(strtolower($item['suburb'])); ?>"><img src="<?php echo $item['photos'][0]['path']; ?>" /></a>
				</div>
				<?php } ?>
				
				<div class="location-desc"><h2><?php echo ucwords(strtolower($item['suburb'])); ?></h2><?php echo $helper->_substr($item['description'],500); ?>&hellip;<p class="view-more"><a href="<?php echo $permalink; ?>" title="View More">View More &raquo;</a></p></div>
				<div class="clear"></div>
			</div>
		<?php $count++; endforeach; ?>
	</div>
	
	<div id="bottom_pagination"><?php echo $location['pagination_above']; ?></div>
</div>