<?php
if(!empty($_GET['id'])){
	require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/wp-load.php');
	global $helper;
	$item=$helper->get_results("select * from testimonials where id=".$_GET['id']);
	$item=$item[0];
}
?>
<form id="new_testi_form" class="form-profile" action="" onsubmit="return false;">
	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
	<input type="hidden" name="user_id" value="<?php echo $_GET['user_id']; ?>">
	<h3>Add Testimonial</h3>
    <table class="widefat">
		<tr>
			<td class="label"><strong>Name<span class="red"> *</span></strong></td>
			<td><input type="text" class="widefat" id="user_name" value="<?php echo stripslashes(str_replace('\\','',$item['user_name'])); ?>" name="user_name"></td>
		</tr>
		<tr>
			<td class="label"><strong>Content<span class="red"> *</span></strong></td>
			<td><textarea class="widefat" id="content" name="content"><?php echo stripslashes(str_replace('\\','',$item['content'])); ?></textarea></td>
		</tr>
        <tr>
        	<td></td>
            <td class="button_testi"><input type="submit" name="submit" value="Save" class="button"><input type="button" class="button" name="cancel" value="Cancel" onClick="close_testi();"></td>
        </tr>
	</table>
</form>	

<script type="text/javascript">
jQuery('#new_testi_form').submit(function() {
	if(jQuery("#user_name").val()=='')alert('Please fill name.');
	else if(jQuery("#content").val()=='')alert('Please fill content.');
	else{
		var query = jQuery(this).serialize();
		jQuery.get(load_testi+query, function(data) {
			alert("Saved");
			close_testi();
			jQuery("#testimonials").html(data);
		});	
		
	}
	return false;
});
</script>