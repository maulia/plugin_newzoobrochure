<?php get_header(); ?>
<?php 
	query_posts('cat=-9');
?>
<div id="content">
    <h1 class="page-title">News</h1>
    <div id="blog-div" class="default-post posts-index">

	<?php if (have_posts()) : ?>
	
		<?php while (have_posts()) : the_post(); ?>

            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
    
                <?php /*if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; }*/ ?>
                <div class="post_title_date<?php if(!function_exists('userphoto_the_author_thumbnail')) { echo ' no_author_thumb'; } ?>">
                    <h2 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                </div>
    
                <div class="entry">
                    <?php the_excerpt_reloaded(50, '<a><img>', 'excerpt', TRUE, 'Read more <span class="aquo">&#187;</span>'); ?>
                    <?php /*global $wpdb, $table_prefix; $plugin_table=$table_prefix . "property_ads"; $q=$wpdb->get_results("select * from $plugin_table where post_id=".$id."");
                    if(!empty($q)){ ?>
                        <p class="post-button"><a class="btn" href="<?php echo get_option('siteurl')."/".$q[0]->property_id;?>">View <?php echo $q[0]->street.", ".ucwords(strtolower($q[0]->suburb)); ?> </a></p>
                        <div class="clear"></div>
                    <?php }*/ ?>
                </div>
                
                <div class="post_data"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> on <?php the_time('jS F, Y') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments <span class="aquo">&#187;</span>', '1 Comment <span class="aquo">&#187;</span>', '% Comments <span class="aquo">&#187;</span>'); ?></div>
    
            </div>
    
        <?php endwhile; ?>

        <div class="navigation">
            <p class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></p>
            <p class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
        </div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div><!-- end #blog-div -->
    
</div><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>