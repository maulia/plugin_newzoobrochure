<?php include 'Qvalent_PayWayAPI.php' ?>
<?php

    $initParams =
        "certificateFile=/home/c-web/c7/10/mhifs.com.au/www/register/ccapi.pem" . "&" .
        "caFile=/home/c-web/c7/10/mhifs.com.au/cacert/cacerts.crt" . "&" .
        "logDirectory=/home/c-web/c7/10/mhifs.com.au/logs/";
    $paywayAPI = new Qvalent_PayWayAPI();
    $paywayAPI->initialise( $initParams );

    //----------------------------------------------------------------------------
    // Get request information
    //----------------------------------------------------------------------------
    $orderECI               = "SSL";
    $orderType              = "capture";

    $cardNumber             = $_POST[ "cardNumber" ];
    $cardVerificationNumber = $_POST[ "cardVerificationNumber" ];
    $cardExpiryYear         = $_POST[ "cardExpiryYear" ];
    $cardExpiryMonth        = $_POST[ "cardExpiryMonth" ];

    $cardCurrency           = "AUD";
    $orderAmountCents       = number_format( (float)$_POST[ "orderAmount" ] * 100, 0, '.', '' );

    $customerUsername       = "Q11579";
    $customerPassword       = "Asyjrxbsz";
    $customerMerchant       = "23549736";
    $orderNumber            = time();

    //----------------------------------------------------------------------------
    // Process credit card request
    //----------------------------------------------------------------------------
    $requestParameters = array();
    $requestParameters[ "order.type" ] = $orderType;
    $requestParameters[ "customer.username" ] = $customerUsername;
    $requestParameters[ "customer.password" ] = $customerPassword;
    $requestParameters[ "customer.merchant" ] = $customerMerchant;
    $requestParameters[ "customer.orderNumber" ] = $orderNumber;
    $requestParameters[ "customer.captureOrderNumber" ] = $orderNumber;
    $requestParameters[ "card.PAN" ] = $cardNumber;
    $requestParameters[ "card.CVN" ] = $cardVerificationNumber;
    $requestParameters[ "card.expiryYear" ] = $cardExpiryYear;
    $requestParameters[ "card.expiryMonth" ] = $cardExpiryMonth;
    $requestParameters[ "card.currency" ] = $cardCurrency;
    $requestParameters[ "order.amount" ] = $orderAmountCents;
    $requestParameters[ "order.ECI" ] = $orderECI;

    $requestText = $paywayAPI->formatRequestParameters( $requestParameters );

    $responseText = $paywayAPI->processCreditCard( $requestText );

    // Parse the response string into an array
    $responseParameters = $paywayAPI->parseResponseParameters( $responseText );

    // Get the required parameters from the response
    $summaryCode = $responseParameters[ "response.summaryCode" ];
    $responseCode = $responseParameters[ "response.responseCode" ];
    $description = $responseParameters[ "response.text" ];
    $receiptNo = $responseParameters[ "response.receiptNo" ];
    $settlementDate = $responseParameters[ "response.settlementDate" ];
    $creditGroup = $responseParameters[ "response.creditGroup" ];
    $cardSchemeName = $responseParameters[ "response.cardSchemeName" ];
?>


<?php

if($responseCode == "08"){
	require("payment_thankyou.php");
	}else{
	require("payment_declined.php");
	}
?>