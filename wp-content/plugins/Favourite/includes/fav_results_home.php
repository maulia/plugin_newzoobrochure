<?php
		$odd_class = "";
		foreach($properties as $property):if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned
			if($count == 2) break; 
			extract($property,EXTR_PREFIX_ALL,"property");			
			$count++;
			?>
			<div class="fav_image<?php echo ($odd_class = empty($odd_class) ? ' alt' : ''); ?>">
				<div class="image">
				<a href="<?php echo $property_url; ?>" title="<?php echo $property_address; ?>">
					<img src="<?php echo $property_photo_url; ?>" alt="<?php echo $property_address; ?>" /></a>
				<?php if ( ! empty ( $property_photo_overlay ) ):?>
				
				<div class="image_overlay">
				<img src="<?php echo $property_photo_overlay; ?>" style="visibility:hidden;" OnLoad="Global.FixPng( this );" alt="" />
				</div>
				<?php endif;?>
				
				</div>
			</div>
			<?php endforeach; ?>
<div class="clearer"></div>