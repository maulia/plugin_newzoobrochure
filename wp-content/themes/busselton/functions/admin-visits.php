<?php
global $visits_tracker;
$visits = $visits_tracker->get_visits();
		
if(isset($_POST['submit_emails'])):
	update_option('emails_to_send_report', explode(",", $_POST['emails_to_send_report']));
	?><div id="message" class="updated fade"><p><strong>E-mails saved.</strong></p></div><?php
endif;
if($_GET['send_reports']):
	?><div id="message" class="updated fade"><p><strong><?php echo $visits_tracker->send_reports(); ?></strong></p></div><?php
endif;
?>
<div class="leftside">
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col" style="text-align: center">Property ID</th>
			<th scope="col">Suburb</th>
			<th scope="col">Address</th>
			<th scope="col" width="90" style="text-align: center">Week</th>
			<th scope="col" style="text-align: center">Month</th>
			<th scope="col" style="text-align: center">Total</th>
		</tr>
		</thead>
		<tbody id="the-list">
			<?php foreach ($visits as $visit): if($visit['grand_total']>0){ ?>
			<tr>
			<th scope="col" style="text-align: center"><a href="<?php echo $visit['url']; ?>" target="blank" title="<?php echo $visit['address']; ?>"><?php echo $visit['property_id']; ?></a></th>
			<th scope="col"><?php echo $visit['suburb']; ?></th>
			<th scope="col"><?php echo $visit['address']; ?></th>
			<th scope="col" width="90" style="text-align: center"><?php echo $visit['week_total']; ?> </th>
			<th scope="col" style="text-align: center"><?php echo $visit['month_total']; ?></th>
			<th scope="col" style="text-align: center"><?php echo $visit['grand_total']; ?></th>
			</tr>
			<?php } endforeach; ?>
		</tbody>
	</table>

	<form action="themes.php?page=theme-settings&tab=visits" method="post">
		<table class="widefat maintable" cellspacing="0">
			<thead>
			<tr>
				<th colspan="3" scope="col" class="manage-column column-title">Settings</th>
			</tr>
			</thead> 
			<tr>
				<th class="rowheader"><strong>Emails to notify</strong></th>
				<td class="field" colspan="2">
					<input type="text" name="emails_to_send_report" value="<?php echo implode(",", get_option('emails_to_send_report')); ?>" size="75">
				</td>
			</tr>
		</table>
		<p class="submit">
		<input name="submit_emails" type="submit" value="Save changes" />
		</p>
	</form>

</div>

<div id="poststuff">
	<div class="postbox">
	<h3>Actions</h3>

	<div class="inside">
	<p class="submit">
		<input name="submit_emails" type="submit" value="Save changes" />
	</p>
	<p>
		<a href="themes.php?page=theme-settings&tab=visits&send_reports=true" class="button">Send Report</a>
	</p>
	</div>

	<div class="clear"></div>
	</div>
</div>