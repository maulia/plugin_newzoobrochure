<?php 
//metadata: access_level=8
if(isset($_POST['submit'])):
	update_option('realty_office', $_POST['search_tool']);
	update_option('realty_limit_office', $_POST['realty_limit_office']);
	$return = 'Settings Updated.';
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
global $realty;
$settings = get_option('realty_office');
$offices = $realty->office_details('','','','','',false);
?><div class="wrap">
<form name="" method="post" action="" id="elements">
<h2>Office</h2>
<table class="widefat">
<thead>
<tr>
	<th scope="col">Office Id</th>
	<th scope="col">Name</th>
	<th scope="col">Address</th>
	<th scope="col">Email</th>
</tr>
</thead>
<?php $i=0;foreach($offices as $office): ?> 
	<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
	<td><?php echo $office['id']; ?></td>
	<td><?php echo $office['name']; ?></td>
	<td><?php echo $office['address']; ?></td>
	<td><a href="mailto:<?php echo $office['email']; ?>" title="Email office"><?php echo $office['email']; ?></a></td>
</tr> 
 <?php $i++;endforeach; ?>
 </table> 
 <br />
 <table class="widefat">
 <tr class="alternate">
	<td>Number of Office To Display In One Page</td>
	<td><input type="text" value="<?php echo get_option('realty_limit_office');?>" name="realty_limit_office"></td>
</tr>
 <tr>
	<td>Include Search Tool In The Office Page</td>
	<td><input type="checkbox" value="1" name="search_tool" <?php if(!empty($settings)) echo 'checked="checked" ';?>></td>
 </tr>  
  </table>
  <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->