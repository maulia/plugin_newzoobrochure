<?php 
$params=$_GET;
$office = $this->office($params);
if(empty($office))return print "<p>We are sorry, no office were found to match this criteria, please check back again soon or contact us with your requirements.<a href='".get_option('siteurl')."/offices/'> Back to Offices</a></p>";	
$queryString = "?" .  preg_replace('/(&?order=)([\w]+&?)/is', '', urldecode($_SERVER['QUERY_STRING']));	
?>

<div class="back_to">
	<p><a href="<?php echo get_option('siteurl');?>/offices/">Back to Offices</a></p>
</div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<div id="office" class="office_detail">
	<div class="office_box">
		<div class="office_logo">
			<img width="200px" height="150px" src="<?php echo ($office['logo1']=='')?get_bloginfo('stylesheet_directory').'/images/download.png':$office['logo1']?>" />
		</div>
		<div class="office_location">
        	<h5><?php echo $office['name']; ?></h5>
			<?php 
			if(!empty($office['unit_number']))$office['street_address'] = $office['unit_number'] .'/';
			$office['street_address'] .=  $office['street_number'] . ' ' .  $office['street']  ;
			$full=($office['street_address']==" " || $office['street_address']=="")?'':$office['street_address']."<br>";
			if(!empty($office['suburb']))$full.=$office['suburb'].", ";
			if(!empty($office['state']))$full.=$office['state'].", ";
			if(!empty($office['zipcode']))$full.=$office['zipcode'].", ";
			$full=substr($full,0,-2);
			if(!empty($full)) echo '<p class="office-address">'.$full.'</p>';
			if(!empty($office['phone'])):?><p class="office-phone">Phone: <?php echo $office['phone'] ; ?></p><?php endif; 
			if(!empty($office['fax'])):?><p class="office-fax">Fax: <?php echo $office['fax'] ; ?></p><?php endif; ?>
			<p class="office-contact">
				<?php if(!empty($office['email'])):?><a href="mailto:<?php echo $office['email'] ; ?>" title="Email Agent" class="btn">Email Office</a>
                <?php endif; ?>
                <?php if(!empty($office['url'])) :?><a href="<?php $website=(strpos($office['url'],'http://')===false)?'http://'.$office['url']:$office['url']; echo $website;?>" target="_newtab" class="btn">View Website</a><?php endif; ?>	
			</p>
        </div>
        <?php if(!empty($office['latitude']) and !empty($office['longitude'])): ?>
            <div class="office-map">                
                <div class="property_map">
                    <div id="map_canvas" style="width:400px;height:300px;">
                        <div id="map_loading">Loading...</div>
                    </div>
                </div>
                <script type="text/javascript">
                var map;
				jQuery(document).ready(function(){
					load_map ();
				});
				function load_map (){
					var myLatLng = new google.maps.LatLng(<?php echo $office['latitude']; ?>,<?php echo $office['longitude']; ?>);
					var myOptions = {
						zoom: 12,
						center:myLatLng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};	
					var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);	
					var Marker = new google.maps.Marker({  position: myLatLng, map: map });
				}
                </script>
            </div>
            <?php endif; ?>
            <div class="clearer"></div>
	</div>
</div>

<?php 
$listings=$office['listings']; 
if($_GET['list']=='sale' && empty($_GET['status']))$type='sale ';
if($_GET['list']=='lease')$type='rental ';
if($_GET['list']=='sale' && !empty($_GET['status']))$type='sold ';
//if($listings['results_count'] < 1)return print '<p class="no_properties_error">We are sorry, no '.$type.' property were found belong to this office.</p>';
if($listings['results_count'] < 1)return print '<p class="no_properties_error">Sorry, no properties were found for this office.</p>';

$page_now=$this->params['page']==''?'1':$this->params['page'];$pdf_param=urldecode($_SERVER['QUERY_STRING']);
if(empty($pdf_param))$pdf_param.='page='.$page_now; else $pdf_param.='&page='.$page_now;
?>

<div id="search_results_container">
	<div id="search_results">
		<div id="sorter_pagination">
			<p class="number_properties">
				There <?php echo ($listings['results_count'] == 1) ? 'is': 'are'; ?> <strong><?php echo $listings['results_count']; ?></strong>
				<?php echo "$types "; ?><?php echo ($listings['results_count'] == 1) ? 'property': 'properties'; ?> available
				<?php if (!empty($price_min)): ?> with prices from <strong>$<?php echo number_format( $price_min, 0, '.',','); ?></strong><?php endif; ?>
				<?php if (!empty($price_max)): ?> to <strong>$<?php echo number_format( $price_max, 0, '.',','); ?></strong><?php endif; ?>
				<?php if (!empty($keywords)) echo " in $keywords"; ?> with your search results.		
			</p>
			<div class="sorter_search_quick">
				<select name="" onchange="window.location=this.value">
					<option value="#">Sort by:</option>
					 <option value="<?php echo $queryString . "&amp;order=price";?>"<?php if($this->params['order'] =='price' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='price') echo ' selected="selected"';?>>Price</option>
					<option value="<?php echo $queryString. "&amp;order=suburb";?>"<?php if($this->params['order'] =='suburb' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='suburb') echo ' selected="selected"';?>>Suburb</option>
					<option value="<?php echo $queryString. "&amp;order=created_at";?>"<?php if($this->params['order'] =='created_at' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='created_at') echo ' selected="selected"';?>>Date Added</option>
				</select>		
				
				<?php if($this->settings['general_settings']['display_pdf_button_in_search_result']=='1'){ ?>
				<p class="pdf btn"><a href="javascript:void(0)" onclick="return window.open('<?php echo $this->pluginUrl."display/elements/search_brochure.php?$pdf_param"; ?>', 'Brochure', 'width=820, height=960,resizable=1,scrollbars=1'); return false;" title="Print Results" class="btn">PDF</a></p>
				<?php } ?>
				<div class="clear"></div>
			</div>	
			
			<?php if($view>1){ 
			wp_print_scripts('jquery-ui-core');wp_print_scripts('jquery-ui-tabs');			?>
		
			<?php 
			if($this->settings['general_settings']['display_map_in_search_result']=='1'):
				foreach($listings as $property):  
					if(!is_array($property)) continue; 
					if(!empty($longitude) && !empty($latitude)) break; 
					if(!empty($property['latitude']) && !empty($property['longitude']) && empty($longitude) && empty($latitude)){
					$latitude=$property['latitude'];
					$longitude=$property['longitude'];
					}
				endforeach;
				if(!empty($latitude) && !empty($longitude))$display_map='1';else $display_map='0';
				if($display_map=='1'){
			?>
			<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
			<?php } endif; ?>
			
			<input type="hidden" id="thumb_tab" value="0" >
			<input type="hidden" id="list_tab" value="1" >	
			<input type="hidden" id="map_tab" value="2" >

			<script type="text/javascript">
			jQuery(document).ready(function(){				
				<?php if($display_map=='1'){?>
				var map = null;
				jQuery("#search_results").tabs({selected:<?php echo $tab;?>});
				jQuery('#search_results').bind('tabsshow', function(event, ui) {
					if (ui.panel.id == 'map_search_result' && !map)
					{
						map = initializeMap();
						google.maps.event.trigger(map, 'resize');
					}
				});
				<?php } else { ?>
				jQuery("#search_results").tabs({selected:<?php echo $tab;?>});
				<?php } ?>
			});
			</script>
			
			<div id="search_results_tab">
				<ul class="shadetabs search_results_view_option">
					<li class="thumbnail_format" <?php if($this->settings['general_settings']['display_search_result_in_thumbnail_format']!='1') echo 'style="display:none !important;"';?> ><a href="#thumbnail_format" onClick="active_tab('thumb');" title="View Thumbnails Mode">Thumbnails</a></li>
					<li class="list_format" <?php if($this->settings['general_settings']['not_display_search_result_in_list_format']=='1') echo 'style="display:none !important;"';?>><a href="#list_format" onClick="active_tab('list');" title="View List Mode">List</a></li>
					<li class="map_search_result"  <?php if($display_map!='1') echo 'style="display:none !important;"';?>><a href="#map_search_result" id="map_tabs" onClick="active_tab('map');" title="View Map Mode">Map</a></li>
				</ul>
			</div>
			<?php } 
		
		echo $listings['pagination_above']; ?>

		</div> <!-- #sorter_pagination -->	
		<?php $this->element('listings', $listings); 

		if($display_map=='1'):?>
		<div id="map_search_result">
		<?php include(dirname(dirname(__FILE__)).'/elements/map_search_results.php'); ?>
		</div>
		<?php endif; 

		if(!empty($listings['pagination_below'])): ?>
		<div id='bottom_pagination'><?php echo $listings['pagination_below']; ?></div>
		<?php endif;?>
	</div>
</div>
<script>
<?php if($view>1){ ?>
function active_tab(tab){
	var index;
	if(tab=="thumb")index=document.getElementById('thumb_tab').value;
	else if(tab=="list")index=document.getElementById('list_tab').value;
	else if(tab=="map")index=document.getElementById('map_tab').value;
	var url="<?php echo $this->pluginUrl; ?>display/elements/active_tab.php?tab="+escape( index );
	jQuery.get(url);
}
<?php } ?>
</script>