<?php 
global $realty, $helper, $wpdb; 
$private_listing = get_option('private_listing');
$row=$wpdb->get_results("select * from payment_type", ARRAY_A);
$payment_type = array();
foreach($row as $item){
	$payment_type[$item['type']]=$item['price'];
}
?>
<table class="adv-table">
<tr>
	<td class="col1">
		<div class="block_content">
		<h2>Agent Account</h2>
		Click to register your agency. <br/>By using your Current Multi Portal Publisher or Portal Pusher.<br/>
		<p><a href="<?php echo $realty->siteUrl."agent-account/";?>" class="btn">REGISTER</a></p>
        <span style="margin-left:55px;"> OR </span>
        <p>
        <a href="http://www.realestatemarket.com.au/sign-up/">Sign Up</a> for your FREE Listings Account
        </p>
        <p>For Residential and Commercial Agents, Builders, and Business Brokers.</p>
		</div>
	</td>
	<?php if($private_listing==1){ ?>
	<td class="col2">
		<div class="block_content">
			<h2>Private Listing</h2>		
			- Advertise with us.<br/>
			- Residential , Commerical, Busines , Shared Accomodation<br/>
			- Edit your listings.<br/>
			- List for 30 days + Free Extension by demand.<br/>
			<br/>
			How it Works<br/>
			Step 1. Set up an Account.<br/>
			Step 2. Add your Listings.<br/>
			Step 3. Wait for Approval of FREE Listings.<br/>
            <span style="margin-left:55px;"> OR </span> <br/>
            Step 4. Pay for Instant Activation as Premium Listings.<br/>
			<p><a href="<?php echo $realty->siteUrl."sign-up/";?>" class="btn">Set up an Account</a></p>
			<br/><br/>
			<p><strong>Premium Listing Rates</strong> (Incl GST)</p>
			<table>
				<tr>
					<td width="150px">Residential Sale</td>
					<td><?php echo "  $". $payment_type['ResidentialSale']; ?></td>
				</tr>
				<tr>
					<td>Residential Lease</td>
					<td><?php echo "  $". $payment_type['ResidentialLease']; ?></td>
				</tr>
				<tr>
					<td>Commercial</td>
					<td><?php echo "  $". $payment_type['Commercial']; ?></td>
				</tr>
				<tr>
					<td>Business</td>
					<td><?php echo "  $". $payment_type['BusinessSale']; ?></td>
				</tr>
				<tr>
					<td>Shared</td>
					<td><?php echo "  $". $payment_type['Share']; ?></td>
				</tr>
			</table>
		</div> 			
	</td>
	<?php } ?>
</tr>
</table>