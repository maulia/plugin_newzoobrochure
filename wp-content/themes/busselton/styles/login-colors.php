<?php
/*
	Colors selected in the theme options page
*/

/* Login colors */

?>
h1 a {
	background: url(<?php bloginfo('template_url'); ?>/images/logos/<?php echo $logo; ?>) no-repeat center center !important;
}
a, a:link, a:visited, .login #nav a { color: #<?php echo $link_color; ?> !important; }
a:hover, a:active, a:focus, .login #nav a:hover { color: #<?php echo $link_hover_color; ?> !important; }

.login form { background-color: #<?php echo $bg_color; ?>; }
label { color: #<?php echo $page_title_color; ?>; }
input, select { color: #<?php echo $main_text_color; ?>; }
#login form .submit input { background-color: #<?php echo $link_color; ?>; border-color: #<?php echo $link_color; ?> !important; ?>; }
#login form .submit input:hover, #login form .submit input:active { background-color: #<?php echo $link_hover_color; ?>; border-color: #<?php echo $link_hover_color; ?> !important; }
#login .message { background-color: #<?php echo $accent_color; ?>; border-color: #<?php echo $border_color; ?>; }