<tr>
	<td align="center" colspan="2" style="background:<?php echo $sm_heading_color; ?>; border-top:solid 1px #fff; color:#<?php echo $sm_heading_font_color; ?>">
		<h4 style="margin:10px 0; padding:0; font-size:16px; "><?php echo $site_alerts[$alert]; ?></h4>
	</td>
</tr>
<?php 
foreach($properties as $property): if(!is_array($property)) continue;  ?>
<table width="100%" cellspacing="0" summary="Forthcoming Auctions" style="font-size:12px;">
	<tr>
		<th colspan="4" style="font-weight: bold; text-transform: uppercase; padding: 10px 0;text-align:left; border-bottom:dotted 1px #ddd; border-top:dotted 1px #ddd;"><?php echo date('l, jS F Y', strtotime($property['auction_date'])); ?></th>
	</tr>
	<tr style="<?php echo ($odd_class = ($odd_class=='background-color: #fff;') ? 'background: #ecf9fe;':'background-color: #fff;'); ?>">
		<td style="width: 150px;padding: 5px 0; vertical-align: top;"><?php echo date('g:ia', strtotime($property['auction_time'])); ?></td>
		<td style="width: 150px;padding: 5px 0; vertical-align: top;"><?php echo $property['suburb']; ?></td>
		<td style="width: 300px;padding: 5px 0; vertical-align: top;"><?php echo $property['street_address']; ?></td>	
		<td style="padding: 5px 0; vertical-align: top;"><a title="View Property" href="<?php echo $property['url']; ?>" style="color:#8e855e; text-decoration:none;">View Property <span style="font-family:Georgia, 'Times New Roman', Times, serif">&#187;</span></a></td>
	</tr>
<?php endforeach; // properties_date ?>
</table><!--date_container_-->