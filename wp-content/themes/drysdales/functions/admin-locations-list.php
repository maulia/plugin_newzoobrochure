<div class="leftside">	
	<table class="widefat">
		  <thead>
		  <tr>			
			<th scope="col"><?php _e('Suburb') ?></th>
			<th scope="col"><?php _e('Population') ?></th>
			<th scope="col"><?php _e('Postcode') ?></th>
			<th scope="col"><?php _e('Council') ?></th>

			<th scope="col" colspan="3" style="text-align: center"><?php _e('Action'); ?></th>
		  </tr>
		  </thead>
		  <tbody id="the-list">
		<?php if(!empty($current_locations)){ foreach($current_locations as $item): extract($item); ?>
			<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">
			<td><?php echo $suburb; ?></td>
			<td><?php echo $population; ?></td>
			<td><?php echo $postcode; ?></td>
			<td><?php echo $council; ?></td>
			<td><a class="edit" title="Edit <?php echo $suburb; ?>" href="themes.php?page=theme-settings&tab=locations&action=edit&suburb_id=<?php echo $suburb_id; ?>">Edit</a></td>
			<td><a onClick="return confirm('You are about to delete the \'<?php echo $suburb; ?>\' from the list of locations.\n\'OK\' to delete, \'Cancel\' to stop.' );" class="delete" href="themes.php?page=theme-settings&tab=locations&task=delete_form&suburb_id=<?php echo $suburb_id; ?>">Delete</a></td>
			</tr>
		<?php endforeach; } ?>
		  </tbody>
		</table>
</div>
		
<div id="poststuff">
	<div class="postbox">
		<h3>Actions</h3>		
		<div class="inside">
			<p>
			<a href="themes.php?page=theme-settings&tab=locations&action=add_new" class="button">Add New</a>
			</p>			
		</div>	
	</div>
</div>