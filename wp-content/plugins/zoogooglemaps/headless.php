<html>
<head>
<title><?=wp_title();?></title>
<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('stylesheet_url'); ?>" />
<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_url'); ?>/styles/type/<?php echo $fonts; ?>.css" />
<link rel="alternate" type="text/css" media="print" href="<?php bloginfo('stylesheet_directory'); ?>/styles/print.css" />
<?php
print_head_scripts();
?>
<style type="text/css">
	body { margin:0; }
	h2.map-address { background:#BE002D; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:normal; height:40px; line-height:40px; margin:0 0 5px; text-align:center; }
</style>
</head>
<body class="zoomap zoogooglemap">
<?php
$realty->property();
?>
<h2 class="map-address"><?=$realty->property['street_address']?></h2>
<div class="property_map">
	<div id="map_canvas" style="width:592px;height:444px;"></div>
</div>
<div class="clear"></div>
<br/>
<div class="buttons signup_button">
	<input type="button" name="cancel" value="Close" onClick="closebox('box_map','filter_map','load_form_map','boxtitle_map')">
</div>
<?php
$zoomap = zoomap::getInstance();
$zoomap->load_map('map_canvas', $realty->property['latitude'], $realty->property['longitude'], 12);
$zoomap->produce_js();
?>
</body>
</html>