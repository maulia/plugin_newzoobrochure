<?php get_header(); ?>

<div id="content">
    <div class="default-post">
    	<div class="post">

            <h1 class="page-title">Oops! File not found.</h1>
            
            <div class="entry">
    
                <p>There could be a few different reasons for this:</p>
                
                <ul>
                    <li>The page was moved.</li>
                    <li>The page no longer exists.</li>
                    <li>The URL is slightly incorrect.</li>
                </ul>
            
                <?php /*<p>Try one of the following to get you back on track:</p>
    
                <ul>
                    <?php wp_list_pages('title_li=&sort_column=menu_order&depth=1'); ?>
                </ul>
            
                <div id="refine_search">
                    <h5>Didn't find what you were looking for? Try a search!</h5>
                    <?php get_search_form(); ?>
                </div>*/ ?>
            
            </div>
        
    	</div><!-- .post -->
    </div><!-- .default-post -->
</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>