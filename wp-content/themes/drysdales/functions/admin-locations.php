<?php
global $helper, $LocationInfo;
$request = $_GET + $_POST;
extract($_POST);
if(!class_exists('form_handler_ex'))require_once('form_handler.class.php');

if(($return = $LocationInfo->direct_task($request)) != false){ ?>
	<div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div>
<?php 
}

if($request['action'] =='edit' && !empty($request['suburb_id'])):// If the form is being updated
	$form_title = 'Edit';
	$task = 'update_form';
	extract($LocationInfo->get_suburb("`suburb_id`= " . $request['suburb_id']));
	include('admin-locations-edit.php');
elseif($request['action'] =='add_new'): // If a new location is being added
	$form_title = 'Add';
	$task = 'insert_form';
	include('admin-locations-new.php');
else:			
	$current_locations = $helper->get_results("SELECT * FROM LocationInfo_suburbs $extra_condition ORDER BY `suburb` ASC");					
	include('admin-locations-list.php');
endif;
?>