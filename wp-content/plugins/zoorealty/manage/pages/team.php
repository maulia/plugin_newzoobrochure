<?php 
//metadata: access_level=1
$settings = (empty($_POST['member']))?$realty->settings['team']: $_POST['member'];//Represents the excluded from members.
global $wpdb;
if(isset($_POST['submit'])):
	//$realty->settings['team'] = $settings;
	//update_option('realty_team', $settings);
	
	if(!empty($_POST['team_ids']))			
		foreach($_POST['team_ids'] as $id)://Update teams order
			$display=($_POST['member'][$id]['display']=='')?'0':$_POST['member'][$id]['display'];
			$display_on_team_page=($_POST['member'][$id]['display_on_team_page']=='')?'0':$_POST['member'][$id]['display_on_team_page'];
			/* New feature to update email via wp-admin dashboard */
			if ( is_super_admin() ) {
				$email = $_POST['member'][$id]['email'];
				$office = $wpdb->get_var(
							$wpdb->prepare("
								SELECT office_id 
								FROM users
								WHERE id = %d",
								$id
							)
						);

				// just to avoid empty email input when site already live
				if ( $offce != 26 ) {
					if ( empty($email) ) {
						$error = array_push($error, $id);
						continue;
					}
				}

				$wpdb->update(
					'users',
					array(
						'display' => $display,
						'display_on_team_page' => $display_on_team_page,
						'email' => $email
					),
					array(
						'id' => $id
					),
					array(
						'%d',
						'%d',
						'%s'
					),
					array(
						'%d'
					)
				);
			} else {
				$wpdb->query("update users set display=".intval($display).", display_on_team_page=".intval($display_on_team_page)." where id=".intval($id));
			}
		endforeach;
	$return = 'Settings Updated.';
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
if($_GET['task']=='delete_user' && !empty($_GET['id'])):
	$user_id=$_GET['id'];
	$wpdb->query("delete from attachments where parent_id=".intval($user_id)." and is_user=1");
	$wpdb->query("delete from users where id=".intval($user_id));
	$return= "User was successfull deleted";
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
$authors = $wpdb->get_results("SELECT ID, user_nicename, display_name FROM $wpdb->users ORDER BY display_name", ARRAY_A);
$users = $realty->users('','','','',true,$_GET['pg']);

?>
<div class="wrap">
<form name="" method="post" action="" id="elements">
<h2>Team Members</h2>
<table class="widefat">
<thead>
<tr>	
	<th scope="col">Agent ID</th>
	<th scope="col">Agent Name</th>
	<th scope="col">Office</th>
	<th scope="col">Display News Articles in Profile</th>
	<th scope="col">Display</th>
	<th scope="col">Display on team page</th>
	<?php /* New feature to update email via wp-admin dashboard */
	if (is_super_admin() ) { ?>
		<th scope="col">Email</th>
		<?php 
	} ?>
	<th scope="col">Action</th>
</tr>
</thead>
	<?php $i=0;foreach($users as $user): if(!is_array($user))continue;$office=$helper->get_var("select name from office where id=".$user['office_id']); ?> 
	<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 	
	<td><label><?php echo $user['user_id']; ?></label></td>
	<td><label><?php echo $user['name']; ?></label></td>
	<td><label><?php echo $office; ?></label></td>
	<td>	
	<select name="member[<?php echo $user['user_id']; ?>][wpUser]">
		<option value="">Select Author Name</option>
		<?php foreach($authors as $author): ?>
		<option value="<?php echo $author['ID']; ?>"<?php if($settings[$user['user_id']]['wpUser']==$author['ID']) echo ' selected="selected"'; ?>><?php echo $author['display_name']; ?></option>
		<?php endforeach; ?>
	</select>
	</td>
	<td><input type="hidden" name="team_ids[]" value="<?php echo $user['user_id']; ?>" />
	<input type="checkbox" <?php if ($user['display']!='0') echo 'checked="checked" '; ?>name="member[<?php echo $user['user_id']; ?>][display]" value="1"></td> 
	<td><input type="checkbox" <?php if ($user['display_on_team_page']!='0') echo 'checked="checked" '; ?>name="member[<?php echo $user['user_id']; ?>][display_on_team_page]" value="1"></td> 
	<?php /* New feature to update email via wp-admin dashboard */
	if (is_super_admin() ) { ?>
		<td><input type="text" value="<?php echo $user['email']; ?>" name="member[<?php echo $user['user_id']; ?>][email]" <?php if ( strpos($user['email'],'agentpoint') !== FALSE || strpos($user['email'],'test') !== FALSE ) echo 'style="border:1px solid red"'; ?>></td><?php
	} ?>
	<td><a class="delete" onClick="return confirm('You are about to delete the this user.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="admin.php?page=Realty/manage/pages/team.php&amp;task=delete_user&amp;id=<?php echo  $user['user_id']; ?>">Delete</a></td>
	  
</tr> 
 <?php $i++;endforeach; ?>
 </table> 
 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->