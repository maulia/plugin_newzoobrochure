<?php
/*
Plugin Name: Zoo Remittance new
Description:  The Zoo Remittance plugin remmit all users data ( filled in subscription manager forms ) , and count the visiters of property page.
Version: 1.3
Author: Agentpoint
Author URI: http://www.agentpoint.com.au

Release History:
1.1 use $wpdb function to query
*/

if(!class_exists('form_handler_ex'))
{
	require_once(dirname(__FILE__).'/includes/form_handler.class.php');
}

if (!class_exists('ZooRemittance')) 
{
	class ZooRemittance extends form_handler_ex
	{			
		function ZooRemittance(){
			$this->plugin_name='ZooRemittance';
			$this->site_url = get_option('siteurl');
			if ( '/' != $this->site_url[strlen($this->site_url)-1] )$this->site_url .= '/';
			$this->plugin_url  = $this->site_url . PLUGINDIR . '/' . $this->plugin_name. '/';
			add_action('admin_menu', array($this, 'admin_menu'));	
			add_action('wp_head', array($this, 'head'));				
			global $table_prefix;
			$this->plugin_table = $table_prefix . "remmit_zoo";
			$this->plugin_table_detail = $table_prefix . "remmit_zoo_detail";
			$this->form_action = "?page=" . $_GET['page'];
			register_activation_hook(__FILE__, array($this, 'install'));
		}	
		
		function head(){
			global $realty, $wpdb;
			$send_count_viewer=get_option("zm_count_viewer");
			if(is_page('property') && !empty($realty->property) && $send_count_viewer=='1'){				
				$property=$realty->property;				
				$ip_address=esc_sql ($_SERVER['REMOTE_ADDR']);
				$keys=$wpdb->get_results("select access_key,private_key from property_xml where office_id=".intval($property['office_id']), ARRAY_A);
				$accesskey=$keys[0]['access_key'];
				$privatekey=$keys[0]['private_key'];
				
				if(!empty($accesskey) && !empty($privatekey)){
					/*
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $zoo_api);
					curl_setopt($ch, CURLOPT_USERPWD, $accesskey . ':'.$privatekey);
					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
					curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
					curl_setopt($ch, CURLOPT_COOKIESESSION, true);
					curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
					$data = curl_exec($ch);
					curl_close($ch);
					*/
					?>
					<script type="text/javascript">
						count_viewer();
							function count_viewer(){
							var url    = "<?php echo get_option('siteurl')?>/wp-content/plugins/ZooRemittanceMobile/js/send_count_viewer2.php?ip_address=<?php echo $_SERVER['REMOTE_ADDR'];?>&property_id=<?php echo $realty->property['id'];?>&agent_id=<?php echo $realty->property['agent_id'];?>&office_id=<?php echo $realty->property['office_id'];?>&accesskey=<?php echo $accesskey;?>&privatekey=<?php echo $privatekey;?>";
							jQuery.get(url);
						}
					</script>
					<?php 
				}			
			}
		}
		
		function install(){
			global $wpdb;
			$q12  = "CREATE TABLE IF NOT EXISTS `property_xml` (
			`id` mediumint(9) NOT NULL auto_increment,
			`office_id` mediumint(9) NOT NULL,			
			`access_key`  varchar(64),
			`private_key`  varchar(64),				
			PRIMARY KEY  (`id`)
			) ;";			
			$wpdb->query($q12);
			$wpdb->query("CREATE TABLE IF NOT EXISTS `$this->plugin_table` (
							  `subscriber_id` bigint(20) NOT NULL auto_increment,
							  `title` varchar(10),							  
							  `first_name` varchar(30),
							  `last_name` varchar(30),
							  `home_number` varchar(20),
							  `mobile_number` varchar(20),							  
							  `work_number` varchar(20),
							  `fax_number` varchar(20),
							  `email` varchar(50),
							  `country` varchar(50),			
							  `heard_about_us` varchar(50),	  	
								PRIMARY KEY  (`subscriber_id`)
							) ENGINE=InnoDB  DEFAULT CHARSET=latin1");	
			$wpdb->query("CREATE TABLE IF NOT EXISTS `$this->plugin_table_detail` (
							  `id` bigint(20) NOT NULL auto_increment,
							  `subscriber_id` bigint(20) NOT NULL,
							  `note_type` int(11) NOT NULL,								  
							  `note` text,
							  `property_id` int(11),
							  `address` varchar(100),
							  `alert` tinyint(1) default '0',
							  `alert_type` varchar(30),
							  `listing_type` varchar(20),
							  `property_type` varchar(30),
							  `min_bedroom`  int(11),
							  `min_bathroom`  int(11),
							  `min_carspace`  int(11),
							  `min_price`  int(11),
							  `max_price`  int(11),
							  `suburbs`  text,
							  `send_api`  text,
							  `data_added`  date,
								PRIMARY KEY  (`id`),
								KEY `subscriber_id` (`subscriber_id`)
							) ENGINE=InnoDB  DEFAULT CHARSET=latin1");
		}
		
		function admin_menu(){
			add_menu_page('Zoo Remmitance', 'Zoo Remmitance', 8, __FILE__, array($this,"setting")); 		
			add_submenu_page(__FILE__, htmlspecialchars(__('Zoo Remmitance: Contacts','Contacts')), htmlspecialchars(__('Contacts','Contacts')), 8, __FILE__, array($this,"setting"));
			add_submenu_page(__FILE__, htmlspecialchars(__('Zoo Remmitance: Offices','Offices')), htmlspecialchars(__('Offices','Offices')), 8, 'Offices', array($this,"office_setting"));
		}	
		
		function process_management_page(){
			global $helper, $wpdb;	
			switch($_REQUEST['task']):
				case 'edit':	
					$return = $this->update_profile($_REQUEST);			
				break;
				case 'delete_contact':	
					$wpdb->query("delete from `$this->plugin_table` where subscriber_id=".intval($_REQUEST['subscriber_id']));
					$wpdb->query("delete from `$this->plugin_table_detail` where subscriber_id=".intval($_REQUEST['subscriber_id']));
					$return = "Contact was succesfully deleted.";			
				break;	
				case 'delete_detail':	
					$wpdb->query("delete from `$this->plugin_table_detail` where id=".intval($_REQUEST['id']));
					$return = "Contact detail was succesfully deleted.";			
				break;	
				case 'new_property_site' :
					$return .= $this->new_property_site( $_REQUEST );
				break;
				case 'delete_property_site' :
					$return .= $this->delete_property_site($_REQUEST['id']);
				break;
				case 'edit_property_site' :
					$return .= $this->edit_property_site( $_REQUEST );
				break;
				default:
				break;
			endswitch;
			return $return;
		}
		
		function new_property_site( $request ){ 
			global $wpdb;
			if($wpdb->query("insert into property_xml values('','".intval($request['office_id'])."','".esc_sql($request['access_key'])."','".esc_sql($request['private_key'])."')")):
				return 'Office successfully created.';
			else:
				return 'Office could not be created. please try again later. ';
			endif;	
		}
		
		function edit_property_site( $request ){ 
			global $wpdb;
			if($wpdb->query("update property_xml set office_id='".intval($request['office_id'])."',access_key='".esc_sql($request['access_key'])."',private_key='".esc_sql($request['private_key'])."' where id=".intval($request['id']))):
				return 'Office successfully updated.';
			else:
				return 'Office site could not be updated. please try again later. ';
			endif;	
		}
		
		function delete_property_site( $id ){ 
			global $wpdb;
			if( $wpdb->query("DELETE FROM property_xml WHERE id = '".intval($id)."'") ):
				return "Office delete successfully";
			else:
				return "Cannot delete Office. Please try again later.";
			endif;				
		}		

		function setting(){
			global $helper, $realty , $wpdb, $subscriptions_manager;
			if(!empty($realty->settings['general_settings']['office_id']))$office_id = implode(', ', $realty->settings['general_settings']['office_id']);			
			$URL=dirname(__FILE__)."/send_visiters.php?office_id=$office_id";
			
			if(($return = $this->process_management_page()) != false):
			?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
			endif;	
			$_GET['subscriber_id']	= intval($_GET['subscriber_id']);
			switch($_GET['action']):		
				case 'edit':
					$all_list=$subscriptions_manager->all_list_referrers();
					$list_countries=$subscriptions_manager->list_countries();
					$sql = "SELECT * FROM `$this->plugin_table` where subscriber_id=".$_GET['subscriber_id'];
					$subscribers=$wpdb->get_results($sql, ARRAY_A);
					$subscribers=$subscribers[0];					

					require_once( 'admin/edit.php');
				break;
				case 'alerts':
					$sql = "SELECT * FROM `$this->plugin_table` where subscriber_id=".$_GET['subscriber_id'];
					$subscribers=$wpdb->get_results($sql, ARRAY_A);
					$subscribers=$subscribers[0];
					
					$sql = "SELECT * FROM `$this->plugin_table_detail` where alert_type!='' and subscriber_id=".$_GET['subscriber_id'];
					$details=$wpdb->get_results($sql, ARRAY_A);
					require_once( 'admin/view_detail.php');
				break;
				case 'notes':
					$sql = "SELECT * FROM `$this->plugin_table` where subscriber_id=".$_GET['subscriber_id'];
					$subscribers=$wpdb->get_results($sql, ARRAY_A);
					$subscribers=$subscribers[0];
					
					$sql = "SELECT * FROM `$this->plugin_table_detail` where note_type!='4' and subscriber_id=".$_GET['subscriber_id'];
					$details=$wpdb->get_results($sql, ARRAY_A);
					require_once( 'admin/view_note.php');
				break;
				default:
					if(!empty($_REQUEST['keywords']))$condition = "(first_name like '%".$_REQUEST['keywords']."%' or last_name like '%".$_REQUEST['keywords']."%' or email like '%".$_REQUEST['keywords']."%')";
					$subscribers = $this->get_subscribers($condition,$_GET['pg'], 50);
					require_once( 'admin/settings.php');
				break;
			endswitch;			
		}		
		
		function office_setting(){
			global $helper, $realty, $wpdb, $subscriptions_manager;
			$property_xml=$wpdb->get_results("select * from property_xml order by office_id", ARRAY_A);
			
			if(($return = $this->process_management_page()) != false):
			?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
			endif;						
			switch( $_REQUEST["action"] ){
				case 'new_property_site' :
					include(dirname(__FILE__).'/admin/add_property_site.php');
					break;				
				case 'edit_property_site' :
					include(dirname(__FILE__).'/admin/edit_property_site.php');
					break;		
				default : 
					require_once( 'admin/office_settings.php');
					break;
			}	
		
		}		
		
		function send_api($zoo_api,$paramaters,$accesskey,$privatekey,$params=array(), $save_flag=true){
			if($save_flag)$this->save_detail($params, $paramaters);
			
			if(strpos($paramaters,'heard_from_id')===false)$paramaters.="&heard_from_id=7";	
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $zoo_api); 
			curl_setopt ($ch, CURLOPT_POST, 1);
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $paramaters);

			curl_setopt($ch, CURLOPT_USERPWD, $accesskey . ':'.$privatekey); 
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			curl_setopt($ch, CURLOPT_COOKIESESSION, true);
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			$data = curl_exec($ch);
			//var_dump($data);	die();
			curl_close($ch); 
			
		}
		
		function save_detail($params=array(), $paramaters){
			global $wpdb;
			$timezone_format = _x('Y-m-d', 'timezone date format');
			$date=date_i18n($timezone_format);

			$check=$wpdb->get_results("select subscriber_id from $this->plugin_table where email='".esc_sql($params['email'])."'", ARRAY_A);
			if(!$check){
				$wpdb->query("insert into $this->plugin_table values('','".esc_sql($params['title'])."','".esc_sql($params['first_name'])."','".esc_sql($params['last_name'])."','".esc_sql($params['home_number'])."','".esc_sql($params['mobile_number'])."','".esc_sql($params['work_number'])."','".esc_sql($params['fax_number'])."','".esc_sql($params['email'])."','".esc_sql($params['country'])."','".esc_sql($params['heard_about_us'])."')");
			}
			else{
				$set="first_name='".esc_sql($params['first_name'])."',last_name='".esc_sql($params['last_name'])."'";
				if(!empty($params['title']))$set.=",title='".esc_sql($params['title'])."'";
				if(!empty($params['home_number']))$set.=",home_number='".esc_sql($params['home_number'])."'";
				if(!empty($params['mobile_number']))$set.=",mobile_number='".esc_sql($params['mobile_number'])."'";
				if(!empty($params['work_number']))$set.=",work_number='".esc_sql($params['work_number'])."'";
				if(!empty($params['fax_number']))$set.=",fax_number='".esc_sql($params['fax_number'])."'";
				if(!empty($params['fax']))$set.=",fax='".esc_sql($params['fax'])."'";
				if(!empty($params['country']))$set.=",country='".esc_sql($params['country'])."'";
				if(!empty($params['heard_about_us']))$set.=",heard_about_us='".esc_sql($params['heard_about_us'])."'";
				$wpdb->query("update $this->plugin_table set $set where subscriber_id='".intval($check[0]['subscriber_id'])."'");
			}
			
			//save detail
			$subscribers=$wpdb->get_results("select subscriber_id from $this->plugin_table where email='".esc_sql($params['email'])."'", ARRAY_A);
			$params['subscriber_id']=$subscribers[0]['subscriber_id'];
			$wpdb->query("insert into $this->plugin_table_detail values('','".intval($params['subscriber_id'])."','".esc_sql($params['note_type'])."','".esc_sql($params['note'])."','".intval($params['property_id'])."','".esc_sql($params['address'])."','".esc_sql($params['alert'])."','".esc_sql($params['alert_type'])."','".esc_sql($params['listing_type'])."','".esc_sql($params['property_type'])."','".intval($params['min_bedroom'])."','".intval($params['min_bathroom'])."','".intval($params['min_carspace'])."','".intval($params['min_price'])."','".intval($params['max_price'])."','".esc_sql($params['suburbs'])."','".esc_sql($paramaters)."','".$date."')");
		}

		function get_subscribers($condition ='', $limit_start=0,$limit=''){
			global $wpdb;
			if(!empty($condition))$condition = "WHERE $condition"; 
	
			$sql = "SELECT * FROM `$this->plugin_table` $condition order by first_name, last_name";
			if(!empty($limit))extract($this->paginate($limit, $limit_start, 0, $sql)); //Will reset sql		

			$subscribers =$wpdb->get_results($sql, ARRAY_A);
			if(!empty($limit))$subscribers['pagination_label'] = $pagination_label;	
			return $subscribers;
		}
		
		function paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
			if($page < 1) //Avoid issuing a negative limit to the SQL statement
				$page = 1;
				//print $sql;
			//Classic pagination		
			if(!empty($sql) && $results_count==0) //Count the results based on the SQL statement provided
				if(($query =mysql_query($sql)) != false)
					$results_count = mysql_num_rows($query);
			if ($limit < $results_count ):				
				$limit_start = $page * $limit - ($limit);
				if($limit_start > $results_count)
					$limit_start = $results_count - $limit; //Prevent the script from inadvertently going to an out of range result
				$sql_limit = " LIMIT $limit_start, $limit";			
			endif;
			$return['sql'] = $sql . $sql_limit;
			//ends classic pagination		
			
			$max_page_links = 10; //Set how many links to show in each page
			
			
			$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
			if($page_link <1)
				$page_link = 1;
			$total_pages = ceil($results_count / $limit);
			
			$current_query = "?" . preg_replace('/&(pg=)([-+]?\\d+)/is', '', $_SERVER['QUERY_STRING']) . "&amp;pg=";
			
			$prev_page = $page - 1;
			$next_page = $page + 1;
			ob_start();
			require('admin/pagination_label.php');//Front page pagination
			$return['pagination_label'] = ob_get_clean();
			return $return;
		}
		
		function contact($request){
			global $wpdb;
			$params=array();
			$zr_office_id=get_option('zr_office_id');
			if(!empty($zr_office_id)){ // remmit back to certain office_id
				$office_id_all=explode(",",$zr_office_id);
			}
			else{
				$settings=get_option('realty_general_settings');
				$office_id_all=$settings['office_id'];
			}

			foreach($office_id_all as $office_id){
				$agents=$wpdb->get_results("select distinct agent_id from office where agent_id!='' and agent_id is not null and id=".intval($office_id), ARRAY_A);
				$zoo_api="http://agentpoint.agentaccount.com/agents/".$agents[0]['agent_id']."/offices/".$office_id."/contact_listeners?";
				foreach( $request AS $key=>$val )
				{		
					if($val=='' || $val=='undefined')continue;
					switch($key){			
						case 'keep_informed': case 'property_alert':case 'news_alert':case 'alerts':case 'property_type':case 'min_bedroom':case 'min_bathroom':case 'min_carspace':case 'min_price':case 'max_price':case 'suburbs':case 'task':case 'post_title':case 'alert':case 'query_string':case 'blog_id':case 'blog_name':case 'office_id':break;
						case 'home_phone':$paramaters.="home_number=".$val."&";$params['home_number']=$val; break;
						case 'mobile_phone':$paramaters.="mobile_number=".$val."&";$params['mobile_number']=$val; break;
						case 'work_phone':$paramaters.="work_number=".$val."&";$params['work_number']=$val; break;
						case 'fax':$paramaters.="fax_number=".$val."&";$params['fax_number']=$val; break;
						case 'referrer':
							$val=trim(strtolower($val));
							switch($val){
								case '': case 'undefined':$heard_about_us='';break;
								case 'newspaper': $heard_about_us='1';break;
								case 'referral': $heard_about_us='2';break;
								case 'web': $heard_about_us='3';break;
								case 'past client': $heard_about_us='4';break;
								case 'sign board': $heard_about_us='5';break;
								default: $heard_about_us='6';break;		
							}
							$paramaters.="heard_about_us=".$heard_about_us."&"; $params['heard_about_us']=$val;
						break;
						case 'comments':$paramaters.="note=".str_replace(";","\n",$val)."&";$params['note']=str_replace(";","\n",$val); break;
						default: $paramaters.=$key."=".$val."&";$params[$key]=$val; break;
					}
				}
				
				$keys=$wpdb->get_results("select access_key,private_key from property_xml where office_id=".intval($office_id), ARRAY_A);
				$accesskey=$keys[0]['access_key'];
				$privatekey=$keys[0]['private_key'];

				if(!empty($request['alert']) &&!empty($accesskey) && !empty($privatekey)){
					$news_flag=0;
					foreach($request['alert'] as $alert){
						$extra_parameters=$paramaters;
						$extra_params=$params;
						$alert=trim($alert);
						switch($alert){
							case 'sale': 	
								$extra_parameters.='alert=1&alert_type=Newly Listed Properties&listing_type=ResidentialSale';
								
								$extra_params['alert']='1';
								$extra_params['alert_type']='Newly Listed Properties';
								$extra_params['listing_type']='ResidentialSale';
								
								$query_string=$request['query_string']['sale'];
								if(!empty($query_string['type_group'])){ $extra_parameters.='&property_type='.$query_string['type_group']; $extra_params['property_type']= $query_string['type_group']; }
								if(!empty($query_string['bedrooms'])){ $extra_parameters.='&min_bedroom='.$query_string['bedrooms'];$extra_params['min_bedroom']= $query_string['bedrooms']; } 
								if(!empty($query_string['bathrooms'])){ $extra_parameters.='&min_bathroom='.$query_string['bathrooms'];$extra_params['min_bathroom']= $query_string['bathrooms']; }
								if(!empty($query_string['carspaces'])){ $extra_parameters.='&min_carspace='.$query_string['carspaces']; $extra_params['min_carspace']= $query_string['carspaces'];}
								if(!empty($query_string['price_min'])){ $extra_parameters.='&min_price='.$query_string['price_min'];$extra_params['min_price']= $query_string['price_min']; }
								if(!empty($query_string['price_max'])){ $extra_parameters.='&max_price='.$query_string['price_max'];$extra_params['max_price']= $query_string['price_max']; }
								if(!empty($query_string['suburb'])){ $extra_parameters.='&suburbs='.implode(",",$query_string['suburb']);$extra_params['suburbs']= implode(",",$query_string['suburb']); }	
								
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'lease': 
								$extra_parameters.='alert=1&alert_type=Newly Listed Properties&listing_type=ResidentialLease';
								
								$extra_params['alert']='1';
								$extra_params['alert_type']='Newly Listed Properties';
								$extra_params['listing_type']='ResidentialLease';
								
								$query_string=$request['query_string']['lease'];
								if(!empty($query_string['type_group'])){ $extra_parameters.='&property_type='.$query_string['type_group']; $extra_params['property_type']= $query_string['type_group']; }
								if(!empty($query_string['bedrooms'])){ $extra_parameters.='&min_bedroom='.$query_string['bedrooms'];$extra_params['min_bedroom']= $query_string['bedrooms']; } 
								if(!empty($query_string['bathrooms'])){ $extra_parameters.='&min_bathroom='.$query_string['bathrooms'];$extra_params['min_bathroom']= $query_string['bathrooms']; }
								if(!empty($query_string['carspaces'])){ $extra_parameters.='&min_carspace='.$query_string['carspaces']; $extra_params['min_carspace']= $query_string['carspaces'];}
								if(!empty($query_string['price_min'])){ $extra_parameters.='&min_price='.$query_string['price_min'];$extra_params['min_price']= $query_string['price_min']; }
								if(!empty($query_string['price_max'])){ $extra_parameters.='&max_price='.$query_string['price_max'];$extra_params['max_price']= $query_string['price_max']; }
								if(!empty($query_string['suburb'])){ $extra_parameters.='&suburbs='.implode(",",$query_string['suburb']);$extra_params['suburbs']= implode(",",$query_string['suburb']); }	
								
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'latest_sale_update': 
								$extra_parameters.='alert=1&alert_type=Updated Properties&listing_type=ResidentialSale';					
								$extra_params['alert']='1';
								$extra_params['alert_type']='Updated Properties';
								$extra_params['listing_type']='ResidentialSale';					
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'latest_lease_update':
								$extra_parameters.='alert=1&alert_type=Updated Properties&listing_type=ResidentialLease';
								$extra_params['alert']='1';
								$extra_params['alert_type']='Updated Properties';
								$extra_params['listing_type']='ResidentialLease';
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'opentimes_sales': 
								$extra_parameters.='alert=1&alert_type=Open Inspections&listing_type=ResidentialSale';
								$extra_params['alert']='1';
								$extra_params['alert_type']='Open Inspections';
								$extra_params['listing_type']='ResidentialSale';
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'opentimes_lease':
								$extra_parameters.='alert=1&alert_type=Open Inspections&listing_type=ResidentialLease';
								$extra_params['alert']='1';
								$extra_params['alert_type']='Open Inspections';
								$extra_params['listing_type']='ResidentialLease';
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'listing_sold': 
								$extra_parameters.='alert=1&alert_type=Sold/Leased Properties&listing_type=ResidentialSale';
								$extra_params['alert']='1';
								$extra_params['alert_type']='Sold/Leased Properties';
								$extra_params['listing_type']='ResidentialSale';
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'listing_lease':
								$extra_parameters.='alert=1&alert_type=Sold/Leased Properties&listing_type=ResidentialLease';
								$extra_params['alert']='1';
								$extra_params['alert_type']='Sold/Leased Properties';
								$extra_params['listing_type']='ResidentialLease';
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							case 'latest_auctions':
								$extra_parameters.='alert=1&alert_type=Auctions';
								$extra_params['alert']='1';
								$extra_params['alert_type']='Auctions';
								$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
							break;
							default:
								if($news_flag=='0'){ 
									$extra_parameters.='alert=1&alert_type=Latest News';
									$news_flag=1; 	
									$extra_params['alert']='1';
									$extra_params['alert_type']='Latest News';
									$this->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params); 
								}
							break;
						}							
					}		
				}
				if(empty($request['alert']) &&!empty($accesskey) && !empty($privatekey)){
					$paramaters.=($request['keep_informed']=='1')?'alert=1&alert_type=Latest News':'';	
						/*
						echo $zoo_api."<br>";
						echo $paramaters."<br>";
						echo $accesskey."<br>";
						echo $privatekey."<br>";
						die();
						*/
					if(!empty($accesskey) && !empty($privatekey))$this->send_api($zoo_api,$paramaters,$accesskey,$privatekey,$params);
				}
			}
		
		}
	
		function remit_subscribers($pg=1){
			global $subscriptions_manager;
			$subscribers = $subscriptions_manager->get_subscribers('',$pg,50);
			if($subscribers){
				foreach($subscribers as $subscriber){
					if(!is_array($subscriber))continue;
					$subscriber['note_type']=4;
					$subscriber['alert']=explode(",",$subscriber['alert']);
					$this->contact($subscriber);			
				}
				echo $pg;
			}
		}
	} 
}

if (class_exists('ZooRemittance')) 
{
	$ZooRemittance = new ZooRemittance();
}
?>