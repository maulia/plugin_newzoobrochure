function calculateACT(amt, live_in, firsthome, new_home, land)
{    
    
    var duty;
    var notes;
    var mortReg = 130;
    var trans = 252;
    var loanduty = 0;
    if (new_home)
    {
        if (amt <= 442500) 
        {
            duty = 20;
        }
        else 
        {
            if (amt <= 560000) 
			{
				duty = (amt - 442500) * 0.1495;
				if (duty < 20) 
				{
					duty = 20;
				}
			}
			else 
			{
				duty=0;
			}
        }
    }
	/*
	else if(land){
		if (amt <= 264700) 
        {
            duty = 20;
        }
        else 
        {
            if (amt <= 303800) 
			{
				duty = (amt - 264700) * 0.1725;
				if (duty < 20) 
				{
					duty = 20;
				}
			}
			else 
			{
				duty=0;
			}
        }	
	}
	*/
    else 
    {
        if (amt <= 200000) 
        {
            duty = Math.max(Math.ceil(amt / 100) * 1.8, 20);
        }
        else 
        {

			if (amt <= 300000) 
			{
				duty = Math.ceil((amt - 200000) / 100) * 3 + 3600;
			}
			else 
			{
				if (amt <= 500000) 
				{
					duty = Math.ceil((amt - 300000) / 100) * 4 + 6600;
				}
				else 
				{
					if (amt <= 750000) 
					{
						duty = Math.ceil((amt - 500000) / 100) * 5 + 14600;
					}
					else
					{
						if (amt <= 1000000) 
						{
							duty = Math.ceil((amt - 750000) / 100) * 6.5 + 27100;
						}
						else 
						{
							if (amt <= 1455000) 
							{
								duty = Math.ceil((amt - 1000000) / 100) * 7 + 43350;
							}
							else 
							{
								duty = Math.ceil(amt / 100) * 5.17;
							}
						}
					}
				}
			}
            
        }
    }
    loanduty = 0;
    notes = "<strong>First Home Owner Grant:</strong><br/>After 1 September 2013, $12,500 grant to first home buyers to purchase their first new home. Established home are now no longer eligible for the First Home Owners Grant";
    display(duty, loanduty, mortReg, trans, notes);
}

function calculateNSW(amt, live_in, firsthome, new_home, land)
{
    
    var __reg3 = amt;
    var duty = undefined;
    var loanduty;
    var mortReg;
    var trans;
    var notes;
    var loandutyPercentage = 1;
    mortReg = 109.5;
    trans = 219;
    if (new_home || land) 
    {
        if (amt >= 550000 && amt <=650000) 
        {
            duty = amt * 0.2474 - 136070;
        }
        else if (amt >= 350000 && amt <=450000) 
        {
            duty = amt * 0.1574 - 55090;
        }
    }
    else 
    {
        amt = Math.ceil(amt / 100) * 100;
        if (amt <= 14000) 
        {
            duty = amt / 100 * 1.25;
        }
        else 
        {
            if (amt <= 30000) 
            {
                duty = (amt - 14000) / 100 * 1.5 + 175;
            }
            else 
            {
                if (amt <= 80000) 
                {
                    duty = (amt - 30000) / 100 * 1.75 + 415;
                }
                else 
                {
                    if (amt <= 300000) 
                    {
                        duty = (amt - 80000) / 100 * 3.5 + 1290;
                    }
                    else 
                    {
                        if (amt <= 1000000) 
                        {
                            duty = (amt - 300000) / 100 * 4.5 + 8990;
                        }
                        else 
                        {
                            if (amt < 3000000) 
                            {
                                duty = (amt - 1000000) / 100 * 5.5 + 40490;
                            }
                            else 
                            {
                                duty = (amt - 3000000) / 100 * 7 + 150490;
                            }
                        }
                    }
                }
            }
        }
    }

    notes = "<strong>First Home Owner Grant:</strong><br/>The grant was established to assist eligible first home owners to purchase a new home or build their home by offering a $15,000 grant. The Scheme applies to new homes only and will reduce to $10,000 on 1 January 2016. The value of the property must not exceed the First Home Owner Grant Cap of $750,000 for contracts dated on or after 1 July 2014.<br/><br/><strong>New Home Grant scheme</strong><br/>Introduced on 1 July 2012 to stimulate the construction of new homes.The scheme provides a grant of $5,000 towards the purchase of new homes, homes off the plan and vacant land on which a new home will be built. The value of the new home must not exceed $650,000 and the value of vacant land must not exceed $450,000. ";
    display(duty, loanduty, mortReg, trans, notes);
}

function calculateNT(amt, live_in, firsthome, new_home, land)
{
    var trans = 142;
	var mortReg = 142;
    var duty = 0;
    var notes;    
    var loanduty;
	
	if(amt <= 525000){
		var V = amt/1000;
		duty = (0.06571441 * V * V ) + (15*V);	
	}
	else{
		if(amt <= 3000000){
			duty = 4.95 * amt / 100;	
		}
		else{
			duty = 5.45 * amt / 100;
		}
	
	}
	
	notes = "<strong>First Home Owner Grant:</strong><br/>After 1 January 2015, the grant is<br/>(a) the FHOG is no longer available for established homes<br/>(b) $26,000 if the home is new contructed. No cap is applied.";
   display(duty, loanduty, mortReg, trans, notes);
}

function calculateQLD(amt, live_in, firsthome, new_home, land)
{
    
    var duty = undefined;
    var loanduty;
    var mortReg;
    var trans;
    var notes;
    notes = "<strong>First Home Owner Grant:</strong><br/>From 12 September 2012, for new home, the grant increases to $15,000 form 12 September 2012. The cap is $750,000";
    trans = 168.60;
	mortReg = 168.60;
	loanduty = 0;	 

	
    if (live_in) 
    {
        if (amt <= 350000) 
        {
            duty = Math.ceil(amt / 100) * 1;
        }
        else 
        {
            if (amt <= 540000) 
            {
                duty = 3500 + Math.ceil((amt - 350000) / 100) * 3.5;
            }
            else 
            {
                if (amt <= 1000000) 
                {
                    duty = 10150 + Math.ceil((amt - 540000) / 100) * 4.5;
                }
                else 
                {
                    duty = 30850 + Math.ceil((amt - 1000000) / 100) * 5.75;
                }
            }
        }
        var __reg3 = 0;
        if (firsthome)
        {
            if (amt < 504999.99) 
            {
                __reg3 = 8750;
            }
            else 
            {
                if (amt < 509999.99) 
                {
                    __reg3 = 7875;
                }
                else 
                {
                    if (amt < 514999.99) 
                    {
                        __reg3 = 7000;
                    }
                    else 
                    {
                        if (amt < 519999.99) 
                        {
                            __reg3 = 6125;
                        }
                        else 
                        {
                            if (amt < 524999.99) 
                            {
                                __reg3 = 5250;
                            }
                            else 
                            {
                                if (amt < 529999.99) 
                                {
                                    __reg3 = 4375;
                                }
                                else 
                                {
                                    if (amt < 534999.99) 
                                    {
                                        __reg3 = 3500;
                                    }
                                    else 
                                    {
                                        if (amt < 539999.99) 
                                        {
                                            __reg3 = 2625;
                                        }
                                        else 
                                        {
                                            if (amt < 544999.99) 
                                            {
                                                __reg3 = 1750;
                                            }
                                            else 
                                            {
                                                if (amt < 549999.99) 
                                                {
                                                    __reg3 = 875;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            duty = duty - __reg3;
            if (duty < 0) 
            {
                duty = 0;
            }
        }
    }
    else if(!live_in)
    {
        if (amt <= 5000) 
        {
            duty = 0;
        }
        else 
        {
			if (amt <= 75000) 
            {
                duty = Math.ceil((amt - 5000) / 100) * 1.5;
            }
            else 
            {
                if (amt <= 540000) 
                {
                    duty = 1050 + Math.ceil((amt - 75000) / 100) * 3.5;
                }
                else 
                {
                    if (amt <= 1000000) 
                    {
                        duty = 17325 + Math.ceil((amt - 540000) / 100) * 4.5;
                    }
                    else 
                    {
                        duty = 38025 + Math.ceil((amt - 1000000) / 100) * 5.75;
                    }
                }
            }
        }
    }
	
	if(amt>180000){
		trans = trans + ((amt-180000) * 31.8 / 10000);
	}
    display(duty, loanduty, mortReg, trans, notes);
}

function calculateSA(amt, live_in, firsthome, new_home, land)
{
    
    var trans = 0;
    var duty = undefined;
    var __reg3 = undefined;
    var mortReg = 155;
    var loanduty;
    
	
    if (amt <= 12000) 
    {
        duty = amt / 100 * 1;
    }
    else 
    {
        if (amt <= 30000) 
        {
            duty = (amt - 12000) / 100 * 2 + 120;
        }
        else 
        {
            if (amt <= 50000) 
            {
                duty = (amt - 30000) / 100 * 3 + 480;
            }
            else 
            {
                if (amt <= 100000) 
                {
                    duty = (amt - 50000) / 100 * 3.5 + 1080;
                }
                else 
                {
                    if (amt <= 200000) 
                    {
                        duty = (amt - 100000) / 100 * 4 + 2830;
                    }
                    else 
                    {
                        if (amt <= 250000) 
                        {
                            duty = (amt - 200000) / 100 * 4.25 + 6830;
                        }
                        else 
                        {
                            if (amt <= 300000) 
                            {
                                duty = (amt - 250000) / 100 * 4.75 + 8955;
                            }
                            else 
                            {
                                if (amt <= 500000) 
                                {
                                    duty = (amt - 300000) / 100 * 5 + 11330;
                                }
                                else 
                                {
                                    duty = (amt - 500000) / 100 * 5.5 + 21330;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
   
    var notes = "<strong>Land Transfer Fee: </strong><br/>from 1 July 2015: new rates and threshold table.<br/><strong>First Home Owner Grant:</strong><br/>FHOG is $15,000 for first home owners who build or purchase a brand new home.<br/>FHOG is no longer available for established home for an established home after 1 July 2014";
    display(duty, loanduty, mortReg, trans, __reg3);
}

function calculateTAS(amt, live_in, firsthome, new_home, land)
{
    
    var duty;
    var loanduty;
    var mortReg = 129.1;
    var trans = 197.81 ;
    var notes = "<strong>First Home Owner Grant:</strong><br/>After 1 July 2015, eligible First Home Owners are entitled to a one-off $10,000 for new constructed home.<br/><strong>First Home Builder Boost: </strong><br/>After 1 July 2015 the FHBB is still $20,000.";
    if (amt <= 3000) 
    {
        duty = 50;
    }
    else 
    {
        if (amt <= 25000) 
        {
            duty = Math.ceil((amt - 3000) / 100) * 1.75 + 50;
        }
        else 
        {
            if (amt <= 75000) 
            {
                duty = Math.ceil((amt - 25000) / 100) * 2.25 + 435;
            }
            else 
            {
                if (amt <= 200000) 
                {
                    duty = Math.ceil((amt - 75000) / 100) * 3.5 + 1560;
                }
                else 
                {
                    if (amt <= 375000) 
                    {
                        duty = Math.ceil((amt - 200000) / 100) * 4.35 + 5935;
                    }
                    else 
                    {
                        if (amt <= 725000) 
                        {
                            duty = Math.ceil((amt - 375000) / 100) * 4.25 + 12935;
                        }
                        else 
                        {
                            duty = Math.ceil((amt - 725000) / 100) * 4.5 + 27810;
                        }
                    }
                }
            }
        }
		
    }
   
    loanduty = 0;
    display(duty, loanduty, mortReg, trans, notes);
}

function calculateVIC(amt, live_in, firsthome, new_home, land)
{
    
    var trans = 0;
    var duty = undefined;
    var notes;
    var mortReg;
    var loanduty;
	
	trans = 138.9 + (2.46 * amt / 1000);  
    if (trans > 1369) 
    {
        trans = 1369;
    }
    mortReg = 114;
    loanduty = 0;
	
    if (!live_in) 
    {
        if (amt <= 25000) 
        {
            duty = amt / 100 * 1.4;
        }
        else 
        {
            if (amt > 25000 && amt <= 130000) 
            {
                duty = (amt - 25000) / 100 * 2.4 + 350;
            }
            else 
            {
                if (amt > 130000 && amt <= 960000) 
                {
                    duty = (amt - 130000) / 100 * 6 + 2870;
                }
                else 
                {
                    duty = amt / 100 * 5.5;
                }
            }
        }
		
    }
    else 
    {
        if (amt <= 25000) 
        {
            duty = amt / 100 * 1.4;
        }
        else 
        {
            if (amt > 25000 && amt <= 130000) 
            {
                duty = (amt - 25000) / 100 * 2.4 + 350;
            }
            else 
            {
                if (amt > 130000 && amt <= 440000) 
                {
                    duty = (amt - 130000) / 100 * 5 + 2870;
                }
                else 
                {
                    if (amt > 440000 && amt <= 550000) 
                    {
                        duty = (amt - 440000) / 100 * 6 + 18370;
                    }
                    else 
                    {
                        if (amt > 550000 && amt <= 600000) 
                        {
                            duty = (amt - 550000) / 100 * 6 + 28070;
                        }
                        else 
                        {
                            duty = ((amt - 550000) / 100 * 6 + 28070) * 2;
                        }
                    }
                }
            }
        }
		duty=duty/2;
    }
   
	
    notes = "Paper - Transaction; $90 Electronic - Transaction;<br/><strong>Land Transfer Fee: </strong>(a) Paper Transaction Fee: the sum of $138.9 plus $2.46 for every whole consideration, the maximum fee is $1,369.00. (b) Electronic Transaction Fee: the sum of $114.80 plus $2.46 for every whole consideration, the maximum fee is $1345.00, the difference is $22. On calculator, you can select payment method Paper Transaction  or Electronic Transaction.<br/><strong>First Home Owner Grant: </strong><br/>For contracts entered into on or after 1 July 2013 to build or purchase a new home, a payment of up to $10,000 is available for eligible first home buyers.";
    display(duty, loanduty, mortReg, trans, notes);
}

function calculateWA(amt, live_in, firsthome, new_home, land)
{
    
    var duty = undefined;
	live_in= true;
	
    if (amt <= 85000) 
    {
        trans = 164;
    }
    else 
    {
        if (amt <= 120000) 
        {
            trans = 174;
        }
        else 
        {
            if (amt <= 200000) 
            {
                trans = 194;
            }
            else 
            {
                if (amt <= 300000) 
                {
                    trans = 214;
                }
                else 
                {
                    if (amt <= 400000) 
                    {
                        trans = 234;
                    }
                    else 
                    {
                        if (amt <= 500000) 
                        {
                            trans = 254;
                        }
                        else 
                        {
                            if (amt <= 600000) 
                            {
                                trans = 274;
                            }
                            else 
                            {
                                if (amt <= 700000) 
                                {
                                    trans = 294;
                                }
                                else 
                                {
                                    if (amt <= 800000) 
                                    {
                                        trans = 314;
                                    }
                                    else 
                                    {
                                        if (amt <= 900000) 
                                        {
                                            trans = 334;
                                        }
                                        else 
                                        {
                                            if (amt <= 1000000) 
                                            {
                                                trans = 354;
                                            }
                                            else 
                                            {
                                                if (amt <= 1100000) 
                                                {
                                                    trans = 374;
                                                }
                                                else 
                                                {
                                                    if (amt <= 1200000) 
                                                    {
                                                        trans = 394;
                                                    }
                                                    else 
                                                    {
                                                        if (amt <= 1300000) 
                                                        {
                                                            trans = 414;
                                                        }
                                                        else 
                                                        {
                                                            if (amt <= 1400000) 
                                                            {
                                                                trans = 434;
                                                            }
                                                            else 
                                                            {
                                                                if (amt <= 1500000) 
                                                                {
                                                                    trans = 454;
                                                                }
                                                                else 
                                                                {
                                                                    if (amt <= 1600000) 
                                                                    {
                                                                        trans = 474;
                                                                    }
                                                                    else 
                                                                    {
                                                                        if (amt <= 1700000) 
                                                                        {
                                                                            trans = 494;
                                                                        }
                                                                        else 
                                                                        {
                                                                            if (amt <= 1800000) 
                                                                            {
                                                                                trans = 514;
                                                                            }
                                                                            else 
                                                                            {
                                                                                if (amt <= 1900000) 
                                                                                {
                                                                                    trans = 534;
                                                                                }
                                                                                else 
                                                                                {
                                                                                    if (amt <= 2000000) 
                                                                                    {
                                                                                        trans = 554;
                                                                                    }
                                                                                    else 
                                                                                    {
                                                                                        trans = Math.ceil((amt - 2000000) / 100000) * 20 + 554;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    var mortgage = 164;
    var notes = "><strong>First Home Owner Grant: </strong><br/>From 25 September 2013 First Home Owner Grant increases from $7,000 to $10,000 for first home buyers purchasing or building a new home and FHOG decreases to $3,000 for those purchasing an established home. The cap limits the total value of properties (i.e. total value of home and land) to $750,000.00 or less, or if the home is located north of the 26th parallel to $1,000,000 or less.";
	
    if (firsthome) 
    {
        if(land){
			if (amt <= 300000) 
			{
				duty = 0;
			}
			else if (amt <= 400000) 
			{
				duty = Math.ceil((amt - 300000) / 100) * 13.01;
			}			
		
		}else{
			if (amt <= 430000) 
			{
				duty = 0;
			}
			else if (amt <= 530000) 
			{
				duty = Math.ceil((amt - 430000) / 100) * 19.19;
			}			
		}
    }
    else 
    {
        if (amt <= 120000) 
        {
            duty = Math.ceil(amt / 100) * 1.9;
        }
        else 
        {
            if (amt > 120000 && amt <= 150000) 
            {
                duty = Math.ceil((amt - 120000) / 100) * 2.85 + 2280;
            }
            else 
            {
                if (amt > 150000 && amt <= 360000) 
                {
                    duty = Math.ceil((amt - 150000) / 100) * 3.8 + 3135;
                }
                else 
                {
                    if (amt > 360000 && amt <= 725000) 
                    {
                        duty = Math.ceil((amt - 360000) / 100) * 4.75 + 11115;
                    }
                    else 
                    {
                        duty = Math.ceil((amt - 725000) / 100) * 5.15 + 28453;
                    }
                }
            }
        }
    }
    var loanduty = 0;
    display(duty, loanduty, mortgage, trans, notes);
}

function loadresult_stamp()
{       
    if(jQuery('#state_id').val()=='')alert("Please choose state."); 
    else if(jQuery('#price').val()=='')alert("Please fill price with numbers.");    
    else if(!checkCharFormat( validNumbersNoSpace, jQuery("#price").val() ) )alert("Please fill price with numbers only."); 
    else{
        var amt = jQuery("#price").val();        
        var live_in = jQuery('#living_property').is(':checked');
		var firsthome = jQuery('#first_home_buyer').is(':checked');
		var new_home = jQuery('#new_home').is(':checked');
		var land = jQuery('#land').is(':checked');
        
        switch (jQuery('#state_id').val()) {
            case "1":calculateNSW(amt, live_in, firsthome, new_home, land);break;
            case "2":calculateVIC(amt, live_in, firsthome, new_home, land);break;
            case "3":calculateQLD(amt, live_in, firsthome, new_home, land);break;
            case "4":calculateSA(amt, live_in, firsthome, new_home, land);break;
            case "5":calculateWA(amt, live_in, firsthome, new_home, land);break;
            case "6":calculateTAS(amt, live_in, firsthome, new_home, land);break;
            case "7":calculateACT(amt, live_in, firsthome, new_home, land);break;
            case "8":calculateNT(amt, live_in, firsthome, new_home, land);break;
        }
    }
}

function display(duty, loanduty, mortReg, trans, notes)
{
    jQuery('#stamp_result_tr').show();
	jQuery('#registration_fee').val('$'+mortReg.formatMoney(2,',','.'));
    if(transfer_fee!=0)jQuery('#transfer_fee').val('$'+trans.formatMoney(2,',','.'));  
	else jQuery('#transfer_fee').val('data not provided yet');
    jQuery('#stamp_result').val('$'+duty.formatMoney(2,',','.'));
	var total = duty + mortReg + trans;
    jQuery('#total').val('$'+total.formatMoney(2,',','.'));
    jQuery('#notes').html(notes);

}

function number_format(num, decimal_places, decimal_separator, thousand_separator) {    
 var result = num.toFixed(decimal_places);
 var snum = result.split(".");
 var fnum = "";
 for (i=0; i<snum[0].length; i++) {
 if ((snum[0].length-i)%3==0) {
 fnum += thousand_separator;
 }
 fnum += snum[0].charAt(i);
 }
 var rnum = fnum ;

 return rnum;
}

Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

var validCharacters          = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz- ";
var validCharactersNoSpace   = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-";
var validLetters             = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
var validLettersNoSpace      = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
var validNumbers             = "0123456789 ";
var validNumbersNoSpace      = "0123456789";
var validDecimal             = "0123456789.";
var validPrice               = "0123456789,. ";

function checkCharFormat(validCharacters, compareValue){
    for(i=0 , n=compareValue.length; i<n ;i++)
    {
        ch1=compareValue.charAt(i);
        rtn1=validCharacters.indexOf(ch1);
        if(rtn1==-1)
        {
            return false;
        }
    }
    return true;
}
