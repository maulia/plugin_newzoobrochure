<div id="list_format">
<table class="property" width="100%" border="0" cellspacing="0" cellpadding="0">
		<?php
		$odd_class = "";
		foreach($properties as $property):if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned
			//if($count == 2) break; 
			extract($property,EXTR_PREFIX_ALL,"property");			
			$count++;
			global $db_helper;
			$row = $db_helper->get_results("SELECT * FROM agentlog_system.properties WHERE id= '" . $property_id. "' ");
			?>
				<tr class="prop_stats">
					<td class="type_cell"><?php echo strtoupper($property_property_type); ?></td>
					<td class="price_cell">Price: <?php echo (empty($property_auction_date)) ? $property_price_text : "Auction, $property_auction_date " . strtolower ($property_auction_time); ?></td>
					<td class="yield_cell">Gross Yield: <?php echo round($row[$count-1]["yield"],2).'%'; ?></td>
					<td class="rooms_cell">
						<?php if ( !$property_is_land): ?>
							<ul class="rooms">
								<li class="beds"><?php echo $property_bedrooms; ?>&nbsp;<img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/icon_bed.gif" alt="icon_bed" /></li>
								<li class="baths"><?php echo $property_bathrooms; ?>&nbsp;<img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/icon_bath.gif" alt="icon_bath" /></li>
								<li class="car"><?php echo $property_carspaces; ?>&nbsp;<img src="<?php bloginfo('stylesheet_directory'); ?>/images/icons/icon_car.gif" alt="icon_car" /></li>
							</ul>
						<?php endif; ?>
					</td>
				</tr>
				<tr class="prop_info">
					<td class="<?php echo ($odd_class = empty($odd_class) ? 'alt ' : ''); ?>imageCont">
						<div class="image">
						<a href="<?php echo $property_url; ?>" title="<?php echo $property_headline; ?>">
							<img src="<?php echo $property_photo_url; ?>" alt="<?php echo $property_address; ?>" /></a>
						<?php if ( ! empty ( $property_photo_overlay ) ):?>
						
						<div class="image_overlay">
						<img src="<?php echo $property_photo_overlay; ?>" style="visibility:hidden;" OnLoad="Global.FixPng( this );" alt="" />
						</div>
						<?php endif;?>
						
						</div>
					</td>
					<td colspan="3" class="<?php echo $odd_class; ?>descCont">
						<h3 class="suburb"><a href="<?php echo $property_url; ?>" title="<?php echo $property_headline; ?>"><?php echo "$property_address $property_suburb, $property_state" ;?></a></h3>
						<?php if($property_is_for_sale): ?><p class="description"><?php echo $property_headline; ?>&hellip;
							<a href="<?php echo $property_url; ?>">More information &raquo;</a></p><?php else: ?>				
						<p class="description">
							<?php echo substr($property_description,0, 130); ?>&hellip;
							<a href="<?php echo $property_url; ?>">More information &raquo;</a>
						</p>
						<?php endif;//property_is_for_sale ?>
							<div class="land_building_size">
							<?php if ($property_is_for_sale):?>
								<?php if(!empty($property_land_size['ha'])): ?>
								<p>Land Size: <span class="land_size"><?php echo $property_land_size['sqm']; ?></span><br />
								[ <?php echo $property_land_size['ha']; ?> ] [ <?php echo $property_land_size['acre']; ?> ]</p>
								<?php endif; ?>
								<?php if(!empty($property_building_size['sqm'])): ?>
								<p>Building Size: <span class="building_size"><?php echo $property_building_size['sqm']; ?></span></p>
								<?php endif; ?>
							<?php endif; ?>
							</div>
						<a onClick="return confirm( ' Are you sure want to remove from your Favourite listing? ' );" href="javascript:remProp(<?php echo $property_id; ?>);">Remove from Favourites</a>
					</td>
				</tr>			
			<?php endforeach; ?>
	</table>
</div>