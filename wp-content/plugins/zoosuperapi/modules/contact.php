<?php

function zoosuperapi_insert_contact(&$log, $contact, $options)
{
	global $wpdb;

	if (!isset($options['save_user_photos']))
		$options['save_user_photos'] = 0;
	if (!isset($options['remove_contact_ids']))
		$options['remove_contact_ids'] = array();
	else
		$options['remove_contact_ids'] = array_map('trim', explode(',', $options['remove_contact_ids']));

	if (empty($contact['id']))
	{
		$log[] = 'No data transmitted? ID was blank';
		return false;
	}

	$table_cache = zoosuperapi_get_tables();

	$columns = $table_cache['users_columns'];

	if (!empty($contact['deleted_at']) || in_array($contact['id'], $options['remove_contact_ids']))
	{
		$log[] = "Deleting contact {$contact['id']}";

		delete_images($contact['id'], true, 1);

		$wpdb->query("DELETE FROM users WHERE id = ".$contact['id']);

		return true;
	}

	$existing_user = $wpdb->get_row('SELECT * FROM users WHERE id = '.$contact['id'], ARRAY_A);

	$data = array();
	foreach ($columns as $column)
	{
		if (isset($contact[$column]))
			$data[$column] = $contact[$column];
	}

	if (empty($data))
	{
		$log[] = 'Empty user? Ignoring. ID '.$contact['id'];
		return false;
	}

	if (!empty($existing_user) && $existing_user['disable_update'] == 1)
	{
		$log[] = 'Disabled updates for this user. Ignoring. ID '.$contact['id'];
		return false;
	}

	if (empty($existing_user))
	{
		$log[] = 'User does not exist';

		if (zoosuperapi_insert('users', $data) === false)
		{
			$log[] = 'Insert Failed!';
			return false;
		}

		$log[] = 'User created';
	} else {
		$skip_fields = explode(',', $options['skip_fields_user']);

		$log[] = 'User does already exist.';
		$log[] = "Last updated at {$existing_user['updated_at']}, updating to {$data['updated_at']}";

		foreach ($skip_fields as $field)
		{
			$field = trim($field);
			unset($data[$field]);
		}

		if (zoosuperapi_update('users', $data, array('id' => $contact['id'])) === false)
		{
			$log[] = 'Update Failed!';
			return false;
		}

		$log[] = 'User updated';
	}

	$mr = new MultiRequests();

	if (isset($contact['attachments']))
	{
		$columns = $table_cache['attachments_columns'];
		
		$delete = array();
		$existing_attachments = $wpdb->get_results('SELECT * FROM attachments WHERE is_user = 1 AND parent_id = '.$contact['id'], ARRAY_A);

		foreach ($existing_attachments as $attachment)
			$delete[$attachment['id']] = true;

		$log[] = count($existing_attachments).' existing attachments';
		$log[] = count($contact['attachments']).' attachments sent';

		$a_inserted = 0;
		$a_updated = 0;
		$a_deleted = 0;
		$a_downloaded = 0;

		foreach ($contact['attachments'] as $aspect => $attachments)
		{
			foreach ($attachments as $position => $attachment)
			{
				$found = false;
				$updated = false;
				$download = false;
				$download_url = false;
				$basename = strtolower(basename($attachment['url']));
				$new_url = $options['save_user_photos'] ? ZOOSUPERAPI_UPLOADURL.'/agents/'.substr($basename, 0, 2).'/'.$attachment['id'].'_'.$basename : $attachment['url'];
				$new_path = $options['save_user_photos'] ? ZOOSUPERAPI_UPLOADDIR.'/agents/'.substr($basename, 0, 2).'/'.$attachment['id'].'_'.$basename : false;
				$attachment_id = $attachment['id'];
				unset($attachment['id']);
				$attachment['position'] = $position;
				$attachment['type'] = ($aspect ? 'landscape' : 'portrait');
				$attachment['is_user'] = 1;

				if ($options['save_user_photos'])
				{
					$download_url = $attachment['url'];
					$attachment['url'] = $new_url;
				}

				foreach ($existing_attachments as $existing_attachment)
				{
					if (
						$existing_attachment['description'] == $attachment['description'] &&
						$existing_attachment['position'] == $attachment['position'] &&
						$existing_attachment['type'] == $attachment['type'] &&
						$existing_attachment['url'] == $new_url &&
						strtotime($existing_attachment['updated_at']) <= strtotime($attachment['updated_at'])
						)
					{
						$found = $existing_attachment['id'];
						$updated = false;
						$delete[$existing_attachment['id']] = false;
						break;
					} elseif (
						$existing_attachment['description'] == $attachment['description'] &&
						$existing_attachment['position'] == $attachment['position'] &&
						$existing_attachment['type'] == $attachment['type']
						)
					{
						$found = $existing_attachment['id'];
						$updated = true;
						$delete[$existing_attachment['id']] = false;
						break;
					}
				}

				if (!$found)
				{
					zoosuperapi_insert('attachments', filter_columns($columns, $attachment));
					if ($wpdb->rows_affected > 0)
					{
						$a_inserted++;
						$download = true;
					} else {
						var_dump($wpdb->func_call, mysql_error());
					}
				} elseif ($found && $updated) {
					delete_images(array($found), false);

					zoosuperapi_update('attachments', filter_columns($columns, $attachment), array('id' => $found));
					$a_updated++;
					$download = true;
				}

				if ($found && $new_path && !file_exists($new_path)) {
					$download = true;
				}

				if ($download && $new_path)
				{
					$mr->request($download_url, array(), 'store_img', array('path' => $new_path));
					$a_downloaded++;
				}
			}
		}

		$imgs = array();
		foreach ($delete as $attachment_id => $process)
		{
			if ($process)
			{
				$imgs[] = $attachment_id;
				$a_deleted++;
			}
		}
		delete_images($imgs);

		$log[] = "Attachments: Inserted $a_inserted, updated $a_updated, deleted $a_deleted, downloaded $a_downloaded";
	}

	if (isset($contact['testimonials']))
	{
		$delete = array();
		$existing_testimonials = $wpdb->get_results('SELECT * FROM testimonials WHERE user_id = '.$contact['id'], ARRAY_A);
		$columns = $table_cache['testimonials_columns'];

		foreach ($existing_testimonials as $testimonial)
			$delete[$testimonial['id']] = true;

		foreach ($contact['testimonials'] as $testimonial)
		{
			$found = false;

			foreach ($existing_testimonials as $existing_testimonial)
			{
				if (
					$existing_testimonial['user_name'] == $testimonial['user_name'] &&
					$existing_testimonial['content'] == $testimonial['content'] &&
					$existing_testimonial['image_url'] == $testimonial['image_url'] &&
					$existing_testimonial['updated_on'] == $testimonial['updated_on']
					)
				{
					$found = true;
					$delete[$existing_testimonial['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$row = filter_columns($columns, array(
					'user_id' => $contact['id'],
					'user_name_id' => $contact['id'],
					'user_name' => $testimonial['user_name'],
					'content' => $testimonial['content'],
					'image_url' => $testimonial['image_url'],
					'created_on' => $testimonial['created_on'],
					'updated_on' => $testimonial['updated_on'],
					));
				if (!empty($row))
					zoosuperapi_insert('testimonials', $row);
			}
		}

		foreach ($delete as $testimonial_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM testimonials WHERE id = '.$testimonial_id);
			}
		}
	} else {
		$wpdb->query('DELETE FROM testimonials WHERE user_id = '.$contact['id']);
	}

	if (isset($contact['user_groups']) && $table_cache['user_groups_exists'])
	{
		$delete = array();
		$existing_user_groups = $wpdb->get_results('SELECT * FROM user_groups WHERE agent_user_id = '.$contact['id'], ARRAY_A);
		$columns = $table_cache['user_groups_columns'];

		foreach ($existing_user_groups as $user_group)
			$delete[$user_group['id']] = true;

		foreach ($contact['user_groups'] as $user_group)
		{
			$found = false;

			foreach ($existing_user_groups as $existing_user_group)
			{
				if (
					$existing_user_group['group'] == $user_group['group'] &&
					$existing_user_group['updated_at'] == $user_group['updated_at']
					)
				{
					$found = true;
					$delete[$existing_user_group['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$row = filter_columns($columns, array(
					'agent_user_id' => $contact['id'],
					'group' => $user_group['group'],
					'created_at' => $user_group['created_at'],
					'updated_at' => $user_group['updated_at'],
					));
				if (!empty($row))
					zoosuperapi_insert('user_groups', $row);
			}
		}

		foreach ($delete as $user_group_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM user_groups WHERE id = '.$user_group_id);
			}
		}
	} elseif ($table_cache['user_groups_exists']) {
		$wpdb->query('DELETE FROM user_groups WHERE agent_user_id = '.$contact['id']);
	}

	while ($mr->active_requests() > 0)
	{
		$mr->wait();
	}

	return true;
}
