<?php if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ ?>
<?php
$similar_listings = $realty->similar_listings($settings);
if($similar_listings['results_count'] >0 )://if any similar properties were found
?>
<div id="similar_listings" class="sim_content">
<?php /*echo $before_title . $settings['Title'] . $after_title;*/ ?>
<h2 class="section_title"><?php echo $settings['Title']; ?></h2>
<div class="block_content">
<?php 
$count =0;
foreach($similar_listings as $property): if(!is_array($property)) continue;?>
<div class="block">
<div class="image">
	<a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>"><img src="<?php echo $property['thumbnail']; ?>"  alt="<?php echo $property['headline']; ?>" /></a>
	<?php if ($property['is_sold']):?>
	<div class="image_overlay">
		<img src="<?php echo $realty->path($realty->settings['property_stickers']['sticker_src'],'url' ); ?>" style="visibility:hidden;" alt="<?php echo $realty->settings['property_stickers']['sticker_text']['sold'];?>" />
	</div>
	<?php endif;?>
</div>

<div class="similar_property_info">
	<ul>
		<li class="sim_suburb suburb"><span class="field">Suburb:</span> <?php echo $property['suburb']; ?></li>
		<li class="sim_type type"><span class="field">Type:</span> <?php echo $property['property_type']; ?></li>
        <li class="sim_price price"><span class="field">Price:</span><?php echo $property['price']; ?></li>
		<li class="sim_link link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">View Property &#187;</a></li>
	</ul>
</div>
<div class="clearer"></div>
</div>
<?php ++$count;endforeach; ?>
</div>
</div><!-- end .similar_listings -->
<?php endif;// Similar properties found or not ?>