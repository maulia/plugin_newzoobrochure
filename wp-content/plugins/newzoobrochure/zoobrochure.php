<?php

/**
 * Plugin Name:       Zoo Brochure V2
 * Description:       New Property brochure templates for realty plugins
 * Version:           2.0.0
 * Author:            Agentpoint
 * Author URI:        http://www.agentpoint.com.au
 */
// If this file is called directly, abort.
defined('ABSPATH') or die('No script kiddies please!');

// Prepare all functionality 
require_once plugin_dir_path(__FILE__) . 'includes/class-zoobrochure.php';


/**
 * Function that will genereate instance of zoobrochure class
 * 
 * @since 2.0.0
 * @return object Instance of ZooBorhure Class
 */
if (!function_exists('zoobrochure')) {

    function zoobrochure() {
        return zoobrochure::instance();
    }

}

/**
 * Begins execution of the plugin, and use $GLOBALS for backward compability
 *
 * @since    2.0.0
 */
$GLOBALS['zoobrochure'] = zoobrochure();


/**
 * Activation & Deactivation Hook zoobrochure
 * 
 * @since 2.0.0
 */
register_activation_hook(__FILE__, function (){
    // We need to trigger route first to make routing works
    zoobrochure()->routes()->set_rewrite_wp_rules();
    flush_rewrite_rules();
});
register_deactivation_hook(__FILE__, array('zoobrochure', 'flush_rules'));