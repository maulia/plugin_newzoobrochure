<?php
global $wpdb, $helper;
$properties = $this->properties(array('list'=> 'upcoming_auction','status'=>'1,4', 'order'=>'auction_date'));
if($properties['results_count']=='0'): ?><p class="return">No properties are available for viewing. Please check again later.</p><?php
else:
?>
<?php if($this->settings['general_settings']['display_pdf_button_in_search_result']=='1'){ ?>
<p class="pdf btn"><a href="javascript:void(0)" onclick="return window.open('<?php echo $this->pluginUrl."display/elements/search_brochure.php?list=upcoming_auction"; ?>', 'Brochure', 'width=820, height=960,resizable=1,scrollbars=1'); return false;" title="Print Results" class="btn">PDF</a></p>
<?php } ?>
<?php echo $properties['pagination_above']; ?>

<div class="table-wrap">
<?php
	$count = 0;$auction_date='';
	foreach($properties as $property):if(!is_array($property))continue;
		$count++; 
		if($auction_date!=$property['auction_date']){
		$auction_date=$property['auction_date'];
		?>
			<table cellspacing="0" id="date_container_<?php echo $count; ?>" class="date_container" summary="Forthcoming Auctions">
			<caption class="opentimes_date"><?php echo date('l jS F Y',strtotime($property['auction_date']));?></caption>
			<tr>
				<th class="auction_time">Time</th>
				<th class="auction_place">Place</th>
				<th class="suburb_cell">Suburb</th>
				<th class="address_cell">Address</th>
				<th class="property_type_cell">Property Type</th>
				<th class="bedrooms_cell"><span>Beds</span></th>
				<th class="bathrooms_cell"><span>Bath</span></th>
				<th class="carspaces_cell"><span>Cars</span></th>
			</tr>
		<?php } ?>
			
			<tr <?php echo $odd_class = empty($odd_class) ? ' class="alt"' : ''; ?>>
				<td class="auction_time">
				<?php if (!empty($property['auction_time']) && $property['auction_time']!='00:00:00'){ ?>
					<a href="<?php echo $this->pluginUrl."display/elements/crm.php?property_id=".$property['id']."&time=".$property['auction_time']; ?>" title="Add to Calendar" class="calendar"></a>
				<?php } ?>
				<span><?php if (!empty($property['auction_time']) && $property['auction_time']!='00:00:00')echo date('g:ia', strtotime($property['auction_time'])); ?></span></td>
				<td class="place_cell"><span><?php echo $property['auction_place']; ?></span></td>
				<td class="suburb_cell"><span><?php echo $property['suburb']; ?></span></td>
				<td class="address_cell"><span><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo ($property['street_address']=='')?'no address available':$property['street_address']; ?></a></span></td>
				<td class="property_type_cell"><span><?php echo $property['property_type']; ?></span></td>
				<td class="bedrooms_cell"><span><?php echo $property['bedrooms']; ?></span></td>
				<td class="bathrooms_cell"><span><?php echo $property['bathrooms']; ?></span></td>
				<td class="carspaces_cell"><span><?php echo $property['carspaces']; ?></span></td>
			</tr>
		<?php if($property['auction_date'] != $properties[$count]['auction_date'])echo "</table>"; 
	endforeach; // properties_date ?>

</div>	
<div id="bottom_pagination"><?php echo $properties['pagination_below']; ?></div>

<?php endif;//auction is empty?>