<?php //metadata: access_level=8 
if($_GET['task']=='send_email_alerts'): 
	require_once('../../../../../wp-blog-header.php');
	extract($_GET);
endif;
?>
<div class="wrap">
<h2><?php _e('Send Alerts'); ?></h2>
	<form name="send_email_alerts" method="get" target='alerts_content' action="<?php echo $this->form_action; ?>" onsubmit="window.open(this.action, this.target, 'width=900, height=700, resizable=1,scrollbars=1, left=300'); return true;">
		<input type="hidden" name="page" value="SendAlerts" />
		<input type="hidden" name="task" value="send_email_alerts" />
		<table class="form-table">
			<tr>
				<td>
					<select name="alert">
						<option value="all_alerts">All Alerts</option>
						<?php foreach($list_alerts as $key=>$value): ?>
						<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
					<input type="checkbox" checked="checked" name="test_only" value="1" onclick="if(document.getElementById('subscriptions_sender').value=='Send Alerts') {document.getElementById('subscriptions_sender').value='Test';} else{ document.send_email_alerts.subscriptions_sender.value='Send Alerts';}" />
					&nbsp;Test Only ( No e-mails will be sent).
				</td>
			</tr>
			<tr>
				<td>Recipient email: <br /><input type="text" name="test_email" value="" size="80"  /></td>
			</tr>
			<tr>
				<td><input class="button" value="Test" id="subscriptions_sender" type="submit" onclick="if (document.send_email_alerts.test_only.checked == 0) return confirm('WARNING: \nYou are about to send the weekly alerts ahead of time.\n\'OK\' to continue, \'Cancel\' to stop.' );" /></td>
			</tr>
		</table>		
	</form>
</div>