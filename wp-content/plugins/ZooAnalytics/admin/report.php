<div class="wrap">		
	<h2><?php echo $title; ?></h2>
	<p><?php echo $offices[0]['address']; ?></p>
	<div class="tablenav"><?php echo $properties['pagination_label']; ?></div>
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col">Suburb</th>
			<th scope="col">Address</th>
			<th scope="col" style="text-align: center">Num<br/>Hits</th>
			<th scope="col" style="text-align: center">Num<br/>Emails</th>
			<th scope="col" style="text-align: center">Brochure<br/>Views</th>
			<th scope="col" style="text-align: center">Email to<br/>Friend</th>
			<th scope="col" style="text-align: center">Email to<br/>Self</th>
			<th scope="col" style="text-align: center">Share on<br/>Twitter</th>
			<th scope="col" style="text-align: center">Share on<br/>Facebook</th>
			<th scope="col" style="text-align: center">Shortlisted</th>
			<?php if($_GET['action']=='view_weekly'){ ?><th scope="col" style="text-align: center">Action</th><?php } ?>
		</tr>
		</thead>
		<tbody id="the-list">
			<?php if(!empty($properties)){
			 $i=0;foreach ($properties as $property): if(!is_array($property))continue; ?>
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
			<td><a href="<?php echo $property['url']; ?>" title="<?php echo $property['address']; ?>"><?php echo $property['suburb']; ?></a></td>
			<td><?php echo $property['address']; ?></td>
			<td style="text-align: center"><?php echo $property['visit']; ?> </td>
			<td style="text-align: center"><?php echo $property['enquiry']; ?></td>
			<td style="text-align: center"><?php echo $property['brochure']; ?></td>
			<td style="text-align: center"><?php echo $property['email_friend']; ?></td>
			<td style="text-align: center"><?php echo $property['email_self']; ?></td>
			<td style="text-align: center"><?php echo $property['share_on_twitter']; ?></td>
			<td style="text-align: center"><?php echo $property['share_on_facebook']; ?></td>
			<td style="text-align: center"><?php echo $property['favorite']; ?></td>
			<?php if($_GET['action']=='view_weekly'){ ?><td style="text-align: center"><a href="<?php echo $this->form_action; ?>&amp;action=view_detail&property_id=<?php echo $property['property_id']; ?>">View Detail</a></td><?php } ?>
			</tr>
			<?php $i++; endforeach; } ?>
		</tbody>
	</table>
	<div class="tablenav"><?php echo $properties['pagination_label']; ?></div>
	<p><a href="<?php echo $this->form_action;?>"><?php _e('&laquo; Back'); ?></a></p>
</div>