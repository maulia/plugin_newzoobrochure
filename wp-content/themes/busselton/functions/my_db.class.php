<?php

/*Version 1.1 */

class my_db{
	/** Queries an SQL statement and produces dbugging information if required.
	* @param string $sql The SQL statement to be executed.
	* @return mixed On success, this function has the same return value as the PHP function mysql_query(). On error, if the script is running on debugging mode, it will stop the script and output an error, otherwise, nothing will be returned. 
	* @uses $query = $this->query($sql); */
		var $debug = false;
		var $print_query = false;
		function query($sql){
		
		//make this $_GET[debug to work only when user is logged in
			$time_start = $this->microtime_float();
			$return = mysql_query( $sql) or $this->error(mysql_error(),$sql);
			$time_end = $this->microtime_float();
			if ($this->debug)
				echo '<p><font color="red">SQL: </font>' . $sql . '<br/><font color="blue" size="-3">*[Query took ' . number_format(($time_end - $time_start),6) . ' seconds ]*</font></p>';
			if ($this->print_query)
				echo '<p>' . $sql . '</p>';

			return $return;
		}//ends function
	/** Returns one field for the SQL statement. The sql statement should have a " LIMIT 1" clause for efficient time management.
	* @param string $sql The SQL statement to be executed.
	* @return string/boolean Returns the field fetched if found, otherwise, returns false. 
	* @uses $projects[$key]['auto_make'] = $this->get_var("SELECT `make` FROM auto_make WHERE `make_id` IN (" . $project['auto_make'] . ") LIMIT 1"); */
		
		function get_var($sql){			
			$query = $this->query($sql);			
			if(!$query || mysql_num_rows($query) <1)
				return false;
			else
				return stripslashes(mysql_result($query,0));
		}//ends function
	/** Returns one single column for the SQL statement.
	* @param string $sql The SQL statement to be executed.
	* @param string $column The name of the column to be returned.
	* @return array The array with all the rows with the values of the fields in the given column. 
	* @uses $paths = $this->get_column("SELECT `path` FROM photos WHERE `project_id` IN ($project_id)", 'path'); */
		function get_column($sql, $column){
			$result = $this->query($sql);
			$return = array();
			while ($row = mysql_fetch_assoc($result))
				array_push($return,stripslashes($row[$column]));
			return $return;	
		}//ends function
	/** Returns one single row for the $sql statement. By default, the row returned is the first row (0) and the sql statement should have a " LIMIT 1" clause for efficient time management.
	* @param string $sql The SQL statement to be executed.
	* @param integer $row The row number to be returned.
	* @return array The array with the columns and their values returned by the SQL statement. 
	* @uses $rows = $this->get_row("SELECT * FROM projects WHERE `plugin`='autoskin' AND auto_model='Ferrari' LIMIT 1 "); */
		function get_row($sql, $row=0){
			$return = $this->get_results($sql);
			return $return[$row];
		}//ends function
	/** Returns the results fetched by an SQL statement.
	* @param string $sql The SQL statement to be executed.
	* @return array The returned rows for the statement. 
	* @uses $rows = $this->get_results("SELECT * FROM projects WHERE `plugin`='autoskin' ORDER BY `completed_on` DESC "); */
		function get_results($sql, $return_type = 'array'){
			$result = $this->query($sql);
			$return = array();
			if($return){
			while ($row = mysql_fetch_assoc($result)):
				array_walk($row,array( 'my_db', 'my_stripslashes'));
				if($return_type == 'object')
					$this->parseArrayToObject($row);
				array_push($return,$row);
			endwhile;	
			}
			return $return;
		}//ends function
	/** Displays the sql statements used in the script and the time it took to complete them.
	* @param float $time_start The start time of the event.
	* @param float $time_end The end time of the event.
	* @return void The function simply outputs the debugging messages if the url is set with $_GET['debug']=true.
	* @uses 	$this->debug($sql, $time_start, $time_end); */
			

	/** Displays an error message determined by $error if the debug option set by $_GET['debug'] is true.
	* @param string $error The error message to be printed.
	* @param string $sql The sql statement that produced the error.
	* @return void This function will cause the script to terminate immediatelly and output the error message. 
	* @uses $return = mysql_query( $sql) or $this->error(mysql_error(),$sql); */
		
		function error($error,$sql){
			if ($this->debug)
				die($error . '<p><font color="red">SQL: </font>' . $sql . '</p>');
		}//ends function
	/** Simple function to replicate PHP 5 behaviour. Returns the time in seconds an event started. Should be used before and after the event to calculate its time interval
	* @return float The number of seconds taken. 
	* @uses 	$time_start = $this->microtime_float();
				$return = mysql_query( $sql) or $this->error(mysql_error(),$sql);
				$time_end = $this->microtime_float(); */

		function microtime_float(){
			list($usec, $sec) = explode(" ", microtime());
			return ((float)$usec + (float)$sec);
		}
		function my_stripslashes(&$string){
			$string = stripslashes($string);
		}
		function parseArrayToObject(&$array) {
		   $object = new stdClass();
		   if (is_array($array) && count($array) > 0) {
			  foreach ($array as $name=>$value) {
				 $name = trim($name);
				 if (!empty($name)) {
					$object->$name = $value;
				 }
			  }
		   }
		   $array = $object;
		   return $object;
		}
		
		function parseObjectToArray(&$object) {
		   $array = array();
		   if (is_object($object)) {
			  $array = get_object_vars($object);
		   }
		   $object = $array;
		   return $array;
		}
		
			
}//ends class
?>
