<?php if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ ?>
<div class="map_and_walkscore">
	<div class="block_content">
	<?php wp_print_scripts('jquery-ui-core');wp_print_scripts('jquery-ui-tabs');?>
	<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude'])): ?>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<?php endif; ?>	
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#map_walk_score").tabs();	
		<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude'])): ?>
		var myLatLng = new google.maps.LatLng(<?php echo $realty->property['latitude']; ?>,<?php echo $realty->property['longitude']; ?>);
        var myOptions = {
            zoom: 12,
            center:myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };	
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);	
        var Marker = new google.maps.Marker({  position: myLatLng, map: map });
		
		jQuery("#map_tabs").click(function(){
			google.maps.event.trigger(map, 'resize');
		});
		
		<?php endif; ?>	
	});
	</script>
	<?php $settings['video_width']=($settings['video_width']=='')?'510':$settings['video_width'];$settings['video_height']=($settings['video_height']=='')?'385':$settings['video_height']; ?>
		<div id="map_walk_score">
			<ul class="shadetabs sales_data" id="stattabs sales_data">	
				<?php if( strpos($realty->property['ext_link_2'],'youtu')!==false || !empty($realty->property['property_video'])): ?>
				<li class="stat4"><a href="#stat4">Video</a></li>		
				<?php endif; ?>
			
				<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude'])): ?>
				<li class="stat1"><a href="#stat1" id="map_tabs">Map</a></li>
				<?php endif; ?>
				
				<li class="stat2"><a href="#stat2">Walkability</a></li>	
				
				<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude']) && $settings['map_mode']=='street_view'): ?>
				<li class="stat3"><a href="#stat3">Street View</a></li>
				<?php endif; ?>
			</ul>
			<div id="property_stats">
				<?php if( strpos($realty->property['ext_link_2'],'youtu')!==false || !empty($realty->property['property_video'])){ ?>
				<div class="tabcontent" id="stat4">
					<?php 
					if(strpos($realty->property['ext_link_2'],'youtu')!==false ){
						$youtube_id= trim($realty->property['ext_link_2']);
						$youtube_id=str_replace("http://www.youtube.com/embed/","",$youtube_id); 
						if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtube_id, $matches)){
							$youtube_id=$matches[0];
						}
					 ?>
					 <object width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>">
						<param name="movie" value="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>">
						<param name="allowFullScreen" value="true">
						<param name="allowscriptaccess" value="always">
						<param name="wmode" value="transparent" />
						<embed src="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>" wmode="transparent" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>"></embed>
					</object>
					<?php }elseif(!empty($property_video['embed_code'])){
						$embed_code=$property_video['embed_code'];
						
						$embed_code=preg_replace('/(width=")([\d]+")/is', 'width="'.$settings['video_width'].'"', $embed_code);
						$embed_code=preg_replace('/(height=")([\d]+")/is', 'height="'.$settings['video_height'].'"', $embed_code);
						
						echo $embed_code ;
					}elseif(!empty($property_video['youtube_id']) && $property_video['status']==NULL){
					$youtube_id= trim($property_video['youtube_id']);
					if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtube_id, $matches)){
						$youtube_id=$matches[0];
					}
					$youtube_id=str_replace("http://www.youtube.com/embed/","",$youtube_id);
					 ?>
					 <object width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>">
						<param name="movie" value="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>">
						<param name="allowFullScreen" value="true">
						<param name="allowscriptaccess" value="always">
						<param name="wmode" value="transparent" />
						<embed src="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>" wmode="transparent" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>"></embed>
					</object>
					<?php }elseif(!empty($youtube_id) && $property_video['status']=='0'){ ?>
						<iframe align="top" border="0" frameborder="0" height="450"  src="<?php echo $realty->pluginUrl.'display/elements/video.php?property_id='.$_REQUEST['property_id']; ?>" target="_top" width="100%">Your browser does not support inline frames or is currently configured not to display inline frames.</iframe>
					<?php }	?>				
				</div>
				<?php } ?>
				
				<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude'])): ?>
				<div class="tabcontent" id="stat1"><?php map($before_title,$after_title); ?></div>	
				<?php endif; ?>
				
				<div class="tabcontent" id="stat2"><?php walkability($before_title,$after_title); ?></div>
				
				<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude']) && $settings['map_mode']=='street_view'){?>
				<div class="tabcontent" id="stat3" style="width:<?php echo $settings['map_width']; ?>px;height:<?php echo $settings['map_height']; ?>px;"><?php street_view($before_title,$after_title); ?></div>
				<?php } ?>				
			</div>
		</div>
	</div>
</div>		
<?php

function video($url, $width, $height){ 
?>
	<object width="<?php echo $width;?>" height="<?php echo $height;?>">
		<param name="movie" value="<?php echo str_replace("v=","v/",str_replace("watch?","",$url)); ?>">
		<param name="allowFullScreen" value="true">
		<param name="allowscriptaccess" value="always">
		<embed src="<?php echo str_replace("v=","v/",str_replace("watch?","",$url)); ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $width;?>" height="<?php echo $height;?>"></embed>
	</object>
<?php	
}

function street_view($before_title,$after_title){
global $realty;
?>
<script type="text/javascript">
    var fenway = new google.maps.LatLng(<?php echo $realty->property['latitude']; ?>, <?php echo $realty->property['longitude']; ?>);
    var panoramaOptions = {
      position:fenway,
      pov: {
        heading: 165,
        pitch:10,
        zoom:1
      }
    };
    var myPano = new google.maps.StreetViewPanorama(document.getElementById("stat3"), panoramaOptions);
    myPano.setVisible(true);
   
</script>

<?php 
 }
 
function walkability($before_title,$after_title){
global $realty;
$settings=$realty->settings['widgets']['map_and_walkscore'];
?>
<div id="walk_score">
<script type="text/javascript">
/* <![CDATA[ */
  var ws_address = "<?php echo $realty->property['street_address'] . ', ' . $realty->property['suburb'] . ' ' . $realty->property['state'] .' '. $realty->property['postcode'] . ' Australia'; ?>";
  var ws_width = "<?php echo $settings['width']; ?>";
  var ws_height = "<?php echo $settings['height']; ?>";
  var ws_hide_scores_below = "50";
  var ws_layout = 'horizontal';
  var ws_iframe_css ="<?php echo $settings['iframe_css']; ?>";
  var ws_map_frame_color = "<?php echo $settings['map_frame_color']; ?>";
  var ws_score_color = "<?php echo $settings['score_color']; ?>";
  var ws_headline_color = "<?php echo $settings['headline_color']; ?>";
  var ws_category_color = "<?php echo $settings['category_color']; ?>";
  var ws_result_color = "<?php echo $settings['result_color']; ?>";
/* ]]> */
</script>

<script type="text/javascript" src="http://walkscore.com/tile/show-tile.php?wsid=<?php echo $realty->settings['general_settings']['walk_score_key']; ?>">
</script>
</div><!-- end #walk_score -->
<?php
}

function map($before_title,$after_title){
global $realty;
$settings=$realty->settings['widgets']['map_and_walkscore'];
if(!empty($realty->property['latitude']) and !empty($realty->property['longitude'])): ?>
<div class="property_map">
	<?php echo $before_title; ?><?php echo $realty->property['street_address']; ?><?php echo $after_title; ?>
	<div id="map_canvas" style="width:<?php echo $settings['map_width']; ?>px;height:<?php echo $settings['map_height']; ?>px;">
		<div id="map_loading">Loading...</div>
	</div>
</div><!-- .property_map ends -->
<div class="clearer"></div>
<?php endif; 
}
?>