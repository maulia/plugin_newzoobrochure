<?php
class realtyRss{
	function 	realtyRss($searchtype, $title){
		$this->property_type = $searchtype;
		$this->title = $title;			
		$this->description = "New Real Estate properties from " .$this->title;
		header("Content-Type: text/xml; charset=ISO-8859-1");
		echo $this->buildRss();
		exit(0); //terminate so the html doesn't output
	}

	function buildRss(){
		global $realty, $aggregator, $helper; 
		echo '<?xml version="1.0" encoding="ISO-8859-1"?><?xml-stylesheet type="text/css" href="'.$this->pluginUrl.'style/rss.css" ?>';
?><rss version="2.0">
		<channel>
			<title><?php echo $this->title ; ?></title>
			<link><?php echo  $realty->siteUrl ; ?></link>
			<description><?php echo  $this->description ; ?></description>
			<language>en-gb</language>
    		<lastBuildDate><?php echo  date("r" ) ; ?></lastBuildDate>
<?php		
		$results = $realty->searchResults(array('searchtype'=>$this->property_type,'max'=>500));
		$properties = (isset($results['Property']['id'])) ? array($results['Property']): $results['Property'];
		//p($results);
		ob_start();			
		if($results['TotalResults'] > 0) foreach($properties as $result):
			$aggregator->formatProperty($result);
?><item>
		<title><?php echo $result['property_type'] . " - " . $result['Suburb']; ?></title>						
		<link><?php echo $result['Url']; ?></link>
		<description><![CDATA[
		<?php echo $result['street_address'] . ' ' .  $result['Suburb'] . ' '. $result['State']; ?>
		<h2><?php echo $result['Title']; ?></h2>
		<?php					
		
		if ($result['Beds'] >0) echo "Bedrooms: ". $result['Beds'];
		if ($result['Baths'] >0) echo " - Bathrooms: ". $result['Baths']; 
		if ($result['Cars'] >0) echo " - Carspaces: ". $result['Cars'];
		if(!empty($result['Price']) and !($result['isAuction']))
			echo $property['Price'].'<br><br>'; 
		echo $helper->_substr($result['About'], $realty->settings['display_results']['propertyDescription']);
?><br><br><img src="<?php echo  $result['Photos']['Photo'][0]['PhotoLink']; ?>" alt="<?php echo $result['Title'] ; ?>" title="<?php echo  $result['Title']; ?>" />
		]]></description>
		<pubDate><?php echo date("r", time() ) ; //date("r", strtotime ($result['created_at']) ) ; ?></pubDate>				
		<guid isPermaLink="false"><?php echo $result['Url']; ?></guid>
	</item><?php
			endforeach;
?></channel>
</rss>
<!-- <atom:link href="<?php echo  $this->site_url ; ?>?view_feed=<?php echo  $this->property_type  ; ?>" rel="self" type="application/rss+xml" />-->			
<?php return ob_get_clean();

	}
}
?>