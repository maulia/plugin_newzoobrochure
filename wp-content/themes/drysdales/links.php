<?php
/*
Template Name: Links
*/
global $options;
foreach ($options as $value) {
    if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
$mycat_id = $links_library;
get_header(); ?>

<div id="content">
<h2 class="entry_title">Links</h2>

<div class="navigation jump">
<form action="...">
<p>Jump to a category: <select id="blogroll_cats_dropdown" name="blogroll_cats_dropdown" onchange="location = this.options[this.selectedIndex].value;">
	<option>Select a Category &#187;</option>
<?php
	$categories = get_terms('link_category', "hide_empty=1");
	foreach ($categories as $cat) {
	$cat_desc = $cat->description;
	$links_var = explode(',',$links_library);
	echo '<option value="'.get_option('home').'/';
		foreach ($GLOBALS['links_var'] as $links_var_value) {
		$this_page = '';
		if ( $cat->term_id == $links_var_value ) { $this_page = 'links/'; }
		echo $this_page;
	}
		echo '#linkcat-' . $cat->term_id . '">' . sanitize_term_field('name', $cat->name, $cat->term_id, 'link_category', 'display') . '</option>
		';
	}
?>
</select></p>
</form>
</div>

<p class="button big"><a href="#" class="btn" title="Add my Link">Add my Link</a></p>

<ul class="myLinks">
<?php my_list_bookmarks(array(
	'show_images' => '1',
	'show_name' =>'1',
	'show_description'=>'1',
	'title_before' => '<h4>',
	'title_after' => '</h4>',
	'before' =>  '<li class="li_border"><span>',
	'between' => '<br />',
	'after' => '</span></li>',
	'link_after' => ' &#187;',
	'category_after' => '</li><li><p class="more"><a href="#pages">Back to top</a></p><hr /></li>',
	'category' => $mycat_id));
?>
</ul>
</div>
</div><!-- end #content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>