<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
<label class="hidden" for="s"><?php _e('Search for:'); ?></label>
<div><input type="text" value="<?php the_search_query(); ?>" name="s" id="s" />
<p class="button"><input type="submit" id="searchsubmit" class="btn" value="Search" /></p>
</div>
</form>
