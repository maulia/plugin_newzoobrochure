<?php if(!isset($_COOKIE["fav_visit"]))$this->set_visit_cookie(); ?>
<div id="list_fav">
<div id="sorter_pagination">
<form name="fav_results" action="" method="get">
	<p class="number_properties">
		There <?php echo ($results_count == 1) ? 'is': 'are'; ?> <strong><?php echo (int)$results_count; ?></strong>
		<?php echo " $radio_type "; ?><?php echo ($results_count == 1) ? 'property': 'properties'; ?> available
		<?php if (!empty($price_min)): ?> with prices from <strong>$<?php echo number_format( $price_min, 0, '.',','); ?></strong><?php endif; ?>
		<?php if (!empty($price_max)): ?> to <strong>$<?php echo number_format( $price_max, 0, '.',','); ?></strong><?php endif; ?>
		<?php if (!empty($keywords)) echo " in $keywords"; ?> with your favourites results.
	</p>
		
	<div class="sorter_search_quick">
		<div class="sorter_nav">
			<select name="sort_by" id="sort_by">
				<option id="sortPrice" value="<?php echo $unpaged_url."&amp;sort_by=price"; ?>"<?php if ($_REQUEST['sort_by'] == "price") echo " selected";?>>Price</option>
				<option id="sortSuburb" value="<?php echo $unpaged_url. "&amp;sort_by=suburb"; ?>"<?php if ($_REQUEST['sort_by'] == "suburb") echo " selected";?>>Suburb</option>
				<option id="sortDateAdded" value="<?php echo $unpaged_url. "&amp;sort_by=created_at"; ?>"<?php if ($_REQUEST['sort_by'] == "created_at") echo " selected";?>>Date Added</option>
				<option  id="sortYield" value="<?php echo $unpaged_url. "&amp;sort_by=yield"; ?>"<?php if ($_REQUEST['sort_by'] == "yield") echo " selected";?>>Yield</option>
			</select>
			 <p style="display:none;"><span>Sort by:</span>
				<a class="sortPrice" href="<?php echo $unpaged_url . "&amp;sort_by=price"; ?>">Price</a>
				<a class="sortSuburb" href="<?php echo $unpaged_url . "&amp;sort_by=suburb"; ?>">Suburb</a>
				<a class="sortDateAdded"  href="<?php echo $unpaged_url . "&amp;sort_by=created_at"; ?>">Date Added</a>
				<a class="sortYield"  href="<?php echo $unpaged_url . "&amp;sort_by=yield"; ?>">Yield</a>
			</p>
			</div>		
	</div><!-- end .sorter_search_quick -->
</form>	
<div class="clearer"></div>
<?php
if(!empty($pre_results)) :
	echo $pre_results;
	echo "</div> <!-- #sorter_pagination -->";
endif;
?><?php
require_once("fav_results.php"); // Must replace the fav_results display...
if(!empty($post_results)) :
	echo "<div id='bottom_pagination'>";
	echo $post_results;
endif;
?>
</div> <!-- #sorter_pagination or #bottom_pagination -->
</div>