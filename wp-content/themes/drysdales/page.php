<?php
global $options;
foreach ($options as $value) {
    if (get_option($value['id']) === FALSE) {
        $$value['id'] = $value['std'];
    } else {
        $$value['id'] = get_option($value['id']);
    }
}
get_header();
?>

<?php
if (is_page('home')) {
    $images = get_option('homepage_images');
    ?>
    <div id="content">
        
        <div class="block home-image">
            <div class="wrap">
                <div class="slideshow">
                    <?php
                    if (!empty($images)) {
                        $photos_upload_url = get_option('siteurl') . '/wp-content/uploads/slideshow/';
                        $desc = get_option('blogname') . " - " . get_option('blogdescription');
                        foreach ($images as $image) {
                            if (!empty($image['large'])) {
                                ?>
                                <img src="<?php echo $photos_upload_url . $image['large']; ?>" alt="<?php echo $desc; ?>" title="<?php echo $desc; ?>" />	
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Main')) {
            
        }
        ?>

        <div class="block home-text">

            <div class="company-profile">
                <h4 class="title">Company Profile</h4>
                <div class="wrap">
                    <?php echo $company_profile; ?>
                    <p><a href="<?php echo $company_profile_link; ?>" title="Read more">Read more &raquo;</a></p>
                </div>
            </div>
            <div class="news-media">
                <h4 class="title">News &amp; Media</h4>
                <div class="wrap">
                    <ul id="news_media">
                        <?php
                        $args = array('category' => 42, 'posts_per_page' => 6);
                        $lastposts = get_posts($args);
                        //$lastposts = get_posts('numberposts=6&cat42');
                        foreach ($lastposts as $post) {
                            setup_postdata($post);
                            ?>
                            <li>
                                <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>"><?php the_title(); ?></a>
                                <span class="post_date"><?php the_time('jS F, Y') ?></span>
                                <div class="clear"></div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <p><a href="<?php echo site_url(); ?>/news" title="More from our Blog">More from our Blog &raquo;</a></p>
                </div>
            </div>

        </div>
    </div>

    <?php
} // END HOME
else {
    ?>

    <div id="content">
        <div class="default-post">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="post" id="post-<?php the_ID(); ?>">
                       <?php // <h1 class="page-title"><?php the_title(); </h1>?>
                        <div class="entry"><?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?></div>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>

    <?php
}
// get_sidebar();
get_footer();
?>
