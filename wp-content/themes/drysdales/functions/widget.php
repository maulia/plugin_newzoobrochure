<?php
add_filter('dynamic_sidebar_params', 'my_widget_class');
function my_widget_class($params) {
	global $widget_num;

	// Widget class
	$class = array();
	$class[] = 'widget';

	// Iterated class
	$widget_num++;
	$class[] = 'widget-' . $widget_num;

	// Alt class
	if ($widget_num % 2) :
		$class[] = '';
	else :
		$class[] = 'alt';
	endif;

	// Join the classes in the array
	$class = join(' ', $class);

	// Interpolate the 'my_widget_class' placeholder
	$params[0]['before_widget'] = str_replace('my_widget_class', $class, $params[0]['before_widget']);
	return $params;
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Main', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="block content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n",
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Sidebar', // goes on the Search Results pages, perhaps in addition to the Static Sidebar
	'before_widget' => "\n".'<div id="%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n".'</div>',
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n".'<div class="wrap">',
	));
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Main Left', // main content area of the Property page
	'before_widget' => "\n".'<div id="%1$s" class="%2$s block my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Main Right', // goes on the Search Results pages, perhaps in addition to the Static Sidebar
	'before_widget' => "\n".'<div id="%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>',
	));
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Quick Search Sidebar', 
	'before_widget' => '<div id="%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n".'</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n".'<div class="wrap">',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Blog Sidebar', 
	'before_widget' => "\n".'<div id="side_%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n".'</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n".'<div class="wrap">',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Static Sidebar', 
	'before_widget' => "\n".'<div id="side_%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n",
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Tag Search', 
	'before_widget' => "\n".'<div id="side_%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n",
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Feature Slider', // goes on the Search Results pages, perhaps in addition to the Static Sidebar
	'before_widget' => "\n".'<div id="%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n".'</div>',
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n".'<div class="wrap">',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Map and Walkability',
	'before_widget' => "\n".'<div id="%1$s" class="block side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n".'</div>',
	'before_title' => '<h4 class="title">',
	'after_title' => '</h4>'."\n".'<div class="wrap">',
	));
}

?>
