<?php get_header(); ?>
<div id="content">
<div id="blog-div" class="archive">

		<?php if (have_posts()) : ?>

 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
 	  <?php /* If this is a category archive */ if (is_category()) { ?>
		<h2 class="section_title">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>
 	  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<h2 class="section_title">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="section_title">Archive for <?php the_time('F jS, Y'); ?></h2>
 	  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="section_title">Archive for <?php the_time('F, Y'); ?></h2>
 	  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="section_title">Archive for <?php the_time('Y'); ?></h2>
	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="section_title">Author Archive</h2>
 	  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="section_title">Blog Archives</h2>
 	  <?php } ?>

		<?php while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<?php if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; } ?>
				<div class="post_title_date<?php if(!function_exists('userphoto_the_author_thumbnail')) { echo ' no_author_thumb'; } ?>">
					<h2 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<p class="postdate"><?php the_time('jS F, Y') ?><?php the_tags( ' | <span class="tagged">Tagged: ', ', ', '</span>'); ?></p>
				</div>
				<?php $category = get_the_category(); $category_link = get_category_link( $category[0]->cat_ID ); ?><p class="flag cat-<?php echo $category[0]->category_nicename; ?>"><span class="filed_cats"><a href="<?php echo $category_link; ?>" title="<?php echo $category[0]->cat_name; ?>"><?php echo $category[0]->cat_name; ?></a></span></p>

				<div class="entry">
					<?php the_excerpt_reloaded(30, '<a>', 'excerpt', TRUE, 'more &#187;'); ?>
				</div>

			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<p class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></p>
			<p class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
		</div>
	<?php else :

		if ( is_category() ) { // If this is a category archive
			printf("<h5 class='center'>Sorry, but there aren't any posts in the %s category yet.</h5>", single_cat_title('',false));
		} else if ( is_date() ) { // If this is a date archive
			echo("<h5>Sorry, but there aren't any posts with this date.</h5>");
		} else if ( is_tag() ) { // If this is a tag archive
			echo("<h5>Sorry, but there aren't any posts with this tag.</h5>");
		} else if ( is_author() ) { // If this is a category archive
			$userdata = get_userdatabylogin(get_query_var('author_name'));
			printf("<h5 class='center'>Sorry, but there aren't any posts by %s yet.</h5>", $userdata->display_name);
		} else {
			echo("<h5 class='center'>No posts found.</h5>");
		}
		get_search_form();

	endif;
?>

	</div><!-- end #blog-div -->
	</div><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
