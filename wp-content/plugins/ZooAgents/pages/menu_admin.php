<div class="dashboard-subnav">
	<div class="nav-sites">     
		<ul>
			<li <?php if(is_page('setting')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl."dashboard/setting/"; ?>">Setting</a></li>
			<li <?php if(is_page('agent-accounts')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl."dashboard/agent-accounts/"; ?>">Agent Accounts</a></li>
			<li <?php if(is_page('property-payment')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl."dashboard/property-payment/"; ?>">Property Payment</a></li>
			<li <?php if(is_page('search-listings')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl."dashboard/search-listings/"; ?>">Search Listings</a></li>
			<li><a href="<?php echo $realty->siteUrl."wp-admin/"; ?>">WP Admin</a></li>
		</ul>
	</div>
    <div class="clear"></div>
</div>