<?php
require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/wp-load.php');
if (!empty($_FILES)) {
	$photos_upload_url=get_option('siteurl').'/wp-content/uploads/slideshow/';

	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_REQUEST['folder'];
	
	extract(pathinfo( $_FILES['Filedata']['name']));
	if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
	$filename = strtolower (preg_replace("/[^a-z0-9-]/", "-", $filename));
	$basename = "$filename.$extension";
	$final_filename = $targetPath . $basename;

	for ($count = 1; file_exists( $final_filename ); $count++): 
		$basename = $filename . "_$count" .  '.' . $extension;
		$final_filename = $targetPath . $basename;	 		
	endfor;	

	if(save_file($tempFile, $final_filename)){			
		resize_image($final_filename, 200, 'thumbnail');		
		
		$photo = array();
		$photo['thumbnail']=$filename."_thumb.$extension";
		$photo['large']=$filename.".$extension";		
		
		$photos=get_option('homepage_images');
		if(!is_array($photos))$photos=array($photos);		

		array_push($photos,$photo);
		
		if(!update_option('homepage_images',$photos))add_option('homepage_images',$photos);
	}		
	echo $final_filename;
}

if(!function_exists('save_file')){
function save_file($url, $file){
	if(!@file_exists(dirname($file)))
		@mkdir(dirname($file), 0777);
	else{
		if(@file_exists($file)){
			@chmod($file);
			@unlink($file);
		}
	}
	
	$content = @file_get_contents($url);
	if($content)
		$result = @file_put_contents($file, $content);
	else
		$result = false;
	
	if($result)
		@chmod($file, 0777);
	
	return $result;
}
}

function get_image_thumbnail($image_path){ 
	extract(pathinfo($image_path));
	if(!isset($filename))$filename=str_replace(".".$extension,"",$basename);
	return "$dirname/$filename" ."_thumb.$extension";	
}
	
function resize_image($original,$new_width, $type='thumbnail') {
	if($type=='thumbnail')$thumbnail = get_image_thumbnail($original); 
	$extension = pathinfo($thumbnail, PATHINFO_EXTENSION);// get information about the original file
	switch ( strtolower ( $extension ) ):
		case 'jpg':
		case 'jpeg':
			$img  = @imagecreatefromjpeg( $original );
			break;
		case 'gif':
			$img  = @imagecreatefromgif( $original );
			break;
		case 'png':
			$img  = @imagecreatefrompng( $original );
			break;
		default:
			return;
		break;
	endswitch;

	$ratio = $new_width / imagesx($img ); // So that the image's height is resized proportionally to the width.
	$new_height = (imagesy($img)) * $ratio;		
	$dstim = @imagecreatetruecolor($new_width, $new_height);	
	imagecopyresized($dstim, $img, 0, 0, 0, 0, $new_width, $new_height, imagesx($img), imagesy($img));
			
	switch ( strtolower ( $extension ) ):
		case 'jpg':
		case 'jpeg':
			imagejpeg($dstim, $thumbnail );
			break;
		case 'gif':
			imagegif($dstim, $thumbnail );
			break;
		case 'png':
			imagepng($dstim, $thumbnail );
			break;
		default:
			break;
	endswitch;	
	//chmod( $imgFilenameThumb , 0777 );		
} // end function
?>