<div class="page_toolbar">
<?php if ( $prev_page >= 1 ): ?>
	<p class="page_prev"><a class="prev_page_link" href="<?php echo $this->replacePageLabel($prev_page); ?>" title="Previous Page">&laquo;</a></p>
<?php else : ?><!--Previous Page-->
<?php endif; ?>
	<p class="page_numbers">
		<?php for ( $counter=1; $counter <= $max_page_links && $page_link<=$total_pages; ++$counter,++$page_link ): ?>
		<a href="<?php echo $this->replacePageLabel($page_link); ?>" class="page_link page_link<?php echo $page_link; ?><?php if($page == $page_link) echo ' current_page_link'; if($page == "" and $page_link=='1') echo ' current_page_link'; ?>" title="Page <?php echo $page_link; ?>">
			<?php echo $page_link; ?>
		</a>
		<?php endfor; ?>
	</p>
<?php if(empty($page))$page='1';if($page < $total_pages): ?>
	<p class="page_next"><a class="next_page_link" href="<?php echo $this->replacePageLabel($next_page); ?>"  title="Next Page">&raquo;</a></p>
<?php else : ?><!--Next Page-->
<?php endif; ?>
<div class="clearer"></div>
</div>
<div class="clear"></div>