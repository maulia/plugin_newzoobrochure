<?php 

$items = array (
'street_address','type', 'price', 'bond', 'tax_rate', 'condo_strata_fee', 'state', 'town_village', 'suburb', 'postcode', 'property_type', 'bedrooms', 'bathrooms', 'carspaces', 'land_size', 'building_size', 'available_at','sold_at','sold_price','leased_at','leased_price','auction_date','auction_time','auction_place', 'energy_efficiency_rating', 'ext_link_1', 'ext_link_2','property_url','virtual_tour','year_built','zoning', 'floorplans', 'brochure'
);
global $helper;
foreach ($items as $key=>$item):
if(is_array($item)): ?>
<label><?php echo $helper->humanize($key); ?></label>
<p><input value="<?php echo (empty($settings[$key]['title']))? $helper->humanize($key):$settings[$key]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $key; ?>][title]" type="text">
<?php foreach ($item as $subitem): ?>
<br /><input value="<?php echo $subitem; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $key; ?>][display][]" type="checkbox"<?php if(is_array($settings[$key]['display']) and in_array($subitem, $settings[$key]['display'])) echo ' checked="checked"'; ?>><label> <?php echo $helper->humanize($subitem); ?></label> 
<?php endforeach;
else: ?>
<label><?php echo $helper->humanize($item); ?></label> 
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][display]" type="checkbox"<?php if($settings[$item]['display']) echo ' checked="checked"'; ?>> 
<input value="<?php echo (empty($settings[$item]['title']) || $settings[$item]['title'] =="1")? $helper->humanize($item):$settings[$item]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][title]" type="text"></p>
<?php endif; endforeach; ?>