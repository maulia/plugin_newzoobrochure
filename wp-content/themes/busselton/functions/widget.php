<?php
add_filter('dynamic_sidebar_params', 'my_widget_class');
function my_widget_class($params) {
	global $widget_num;

	// Widget class
	$class = array();
	$class[] = 'widget';

	// Iterated class
	$widget_num++;
	$class[] = 'widget-' . $widget_num;

	// Alt class
	if ($widget_num % 2) :
		$class[] = '';
	else :
		$class[] = 'alt';
	endif;

	// Join the classes in the array
	$class = join(' ', $class);

	// Interpolate the 'my_widget_class' placeholder
	$params[0]['before_widget'] = str_replace('my_widget_class', $class, $params[0]['before_widget']);
	return $params;
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Top', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Middle', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Bottom', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Main', // main content area of the Property page
	'before_widget' => "\n".'<div id="%1$s" class="property_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .property_block -->'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Side', // sidebar content for Property page
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .side_block -->'."\n",
	'before_title' => '<h3>',
	'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Tag Search', // goes on the Search Results pages, perhaps in addition to the Static Sidebar
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .side_block -->'."\n",
	'before_title' => '<h3>',
	'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Static Sidebar', //goes on every page except home and blog
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .side_block -->'."\n",
	'before_title' => '<h3>',
	'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Blog Sidebar', // guess what this is for!!
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div></div><!-- end .side_block -->'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2><div class="block_content">',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Media', // media content area of the Property page
	'before_widget' => "\n".'<div id="%1$s" class="property_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .property_block -->'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}

?>