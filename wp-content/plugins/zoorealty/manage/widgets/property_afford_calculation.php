<p><label>Title</label> <br><input value="<?php echo $settings['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Title]" type="text"></p>
<p><label>Deposit (number only 0-100)</label> <br><input value="<?php echo $settings['deposit']; ?>" name="realty_widget_<?php echo $caption; ?>[deposit]" type="text"></p>
<p><label>Term (year)</label> <br><input value="<?php echo $settings['term']; ?>" name="realty_widget_<?php echo $caption; ?>[term]" type="text"></p>
<p><label>Interest Rate (number only 0-100)</label><br /><input value="<?php echo $settings['interest']; ?>" name="realty_widget_<?php echo $caption; ?>[interest]" type="text"></p>
<p><label>Free text</label><br /><textarea name="realty_widget_<?php echo $caption; ?>[text]"><?php echo $settings['text']; ?></textarea></p>