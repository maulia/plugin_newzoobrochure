<div class="lightbox-wrap">
    <h2>Email to a friend</h2>
    <div id="return" class="center"></div>
    <ol class="cf-ol">
        <li><label><span>Your First Name <span class="red">*</span> </span></label><input type="text" size="37" name="first_name" id="first_name"/></li>
        <li><label><span>Your Last Name</span></label><input type="text"  size="37" name="last_name" id="last_name" /></li>
        <li><label><span>Your Email <span class="red">*</span> </span></label><input type="text" size="37" name="from" id="from" size="45" /></li>
        <li><label><span>Friend's First Name <span class="red">*</span> </span></label><input type="text" size="37" name="first_name1" id="first_name1"/></li>
        <li><label><span>Friend's Last Name</span></label><input type="text"  size="37" name="last_name1" id="last_name1" /></li>
        <li><label><span>Friend's Email <span class="red">*</span> </span></label><input type="text" size="37" name="email" id="email" size="45" /></li>
        <li><label><span>Comments:</span></label><textarea id="comments" name="comments" cols="35" rows="3"></textarea></li>
        <li><label>&nbsp;</label><input type="button" class="btn" value="Send" onClick="send_email();"><div class="clear"></div></li>
    </ol>
    <input type="hidden" id="property_id" value="<?php echo $_GET['id']; ?>" />
</div>