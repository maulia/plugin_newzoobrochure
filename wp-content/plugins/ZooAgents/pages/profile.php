<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_condition="and office_id in ($office_id)";
$office=$wpdb->get_results("select * from office where id in ($office_id)", ARRAY_A);
$office=$office[0];

if(!$office)return;

$uploaded_path=ABSPATH . 'wp-content/uploads/';
$uploaded_url=$realty->siteUrl.'wp-content/uploads/';
$logos_upload_path=$uploaded_path.'office_logos/';
$logos_upload_url=$uploaded_url.'office_logos/';

if(!@file_exists($logos_upload_path)){ @mkdir($logos_upload_path, 0777); $this->authorized_dir($logos_upload_path); }

if(isset($_POST['submit'])){
	$_POST['name']=$_POST['office_name'];
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'name':case 'email':
					 $return.=$helper->humanize($key)." can not be empty.<br/>"; break;					
				break;
			}
		}		
	}
	
	if($_POST["password"] != $_POST["retypepassword"])
		$return.="Password does not match.<br/>"; 
	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else{
		$userfile_name = $_FILES['logo']['name']; 
		$userfile_tmp = $_FILES['logo']['tmp_name']; 
		if(!empty($userfile_tmp)){
			$logo=$userfile_name;
			$logos_upload = $logos_upload_path.$_POST['id'].'/';
			$this->remove_folder_contents($logos_upload);
			if(!$this->save_file($userfile_tmp, $logos_upload.$logo))$return.="Logo can not be saved. Please try again.<br/>";
			else $_POST['logo1']=$logos_upload_url.$_POST['id']."/$logo";
		}		
		
		//update password		
		if($_POST["password"] != "")		
		{
			//$helper->query("update wp_users set user_pass = MD5('".$_POST["password"]."') where id = ".$current_user->ID);		
			$wpdb->query("update agent_account set password = '".$_POST["password"]."' where id = ".$office['wp_user_id']);		
		}
		
		if($this->update_form( "office", $_POST ))$return.= 'Profile successfully updated';
		else $return.= 'Profile could not be updated. please try again later';
		echo "<div id='success'><span>$return</span></div>";
	}
	$office=$_POST;	
}
?>

<form name="update_properties" id="update_properties" class="form-profile" action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="content-left-column">
		<div class="block_content profile-block">
        	<h4>Your Profile</h4>
			<input type="hidden" name="id"  value="<?php echo $office['id']; ?>">		
			<table>
				<tr>
					<td class="label"><strong>Account ID</strong></td>
					<td><strong><?php echo $office_id; ?></strong></td>
				</tr>
				<tr>
					<td class="label"><strong>Name<span class="red"> *</span></strong></td>
					<td>
						<input type="text" class="full" id="name" name="office_name"  value="<?php echo $office['name']; ?>">						
					</td>
				</tr>
				<tr>
					<td class="label"><strong>State</strong></td>
					<td>
						<input type="text" class="full" id="state" name="state" value="<?php echo $office['state']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Suburb</strong></td>
					<td>
						<input type="text" class="full" id="suburb" name="suburb"  value="<?php echo $office['suburb']; ?>">
					</td>
				</tr>				
				<tr>
					<td class="label"><strong>Postcode</strong></td>
					<td>
						<input type="text" class="full" id="postcode" name="zipcode"  value="<?php echo $office['zipcode']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Country</strong></td>
					<td>
						<input type="text" class="full" id="country" name="country"  value="<?php echo $office['country']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label">Unit Number</td>
					<td><input type="text" name="unit_number" class="full"  value="<?php echo $office['unit_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Street Number</td>
					<td><input type="text" id="street_number" name="street_number" class="full"  value="<?php echo $office['street_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Street Name/Type</td>
					<td><input type="text" id="street" name="street" class="full"  value="<?php echo $office['street']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Email<span class="red"> *</span></strong></td>
					<td>
						<input type="text" class="full" id="email" name="email"  value="<?php echo $office['email']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label">Phone</td>
					<td><input type="text" name="phone" class="full"  value="<?php echo $office['phone']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Fax</td>
					<td><input type="text" id="fax" name="fax" class="full"  value="<?php echo $office['fax']; ?>"></td>
				</tr>
				<?php /*<tr>
					<td class="label"><strong>Zoo Office ID</strong></td>
					<td>
						<input type="text" class="full" id="extra_office_id" name="extra_office_id" value="<?php echo $office['extra_office_id']; ?>">
					</td>
				</tr>*/ ?>
			</table>
			 <div class="clear"></div>
		</div>
        
        <div class="block_content">
			<h4>Change Password</h4>
			<table>
				<tr>
					<td class="label"><strong>New Password</strong></td>
					<td>
						<input type="password" class="full" id="password" name="password"  value="">						
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Retype New Password</strong></td>
					<td>
						<input type="password" class="full" id="retypepassword" name="retypepassword" value="">
					</td>
				</tr>
			</table>
        </div>
        
        <div class="block_content profile-logo">
			<h4>Logo</h4>
			<?php if(!empty($office['logo1'])){ ?>
                <p class="logo-image"><img src="<?php echo $office['logo1']; ?>" class="logo"><input type="hidden" value="<?php echo $office['logo1']; ?>" name="logo1"></p>
			<?php } ?>
				<p class="logo-button"><input type="file" name="logo" value="" size="56"></p>
		</div>
		
	</div>

	<div class="content-right-column">
		<div id="map_div" class="block_content">
			<h4>Mapping Coordinates</h4>
			<p><strong><a class="submit_geocode" href="javascript:submit_geocode();">Set Map Location</a></strong></p>
			<p>
				<strong>Longitude:<span id="longitude_span" class="value"><?php echo ($office['longitude']=='')?'&nbsp':$office['longitude']; ?></span></strong>
				<strong>Latitude:<span id="latitude_span" class="value"><?php echo ($office['latitude']=='')?'&nbsp':$office['latitude']; ?></span></strong>
				<input type="hidden" name="longitude" id="longitude" value="<?php echo $office['longitude']; ?>">
				<input type="hidden" name="latitude" id="latitude" value="<?php echo $office['latitude']; ?>">
			</p>
			<div class="map-wrapper">
                <div id="map_canvas" style="width:450px !important;height:350px !important;"></div>
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <?php if(!empty($office['latitude'])){ ?>
                    <script type="text/javascript"> jQuery(document).ready(function () { load(); });	</script>
                <?php } ?>
			</div>
		</div>
        
        <div class="block_content profile-save">
        	<h4>Save Changes</h4>
        	<p class="button"><input type="submit" name="submit" value="Save" class="btn"></p>
        </div>
        
	</div>
    
    <div class="clear"></div>
    	
</form>
<script type="text/javascript">
/* map */
function submit_geocode(){
	var geocoder = new google.maps.Geocoder();
	var street_number=jQuery('#street_number').val();
	var street=jQuery('#street').val();
	var suburb=jQuery('#suburb').val();
	var state=jQuery('#state').val();
	var postcode=jQuery('#postcode').val();				
	var address = "";

	if(street_number!='')address=street_number+" ";
	if(street!='')address=address+street+" ";
	if(suburb!='')address=address+suburb+" ";
	if(state!='')address=address+state+" ";
	if(postcode!='')address=address+postcode+" ";
	address=address.substr(0,address.length-1); 

	geocoder.geocode( { 'address': address}, function(results, status) {

	if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();
		document.getElementById('latitude').value=latitude;
		document.getElementById('latitude_span').innerHTML=latitude;
		document.getElementById('longitude').value=longitude;
		document.getElementById('longitude_span').innerHTML=longitude;
		load();
	}
	else 
	{
		alert('cannot allocate this address.');
	}
		
	}); 
}
var myLatLng;
function load() {
	var latitude = jQuery('#latitude').val();
	var longitude = jQuery('#longitude').val();
	myLatLng = new google.maps.LatLng(latitude, longitude);
	var myOptions = {
		zoom: 14,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	var marker = new google.maps.Marker({draggable:true,position: myLatLng, map: map});
	google.maps.event.addListener(marker, 'dragend', function() 
	{
		updateMarker(marker.getPosition());
	});

}

function updateMarker(pos){
   geocoder = new google.maps.Geocoder();
   geocoder.geocode
	({
		latLng: pos
	}, 
		function(results, status) 
		{
			if (status == google.maps.GeocoderStatus.OK) 
			{
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				document.getElementById('latitude').value = latitude;
				document.getElementById('latitude_span').innerHTML = latitude;
				document.getElementById('longitude').value = longitude;
				document.getElementById('longitude_span').innerHTML = longitude;
			} 
			else 
			{
				//alert('Cannot determine address at this location.');
				alert('cannot allocate this address.');
			}
		}
	);
}

</script>