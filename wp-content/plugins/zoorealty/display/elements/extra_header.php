<?php global $helper; 
$postcode = $this->property['postcode'];
$region = $this->property['town_village'];
$headline = $this->property['headline'];
$desc = $this->property['description'];
$property_type = ucwords($this->property['property_type']);
$state = $this->property['state'];
$suburb = $this->property['suburb'];

$type = strtolower($this->property['type']);
			$deal_type = strtolower($this->property['deal_type']);
			if(strpos($type,'sale')!==false)$listing_type='For Sale';
			else if(strpos($type,'lease')!==false)$listing_type='For Lease';
			else if(strpos($type,'commercial')!==false && $deal_type=='sale')$listing_type='For Commercial Sale';
			else if(strpos($type,'commercial')!==false && $deal_type=='lease')$listing_type='For Commercial Lease';
			else if(strpos($type,'commercial')!==false && $deal_type=='both')$listing_type='For Commercial Sale & Lease';
			else if(strpos($type,'development')!==false)$listing_type='New Development';
			
$address = $this->property['street_address'] ." - ". $this->property['suburb']." , ". $this->property['state'];
$description= get_option('realty_property_meta_description');
$description = str_replace("%address%",$address,$description);
$description = str_replace("%postcode%",$postcode,$description);
$description = str_replace("%region%",$region,$description);
$description = str_replace("%headline%",$headline,$description);
$description = str_replace("%description%",$desc,$description);
$description = str_replace("%property_type%",$property_type,$description);
$description = str_replace("%state%",$state,$description);
$description = str_replace("%suburb%",$suburb,$description);
$description = str_replace("%listing_type%",$listing_type,$description);
$description = str_replace("+","",$description);
$keywords= get_option('realty_property_meta_keywords');
$keywords = str_replace("%address%",$address,$keywords);
$keywords = str_replace("%postcode%",$postcode,$keywords);
$keywords = str_replace("%region%",$region,$keywords);
$keywords = str_replace("%headline%",$headline,$keywords);
$keywords = str_replace("%description%",$desc,$keywords);
$keywords = str_replace("%property_type%",$property_type,$keywords);
$keywords = str_replace("%state%",$state,$keywords);
$keywords = str_replace("%suburb%",$suburb,$keywords);
$keywords = str_replace("%listing_type%",$listing_type,$keywords);
$keywords = str_replace("+","",$keywords);
$other=get_option('realty_property_meta_other');
$number_of_desc=get_option('realty_property_meta_num_of_desc');
?>
<meta name="description" content="<?php echo htmlspecialchars($helper->_substr($description, $number_of_desc)) ?>" />
<meta name="keywords" content="<?php echo htmlspecialchars($keywords);	?>" />
<?php echo $other; ?>


<?php
//cookie for favourite widget
if(!$realty->is_property_page && !isset($_COOKIE["fav_visit"])) 
	setcookie("fav_visit" ,'true', time()+(60*60*24*42), COOKIEPATH, COOKIE_DOMAIN,0,1);
?>