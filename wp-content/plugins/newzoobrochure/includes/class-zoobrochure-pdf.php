<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * Main parent zoobrochure PDF Lib
 * 
 * @author JONY <jony@agentpoint.com.au>
 * @package zoobrochure
 * @subpackage zoobrochure_pdf
 * @since 2.0.0
 */
class zoobrochure_pdf extends FPDF_Curve {
    
    /**
     * Hook for the Header
     * 
     * @since 2.0.0
     */
    public function Header() {
        do_action('zoobrochure_pdf_header');
    }
    
    /**
     * Hook for the Footer
     * 
     * @since 2.0.0
     */
    public function Footer() {
        do_action('zoobrochure_pdf_footer');
    }

    /**
     * Convert URL to Image PNG QRCode
     * 
     * @param string $url URL will be save as content
     * @return string Path that QRCode image located
     */
    public function GetQRCode($url = '') {
        $filename = sprintf('qrcode_%s.png', md5($url));
        $path = $this->get_tmp_dir() . $filename;
        if (!file_exists($path)) {
            QRcode::png($url, $path);
        }
        return $path;
    }

    /**
     * Produce height of paper size
     * 
     * @return int Height of paper
     */
    public function GetPaperHeight() {
        return $this->h;
    }

    /**
     * Produce height of paper size
     * 
     * @return int Width of paper
     */
    public function GetPaperWidth() {
        return $this->w;
    }

    /**
     * Return the name of Image type
     * 
     * @param string $file Url or path of Image
     * @return string Image type such as 'png,jpg,gif and etc'
     */
    public function GetImageType($file = '') {
        $type = image_type_to_extension(exif_imagetype($file));
        if (preg_match('/jpg|jpeg/i', $type)) {
            $type = 'jpg';
        }
        return str_replace('.', '', strtolower($type));
    }

    /**
     * Override the Image method to can handle many image file type
     * 
     * @param string $file Image source or url
     * @param int $x X position of image
     * @param int $y Y position of image
     * @param int $w Width of image
     * @param int $h Height of image
     * @param string $type Kind of image such as 'png,jpeg and etc'
     * @param string $link URL that use to applied on image, when user click it then redirected to URL/Link 
     * @params array $args Options as array that use to setup maximum width+heigth and etc
     * @return boolean|void
     */
    public function Image($file = '', $x = 0, $y = 0, $w = 0, $h = 0, $type = '', $link = '', $args = array()) {

        // Do nothing if file is empty
        if (empty($file)) {
            return;
        }
        
        // Get Cache file if available 
        $cache_file = $this->Image_Cachefile($file);        
        $file = $cache_file ? $cache_file : $file;

        // Setup the options
        $defaults = array(
            'max_width' => 0,
            'max_height' => 0
        );
        $options = wp_parse_args($args, $defaults);

        // Read the options and count actual size
        $is_max_width = !empty($options['max_width']);
        $is_max_height = !empty($options['max_height']);
        if ($is_max_width || $is_max_height) {
            list($width, $height) = getimagesize($file);
        }

        // Set maximum image width if any
        if ($is_max_width) {
            $max_width = $options['max_width'];
            $width_in_mm = $this->pixelsToMM($width);
            $w = ($width_in_mm > $max_width) ? $max_width : $width_in_mm;
        }

        // Set maximum image height if any
        if ($is_max_height) {
            $max_height = $options['max_height'];
            $height_in_mm = $this->pixelsToMM($height);
            $h = ($height_in_mm > $max_height) ? $max_height : $height_in_mm;
        }

        // Handle PNG image type by read type of Image first
        $type = (empty($type)) ? $this->GetImageType($file) : $type;
        if ($type == 'png') {
            return $this->ImagePngWithAlpha($file, $x, $y, $w, $h, $link);
        }

        // Do default handle
        parent::Image($file, $x, $y, $w, $h, $type, $link);
    }

    /**
     * Convert Hexadecimal number to RGB value as array
     * 
     * @param int $hex Hexadecimal code with/without # symbol
     * @return array Color with produce as array index 0 is RED, index 1 is GREEN and last index is BLUE
     */
    function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);
        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = array($r, $g, $b);
        return $rgb; // returns an array with the rgb values
    }    

    /**
     * Display Property Floorplan with proportional size
     * 
     * @param string $url
     * @param int $x
     * @param int $y
     * @return boolean
     */
    public function Floorplan($url = '', $x = 10, $y = 10, $max = 180, $centre = true) {
        if (empty($url)) {
            return FALSE;
        }
        
        // Get Cache file if available 
        $cache_file = $this->Image_Cachefile($url);        
        $url = $cache_file ? $cache_file : $url;

        // Get actual width;
        list($width) = getimagesize($url);
        $width_in_mm = $this->pixelsToMM($width);

        // Count proportional width
        $w = ($width_in_mm > $max) ? $max : $width_in_mm;

        // Make image centre
        if ($centre) {
            $middle = $this->w / 2;
            $middle_img = $w / 2;
            $x = $middle - $middle_img;
        }

        // Display Image
        $this->AddPage();
        $this->SetXY($x, $y);
        $this->Image($url, $x, $y, $w, 0);
    }

    /**
     * Convert Pixel to Milimeters
     * 
     * @param int $val the number that will convert to mm
     * @return int Milimeters value
     */
    public function pixelsToMM($val) {
        $DPI = 96;
        $MM_IN_INCH = 25.4;
        return $val * $MM_IN_INCH / $DPI;
    }

}
