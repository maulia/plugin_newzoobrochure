<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require($path.'/wp-load.php');
global $realty; 
$settings=$realty->settings['widgets']['map_and_walkscore'];
$properties=$realty->properties(array('id'=> $_REQUEST['property_id'], 'limit'=>1));
$property= $properties[0];
?>
<html>
<body>
<div id="walk_score">
<script type="text/javascript">
/* <![CDATA[ */
  var ws_address = "<?php echo $property['street_address'] . ' ' . $property['suburb'] . ' ' . $property['state'] .' ' . $property['postcode'] . ' Australia'; ?>";
  var ws_width = "<?php echo $settings['width']; ?>";
  var ws_iframe_css ="<?php echo $settings['iframe_css']; ?>";
  var ws_map_frame_color = "<?php echo $settings['map_frame_color']; ?>";
  var ws_score_color = "<?php echo $settings['score_color']; ?>";
  var ws_headline_color = "<?php echo $settings['headline_color']; ?>";
  var ws_category_color = "<?php echo $settings['category_color']; ?>";
  var ws_result_color = "<?php echo $settings['result_color']; ?>";
/* ]]> */
</script>

<script type="text/javascript" src="http://walkscore.com/tile/show-tile.php?wsid=<?php echo $realty->settings['general_settings']['walk_score_key']; ?>"></script>
</div>
</body>
</html>