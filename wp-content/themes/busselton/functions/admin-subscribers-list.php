<form name="form_subscriptions_manager" method="post" action="">
<div class="leftside">	
	<input type="hidden" name="task" id="task" value="form_subscriptions_manager" />	
	<div class="tablenav">
	<?php echo $subscribers['pagination_label']; ?>
	</div>
	<table class="widefat">
	  <thead>
	  <tr>
		<th scope="col"><?php _e('Delete') ?></th>   
		<th scope="col"><?php _e('E-mail') ?></th>
		<th scope="col"><?php _e('Name') ?></th>
		<th scope="col"><?php _e('Mobile') ?></th>
		<th scope="col"><?php _e('Home Phone') ?></th>
		<th scope="col"><?php _e('Date Added') ?></th>
		<th scope="col"><?php _e('Action'); ?></th>
	  </tr>
	  </thead>
	  <tbody id="the-list">
	<?php if(!empty($subscribers)) foreach($subscribers as $item): if(!is_array($item)) continue; ?>
		<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">
		
		<td><input type="checkbox" value="<?php echo $item['subscriber_id']; ?>" name="delete_subscriber_ids[]" /></td>
		<td><a href="mailto:<?php echo $item['email']; ?>" title=""><?php echo $item['email']; ?></a></td>
		<td>	<?php echo $item['first_name'] . ' ' . $item['last_name']; ?></td>
		<td>	<?php echo $item['mobile_phone']; ?></td>
		<td>	<?php echo $item['home_phone']; ?></td>
		<td>	<?php echo date("d-m-Y", strtotime($item['updated_at'])); ?></td>
		<td><a href="themes.php?page=theme-settings&tab=subscribers&action=edit<?php echo "&search_by_alert=$search_by_alert&keywords=$keywords&subscriber_id=".$item['subscriber_id']; ?>">Edit</a></td>
		</tr>
		
	<?php endforeach; ?>
	 </tbody>
	</table>	
</div>
<div id="poststuff">
	<div class="postbox">
		<h3>Delete</h3>
		<div class="inside">
		<input type="submit" class="button-secondary delete" value="Delete" name="" onclick="return confirm('You are about to delete the users checked.\n\'OK\' to delete, \'Cancel\' to stop.' );" />		
		</div>
	</div>
</div>
</form>
	
<div id="poststuff">
	<div class="postbox">
		<h3>Actions</h3>
		<div class="inside">
		<p><a href="themes.php?page=theme-settings&tab=subscribers&action=add_new<?php echo "&search_by_alert=$search_by_alert&keywords=$keywords"; ?>" class="button">Add New</a>
		</p>
		<p><a title="Download subscribers as a CSV file" class="button" href="themes.php?page=theme-settings&tab=subscribers&task=subscriptions_manager_create_csv<?php echo "&search_by_alert=$search_by_alert&keywords=$keywords"; ?>">Download List as a CSV File</a></p>
		</div>
	</div>

	<div class="postbox">
		<h3>Search Subscribers</h3>		
		<div class="inside">
			<form action="" method="get" onsubmit="if(document.getElementById('keywords').value=='Search by keywords')document.getElementById('keywords').value='';">
				<input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
				<input type="hidden" name="tab" value="<?php echo $_GET['tab']; ?>" />
				<input type="hidden" name="search_by_alert" value="<?php echo $_GET['search_by_alert']; ?>" />
				<input type="text" id="keywords" value="<?php echo ($request['keywords']=='')?'Search by keywords':$request['keywords']; ?>" name="keywords"  onmouseout="if(this.value=='') this.value='Search by keywords';" onclick="if(this.value=='Search by keywords') this.value='';" />
				<input type="submit" class="button" value="Go"/>	
			</form>
			<p>
				<select onChange="window.location=this.value;">
					<option value="<?php echo "themes.php?page=theme-settings&tab=subscribers&keywords=$keywords"; ?>">Filter by Alert</option>
					<?php foreach($list_alerts as $key=>$value): ?>
					<option value="<?php echo "themes.php?page=theme-settings&tab=subscribers&search_by_alert=$key&keywords=$keywords"; ?>" <?php if($request['search_by_alert']==$key)echo 'selected="selected"'; ?>><?php echo $value; ?></option>
					<?php endforeach; ?>
				</select>
			</p>							
		</div>	
	</div>
</div>