<div class='infoaddress'><h5><?php echo ($street_address=='')?'no street name provided':$street_address.", ".$suburb.", ".$state;?></h5></div>
<?php echo $property_type." - ".$price ?><br/>
<fieldset>
<ul class="rooms">
<?php if(!empty($bedrooms)){ ?><li class="bedrooms"><span class="room_count"><?php echo $bedrooms ?></span> <span class="room_type"></span></li><?php } ?>
<?php if(!empty($bathrooms)){ ?><li class="bathrooms"><span class="room_count"><?php echo $bathrooms; ?></span><span class="room_type"></span></li><?php } ?>
<?php if(!empty($carspaces)){ ?><li class="carspaces"><span class="room_count"><?php echo $carspaces; ?></span><span class="room_type"></span></li><?php } ?>
</ul>
</fieldset>
<div class='info_img'><img src='<?php echo $thumbnail; ?>' title = "<?php echo $headline; ?>" /><br />
<a href="#map_search_result" onClick="<?php echo $url; ?>">View full listing</a></div>	