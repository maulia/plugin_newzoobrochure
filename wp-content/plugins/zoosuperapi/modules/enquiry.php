<?php

function zoo_send_enquiry()
{
	global $wpdb;
	
	$enquiries = $wpdb->get_results('SELECT property_enquiry_form.*, office.id office_id, office.private_key FROM property_enquiry_form JOIN properties ON property_enquiry_form.property_id = properties.id JOIN office ON office.id = properties.office_id WHERE sent_to_zoo < 1 AND zoo_note_id IS NULL AND private_key IS NOT NULL LIMIT 25', ARRAY_A);

	$sent = 0;
	foreach ($enquiries as $enquiry)
	{
		$enquiry_id = isset($enquiry['enquiry_id']) ? $enquiry['enquiry_id'] : $enquiry['id'];
		$id_field = isset($enquiry['enquiry_id']) ? 'enquiry_id' : 'id';
		
		$customer_name = trim($enquiry['customer_name']);
		$last_space = strrpos($customer_name, ' ');
		if ($last_space === false)
		{
			$first_name = $customer_name;
		} else {
			$first_name = trim(substr($customer_name, 0, $last_space));
			$last_name = trim(substr($customer_name, $last_space));
		}
		
		if (empty($first_name))
			$first_name = '-';
		if (empty($last_name))
			$last_name = '-';
		
		$result = zoo_api2('contacts', array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'token' => $enquiry['private_key'],
			));
		
		if (empty($result) || !isset($result['results']))
			continue;
		
		$contact_id = 0;
		if (empty($result['results']))
		{
			$result = zoo_api2('contacts', array('token' => $enquiry['private_key']), array(
				'office_id' => $enquiry['office_id'],
				'agent_id' => $enquiry['agent_id'],
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $enquiry['email'],
				'contact_number' => $enquiry['phone'],
				));
			
			if (empty($result) || !isset($result['id']))
				continue;
			
			$contact_id = $result['id'];
		} else {
			if (!isset($result['results'][0]['id']))
				continue;
			$contact_id = $result['results'][0]['id'];
		}
		
		if ($contact_id == 0)
			continue;

		$result = zoo_api2('notes', array('token' => $enquiry['private_key']), array(
			'contact_id' => $contact_id,
			'agent_id' => $enquiry['agent_id'],
			'property_id' => $enquiry['property_id'],
			'note_type_id' => 6,
			'note_date' => isset($enquiry['date']) ? $enquiry['date'] : $enquiry['created_at'],
			'description' => $enquiry['message'],
			'source_url' => get_bloginfo('wpurl'),
			'source_name' => get_bloginfo('name'),
			'is_locked' => 1,
			));
		
		if (empty($result) || !isset($result['id']))
			continue;
		
		$wpdb->query("UPDATE property_enquiry_form SET sent_to_zoo = ".time().", zoo_note_id = '".$result['id']."' WHERE $id_field = $enquiry_id");
		$sent++;
	}
	
	return $sent;
}
