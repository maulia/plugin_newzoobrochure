<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;};

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];

$property=$wpdb->get_results("select * from properties where id = $id $extra_condition", ARRAY_A);
$property=$property[0];
if(empty($property))return print "Listing ID $id is not exist";

include('menu_listings.php');

$uploaded_path=ABSPATH . 'wp-content/uploads/';
$uploaded_url=$realty->siteUrl.'wp-content/uploads/';
$photos_upload_path=$uploaded_path."photos/$id/";
$photos_upload_url=$uploaded_url."photos/$id/";
$floorplans_upload_path=$uploaded_path."floorplans/$id/";
$floorplans_upload_url=$uploaded_url."floorplans/$id/";
$brochures_upload_path=$uploaded_path."brochures/$id/";
$brochures_upload_url=$uploaded_url."brochures/$id/";

if(!@file_exists($uploaded_path."photos/"))@mkdir($uploaded_path."photos/", 0777);
if(!@file_exists($uploaded_path."floorplans/"))@mkdir($uploaded_path."floorplans/", 0777);
if(!@file_exists($uploaded_path."brochures/"))@mkdir($uploaded_path."brochures/", 0777);

wp_print_scripts('jquery-ui-core');
wp_print_scripts('jquery-ui-sortable');		
		
if(!@file_exists($photos_upload_path)){ @mkdir($photos_upload_path, 0777); $this->authorized_dir($photos_upload_path); }
if(!@file_exists($floorplans_upload_path)){ @mkdir($floorplans_upload_path, 0777);$this->authorized_dir($floorplans_upload_path); }
if(!@file_exists($brochures_upload_path)){ @mkdir($brochures_upload_path, 0777);$this->authorized_dir($brochures_upload_path); }

if($_GET['task']=='delete' && !empty($id)){
	$row=$wpdb->get_results("select url from attachments where id in(".$_GET['ids'].")", ARRAY_A);	
	if(!empty($row)){
		foreach($row as $key=>$item){
			if($_GET['type']=='photo')$url=$photos_upload_path.str_replace($photos_upload_url,"",$item['url']);
			else if($_GET['type']=='floorplan')$url=$floorplans_upload_path.str_replace($floorplans_upload_url,"",$item['url']);
			else if($_GET['type']=='brochure')$url=$brochures_upload_path.str_replace($brochures_upload_url,"",$item['url']);
			@unlink($url);
		}
	}
	$wpdb->query("delete from attachments where id in(".$_GET['ids'].")");
	$row=$wpdb->get_results("select distinct position from attachments where parent_id=$id and is_user=0 and type='".$_GET['type']."' order by position", ARRAY_A);
	if(!empty($row)){
		foreach($row as $key=>$item){
			$wpdb->query("update attachments set position=$key where parent_id=$id and is_user=0 and position=".$item['position']." and type='".$_GET['type']."'");
		}
	}
	echo "<div id='success'><span>".$_GET['type']." deleted.</span></div>";
}

if(isset($_POST['submit'])){
	if(!empty($_POST['photo_ids'])){
		foreach($_POST['photo_ids'] as $key=>$ids){
			if($ids)$wpdb->query("update attachments set position=$key where id in($ids)");
		}
	}	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else echo "<div id='success'><span>Photos updated</span></div>";
}

if(isset($_POST['submit_floor'])){
	if(!empty($_POST['floorplan_ids'])){
		foreach($_POST['floorplan_ids'] as $key=>$ids){
			if($ids)$wpdb->query("update attachments set position=$key where id in($ids)");
		}
	}	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else echo "<div id='success'><span>Floorplans updated</span></div>";
}

if(isset($_POST['submit_brochure'])){
	if(!empty($_POST['brochure_ids'])){
		foreach($_POST['brochure_ids'] as $key=>$ids){
			if($ids)$wpdb->query("update attachments set position=$key where id in($ids)");
		}
	}	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else echo "<div id='success'><span>Brochures updated</span></div>";
}

$photos=$realty->photos($id,'photos');
$floorplans=$realty->photos($id,'floorplans');
$brochures=$realty->brochure($id);
?>
<link rel="stylesheet" href="<?php echo $this->pluginUrl.'css/prettyPhoto.css'; ?>" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<?php echo $this->pluginUrl.'js/jquery.prettyPhoto.js'; ?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/uploadify.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/swfobject.js"></script>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/jquery.uploadify.v2.1.4.js"></script>
<div class="listing-media">		
	<p>Click, drag and drop images to change the order, please press the `Update Order` button after images have been reorded. To delete an image press the red cross in the corner of the image.</p>	
    <form name="update_properties" id="update_properties" action="<?php echo $realty->siteUrl."dashboard/media/?office_id=$office_id&id=$id";?>" method="post" autocomplete="off" enctype="multipart/form-data">
        <div id="photo_div" class="block_content">
            <h4>Photos</h4>
            <p class="info">File type allowed .jpg.</p>
			<ul class="image_list" id="photo_list">
                <?php 
                if(!empty($photos)){ 
                    foreach($photos as $key=>$photo):
                    $ids=$wpdb->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='photo'", ARRAY_A);
                    $ids=$ids[0]['ids'];
                    ?>
                    <li>			
                        <input type="hidden" name="photo_ids[]" value="<?php echo $ids; ?>">
                        <a class="uploaded-image" href="<?php echo $photo['large']; ?>" rel="prettyPhoto[photo]"><img src="<?php echo $photo['small']; ?>"/></a>
                        <a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=photo&office_id=$office_id&task=delete&id=$id&ids=$ids";?>" title="Delete Image"></a>
                    </li>
                    <?php endforeach; 
                }	?>
            </ul>
            <div class="clear"></div>
			
            <div class="photo-upload-box">
            	<div id="photo_message" class="upload_message">Select some files to upload:</div>
                <div id="photo_queue" class="custom-queue"></div>
                <div class="photo-upload-btn"><input id="photo_upload" type="file" name="Filedata" /></div>
            </div>
			
            <div class="update-order"><input type='submit' name='submit' value='Update Order' class='btn'/></div>
        </div>
    </form>		
    
    <form name="update_properties" id="update_properties" action="<?php echo $realty->siteUrl."dashboard/media/?office_id=$office_id&id=$id";?>" method="post" autocomplete="off" enctype="multipart/form-data">
        <div id="floorplan_div" class="block_content">
            <h4>Floorplans</h4>
            <p class="info">File type allowed .jpg & .gif.</p>
            <ul class="image_list" id="floorplan_list">
                <?php 
                if(!empty($floorplans)){ 
                    foreach($floorplans as $key=>$floorplan):
                    $ids=$wpdb->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='floorplan'", ARRAY_A);
                    $ids=$ids[0]['ids'];
                    ?>
                    <li>			
                        <input type="hidden" name="floorplan_ids[]" value="<?php echo $ids; ?>">
                        <a class="uploaded-image" href="<?php echo $floorplan['large']; ?>" rel="prettyPhoto[floorplan]"><img src="<?php echo $floorplan['small']; ?>"/></a>
                        <a class="delete-image" onClick="return confirm('You are about to delete the this floorplan.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=floorplan&office_id=$office_id&task=delete&id=$id&ids=$ids";?>" title="Delete Image"></a>
                    </li>
                    <?php endforeach; 
                }	?>
            </ul>
            <div class="clear"></div>
            
            <div class="photo-upload-box">
            	<div id="floorplan_message" class="upload_message">Select some files to upload:</div>
                <div id="floorplan_queue" class="custom-queue"></div>
                <div class="photo-upload-btn"><input id="floorplan_upload" type="file" name="Floordata" /></div>
            </div>
			
            <div class="update-order"><input type='submit' name='submit_floor' value='Update Order' class='btn'/></div>
            
        </div>
    </form>
    
    <form name="update_properties" id="update_properties" action="<?php echo $realty->siteUrl."dashboard/media/?office_id=$office_id&id=$id";?>" method="post" autocomplete="off" enctype="multipart/form-data">
        <div id="brochure_div" class="block_content">
            <h4>Brochures</h4>
            <p class="info">File type allowed .pdf.</p>
            <ul class="image_list" id="brochures">
                <?php 
                if(!empty($brochures)){ 
                    foreach($brochures as $key=>$brochure):
                    $ids=$wpdb->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='brochure'", ARRAY_A);
                    $ids=$ids[0]['ids'];
                    ?>
                    <li>						
                        <input type="hidden" name="brochure_ids[]" value="<?php echo $ids; ?>">
                        <a class="uploaded-pdf" href="<?php echo $brochure['url']; ?>" target="blank"><?php echo $brochure['description']; ?></a>
                        <a class="delete-pdf" onClick="return confirm('You are about to delete the this brochure.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=brochure&office_id=$office_id&task=delete&id=$id&ids=$ids";?>">Delete</a>
                    	<div class="clear"></div>
                    </li>
                    <?php endforeach; 
                }	?>
            </ul>
            <div class="clear"></div>
            
            <div class="photo-upload-box">
            	<div id="brochure_message" class="upload_message">Select some files to upload:</div>
                <div id="brochure_queue" class="custom-queue"></div>
                <div class="photo-upload-btn"><input id="brochure_upload" type="file" name="Brochuredata" /></div>
            </div>
			
            <div class="update-order"><input type='submit' name='submit_brochure' value='Update Order' class='btn'/></div>
            
        </div>
    </form>
</div>

<div class="block_content">
	<a href="<?php echo  $realty->siteUrl.'dashboard/opens/?id='.$property['id']."&office_id=".$property['office_id']; ?>" class="button"  id="next_btn">Save & Next</a>
</div>
	
<script type="text/javascript">
var uploader='<?php echo $this->pluginUrl; ?>js/uploadify.swf';
var script_photo='<?php echo $this->pluginUrl; ?>js/uploadify_photo.php';
var script_floorplan='<?php echo $this->pluginUrl; ?>js/uploadify_floorplan.php';
var script_brochure='<?php echo $this->pluginUrl; ?>js/uploadify_brochure.php';
var button_image='<?php echo $this->pluginUrl; ?>images/select_files.png';
var button_width='100';
var button_height='27';
var cancel_image='<?php echo $this->pluginUrl; ?>images/cancel.png';
var photos_upload_path='<?php echo $photos_upload_path; ?>';
var floorplans_upload_path='<?php echo $floorplans_upload_path; ?>';
var brochures_upload_path='<?php echo $brochures_upload_path; ?>';
var load_photo='<?php echo $this->pluginUrl."js/load_photo.php?id=$id&office_id=$office_id";?>';
var load_floorplan='<?php echo $this->pluginUrl."js/load_floorplan.php?id=$id&office_id=$office_id";?>';
var load_brochure='<?php echo $this->pluginUrl."js/load_brochure.php?id=$id&office_id=$office_id";?>';
var id=<?php echo $id; ?>;
</script>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/media.js"></script>