<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path . '/wp-load.php');
global $wpdb, $realty, $helper;

$login = $realty->siteUrl . 'agent-login/';
if(isset($_GET['test']))$row = $wpdb->get_results("select properties.id, office_id, properties.street_number, properties.street, properties.suburb, properties.state, email, name from properties, office where office_id=office.id and properties.id=".$_GET['id'], ARRAY_A); //for testing

if(!isset($_GET['test']))$row = $wpdb->get_results("select properties.id, office_id, properties.street_number, properties.street, properties.suburb, properties.state, email, name from properties, office where feature_at < CURDATE() and feature_at is not null and feature_at not in ('0000-00-00','') and listingpaid='1' and office_id=office.id and listing_type='Featured'", ARRAY_A); // removed Feature after Feature expire at
if ($row)
{
    foreach ($row as $item)
    {
        if(!isset($_GET['test'])){
			$wpdb->query("update extras set featured_property=0 where property_id=" . $item['id']);
			$wpdb->query("update properties set listing_type='Standard' where id=" . $item['id']);
		}
		
		extract($item);
		$full_address='';
		if(!empty($item['street_number']))$full_address.=$item['street_number']." ";
		if(!empty($item['street']))$full_address.=$item['street'].", ";
		if(!empty($item['suburb']))$full_address.=$item['suburb'].", ";
		if(!empty($item['state']))$full_address.=$item['state'];
		$subject='Your Featured listing upgrade has expired!';	
		$url=$realty->siteUrl."$id/";
		$extra_message="
		<br/>Dear $name,<br/><br/>
		This is an email notification to advise that your Featured listing upgrade for ID <a href='$url'>$id</a> has expired.<br/>
		If you would like to renew your upgrade please <a href='$login'>login</a> to your account.<br/><br/><br/>
		Regards,<br/><br/><br/>
		$realty->blogname";
		include(dirname(dirname(__FILE__)).'/email_template/notification.php');  
		if(isset($_GET['email']))$email=$_GET['email'];
		if(!isset($_GET['test']) || isset($_GET['email']))$helper->email($email, $subject, $message, '', '', false);
		else echo "$email<br/>$subject<br/>$message<br/>";
    }
}

if(!isset($_GET['test']))$row = $wpdb->get_results("select properties.id, office_id, properties.street_number, properties.street, properties.suburb, properties.state, email, name from properties, office where premium_at < CURDATE() and premium_at is not null and premium_at not in ('0000-00-00','') and listingpaid='1' and office_id=office.id and listing_type='Premium'", ARRAY_A); // removed Premium after Premium expire at
if ($row)
{
    foreach ($row as $item)
    {
		if(!isset($_GET['test'])){
			$wpdb->query("update extras set featured_property=0 where property_id=" . $item['id']);
			$wpdb->query("update properties set listing_type='Standard' where id=" . $item['id']);
		}
		
		extract($item);
		$full_address='';
		if(!empty($item['street_number']))$full_address.=$item['street_number']." ";
		if(!empty($item['street']))$full_address.=$item['street'].", ";
		if(!empty($item['suburb']))$full_address.=$item['suburb'].", ";
		if(!empty($item['state']))$full_address.=$item['state'];
		$subject='Your Premium listing upgrade has expired!';
		$url=$realty->siteUrl."$id/";		
		$extra_message="
		<br/>Dear $name,<br/><br/>
		This is an email notification to advise that your Premium listing upgrade for ID <a href='$url'>$id</a> has expired.<br/>
		If you would like to renew your upgrade please <a href='$login'>login</a> to your account.<br/><br/><br/>
		Regards,<br/><br/><br/>
		$realty->blogname";
		include(dirname(dirname(__FILE__)).'/email_template/notification.php');
		if(isset($_GET['email']))$email=$_GET['email'];
		if(!isset($_GET['test']) || isset($_GET['email']))$helper->email($email, $subject, $message, '', '', false);
		else echo "$email<br/>$subject<br/>$message<br/>";
    }
}

if(!isset($_GET['test']))$row = $wpdb->get_results("select properties.id, office_id, properties.street_number, properties.street, properties.suburb, properties.state, email, name from properties, office where expire_at < CURDATE() and expire_at is not null and expire_at not in ('0000-00-00','') and listingpaid='1' and office_id=office.id", ARRAY_A); // update listing to Expired, agent can only update the status into withdrawn, sold, leased
if ($row)
{
    foreach ($row as $item)
    {
        if(!isset($_GET['test']))$wpdb->query("update properties set active='Expired', status='8', listingpaid='0', status_access='2,5,6,8' where id =" . $item['id']);
		
		extract($item);
		$full_address='';
		if(!empty($item['street_number']))$full_address.=$item['street_number']." ";
		if(!empty($item['street']))$full_address.=$item['street'].", ";
		if(!empty($item['suburb']))$full_address.=$item['suburb'].", ";
		if(!empty($item['state']))$full_address.=$item['state'];
		$subject='Your listing has expired!';
		$url=$realty->siteUrl."$id/";
		$extra_message="
		<br/>Dear $name,<br/><br/>
		This is an email notification to advise that your listing for ID <a href='$url'>$id</a> has expired.<br/>
		If you would like to renew your listing please <a href='$login'>login</a> to your account.<br/><br/><br/>
		Regards,<br/><br/><br/>
		$realty->blogname";
		include(dirname(dirname(__FILE__)).'/email_template/notification.php');
		if(isset($_GET['email']))$email=$_GET['email'];
		if(!isset($_GET['test']) || isset($_GET['email']))$helper->email($email, $subject, $message, '', '', false);
		else echo "$email<br/>$subject<br/>$message<br/>";
    }
}
?>