<?php
global $wpdb, $realty;
if(isset($_POST['submit'])){
	extract ($_POST);	
	if(empty($officename))$error.="Please fill agency name<br/>";
	if(empty($portal))$error.="Please choose your portal publisher<br/>";
	if(empty($firstname))$error.="Please fill your first name<br/>";
	if(empty($lastname))$error.="Please fill your last name<br/>";
	if(empty($user_email))$error.="Please fill your email<br/>";
	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $user_email))$error.="Invalid email format.<br/>";
	if($_SESSION['security_code'] != $_POST['securitycode'] && !empty($_SESSION['security_code'] ))$error.="Wrong spam code<br/>";
	if( $_SESSION['security_code'] == $_POST['securitycode'] && !empty($_SESSION['security_code'] ) )unset($_SESSION['security_code']);
	if(!empty($error))echo "<p class='error'>$error</p>";
	
	if(empty($error)){
		$_POST['email'] = $user_email;	
		$_POST['ip_address']=$ip_address=esc_sql ($_SERVER['REMOTE_ADDR']);
		$_POST['created_at'] =  date("Y-m-d h:i:s");
		$check_email=$wpdb->get_var("select id from agent_account where email='$user_email'");
		if($check_email)$error.="Email already exist.<br/>";
		
		if(!empty($error))echo "<p class='error'>$error</p>";
		else{
			$_POST['password'] = $password= wp_generate_password(8, false);
		    $this->insert_form('agent_account', $_POST);
		   
		    $id=$wpdb->get_var("select id from agent_account where email='$user_email'");			
			$office_id=$wpdb->get_var("select max(id) from office");
			$_POST['office_id']=$_POST['id']=$office_id=($office_id<100000)?100000:$office_id+1;
			$_POST['name']=$_POST['officename'];
			$_POST['email'] = $user_email;		
			$_POST['contributor']=1;
			$_POST['is_zoo']=0;
			$_POST['display']=1;
			$_POST['wp_user_id']=$id;
			
			$this->insert_form( 'office', $_POST );
			
			$user_id=$wpdb->get_var("select max(id) from users");
			$_POST['id']=$user_id=($user_id<100000)?100000:$user_id+1;
			$this->insert_form( 'users', $_POST );
			
			$settings=get_option('realty_general_settings');
			if(!in_array($_POST['office_id'], $settings['office_id']))array_push($settings['office_id'], $_POST['office_id']);
			update_option('realty_general_settings', $settings);

            $subject = "Thank you for registering with " . $this->blogname;
            $login=$realty->siteUrl.'agent-login/';
			$message="Thanks for signing up to ".$this->blogname.". You can edit your account or create a listing at $login. Below is your login detail:\n\rUsername: $user_email\n\rPassword: $password\n\r\n\rBest Regards,\n\r\n\r".$this->blogname;				
            $this->text_email($user_email, $subject, $message, '', '', false);
				
			$za_email = get_option('za_email');
			if(!empty($za_email)){
				$subject="[".$this->blogname."] new agent account registration";
				$message="Agency Name: $officename\n\rPortal Publisher: $portal\n\r\n\rAddress\n\rUnit Number: $unit_number\n\rStreet Number: $street_number\n\rStreet: $street\n\rSuburb: $suburb\n\rState: $state\n\rPostcode: $zipcode\n\r\n\rContact Person\n\rName: $firstname  $lastname\n\rEmail: $user_email\n\rPhone: $phone\n\r\n\rIp Address: $ip_address\n\r\n\rSent out from ".$realty->siteUrl;
				$this->text_email($za_email, $subject, $message, '', '', false);
			}
			
			echo "<p class='success'>Thank you for your Interest. Our Representative will Contact you</p>";
			$success=1;
			$_POST=array();
		}
		
	}
}
if($success!=1){
?>
<form id="setupform" method="post" action="" onsubmit="return validate_form();" class="form-agent-log">
	<ol class="cf-ol">
		<li><label>Agency name <span class="red">*</span></label> <input name="officename" type="text"  value="<?php echo $_POST['officename']; ?>" maxlength="200" /></li>
		<li><label>Portal Publisher <span class="red">*</span></label>
			<select name="portal">
				<option value="">Please Select</option>
				<option value="MyDesktop" <?php if($_POST['portal']=='MyDesktop')echo 'selected="selected"'; ?>>MyDesktop</option>
				<option value="PortPlus" <?php if($_POST['portal']=='PortPlus')echo 'selected="selected"'; ?>>PortPlus</option>
				<option value="Agent Point" <?php if($_POST['portal']=='Agent Point')echo 'selected="selected"'; ?>>Agent Point</option>
				<option value="Other" <?php if($_POST['portal']=='Other')echo 'selected="selected"'; ?>>Other</option>
			</select>
		</li>
    </ol>
	<h4>Agency Address</h4>
    <ol class="cf-ol">
		<li><label>Unit number</label> <input name="unit_number" type="text"  value="<?php echo $_POST['unit_number']; ?>" maxlength="200" /></li>
		<li><label>Street number<span class="red">*</span></label> <input name="street_number" type="text"  value="<?php echo $_POST['street_number']; ?>" maxlength="200" /></li>
		<li><label>Street<span class="red">*</span></label> <input name="street" type="text"  value="<?php echo $_POST['street']; ?>" maxlength="200" /></li>
		<li><label>Suburb<span class="red">*</span></label> <input name="suburb" type="text"  value="<?php echo $_POST['suburb']; ?>" maxlength="200" /></li>       
		<li><label>State<span class="red">*</span></label> 
            <select name="state" >
                <option value="">Please Select</option>
                <option value="ACT" <?php if($_POST['state']=='ACT')echo 'selected="selected"'; ?>>ACT</option>
                <option value="NSW" <?php if($_POST['state']=='NSW')echo 'selected="selected"'; ?>>NSW</option>
                <option value="NT" <?php if($_POST['state']=='NT')echo 'selected="selected"'; ?>>NT</option>
                <option value="QLD" <?php if($_POST['state']=='QLD')echo 'selected="selected"'; ?>>QLD</option>
                <option value="SA" <?php if($_POST['state']=='SA')echo 'selected="selected"'; ?>>SA</option>
                <option value="TAS" <?php if($_POST['state']=='TAS')echo 'selected="selected"'; ?>>TAS</option>
                <option value="VIC" <?php if($_POST['state']=='VIC')echo 'selected="selected"'; ?>>VIC</option>
                <option value="WA" <?php if($_POST['state']=='WA')echo 'selected="selected"'; ?>>WA</option>
            </select>
		</li>
		<li><label>Postcode<span class="red">*</span></label> <input name="zipcode" type="text"  value="<?php echo $_POST['zipcode']; ?>" maxlength="200" /></li>
    </ol>
    <h4>Contact Person</h4>
    <ol class="cf-ol">
		<li><label>First name <span class="red">*</span></label> <input name="firstname" type="text"  value="<?php echo $_POST['firstname']; ?>" maxlength="200" /></li>
		<li><label>Last name <span class="red">*</span></label> <input name="lastname" type="text"  value="<?php echo $_POST['lastname']; ?>" maxlength="200" /></li>
		<li><label>Email <span class="red">*</span></label> <input name="user_email" type="text"  value="<?php echo $_POST['user_email']; ?>" maxlength="200" /></li>
		<li><label>Telephone<span class="red">*</span></label> <input name="phone" type="text"  value="<?php echo $_POST['phone']; ?>" maxlength="200" /></li>
		<li><label>Spam Code <span class="red">*</span></label><img id="captcha-image" src="<?php echo $this->pluginUrl.'includes/CaptchaSecurityImages.php?width=100&height=40&characters=5'; ?>" alt="Anti-Spam Image" style="vertical-align:top;" /><span class="reload-captcha"><a href="javascript:captchas_image_reload('captcha-image')" class='reload-image' title="Reload Image"><i class="icon-refresh"></i></a></span></li>
		<li><label>Enter the above code</label><input type="text" name="securitycode" id="securitycode" size="20" /></li>
        <li><label>&nbsp;</label><input type="hidden" name="country" value="Australia"><input type="submit" name="submit" class="button" value="Register" /></li>
    </ol>
	<div class="clear"></div>
</form>
	
<script type="text/javascript">
  function captchas_image_reload (imgId) 
  {
	var image_url = document.getElementById(imgId).src;
	image_url+= "&";
	document.getElementById(imgId).src = image_url;
  }
</script>
<?php } ?>