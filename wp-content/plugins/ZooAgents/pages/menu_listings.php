<div class="dashboard-subnav">
    <h4>Edit Listing</h4>
    <ul class="listing-menu">
        <li<?php if(is_page('detail')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/detail/?id='.$property['id']."&type=".$property['type']."&office_id=".$property['office_id'];?>">Detail</a></li>
        <li<?php if(is_page('media')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/media/?id='.$property['id']."&office_id=".$property['office_id']; ?>">Media</a></li>
        <li<?php if(is_page('status')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/status/?id='.$property['id']."&office_id=".$property['office_id']; ?>">Status</a></li>
		<?php if($property['type']!='HolidayLease'){ ?>
        <li<?php if(is_page('opens')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/opens/?id='.$property['id']."&office_id=".$property['office_id']; ?>">Opens</a></li>
        <?php } ?>
		<?php /*if($is_admin==1){ ?>
		<li<?php if(is_page('active')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/active/?id='.$property['id']."&office_id=".$property['office_id']; ?>">Active</a></li>		
		<?php }*/ ?>
		<li<?php if(is_page('payment')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id']; ?>">Payment</a></li>
		<li>
		<form name="preview_form" id="preview_form" action="<?php echo $realty->siteUrl."$id/"; ?>" method="post" target="_blank">
			<input type="hidden" name="preview" value="1">			
			<a href="javascript:document.forms['preview_form'].submit();">Preview</a>
		</form>
		</li>
    </ul>
    <div class="clear"></div>
</div>