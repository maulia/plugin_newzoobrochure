<?php require_once('../../../../../wp-blog-header.php');
$floorplan = $realty->photos($_GET['property_id'], "floorplans", array('800x600'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" title="" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php bloginfo('name'); ?></title>
</head>

<body>
<div id="property_floorplan"><img src="<?php echo $floorplan[$_GET['page']]['large']; ?>" title="" alt="" /></div>
<?php
$paginate_link = $helper->sanitizeUrl('page=all'). '&amp;page=';

if(array_key_exists($_GET['page']+-1,$floorplan)): //If there is a floorplan previous to this in the array?>

<p class="next"><a href="<?php echo $paginate_link . ($_GET['page']-1); ?>" title="Previous">&larr;Previous</a></p>	

<?php endif; 
if(array_key_exists($_GET['page']+1,$floorplan))://If there is a floorplan after this in the array ?>

<p class="next"><a href="<?php echo $paginate_link . ($_GET['page']+1); ?>" title="Next">Next&rarr;</a></p>	

<?php endif; ?>
</body>
</html>