<script type="text/javascript">
//<!--
function open_query_form(element){
	if(element.value != 'sale' && element.value != 'lease')
		return;
	if(element.checked==1)
		document.getElementById(element.value + '_search').style.display='block';
	else
		document.getElementById(element.value + '_search').style.display='none';
}

//-->
</script>
<style type="text/css">
	.select_suburb{
		height:auto !important;
	}
</style>
<form name="" action="" method="post">
	<div class="leftside">	
		<input type="hidden" name="task" value="add_new" />
		<input type="hidden" name="subscriber_id" value="<?php echo $subscriber_id; ?>" />
		<input type="hidden" name="keep_informed" value="<?php echo $keep_informed;?>">
		<table class="widefat maintable" cellspacing="0">
		<thead>
		<tr>
			<th colspan="2" scope="col" class="manage-column column-title">New Subscriber</th>
		</tr>
		</thead>
		<tr class="alternate">
			<th>Title</th>
			<td>
				<select name="title">
					<option value="">Title</option>
					<option value="Miss"<?php if($title=='Miss') echo ' selected="selected"'; ?>>Miss</option>
					<option value="Mr"<?php if($title=='Mr') echo ' selected="selected"'; ?>>Mr</option>
					<option value="Mrs"<?php if($title=='Mrs') echo ' selected="selected"'; ?>>Mrs</option>
					<option value="Ms"<?php if($title=='Ms') echo ' selected="selected"'; ?>>Ms</option>
					<option value="Dr"<?php if($title=='Dr') echo ' selected="selected"'; ?>>Dr</option>
				</select>
			</td>
		</tr>	 
			 
		<tr>
			<th>E-mail *</th>
			<td><input type="text" name="email" size="33" class="searchbox" value="<?php echo $email; ?>" onClick="if(this.value=='enter email address') this.value='';" /></td>
		</tr>

		<tr class="alternate">
			<th>First Name *</th>
			<td><input type="text" name="first_name" size="33" class="searchbox" value="<?php echo $first_name; ?>" /></td>
		</tr>

		<tr>
			<th>Last Name *</th>
			<td><input type="text" name="last_name" size="33" class="searchbox" value="<?php echo $last_name; ?>" /></td>
		</tr>
		
		<tr class="alternate">
			<th>Home Number</th>
			<td><input type="text" name="home_phone" size="33" class="searchbox" value="<?php echo $home_phone; ?>" /></td>
		</tr>
		
		<tr>
			<th>Mobile Number</th>
			<td><input type="text" name="mobile_phone" size="33" class="searchbox" value="<?php echo $mobile_phone; ?>" /></td>
		</tr>
		  
		<tr class="alternate">
			<th>Country</th>
			<td><select id="country" name="country">
					<option value="Australia">Australia</option>
					<?php foreach ($list_countries as $item): ?>	
						<option value="<?php echo $item; ?>" <?php if($item == $country) echo 'selected="selected"'; ?>><?php echo $item; ?></option>
					<?php endforeach; ?>
					</select></td>
		</tr>

		<tr>
			<th>How did you find us?</th>
			<td>		<select name="referrer">
					<option value="">How you heard about us?</option>
				<?php foreach($all_list as $item_value): ?>
					<option value="<?php echo $item_value['name']; ?>"<?php if ($referrer == $item_value['name']) echo 'selected="selected"'; ?>><?php echo $item_value['name']; ?></option>
				<?php endforeach; ?>

				</select>
		</td>
		</tr>
		
		<tr class="alternate">
			<th>Property Address</th>
			<td><input type="text" name="property_address" class="searchbox" value="<?php echo $property_address; ?>" size="78"/></td>
		</tr>
		
		<tr>
			<th>Comments</th>
			<td><textarea name="comments" cols="64"><?php echo $comments; ?></textarea></td>
		</tr>
	
		<tr class="alternate">
			<th><?php _e('Property Alert'); ?></th>
			<td><div class="site_alerts">
				<?php $property_alert=array();$property_alert=array_diff($list_alerts, $news_alerts);?>
				<?php $news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);?>
				<?php foreach($property_alert as $key=>$item_value):?>
					<span><input type="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($subscriptions_manager->subscriber[$key])) echo ' checked="checked"'; ?> onclick="open_query_form(this)">
					<?php echo $item_value; ?></span><br />
					<?php echo $subscriptions_manager->get_form_query_string($key);
			endforeach; ?>
			</div></td>
		</tr>
		
		<?php if(!empty($news_alert)): ?>
		<tr>
			<th><?php _e('News Alert'); ?></th>
			<td><div class="site_alerts">
				<?php foreach($news_alert as $key=>$item_value):?>
					<span><input type="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($subscriptions_manager->subscriber[$key])) echo ' checked="checked"'; ?>>
					<?php echo $item_value; ?></span><br />
				<?php	endforeach; ?>
			</div></td>
		</tr>
		<?php endif; ?>
		</table>
		
		<p class="submit">
		<input name="save" type="submit" value="Save" />
		</p>
	</div>
	<div id="poststuff">
	<div class="postbox">
		<h3>Actions</h3>
			<div class="inside">
				<p class="submit"><input type="submit" value="<?php _e('Save &raquo;') ?>" name="Save" /></p>
				<p><a href="themes.php?page=theme-settings&tab=subscribers<?php echo "&search_by_alert=$search_by_alert&keywords=$keywords"; ?>" class="button"><?php _e('&laquo; Back to Subscribers List'); ?></a></p>
			</div>
		</div>
	</div>
</form>