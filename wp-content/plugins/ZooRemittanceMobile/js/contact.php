<?php
foreach( $_REQUEST AS $key=>$val )
{
	if(!is_array($val))$_REQUEST[$key] = urldecode( $val );
}
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');

global $wpdb, $ZooRemittance;

// send to zoo api
$params=array();
$zr_office_id=get_option('zr_office_id');
if(!empty($zr_office_id)){ // remmit back to certain office_id
	$office_id_all=explode(",",$zr_office_id);
}
else{
	$settings=get_option('realty_general_settings');
	$office_id_all=$settings['office_id'];
}

foreach($office_id_all as $office_id){
	$agents=$wpdb->get_results("select distinct agent_id from office where agent_id!='' and agent_id is not null and id=".intval($office_id), ARRAY_A);
	$zoo_api="http://agentpoint.agentaccount.com/agents/".$agents[0]['agent_id']."/offices/".$office_id."/contact_listeners?";
	foreach( $_REQUEST AS $key=>$val )
	{		
		if($val=='' || $val=='undefined')continue;
		switch($key){			
			case 'keep_informed': case 'property_alert':case 'news_alert':case 'alerts':case 'property_type':case 'min_bedroom':case 'min_bathroom':case 'min_carspace':case 'min_price':case 'max_price':case 'suburbs':break;
			case 'home_phone':$paramaters.="home_number=".$val."&amp;";$params['home_number']=$val; break;
			case 'mobile_phone':$paramaters.="mobile_number=".$val."&amp;";$params['mobile_number']=$val; break;
			case 'work_phone':$paramaters.="work_number=".$val."&amp;";$params['work_number']=$val; break;
			case 'fax':$paramaters.="fax_number=".$val."&amp;";$params['fax_number']=$val; break;
			case 'referrer':
				$val=trim(strtolower($val));
				switch($val){
					case '': case 'undefined':$heard_about_us='';break;
					case 'newspaper': $heard_about_us='1';break;
					case 'referral': $heard_about_us='2';break;
					case 'web': $heard_about_us='3';break;
					case 'past client': $heard_about_us='4';break;
					case 'sign board': $heard_about_us='5';break;
					default: $heard_about_us='6';break;		
				}
				$paramaters.="heard_about_us=".$heard_about_us."&amp;"; $params['heard_about_us']=$val;
			break;
			case 'comments':			
			$paramaters.="note=".str_replace(";","\n",$val)."&amp;";$params['note']=str_replace(";","\n",$val);break;
			default: $paramaters.=$key."=".$val."&amp;";$params[$key]=$val; break;
		}
	}
	
	$keys=$wpdb->get_results("select access_key,private_key from property_xml where office_id=".intval($office_id), ARRAY_A);
	$accesskey=$keys[0]['access_key'];
	$privatekey=$keys[0]['private_key'];

	if(!empty($_REQUEST['alerts']) &&!empty($accesskey) && !empty($privatekey)){
		$news_flag=0;
		$alerts=explode(",",$_REQUEST['alerts']);
		foreach($alerts as $alert){
			$extra_parameters=$paramaters;
			$extra_params=$params;
			$alert=trim($alert);
			switch($alert){
				case 'sale': 	
					$extra_parameters.='alert=1&amp;alert_type=Newly Listed Properties&amp;listing_type=ResidentialSale';
					
					$extra_params['alert']='1';
					$extra_params['alert_type']='Newly Listed Properties';
					$extra_params['listing_type']='ResidentialSale';
					
					if(!empty($_REQUEST['property_type_sale'])){ $extra_parameters.='&amp;property_type='.$_REQUEST['property_type_sale']; $extra_params['property_type']= $_REQUEST['property_type_sale']; }
					if(!empty($_REQUEST['min_bedroom_sale'])){ $extra_parameters.='&amp;min_bedroom='.$_REQUEST['min_bedroom_sale'];$extra_params['min_bedroom']= $_REQUEST['min_bedroom_sale']; } 
					if(!empty($_REQUEST['min_bathroom_sale'])){ $extra_parameters.='&amp;min_bathroom='.$_REQUEST['min_bathroom_sale'];$extra_params['min_bathroom']= $_REQUEST['min_bathroom_sale']; }
					if(!empty($_REQUEST['min_carspace_sale'])){ $extra_parameters.='&amp;min_carspace='.$_REQUEST['min_carspace_sale']; $extra_params['min_carspace']= $_REQUEST['min_carspace_sale'];}
					if(!empty($_REQUEST['min_price_sale'])){ $extra_parameters.='&amp;min_price='.$_REQUEST['min_price_sale'];$extra_params['min_price']= $_REQUEST['min_price_sale']; }
					if(!empty($_REQUEST['max_price_sale'])){ $extra_parameters.='&amp;max_price='.$_REQUEST['max_price_sale'];$extra_params['max_price']= $_REQUEST['max_price_sale']; }
					if(!empty($_REQUEST['suburbs_sale'])){ $extra_parameters.='&amp;suburbs='.implode(",",$_REQUEST['suburbs_sale']);$extra_params['suburbs']= implode(",",$_REQUEST['suburbs_sale']); }	
					
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'lease': 
					$extra_parameters.='alert=1&amp;category_id=11&amp;alert_type=Newly Listed Properties&amp;listing_type=ResidentialLease';
					
					$extra_params['alert']='1';
					$extra_params['category_id']='11';
					$extra_params['alert_type']='Newly Listed Properties';
					$extra_params['listing_type']='ResidentialLease';
					
					if(!empty($_REQUEST['property_type_lease'])){ $extra_parameters.='&amp;property_type='.$_REQUEST['property_type_lease']; $extra_params['property_type']= $_REQUEST['property_type_lease']; }
					if(!empty($_REQUEST['min_bedroom_lease'])){ $extra_parameters.='&amp;min_bedroom='.$_REQUEST['min_bedroom_lease'];$extra_params['min_bedroom']= $_REQUEST['min_bedroom_lease']; } 
					if(!empty($_REQUEST['min_bathroom_lease'])){ $extra_parameters.='&amp;min_bathroom='.$_REQUEST['min_bathroom_lease'];$extra_params['min_bathroom']= $_REQUEST['min_bathroom_lease']; }
					if(!empty($_REQUEST['min_carspace_lease'])){ $extra_parameters.='&amp;min_carspace='.$_REQUEST['min_carspace_lease']; $extra_params['min_carspace']= $_REQUEST['min_carspace_lease'];}
					if(!empty($_REQUEST['min_price_lease'])){ $extra_parameters.='&amp;min_price='.$_REQUEST['min_price_lease'];$extra_params['min_price']= $_REQUEST['min_price_lease']; }
					if(!empty($_REQUEST['max_price_lease'])){ $extra_parameters.='&amp;max_price='.$_REQUEST['max_price_lease'];$extra_params['max_price']= $_REQUEST['max_price_lease']; }
					if(!empty($_REQUEST['suburbs_lease'])){ $extra_parameters.='&amp;suburbs='.implode(",",$_REQUEST['suburbs_lease']);$extra_params['suburbs']= implode(",",$_REQUEST['suburbs_lease']); }	
					
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'latest_sale_update': 
					$extra_parameters.='alert=1&amp;alert_type=Updated Properties&amp;listing_type=ResidentialSale';					
					$extra_params['alert']='1';
					$extra_params['alert_type']='Updated Properties';
					$extra_params['listing_type']='ResidentialSale';					
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'latest_lease_update':
					$extra_parameters.='alert=1&amp;category_id=11&amp;alert_type=Updated Properties&amp;listing_type=ResidentialLease';
					$extra_params['alert']='1';
					$extra_params['category_id']='11';
					$extra_params['alert_type']='Updated Properties';
					$extra_params['listing_type']='ResidentialLease';
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'opentimes_sales': 
					$extra_parameters.='alert=1&amp;alert_type=Open Inspections&amp;listing_type=ResidentialSale';
					$extra_params['alert']='1';
					$extra_params['alert_type']='Open Inspections';
					$extra_params['listing_type']='ResidentialSale';
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'opentimes_lease':
					$extra_parameters.='alert=1&amp;category_id=11&amp;alert_type=Open Inspections&amp;listing_type=ResidentialLease';
					$extra_params['alert']='1';
					$extra_params['category_id']='11';
					$extra_params['alert_type']='Open Inspections';
					$extra_params['listing_type']='ResidentialLease';
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'listing_sold': 
					$extra_parameters.='alert=1&amp;alert_type=Sold/Leased Properties&amp;listing_type=ResidentialSale';
					$extra_params['alert']='1';
					$extra_params['alert_type']='Sold/Leased Properties';
					$extra_params['listing_type']='ResidentialSale';
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'listing_lease':
					$extra_parameters.='alert=1&amp;alert_type=Sold/Leased Properties&amp;listing_type=ResidentialLease';
					$extra_params['alert']='1';
					$extra_params['alert_type']='Sold/Leased Properties';
					$extra_params['listing_type']='ResidentialLease';
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				case 'latest_auctions':
					$extra_parameters.='alert=1&amp;alert_type=Auctions';
					$extra_params['alert']='1';
					$extra_params['alert_type']='Auctions';
					$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params);
				break;
				default:
					if($news_flag=='0'){ 
						$extra_parameters.='alert=1&amp;alert_type=Latest News';
						$news_flag=1; 	
						$extra_params['alert']='1';
						$extra_params['alert_type']='Latest News';
						$ZooRemittance->send_api($zoo_api,$extra_parameters,$accesskey,$privatekey,$extra_params); 
					}
				break;
			}							
		}		
	}
	if(empty($_REQUEST['alerts']) &&!empty($accesskey) && !empty($privatekey)){
		$paramaters.=($_REQUEST['keep_informed']=='1')?'alert=1&amp;alert_type=Latest News':'';		
		/*
		print_r ($_REQUEST);echo "<br>";
		echo $zoo_api."<br>";
		echo $paramaters."<br>";
		echo $accesskey."<br>";
		echo $privatekey."<br>";
		die();
		*/
		if(!empty($accesskey) && !empty($privatekey))$ZooRemittance->send_api($zoo_api,$paramaters,$accesskey,$privatekey,$params);
	}
}
echo 'a';
?>