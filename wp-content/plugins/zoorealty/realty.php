<?php
/* 
Plugin Name: Zoo Realty New
Description: The Zoo Real Estate Plug-in creates multi pages for property listings search results and property pages. Includes favourites, general settings, media slideshow, page creator, search results, search results thumbnails, sold and leased and team configurations. Zoo Realty is licensed under the <a href="http://www.zooproperty.com/plugin-license/">Zoo Property License</a>.
Version: 3.0
Author: Agentpoint
Author URI: http://www.agentpoint 
*/
if(!class_exists('my_db')){
	require_once('controller/my_db.php');	
}
require_once('controller/helper.php');
$helper = $propertyProfile = null;
class realty{
	var $debug = false;
	var $templates = array();	
	var $params = array();	
	var $property = array();
	var $sale_status = "'1', '2', '4', '6'";
	var $settings = array();	
	var $paths = array(
	'stickers'=>'style/stickers/',	
	'rss'=>'controller/realty_rss.php',
	);
	var $is_property_page = false;
	
	function realty(){		
		add_action('init', array($this, 'init'));
		add_action('wp_head', array($this, 'wp_head'),0);
		add_filter('query_vars', array($this,'query_vars'));
		add_action('generate_rewrite_rules', array($this,'realty_rewrite_rules'));
		add_action('admin_menu', array($this, 'add_pages'));			
		//add_filter('the_content',array($this, 'the_content'));		
		add_shortcode('realty_plugin', array($this, 'parse_shortcode'));
		add_action('template_redirect', array($this, 'rewritetitle'));
		remove_action('wp_head', 'wp_generator'); 
		remove_action( 'wp_head', 'feed_links_extra', 3 ); 
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'rsd_link' ); 
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'index_rel_link');
		remove_action( 'wp_head', 'parent_post_rel_link' ); 
		remove_action( 'wp_head', 'start_post_rel_link' ); 
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );		
		register_activation_hook(__FILE__, array($this, 'install'));
	}

	function init(){
		global $helper, $wp_rewrite, $wpdb;
		$helper = new helper;					

		if(isset($_GET['debug']) and is_user_logged_in()):
			$this->debug = $_GET['debug'];
			$helper->debug = true;

			add_action('wp_footer', array($this,'wp_footer'));
		else:
			if(!$helper->isLocalhost()):

			endif;
		endif;
		$this->blogname = get_option('blogname');
		$this->siteUrl = get_option('siteurl');
		if ( '/' != $this->siteUrl[strlen($this->siteUrl)-1]) $this->siteUrl .= '/';
		$this->pluginUrl = $this->siteUrl . PLUGINDIR . '/' . basename(dirname(__FILE__)) .'/';		
		$this->paths['theme'] = get_bloginfo('stylesheet_directory') .'/';
		if(preg_match ('/http:\/\/www.(.*?).com.au\//is', $this->siteUrl, $list))$this->sitename=$list[1];
		else if(preg_match ('/http:\/\/www.(.*?).com\//is', $this->siteUrl, $list))$this->sitename=$list[1];
		
		//property page id
		$this->property_page_id=get_option('realty_property_id');
		if(!$this->property_page_id){
			$this->property_page_id=$wpdb->get_var("SELECT `ID` FROM $wpdb->posts where `post_name`= 'property' and post_status='publish'");
			if(!update_option('realty_property_id',$this->property_page_id))add_option('realty_property_id',$this->property_page_id);
		}
		$this->team_url=get_option('realty_team_url');
		if(!$this->team_url){
			$post_id=$wpdb->get_var("SELECT `ID` FROM $wpdb->posts WHERE `post_content` like '%template:team]%' and post_status='publish' and post_type='page' LIMIT 1");
			if($post_id){
				$this->team_url=get_permalink($post_id);
				if(!update_option('realty_team_url',$this->team_url))add_option('realty_team_url',$this->team_url);
			}
		}
		
		//randw
		if(strpos($this->siteUrl,'randw.com.au')!==false){
		//md view page id
		$sql = "SELECT `ID` FROM $wpdb->posts WHERE `post_content` like '%template:md_view%' and post_status='publish' LIMIT 1";		
		$this->md_page_id=$wpdb->get_var($sql);
		}
		//////////////////////////////////////SETTINGS///////////////////////////////////////////////////////////////////
		$this->paths['AdminPages'] = $helper->getTemplates($this->path('manage/pages/'));
		foreach($this->paths['AdminPages'] as $filename=>$path)
			if(is_array($pageOption = get_option('realty_' .$filename) ))
				$this->settings[$filename]= $pageOption;
		 $this->settings['widgets'] = get_option('realty_widgets');

		 $this->widget_register();
		 
		 /*******************************DEFAUT FOR PARAMS***************************************/
		///$this->params['page']  moved to wp_header
		$this->params['limit'] = $this->settings['general_settings']['number_of_listings_on_each_page'];
		if(!empty($_GET['limit']) && is_numeric($_GET['limit'])){
			setcookie("limit", $_GET['limit'],time()+(60*60*24*365), '/');
			$this->params['limit']=$_GET['limit'];
		}
		if(!empty($this->settings['general_settings']['user_id']))$this->params['user_id'] = implode(",",$this->settings['general_settings']['user_id']);
		$this->params['order'] = ($this->settings['general_settings']['sort_listing_by']=='')?"created_at":$this->settings['general_settings']['sort_listing_by'];
		$this->params['default_currency_code'] = ($this->settings['general_settings']['default_currency_code']=='')?"$":$this->settings['general_settings']['default_currency_code'];
		if(!empty($this->settings['general_settings']['sort_order'])){
			if($this->params['order']=='created_at' && ($_GET['order']=='created_at' || empty($_GET['order'])))$this->params['order_direction'] = "DESC";
			else $this->params['order_direction'] = "asc";
		}
		else {	
			if($this->params['order']=='created_at' && ($_GET['order']=='created_at' || empty($_GET['order'])))$this->params['order_direction'] = "asc";
			else $this->params['order_direction'] = "DESC";	
		}
		
		//bushby
		if(strpos($this->siteUrl,'bushby.com.au')!==false){
			if(empty($this->settings['featured_listings']['featured_listings']['lease']))$this->settings['featured_listings']['featured_listings']['lease']=array();
			if(empty($this->settings['featured_listings']['featured_listings']['sale']))$this->settings['featured_listings']['featured_listings']['sale']=array();
			$this->featured_listings_ids = @array_merge($this->settings['featured_listings']['featured_listings']['sale'], $this->settings['featured_listings']['featured_listings']['lease']);
		}	
			
		/*******************************************************************************/	
		
		//////////////////////RSS Feeds////////////////////////////////////////////
		if (isset($_GET['rss'])):
			require_once($this->paths['rss']);
			$realty_rss = new realty_rss($_GET['rss'], $this->blogname . ' for ' . $_GET['rss']);
		endif;

		//////////////////////////Uninstall button pressed/////////////////////////////////////////
		
		if (isset($_GET['uninstall'])  and isset($_GET['delete_all_pages_and_options']))
			$this->uninstall();
		//////////////////////////////////////////////////////////////////////////////////////////

		if($_GET['task'] =='agent_csv'){		
			$this->download_agents($_GET['office_id'],$_GET['groups'],$_GET['state'],$_GET['order']);
		}
		
	}
	
	function my_flush_rules(){
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }
	
	function install(){
		global $helper;
		require_once('controller/install.php');
		require_once('controller/suburb_region.php');
	}
		
	function wp_footer(){
		?><div>
		<?php //$this->return .
		if($this->debug == 'all')
			p( $this, 0, 1);			
		elseif(!empty($this->debug) and is_string($this->debug))
			p($this->return[$this->debug], 0, 1);
		?>
		</div><?php
	}

	function error_handler($code, $msg, $file, $line, $errcontext) {			

			$this->return['error'] .= "<font color='red'>$msg [code: $code ]</font> on $file, line $line {$errcontext}<br />";
	}

	function path($path, $type = null ){
		global $helper;
		switch($type):
			case 'url':	/*Converts an absolute directory from a WordPress root to its url in the blog. */
				return $this->siteUrl .$this->relativePath($path);
			break;
			case 'flip':
				if($helper->validUrl($path)) //The path is a url; convert to absolute directory
					return ABSPATH . $this->relativePath($path);
				else //The path is a directory; convert to absolute url
					return $this->siteUrl . $this->relativePath($path);				
			break;
			case 'rel': //Converts an absolute directory or url to a relative path
				return $this->relativePath($path);
			break;
			default: 	/*Append this plugin directory to a path. Used for files inside this plugin directory.*/
				return $helper->replaceSlashes(dirname( __FILE__ ). '/' . $path);
			break;						
		endswitch;		
	}

	function relativePath($path){
		global $helper;
		return str_replace(array($this->siteUrl, $helper->replaceSlashes(ABSPATH) ), '', $helper->replaceSlashes($path));
	}

	function element($elements, $params = array()){
		global $helper, $realty;
			return require($this->path((is_admin())? 'manage/elements/':'display/elements/'). "$elements.php");		
	}

	function add_pages(){
		global $helper;	
		$firstFile = 'featured_listings';
		
		$firstFile = $this->path('manage/pages/') .$firstFile.'.php';
		
		add_menu_page('Zoo Realty', 'Zoo Realty', 8, $firstFile);
		add_submenu_page($firstFile, 'Featured Listings', 'Featured Listings', 8, $firstFile);
		foreach($this->paths['AdminPages'] as $filename=>$path):
			if($path != $firstFile ): //Avoid including the first page twice
				$metadataArr = $this->getFileMetadata($path);
				if($filename=='team')$filename='The Team';if($filename=='pages')$filename='Listing Pages';
				if($filename=='search_results_thumbnail')$filename='Results Thumb';if($filename=='search_results')$filename='Results List';
				add_submenu_page($firstFile, $helper->humanize($filename), $helper->humanize($filename), $metadataArr['access_level'], $path);
			endif;
		endforeach;
	}

	function get_permalink($slug=null){
		global $helper;
		// return get_permalink($helper->getPost($slug));		
	}

	function query_vars($public_query_vars) {
		$public_query_vars[] = "property_id";
		$public_query_vars[] = "xml_id";
		$public_query_vars[] = "user_id";
		$public_query_vars[] = "user_name";
		return $public_query_vars;
	}

	function realty_rewrite_rules( $wp_rewrite ){
		global $helper;
		$new_rules = array(
		'([0-9]+)/?$' => 'index.php?page_id='.$this->property_page_id . '&property_id='.	$wp_rewrite->preg_index(1),
		'(.*?)-([0-9]{5,10}+)/?$' => 'index.php?page_id=' . $this->property_page_id . '&property_id=' . $wp_rewrite->preg_index( 2 ),
		'(.*?)P(.*?)' => 'index.php?page_id='.$this->property_page_id . '&xml_id='.	$wp_rewrite->preg_index(1)."P".$wp_rewrite->preg_index(2),
		'(.*?)P(.*?)/?$' => 'index.php?page_id='.$this->property_page_id . '&xml_id='.	$wp_rewrite->preg_index(1)."P".$wp_rewrite->preg_index(2),
		'property-(.*?)-(.*?)-(.*?)-([0-9]+)/?$' => 'index.php?page_id='.$this->property_page_id . '&property_id='.	$wp_rewrite->preg_index(4),
		'real-estate-agent-([0-9]+)/?$' => 'index.php?page_id='.$this->agent_page_id . '&user_id='.	$wp_rewrite->preg_index(1),
		'projects(.*?)/([0-9]+)/?$' => 'index.php?page_id='.$this->property_page_id . '&property_id='.	$wp_rewrite->preg_index(2),		
		'agent/(.*?)/([0-9]+)/?$' => 'index.php?page_id='.$this->agent_page_id . '&user_name='.	$wp_rewrite->preg_index(1),
		'agent/(.*?)/?$' => 'index.php?page_id='.$this->agent_page_id . '&user_name='.	$wp_rewrite->preg_index(1),
		'buat-test' => 'index.php',
		);
		$wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
	}	
	
	function uninstall(){

	}
		
	function select_commercial($flag=true){		
		$tbl_properties = "properties";
		$placeholder_image= $this->paths['theme'].'images/download.png';
		if($flag){
			$sql =  "SELECT $tbl_properties.id, type, property_type, category, deal_type, status, display_address, unit_number, street_number, street, state, town_village, suburb, postcode, latitude,	longitude, bedrooms, bathrooms, (garage + carport + off_street_park) AS carspaces, price, display_price_text as price_display_value, display_price as price_display, show_price as display_sold_price, headline, description, floor_area as building_size, land_area as land_size, agent_id, office_id, user_id, secondary_user, forthcoming_auction, updated_at, video_url, created_at, xml_id	, 
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,			 
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END)as auction_date,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END)as auction_time,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place	else '' END)as auction_place,	
			third_user, fourth_user, date_available as available_at, property_type2, property_type3, method_of_sale, authority, highlight1, highlight2, highlight3, office_area, warehouse_area, retail_area, other_area, area_range, area_range_to, rental_price, rental_price_to, sale_price_from as sale_price, sale_price_to";		
		} 
		else {
			$sql="SELECT $tbl_properties.id, type, property_type, category, deal_type, status, display_address, unit_number, street_number, street, state, suburb, bedrooms, bathrooms, (garage + carport + off_street_park) AS carspaces, price, display_price_text as price_display_value, display_price as price_display, show_price as display_sold_price, headline, description, floor_area as building_size, land_area as land_size, agent_id, office_id, user_id, secondary_user, forthcoming_auction, updated_at, area_range, area_range_to, video_url, created_at, postcode, xml_id	,			
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,			 
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END)as auction_date,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END)as auction_time,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place	else '' END)as auction_place,	
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='floor_area_metric' LIMIT 1) AS floor_area_metric,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='land_area_metric' LIMIT 1) AS land_area_metric,		
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='price_period' LIMIT 1) AS price_period
			";
		}
		return $sql;
	}
	
	function select_projects($flag=true){		
		$tbl_properties = "properties";
		$placeholder_image= $this->paths['theme'].'images/download.png';
		if($flag){
			$sql =  "SELECT created_at, unique_id, $tbl_properties.id, type, user_id, secondary_user, third_user, to_price, description, latitude, longitude, (garage + carport + off_street_park) AS carspaces, bedrooms, show_price as display_sold_price, bathrooms, headline, postcode, property_type, display_price as price_display, price, display_price_text as price_display_value, town_village, floor_area as building_size, land_area as land_size, date_available as available_at, status, max_persons as maximum_persons, forthcoming_auction, category, estimate_rental_return, deal_type, display_address, unit_number, street_number, street, state, suburb, country, office_id, agent_id,xml_id	,
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,	
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END)as auction_date,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END)as auction_time,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place	else '' END)as auction_place";		
		}
		else {
			$sql="SELECT  $tbl_properties.id, bedrooms, show_price as display_sold_price, postcode, property_type, display_price as price_display, price, display_price_text as price_display_value, status ,forthcoming_auction, deal_type, display_address, state, suburb, country, agent_id, office_id, headline, description, xml_id	,
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,			
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END)as auction_date,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END)as auction_time,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place	else '' END)as auction_place,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='bedroom_from' LIMIT 1) AS bedroom_from,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='bedroom_to' LIMIT 1) AS bedroom_to,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='number_of_floors' LIMIT 1) AS number_of_floors,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='number_of_properties' LIMIT 1) AS number_of_properties,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='completion_date' LIMIT 1) AS completion_date,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='development_name' LIMIT 1) AS development_name";
		}
		return $sql;
	}

	function select_holiday($flag=true){		
		$tbl_properties = "properties";
		$placeholder_image= $this->paths['theme'].'images/download.png';
		if($flag){
			$sql="SELECT  $tbl_properties.id, type, property_type, user_id, secondary_user, agent_id, office_id, headline, description, (garage + carport + off_street_park) AS carspaces, bedrooms,  bathrooms, display_price as price_display, price, display_price_text as price_display_value, high_season_price, peak_season_price, mid_season_price, max_persons as maximum_persons, status, deal_type, display_address, unit_number, street_number, street, state, suburb, latitude, longitude, postcode, floor_area as building_size,xml_id	,land_area as land_size";	
		}
		else {
			$sql="SELECT  $tbl_properties.id, type, property_type, user_id, secondary_user, agent_id, office_id, headline, description, (garage + carport + off_street_park) AS carspaces, bedrooms,  bathrooms, display_price as price_display, price, display_price_text as price_display_value, high_season_price, peak_season_price, mid_season_price, max_persons as maximum_persons, status, deal_type, display_address, unit_number, street_number, street, state, suburb, latitude, longitude, postcode, floor_area as building_size, land_area as land_size,xml_id	,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='floor_area_metric' LIMIT 1) AS floor_area_metric,	
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='land_area_metric' LIMIT 1) AS land_area_metric,	
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='mid_season_period' LIMIT 1) AS mid_season_period,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='high_season_period' LIMIT 1) AS high_season_period,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='peak_season_period' LIMIT 1) AS peak_season_period,		
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='price_period' LIMIT 1) AS price_period";
		}
		return $sql;
	}
	
	function select($flag=true){		
		$tbl_properties = "properties";
		$sql =  "SELECT unique_id, $tbl_properties.id, type, user_id, secondary_user, description, latitude, longitude, (garage + carport + off_street_park) AS carspaces,  bedrooms, show_price as display_sold_price, bathrooms, headline, postcode, property_type, display_price as price_display, price, display_price_text as price_display_value, town_village, floor_area as building_size, land_area as land_size, date_available as available_at, status, category, deal_type, display_address, unit_number, street_number, street ,state, suburb, agent_id, office_id, xml_id,
		(Case when status='2' Then sold_at	else '' END)as sold_at,
		(Case when status='2' Then sold_price else '' END)as sold_price,
		(Case when status='6' Then leased_at else '' END)as leased_at,
		(Case when status='6' Then leased_price	else '' END)as leased_price,	
		(Case when datediff(curdate(),created_at)<=7 Then 'NEW' else '' END)as new,	
		(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END)as auction_date,
		(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END)as auction_time,
		(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place	else '' END)as auction_place";		
		return $sql;
	}

	function properties($params = array(), $send_query=true, $select=''){
		global $helper, $wpdb;
		$return=$this->search($params);
		if(empty($select)){					
			if($this->settings['general_settings']['default_listing']=='commercial'){
				if($params['limit']!=1)$select = $this->select_commercial(false);
				else $select = $this->select_commercial();
			}
			elseif($this->settings['general_settings']['default_listing']=='projects'){
				if($params['limit']!=1)$select = $this->select_projects(false);
				else $select = $this->select_projects();
			}
			elseif($this->settings['general_settings']['default_listing']=='holiday_lease'){
				if($params['limit']!=1)$select = $this->select_holiday(false);
				else $select = $this->select_holiday();
			}
			else{
				if($params['limit']!=1)$select = $this->select(false);
				else $select = $this->select();
			}
		}
		if(empty($params['all_listings']) && class_exists('zoo_agents'))$return['condition'] = $return['condition'] . " and properties.active ='Active'";// zoo agents code, only display listing with status Active
		
		if($params['related']){
			$from.=" LEFT JOIN suburb_region ON suburb_region.suburb LIKE ='".esc_sql($params['related'])."'";	
			$select.=",111.045* DEGREES(ACOS(COS(RADIANS(suburb_region.latitude))
					* COS(RADIANS(properties.latitude))
					* COS(RADIANS(suburb_region.longitude) - RADIANS(properties.longitude))
					+ SIN(RADIANS(suburb_region.latitude))
					* SIN(RADIANS(properties.latitude)))) AS distance_in_km";
		}
		
		if(strpos($this->siteUrl,'888usa')!==false){
				$sql = "$select,(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='bond' LIMIT 1) AS bond,		
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='ext_link_1' LIMIT 1) AS ext_link_1,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='ext_link_2' LIMIT 1) AS ext_link_2,			
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='tax_rate' LIMIT 1) AS tax_rate,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='condo_strata_fee' LIMIT 1) AS condo_strata_fee,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='floor_area_metric' LIMIT 1) AS floor_area_metric,	
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='land_area_metric' LIMIT 1) AS land_area_metric,				
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='property_url' LIMIT 1) AS property_url,			
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='price_period' LIMIT 1) AS price_period,	
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='virtual_tour' LIMIT 1) AS virtual_tour,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='year_built' LIMIT 1) AS year_built,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='zoning' LIMIT 1) AS zoning FROM properties WHERE ".$return['condition'];	
		}
		else{
		$sql = "$select FROM properties WHERE ".$return['condition'];	
		}
		
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE ".$return['condition']);
		if(!empty($params['limit']) && $results_count>$params['limit'])$results_count=$params['limit'];

		$sql .= " ".$return['end'];
		if($this->debug)print $sql ;	
		if($send_query):	
			$return = $wpdb->get_results($sql, ARRAY_A);
			if(!empty($return)){
				$placeholder_image= $this->paths['theme'].'images/download.png';
				foreach($return as $key=>$row){
					if(!is_array($row))continue;
					if(!empty($row['id'])){
						$photos=$wpdb->get_results("SELECT url, description, width, height FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('original','large','medium','thumb') and type='photo' and is_user='0' order by position LIMIT 4 ", ARRAY_A);
						if(!empty($photos)){
							foreach($photos as $photo):
								if($photo['description']=='thumb'){
									$return[$key]['thumbnail']=$photo['url'];
									$return[$key]['thumbnail_width']=$photo['width'];
									$return[$key]['thumbnail_height']=$photo['height'];
								}
								if($photo['description']=='medium'){
									$return[$key]['medium']=$photo['url'];
									$return[$key]['medium_width']=$photo['width'];
									$return[$key]['medium_height']=$photo['height'];
								}
								if($photo['description']=='large'){
									$return[$key]['large']=$photo['url'];
									$return[$key]['large_width']=$photo['width'];
									$return[$key]['large_height']=$photo['height'];
								}
								if($photo['description']=='original'){
									$return[$key]['original']=$photo['url'];
									$return[$key]['original_width']=$photo['width'];
									$return[$key]['original_height']=$photo['height'];								
								}
							endforeach;
						}
						if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
						if(empty($return[$key]['medium']))$return[$key]['medium']=$placeholder_image;
						if(empty($return[$key]['large']))$return[$key]['large']=$placeholder_image;
						if(empty($return[$key]['original']))$return[$key]['original']=$placeholder_image;	
													
						//bondibeachrentals
						if(strpos($this->siteUrl,'bondibeachrentals')!==false){
							$return[$key]['sbid'] = $this->getSherlockBookingID($row['xml_id']);
						}
					}
				}
			}
			if(is_array($return) && empty($params['group']))array_walk($return,array( $this, 'format_property'));	
			$return['results_count'] = $results_count;
			$pagination = $helper->paginate($this->params['limit'], $this->params['page'], $results_count);
			foreach($this->settings['general_settings']['display_pagination'] as $paginate)
				$return['pagination_' . $paginate] = $pagination; //Returns $return['pagination_above'] and/or $return['pagination_below']
 			return $return;
 		endif;
		return $sql; 
	}
	
	function ajax_properties($params = array(), $send_query=true, $select=''){
		global $helper, $wpdb;
		extract($this->search($params));
		if(empty($select)){			
			if(empty($params['id']))$select = $this->select(false);
			else $select = $this->select();
		}
		$sql = "$select FROM properties WHERE $condition";	
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE $condition");
		$sql .= " $end";
		if($this->debug)
			print $sql ;	
				
		if($send_query):	
			$return = $wpdb->get_results($sql, ARRAY_A);
			if(!empty($return)){
				$placeholder_image= $this->paths['theme'].'images/download.png';
				foreach($return as $key=>$row){
					if(!is_array($row))continue;
					if(!empty($row['id'])){
						$photos=$wpdb->get_results("SELECT url, description, width, height FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('medium','thumb') and type='photo' and is_user='0' order by position LIMIT 2 ", ARRAY_A);
						if(!empty($photos)){
							foreach($photos as $photo):
								if($photo['description']=='thumb'){
									$return[$key]['thumbnail']=$photo['url'];
									$return[$key]['thumbnail_width']=$photo['width'];
									$return[$key]['thumbnail_height']=$photo['height'];
								}
								if($photo['description']=='medium'){
									$return[$key]['medium']=$photo['url'];
									$return[$key]['medium_width']=$photo['width'];
									$return[$key]['medium_height']=$photo['height'];
								}
							endforeach;
						}
						if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
						if(empty($return[$key]['medium']))$return[$key]['medium']=$placeholder_image;
					}
				}
			}
			if(is_array($return)) 
				array_walk($return,array( $this, 'format_property'));			
			$return['results_count'] = $results_count;
			$pagination = $helper->ajax_paginate($this->params['limit'], $params['page'], $results_count);
			foreach($this->settings['general_settings']['display_pagination'] as $paginate)
				$return['pagination_' . $paginate] = $pagination; 
 			return $return;
 		endif;
		return $sql; 
	}
	
	function team_properties($params = array(), $send_query=true, $select=''){
		global $helper, $wpdb;
		$return=$this->search($params);
		if(empty($select)){			
			if(empty($params['id']))$select = $this->select(false);
			else $select = $this->select();
		}
		$sql = "$select FROM properties WHERE ".$return['condition'];	
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE ".$return['condition']);
		$sql .= " ".$return['end'];
		if($this->debug)print $sql ;	
				
		if($send_query):	
			$return = $wpdb->get_results($sql, ARRAY_A);
			if(!empty($return)){
				$placeholder_image= $this->paths['theme'].'images/download.png';
				foreach($return as $key=>$row){
					if(!is_array($row))continue;
					if(!empty($row['id'])){
						$photos=$wpdb->get_results("SELECT url, description, width, height FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('original','large','medium','thumb') and type='photo' and is_user='0' order by position LIMIT 4 ", ARRAY_A);
						if(!empty($photos)){
							foreach($photos as $photo):
								if($photo['description']=='thumb'){
									$return[$key]['thumbnail']=$photo['url'];
									$return[$key]['thumbnail_width']=$photo['width'];
									$return[$key]['thumbnail_height']=$photo['height'];
								}
								if($photo['description']=='medium'){
									$return[$key]['medium']=$photo['url'];
									$return[$key]['medium_width']=$photo['width'];
									$return[$key]['medium_height']=$photo['height'];
								}
								if($photo['description']=='large'){
									$return[$key]['large']=$photo['url'];
									$return[$key]['large_width']=$photo['width'];
									$return[$key]['large_height']=$photo['height'];
								}
								if($photo['description']=='original'){
									$return[$key]['original']=$photo['url'];
									$return[$key]['original_width']=$photo['width'];
									$return[$key]['original_height']=$photo['height'];								
								}
							endforeach;
						}
						if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
						if(empty($return[$key]['medium']))$return[$key]['medium']=$placeholder_image;
						if(empty($return[$key]['large']))$return[$key]['large']=$placeholder_image;
						if(empty($return[$key]['original']))$return[$key]['original']=$placeholder_image;						
					}
				}
			}
			if(is_array($return))array_walk($return,array( $this, 'format_property'));			
			$return['results_count'] = $results_count;
			$pagination = $helper->team_paginate($params['status'],$params['user_id'],$this->params['limit'], $params['page'], $results_count);
			foreach($this->settings['general_settings']['display_pagination'] as $paginate)
				$return['pagination_' . $paginate] = $pagination;
 			return $return;
 		endif;
		return $sql; 
	}
	
	function properties_video($params = array(), $select=''){
		global $wpdb, $helper, $wp_query;
		
		$temp=array();
		$i=0;
		$j=1;
		$results_count=0;
		$placeholder_video=$this->paths['theme'].'on-demand/images/place_tv.jpg';
		if($params['video_type']!='sold_property_videos' && $params['video_type']!='current_property_videos' && $params['video_type']!='agent_profile_videos'){
			$admin_videos=get_option('realty_videos');	
			if(!empty($admin_videos)){
				foreach($admin_videos as $item){
					if(empty($params['admin_ids']) || (is_array($params['admin_ids']) && in_array($j,$params['admin_ids'])) ){
						$temp[$i]=$item; 
						$temp[$i]['admin']='1';
						$temp[$i]['id']=$j;
						if(!empty($item['youtube_id'])){
							$temp[$i]['thumb']=str_replace("youtube_id",$item['youtube_id'],"http://img.youtube.com/vi/youtube_id/2.jpg");
							$temp[$i]['large']=str_replace("youtube_id",$item['youtube_id'],"http://img.youtube.com/vi/youtube_id/0.jpg");					
						}
						else {
							$temp[$i]['thumb']=$placeholder_video;
							$temp[$i]['large']=$placeholder_video;
						}
					}
					$i++;
					$j++;
				}
				$results_count=count($admin_videos);
			}
		}
		
		if($params['video_type']!='agent_profile_videos' && $params['video_type']!='other_videos'){
			$return=$this->search($params);
			$condition=$return['condition'];
			$property_ids=array();
			if($params['video_type']=='sold_property_videos')$condition.=" and properties.status in (2)";
			if($params['video_type']=='current_property_videos')$condition.=" and properties.status in (1,4)";
			$video_condition="$condition and properties.id=property_video.property_id and ((embed_code!='' and embed_code is not null) or (youtube_id!='' and youtube_id is not null))";
			
			if(empty($params['limit']))$params['limit']=$this->settings['general_settings']['number_of_videos_on_video_page'];
			if(empty($params['limit']))$params['limit']='10';
			$params['page'] = $wp_query->query_vars['page'];
			$params['page']=($params['page']=='')?'1':$params['page'];
			if (!empty($params['limit'])):
				if(isset($params['page']))$limit_start = $params['page'] * $params['limit'] - ($params['limit']) . ", ";			
				$sql_limit = " LIMIT $limit_start " . $params['limit'] ;
			endif;	
				
			$tbl_properties = "properties"; 
			if(empty($select))$select="SELECT $tbl_properties.id, type, (garage + carport + off_street_park) AS carspaces, bedrooms, show_price as display_sold_price,bathrooms, headline, postcode,property_type,display_price as price_display, price, display_price_text as price_display_value, 
				(Case when properties.status='2' Then sold_at	else '' END)as sold_at,
				(Case when properties.status='2' Then sold_price else '' END)as sold_price,
				(Case when properties.status='6' Then leased_at else '' END)as leased_at,
				(Case when properties.status='6' Then leased_price	else '' END)as leased_price, 
				status, deal_type,display_address, unit_number, street_number, street, state, suburb, agent_id, office_id, property_video.`title`, `embed_code`, `youtube_id`, property_video.status as video_status,  description ";			
				
			$sql="$select from properties , property_video where $video_condition ORDER BY `created_at` desc , `street` , `suburb` $sql_limit";
			$listing_count = $wpdb->get_var("SELECT COUNT(*) AS results_count From properties , property_video where $video_condition");
			if($this->debug)print $sql ;		
				
			$return = $wpdb->get_results($sql, ARRAY_A);
			
			if(is_array($return))array_walk($return,array( $this, 'format_property'));
			
			if($listing_count > 0){
				foreach($return as $item){
					array_push($property_ids, $item['id']);
					$temp[$i]=$item; 
					if(!empty($item['youtube_id']) && $item['video_status']==NULL){
						$item['youtube_id']= trim($item['youtube_id']); 
						if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $item['youtube_id'], $matches)){
							$item['youtube_id']=$matches[0];
						}
						$temp[$i]['youtube_id']=$item['youtube_id']=str_replace("http://www.youtube.com/embed/","",$item['youtube_id']);
						$temp[$i]['thumb']=str_replace("youtube_id",$item['youtube_id'],"http://img.youtube.com/vi/youtube_id/2.jpg");
						$temp[$i]['large']=str_replace("youtube_id",$item['youtube_id'],"http://img.youtube.com/vi/youtube_id/0.jpg");					
					}
					else {
						$temp[$i]['thumb']=$placeholder_video;
						$temp[$i]['large']=$placeholder_video;
					}
					$temp[$i]['properties']='1';				
					$i++;					
				}				
				$results_count=$results_count+$listing_count;
			}
			
			/* from ext link 2 */
			$condition.=" and properties.id in (select distinct property_id from property_meta where meta_key='ext_link_2' and meta_value like '%youtube%')";	
			if(!empty($property_ids)){
				$condition.=" and properties.id not in (".implode(',',$property_ids).")";
			}
			$select="SELECT $tbl_properties.id, type, (garage + carport + off_street_park) AS carspaces, bedrooms, show_price as display_sold_price,bathrooms, headline, postcode,property_type,display_price as price_display, price, display_price_text as price_display_value, 
				(Case when properties.status='2' Then sold_at	else '' END)as sold_at,
				(Case when properties.status='2' Then sold_price else '' END)as sold_price,
				(Case when properties.status='6' Then leased_at else '' END)as leased_at,
				(Case when properties.status='6' Then leased_price	else '' END)as leased_price, 
				status, deal_type,display_address, unit_number, street_number, street, state, suburb, agent_id, office_id, description, (SELECT meta_value FROM property_meta WHERE property_meta.property_id = id and meta_key='ext_link_2' LIMIT 1) AS ext_link_2 ";
			$sql="$select from properties where $condition ORDER BY `created_at` desc , `street` , `suburb` $sql_limit";
			$listing_count = $wpdb->get_var("SELECT COUNT(*) AS results_count From properties where $condition");
			if($this->debug)print $sql ;		
				
			$return = $wpdb->get_results($sql, ARRAY_A);
			
			if(is_array($return))array_walk($return,array( $this, 'format_property'));
			
			if($listing_count > 0){
				foreach($return as $item){
					$temp[$i]=$item; 
					$item['youtube_id']= trim($item['ext_link_2']); 
					if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $item['youtube_id'], $matches)){
						$item['youtube_id']=$matches[0];
					}
					$temp[$i]['youtube_id']=$item['youtube_id']=str_replace("http://www.youtube.com/embed/","",$item['youtube_id']);
					$temp[$i]['thumb']=str_replace("youtube_id",$item['youtube_id'],"http://img.youtube.com/vi/youtube_id/2.jpg");
					$temp[$i]['large']=str_replace("youtube_id",$item['youtube_id'],"http://img.youtube.com/vi/youtube_id/0.jpg");		
					$temp[$i]['properties']='1';				
					$i++;
				}				
				$results_count=$results_count+$listing_count;
			} 
		}
		
		if($params['display_agent']=='1' && $params['video_type']!='sold_property_videos' && $params['video_type']!='current_property_videos' && $params['video_type']!='other_videos'){
			$office_id=implode(', ', $this->settings['general_settings']['office_id']);
			$user_condition="((video_url!='' and video_url is not null) or (video_embed!='' and video_embed is not null)) and office_id in ($office_id)";
			if($params['agent_ids']!='')$user_condition.=" and id in (".$params['agent_ids'].")";
			$sql="select id, CONCAT(firstname, ' ', lastname) as headline, description, role as position_for_display, `group`, video_url, video_embed from users where $user_condition";
			if($this->debug)print $sql ;		
			$users = $wpdb->get_results($sql, ARRAY_A);
			$agents_count = $wpdb->get_var("SELECT COUNT(*) AS results_count From users where $user_condition");
			if($agents_count > 0){
				foreach($users as $item){
					$temp[$i]=$item; 
					$temp[$i]['agent']='1';
					$temp[$i]['url']=$this->user_url($item['id']);				
					$landscape=$wpdb->get_row("select url from attachments where parent_id= '".intval($item['id'])."' and type='landscape' and description='large' order by position limit 1", ARRAY_A);
					$temp[$i]['thumb']=($landscape['url']=='')?$placeholder_video:$landscape['url'];
					$temp[$i]['large']=($landscape['url']=='')?$placeholder_video:$landscape['url'];
					$i++;
				}
				$results_count=$results_count+$agents_count;			
			}
		}
			
		$return=$temp;
		$return['results_count'] = $results_count;
		if(empty($params['page']))$params['page']='1';
		$pagination = $helper->paginate($params['limit'], $params['page'], $results_count);
		foreach($this->settings['general_settings']['display_pagination'] as $paginate)
			$return['pagination_' . $paginate] = $pagination; //Returns $return['pagination_above'] and/or $return['pagination_below']
		return $return;
	}
	
	function development_properties($params = array(), $send_query=true, $select=''){
		global $helper, $wpdb;
		$return=$this->search($params);
		if(empty($select)){			
			if(empty($params['id']))$select = $this->select(false);
			else $select = $this->select();
		}
		$sql = "$select FROM properties WHERE ".$return['condition'];	
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE ".$return['condition']);
		$sql .= " ".$return['end'];
		if($this->debug)print $sql ;	
				
		if($send_query):	
			$return = $wpdb->get_results($sql, ARRAY_A);
			if(!empty($return)){
				$placeholder_image= $this->paths['theme'].'images/download.png';
				foreach($return as $key=>$row){
					if(!is_array($row))continue;
					if(!empty($row['id'])){
						$photos=$wpdb->get_results("SELECT url,width, height FROM attachments WHERE parent_id = ".$row['id']." and description IN('thumb') and type='photo' and is_user='0' order by position LIMIT 1", ARRAY_A);
						if(!empty($photos)){
							$return[$key]['thumbnail']=$photos[0]['url'];
							$return[$key]['thumbnail_width']=$photos[0]['width'];
							$return[$key]['thumbnail_height']=$photos[0]['height'];
						}
						if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
					}
				}
			}
			if(is_array($return))array_walk($return,array( $this, 'format_property'));			
			$return['results_count'] = $results_count;
			return $return;
 		endif;
		return $sql; 
	}
	
	function map_results($params = array(), $select=''){
		global $helper, $wpdb;
		$return=$this->search($params);
		$select = $this->select(false);
		$sql = "$select FROM properties WHERE ".$return['condition'];	
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE ".$return['condition']);
		$sql .= " ".$return['end'];
		if($this->debug)print $sql ;					
		$return = $wpdb->get_results($sql, ARRAY_A);
		if(!empty($return)){
			$placeholder_image= $this->paths['theme'].'images/download.png';
			foreach($return as $key=>$row){
				if(!is_array($row))continue;
				if(!empty($row['id'])){
				$photos=$wpdb->get_results("SELECT url FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('medium') and type='photo' and is_user='0' order by position LIMIT 1", ARRAY_A);
				$return[$key]['mainimage']=($photos[0]['url']=='')?$placeholder_image:$photos[0]['url'];
				}
			}
		}
		if(is_array($return)) array_walk($return,array( $this, 'format_property'));			
 		return $return;
	}
	
	function property($id=null){
		global $wp_query, $wpdb;
		if(is_null($id))$id = $wp_query->query_vars['property_id'];
		if(is_null($id))$id = $_GET['property_id']; 
		if(is_null($id))return false; 
		$this->is_property_page = true;
		
		if ((is_user_logged_in() || $_POST['preview']==1) && class_exists('zoo_agents'))$row =  $this->properties( array('id'=> $id, 'limit'=>1,'all_listings'=>1));
		else $row =  $this->properties( array('id'=> $id, 'limit'=>1));  // zoo agents code, force listing to display in property page
		
		if ($row['results_count'] < 1) return false;
		$row = $row[0];
		
		
		if($row['type']=='NewDevelopment'){
			$details=$wpdb->get_results("select * from new_development_details where new_development_id=".intval($row['id']), ARRAY_A);
			if($details[0]){
				foreach($details[0] as $key=>$value){
					if(!isset($row[$key]))$row[$key]=$value;
				}
			}
		}
		
		//yourhome
		if(strpos($this->siteUrl,'yourhomeawayfromhome')!==false){
			if(!empty($row['xml_id'])):
				$sherlock_abn = '78150854505'; $sherlock_client_id = '0370570';
				$row['sbid']=$sherlock_abn.$sherlock_client_id.$row['xml_id'];
			else:
				$row['sbid']='0';
			endif;
		}
		
		$row['itemized_features'] = $this->property_features('for display', $row, $id);				
		$row['photos'] = $this->photos($id,"photos", array('200x150', '800x600','400x300'));
		$row['floorplans'] = $this->photos($id, "floorplans", array('800x600'));
		$row['user'][] = $this->user($row['user_id']);
		$row['opentimes'] = $this->opentimes($id);		
		$row['print_version'] = $this->siteUrl . "print-property/?property_id=" .$row['id'];
		$row['brochure'] = $this->brochure($row['id']);
		if(strpos($this->siteUrl,'barclaysbusiness')!==false || strpos($this->siteUrl,'bodyryan')!==false ){ // special adjustment for barclays
			if(count($row['brochure']) > 0)$row['print_version'] = $row['brochure'][0]["url"];
			else $row['print_version'] = "";
		}
		
		$office=$wpdb->get_results("select name, phone, fax, street_number, unit_number, street, state, suburb, zipcode from office where id=".intval($row['office_id']), ARRAY_A);
		$row['office']=$office[0];
		
		if(!empty($row['secondary_user']) && $row['secondary_user']!= $row['user_id'])$row['user'][] = $this->user($row['secondary_user']);			
		if(!empty($row['third_user']) && $row['third_user']!= $row['user_id'] && $row['third_user']!= $row['secondary_user'])$row['user'][] = $this->user($row['third_user']);
		if($row['type']=='HolidayLease'){			
			if(strpos($this->siteUrl,'book')!==false){
				$row['rental_season'] = $this->rental_season2($row['id']);	
				$row['unavailable_date'] = $this->unavailable_date($row['id']);	
			}else{
				$row['rental_season'] = $this->rental_season($id,$row['price'],$row['peak_season_price'],$row['high_season_price'],$row['mid_season_price'],$row['peak_season_period'],$row['high_season_period'],$row['mid_season_period']);
			}
		}
		$this->property = $row;
		return $this->property;
	}
	
	function property_features($purpose="not specified", $fields=array(),$id=''){
		global $wpdb;
		$property_features = $wpdb->get_results("select feature from property_features where property_id=".intval($id)." order by feature");
		switch ($purpose):
			
			case 'for sql':
				$return = implode(", ", array_keys($property_features));
			break;
			
			case 'for display':		
				$new_fields = array();
					for($i=0;$i<count($property_features);$i++){
						array_push($new_fields, $property_features[$i]->feature);
					}
				$return = $new_fields;
			break;
			
			default:
				$return = $property_features;
			break;
			
		endswitch;
		return $return;	
	}

	function property_video($id){
		if(empty($id))return;
		global $wpdb;
		$property_video=$wpdb->get_results("select * from property_video where property_id=".intval($id)." and ((embed_code!='' and embed_code is not null) or (youtube_id!='' and youtube_id is not null))", ARRAY_A);
		return $property_video[0];
	}
	
	function brochure($id=''){
		global $wpdb, $wp_query;
		if(is_null($id))$id = $wp_query->query_vars['property_id']; 
		if(is_null($id))$id = $_GET['property_id']; 
		if(is_null($id))return false; 		
		$result=$wpdb->get_results("select * from attachments where parent_id=".intval($id)." and type='brochure' and is_user='0' and url NOT IN('',' ')", ARRAY_A);
		return $result;
	}
	
	function property_url($id, $type='', $property_type='', $state='', $suburb='', $postcode=''){
		global $wp_rewrite, $helper;
		if(strpos($this->siteUrl,'calibrerealestate')!==false)$url=$this->siteUrl.sanitize_title("property-$property_type-$state-$suburb-$id");
		elseif(strpos($this->siteUrl,'raywhiteprojects')!==false || strpos($this->siteUrl,'raywhitemetro')!==false) {
			if($type=='NewDevelopment' || $type=='ProjectSale' ){ 
				$params=sanitize_title("Projects $suburb $postcode"); $url= $this->siteUrl . "$params/$id/"; 
			}
			else $url=$this->siteUrl . $id .'/' ;
		}
		else $url=$this->siteUrl . $id .'/' ;
		return $url;		
	}
	
	function sold($params=array()){
		if(!isset($params['exclude_id'])){
			$sale_information = $this->settings['sold_and_leased'];
			$exclude_id='';
			foreach($sale_information as $key=>$item):
				if(!is_array($item))$exclude_id.=stripslashes(str_replace("'","",$key)).",";
			endforeach;
			$exclude_id=substr($exclude_id,0,-1);
		}
		if(strpos($params['list'] , 'lease')!==false){
			if($params['order']==''){
				$order='leased_at';
				$order_direction='DESC';
			}else{ 
				$order=$params['order'];
				$order_direction='asc';
			}
			$params = array_merge($params, array( 'status' => 6,'exclude_id'=>$exclude_id, 'leased_at'=>$this->settings['general_settings']['display_leased_listings_for_days'], 'order'=>$order, 'order_direction'=>$order_direction));	
		}else{
			//welsh
			if(strpos($this->siteUrl,'welshrealestate')!==false){
				if(empty($params['order'])){$params['order']='status';$params['order_direction']='DESC, sold_at DESC';}
				$params = array_merge($params, array( 'status' => '2,4','exclude_id'=>$exclude_id, 'sold_at'=>$this->settings['general_settings']['display_sold_listings_for_days']));	
			}else{			
				if($params['order']==''){
					$order='sold_at';
					$order_direction='DESC';
				}else{ 
					$order=$params['order'];
					$order_direction='asc';
				}
				$params = array_merge($params, array( 'status' => 2,'exclude_id'=>$exclude_id, 'sold_at'=>$this->settings['general_settings']['display_sold_listings_for_days'], 'order'=>$order, 'order_direction'=>$order_direction));	
			}
		}
		
		if(isset($params['admin'])){
			global $helper;
			extract($this->search($params));
			$select = $this->select();		
			$sql = "$select FROM properties WHERE $condition";	
			$sql .= " $order";
			if($this->debug)print $sql ;	
			extract($this->admin_paginate('50', $params['admin_page'], 0, $sql)); 
			$return =$wpdb->get_results($sql, ARRAY_A);
			if(is_array($return)) array_walk($return,array( $this, 'format_property'));			
			$return['pagination_label'] = $pagination_label;	
			return $return;
		}
		else{		
			return $this->properties($params);
		}
	}	

	function similar_listings($params){
		global $helper;
		if(!empty($params['property_id'])){ $property=$this->properties(array('id'=>$params['property_id']));$property=$property[0]; }
		else { $property=$this->property; } 

		if(!empty($property['id']))$params['condition'] = 'properties.id != ' . $property['id']; //Exclude the current property from the results		
		if($params['Title']!="Similar Sold Listings"){
			foreach ($params['display'] as $key=>$param)$params[$key] = $property[$key];		
		}
		$params['status']='1,4';
		$params['state']=$property['state'];
		if((int) $params['price_range'] >0):
			if(!$this->property['is_for_sale'])$params['price_range'] /= 1000; //Range for rent will be range divided by 1000;
			$params['price_min'] = (int) $property['based_price'] - (int) $params['price_range'];
			$params['price_max'] = (int) $property['based_price'] + (int) $params['price_range'];
		endif;
		//When $params['list'] is set, we will be looking for sold properties

		return (isset($params['list'])) ? $this->sold($params):$this->properties($params);
	}

	function search($params = array()){
		global $helper, $wpdb; 

		$params = array_merge($this->params, $params);
		$numerics=array('id', 'status', 'price', 'user_id', 'secondary_user', 'max_persons');
		foreach($params as $key=>$value){
			if($key!='condition' && $key!='order'){
				if(!is_array($value)){
					$params[$key]=(in_array($key,$numerics) && strpos($value,',')===false)?intval($value):esc_sql($value);
				}
				else{
					foreach($value as $i=>$item_value){
						$params[$key][$i]=(in_array($key,$numerics) && strpos($value,',')===false)?intval($value):esc_sql($item_value);
					}			
				}
			}
		}				
		if(!empty($params['condition']))$condition .= $params['condition'] ." AND ";

		//suburb categories for yourhome
		if(strpos($this->siteUrl,'yourhomeawayfromhome')!==false){
			$settings = get_option('realty_suburb_categories');
			$beach=(is_array($settings['beach']) && !empty($settings['beach']))?implode("','",$settings['beach']):'';
			$innercity=(is_array($settings['innercity']) && !empty($settings['innercity']))?implode("','",$settings['innercity']):'';
			$cityfringe=(is_array($settings['cityfringe']) && !empty($settings['cityfringe']))?implode("','",$settings['cityfringe']):'';
			$village=(is_array($settings['village']) && !empty($settings['village']))?implode("','",$settings['village']):'';
			$bay=(is_array($settings['bay']) && !empty($settings['bay']))?implode("','",$settings['bay']):'';
			$rural=(is_array($settings['rural']) && !empty($settings['rural']))?implode("','",$settings['rural']):'';
		}
		
		switch($params['list']):
		case '':case 'both':case 'Both':break;
		case 'sale':case 'Sale':
			$condition .= "properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale', 'NewDevelopment')  AND properties.deal_type IN('sale','both','') AND ";
		break;
		case 'lease':case 'Lease':
			$condition .= "properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND  ";
		break;		
		case 'residential_lease':case 'ResidentialLease':
			$condition .= "properties.type IN ('ResidentialLease') AND ";
		break;
		case 'residential_sale':case 'ResidentialSale':
			$condition .= "properties.type IN ('ResidentialSale') AND ";
		break;
		case 'holiday_lease':case 'HolidayLease':
			$condition .= "properties.type IN ('HolidayLease') AND ";
		break;
		case 'project_sale':case 'ProjectSale':
			$condition .= "properties.type IN ('ProjectSale') AND ";
		break;
		case 'NewDevelopment':case 'new_development':
			$condition .= "properties.type IN ('NewDevelopment') AND ";
		break;
		case 'business_sale':case 'BusinessSale':
			$condition .= "properties.type IN ('BusinessSale') AND ";
		break;
		case 'commercial_lease':case 'CommercialLease':
			$condition .= "properties.type IN ('Commercial') AND properties.deal_type IN('lease') AND ";
		break;
		case 'commercial_sale':case 'CommercialSale':
			$condition .= "properties.type IN ('Commercial') AND properties.deal_type IN('sale','both') AND ";
		break;
		case 'commercial':case 'Commercial':
			$condition .= "properties.type IN ('Commercial') AND ";
		break;
		case 'residential':
			$condition .= "properties.type IN ('ResidentialSale', 'ResidentialLease') AND ";
		break;
		case 'ShareAccomodation':case 'share':
			$condition .= "properties.type IN ('ResidentialLease') AND share='1' AND ";
		break;
		case 'waterfront':
			$condition .= "(properties.waterfront >0) AND ";
		break;
		case 'under_offer':
			$condition .= "properties.status IN(4) AND ";
		break;
		case 'latest_sale':
			$condition .= "properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale', 'NewDevelopment') AND properties.deal_type IN('sale','both','') AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`created_at`)<= ".intval($params['days_limit'])." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`created_at`)<= 1 AND ";
		break;
		case 'latest_lease':
			$condition .= "properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`created_at`)<= ".intval($params['days_limit'])." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`created_at`)<= 1 AND ";
		break;
		case 'latest_commercial':
			$condition .= "properties.type IN ('Commercial') AND updated_at!=created_at AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`updated_at`)<= ".$params['days_limit']." AND DATEDIFF(updated_at,`created_at`)>".$params['days_limit']." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`updated_at`)<= 1 AND DATEDIFF(updated_at,`created_at`)>1 AND ";
		break;
		case 'latest_update':
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`updated_at`)<= ".$params['days_limit']." AND DATEDIFF(updated_at,`created_at`)>".$params['days_limit']." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`updated_at`)<= 1 AND DATEDIFF(updated_at,`created_at`)>1 AND ";
		break;
		case 'latest_sale_update':
			$condition .= "properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale', 'NewDevelopment') AND properties.deal_type IN('sale','') AND updated_at!=created_at AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`updated_at`)<= ".intval($params['days_limit'])." AND DATEDIFF(updated_at,`created_at`)>".intval($params['days_limit'])." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`updated_at`)<= 1 AND DATEDIFF(updated_at,`created_at`)>1 AND ";
		break;
		case 'latest_lease_update':
			$condition .= "properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','both','') AND updated_at!=created_at AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`updated_at`)<= ".intval($params['days_limit'])." AND  DATEDIFF(updated_at,`created_at`)> ".intval($params['days_limit'])." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`updated_at`)<= 1 AND  DATEDIFF(updated_at,`created_at`)> 1 AND ";
		break;
		case 'latest_sold':
			if(!empty($params['days_limit']))$condition .= "status='2' AND if (sold_at!='0000-00-00' and sold_at is not null and sold_at!='',DATEDIFF(CURDATE(),`sold_at`)< ".intval($params['days_limit']).",DATEDIFF(CURDATE(),`updated_at`)< 1) AND ";
			else $condition .= "status='2' AND if (sold_at!='0000-00-00' and sold_at is not null and sold_at!='',CURDATE()=sold_at,DATEDIFF(CURDATE(),`updated_at`)< 1) AND ";
		break;
		case 'latest_leased':
			if(!empty($params['days_limit']))$condition .= "status='6' AND if (leased_at!='0000-00-00' and leased_at is not null and leased_at!='',DATEDIFF(CURDATE(),`leased_at`)< ".intval($params['days_limit']).",DATEDIFF(CURDATE(),`updated_at`)< 1) AND ";
			else $condition .= "status='6' AND if (leased_at!='0000-00-00' and leased_at is not null and leased_at!='',CURDATE()=leased_at,DATEDIFF(CURDATE(),`updated_at`)< 1) AND "; 
		break;
		case 'latest_auction':
			if(!empty($params['days_limit']))$condition .= "properties.auction_date >= CURDATE() AND DATEDIFF(properties.auction_date,CURDATE()) <= ".intval($params['days_limit'])."  AND  forthcoming_auction='1' AND ";
			else $condition .= "properties.auction_date >= CURDATE() AND DATEDIFF(properties.auction_date,CURDATE()) <= 7  AND ";
		break;
		case 'upcoming_auction':
			$condition .= "properties.auction_date >= CURDATE() AND forthcoming_auction='1' AND properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale')  AND properties.deal_type IN('sale','both','') AND ";
		break;
		case 'furnished':
			$condition .= "properties.id IN (select property_id from property_features where feature IN ('Furnished')) AND ";
		break;
		case 'newly_built':
			$condition .= "properties.id IN (select property_id from property_features where feature IN ('Newly Built Property')) AND ";
		break;
		case 'lease_prestige':
			$condition .= "properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND  properties.id IN (select property_id from property_features where feature IN ('Prestige Property')) AND ";
		break;
		case 'house_land_packages':
			$condition .= "properties.type IN ('ProjectSale') AND properties.category IN ('House & Land Packages') AND ";
		break;
		case 'new_homes':
			$condition .= "properties.type IN ('ProjectSale') AND properties.category IN ('House Packages') AND ";
		break;
		case 'land_packages':
			$condition .= "properties.type IN ('ProjectSale') AND (properties.property_type ='land' AND properties.category NOT IN ('Land Packages')) AND ";
		break;
		case 'residential_block':
			$condition .= "land_area < 4000  AND properties.property_type IN ('land') AND ";
		break;
		case 'small_acreage_block':
			$condition .= "land_area >= 4000 AND land_area <= 40000 AND properties.property_type IN ('land') AND ";
		break; 
		case 'large_acreage_block':
			$condition .= "land_area > 40000 AND properties.property_type IN ('land') AND ";
		break;
		case 'property_of_the_week':
			$condition .= "properties.id in (select property_id from extras where property_of_week='1') AND ";
		break;
		case 'featured_property':
			$condition .= "properties.id in (select property_id from extras where featured_property='1') AND ";
		break;
		case 'projects':
			$condition .= "new_project_rea=1 AND ";
		break;		
		case 'apartment':		
			$condition .= "properties.property_type IN ('unit','villa','flat','apartment','studio') AND ";
		break;	
		case 'houses':		
			$condition .= "properties.property_type NOT IN ('unit','villa','flat','apartment','studio', 'land') AND ";
		break;	
		case 'furnished_lease':
			$condition .= "properties.id IN (select distinct property_id from property_features where feature = 'furnished') AND properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','both','') AND ";
		break;
		//drebusiness
		case 'subssale':
			$condition .= "properties.type IN ('Commercial', 'BusinessSale', 'ProjectSale') AND properties.deal_type IN('sale','both','') AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`created_at`)<= ".$params['days_limit']." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`created_at`)<= 1 AND ";
		break;
		case 'subslease':
			$condition .= "properties.type IN ('Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`created_at`)<= ".$params['days_limit']." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`created_at`)<= 1 AND ";
		break;
		case 'lsubssale':
			$condition .= "properties.type IN ('Commercial', 'BusinessSale', 'ProjectSale') AND properties.deal_type IN('sale','') AND updated_at!=created_at AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`updated_at`)<= ".$params['days_limit']." AND DATEDIFF(updated_at,`created_at`)>=1 AND ";
			else $condition .= "DATEDIFF(CURDATE(),`updated_at`)<= 1 AND DATEDIFF(updated_at,`created_at`)>=1 AND ";
		break;
		case 'lsubslease':
			$condition .= "properties.type IN ('Commercial', 'HolidayLease') AND properties.deal_type IN('lease','both','') AND updated_at!=created_at AND ";
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`updated_at`)<= ".$params['days_limit']." AND  DATEDIFF(updated_at,`created_at`)>= 1 AND ";
			else $condition .= "DATEDIFF(CURDATE(),`updated_at`)<= 1 AND  DATEDIFF(updated_at,`created_at`)>= 1 AND ";
		break;
		//ian thompson
		case 'unit_projects':
			$condition .= "new_project_rea=1 AND property_type in ('Apartment','Unit') AND ";
		break;	
		case 'land_projects':
			$condition .= "new_project_rea=1 AND property_type in ('Land') AND ";
		break;
		case 'townhouse_projects':
			$condition .= "new_project_rea=1 AND property_type in ('Townhouse') AND ";
		break;
		case 'house_and_land_projects':
			$condition .= "new_project_rea=1 AND category IN ('House & Land Packages') AND ";
		break;
		//micm
		case 'furnished2':
			$condition .= "properties.property_type IN ('unit','villa','flat','apartment','studio') AND properties.type IN ('ResidentialLease') AND properties.id IN (select property_id from property_features where feature IN ('Furnished')) AND ";
		break;
		case 'furnished3':
			$condition .= "properties.property_type IN ('unit','villa','flat','apartment','studio') AND properties.type IN ('ResidentialLease') AND properties.id IN (select property_id from property_features where feature IN ('Furnished','Prestige Property','Newly Built Property')) AND ";
		break;
		case 'corporate_apartments':
			$condition .= "properties.id in (select property_id from extras where field1='1' or  field2='1' or  field3='1') AND ";
		break;
		case 'ca_short_term':
			$condition .= "properties.id in (select property_id from extras where field2='1') AND ";
		break;
		case 'ca_long_term':
			$condition .= "properties.id in (select property_id from extras where field3='1') AND ";
		break;
		//nfn
		case 'acreage/semi-rural/sale':
			$condition .= "properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale') AND properties.property_type IN ('acreage/semi-rural') AND ";
		break;
		//propertyaffairs
		case 'houseboat':
			$condition .= "properties.headline LIKE '%houseboat%' AND ";
		break;
		//rhnewcastle
		case 'house_land':
			$condition .= "properties.property_type LIKE '%land%' OR properties.property_type LIKE '%house%' AND ";
		break;
		//yourhome
		case 'HolidayLease:Beach':
			$condition .= "properties.suburb IN ('$beach') AND ";
		break;
		case 'HolidayLease:InnerCity':
			$condition .= "properties.suburb IN ('$innercity') AND ";
		break;
		case 'HolidayLease:cityfringe':
			$condition .= "properties.suburb IN ('$cityfringe') AND ";
		break;
		case 'HolidayLease:Village':
			$condition .= "properties.suburb IN ('$village') AND ";
		break;
		case 'HolidayLease:Bay':
			$condition .= "properties.suburb IN ('$bay') AND ";
		break;
		case 'HolidayLease:Rural':
			$condition .= "properties.suburb IN ('$rural') AND ";
		break;
		case 'HolidayLease:Beach:New':
			// $condition .= "properties.type IN ('HolidayLease') AND properties.suburb IN ('$beach') AND DATEDIFF(CURDATE(),`created_at`)<= 30 AND ";
			$condition .= "properties.type IN ('HolidayLease') AND DATEDIFF(CURDATE(),`created_at`)<= 30 AND ";
		break;
		case 'HolidayLease:InnerCity:New':
			$condition .= "properties.type IN ('HolidayLease') AND properties.suburb IN ('$innercity') AND DATEDIFF(CURDATE(),`created_at`)<= 30 AND ";
		break;
		case 'HolidayLease:Village:New':
			$condition .= "properties.type IN ('HolidayLease') AND properties.suburb IN ('$village') AND DATEDIFF(CURDATE(),`created_at`)<= 30 AND ";
		break;
		case 'HolidayLease:Bay:New':
			$condition .= "properties.type IN ('HolidayLease') AND properties.suburb IN ('$bay') AND DATEDIFF(CURDATE(),`created_at`)<= 30 AND ";
		break;
		case 'HolidayLease:Rural:New':
			$condition .= "properties.type IN ('HolidayLease') AND properties.suburb IN ('$rural') AND DATEDIFF(CURDATE(),`created_at`)<= 30 AND ";
		break;
		//noosaholiday
		case 'special_offers':		
			$condition .= "properties.id IN (select distinct property_id from extras where field1!='' or  field2!='' or  field3!='') AND ";
		break;
		case 'internet':		
			$condition .= "properties.id IN (select property_id from property_features where feature IN ('Wireless Internet')) AND ";
		break;
		//mq
		case 'hotel':case 'Hotel':
			$condition .= "properties.category IN ('Hotel') AND ";
		break;
		case 'motel':case 'Motel':
			$condition .= "properties.category IN ('Motel') AND ";
		break;	
		case 'park':case 'Park':
			$condition .= "properties.category IN ('Caravan Park') AND ";
			break;
		case 'liquor':case 'Liquor':
			$condition .= "properties.category IN ('Alcohol/Liquor') AND ";
			break;
		case 'restaurant':case 'Restaurant':
			$condition .= "properties.category IN ('Restaurant') AND ";
			break;
		case 'Childcare':case 'childcare':
			$condition .= "properties.category IN ('Child Care') AND ";
			break;	
				case 'general_business':case 'Generalbusiness':
			$condition .= "properties.category NOT IN ('Hotel','Motel','Caravan Park','Alcohol/Liquor','Restaurant','Child Care') AND properties.type  NOT IN ('Commercial') AND ";
		break;
		//api
		case 'latest_listings':
			if(!empty($params['days_limit']))$condition .= "DATEDIFF(CURDATE(),`created_at`)<= ".$params['days_limit']." AND ";
			else $condition .= "DATEDIFF(CURDATE(),`created_at`)<= 1 AND ";
		break;
		default:		
			if(strpos($params['list'],"/")===false && strpos($params['list'],";")===false && strpos($params['list'],":")===false && strpos($params['list'],"-")===false)$condition .= "properties.property_type LIKE '%".$params['list']."%' AND ";
			
			if(strpos($params['list'],"/")!==false){ // combine type & property_type 
				$list=array();
				$list=explode("/",$params['list']);
				for($j=0;$j<count($list);$j++){
					$list[$j]=trim($list[$j]);
					if(!empty($list[$j])){
					switch(strtolower($list[$j])):
						case '' :case 'sale':case 'lease': break;
						case 'apartment':		
							$extra_condition .= "properties.property_type IN ('unit','villa','flat','apartment','studio') OR ";
						break;	
						case 'houses':		
							$extra_condition .= "properties.property_type NOT IN ('unit','villa','flat','apartment','studio', 'land') OR ";
						break;	
						//ddrealestate
						case 'residential_lease':case 'ResidentialLease':
							$condition .= "properties.type IN ('ResidentialLease') AND ";
						break;
						case 'residential_sale':case 'ResidentialSale':
							$condition .= "properties.type IN ('ResidentialSale') AND ";
						break;
						case 'holiday_lease':case 'HolidayLease':
							$condition .= "properties.type IN ('HolidayLease') AND ";
						break;
						case 'project_sale':case 'ProjectSale':
							$condition .= "properties.type IN ('ProjectSale') AND ";
						break;
						case 'business_sale':case 'BusinessSale':
							$condition .= "properties.type IN ('BusinessSale') AND ";
						break;
						case 'commercial':case 'Commercial':
							$condition .= "properties.type IN ('Commercial') AND ";
						break;
						case 'commercial_lease':case 'CommercialLease':
							$condition .= "properties.type IN ('Commercial') AND properties.deal_type IN('lease','both') AND ";
						break;
						case 'commercial_sale':case 'CommercialSale':
							$condition .= "properties.type IN ('Commercial') AND properties.deal_type IN('sale','both') AND ";
						break;
						case 'latest_sale':
							$condition .= "properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale') AND properties.deal_type IN('sale','both','')  AND DATEDIFF(CURDATE(),`created_at`) <= 1 AND ";
						break;
						case 'latest_lease':
							$condition .= "properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','both','') AND DATEDIFF(CURDATE(),`created_at`) <= 1 AND ";
						break;
						case 'latest_sale_update':
							$condition .= "properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale') AND properties.deal_type IN('sale','both','') AND DATEDIFF(CURDATE(),`updated_at`)<= 1 AND updated_at!=created_at AND ";
						break;
						case 'latest_lease_update':
							$condition .= "properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','both','') AND DATEDIFF(CURDATE(),`updated_at`)<= 1 AND updated_at!=created_at AND ";
						break;
						case 'latest_sold':
							$condition .= "status='2' AND ";
						break;
						case 'latest_leased':
							$condition .= "status='6' AND ";
						break;					
						case 'latest_auction':
							$condition .= "properties.auction_date >= CURDATE() AND DATEDIFF(properties.auction_date,CURDATE()) <= 7 AND ";
						break;
						case 'latest_listings':
							$condition .= "DATEDIFF(CURDATE(),`created_at`) <= 7 AND ";
						break;						
						default:
							$extra_condition .="properties.property_type LIKE '%".$list[$j]."%' OR ";
						break;
					endswitch;
					}
				}
				if(!empty($extra_condition))$extra_condition="(".substr($extra_condition,0,-3).") AND ";
				if(strpos($params['list'],"sale")!==false)$extra_condition.="properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale', 'NewDevelopment')  AND properties.deal_type IN('sale','both','') AND ";
				if(strpos($params['list'],"lease")!==false)$extra_condition.="properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND  ";
				$condition .= $extra_condition;
			}
			
			if(strpos($params['list'],";")!==false){ // combine type & features
				$list=array();
				$list=explode(";",$params['list']);
				foreach($list as $value):
					if(strpos($value,"sale")===false && strpos($value,"lease")===false )$all_features .="'".str_replace("_"," ",$value)."',";			
				endforeach;
				$all_features=substr($all_features,0,-1);
				$condition .= "properties.id IN (select property_id from property_features where feature IN (".$all_features.")) AND ";				
				if(strpos($params['list'],"sale")!==false)$condition.="properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale', 'NewDevelopment')  AND properties.deal_type IN('sale','both','') AND ";
				if(strpos($params['list'],"lease")!==false)$condition.="properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND  ";
			}
			
			if(strpos($params['list'],":")!==false){ // combine type & suburbs
				$list=array();
				$list=explode(":",$params['list']);
				foreach($list as $value):
					if(strpos($value,"sale")===false && strpos($value,"lease")===false )$all_suburbs .="'".str_replace("_"," ",$value)."',";			
				endforeach;
				$all_suburbs=substr($all_suburbs,0,-1);
				$condition .= "properties.suburb IN ($all_suburbs) AND ";				
				if(strpos($params['list'],"sale")!==false)$condition.="properties.type IN ('ResidentialSale', 'Commercial', 'BusinessSale', 'ProjectSale', 'NewDevelopment')  AND properties.deal_type IN('sale','both','') AND ";
				if(strpos($params['list'],"lease")!==false)$condition.="properties.type IN ('ResidentialLease', 'Commercial', 'HolidayLease') AND properties.deal_type IN('lease','') AND  ";
			}
			
			if(strpos($params['list'],"-")!==false){ // combine list types
				$list=str_replace("-","','",$params['list']);
				$condition .= "properties.type IN ('$list') AND ";
			}	
		break;
		endswitch;

		if(!empty($params['hour_limit']))$condition .= "DATEDIFF(CURDATE(),`created_at`) <= ".intval($params['hour_limit'])." AND ";
		if(!empty($params['days']))$condition .= "DATEDIFF(CURDATE(),`created_at`) <= ".$params['days']." AND ";
		
		if(!empty($params['land_min']))$condition .= "land_area >= ".intval($params['land_min'])." AND ";
		if(!empty($params['land_max']))$condition .= "land_area <= ".intval($params['land_max'])." AND ";
		if(!empty($params['building_min']))$condition .= "(floor_area >= ".$params['building_min']." OR properties.id in(SELECT distinct property_id FROM property_meta WHERE meta_key='size_from' and meta_value >= ".$params['building_min']." )) AND ";
		if(!empty($params['building_max']) && $params['building_max']!='500000')$condition .= "(floor_area <= ".$params['building_max']." OR properties.id in(SELECT distinct property_id FROM property_meta WHERE meta_key='size_to' and meta_value <= ".$params['building_max']." )) AND ";
		
		if(!empty($params['opentimes']))$condition .= "properties.id IN(SELECT distinct `property_id` FROM opentimes where opentimes.date >= CURDATE()) AND ";
		if(!empty($params['upcoming_auction']))$condition .= "properties.auction_date >= CURDATE() AND forthcoming_auction='1' AND ";
		if(!empty($params['auction_dayname']))$condition .= "DAYNAME(auction_date)='".$params['auction_dayname']."' AND ";
		if(!empty($params['sold_from']))$condition .= " sold_at >='".$params['sold_from']."' AND ";
		if(!empty($params['sold_until']))$condition .= " sold_at <='".$params['sold_until']."' AND ";
		if(!empty($params['key']))$condition .= "(suburb like '%".$params['key']."%' OR state like '%".$params['key']."%'  OR postcode like '%".$params['key']."%'  OR id like '%".$params['key']."%') AND ";
		
		if(!empty($params['project_design_type_name']))$condition .= "properties.id in(select property_id from property_meta where  meta_key='project_design_type_name' and meta_value='".$params['project_design_type_name']."') AND ";
		if(!empty($params['development_name']))$condition .= "properties.id in(select property_id from extras where field1 like '%".$params['development_name']."%') AND ";
		
		if(!empty($params['suburbs']))$condition .= "suburb in('".str_replace(",","','",$params['suburbs'])."') AND ";
		if(!empty($params['ctgr']))$condition .= " id IN (SELECT property_id from extras where dropdown_value IN(select id from extra_options where option_title IN ('".$params['ctgr']."'))) AND ";
		
		if(!empty($params['night_rate_min'])){ $night_rate_min=$params['night_rate_min']*7; $condition .= " price >=$night_rate_min AND ";}
		if(!empty($params['night_rate_max'])){ $night_rate_max=$params['night_rate_max']*7; $condition .= " price <=$night_rate_max AND ";}
		if(!empty($params['min_bed']))$condition .= "bedrooms >= ".$params['min_bed']." AND ";
		if(!empty($params['max_bed']))$condition .= "bedrooms <= ".$params['max_bed']." AND ";
		if(!empty($params['min_bath']))$condition .= "bathrooms >= ".$params['min_bath']." AND ";
		if(!empty($params['max_bath']))$condition .= "bathrooms <= ".$params['max_bath']." AND ";
		
		//shore-commercial
		if(!empty($params['rent_min']) || !empty($params['rent_max'])){			
			if(!empty($params['rent_min']))$rent_condition="meta_value >= ".$params['rent_min']." and ";
			if(!empty($params['rent_max']))$rent_condition.="meta_value <= ".$params['rent_max']." and ";
			$condition .= "properties.id in( select property_id from property_meta where $rent_condition meta_key in('current_rent')) AND ";
		}
		
		//bushbystays
		if(strpos($this->siteUrl,'bushbystays')===false){
			if(!empty($params['date_from']) && !empty($params['date_to'])){
				$condition .= " id IN (SELECT DISTINCT property_id FROM rental_seasons 
				WHERE ( 
							start_date BETWEEN '".$params['date_to']."' AND '".$params['date_from']."' OR 
							end_date BETWEEN '".$params['date_to']."' AND '".$params['date_from']."' OR 
							'".$params['date_from']."' BETWEEN start_date AND end_date OR 
							'".$params['date_to']."' BETWEEN start_date AND end_date  
						)
					AND start_date!='0000-00-00' AND end_date!='0000-00-00') AND ";
			}
			if(empty($params['date_from']) && !empty($params['date_to']))$condition .= " id not in(select distinct property_id from booking_date where `date` !='".$params['date_to']."')) AND id in(select distinct property_id from rental_seasons where `end_date` >='".$params['date_to']."') AND ";
			if(!empty($params['date_from']) && empty($params['date_to']))$condition .= " id not in(select distinct property_id from booking_date where `date` !='".$params['date_from']."') AND id in(select distinct property_id from rental_seasons where `start_date` <='".$params['date_from']."') AND ";
		}
		
		//can-we-book
		if(strpos($this->siteUrl,'book')!==false){
			if(!empty($params['date_from']) && !empty($params['date_to']))$condition .= " id not in(select distinct property_id from booking_date where `date` >='".$params['date_from']."' and `date` <='".$params['date_to']."') AND id in(select distinct property_id from rental_seasons where `start_date` <='".$params['date_from']."' and `end_date` >='".$params['date_to']."') AND ";		
			if(empty($params['date_from']) && !empty($params['date_to']))$condition .= " id not in(select distinct property_id from booking_date where `date` !='".$params['date_to']."')) AND id in(select distinct property_id from rental_seasons where `end_date` >='".$params['date_to']."') AND ";
			if(!empty($params['date_from']) && empty($params['date_to']))$condition .= " id not in(select distinct property_id from booking_date where `date` !='".$params['date_from']."') AND id in(select distinct property_id from rental_seasons where `start_date` <='".$params['date_from']."') AND ";
		}
		
		//  Corporate Apartments		
		if(strpos($condition,'field1')===false && strpos($this->siteUrl,'corporateapartment')!==false)$condition .= "properties.id in (select property_id from extras where  field1='1' or  field2='1' or  field3='1') AND "; 
		if($params['term']=='short')$condition .= "properties.id in (select property_id from extras where field2='1') AND ";
		if($params['term']=='long')$condition .= "properties.id in (select property_id from extras where field3='1') AND ";	

		//dijones
		if(!empty($params['carspaces']) && strpos($this->siteUrl,'dijones')!==false) {
			if (is_array($params['carspaces'])) {
				for($i=0;$i<count($params['carspaces']);$i++) {
					if ($i != (count($params['carspaces'])-1)) $sp = ',';
					else $sp = '';
					$car .= $params['carspaces'][$i] . $sp;
				}
			}
			else {
				$car = $params['carspaces'];
			}
			$condition .= "(garage + carport + off_street_park) IN (".$car.") AND ";
			if ($params['page'] == 1) $params['order'] = 'carspaces';
		}
		
		//portfolio
		if(!empty($params['featured']))$condition .= "properties.id in (select property_id from extras where featured_property='1') AND ";
		if(!empty($params['property_of_week']))$condition .= "properties.id in (select property_id from extras where property_of_week='1') AND ";
					
		//api
		if(!empty($params['house_growth']))$condition .= "property_type like '%house%' and capital_growth >= ".$params['house_growth']." AND ";
		if(!empty($params['house_rent']))$condition .= "property_type like '%house%' and rental_yield >= ".$params['house_rent']." AND ";
		if(!empty($params['unit_growth']))$condition .= "property_type in ('unit','apartment','studio','flat') and capital_growth >= ".$params['unit_growth']." AND ";
		if(!empty($params['unit_rent']))$condition .= "property_type in ('unit','apartment','studio','flat') and rental_yield >= ".$params['unit_rent']." AND ";
		
		if(!empty($params['features'])){
			foreach($params['features'] as $item=>$value):
				$all_features .="'".$value."',";			
			endforeach;
			$all_features=substr($all_features,0,-1);
			$condition .= "properties.id IN (select property_id from property_features where feature IN (".$all_features.")) AND ";
		}

		if(strpos($condition, 'status')===false){
			if(empty($params['status'])) $condition .= "properties.status IN ( $this->sale_status ) AND ";
			else $condition .= "properties.status IN ( ".$params['status']." ) AND ";
		}
		if(!empty($params['exclude_id'])){ $val=$params['exclude_id']; $condition .= "id NOT IN ($val) AND "; }
				
		/* zoo agents code */	
		if(class_exists('zoo_agents')){
			if(!empty($params['office_id'])){ // search by office id 
				$location = " office_id IN (".$params['office_id'].") ";
			}
			elseif(!empty($params['all_listings'])){  
				$rows=$wpdb->get_col("select distinct office_id from properties where office_id  not in (1)");
				$all_office_id=implode(", ",$rows);
				$location = " office_id IN ($all_office_id) ";
			}
			elseif(empty($params['all_office_id']) && empty($params['office_id'])){
				$location = $this->get_location();
			}		
		}else{
			$location = $this->get_location();
		}
		/* zoo agents code */
		
		$location2 = (strpos($location,"office_id")!== false)? substr($location, 7): $location;
		
		$agent_id = (empty($this->settings['general_settings']['agent_id']))? '': implode(', ', $this->settings['general_settings']['agent_id']);
		if(!empty($agent_id)){
			$agent_id=esc_sql($agent_id);
			$condition.="(user_id in ($agent_id) or secondary_user in ($agent_id) or third_user in ($agent_id)) AND ";		
		}
		
		if(!empty($params['status']) && strpos($params['status'],'2')!==false && $params['status']!='2' && (strpos($params['type'],'Sale')!==false || strpos($params['list'],'sale')!==false))$condition .="if (status='2',DATEDIFF(CURDATE(),updated_at) <= '" . intval($this->settings['general_settings']['display_sold_listings_for_days']) . "',created_at is not null) AND ";
		if(!empty($params['status']) && strpos($params['status'],'6')!==false && $params['status']!='6' && (strpos($params['type'],'Lease')!==false || strpos($params['list'],'lease')!==false))$condition .="if (status='6',DATEDIFF(CURDATE(),updated_at) <= '" . intval($this->settings['general_settings']['display_leased_listings_for_days']) . "',created_at is not null) AND ";

		if ( !empty($params['keyword']) ){
			$return=$this->keywords_condition($params['keyword']);
			$condition .= $return['sql_keywords']." AND ";
		}
		$table_cols = array_merge(array('price_min', 'price_max'), $helper->table_colums("properties"));
		foreach($params as $col=>$value):
			if(!in_array($col, $table_cols) || empty($value))continue;
			if($col=='sold_at' || $col=='leased_at'|| $col=='status')continue;
			switch($col):
				case 'user_id':
					$value=intval($value);
					$condition .= 	"( user_id in ($value) or secondary_user in($value) ) AND ";
				break;
				case 'price_min':
					$value=intval($value);
					$condition .= 	"( price >= ". str_replace('+', '',$value) . " ) AND ";
				break;
				case 'price_max':
					$value=intval($value);
					if(strpos($value,'+')===false)$condition .= 	"( price <= ". str_replace('+', '',$value) . " ) AND ";
				break;				
				case 'suburb':
					if(!is_array($value))$value = array($value);
					if($value[0]!='%' && $value[0]!='' ){
						$suburb_all=implode("','",$value);
						if(!empty($suburb_all))$condition .= " properties.$col IN('$suburb_all')  AND ";		
					}
				break;				
				case 'carspaces' :
					$value=trim(str_replace('+', '', $value));
					$value=intval($value);
					$condition .= "(carport + garage + off_street_park) >= $value AND ";
				break;
				
				case 'bedrooms':case 'bathrooms' : //bedrooms can be an array of bedrooms
					if(is_array($value)){
						$condition .= "properties.$col IN ('" . implode("', '", $value) ."') AND ";
					}else{
						$value=trim(str_replace('+', '', $value));
						if($value!='Studio')$condition .= " $col >= $value AND ";
						else $condition .= "properties.$col IN ('$value') AND ";
					}
				break;			
				case 'property_type':
					if(!is_array($value))$value = array($value);
					$property_type_all=implode("','",$value);
					if(!empty($property_type_all))$condition .= " properties.property_type IN('$property_type_all')  AND ";				
				break;
				//portfolio
				case 'rental_yield':
					switch($value){
						case '3-4':$condition .= 'rental_yield>=3 and rental_yield<=4 AND ';break;
						case '4-5':$condition .= 'rental_yield>=4 and rental_yield<=5 AND ';break;
						case '5-6':$condition .= 'rental_yield>=5 and rental_yield<=6 AND ';break;
						case '7-8':$condition .= 'rental_yield>=7 and rental_yield<=8 AND ';break;
						case '9-10':$condition .= 'rental_yield>=8 and rental_yield<=10 AND ';break;
						case '10+':$condition .= 'rental_yield>10 AND ';break;
						default:$condition .= 'rental_yield>='.intval($value).' AND ';break;
					}
				break;
				default:				
					if(is_array($value)){
						$field_all=implode("','",$value);
						if(!empty($field_all))$condition .= " properties.$col IN('$field_all')  AND ";		
					}
					else $condition .= "properties.$col ='$value' AND ";	
				break;
		endswitch;
		endforeach;
	
		/*************************************End of sql starts. E.g. GROUP BY ORDER BY LIMIT***************/	
		$params['page']=($params['page']=='' || $params['page']=='0' )?'1':$params['page'];
		if (!empty($params['limit'])):
			if(isset($params['page']))
				$limit_start = $params['page'] * $params['limit'] - ($params['limit']) . ", ";			
			$params['limit'] = " LIMIT $limit_start " . $params['limit'] ;
		endif;
		
		if(!empty($params['group'])){ 			
			if(in_array($params['group'], $table_cols))$params['group'] = ' GROUP BY `'. $params['group'] . '`';	
			else $params['group']='';
		}
		if ( $params['order'] == "rand")$params['order'] = "RAND( )";
		if($params['order']=='price_desc'){ $params['order'] = "price";$params['order_direction']='desc'; }
		if($params['order']=='updated_at')$params['order_direction']='desc';
		if($params['order']=='auction_date')$params['order_direction']='asc , auction_time asc';
	
		//bushby
		if(strpos($this->siteUrl,'bushby.com.au')!==false){
			if($params['limit']!=1000 && $params['limit']!=1 && !empty($this->featured_listings_ids)){
				foreach($this->featured_listings_ids as $id)$order_by_featured_listing_ids.="(id = $id) DESC, ";										
			}
			if($params['order']=='')$params['order']=$this->params['order'];
			$params['order']=$order_by_featured_listing_ids.$params['order'];
		}
		
		return array('condition'=> $condition .$location, 'end'=>$params['group'] . " ORDER BY " . $params['order'] . " " . $params['order_direction'] . $params['limit'], 'order'=>$params['group'] . " ORDER BY " . $params['order'] . " " . $params['order_direction']);			
			
	}

	function get_location(){
		$office_id = (empty($this->settings['general_settings']['office_id']))? '': "office_id IN (". implode(', ', $this->settings['general_settings']['office_id']) . ")";
		return $office_id;
	}

	function lookup_number($int_value, $sql_keywords){
		global $wpdb;
		$check_postcode=$wpdb->get_var("select distinct postcode from properties where postcode like '$int_value%'");
		$check_id=$wpdb->get_var("select id from properties where id like '%$int_value%' or unique_id like '%$int_value%'");
		if($check_postcode)$sql_keywords.="postcode in ($int_value) AND ";		
		elseif($check_id)$sql_keywords.="(id like '%$int_value%' or unique_id like '%$int_value%') AND ";
		$return=array('sql_keywords'=>$sql_keywords);
		return $return;
	}
	
	function lookup_string($value, $sql_keywords, $sur_suburbs='0'){
		global $wpdb;
		$suburb_region=$wpdb->get_results("show columns from suburb_region");
		$table=($suburb_region)?'suburb_region':'properties';
		if($sur_suburbs==1){
			if(strpos($value,',')!==false){
				$check_all=$wpdb->get_results("select distinct town_village, suburb, state from $table where (concat(suburb,', ',state,', ',postcode)='$value' or concat(suburb,', ',postcode)='$value' or concat(suburb,', ',state,' ',postcode)='$value' or concat(suburb,', ',state)='$value' or CONCAT(town_village,', ',state) like '%$value%')", ARRAY_A);			
				$check_suburb=$wpdb->get_var("select distinct suburb from $table where CONCAT(suburb,', ',state, ', ',postcode) like '%$value%'");
			}			
			
			if(!$check_suburb)$check_suburb=$wpdb->get_var("select distinct suburb from $table where suburb in('$value')");		
			if($check_suburb)$check_town_village=$wpdb->get_var("select distinct town_village from $table where suburb in('$value')");
			if($check_suburb && !$check_town_village)$check_state_similar=$wpdb->get_var("select distinct state from $table where suburb in('$value')");
			
			$check_first_suburb=$wpdb->get_var("select distinct suburb from suburb_region where suburb like '$value%' limit 1");
			if($check_first_suburb)$check_first_town_village=$wpdb->get_var("select distinct town_village from suburb_region where suburb like '$value%'");
			if($check_first_suburb && !$check_first_town_village)$check_like_state_similar=$wpdb->get_var("select distinct state from suburb_region where suburb like '$value%' limit 1");
			
			if($check_all)$sql_keywords.="town_village in ('".$check_all[0]['town_village']."') AND state in ('".$check_all[0]['state']."') AND ";
			elseif($check_town_village && strpos($sql_keywords,'town_village')===false)$sql_keywords.="town_village in ('$check_town_village') AND ";
			elseif($check_first_town_village && strpos($sql_keywords,'town_village')===false)$sql_keywords.="town_village in ('$check_first_town_village') AND ";
			elseif($check_state_similar)$sql_keywords.="state in ('$check_state_similar') AND ";
			elseif($check_like_state_similar)$sql_keywords.="state in ('$check_like_state_similar') AND ";
		}
		else{
			if(strpos($value,',')!==false)$check_all=$wpdb->get_results("select distinct town_village, suburb, state from $table where (concat(suburb,', ',state,', ',postcode)='$value' or concat(suburb,', ',state,' ',postcode)='$value' or concat(suburb,', ',postcode)='$value' or concat(suburb,', ',state)='$value' or CONCAT(town_village,', ',state) like '%$value%')", ARRAY_A);
			$check_state=$wpdb->get_var("select distinct state from $table where state in ('$value')");
			$check_like_state=$wpdb->get_var("select distinct state from $table where state like '$value%'");
			$check_town_village=$wpdb->get_var("select distinct town_village from $table where town_village in ('$value')");
			$check_like_town_village=$wpdb->get_var("select distinct town_village  from $table where town_village like '$value%'");
			if(strpos($sql_keywords,'suburb')===false){
				$check_suburb=$wpdb->get_var("select distinct suburb from $table where suburb in('$value')");				
				$check_first_suburb=$wpdb->get_var("select distinct suburb from $table where suburb like '$value%' limit 1");			
			}
			$check_street=$wpdb->get_var("select id from properties where street like '%$value%' limit 1");
			if(strpos($value,'/')!==false){
				$keywords_array = explode('/',$value);
				$unit_number=$keywords_array[0];
				$street_number=$keywords_array[1];
				$check_street_number=$wpdb->get_var("select id from properties where unit_number like '%$unit_number%' and street_number like '%$street_number%' limit 1");
			}
			$check_street_u_number=$wpdb->get_var("select id from properties where unit_number like '%$value%' limit 1");
			$check_street_s_number=$wpdb->get_var("select id from properties where street_number like '%$value%' limit 1");
			
			if($check_all)$sql_keywords.="(concat(suburb,', ',state,', ',postcode)='$value' or concat(suburb,', ',state,' ',postcode)='$value' or concat(suburb,', ',postcode)='$value' or concat(suburb,', ',state)='$value' or CONCAT(town_village,', ',state) like '%$value%') AND ";
			elseif($check_state)$sql_keywords.="state in ('$value') AND ";
			elseif($check_like_state)$sql_keywords.="state like '$value%' AND ";
			elseif($check_town_village && strpos($sql_keywords,'town_village')===false)$sql_keywords.="town_village in ('$value') AND ";
			elseif($check_like_town_village && strpos($sql_keywords,'town_village')===false)$sql_keywords.="town_village like '$value%' AND ";
			elseif($check_suburb)$sql_keywords.="suburb in ('$value') AND ";
			elseif($check_first_suburb)$sql_keywords.="suburb like '$value%' AND ";
			elseif($check_street)$sql_keywords.="street like '%$value%' AND ";
			elseif($check_street_number)$sql_keywords.="unit_number like '%$unit_number%' and street_number like '%$street_number%' AND ";
			elseif($check_street_u_number)$sql_keywords.="unit_number like '%$value%' AND ";
			elseif($check_street_s_number)$sql_keywords.="street_number like '%$value%' AND ";
		}
	
		if($check_all){
			$suburb=$check_all[0]['suburb'];
			$state=$check_all[0]['state'];
			$town_village=$check_all[0]['town_village'];
		}
		if(empty($suburb))$suburb=($check_suburb!='')?$check_suburb:$check_first_suburb;
		if(empty($state))$suburb=($check_state!='')?$check_state:$check_like_state;
		if(empty($town_village))$town_village=($check_town_village!='')?$check_town_village:$check_like_town_village;
		$return=array('sql_keywords'=>$sql_keywords,'suburb'=>$suburb,'state'=>$state,'town_village'=>$town_village);
		return $return;
	}	
	
	function keywords_condition($keywords, $sur_suburbs='0'){		
		global $wpdb;
		$keywords=trim($keywords);
		$sql_keywords = '';
		if(is_numeric($keywords)){
			$return=$this->lookup_number($keywords, $sql_keywords);
		}
		else{
			$keywords=esc_sql($keywords);
			$return=$this->lookup_string($keywords, $sql_keywords, $sur_suburbs);
		}
		
		if(!empty($return['sql_keywords'])){			
			$return['sql_keywords']=substr($return['sql_keywords'],0,-5);
			if(strpos(strtolower($keywords),'pet')!==false)$return['sql_keywords']="( ".$return['sql_keywords']." OR properties.id in (select id from property_features where feature='Pet Friendly')) ";
			return $return;				
		}	
			
		
		if(strpos($keywords,',')!==false)$keywords_array = explode(',',$keywords);else $keywords_array = explode(' ',$keywords);		
		foreach($keywords_array as $key => $value ) :
			$value = strtoupper(trim($value));									
			if (!empty($value)){																
				$int_value = (int) $value;				
				if ( is_int($int_value) && !empty($int_value) ){
					if($sur_suburbs!='1')$return=$this->lookup_number($int_value, $sql_keywords);
					$sql_keywords=$return['sql_keywords'];
				}else{				
					$value=esc_sql($value);
					$return=$this->lookup_string($value, $sql_keywords, $sur_suburbs);					
					$sql_keywords=$return['sql_keywords'];
					if(!empty($return['suburb']) && empty($suburb))$suburb=$return['suburb'];
					if(!empty($return['state']) && empty($state))$suburb=$return['state'];
					if(!empty($return['town_village']) && empty($town_village))$suburb=$return['town_village'];
				}
			}
		endforeach;
		$sql_keywords=substr($sql_keywords,0,-5);
		if(strpos(strtolower($keywords),'pet')!==false)$sql_keywords="( $sql_keywords OR properties.id in (select id from property_features where feature='Pet Friendly')) ";
		if(trim($sql_keywords)=='')$sql_keywords="(headline like '%$keywords%' or description like '%$keywords%')";
		$return=array('sql_keywords'=>$sql_keywords,'suburb'=>$suburb,'state'=>$state,'town_village'=>$town_village);
		return $return;
		//return $sql_keywords;	
	}
	
	function format_property(&$property){
		global $helper, $wpdb;
		if(!empty($property['id'])){
			$details=$wpdb->get_results("select meta_key, meta_value from property_meta where property_id=".intval($property['id']), ARRAY_A);
			if($details){
				foreach($details as $detail){
					if(!isset($property[$detail['meta_key']]))$property[$detail['meta_key']]=$detail['meta_value'];
				}
				$property['rent_pa']=$property['current_rent'];
				$property['occupancy']=$property['current_leased'];
				$property['plus_outgoings']=$property['outgoings_paid_by_tenant'];
				$property['tax']=$property['current_rent_include_tax'];
			}
			
			if($property['type']=='Commercial' && empty($property['price'])){
				if(!empty($property['rent_pa'])){
					$property['real_price'] = $this->params['default_currency_code'].number_format($property['rent_pa'])." pa";
					$property['based_price'] = $property['price'] = $property['rent_pa'];
				}
				elseif(!empty($property['sale_price'])){
					$property['real_price'] = $this->params['default_currency_code'].number_format($property['sale_price']);
					$property['based_price'] = $property['price'] = $property['sale_price'];
				}
				elseif(!empty($property['rental_price'])){
					$property['real_price'] = $this->params['default_currency_code'].number_format($property['rental_price'])." psm/pa";
					$property['based_price'] = $property['price'] = $property['sale_price'];
				}
			}else{			
				if(!empty($property['price']) && is_numeric($property['price']))$property['based_price'] = $property['price'];
				if(!empty($property['price']) && is_numeric($property['price']))$property['real_price'] = $this->params['default_currency_code']. number_format($property['price']);
			}
			
			switch($property['status']){
				case 1:case 4:
					if($property['status']=='4')$property['under_offer'] = true;
					switch($property['price_display']):
						case 1:
							if(empty($property['price']))
								break;
							$property['price'] = $property['real_price'];
							if($property['type']=='ResidentialLease'){
								if($property['price_period']=='Monthly')$property['price'] .= ' pm';
								else if($property['price_period']=='Nightly')$property['price'] .= ' pn';
								else $property['price'] .= ' pw';
							}
							elseif($property['type']=='HolidayLease' && strpos($this->siteUrl,'book')===false){
								$property['price_period']=$wpdb->get_var("select meta_value from property_meta where meta_key='normal_season_period' and property_id=".intval($property['id']));
								if($property['price_period']=='Monthly')$property['price'] .= ' pm';
								else if($property['price_period']=='Nightly')$property['price'] .= ' pn';
								else $property['price'] .= ' pw';
							}
							elseif($property['type']=='HolidayLease' && strpos($this->siteUrl,'book')!==false){
								if(!empty($property['based_price'])){
									if($property['price_period']=='Monthly'){
										$property['based_price']=$property['normal_season_price']= ceil(1.055*$property['based_price']/30);
										$property['normal_season_price_week']= ceil($property['based_price']/30*7);
										$property['price'] =  '$'.$property['normal_season_price'].' pn';
									}
									else if($property['price_period']=='Nightly' || empty($property['price_period'])){
										$property['based_price']=$property['normal_season_price']= ceil(1.055*$property['based_price']);
										$property['normal_season_price_week']= ceil($property['based_price']*7);
										$property['price'] =  '$'.$property['normal_season_price'].' pn';									
									}
									else if($property['price_period']=='Weekly'){
										$property['based_price']=$property['normal_season_price']= ceil(1.055*$property['based_price']/7);
										$property['normal_season_price_week']= ceil($property['based_price']);
										$property['price'] =  '$'.$property['normal_season_price'].' pn';
									}
								}else{
									$property['price']='';
									$min_pn=$wpdb->get_var("select min(price_per_weekday) from rental_seasons where price_per_weekday is not null and price_per_weekday!=0 and property_id=".$property['id']);		
									$max_pn=$wpdb->get_var("select max(price_per_weekend) from rental_seasons where price_per_weekend is not null and price_per_weekend!=0 and property_id=".$property['id']);
									if(empty($max_pn))$max_pn=$wpdb->get_var("select max(price_per_weekday) from rental_seasons where price_per_weekday is not null and price_per_weekday!=0 and property_id=".$property['id']);
									if(empty($min_pn))$min_pw=$wpdb->get_var("select min(price_per_week) from rental_seasons where price_per_week is not null and price_per_week!=0 and property_id=".$property['id']);
									if(empty($max_pn))$max_pw=$wpdb->get_var("select max(price_per_week) from rental_seasons where price_per_week is not null and price_per_week!=0 and property_id=".$property['id']);
									if(!empty($min_pn)){
										$property['normal_season_price']=ceil(1.055*$min_pn);
										$property['normal_season_price_week']=ceil(1.055*$min_pn*7);
										$property['price'] ="$".$property['normal_season_price']." p/n";
										if(!empty($max_pn) && $max_pn!=$min_pn)$property['price'].= "- $".ceil(1.055*$max_pn)." p/n";
									}
									elseif(!empty($min_pw)){
										$property['normal_season_price']=ceil(1.055*$min_pw/7);
										$property['normal_season_price_week']=ceil(1.055*$min_pw);
										$property['price'] = "$".$property['normal_season_price']." p/n";
										if(!empty($max_pw) && $max_pw!=$min_pw)$property['price'].= "- $".ceil(1.055*$max_pw/7)." p/n";
									}
								}
							}
						break;			
						case 2:
							$property['price'] = $property['price_display_value'];
						break;
						default:
							$property['price'] = '';			
						break;			
					endswitch;	
				break;
				case 2:
					$property['last_price'] = (empty($property['sold_price']))? $property['real_price']: '$'. number_format($property['sold_price']);
					$property['price'] = 'SOLD';
					$property['date'] = $property['sold_at'];
					$property['is_sold'] = true;
				break;
				case 6:
					$property['last_price'] = (empty($property['leased_price']))? $property['real_price'] .  ' pw': '$'. number_format($property['leased_price']) . ' pw';
					$property['price'] = 'LEASED';
					$property['date'] = $property['leased_at'];
					$property['is_leased'] = true;			
				break;
				default:			
				break;
			}
			$property['is_for_sale'] = (stripos($property['type'],"Lease")=== false && strtolower($property['deal_type'])!='lease');
			$property['listing_type'] = (stripos($property['type'],"Lease")=== false && strtolower($property['deal_type'])!='lease')?'FOR SALE':'FOR LEASE';
			switch(trim($property['land_area_metric'])){
				case 'Square Meters':case 'Square Metres':case '':$property['land_area_metric']='m2';break;
				case 'Squares':$property['land_area_metric']='Sqrs';break;
				case 'Square Feet':$property['land_area_metric']='Sqft';break;
				
			}
			switch(trim($property['floor_area_metric'])){
				case 'Square Meters':case 'Square Metres':case '':$property['floor_area_metric']='m2';break;
				case 'Squares':$property['floor_area_metric']='Sqrs';break;
				case 'Square Feet':$property['floor_area_metric']='Sqft';break;
			}	
			switch(trim($property['office_area_metric'])){
				case 'Square Meters':case 'Square Metres':case '':$property['office_area_metric']='m2';break;
				case 'Squares':$property['office_area_metric']='Sqrs';break;
				case 'Square Feet':$property['office_area_metric']='Sqft';break;
				case 'Hectares':$property['office_area_metric']='Ha';break;	
			}
			switch(trim($property['other_area_metric'])){
				case 'Square Meters':case 'Square Metres':case '':$property['other_area_metric']='m2';break;
				case 'Squares':$property['other_area_metric']='Sqrs';break;
				case 'Square Feet':$property['other_area_metric']='Sqft';break;
				case 'Hectares':$property['other_area_metric']='Ha';break;	
			}
			switch(trim($property['retail_area_metric'])){
				case 'Square Meters':case 'Square Metres':case '':$property['retail_area_metric']='m2';break;
				case 'Squares':$property['retail_area_metric']='Sqrs';break;
				case 'Square Feet':$property['retail_area_metric']='Sqft';break;
				case 'Hectares':$property['retail_area_metric']='Ha';break;	
			}
			switch(trim($property['warehouse_area_metric'])){
				case 'Square Meters':case 'Square Metres':case '':$property['warehouse_area_metric']='m2';break;
				case 'Squares':$property['warehouse_area_metric']='Sqrs';break;
				case 'Square Feet':$property['warehouse_area_metric']='Sqft';break;
				case 'Hectares':$property['warehouse_area_metric']='Ha';break;	
			}
			$property['is_land'] = stripos($property['property_type'], 'land');
			$property['property_video'] = $this->property_video($property['id']);
			$property['url'] = $this->property_url($property['id'], $property['type'], $property['property_type'], $property['state'], $property['suburb'], $property['postcode']);
			$this->glue_address($property);
			$property['pool']=$property['have_pool']=$wpdb->get_var("select count(id) from property_features where feature like '%pool%' and property_id=".$property['id']);
			$property['booking_id']=$wpdb->get_var("select field1 from extras where property_id=".intval($property['id']));
			$property['agentID'] = ($property['office_id']=='447')?'AP447':'AP718'; //fnuupercomera
			if(!empty($property['suburb']) && strpos($this->siteUrl,'dijones')===false)$property['suburb'] = ucwords(strtolower($property['suburb']));
			//liveagent
			if(strpos($this->siteUrl,'liveagent')!==false){
			$extras = $this->extra($property['id']);
				if(!empty($extras)){
					$extra=$extras[0];
					if(!empty($extra['description']) && (!empty($extra['property_of_week']) || !empty($extra['featured_property'])))$property["extra_description"] = nl2br($extra['description']);
				}
			}
			//bushby
			if(strpos($this->siteUrl,'bushby.com.au')!==false){
				if(!empty($this->featured_listings_ids) && in_array($property['id'],$this->featured_listings_ids))$property['featured_listings']=1;	
				$property['floorplans'] = $this->photos($property['id'], "floorplans", array('800x600'));	
			}
			//noosaholidays
			if(strpos($this->siteUrl,'noosaholidays')!==false){
				if(!empty($property['mid_season_price'])){
					switch($property['mid_season_period']){
						case 'Nightly':	case '':case 'Please select':	
							$property['mid_season_price_week']='$'.number_format($property['mid_season_price']*7);
							$property['mid_season_price']='$'.number_format($property['mid_season_price']);					
						break;
						case 'Weekly':
							$property['mid_season_price_week']='$'.number_format($property['mid_season_price']);
							$property['mid_season_price']='$'.number_format($property['mid_season_price']/7);					
						break;
						case 'Monthly':
							$property['mid_season_price_week']='$'.number_format($property['mid_season_price']/30*7);
							$property['mid_season_price']='$'.number_format($property['mid_season_price']/30);					
						break;
					}
				}
				if(!empty($property['high_season_price'])){
					switch($property['high_season_period']){
						case 'Nightly':case '':case 'Please select':					
							$property['high_season_price_week']='$'.number_format($property['high_season_price']*7);
							$property['high_season_price']='$'.number_format($property['high_season_price']);					
						break;
						case 'Weekly':
							$property['high_season_price_week']='$'.number_format($property['high_season_price']);
							$property['high_season_price']='$'.number_format($property['high_season_price']/7);					
						break;
						case 'Monthly':
							$property['high_season_price_week']='$'.number_format($property['high_season_price']/30*7);
							$property['high_season_price']='$'.number_format($property['high_season_price']/30);					
						break;
					}
				}
				if(!empty($property['peak_season_price'])){
					switch($property['peak_season_period']){
						case 'Nightly':case '':case 'Please select':					
							$property['peak_season_price_week']='$'.number_format($property['peak_season_price']*7);
							$property['peak_season_price']='$'.number_format($property['peak_season_price']);					
						break;
						case 'Weekly':
							$property['peak_season_price_week']='$'.number_format($property['peak_season_price']);
							$property['peak_season_price']='$'.number_format($property['peak_season_price']/7);					
						break;
						case 'Monthly':
							$property['peak_season_price_week']='$'.number_format($property['peak_season_price']/30*7);
							$property['peak_season_price']='$'.number_format($property['peak_season_price']/30);					
						break;
					}
				}
			}
		}
	}

	function glue_address(&$pieces){ 
		if($pieces['display_address']==1):
			if(!empty($pieces['unit_number']))
				$pieces['street_address'] = $pieces['unit_number'] .'/';
			$pieces['street_address'] .=  $pieces['street_number'] . ' ' .  $pieces['street']  ;
		else:
			$pieces['street_address']=''; //Unset the address variable which has the value of the previous property
		endif;
	}

	function photos($property_id, $photo_or_floor = "photos", $sizes = array('100x75','800x600')) {
		global $wpdb;		
		$photo_or_floor=substr($photo_or_floor,0,-1);
		$return=array();
		$sql="select url, description ,`position`, width, height from attachments where parent_id='".intval($property_id)."' and type='$photo_or_floor' ORDER BY `position`, `description` ";
		$results=$wpdb->get_results($sql, ARRAY_A);
		if($results){
			foreach($results as $item){
				if(!empty($item['url'])){
					if($item['description']=='thumb')$item['description']='small';
					$return[$item['position']][$item['description']]=$item['url'];
					$return[$item['position']][$item['description'].'_width']=$item['width'];
					$return[$item['position']][$item['description'].'_height']=$item['height'];
				}
			}
		}
		return $return;
	}//ends function
		
	function opendates($list = 'both', $days=''){
		global $wpdb;
		/*Simply get all the distinct and not-past dates in which properties from this office are open for inspection*/
		$date_condition = $this->properties(array('list'=>$list, 'limit'=>'', 'status' =>'1,4'), false, "SELECT `id`"); 
		if(!empty($days))$extra_condition=" AND DATEDIFF(opentimes.date,CURDATE()) <= ".$days;
		$opendates = $wpdb->get_results("SELECT `date`, `start_time`, end_time, `property_id` FROM opentimes WHERE property_id IN ($date_condition) and  opentimes.date >= CURDATE() $extra_condition ORDER BY `date` ASC, `start_time` ASC", ARRAY_A);
		foreach ($opendates as $opendate):
			$property = $this->properties(array('id'=>$opendate['property_id']) );
			$property[0]['start_time']=$opendate['start_time'];
			$property[0]['end_time']=$opendate['end_time'];
			$all_opentimes[$opendate['date']][] = $property[0];
		endforeach;
		return $all_opentimes;
	}

	function opentimes($property_id,$condition=''){
		global $wpdb;
		$sql = "SELECT opentimes.date, opentimes.start_time, opentimes.end_time FROM opentimes WHERE `property_id`='".intval($property_id)."' and  opentimes.date >= CURDATE() $condition
				ORDER BY opentimes.date ASC, opentimes.start_time ASC";
		return $wpdb->get_results($sql, ARRAY_A);
	}
	
	function property_search($tag,$list='',$state='',$status='1,4', $property_type=''){
	
		if($tag!='type')$tag_condition = array('list'=>$list,'type'=>$type,'state'=>$state,'group'=>$tag, 'order'=>$tag,'order_direction'=>'ASC','limit'=>1000, 'page'=>'1');
		else $tag_condition = array('list'=>$list,'group'=>$tag, 'order'=>$tag,'order_direction'=>'DESC','limit'=>1000, 'page'=>'1');
		
		$tag_condition['status']=$status;
		$tag_condition['condition']="$tag!='' and $tag is not null";
		
		$results = $this->properties($tag_condition, true, "SELECT $tag AS tag, COUNT(*) AS number_of_properties");
		return $results;
	} 
	
	//bushby
	function users2($condition='', $order='', $limit='',$group='', $admin=false , $admin_page='1'){
		global $helper,$wp_query,$wpdb;
		$placeholder_image= $this->paths['theme'].'images/default_avatar.jpg';
		$condition = (empty($condition))? $this->get_location(): $this->get_location() . " AND $condition";
		if(!empty($group))$condition .= " AND (`group`='".$group."' or role='".$group."')";		
		if(empty($order) && !empty($limit)){ 	
			$row = $wpdb->get_col("select id from users where $condition and id in(select distinct parent_id from attachments where is_user=1 and type='portrait')");
			shuffle($row);
			if($limit!=1){
				$new_id=array(); 
				for($i=0;$i<=count($row);$i++){ if(count($new_id)==$limit)continue;array_push($new_id, $row[$i]);} 
				$new_id=implode(",",$new_id); 
			}
			if($limit==1)$new_id=$row[0];
			$sql="select id as user_id, office_id, email, fax, phone as business_phone,CONCAT(firstname, ' ', lastname) as name, role as position_for_display,  mobile, description, `group`, display from users where id IN($new_id)"; 
		}
		else{		
			if(!$admin)$condition.=" and (display !='0' or display is null )";				
			$order = ($order=='')? "order by (category='Directors') DESC,(category='Sales Department') DESC,(category='Property Management Department') DESC,(category='Technology and Marketing Department') DESC,(category='Administration Department') DESC,position":"order by $order";	
			$sql = "select id as user_id, office_id, email, fax, phone as business_phone,CONCAT(firstname, ' ', lastname) as name, role as position_for_display,  mobile, facebook_username, twitter_username, linkedin_username, description, `group`, vcard_title, vcard_path, video_url, video_embed, audio, display, category from users where $condition $order $limit";
		}  
		
		if($this->debug)print $sql ;		
		
		if($admin){
			$return=$this->admin_paginate('50', $admin_page, $results_count, $sql); 
			$results =$wpdb->get_results($sql, ARRAY_A);
			$results['pagination_label'] = $return['pagination_label'];	
		}
		
		if(!$admin){
			$results = $wpdb->get_results($sql, ARRAY_A);	
			if(!empty($results)){
				foreach($results as $key=>$row):
					$photo=$wpdb->get_var("select url from attachments where parent_id= '".intval($row['user_id'])."' and type='portrait' and description='large' order by position limit 1");
					$results[$key]['photo']=($photo=='')?$placeholder_image:$photo;
					$landscape=$wpdb->get_var("select url from attachments where parent_id= '".intval($row['user_id'])."' and type='landscape' and description='large' order by position limit 1");
					$results[$key]['photo_landscape']=($landscape=='')?$placeholder_image:$landscape;
					$results[$key]['big_landscape']=$wpdb->get_var("select url from attachments where parent_id= '".$row['user_id']."' and type='landscape' and description='large' and position='1' limit 1");	
					$results[$key]["description"] = nl2br($row["description"]);
					$results[$key]["url"] = $this->user_url($row['user_id'], $row['name']);
				endforeach;
			}
		}

		
		

		return $results;		
	}
	
	function users($condition='', $order='', $limit='',$group='', $admin=false , $admin_page='1'){
		global $helper,$wp_query, $wpdb;
		$placeholder_image= $this->paths['theme'].'images/default_avatar.jpg';
		$condition = (empty($condition))? $this->get_location(): $this->get_location() . " AND $condition";
		$agent_id = (empty($this->settings['general_settings']['agent_id']))? '': implode(', ', $this->settings['general_settings']['agent_id']);
		if(!empty($agent_id))$condition.=" AND (id in (".esc_sql($agent_id)."))";	
		if(!empty($group))$condition .= " AND (`group`='".esc_sql($group)."' or role='".esc_sql($group)."' or users.id in (select distinct agent_user_id from user_groups where `group`='".esc_sql($group)."'))";		
		if(empty($order) && !empty($limit)){ 	
			$row = $wpdb->get_col("select id from users where $condition and id in (select distinct parent_id from attachments where is_user='1' and type='portrait')");
			shuffle($row);
			if($limit!=1){
				$new_id=array(); 
				for($i=0;$i<=count($row);$i++){ if(!empty($row[$i])){ if(count($new_id)==$limit)continue;array_push($new_id, $row[$i]);} } 
				$new_id=implode(",",$new_id); 
			}
			if($limit==1)$new_id=$row[0];
			$sql="select id as user_id, office_id, email,fax, phone as business_phone,CONCAT(firstname, ' ', lastname) as name, role as position_for_display,  mobile, facebook_username, twitter_username, linkedin_username, description, `group`, vcard_title, vcard_path, video_url, video_embed, audio, display from users where id IN(".esc_sql($new_id).")";
		}
		else{		
			if(!$admin){
				//dijones
				if(strpos($this->siteUrl,'dijones')!==false){
					$params['page'] = $wp_query->query_vars['page'];
					$params['page']=($params['page']=='')?'1':$params['page'];
					$params['limit']=12;
					if(isset($params['page']))$limit_start = $params['page'] * $params['limit'] - ($params['limit']) . ", ";			
					$limit = " LIMIT $limit_start " . $params['limit'] ;
				}
				$condition.=" and (display !='0' or display is null )";				
			}
			$order = ($order=='')? "order by position":"order by $order";	
			$sql = "select id as user_id, office_id, email,fax, phone as business_phone,CONCAT(firstname, ' ', lastname) as name, role as position_for_display,  mobile, facebook_username, twitter_username, linkedin_username, description, `group`, vcard_title, vcard_path, video_url, video_embed, audio, display, display_on_team_page from users where $condition $order $limit";
		}  
		
		if($this->debug)print $sql ;		
		
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM users WHERE $condition");
		if($admin){
			$return=$this->admin_paginate('50', $admin_page, $results_count, $sql); 
			$results =$wpdb->get_results($sql, ARRAY_A);
			$results['pagination_label'] = $return['pagination_label'];	
		}
		
		if(!$admin){
			$results = $wpdb->get_results($sql, ARRAY_A);	
			if(!empty($results)){
				foreach($results as $key=>$row):
					$big_photo=$wpdb->get_var("select url from attachments where parent_id= '".intval($row['user_id'])."' and type='portrait' and description='large' order by position desc");
					$photo=$wpdb->get_var("select url from attachments where parent_id= '".intval($row['user_id'])."' and type='portrait' and description='large' order by position limit 1");
					$results[$key]['photo']=($photo=='')?$placeholder_image:$photo;
					$landscape=$wpdb->get_var("select url from attachments where parent_id= '".intval($row['user_id'])."' and type='landscape' and description='large' order by position limit 1");
					$results[$key]['photo_landscape']=($landscape=='')?$placeholder_image:$landscape;
					$results[$key]['big_landscape']=$wpdb->get_var("select url from attachments where parent_id= '".$row['user_id']."' and type='landscape' and description='large' and position='1' limit 1");	
					$results[$key]['big_photo']=($big_photo=='')?$results[$key]['photo']:$big_photo;
					$results[$key]["description"] = nl2br($row["description"]);
					$results[$key]["url"] = $this->user_url($row['user_id'], $row['name']);
				endforeach;
			}			
			$results['results_count'] = $results_count;
			$pagination = $helper->paginate('10', $params['page'], $results_count);
			$results['pagination_above'] = $pagination; 
		}
		return $results;		
	}
	
	function admin_paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $wpdb;
		if($page < 1)$page = 1;
			
		if(!empty($sql) && $results_count==0) 
			if(($query =$wpdb->query($sql)) != false)
				$results_count = count($query);
		if ($limit < $results_count ):				
			$limit_start = $page * $limit - ($limit);
			if($limit_start > $results_count)
				$limit_start = $results_count - $limit;
			$sql_limit = " LIMIT $limit_start, $limit";			
		endif;
		$return['sql'] = $sql . $sql_limit;
		$max_page_links = 10; 		
		$page_link = $page - floor($max_page_links / 2); 
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
		
		$current_query = "?" . preg_replace('/&(pg=)([-+]?\\d+)/is', '', $_SERVER['QUERY_STRING']) . "&amp;pg=";
		
		$prev_page = $page - 1;
		$next_page = $page + 1;
		ob_start();
		require('admin/pagination_label.php');
		$return['pagination_label'] = ob_get_clean();
		return $return;
	}
	
	function userrandom( $condition = '', $order = '', $limit = '', $group = '', $admin = false, $admin_page = '1' )
    {
        global $helper, $wp_query, $wpdb;
        $placeholder_image = $this->paths['theme'] . 'images/default_avatar.jpg';
        $condition = (empty( $condition )) ? $this->get_location() : $this->get_location() . " AND $condition";
        $agent_id = (empty( $this->settings['general_settings']['agent_id'] )) ? '' : implode( ', ', $this->settings['general_settings']['agent_id'] );
        if ( !empty( $agent_id ) )
            $condition.=" AND (id in (" . esc_sql( $agent_id ) . "))";
        if ( !empty( $group ) )
            $condition .= " AND (`group`='" . esc_sql( $group ) . "' or role='" . mysql_real_escape_string( $group ) . "' or users.id in (select distinct agent_user_id from user_groups where `group`='" . mysql_real_escape_string( $group ) . "'))";
        if ( empty( $order ) && !empty( $limit ) ) {
            $row = $wpdb->get_col( "select id from users where $condition and id in (select distinct parent_id from attachments where is_user='1' and type='portrait')" );
            shuffle( $row );
            if ( $limit != 1 ) {
                $new_id = array();
                for ( $i = 0; $i <= count( $row ); $i++ ) {
                    if ( !empty( $row[$i] ) ) {
                        if ( count( $new_id ) == $limit )
                            continue;array_push( $new_id, $row[$i] );
                    }
                }
                $new_id = implode( ",", $new_id );
            }
            if ( $limit == 1 )
                $new_id = $row[0];
            $sql = "select id as user_id, office_id, email,fax, phone as business_phone,CONCAT(firstname, ' ', lastname) as name, role as position_for_display,  mobile, facebook_username, twitter_username, linkedin_username, description, `group`, vcard_title, vcard_path, video_url, video_embed, audio, display from users where id IN(" . esc_sql( $new_id ) . ") Order by rand() LIMIT 1,0";
        }
        else {
            if ( !$admin ) {
                //dijones
                if ( strpos( $this->siteUrl, 'dijones' ) !== false ) {
                    $params['page'] = $wp_query->query_vars['page'];
                    $params['page'] = ($params['page'] == '') ? '1' : $params['page'];
                    $params['limit'] = 12;
                    if ( isset( $params['page'] ) )
                        $limit_start = $params['page'] * $params['limit'] - ($params['limit']) . ", ";
                    $limit = " LIMIT $limit_start " . $params['limit'];
                }
                $condition.=" and (display !='0' or display is null )";
            }
            $order = "Order by rand()";
            $sql = "select id as user_id, office_id, email,fax, phone as business_phone,CONCAT(firstname, ' ', lastname) as name, role as position_for_display,  mobile, facebook_username, twitter_username, linkedin_username, description, `group`, vcard_title, vcard_path, video_url, video_embed, audio, display, display_on_team_page from users where $condition $order LIMIT 1";
        }

        if ( $this->debug )
            print $sql;

        if ( !$admin ) {
            $results = $wpdb->get_results( $sql, ARRAY_A );
            if ( !empty( $results ) ) {
                foreach ( $results as $key => $row ):
                    $big_photo = $wpdb->get_var( "select url from attachments where parent_id= '" . intval( $row['user_id'] ) . "' and type='portrait' and description='large' order by position desc" );
                    $photo = $wpdb->get_var( "select url from attachments where parent_id= '" . intval( $row['user_id'] ) . "' and type='portrait' and description='large' order by position limit 1" );
                    $results[$key]['photo'] = ($photo == '') ? $placeholder_image : $photo;
                    $landscape = $wpdb->get_var( "select url from attachments where parent_id= '" . intval( $row['user_id'] ) . "' and type='landscape' and description='large' order by position limit 1" );
                    $results[$key]['photo_landscape'] = ($landscape == '') ? $placeholder_image : $landscape;
                    $results[$key]['big_landscape'] = $helper->get_var( "select url from attachments where parent_id= '" . $row['user_id'] . "' and type='landscape' and description='large' and position='1' limit 1" );
                    $results[$key]['big_photo'] = ($big_photo == '') ? $results[$key]['photo'] : $big_photo;
                    $results[$key]["description"] = nl2br( $row["description"] );
                    $results[$key]["url"] = $this->user_url( $row['user_id'], $row['name'] );
                endforeach;
            }
        }
        return $results;
    }
	
	function user($search, $recursive = false){
		global $helper, $wpdb;
		$search = trim(strtolower($search));
		if(empty($search)) return;
		$user = $this->users("(id='$search' OR CONCAT(trim(`firstname`), '_',trim( `lastname`)) LIKE '%$search%')");
		$user = $user[0];
		$user['user_id']=intval($user['user_id']);
		if($recursive):
			$user['listings'] = $this->team_properties(array('user_id'=>$user['user_id'], 'status' => '1,4')); 
			$user['sold_listings'] = $this->team_properties(array('user_id'=>$user['user_id'], 'list'=>'sale', 'status' => 2, 'order'=>'sold_at', 'order_direction'=>'DESC'));
			$user['lease_listings'] = $this->team_properties(array('user_id'=>$user['user_id'], 'list'=>'lease', 'status' => 6, 'order'=>'leased_at', 'order_direction'=>'DESC'));
			//newtonrealestate
			if(strpos($this->siteUrl,'newtonrealestate')!==false){
				$user['map_listings'] = $this->map_properties(array('user_id'=>$user['user_id'], 'status' => '1,4', 'page'=>1)); 
				$user['open'] = $this->team_properties(array('user_id'=>$user['user_id'], 'status' => '1,4', 'opentimes'=>'1')); 
				$user['map_sold_listings'] = $this->map_properties(array('user_id'=>$user['user_id'], 'list'=>'sale', 'status' => 2, 'order'=>'sold_at', 'order_direction'=>'DESC', 'page'=>1)); 
			}
			if(!empty($this->settings['team'][$user['user_id']]['wpUser']))$user['posts'] = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type='post' and post_status='publish' and `post_author`=" . intval($this->settings['team'][$user['user_id']]['wpUser']), 'object');
			$user['testimonials'] = $wpdb->get_results("SELECT `content`, `user_name` FROM testimonials WHERE `user_id` =".intval($user['user_id']), ARRAY_A);
		endif;
		return $user;
	}

	function user_url($id, $name=''){		
		if(strpos($this->siteUrl,'liveagent')!==false)return $this->siteUrl."real-estate-agent-$id/";
		elseif(strpos($this->siteUrl,'raywhitemetro')!==false){
			$name=sanitize_title($name); return $this->siteUrl . "agent/$name/"; 
		}
		// else return $this->team_url . "?u=$id";
		//url agent page
		else $name=strtolower(str_replace(" ", "", $name)); return $this->siteUrl . "$name/"; 
	}
	
	function offices($params=array(),$is_admin=false ){
		global $helper, $wpdb, $wp_query;
		switch($params['state']){
			case "new south wales":$state="new south wales','nsw";break;
			case "western australia":$state="western australia','wa";break;
			case "south australia":$state="south australia','sa";break;
			case "queensland":$state="queensland','qld";break;
			case "victoria":$state="victoria','vic";break;
			case "australian capital territory":$state="australian capital territory','act";break;
			case "tasmania":$state="tasmania','tasmanian','tas";break;
			default:$state=$state;break;
		}

		if(!empty($params['office_name']))$condition = "name like '%".esc_sql($params['office_name'])."%'";
		if(!empty($params['suburb']))$condition = "suburb like '%".esc_sql($params['suburb'])."%'";
		if(!empty($params['state']))$condition = (empty($condition))? "state in ('".esc_sql($params['state'])."')": "state in ('".esc_sql($params['state'])."') AND $condition";
		
		$office_id_all= "id IN (". implode(', ', $this->settings['general_settings']['office_id']) . ")";
		
		if(strpos($condition,'office_id')===false)$condition = (empty($condition))? $office_id_all: $office_id_all . " AND $condition";			
		
		if(!$is_admin){
			$params['page'] = $wp_query->query_vars['page'];
			$params['page']=($params['page']=='')?'1':$params['page']; 
			$params['limit']='10';
			if(isset($params['page']))$limit_start = $params['page'] * $params['limit'] - ($params['limit']) . ", ";	
			$limit = " LIMIT $limit_start " . $params['limit'] ;	
		}		
		
		$sql = "SELECT * FROM office  where $condition order by name $limit";
		if($this->debug)print $sql ;	
		$results=$wpdb->get_results($sql, ARRAY_A); 
		
		$results['results_count']=$wpdb->get_var("SELECT count(id) as results_count FROM office where $condition");
		$pagination = $helper->paginate($params['limit'], $params['page'], $results['results_count']);
		$results['pagination_above'] = $pagination; 
		return $results;		
	}
	
	function office( $params=array()){
		global $wpdb;
		$office_id_all= "id IN (". esc_sql(implode(', ', $this->settings['general_settings']['office_id'])) . ")";
		if(!empty($params['office_id']))$condition .= "where id in(".esc_sql($params['office_id']).")";
		if(!empty($office_id_all))$condition = ($condition=='')?"$office_id_all":"$condition and $office_id_all";
		$sql = "SELECT * FROM office $condition";
		if($this->debug)print $sql ;
		$offices=$wpdb->get_results($sql, ARRAY_A); 
		$office = $offices[0];
		if(empty($office))return;
		
		if(empty($params['status']))$params['status']='1,4';
		$office['listings']=$this->properties($params);		
		return $office;		
	}	

	function get_unpaged_url(){
		return "?" . preg_replace('/&(page=)([-+]?\\d+)/is', '', $_SERVER['QUERY_STRING']);	
	}
	
	function wp_head(){
		global $post, $helper, $wp_query, $wpdb;		
		if(preg_match ('/\[realty_plugin.*?(.*?)template=(.*?)\]/is', $post->post_content, $matches)){	
			ob_start();

			$this->params['page'] = $wp_query->query_vars['page'];
			$post_params = $helper->parse_query_string(trim(str_replace("," , "&" ,html_entity_decode($matches[1]))));
			if(!$post_params) $post_params = array(); 
			$this->params = array_merge($this->params,$post_params);
			$this->renderer = trim($matches[2]);		
			
			//meta agent page
			// $agent_name = trim($agent['firstname']).trim($agent['lastname']);
			// if(is_page($agent_name)){ 
			// 	$office_id = implode(",",$this->settings['general_settings']['office_id']);
			// 	$agents = $wpdb->get_results("SELECT suburb, description, group FROM users WHERE office_id IN ($office_id) AND (id =".$this->params['user_id'].")", ARRAY_A);
				
			// 	foreach ($agents as $key => $agent) {
			// 		$agent_suburb = $agent['suburb'];
			// 		$description = $agent['description'];
			// 		$group = $agent['group'];					
			// 	}
				
			?>				
		<?php /*		<meta name="description" content="Real Estate Agent <?php echo htmlspecialchars($helper->_substr($agent_suburb." ".$description, 150)) ?>" />				
				<meta name="keywords" content="Real Estate Agent, estate agent, property agent, property sales, real estate sales, sell my property, sell my house" />
*/ ?>
			<?php //}
			//end meta agent page

			if(is_page('property')){
				$this->property();	
				
				//newsagenciesforsale
				if(strpos($this->siteUrl,'newsagenciesforsale')!==false || strpos($this->siteUrl,'qnfsales')!==false){
					if($this->renderer=='search_results' || $this->renderer=='sold'  || $this->renderer=='opentimes'  || $this->renderer=='latest_auction'  || $this->renderer=='favourite_property'){ // set cookie for back to search results link on headoffice site
						$search_results_link = $this->curPageURL();
						setcookie("search_results_link_rhn", $search_results_link,time()+(60*60), '/');		
					}
				}
				
				//liveagent
				if(strpos($this->siteUrl,'liveagent')!==false){
					$settings=$this->settings['widgets']['media'];
					if(($this->renderer=='search_results' || $this->renderer=='sold'  || $this->renderer=='opentimes'  || $this->renderer=='latest_auction'  || $this->renderer=='favourite_property') && $settings['mode']=='tbslideshow_lightbox'){ // set cookie for back to search results link on headoffice site
						$search_results_link= $this->curPageURL();
						setcookie("search_results_link", $search_results_link,time()+(60*60), '/');		
					}
				}
			
				//exhange-property
				if(strpos($this->siteUrl,'exchange-property')!==false || strpos($this->siteUrl,'fnredcliffe')!==false || strpos($this->siteUrl,'fnuppercoomera')!==false || strpos($this->siteUrl,'nfn.com.au')!==false){					
					if(!empty($_GET['u'])){
						$condition = (empty($condition))? $this->get_location(): $this->get_location() . " AND $condition";
						$users = $wpdb->get_col("select id from users where $condition", ARRAY_A);	
						if(in_array($_GET['u'],$users)){ $user_id=$_GET['u']; }
						else { shuffle($users); $user_id=$users['0']; } 
						setcookie("active_user_id", $user_id,time()+(60*15), '/'); 
						setcookie("time_user_id", date('d-n-Y H:i:s'),time()+(60*60*24*365), '/'); 
					}
					
					if(!empty($_COOKIE['active_user_id'])){
						if($this->time_diff($_COOKIE['time_user_id']) > 15){ setcookie("active_user_id", '',time()+(60*60*24*365), '/'); }	
						else{
							$condition = (empty($condition))? $this->get_location(): $this->get_location() . " AND $condition";
							$users = $wpdb->get_col("select id from users where $condition", ARRAY_A);	
							if(!in_array($_COOKIE['active_user_id'],$users)){ setcookie("active_user_id", '',time()+(60*60*24*365), '/'); }	
						}
					}
				}
			
				$this->element('extra_header');
				$description=$this->property['property_type'].": ";
				if(!empty($this->property['bedrooms']))$rooms=$this->property['bedrooms']." bedrooms, ";
				if(!empty($this->property['bathrooms']))$rooms.=$this->property['bathrooms']." bathrooms, ";
				if(!empty($this->property['carspaces']))$rooms.=$this->property['carspaces']." carspaces, ";
				$description.=substr($rooms,0,-2);
				$description.=($this->property['is_for_sale'])?' for sale. ':' for lease. ';
				$description.='Contact: '.$this->property['user'][0]['name'].' re: '.$this->property['street_address'].', '.$this->property['suburb'];				
				?>
				<meta name="geo.position" content="<?php echo $this->property['latitude'].";".$this->property['longitude']; ?>" />
				<meta name="geo.placename" content="<?php echo $this->property['suburb'].", ".$this->property['street_address']; ?>" />
				<meta name="geo.region" content="<?php echo "AUS-".$this->property['state']; ?>" />
				<meta property="og:title" content="<?php echo $this->property['headline']; ?>"/>
				<meta property="og:type" content="product"/>			
				<meta property="og:url" content="<?php echo $this->property['url']; ?>"/>
				<?php if(!empty($this->property['photos'])){ foreach($this->property['photos'] as $photo){ ?>
				<meta property="og:image" content="<?php echo $photo['small']; ?>"/>
				<?php } } ?>
				<meta property="og:site_name" content="<?php echo $this->siteUrl; ?>"/>
				<meta property="fb:admins" content="829389659"/>
				<meta property="og:description" content="<?php echo $description; ?>"/>
				<script type="text/javascript">
				function remProp(id) {
					jQuery("#list_fav").load("?action=load_fav&id="+id+"&isRemoveFavouriteList=true");
				}

				function saveProp(url,id) {
					jQuery("#add_to_favs").load( url+"?action=add_prop&id="+id+"" );
					<?php if(class_exists('zoo_analytics')){ ?>share_trackers(id,'favorite');<?php } ?>//save into zoo analytics
				}

				function delProp(url,id) {
					jQuery('#view_fav').hide();
					jQuery('#back_fav').hide();
					jQuery("#add_to_favs").load( url+"?action=remove_prop&id="+id+"" );
				}
				jQuery(document).ready(function(){
					jQuery(".popup_window,.action_buttons a, .email_alert").click(function() {
						if((jQuery(this).attr('href'))!='javascript:void(0);'){
							window.open(jQuery(this).attr('href'), jQuery(this).attr('title').replace(' ','_'), jQuery(this).attr('rel')  + ', resizable=1,scrollbars=1');
							return false;
						}						
					});		
				});
				</script>
			<?php
				if(!empty($this->property['rental_season']) && strpos($this->siteUrl,'awaba')!==false){
				?>
				<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>display/pages/js/val.js"></script>
				<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>display/pages/js/load_calendar.js"></script>
				<script type="text/javascript">
				function switch_calendars(calendar){
					jQuery("#calendars table").addClass("hidden"); 
					jQuery("#" + calendar).removeClass("hidden");					
					load_calendar(calendar,<?php echo $this->property['id'];?>,"<?php echo get_option("siteurl");?>");
				}
				function fill_stay_period(value){
					if(jQuery("#first_night").val() =='')
						jQuery("#first_night").val(value);
					else
						jQuery("#last_night").val(value);
				}
				jQuery(document).ready(function(){
					jQuery("#calendars table").eq(0).removeClass("hidden");//First month to be visible
					jQuery(".toggle_dates").click(function () {
						jQuery(this).next().toggle();
						 if(jQuery(this).next().css("display") =='none')
							jQuery(this).html('+view dates');
						 else
							jQuery(this).html('-hide dates');
						
					});
					
					jQuery("#calendars td > a").click(function () {
					   if(jQuery("#first_night").val()=='')jQuery("#min_day").val(jQuery(this).attr("alt"));
					   fill_stay_period(jQuery(this).attr("href"));
					 
					 jQuery("#first_night").trigger('blur');
					  return false;
					});
				   
					jQuery("#reset_first_night").click(function () { 	jQuery("#first_night").val(''); 	});
					
					jQuery("#prev_month").click(function () { 
						var selected_index = jQuery("#calendars select")[0].selectedIndex -1;
						if(selected_index <0) 
							selected_index = jQuery("#calendars select")[0].length -1;
						jQuery("#calendars select")[0].selectedIndex = selected_index;
						switch_calendars(jQuery("#calendars select")[0].options[selected_index].value);	
					});    
					 jQuery("#next_month").click(function () { 
						var selected_index = jQuery("#calendars select")[0].selectedIndex +1;
						if(selected_index >jQuery("#calendars select")[0].length -1) //Last month, then start at the first month, in a circle
							selected_index = 0;
						jQuery("#calendars select")[0].selectedIndex = selected_index;
						switch_calendars(jQuery("#calendars select")[0].options[selected_index].value);	
					});  
					jQuery("#reset_last_night").click(function () {   	jQuery("#last_night").val('');	});
					
					jQuery("#calendars select").change(function () {   	switch_calendars(jQuery(this).val()); });
					
				});
				</script>
			<?php }
				if(!empty($this->property['rental_season']) && strpos($this->siteUrl,'bushbystays')!==false){
				?>
				<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>display/pages/js/val.js"></script>
				<script type="text/javascript">
				/* map & walkscrore widget */
					function switch_calendars(calendar){
						jQuery("#calendars table").addClass("hidden"); 
						jQuery("#" + calendar).removeClass("hidden");
					}
					
					function fill_stay_period(value){
						if(jQuery("#first_night").val() =='')
							jQuery("#first_night").val(value);
						else
							jQuery("#last_night").val(value);
					}
					
					/* availability calender */
					function switch_calendars_only(calendar){
						jQuery("#calendars_only table").addClass("hidden"); 
						jQuery("#" + calendar).removeClass("hidden");
					}
					
					jQuery(document).ready(function(){
						/* map & walkscrore widget */
						jQuery("#calendars table").eq(0).removeClass("hidden");//First month to be visible
						jQuery(".toggle_dates").click(function () {
							jQuery(this).next().toggle();
							 if(jQuery(this).next().css("display") =='none')
								jQuery(this).html('+view dates');
							 else
								jQuery(this).html('-hide dates');
							
						});
						
						jQuery("#calendars td > a").click(function () {
						   if(jQuery("#first_night").val()=='')jQuery("#min_day").val(jQuery(this).attr("alt"));
						   fill_stay_period(jQuery(this).attr("href"));
						 
						 jQuery("#first_night").trigger('blur');
						  return false;
						});
					   	jQuery("#reset_first_night").click(function () { 	jQuery("#first_night").val(''); 	});		
						jQuery("#reset_last_night").click(function () {   	jQuery("#last_night").val('');	});						
						jQuery("#calendars select").change(function () {   	switch_calendars(jQuery(this).val()); });
						
						jQuery("#prev_month").click(function () { 
							var selected_index = jQuery("#calendars select")[0].selectedIndex -1;
							if(selected_index <0) 
								selected_index = jQuery("#calendars select")[0].length -1;
							jQuery("#calendars select")[0].selectedIndex = selected_index;
							switch_calendars(jQuery("#calendars select")[0].options[selected_index].value);	
						});    
						
						 jQuery("#next_month").click(function () { 
							var selected_index = jQuery("#calendars select")[0].selectedIndex +1;
							if(selected_index >jQuery("#calendars select")[0].length -1) //Last month, then start at the first month, in a circle
								selected_index = 0;
							jQuery("#calendars select")[0].selectedIndex = selected_index;
							switch_calendars(jQuery("#calendars select")[0].options[selected_index].value);	
						});  
						
						
						/* availability calender */
						jQuery("#calendars_only table").eq(0).removeClass("hidden");//First month to be visible
						jQuery("#prev_month_only").click(function () { 
							var selected_index = jQuery("#calendars_only select")[0].selectedIndex -1;
							if(selected_index <0) 
								selected_index = jQuery("#calendars_only select")[0].length -1;
							jQuery("#calendars_only select")[0].selectedIndex = selected_index;
							switch_calendars_only(jQuery("#calendars_only select")[0].options[selected_index].value);	
						});    
						 jQuery("#next_month_only").click(function () { 
							var selected_index = jQuery("#calendars_only select")[0].selectedIndex +1;
							if(selected_index >jQuery("#calendars_only select")[0].length -1) //Last month, then start at the first month, in a circle
								selected_index = 0;
							jQuery("#calendars_only select")[0].selectedIndex = selected_index;
							switch_calendars_only(jQuery("#calendars_only select")[0].options[selected_index].value);	
						});  		
						jQuery("#calendars_only select").change(function () {   	switch_calendars_only(jQuery(this).val()); });		
					});
				</script>
			<?php }
			}				
			?>
			<script type="text/javascript">
				function openbox(formtitle, formid, formfilter , formboxtitle, load_form, url)
				{
				  jQuery('#'+load_form).load(url);
				  var box = document.getElementById(formid); 
				  document.getElementById(formfilter).style.display='block';

				  var btitle = document.getElementById(formboxtitle);
				  btitle.innerHTML = formtitle;
				  
				  box.style.display='block'; 	 
				}

				function closebox( formid, formfilter, load_form, formboxtitle)
				{
				   document.getElementById(load_form).innerHTML = '';
				   document.getElementById(formboxtitle).innerHTML = '';
				   document.getElementById(formid).style.display='none';
				   document.getElementById(formfilter).style.display='none';
				}
				<?php //noosaholidays
				if($_GET['action'] == 'add_compare'){ ?>		
					function open_comparisons(){
						window.open('<?php echo $this->pluginUrl; ?>display/elements/comparison.php', 'Comparisons', 'width=800, height=600,resizable=1,scrollbars=1');
						return false;  
					}
					window.onload=open_comparisons();	
				<?php }
				if(strpos($this->siteUrl,'bondibeachrentals')!==false){ ?>
				function pop_yesbookit(url){
					popht = 600;
					popwth = 770;
					scrleft = (screen.width / 2) - (popwth /2);
					scrtop = ((screen.height / 2) - (popht /2)) - 40;
					window.open(url,'YBI','top='+scrtop+',left='+scrleft+',height=600,width=770,scrollbars=yes,resizable=no');
				}	
				<?php } ?>
			</script>		
			<?php
		}		
	}
	
	function getFileMetadata($plugin_file, $total_kiB = 1 ) {
		$fp = fopen($plugin_file, 'r');	// We don't need to write to the file, so just open for reading.
		$file_data = fread( $fp, $total_kiB * 1192 );// Pull only the first $total_kiB kiB of the file in.	
		fclose($fp);// PHP will close file handle, but we are good citizens.
		preg_match( '|metadata:(.*)$|mi', $file_data, $metadata );
		$metadata = trim($metadata[1]);
		parse_str($metadata, $metadataArr);
		return $metadataArr;
	}

	function parse_shortcode($attributes, $content = null){
        extract(shortcode_atts(array(
             'template' => '',
             'list' => '',
             'property_type' => '',
             'suburb' => '',
             'state' => '',
             'group' => '',
             'user_id' => '',
             'office_id' => '',
             'flag' => '',
             'category' => '',
             'status' => ''
          ), $attributes));

        $shortcode_string = "";
		if(empty($template))return;
		if($list)$this->params['list']=$list;
		if($property_type)$this->params['property_type']=$property_type;
		if($suburb)$this->params['suburb']=$suburb;
		if($state)$this->params['state']=$state;
		if($status)$this->params['status']=$status;
		if($group)$this->params['group']=$group;
		if($office_id)$this->params['office_id']=$office_id;
		if($user_id)$this->params['user_id']=$user_id;
		if($flag)$this->params['flag']=$flag;
		if($category)$this->params['category']=$category; 
		
        ob_start();
		require_once(dirname(__FILE__)."/display/pages/".$template.".php");
		$shortcode_string=ob_get_clean();
		
		$shortcode_string = apply_filters("realty_shortcode", $shortcode_string, $attributes, $content);

        return $shortcode_string;
    }
	
	function the_content($content){
		global $helper;
		if(empty($this->renderer)) //If this is not one of the pages created by the plugin, return the content.
			return $content;
			
		if($this->renderer == 'parse'):
			$content = str_get_html( $content);
			
			foreach( $content->find('.ritem') as $items):
				 if ($c=preg_match_all ("/.*?(\\{.*?\\})/is", $items, $matches)):
					$index = substr($matches[1][0], 1, -1);
				 
					if(!empty($this->property[$index])):
						if(is_array($this->property[$index])): //If it is an array of items			
							foreach($this->property[$index] as $part)//the wrapping tag is the last found in the the ritem class
								$wrapper .=  preg_replace("/(\\{.*?\\})/is", $part, $items->last_child ()->innertext);
							$items->last_child ()->innertext = $wrapper;
							echo  $items;
						else:
							echo preg_replace("/(\\{.*?\\})/is", $this->property[$index], $items);
						endif;
					endif;
				 endif;
			endforeach;				
			return;
		endif;

		if(!file_exists($renderer = $this->path('display/pages/' .$this->renderer .'.php')))return 'Error: Renderer does not exist.';			

		echo preg_replace('/\[realty_plugin:.*?(.*?)template:(.*?)\]/', '', $content);

		require ($renderer);	
	}
	
	function rewritetitle()	{
		ob_start(array($this, 'output_callback_for_title'));
	}

	function output_callback_for_title($content) {
		return $this->rewrite_title($content);
	}
	
	function rewrite_title($header){
		$content = get_the_content();
		global $post;
		if( $this->is_property_page && is_page('property')): //Try to get a property from the query_vars
			$postcode = $this->property['postcode'];
			$region = $this->property['town_village'];
			$headline = $this->property['headline'];
			$desc = $this->property['description'];
			$property_type = ucwords($this->property['property_type']);
			$state = $this->property['state'];
			$suburb = $this->property['suburb'];
			$type = strtolower($this->property['type']);
			$deal_type = strtolower($this->property['deal_type']);
			if(strpos($type,'sale')!==false)$listing_type='For Sale';
			else if(strpos($type,'lease')!==false)$listing_type='For Lease';
			else if(strpos($type,'commercial')!==false && $deal_type=='sale')$listing_type='For Commercial Sale';
			else if(strpos($type,'commercial')!==false && $deal_type=='lease')$listing_type='For Commercial Lease';
			else if(strpos($type,'commercial')!==false && $deal_type=='both')$listing_type='For Commercial Sale & Lease';
			else if(strpos($type,'development')!==false)$listing_type='New Development';
			$address = $this->property['street_address'] ." - ". $this->property['suburb']." , ". $this->property['state'];
			$title= get_option('realty_property_meta_title');
			$title = str_replace("%address%",$address,$title);
			$title = str_replace("%postcode%",$postcode,$title);
			$title = str_replace("%region%",$region,$title);
			$title = str_replace("%headline%",$headline,$title);
			$title = str_replace("%desc%",$description,$title);
			$title = str_replace("%property_type%",$property_type,$title);
			$title = str_replace("%state%",$state,$title);
			$title = str_replace("%suburb%",$suburb,$title);
			$title = str_replace("%listing_type%",$listing_type,$title);
			$title = str_replace("+","",$title);
			$header = $this->replace_title($header, $title);
		endif;		
		return $header;
	}
	
	function replace_title($content, $title) {
		$title = trim(strip_tags($title));
		
		$title_tag_start = "<title>";
		$title_tag_end = "</title>";
		$len_start = strlen($title_tag_start);
		$len_end = strlen($title_tag_end);
		$title = stripcslashes(trim($title));
		$start = strpos($content, $title_tag_start);
		$end = strpos($content, $title_tag_end);
		
		$this->title_start = $start;
		$this->title_end = $end;
		$this->orig_title = $title;
		
		if ($start && $end) {
			$header = substr($content, 0, $start + $len_start) . $title .  substr($content, $end);
		} else {
			$header = $content;
		}		
		return $header;
	}
		
	function nl2p(&$text){		
		$text = nl2br($text);
	}
	
	function get_suburb($suburb=''){
		global $wpdb;
		if(empty($suburb))$suburb = $this->property['suburb'];
		$return = $wpdb->get_results("SELECT * FROM LocationInfo_suburbs WHERE suburb = '". trim(esc_sql($suburb))."' LIMIT 1", ARRAY_A);
		if(!$return) // If a suburb hasnt been found, theres no need for the widget
			return false;
		$this->nl2p($return['description']);
		$suburb_id = $return[0]['suburb_id'];
		
		//get photos
		$photos = $wpdb->get_results("SELECT * FROM LocationInfo_photos WHERE `suburb_id` =".intval($suburb_id)." ORDER BY `ord`", ARRAY_A);
		foreach ($photos as $key=>$photo):
			$this->fill_url($photos[$key]['path']);
			$photos[$key]['thumbnail'] = $this->get_thumbnail($photos[$key]['path']);
		endforeach;		
		
		$return['photos'] =  $photos;
		$return['amenities'] = array();
		$amenities = array('school'=>'schools', 'shopping'=>'shoppings', 'landmark'=>'landmarks','link'=>'interesting links');

		foreach($amenities as $key=>$amenity): //Get each type of amenity in a separate array
			$return[$amenity] = $wpdb->get_results("SELECT * FROM LocationInfo_amenities WHERE `suburb_id`=" . intval($suburb_id) . " AND `type`='$key' ORDER BY `type` DESC $limit", ARRAY_A);			
			$return['amenities'] = array_merge($return['amenities'], $return[$amenity]);
		endforeach;
			
		return $return;
	}	
	
	function get_thumbnail($path){
		return $path;
	}
	
	function fill_url(&$url){
		if(!empty($url))
			$url = get_option('siteurl') . "/wp-content/uploads/LocationInfoPlugin/$url";
	}
		
	function widget_register() {
		global $helper;
		$number = 1;
		$widget_files = $helper->getTemplates($this->path('display/widgets/'));
		foreach($widget_files as $caption=>$file):
			if(substr($file,-4)=='.php'){
			$settings = $this->settings['widgets'][$caption];
			$prefix = "realty_widget_". $caption; // $id prefix
			$name = 'Real Estate ' . __($helper->humanize($caption));
			$widget_ops = array('classname' => $prefix, 'description' => sprintf(__('Realty Plugin %s Widget'), $name));
			$control_ops = array('classname' => 'realty', 'width' => 200, 'height' => 200, 'id_base' => $prefix);
			wp_register_sidebar_widget($prefix, $name, 
			
			create_function('$args, $params',
			'global $realty;
			extract($args);
			extract($params);
		    echo $before_widget;
		    require($file);
		    echo $after_widget;
	    '), $widget_ops, array( 'file'=>$file,'settings'=>$settings));
			$widget_control = $this->path('manage/widgets/'). $caption . ".php";
			
			if(file_exists($widget_control))
				wp_register_widget_control($prefix, $name, create_function('$args',
			'global $realty, $caption;
			
			extract($args);
			if(!empty($_POST["realty_widget_". $caption])):
				$realty->settings["widgets"][$caption] = $_POST["realty_widget_". $caption];
				update_option("realty_widgets", $realty->settings["widgets"]);			
			endif;
			//reload the setting
			$tempsettings = get_option("realty_widgets");
			$settings = $tempsettings[$caption]; 
			require($file);
			'), $control_ops, array('caption'=>$caption, 'file'=>$widget_control,'settings'=>$settings));
			++$number;
			}
		endforeach;
	}

	function wp_user_id($name){
		global $wpdb, $helper;
		return $wpdb->get_var("SELECT `ID` FROM $wpdb->users WHERE `display_name`='".esc_sql($name)."'");
	}
	
	function all_list_referrers(){
		global $wpdb;
		$result=$wpdb->get_results("select * from list_referrers order by `name`", ARRAY_A); 
		return $result;
	}
	
	function property_testimonials($property_id=''){		
		global $helper, $wpdb;
		if(!empty($property_id))$condition=" property_id =".intval($property_id)." and ";
		$office_id=implode(', ', $this->settings['general_settings']['office_id']);
		
		$ids=$wpdb->get_col("select id from property_testimonials where $condition property_id in(select id from properties where office_id in (".esc_sql($office_id)."))");
		if(empty($ids))return;
		
		if(count($ids)>5){
			shuffle($ids);
			$random_ids=array();
			for($i=0;$i<5;$i++)array_push($random_ids,$ids[$i]);
			$ids=$random_ids;
		}
		$ids=implode(",",$ids);
		
		$sql="select property_id, contact_name, first_name, last_name, `date`, note, user_id, secondary_user, status, suburb, headline from property_testimonials , properties where property_testimonials.id in(".esc_sql($ids).") and property_testimonials.property_id=properties.id";
		$property_testimonials=$wpdb->get_results($sql, ARRAY_A);
		
		if($this->debug)print $sql ;
		
		$placeholder_image= $this->paths['theme'].'images/download.png';
		$agent_placeholder_image= $this->paths['theme'].'images/default_avatar.jpg';
		
		foreach($property_testimonials as $key=>$row){
			$thumbnail='';$medium='';
			$photos=$wpdb->get_results("SELECT url, description FROM attachments WHERE parent_id = ".intval($row['property_id'])." and description IN('medium','thumb') and type='photo' and is_user='0' order by position LIMIT 2 ", ARRAY_A);
				if(!empty($photos)){
					foreach($photos as $photo):
						if($photo['description']=='thumb')$thumbnail=$photo['url'];
						if($photo['description']=='medium')$medium=$photo['url'];
					endforeach;
				}
			$property_testimonials[$key]['thumbnail']=($thumbnail=='')?$placeholder_image:$thumbnail;
			$property_testimonials[$key]['medium']=($medium=='')?$placeholder_image:$medium;
			$property_testimonials[$key]['property_url']=$this->siteUrl.$row['property_id']."/";
			if(!empty($row['user_id'])){
				$users=$wpdb->get_results("SELECT CONCAT(firstname, ' ', lastname) as name, role, `group`, phone, mobile from users where id=".intval($row['user_id']), ARRAY_A);
				$photo=$wpdb->get_row("select url from attachments where parent_id= '".intval($row['user_id'])."' and type='portrait' and description='large' order by position limit 1", ARRAY_A);
				$property_testimonials[$key]['agent_name']=$users[0]['name'];
				$property_testimonials[$key]['agent_phone']=$users[0]['phone'];
				$property_testimonials[$key]['agent_mobile']=$users[0]['mobile'];
				$property_testimonials[$key]['agent_position']=($users[0]['role']=='')?$users[0]['group']:$users[0]['role'];					
				$property_testimonials[$key]['agent_photo']=($photo['url']=='')?$agent_placeholder_image:$photo['url'];
				$property_testimonials[$key]['user_url']=$this->team_url . "?u=".$row['user_id']; 
			}
			if(!empty($row['secondary_user']) && empty($row['user_id'])){
				$users=$wpdb->get_results("SELECT CONCAT(firstname, ' ', lastname) as name, role, `group`, phone, mobile from users where id=".intval($row['secondary_user']), ARRAY_A);
				$photo=$wpdb->get_row("select url from attachments where parent_id= '".intval($row['secondary_user'])."' and type='portrait' and description='large' order by position limit 1", ARRAY_A);
				$property_testimonials[$key]['agent_name']=$users[0]['name'];
				$property_testimonials[$key]['agent_phone']=$users[0]['phone'];
				$property_testimonials[$key]['agent_mobile']=$users[0]['mobile'];
				$property_testimonials[$key]['agent_position']=($users[0]['role']=='')?$users[0]['group']:$users[0]['role'];				
				$property_testimonials[$key]['agent_photo']=($photo['url']=='')?$agent_placeholder_image:$photo['url'];
				$property_testimonials[$key]['user_url']=$this->team_url . "?u=".$row['secondary_user']; 
			}
		}
		return $property_testimonials;
	}
	
	function curPageURL() {
		 $pageURL = 'http';
		 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		 $pageURL .= "://";
		 if ($_SERVER["SERVER_PORT"] != "80") {
		  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		 } else {
		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		 }
		 return $pageURL;
	}
	/* custom functions start */
	function properties_unfurnished($params = array(), $send_query=true, $select=''){
		global $helper, $wpdb;
		$return=$this->search($params);
		if(empty($select)){			
			if(empty($params['id']))$select = $this->select(false);
			else $select = $this->select();
		}
		$sql = "$select FROM properties WHERE type IN ('ResidentialLease') AND id NOT IN (SELECT property_id FROM property_features WHERE TRIM(feature) in ('furnished'))";	
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE type IN ('ResidentialLease') AND id NOT IN (SELECT property_id FROM property_features WHERE TRIM(feature) in ('furnished'))");
		$sql .= " ".$return['end'];
		if($this->debug)print $sql ;		
				
		if($send_query):	
			$return = $wpdb->get_results($sql, ARRAY_A);
			if(!empty($return)){
				$placeholder_image= $this->paths['theme'].'images/download.png';
				foreach($return as $key=>$row){
					if(!is_array($row))continue;
					if(!empty($row['id'])){
						$photos=$wpdb->get_results("SELECT url, description, width, height FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('medium','thumb') and type='photo' and is_user='0' order by position LIMIT 2 ", ARRAY_A);
						if(!empty($photos)){
							foreach($photos as $photo):
								if($photo['description']=='thumb'){
									$return[$key]['thumbnail']=$photo['url'];
									$return[$key]['thumbnail_width']=$photo['width'];
									$return[$key]['thumbnail_height']=$photo['height'];
								}
								if($photo['description']=='medium'){
									$return[$key]['medium']=$photo['url'];
									$return[$key]['medium_width']=$photo['width'];
									$return[$key]['medium_height']=$photo['height'];
								}
							endforeach;
						}
						if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
						if(empty($return[$key]['medium']))$return[$key]['medium']=$placeholder_image;
					}
				}
			}
			if(is_array($return))array_walk($return,array( $this, 'format_property'));			
			$return['results_count'] = $results_count;
			$pagination = $helper->paginate($this->params['limit'], $this->params['page'], $results_count);
			foreach($this->settings['general_settings']['display_pagination'] as $paginate)
				$return['pagination_' . $paginate] = $pagination; //Returns $return['pagination_above'] and/or $return['pagination_below']
 			return $return;
 		endif;
		return $sql; 
	}

	function map_properties($params = array(), $select=''){
		global $helper, $wpdb;
		$return=$this->search($params);
		if(empty($select)){			
			if(empty($params['id']))$select = $this->select(false);
			else $select = $this->select();
		}
		$sql = "$select FROM properties WHERE ".$return['condition'];	
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count FROM properties WHERE ".$return['condition']);
		$sql .= " ".$return['end'];
		if($this->debug)print $sql ;
		
		$return = $wpdb->get_results($sql, ARRAY_A);
		if(!empty($return)){
			$placeholder_image= $this->paths['theme'].'images/download.png';
			foreach($return as $key=>$row){
				if(!is_array($row))continue;
				if(!empty($row['id'])){
					$photos=$wpdb->get_results("SELECT url, description, width, height FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('thumb') and type='photo' and is_user='0' order by position LIMIT 1 ", ARRAY_A);
					if(!empty($photos)){
						foreach($photos as $photo):
							if($photo['description']=='thumb'){
								$return[$key]['thumbnail']=$photo['url'];
								$return[$key]['thumbnail_width']=$photo['width'];
								$return[$key]['thumbnail_height']=$photo['height'];
							}
							endforeach;
					}
					if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
				}
			}
		}
		if(is_array($return)) 
			array_walk($return,array( $this, 'format_property'));			
		$return['results_count'] = $results_count;
 		return $return;
	}
	
	function latest_properties(){
		global $helper, $wpdb;
		$params = array('order'=>'created_at' ,'order_direction'=> 'desc', 'limit'=>30, 'status'=>'1,4');
		$return = $this->search($params);
		$select = $this->select(false);		
		$sql = "$select FROM properties WHERE ".$return['condition'];	
		$sql .= " ".$return['end'];
		$return = $wpdb->get_results($sql, ARRAY_A);
		if(!empty($return)){
			$placeholder_image= $this->paths['theme'].'images/download.png';
			foreach($return as $key=>$row){
				if(!is_array($row))continue;
				if(!empty($row['id'])){
					$photos=$wpdb->get_results("SELECT url, description, width, height FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('medium','thumb') and type='photo' and is_user='0' order by position LIMIT 2 ", ARRAY_A);
					if(!empty($photos)){
						foreach($photos as $photo):
							if($photo['description']=='thumb'){
								$return[$key]['thumbnail']=$photo['url'];
								$return[$key]['thumbnail_width']=$photo['width'];
								$return[$key]['thumbnail_height']=$photo['height'];
							}
							if($photo['description']=='medium'){
								$return[$key]['medium']=$photo['url'];
								$return[$key]['medium_width']=$photo['width'];
								$return[$key]['medium_height']=$photo['height'];
							}
						endforeach;
					}
					if(empty($return[$key]['thumbnail']))$return[$key]['thumbnail']=$placeholder_image;
					if(empty($return[$key]['medium']))$return[$key]['medium']=$placeholder_image;
				}
			}
		}
		if(is_array($return))array_walk($return,array( $this, 'format_property'));						
		return $return;
	}
	
	function tags($tag,$price_min='',$price_max='',$property_type='',$bedrooms='',$bathrooms='', $suburb='', $price=''){
		
		global $post;
		if(strpos($post->post_content,'template:search_results')!==false)$type=str_replace("<!--realty_plugin: ","",str_replace(" template:search_results-->","",str_replace("list=","",$post->post_content)));
		$tag_condition = array('list'=>$type,'group'=>$tag, 'order'=>$tag,'order_direction'=>'ASC','limit'=>1000, 'page'=>'1');
		if($this->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']=='1')$tag_condition['status']='1,2,4,6';
		if($this->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']!='1')$tag_condition['status']='1,2,4';
		if($this->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']=='1')$tag_condition['status']='1,4,6';
		if($this->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']!='1')$tag_condition['status']='1,4';
		if($price_min!='')$tag_condition['price_min']=$price_min;
		if($price_max!='')$tag_condition['price_max']=$price_max;
		if($property_type!='')$tag_condition['property_type']=$property_type;
		if($bedrooms!='')$tag_condition['bedrooms']=$bedrooms;
		if($bathrooms!='')$tag_condition['bathrooms']=$bathrooms;
		if($suburb!='')$tag_condition['suburb']=$suburb;
		if($price!='')$tag_condition['price']=$price;
		$results = $this->properties($tag_condition, true, "SELECT $tag AS tag, COUNT(*) AS number_of_properties");
		return $results;
	}
	
	function office_details($columns = "*", $office_id ='',$state='',$region='',$limit='', $flag=true){
		global $helper, $wpdb;
		if (empty($office_id) && !empty($this->settings['general_settings']['office_id'])) $office_id = implode(',', $this->settings['general_settings']['office_id']);
		if(empty($office_id))return;
		$num_of_office=get_option('realty_limit_office');
		if(!empty($limit)):
			$limit=($limit-1)*$num_of_office;
			$limit= 'limit '.$limit.','.$num_of_office;
		else: 
		if($flag)$limit = 'limit '.$num_of_office;
		endif;	
		if(!empty($state) || !empty($region)){
			if(!empty($state) && $state!='%')$condition .= " and state='".$state."'";
			if(!empty($region) && $region!='%')$condition .= " and suburb='".$region."'";	
		}
			$columns = "CONCAT( IF ( (TRIM(office.unit_number) IS NULL) OR (TRIM(office.unit_number) = '') , '', CONCAT(TRIM(office.unit_number),'/') ), office.street_number,' ',office.street,CONCAT( IF ( (TRIM(office.street_type) IS NULL) OR (TRIM(office.street_type) = '') , '', CONCAT(',',TRIM(office.street_type)) ))) AS address, id, name, state, suburb, zipcode, country, latitude, longitude, status, email, url, fax, phone ";
			
		$sql = "SELECT $columns FROM office WHERE `id` IN ($office_id) $condition $limit";
		if($columns=="*" || strpos($columns,",")!==false )$row=$wpdb->get_results($sql, ARRAY_A); else $row=$wpdb->get_row($sql, ARRAY_A);
		$row['results_count']=$wpdb->get_var("SELECT count(id) as results_count FROM office WHERE `id` IN ($office_id) $condition");
		return $row;		
	}
	
	function property_research($postcode='',$suburb=''){
		global $wpdb;
		if(empty($postcode) || empty($suburb)):
		$results = $wpdb->get_results("SELECT b.state as state, b.suburb as suburb, b.postcode as postcode FROM demographicdata a,pricedata b,medianhistory c WHERE a.postcode=b.postcode and b.postcode=c.medianhistorypostcode and a.state=b.state and b.state=c.medianhistorystate and 
	medianhistorylabel !='' and population !=''", ARRAY_A);
		else :
		$results = $wpdb->get_results("SELECT b.state as state, b.suburb as suburb, b.postcode as postcode FROM demographicdata a,pricedata b,medianhistory c WHERE a.postcode=b.postcode and b.postcode=c.medianhistorypostcode and a.state=b.state and b.state=c.medianhistorystate and 
	medianhistorylabel !='' and population !='' and b.postcode='".intval($postcode)."' and b.suburb NOT IN ('".esc_sql($suburb)."')", ARRAY_A);
		endif;
		foreach($results as $key=>$row):
				$results[$key]["url"] = $this->suburb_url($row['state'],$row['suburb'],$row['postcode']);
			endforeach;
		return $results;	
	}
	
	function suburb_url($state='',$suburb='',$postcode=''){
		return $this->siteUrl."property_research/?ps=" .trim(strtolower($state))."-".trim(strtolower($suburb))."-".trim($postcode);
	}
	
	function sales_data($limit='',$type=''){
		global $wpdb;
		$order='';
		if(!empty($limit))$order = ' order by rand() limit '.$limit;
		if($type=='house'):
		$results = $wpdb->get_results("SELECT b.state as state, b.suburb as suburb, b.postcode as postcode, lasthousemedianprice as median_price, housesuburb_longterm_change as long_term_trend, housesuburb_Au_Cl_Rate_Base_Period as auction_clearance_rate, housesuburb_DoM_Base_Period as days_on_market, housesuburb_Disc_Base_Period as discounting, onemedianhousegrowth as 12_month_growth, twomedianhousegrowth as 24_month_growth,  threemedianhousegrowth as 36_month_growth, lasthouseyield as rental_yield FROM demographicdata a,pricedata b,medianhistory c WHERE a.postcode=b.postcode and b.postcode=c.medianhistorypostcode and a.state=b.state and b.state=c.medianhistorystate and 
	medianhistorylabel !='' and population !='' and lasthousemedianprice!='0' $order", ARRAY_A);
		else:
		$results = $wpdb->get_results("SELECT b.state as state, b.suburb as suburb, b.postcode as postcode,lastunitmedianprice as median_price, unitsuburb_longterm_change as long_term_trend, unitsuburb_Au_Cl_Rate_Base_Period as auction_clearance_rate,unitsuburb_DoM_Base_Period as days_on_market,  unitsuburb_Disc_Base_Period as discounting, onemedianunitgrowth as 12_month_growth, twomedianunitgrowth as 24_month_growth,threemedianunitgrowth as 36_month_growth, lastunityield as rental_yield FROM demographicdata a,pricedata b,medianhistory c WHERE a.postcode=b.postcode and b.postcode=c.medianhistorypostcode and a.state=b.state and b.state=c.medianhistorystate and 
	medianhistorylabel !='' and population !='' and lastunitmedianprice!='0' $order", ARRAY_A);
		endif;
		foreach($results as $key=>$row):
				$results[$key]["url"] = $this->suburb_url($row['state'],$row['suburb'],$row['postcode']);
			endforeach;
		return $results;	
	} 
	
	function comparable_sales($state){
		global $wpdb;
		$result=$wpdb->get_results("select * from comparable_sales where state like '%".esc_sql($state)."%'", ARRAY_A);
		return $result;
	}
	
	// awaba, bushbystays
	function rental_season($property_id,$price,$peak_season_price,$high_season_price,$mid_season_price,$peak_season_period,$high_season_period,$mid_season_period){
		global $helper, $wpdb;
		$peak_season_period=($peak_season_period=='')?' pn':$peak_season_period;
		$high_season_period=($high_season_period=='')?' pn':$high_season_period;
		$mid_season_period=($high_season_period=='')?' pn':$mid_season_period;
		$peak_season_period=str_replace("Nightly"," pn",str_replace("Weekly"," pw",str_replace("Monthly"," pm",$peak_season_period)));
		$high_season_period=str_replace("Nightly"," pn",str_replace("Weekly"," pw",str_replace("Monthly"," pm",$high_season_period)));
		$mid_season_period=str_replace("Nightly"," pn",str_replace("Weekly"," pw",str_replace("Monthly"," pm",$mid_season_period)));
		$normal_price=$price;
		$peak_price="$".$peak_season_price.$peak_season_period;
		$high_price="$".$high_season_price.$high_season_period;
		$mid_price="$".$mid_season_price.$mid_season_period;		
		$sql = "SELECT distinct season FROM rental_seasons WHERE `property_id`='$property_id' $condition AND start_date!='0000-00-00' and end_date!='0000-00-00' ORDER BY season ASC";
		$rentals=$wpdb->get_results($sql, ARRAY_A);
		foreach($rentals as $rental):
			$detail = "SELECT season, start_date, end_date, minimum_stay FROM rental_seasons WHERE `property_id`='$property_id' $condition AND start_date!='0000-00-00' and end_date!='0000-00-00' and season='".$rental['season']."' ORDER BY  start_date ASC, end_date ASC ";
			$rental_season[$rental['season']]=$wpdb->get_results($detail, ARRAY_A);
			if($rental['season']=='normal')$rental_season[$rental['season']]['price']=$normal_price;
			if($rental['season']=='peak')$rental_season[$rental['season']]['price']=$peak_price;
			if($rental['season']=='mid')$rental_season[$rental['season']]['price']=$mid_price;
			if($rental['season']=='high')$rental_season[$rental['season']]['price']=$high_price;
		endforeach;
		return $rental_season;
	}
	
	function show_months($limit = 12, $id='', $month = 0, $start_year = 0){
		for($count = 0; $count <= $limit; ++$count):
			$calendar .= $this->calendar($month, $start_year, $id);
			$month_name = date('F', mktime(0,0,0,$month));
			$list_options .= '<option value="'.strtolower($month_name) . '_' . $start_year . '" >'."$month_name $start_year ".'</option>';		
			if($month< 12):
				++$month;
			else:
				$month = 1;
				$start_year += 1;
			endif;	
		endfor;
		return array('list_options'=>$list_options,'calendar'=> $calendar);
	}
	
	function calendar(&$month =0, &$year=0, $id=''){
		 if(empty($id)) 
		 	return;
		$lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));
		$date=getDate();
		if($month==0):	
			$month=$date["mon"];
			$year=$date["year"];	
		endif;
		$this_month=getDate(mktime(0,0,0,$month,1,$year));
		$first_week_day = $this_month["wday"];
		$days_in_this_month = cal_days_in_month(CAL_GREGORIAN, $month, $year); //round(($next_month[0]-$this_month[0])/(60*60*24));
		
		
		if(!empty($this->property['rental_season'])){$rental_season=$this->property['rental_season'];}
		else {$listings=$this->property($id);$rental_season=$listings['rental_season'];}
		
		ob_start();
		require(dirname(__FILE__).'/display/elements/calendar_table.php');	
		$contents=ob_get_contents();
		ob_end_clean();
		return $contents;
	}
	
	function process_contact_form($request){ 
		global $helper, $wpdb;
		extract($request);			
		if(!empty($return))return $return;
		
		if(class_exists('subscriptions_manager') && strpos($this->siteUrl,'bondibeachrentals.com')!==false){
			$chk=$wpdb->get_var("select email from subscriptions_manager where email='".esc_sql($_POST['sender_email'])."'");		
			if(!$chk){
			$wpdb->query("INSERT INTO `subscriptions_manager` (property_id,first_name,email,home_phone,comments,keep_informed,created_at,updated_at) VALUES ('".intval($request['id'])."','".esc_sql($request['sender_name'])."','".esc_sql($request['sender_email'])."','".esc_sql($request['sender_phone'])."','".esc_sql($request['sender_message'])."','".intval($request['keep_informed'])."', NOW(),NOW())");
			}else{
			$wpdb->query ("update `subscriptions_manager` set property_id='".intval($request['id'])."',first_name='".esc_sql($request['sender_name'])."',home_phone='".esc_sql($request['sender_phone'])."',comments='".esc_sql($request['sender_message'])."',keep_informed='".intval($request['keep_informed'])."',updated_at=NOW() where email='".esc_sql($request['sender_email'])."'");	
			}
			
			if($request['keep_informed']=='1'){
				$subscriber_id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_POST['sender_email'])."'");	
				global $subscriptions_manager;
				$wpdb->query("DELETE FROM `site_alerts` WHERE `subscriber_id`=" . $subscriber_id);
				$list_alerts = $subscriptions_manager->get_site_alerts();$all_alerts = $subscriptions_manager->get_site_alerts(true);
				$news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);
				$i=0;
				if(!empty($news_alert)){
					foreach($news_alert as $key=>$item_value):
					$_REQUEST['alert'][$i]=$key;$i++;
					endforeach;		
					foreach( $_REQUEST['alert'] as $alert):
						if(is_array($_REQUEST['query_string'][$alert]))
							$query_string = str_replace('%2C', ',', http_build_query($_REQUEST['query_string'][$alert]));
							$alerts_sql .="(" . $subscriber_id . ", '$alert', '". $query_string . "'), ";
							$query_string = '';
					endforeach;
					$alerts_sql = substr($alerts_sql,0,-2); //Remove the last comma
					$wpdb->query("INSERT INTO `site_alerts` (`subscriber_id`, `alert`, `query_string`) VALUES $alerts_sql");
				}
			}
		}
		
		extract($this->property($request['id']),EXTR_PREFIX_ALL,"property");
		$user = $this->property['user'];
		$to=$user[0]['email'];
		if(!empty($user[1]['email'])) $to .= ", $user[1]['email']"; //Adds the secondary user's email
		
		$subject = "New booking for Property ID: $property_id";

		require(dirname(__FILE__). "/display/elements/reservation_details.php");		
		$to_bcc = $this->settings['general_settings']['bcc_all_mail_to'];

		$helper->email($to_bcc, $subject, $message, '', '', false);
		return "Thank you for your enquiry. Our staff will be in contact with you soon.";	
	}
	
	//can-we-book
	function rental_season2($property_id){
		global $wpdb;
		$rental_seasons=$wpdb->get_results("select * from rental_seasons where property_id in ($property_id) order by (season='normal') desc, (season='mid') desc, (season='high') desc, (season='peak') desc, position", ARRAY_A);
		return $rental_seasons;
	}
	
	function show_multi_months($limit = 6, $id='', $month = 0, $start_year = 0){
		for($count = 0; $count <= $limit; ++$count):
			$calendar .= $this->multi_calendar($month, $start_year, $id);
			$month_name = date('F', mktime(0,0,0,$month));
			if($count==0)$first_month="$month_name $start_year";					
			if($month< 12):
				++$month;
			else:
				$month = 1;
				$start_year += 1;
			endif;	
		endfor;
		$list_options = "<span id='prev_month_multi'>&lt;</span> $first_month - $month_name $start_year <span id='next_month_multi' rel='$month'>&gt;</span>";
		return array('list_options'=>$list_options,'calendar'=> $calendar);
	}
	
	function multi_calendar(&$month =0, &$year=0, $id=''){
		 if(empty($id)) 
		 	return;
		$lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));
		$date=getDate();
		if($month==0):	
			$month=$date["mon"];
			$year=$date["year"];	
		endif;
		$this_month=getDate(mktime(0,0,0,$month,1,$year));
		$first_week_day = $this_month["wday"];
		$days_in_this_month = cal_days_in_month(CAL_GREGORIAN, $month, $year); //round(($next_month[0]-$this_month[0])/(60*60*24));
		
		
		if(!empty($this->property['rental_season'])){$rental_season=$this->property['rental_season'];}
		else {$listings=$this->property($id);$rental_season=$listings['rental_season'];}
		
		ob_start();
		require(dirname(__FILE__).'/display/elements/calendar_multi.php');	
		$contents=ob_get_contents();
		ob_end_clean();
		return $contents;
	}	
	
	function unavailable_date($id){
		global $wpdb;
		$notavail_dates = $wpdb->get_results("SELECT meta_value FROM `property_meta` WHERE property_id IN (".$id.") AND meta_key LIKE '%unavailable_date%'", ARRAY_A);
		$notavail_date = $notavail_dates[0]['meta_value'];
		
		$check_unavail=array();
		$unavailable_date=array();
		if(!empty($notavail_date)){
			$check_unavail=explode(" ",$notavail_date);
			for($ivail=0;$ivail<count($check_unavail);$ivail++){
				$check_unavail[$ivail]=trim($check_unavail[$ivail]);
				if($check_unavail[$ivail]!=''){
					$date_u = substr($check_unavail[$ivail],0,2);
					$month_u = substr($check_unavail[$ivail],2,2);
					$year_u = substr($check_unavail[$ivail],4,strlen($check_unavail[$ivail]));
					$check_unavail[$ivail]=date("Y-m-d",mktime(0,0,0,$month_u, $date_u, $year_u));
				}
				//else unset($check_unavail[$ivail]);
			}
		}
		return	$check_unavail;	
	}
	
	// bushbystays
	function show_months_only($limit = 12, $id='', $month = 0, $start_year = 0){
		for($count = 0; $count <= $limit; ++$count):
			$calendar .= $this->calendar_only($month, $start_year, $id);
			$month_name = date('F', mktime(0,0,0,$month,1));
			$list_options .= '<option value="only_'.strtolower($month_name) . '_' . $start_year . '" >'."$month_name $start_year ".'</option>';		
			if($month< 12):
				++$month;
			else:
				$month = 1;
				$start_year += 1;
			endif;	
		endfor;
		return array('list_options'=>$list_options,'calendar'=> $calendar);
	}
	
	function calendar_only(&$month =0, &$year=0, $id=''){
		 if(empty($id)) 
		 	return;
		$lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));
		$date=getDate();
		if($month==0):	
			$month=$date["mon"];
			$year=$date["year"];	
		endif;
		$this_month=getDate(mktime(0,0,0,$month,1,$year));
		$first_week_day = $this_month["wday"];
		$days_in_this_month = cal_days_in_month(CAL_GREGORIAN, $month, $year); //round(($next_month[0]-$this_month[0])/(60*60*24));
		
		/* unavailable date */
		global $wpdb;
		$notavail_dates = $wpdb->get_results("SELECT meta_value FROM `property_meta` WHERE property_id IN (".$id.") AND meta_key LIKE '%unavailable_date%'", ARRAY_A);
		$notavail_date = $notavail_dates[0]['meta_value'];
		
		if(!empty($notavail_date)){
			$check_unavail=explode(" ",$notavail_date);
			for($ivail=0;$ivail<count($check_unavail);$ivail++){
				$check_unavail[$ivail]=trim($check_unavail[$ivail]);
				if($check_unavail[$ivail]!=''){
					$date_u = substr($check_unavail[$ivail],0,2);
					$month_u = substr($check_unavail[$ivail],2,2);
					$year_u = substr($check_unavail[$ivail],4,strlen($check_unavail[$ivail]));
					$check_unavail[$ivail]=date("Y-m-d",mktime(0,0,0,$month_u, $date_u, $year_u));
				}
			}
		}
		
		if(!empty($this->property['rental_season'])){$rental_season=$this->property['rental_season'];}
		else {$listings=$this->property($id);$rental_season=$listings['rental_season'];}
		ob_start();
		require(dirname(__FILE__).'/display/elements/calendar_table_only.php');	
		$contents=ob_get_contents();
		ob_end_clean();
		return$contents;
	}
	
	//barclaybusiness
	function categories($parent_id='') { 
		global $wpdb;		
		$q = "SELECT `name` FROM `categories` WHERE parent_id = ".intval($parent_id);
		return $wpdb->get_results($q, ARRAY_A);
	}
	
	function get_state() { 
		global $wpdb;
		$sql = "SELECT DISTINCT `state` FROM `properties` order by state";
		$results = $wpdb->get_results($sql, ARRAY_A);		
		return $results;		
	}
	
	function get_region($state='', $orderby='town_village') {
		global $wpdb;
		
		$q = "SELECT DISTINCT `town_village` FROM `properties` ";
		
		if(!empty($state))
			$q .= "WHERE `state`='".esc_sql(addslashes($state))."' ";
		
		if(!empty($orderby))
			$q .= "ORDER BY $orderby";
		
		return $wpdb->get_results($q, ARRAY_A);
	}
	
	//bondibeach rentals
	function locationInfo($is_experience, $limit=5){ 
		global $wpdb;		
		$q = "SELECT * FROM LocationInfo_suburbs a, LocationInfo_photos b WHERE a.suburb_id=b.suburb_id AND a.experience = '$is_experience' ORDER BY RAND() LIMIT $limit";
		$result=$wpdb->get_results($q, ARRAY_A); 		
		return $result;
	
	}
	
	function getSherlockBookingID($xml_id){
		if(empty($xml_id)) return 0;
		
		$sherlock_abn 		= '90151323527'; 
		$sherlock_client_id = '9999195';
		return $sherlock_abn.$sherlock_client_id.$xml_id;		
	}
	
	function rentals($id=null) {
		global $wpdb;
		$sql = "SELECT * FROM `Rentals` WHERE `property_id` = =".intval($id)." order by `id` DESC LIMIT 1";
		$results = $wpdb->get_row($sql, ARRAY_A);
		
		return $results;
	}
	
	function comments_properties($id=null) {
		global $wpdb;
		$sql = "SELECT * FROM `comments_properties` WHERE `property_id` = ".intval($id)." order by `id` DESC LIMIT 1";
		$results = $wpdb->get_row($sql, ARRAY_A);
		
		return $results;
	}
	
	//bushby
	function get_office($office_id){
		global $wpdb;
		if(empty($office_id))return;
		$offices=$wpdb->get_results("select * from office where id=".intval($office_id), ARRAY_A );
		$office=$offices[0];
		if(!empty($office['unit_number']))$office['street_address'] = $office['unit_number'] .'/';
		$office['street_address'] .=  $office['street_number'] . ' ' .  $office['street']  ;
		return $office;
	}
	
	function simple_user($user_id){
		global $wpdb;
		$placeholder_image= $this->paths['theme'].'images/default_avatar.jpg';
		$sql="select id as user_id, office_id, CONCAT(firstname, ' ', lastname) as name, email, phone as business_phone, mobile, role as position_for_display, (select url from attachments where parent_id= users.id and type='portrait' and description='large' and position='0' limit 1) as photo from users where id =".intval($user_id);
		$user=$wpdb->get_results($sql, ARRAY_A);
		if($user[0]['photo']=='')$user[0]['photo']=$placeholder_image;
		return $user[0];
	}
	
	//castleproperty
	function sold_leased_search($tag,$list){	
		$tag_condition = array('list'=>$list,'group'=>$tag, 'order'=>$tag,'order_direction'=>'ASC','limit'=>1000, 'page'=>'1');		
		$tag_condition['status']='2,6';
		$results = $this->properties($tag_condition, true, "SELECT $tag AS tag, COUNT(*) AS number_of_properties");
		return $results;
	}
	
	//calibre
	function displayBookInspectionLink($listing, $text='Book Inspection'){
		
		//limited to office calibre redhill - 1841
		if($listing['office_id'] != 1841) return;
		if($listing['type'] != 'ResidentialLease') return;
		
		
		$params['agentID']  = 'AP'.$listing['office_id'];
		$params['uniqueID'] = $listing['id'];
		$params['imgURL']   = $listing['thumbnail'];
		
		$url = 'http://www.inspectrealestate.com.au/RegisterOnline/Register.aspx?'.http_build_query($params, '&');

		?><a href="<?php echo $url; ?>" target="_blank" title="Book Inspection"><?php echo $text; ?></a><?php
	}
	
	//exchange-property
	function time_diff($date){
		$date1 = time();
		
		$daydate=date("d", strtotime($date));
		$monthdate=date("n", strtotime($date));
		$yeardate=date("Y", strtotime($date));
		$hourdate=date("H", strtotime($date));
		$mindate=date("i", strtotime($date));
		$secdate=date("s", strtotime($date));
		$date2 = mktime($hourdate,$mindate,$secdate,$monthdate,$daydate,$yeardate);// hour, min, sec, month, day, year
		
		$dateDiff = $date1 - $date2;
		$fullmin = floor($dateDiff/(60));
		return $fullmin;		
	}
	
	//liveagent
	function opendates2($params=array()){
		global $helper, $wp_query, $wpdb;
		extract($params);		
		extract($this->search($params));		
		if(!empty($days))$condition.=" AND DATEDIFF(opentimes.date,CURDATE()) <= ".$days." and opentimes.date> CURDATE() ";		
		if(!empty($weekday))$condition.=" AND DAYNAME(`date`) = '$weekday' ";					
		$condition.=" and properties.id=opentimes.property_id and `date`!='0000-00-00' AND opentimes.date >= CURDATE() ";
		
		$params['page'] = $wp_query->query_vars['page'];
		if (!empty($params['limit'])):
			if(isset($params['page']))$limit_start = $params['page'] * $params['limit'] - ($params['limit']) . ", ";			
			$sql_limit = " LIMIT $limit_start " . $params['limit'] ;
		endif;
		
		$tbl_properties = "properties";
		$sql="SELECT $tbl_properties.id, $tbl_properties.type, ($tbl_properties.garage + $tbl_properties.carport + $tbl_properties.off_street_park) AS carspaces, $tbl_properties.bedrooms, $tbl_properties.show_price as display_sold_price,$tbl_properties.bathrooms, $tbl_properties.headline, $tbl_properties.postcode,$tbl_properties.property_type,$tbl_properties.display_price as price_display, $tbl_properties.price, $tbl_properties.display_price_text as price_display_value, 
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,
			$tbl_properties.status, deal_type,$tbl_properties.display_address, $tbl_properties.unit_number, $tbl_properties.street_number, $tbl_properties.street, $tbl_properties.state, $tbl_properties.suburb, $tbl_properties.agent_id, $tbl_properties.office_id, `date` as open_date, `start_time`, `end_time` from properties , opentimes where $condition ORDER BY `date` , `start_time` , `suburb` $sql_limit";			
		
		$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count From properties , opentimes where $condition");
		if($this->debug)print $sql ;		
			
		$opendates = $wpdb->get_results($sql, ARRAY_A);
		//die($sql);	

		if(is_array($opendates))array_walk($opendates,array( $this, 'format_property'));
	
		foreach ($opendates as $property):			
			$all_opentimes[$property['open_date']][] = $property;
		endforeach;	
		
		$all_opentimes['results_count'] = $results_count;
		$limit=($params['limit']=='')?$this->params['limit']:$params['limit'];
		$pagination = $helper->paginate($limit, $this->params['page'], $results_count);
		foreach($this->settings['general_settings']['display_pagination'] as $paginate)$all_opentimes['pagination_' . $paginate] = $pagination; 		
			
		return $all_opentimes;
	}
	
	//micm
	function opendates3($list = 'both', $days='',$order='',$suburb='',$bed='',$bath='',$min='',$max=''){
		global $helper, $wpdb;
		/*Simply get all the distinct and not-past dates in which properties from this office are open for inspection*/
		$date_condition = $this->properties(array('list'=>$list, 'limit'=>'','suburb'=>$suburb,'bedrooms'=>$bed,'bathrooms'=>$bath,'price_min'=>$min,'price_max'=>$max), false, "SELECT `id`"); 
		if(!empty($days))$extra_condition=" AND DATEDIFF(opentimes.date,CURDATE()) <= ".$days;
		if($order=='suburb')
		{	
			$opendates = $wpdb->get_results("SELECT opentimes.date, opentimes.start_time, opentimes.property_id FROM opentimes,properties WHERE opentimes.property_id = properties.id AND opentimes.property_id IN ($date_condition) and opentimes.date>= CURDATE()  $extra_condition ORDER BY opentimes.date ASC,properties.suburb ASC", ARRAY_A);
		}
		else if($order=='price')
		{	
			$opendates = $wpdb->get_results("SELECT opentimes.date, opentimes.start_time, opentimes.property_id FROM opentimes,properties WHERE opentimes.property_id = properties.id AND opentimes.property_id IN ($date_condition) and opentimes.date>= CURDATE()  $extra_condition ORDER BY opentimes.date ASC,properties.price ASC", ARRAY_A);
		}
		else if($order=='bedrooms')
		{	
			$opendates = $wpdb->get_results("SELECT opentimes.date, opentimes.start_time, opentimes.property_id FROM opentimes,properties WHERE opentimes.property_id = properties.id AND opentimes.property_id IN ($date_condition) and opentimes.date>= CURDATE()  $extra_condition ORDER BY opentimes.date ASC,properties.bedrooms ASC", ARRAY_A);
		}
		else if($order=='property_type')
		{	
			$opendates = $wpdb->get_results("SELECT opentimes.date, opentimes.start_time, opentimes.property_id FROM opentimes,properties WHERE opentimes.property_id = properties.id AND opentimes.property_id IN ($date_condition) and opentimes.date>= CURDATE()  $extra_condition ORDER BY opentimes.date ASC,properties.property_type ASC", ARRAY_A);
		}
		else if($order=='start_time')
		{	
			$opendates = $wpdb->get_results("SELECT opentimes.date, opentimes.start_time, opentimes.property_id FROM opentimes,properties WHERE opentimes.property_id = properties.id AND opentimes.property_id IN ($date_condition) and opentimes.date>= CURDATE()  $extra_condition ORDER BY opentimes.date ASC,opentimes.start_time", ARRAY_A);
		}
		else
		{
			$opendates = $wpdb->get_results("SELECT `date`, `start_time`,`property_id` FROM opentimes WHERE property_id IN ($date_condition) and opentimes.date>= CURDATE()  $extra_condition ORDER BY `date` ASC, `start_time` ASC", ARRAY_A);
	    }
		foreach ($opendates as $opendate):
			$property = $this->properties(array('id'=>$opendate['property_id'],'suburb'=>$suburb,'bedrooms'=>$bed,'bathrooms'=>$bath,'price_min'=>$min,'price_max'=>$max) );
			$all_opentimes[$opendate['date']][] = $property[0];
		endforeach;
		return $all_opentimes;
	}

	function get_office_details($office_id){
		global $wpdb;
		$result=$wpdb->get_row("select * from office where id=".intval($office_id), ARRAY_A); 
		return $result;
	}
	
	//shore-commercial
	function photo_gallery($id) {
		global $wpdb;
		$q = "SELECT * FROM `attachments` WHERE `description` = 'thumb' AND `is_user` = '0' AND `parent_id` = ".intval($id);
		$query = $wpdb->get_results($q, ARRAY_A);
		
		return $query;
	}	
	
	//newsagency
	function property_search3($tag,$status='1,4',$property_type='',$category='',$state='',$town_village=''){
	
		if($tag!='type')$tag_condition = array('property_type'=>$property_type,'category'=>$category,'state'=>$state,'town_village'=>$town_village,'group'=>$tag, 'order'=>$tag,'order_direction'=>'ASC','limit'=>1000, 'page'=>'1');
		
		$tag_condition['status']=$status;
		$tag_condition['condition']="$tag is not null and $tag!=''";
		
		$results = $this->properties($tag_condition, true, "SELECT $tag AS tag, COUNT(*) AS number_of_properties");
		return $results;
	} 
	/* custom functions end */
}//endsclass
$realty = new realty;
?>
