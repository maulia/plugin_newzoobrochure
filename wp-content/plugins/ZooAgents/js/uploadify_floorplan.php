<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');
global $helper, $zoo_agents, $realty, $wpdb;

if (!empty($_FILES) && (is_user_logged_in() || $this->is_logged_in()) && intval($_REQUEST['id'])) {
	$id= intval($_REQUEST['id']);
	$uploaded_url=$realty->siteUrl.'wp-content/uploads/';
	$floorplans_upload_url=$uploaded_url."floorplans/$id/";

	$tempFile = $_FILES['Floordata']['tmp_name'];
	$targetPath = $_REQUEST['folder'];
	
	extract(pathinfo( $_FILES['Floordata']['name']));
	if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
	$filename=sanitize_title($filename);
	$basename =  "$filename.$extension";
	$final_filename = $targetPath . $basename;
	$original_url = $floorplans_upload_url . $basename;	

	for ($count = 1; file_exists( $final_filename ); $count++): 
		$basename = $filename . "_$count" .  '.' . $extension;
		$final_filename = $targetPath . $basename;	 			
		$original_url = $floorplans_upload_url . $basename;	 			
	endfor;	

	if($zoo_agents->save_file($tempFile, $final_filename)){
		$zoo_agents->resize($final_filename, 200, 'thumbnail');		
		$zoo_agents->resize($final_filename, 400, 'medium');		
		$zoo_agents->resize($final_filename, 800, 'large');		
		extract(pathinfo($final_filename));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
		$thumbnail_url=$floorplans_upload_url.$filename."_thumb.$extension";
		$medium_url=$floorplans_upload_url.$filename."_medium.$extension";
		$large_url=$floorplans_upload_url.$filename."_large.$extension";
		
		$position=$wpdb->get_var("select max(position) from attachments where parent_id=$id and type='floorplan'");
		if($position=='')$position=0;
		else $position=$position+1;				
										
		$zoo_agents->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'thumb','url'=>$thumbnail_url, 'position'=>$position));
		$zoo_agents->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'medium','url'=>$medium_url, 'position'=>$position));
		$zoo_agents->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'large','url'=>$large_url, 'position'=>$position));	
		$zoo_agents->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'original','url'=>$original_url, 'position'=>$position));					
	}	
	
	echo $final_filename;
}
?>