<?php get_header(); ?>

	<div id="content">
		<h2 class="page_title">Oops! File not found.</h2>
		
		<p>There could be a few different reasons for this:</p>
		<ul>
			<li>The page was moved.</li>
			<li>The page no longer exists.</li>
			<li>The URL is slightly incorrect.</li>
		</ul>

		<p>Try one of the following to get you back on track:</p>
		<ul>
			<?php wp_list_pages('title_li=&sort_column=menu_order&depth=1'); ?>
		</ul>

		<?php get_search_form(); ?>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>