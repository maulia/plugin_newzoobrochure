<?php get_header(); ?>
<div id="main_body">
<div id="content">
<div id="blog-div" class="search">

	<?php if (have_posts()) : ?>

		<h5 class="page_title">Search Results</h5>

		<?php while (have_posts()) : the_post(); ?>

			<div class="post">
				<?php if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; } ?>
				<div class="post_title_date<?php if(!function_exists('userphoto_the_author_thumbnail')) { echo ' no_author_thumb'; } ?>">
					<h2 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<p class="postdate"><?php the_time('jS F, Y') ?></p>
				</div>

				<div class="entry">
					<?php the_excerpt_reloaded(30, '<a>', 'excerpt', TRUE, 'more &#187;'); ?>
				</div>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>

			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<p class="alignleft"><?php next_posts_link('View older posts') ?></p>
			<p class="alignright"><?php previous_posts_link('View newer posts') ?></p>
		<div class="clearer"></div>
		</div>

	<?php else : ?>

		<h2 class="center">No posts found. Try a different search?</h2>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div><!-- end #blog-div -->
	</div><!-- end #content -->

<?php get_sidebar(); ?>
</div><!-- end #main_body -->
<?php get_footer(); ?>
