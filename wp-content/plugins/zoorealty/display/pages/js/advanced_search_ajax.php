<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
require($path.'/wp-load.php');
global $realty, $helper, $wpdb;
$settings=$realty->settings['widgets']['quick_search'];
$cat=$_GET['cat'];
$list=$_GET['list'];
$type=$_GET['type'];
if(strpos(strtolower($type), 'lease') !== false)$type='lease';
if(strpos(strtolower($type), 'lease') === false)$type='sale';
if($cat=='price'){ $price_options = $settings[$type]; $max_price = end($price_options);
?>
<p class="p1">
 <label>Price</label>
<select name="price_min" id="cbo_price_min">
	<option value="">Min Price</option>
	<?php foreach($price_options as $price_option): 
	$delimiter = strpos($price_option, ':'); 
	$value = substr($price_option, 0, $delimiter); ?>
	<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_min']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1);// if($price_option ==$max_price) echo '+'; ?></option>
	<?php endforeach;?>
</select>
</p>
<p class="p2">
<select name="price_max" id="cbo_price_max">
	<option value="">Max Price</option>
	<?php foreach($price_options as $price_option): 
	$delimiter = strpos($price_option, ':'); 
	$value = substr($price_option, 0, $delimiter); ?>
   <option value="<?php if($price_option ==$max_price)$value.="+";echo $value; ?>"<?php if( $value ==$_GET['price_max']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1); if($price_option ==$max_price) echo '+';  ?></option>	
	<?php endforeach;?>
</select>
</p>
<div class="clear"></div>
<?php } 

if($cat=='suburb') { $suburbs = $realty->property_search('suburb',$list); ?>
	<p class="chk-protype"><input type="checkbox" class="checkbox" value="1" id="all_suburb" onClick="check_all_suburb();"> All</p>
	<?php foreach($suburbs as $suburb){ if(!is_array($suburb)) continue;?>
	<p class="chk-protype"><input name="suburb[]" type="checkbox" class="checkbox" value="<?php echo ucwords(strtolower($suburb['tag'])); ?>" onClick="check_suburb('<?php echo $suburb['tag']; ?>');"> <?php echo ucwords(strtolower($suburb['tag'])); ?></p>
	<?php } ?>
	<div class="clear"></div> 
<?php
} 

if($cat=='feature') { 
$office_id_all= implode(', ', $realty->settings['general_settings']['office_id']);
switch(strtolower($list)){
	case 'sale':
		$list_condition .= " and properties.type IN ('ResidentialSale', 'BusinessSale', 'ProjectSale')";
	break;
	case 'lease':
		$list_condition .= " and properties.type IN ('ResidentialLease', 'HolidayLease')";
	break;
	case 'commercial':
		$list_condition .= " and properties.type IN ('Commercial')";
	break;
	default:
		$list_condition .= " and properties.type IN ('$list')";
	break;
}
$list_features = $wpdb->get_results("select distinct feature from property_features where property_id in (select id from properties where office_id IN ($office_id_all) $list_condition) order by feature", ARRAY_A);
?>
	 <p class="chk-protype"><input type="checkbox" class="checkbox" value="1" id="all_feature" onClick="check_all_feature();" checked> All Features</p>
	<?php foreach($list_features as $item):?>
	<p class="chk-protype"><input type="checkbox" class="checkbox" name="features[]" value="<?php echo str_replace("\n",'',$item['feature']); ?>" onClick="check_feature('<?php echo str_replace("\n",'',$item['feature']); ?>');"> <?php echo ucwords(strtolower($item['feature'])); ?></p>
	<?php endforeach; ?>
	<div class="clear"></div>
<?php
} 

if($cat=='property_type') { $property_types = $realty->property_search('property_type',$list); ?>
	 <p class="chk-protype"><input type="checkbox" value="" id="all_type" onClick="check_all_type();" checked> All</p>
	<?php 
	// display these property types at top, the the rest is sort by alphabet
	$display_first=array('House','Land','Rural','Apartment');
	foreach($property_types as $property_type):
		if(!is_array($property_type)) continue;
		if(in_array($property_type['tag'],$display_first)){						
		?>
		<p class="chk-protype"><input name="property_type[]" type="checkbox" value="<?php echo $property_type['tag']; ?>" onClick="check_type('<?php echo $property_type['tag']; ?>');" <?php if(is_array($_GET['property_type'])){ if(in_array($property_type['tag'],$_GET['property_type'])) echo 'checked="checked"';}?>> <?php echo $property_type['tag']; ?></p>
	<?php } endforeach; 
	
	foreach($property_types as $property_type):
		if(!is_array($property_type)) continue;
		if(!in_array($property_type['tag'],$display_first)){ ?>
		<p class="chk-protype"><input name="property_type[]" type="checkbox" value="<?php echo $property_type['tag']; ?>" onClick="check_type('<?php echo $property_type['tag']; ?>');"> 	<?php echo $property_type['tag']; ?></p>
	<?php } endforeach; ?>
	 <div class="clear"></div>
<?php 
} 
?>