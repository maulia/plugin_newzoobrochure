<?php
global $helper;
$condition=$_GET;
if($this->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']=='1')$condition['status']='1,2,4,6';
if($this->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']!='1')$condition['status']='1,2,4';
if($this->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']=='1')$condition['status']='1,4,6';
if($this->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $this->settings['general_settings']['display_leased_properties_in_search_result']!='1')$condition['status']='1,4';

if(!empty($condition['keywords'])){	
	switch(strtolower(trim($condition['keywords']))){
		case "nsw (new south wales)":case "new south wales":$condition['keywords']='NSW';break;
		case "sa (south australia)":case "south australia":$condition['keywords']='SA';break;
		case "vic (victoria)":case "victoria":$condition['keywords']='VIC';break;
		case "qld (queensland)":case "queensland":$condition['keywords']='QLD';break;
		case "wa (western australia)":case "western australia":$condition['keywords']='WA';break;
		case "tas (tasmania)":case "tasmania":$condition['keywords']='TAS';break;
		case "act (australian capital territory)":case "australian capital territory":$condition['keywords']='ACT';break;
		default : $condition['keywords']=$condition['keywords'];break;
	}
	$return=$this->keywords_condition($condition['keywords'],$condition['sur_suburbs']);
	if($_GET['debug'])var_dump($return);
	$condition['condition']=$return['sql_keywords'];
	if(!empty($return['suburb']) && $condition['sur_suburbs']=='1'){
		$current_sort=($condition['order']=='')?$this->params['order']:$condition['order'];
		$condition['order']="(`suburb` = '".$return['suburb']."') DESC, $current_sort";
	}
}

$condition['keywords']=''; 
if(is_null($listings))$listings = $this->properties($condition);

/* similar listings */
if($listings['results_count'] < 1){
	
	// keep town_village, remove suburb
	if(!empty($_GET['keywords'])){
		unset($condition['condition']);
		if($return['town_village'])$condition['town_village']=$return['town_village'];
		if($return['state'])$condition['state']=$return['state'];
		$listings = $this->properties($condition);
	}elseif(!empty($_GET['suburb']) && $_GET['suburb']['suburb'][0]=!'%25'){
		unset($condition['suburb']);
		$listings = $this->properties($condition);
	}
	
	// remove town_village
	if($listings['results_count'] < 1 && !empty($condition['town_village'])){
		unset($condition['town_village']);
		$listings = $this->properties($condition);		
	}
	
	// keep state & property type, remove other criteria
	if($listings['results_count'] < 1 && ($condition['land_min'] || $condition['bathrooms']  || $condition['bedrooms'] || $condition['max_bed'] || $condition['price_min'] || $condition['price_max'])){
		unset($condition['land_min']);
		unset($condition['bathrooms']);
		unset($condition['bedrooms']);
		unset($condition['max_bed']);
		unset($condition['price_min']);
		unset($condition['price_max']);
		$listings = $this->properties($condition);
	}
	
	if($listings['results_count'] < 1){
		return print '<p class="no_properties_error">We are sorry, no property listings were found to match this criteria, please check back again soon or contact us with your requirements.</p>';
	}
	else{
		print '<p class="no_properties_error">We cannot find any properties that exactly match your criteria but here are some similar listings.</p>';
	}
}



$queryString = "?" .  preg_replace('/(&?order=)(&?limit=)([\w]+&?)/is', '', urldecode($_SERVER['QUERY_STRING']));
if(empty($_GET['list']))$list="&list=".$this->params['list'];
if($this->renderer=='sold')$list.="&status=2,6";
$page_now=($this->params['page']=='')?'1':$this->params['page'];
$pdf_param=urldecode($_SERVER['QUERY_STRING']);
if(empty($pdf_param))$pdf_param.='page='.$page_now.$list; else $pdf_param.='&page='.$page_now.$list;

if($this->settings['general_settings']['search_results_display']=='thumb' || $this->settings['general_settings']['search_results_display']=='')$default='0';
if($this->settings['general_settings']['search_results_display']=='list')$default='1';
if($this->settings['general_settings']['search_results_display']=='map')$default='2';
 
$view=0;
if($this->settings['general_settings']['display_search_result_in_thumbnail_format']=='1')$view++;
if($this->settings['general_settings']['not_display_search_result_in_list_format']!='1')$view++;
if($this->settings['general_settings']['display_map_in_search_result']=='1')$view++;

$tab=($_COOKIE['active_tab']=='')?$default:$_COOKIE['active_tab'];
if($tab=='0' && $this->settings['general_settings']['display_search_result_in_thumbnail_format']!='1')$tab=1;
if($tab=='2' && $this->settings['general_settings']['display_map_in_search_result']!='1')$tab=1;

if ($this->settings['search_results']['action_buttons']){ 
 ?>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/jquery.prettyPhoto.js?v=0,01"></script>
<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php echo $this->pluginUrl.'css/prettyPhoto.css'; ?>?v=0,01" />
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("a[rel^='prettyPhoto']").prettyPhoto({slideshow:5000, autoplay_slideshow:true,allow_resize: false,deeplinking: false,social_tools: false});
		jQuery("a[rel^='iframe']").prettyPhoto({allow_resize: false,deeplinking: false,social_tools: false, iframe:true});
	});
</script>
<?php } ?>
<div id="search_results_container">
	<div id="search_results"> 
		<div id="sorter_pagination">
			<p class="number_properties">
				There <?php echo ($listings['results_count'] == 1) ? 'is': 'are'; ?> <strong><?php echo $listings['results_count']; ?></strong>
				<?php echo "$types "; ?><?php echo ($listings['results_count'] == 1) ? 'property': 'properties'; ?> available
				<?php if (!empty($price_min)): ?> with prices from <strong>$<?php echo number_format( $price_min, 0, '.',','); ?></strong><?php endif; ?>
				<?php if (!empty($price_max)): ?> to <strong>$<?php echo number_format( $price_max, 0, '.',','); ?></strong><?php endif; ?>
				<?php if (!empty($keywords)) echo " in $keywords"; ?> with your search results.		
			</p>
			<div class="sorter_search_quick">
				<select name="" onchange="window.location=this.value">
					<option value="#">Sort by:</option>
					 <option value="<?php echo $queryString . "&amp;order=price";?>"<?php if($this->params['order'] =='price' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='price') echo ' selected="selected"';?>>Price</option>
					<option value="<?php echo $queryString. "&amp;order=suburb";?>"<?php if($this->params['order'] =='suburb' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='suburb') echo ' selected="selected"';?>>Suburb</option>
					<option value="<?php echo $queryString. "&amp;order=created_at";?>"<?php if($this->params['order'] =='created_at' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='created_at') echo ' selected="selected"';?>>Date Added</option>
				</select>		
				
				<?php if($this->settings['general_settings']['display_pdf_button_in_search_result']=='1'){ ?>
				<p class="pdf btn"><a href="javascript:void(0)" onclick="return window.open('<?php echo $this->pluginUrl."display/elements/search_brochure.php?$pdf_param"; ?>', 'Brochure', 'width=820, height=960,resizable=1,scrollbars=1'); return false;" title="Print Results" class="btn">PDF</a></p>
				<?php } ?>
				<div class="clear"></div>
			</div>	
			
			<?php 
			if($view>1){ 
				wp_print_scripts('jquery-ui-core');wp_print_scripts('jquery-ui-tabs');							
				if($this->settings['general_settings']['display_map_in_search_result']=='1'){
					foreach($listings as $property):  
						if(!is_array($property)) 
							continue; 
						if(!empty($longitude) && !empty($latitude)) 
							break; 
						if(!empty($property['latitude']) && !empty($property['longitude']) && empty($longitude) && empty($latitude)){
							$latitude=$property['latitude'];
							$longitude=$property['longitude'];
						}
					endforeach;
					if(!empty($latitude) && !empty($longitude))
						$display_map='1';
					else 
						$display_map='0';
				} ?>
				
				<input type="hidden" id="thumb_tab" value="0" >
				<input type="hidden" id="list_tab" value="1" >	
				<input type="hidden" id="map_tab" value="2" >
				
				<script type="text/javascript">
				jQuery(document).ready(function(){				
					<?php if($display_map=='1')
					{ ?>
						var map = null;
						jQuery("#search_results").tabs({active:<?php echo $tab;?>});
						jQuery("#map_tabs").click(function(){
							var gmap_resize_list = 0;
							zoogooglemaps_resize();
						});
					<?php 
					} else 
					{ ?>
						jQuery("#search_results").tabs({active:<?php echo $tab;?>});
					<?php 
					}  ?>					
				});
				</script>
				
				<div id="search_results_tab">
					<ul class="shadetabs search_results_view_option">
						<li class="thumbnail_format" <?php if($this->settings['general_settings']['display_search_result_in_thumbnail_format']!='1') echo 'style="display:none !important;"';?> ><a href="#thumbnail_format" onClick="active_tab('thumb');" title="View Thumbnails Mode">Thumbnails</a></li>
						<li class="list_format" <?php if($this->settings['general_settings']['not_display_search_result_in_list_format']=='1') echo 'style="display:none !important;"';?>><a href="#list_format" onClick="active_tab('list');" title="View List Mode">List</a></li>
						<li class="map_search_result"  <?php if($display_map!='1') echo 'style="display:none !important;"';?>><a href="#map_search_result" id="map_tabs" onClick="active_tab('map');" title="View Map Mode">Map</a></li>
					</ul>
				</div>
				<?php echo $listings['pagination_above'];
			} ?>

		</div> <!-- #sorter_pagination -->	
		<?php $this->element('listings', $listings); 

		if($display_map=='1'):?>
		<div id="map_search_result">
		<?php include(dirname(dirname(__FILE__)).'/elements/map_search_results.php'); ?>
		</div>
		<?php endif; 

		if(!empty($listings['pagination_below'])): ?>
		<div id='bottom_pagination'><?php echo $listings['pagination_below']; ?></div>
		<?php endif;?>
	</div>
</div>
<script>
<?php if($view>1){ ?>
function active_tab(tab){
	var index;
	if(tab=="thumb")index=document.getElementById('thumb_tab').value;
	else if(tab=="list")index=document.getElementById('list_tab').value;
	else if(tab=="map")index=document.getElementById('map_tab').value;
	var url="<?php echo $this->pluginUrl; ?>display/elements/active_tab.php?tab="+escape( index );
	jQuery.get(url);
}
<?php } ?>
</script>