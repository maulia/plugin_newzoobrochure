<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$uploaded_path=ABSPATH . 'wp-content/uploads/zooproperty_api/attachments/';
$uploaded_url=$realty->siteUrl.'wp-content/uploads/zooproperty_api/attachments/';
$logos_upload_path=$uploaded_path.'user_photos/';
$logos_upload_url=$uploaded_url.'user_photos/';
$groups=array('Executive','Sales Agent','Property Manager','Admin','Industrial','Office Leasing','Office Sales','Retail','Other');


if(isset($_POST['submit'])){
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'firstname':case 'lastname':case 'email':
					 $return.=$helper->humanize($key)." can not be empty.<br/>"; break;					
				break;
			}
		}		
	}
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else{
		$_POST['id']=$wpdb->get_var("select max(id) from users");
		if($_POST['id']<1000000)$_POST['id']=1000000;
		$_POST['id']=$_POST['id']+1;
		$_POST['office_id']=$office_id;
		$_POST['display']='1';
		$_POST['position']=$wpdb->get_var("select max(position) from users where office_id in ($office_id)");
		$_POST['position']=$_POST['position']+1;
		$_POST['created_at']=$_POST['updated_at']=date("Y-m-d h:i:s");
		if($this->insert_form( "users", $_POST )){
			$id=$wpdb->insert_id;
			?>
			<script type="text/javascript">
				location.href="<?php echo $realty->siteUrl."dashboard/agents/?office_id=$office_id&x=1"; ?>";
			</script>
			<?php
		}
		else $return.= 'Agent could not be saved. please try again later';
		echo "<div id='return'>$return</div>";
	}
	$user=$_POST;	
}
?>
<p class="add-agent-link"><a href="<?php echo $realty->siteUrl."dashboard/new-agent/?office_id=$office_id"; ?>">Add New Agent</a></p>
<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off" enctype="multipart/form-data" class="form-team">
    <div id="address_div" class="block_content">
        <table>
            <tr>
                <td class="label"><strong>First Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="full" id="firstname" name="firstname"  value="<?php echo $user['firstname']; ?>">						
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Last Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="full" id="lastname" name="lastname"  value="<?php echo $user['lastname']; ?>">						
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Email<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="full" id="email" name="email"  value="<?php echo $user['email']; ?>">
                </td>
            </tr>
            <tr>
                <td class="label">Phone</td>
                <td><input type="text" name="phone" class="full"  value="<?php echo $user['phone']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Mobile</td>
                <td><input type="text" name="mobile" class="full"  value="<?php echo $user['mobile']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Fax</td>
                <td><input type="text" id="fax" name="fax" class="full"  value="<?php echo $user['fax']; ?>"></td>
            </tr>
			<tr>
                <td class="label">Role</td>
                <td><input type="text" name="role" class="full"  value="<?php echo $user['role']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Group</td>
                <td>
                <select name="group" class="full">
                    <option value="">Please Select</option>
                    <?php foreach($groups as $group){ ?>
                    <option value="<?php echo $group; ?>" <?php if($group==$user['group'])echo 'selected="selected"'; ?>><?php echo $group; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr>
                <td class="label">Description</td>
                <td>
                    <textarea name="description" rows="10" class="full"><?php echo $user['description']; ?></textarea>
                </td>
            </tr>
           <?php /*
		   <tr>
                <td class="label">Suburb</td>
                <td><input type="text" id="suburb" name="suburb" class="full"  value="<?php echo $user['suburb']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Display on site?</td>
                <td><input type="checkbox" id="display" name="display" value="1" <?php if($user['display']==1)echo 'checked="checked"'; ?>></td>
            </tr>
            */ ?>
        </table>
        
        <p class="button"><input type="submit" name="submit" value="Save" class="btn"></p>
        
    </div>
	<?php /*
	<div class="content-right-column">
		<div id="social_div" class="block_content">
			<h4>Social Networking</h4>
			<input type="hidden" name="id"  value="<?php echo $user['id']; ?>">		
			<table>				
				<tr>
					<td class="label">Facebook account</td>
					<td>www.facebook.com/ <input type="text" name="facebook_username" class="half"  value="<?php echo $user['facebook_username']; ?>"></td>
				</tr>	
				<tr>
					<td class="label">Twitter account</td>
					<td>www.twitter.com/ <input type="text" name="twitter_username" class="half"  value="<?php echo $user['twitter_username']; ?>"></td>
				</tr>	
				<tr>
					<td class="label">Linkedin account</td>
					<td>www.linkedin.com/ <input type="text" name="linkedin_username" class="half"  value="<?php echo $user['linkedin_username']; ?>"></td>
				</tr>		
			</table>			
		</div>
		<div class="clearer"></div>
		
		
	</div>	
	*/ ?>
</form>