<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * This class used to remove dependence with Realty Plugin
 * with this class, zoobrochure plugin can stand alone
 * 
 * @author Jony <jony@agentpoint.com.au>
 * @package zoobrochure
 * @subpackage zoobrochure_property
 * @since 2.0.0
 */
class zoobrochure_property {

    private static $instance = null;

    /**
     * Main zoobrochure_property Instance.
     *
     * Ensures only one instance of zoobrochure_property is loaded or can be loaded.
     *
     * @since 2.0.0
     * @static
     * @return object zoobrochure_property Main instance
     */
    public static function instance() {
        if (null == self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Produce detail individual property
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return boolean|array 
     */
    public function property($property_id = '') {

        if (empty($property_id)) {
            return false;
        }

        global $wpdb;

        $alias = array(
            'land_area as land_size',
            '(garage + carport + off_street_park) AS carspaces',
            'display_price_text as price_display_value',
            'display_price as price_display',
            'show_price as display_sold_price',
            'floor_area as building_size',
            'date_available as available_at',
            "(Case when status='2' Then sold_at	else '' END) as sold_at",
            "(Case when status='2' Then sold_price else '' END) as sold_price",
            "(Case when status='6' Then leased_at else '' END) as leased_at",
            "(Case when status='6' Then leased_price	else '' END) as leased_price",
            "(Case when datediff(curdate(),created_at) <= 7 Then 'NEW' else '' END) as new",
            "(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END) as auction_date",
            "(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END) as auction_time",
            "(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place else '' END) as auction_place"
        );
        $schema = implode(', ', $alias);
        $property = $wpdb->get_row($wpdb->prepare("SELECT properties.*, {$schema} FROM properties WHERE id = %d", $property_id), ARRAY_A);
        $property_meta = $this->property_meta($property_id);
        foreach ($property_meta as $meta) {
            if (!isset($property[$meta['meta_key']])) {
                $property[$meta['meta_key']] = $meta['meta_value'];
            }
        }
        $prefix_address = (!empty($property['unit_number'])) ? sprintf('%s/%s', $property['unit_number'], $property['street_number']) : $property['street_number'];
        $property['street_address'] = ($property['display_address'] == 1) ? sprintf('%s %s', $prefix_address, $property['street']) : '';
        $property['is_sold'] = (isset($property['status']) && ($property['status'] == 2)) ? true : false;
        $property['is_leased'] = (isset($property['status']) && ($property['status'] == 6)) ? true : false;
        $property['is_for_sale'] = (stripos($property['type'], "Lease") === false && strtolower($property['deal_type']) != 'lease');
        $property['itemized_features'] = $this->property_features($property_id);
        $property['photos'] = $this->photos($property_id);
        $property['user'] = $this->users($property_id);
        $property['floorplans'] = $this->photos($property_id, 'floorplan');
        $property['opentimes'] = $this->opentimes($property_id);
        $property['brochure'] = $this->brochure($property_id);
        $property['videos'] = $this->videos($property_id);
        $property['office'] = $this->office($property['office_id']);
        $property['url'] = sprintf('%s/%d', site_url(), $property['id']);

        return $property;
    }

    /**
     * Will produce photos on each property or each agent
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $parent_id
     * @param string $type
     * @param int $is_user
     * @return array
     */
    public function photos($parent_id = '', $type = 'photo', $is_user = 0) {
        global $wpdb;
        $photos = [];
        $images = $wpdb->get_results($wpdb->prepare('SELECT url, description ,`position`, width, height FROM attachments WHERE parent_id = %d AND type = %s AND is_user = %d ORDER BY `position`, `description` ASC', $parent_id, $type, $is_user), ARRAY_A);
        foreach ($images as $img) {
            if (!empty($img['url'])) {
                if ($img['description'] == 'thumb') {
                    $img['description'] = 'small';
                }
                $photos[$img['position']][$img['description']] = $img['url'];
                $photos[$img['position']][$img['description'] . '_width'] = $img['width'];
                $photos[$img['position']][$img['description'] . '_height'] = $img['height'];
            }
        }
        return $photos;
    }

    /**
     * Property brochure list
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return array
     */
    public function brochure($property_id = '') {
        global $wpdb;
        return $wpdb->get_results($wpdb->prepare('SELECT url, description, `position` FROM attachments WHERE parent_id = %d AND is_user = 0 AND type = %s ORDER BY `position`, `description` ASC', $property_id, 'brochure'), ARRAY_A);
    }

    /**
     * Property meta list
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return array
     */
    public function property_meta($property_id = '') {
        global $wpdb;
        return $wpdb->get_results($wpdb->prepare('SELECT meta_key, meta_value FROM property_meta WHERE property_id = %d', $property_id), ARRAY_A);
    }

    /**
     * Property features list
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return array
     */
    public function property_features($property_id = '') {
        global $wpdb;
        return $wpdb->get_col($wpdb->prepare('SELECT feature FROM property_features WHERE property_id = %d ORDER BY feature ASC', $property_id));
    }

    /**
     * Detail of Office
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $office_id
     * @return array
     */
    public function office($office_id = '') {
        global $wpdb;
        return $wpdb->get_row($wpdb->prepare('SELECT office.* FROM office WHERE id = %d', $office_id), ARRAY_A);
    }

    /**
     * Property opentimes list
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return array
     */
    public function opentimes($property_id = '') {
        global $wpdb;
        return $wpdb->get_results($wpdb->prepare('SELECT opentimes.date, opentimes.start_time, opentimes.end_time FROM opentimes WHERE `property_id`= %d and  opentimes.date >= CURDATE() ORDER BY opentimes.date ASC, opentimes.start_time ASC', $property_id), ARRAY_A);
    }

    /**
     * Property videos list
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return array
     */
    public function videos($property_id = '') {
        global $wpdb;
        return $wpdb->get_results($wpdb->prepare('SELECT * FROM property_video WHERE property_id = %d AND ((embed_code != "" and embed_code is not null) or (youtube_id != "" and youtube_id is not null))', $property_id), ARRAY_A);
    }

    /**
     * Produce list of team member that asiggned with current property id
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $property_id
     * @return array
     */
    public function users($property_id = '') {
        global $wpdb;
        $users = [];
        $user_ids = $wpdb->get_row($wpdb->prepare('SELECT user_id, secondary_user, third_user, fourth_user FROM properties WHERE id = %d LIMIT 1', $property_id));
        if (!empty($user_ids->user_id)) {
            $users[] = $this->user($user_ids->user_id);
        }
        if (!empty($user_ids->secondary_user)) {
            $users[] = $this->user($user_ids->secondary_user);
        }
        if (!empty($user_ids->third_user)) {
            $users[] = $this->user($user_ids->third_user);
        }
        if (!empty($user_ids->fourth_user)) {
            $users[] = $this->user($user_ids->fourth_user);
        }
        return $users;
    }

    /**
     * Detail of team member
     * 
     * @since 2.0.0
     * @global object $wpdb
     * @param int $user_id
     * @return boolean|array
     */
    public function user($user_id = '') {
        global $wpdb;
        $user = $wpdb->get_row($wpdb->prepare('SELECT users.*, CONCAT_WS(" ", firstname, lastname) as name, users.id as user_id FROM users WHERE id = %d LIMIT 1', $user_id), ARRAY_A);

        if (empty($user)) {
            return false;
        }

        $user['photo'] = $wpdb->get_var($wpdb->prepare('SELECT url FROM attachments WHERE parent_id = %d AND is_user = 1 AND type = %s ORDER BY `position` ASC', $user_id, 'portrait'));
        $user['landscape'] = $wpdb->get_var($wpdb->prepare('SELECT url FROM attachments WHERE parent_id = %d AND is_user = 1 AND type = %s ORDER BY `position` ASC', $user_id, 'landscape'));
        $user['big_photo'] = $wpdb->get_var($wpdb->prepare('SELECT url FROM attachments WHERE parent_id = %d AND is_user = 1 AND type = %s ORDER BY `position` DESC', $user_id, 'portrait'));
        $user['big_landscape'] = $wpdb->get_var($wpdb->prepare('SELECT url FROM attachments WHERE parent_id = %d AND is_user = 1 AND type = %s ORDER BY `position` DESC', $user_id, 'landscape'));
        $user['url'] = sprintf('%s/%s', site_url(), sanitize_title($user['name']));

        return $user;
    }

}
