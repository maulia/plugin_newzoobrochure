jQuery(function() {
	jQuery('#photo_upload').uploadify({
	'uploader'       : uploader,
	'script'         : script_photo,
	'buttonImg'      : button_image,
	'width'          : button_width,
	'height'         : button_height,
	'rollover'       : true,
	'cancelImg'      : cancel_image,
	'folder'         : photos_upload_path,
	'scriptData'  	 : {'id':id},
	'multi'          : true,
	'auto'           : true,
	'fileExt'        : '*.jpg',
	'fileDesc'       : 'Image Files (.JPG)',
	'queueID'        : 'photo_queue',
	'queueSizeLimit' : 30,
	'simUploadLimit' : 30,
	'removeCompleted': true,
	'onSelect'   : function(event,data) {
		jQuery('#photo_message').text(data.filesSelected + ' files have been added to the queue.');
	},
	'onComplete'  : function(event, ID, fileObj, response, data) {
		jQuery('#photo_list').load(load_photo);
    },
	'onAllComplete'  : function(event,data) {
		jQuery('#photo_message').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
	},
	'onQueueFull'    : function (event,queueSizeLimit) {
		jQuery('#photo_message').text("Maximum upload of photos is 30.");
		return false;
    }
	});		
	jQuery('#floorplan_upload').uploadify({
	'uploader'       : uploader,
	'script'         : script_floorplan,
	'buttonImg'      : button_image,
	'width'          : button_width,
	'height'         : button_height,
	'rollover'       : true,
	'cancelImg'      : cancel_image,
	'folder'         : floorplans_upload_path,
	'scriptData'  	 : {'id':id},
	'multi'          : true,
	'auto'           : true,
	'fileExt'        : '*.jpg;*.gif',
	'fileDesc'       : 'Image Files (.JPG, .GIF)',
	'queueID'        : 'floorplan_queue',
	'queueSizeLimit' : 30,
	'simUploadLimit' : 30,
	'removeCompleted': true,
	'fileDataName'   : 'Floordata',
	'onSelect'   : function(event,data) {
		jQuery('#floorplan_message').text(data.filesSelected + ' files have been added to the queue.');
	},
	'onComplete'  : function(event, ID, fileObj, response, data) {
		jQuery('#floorplan_list').load(load_floorplan);
    },
	'onAllComplete'  : function(event,data) {
		jQuery('#floorplan_message').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
	},
	'onQueueFull'    : function (event,queueSizeLimit) {
		jQuery('#floorplan_message').text("Maximum upload of floorplan is 30.");
		return false;
    }
	});		
	jQuery('#brochure_upload').uploadify({
	'uploader'       : uploader,
	'script'         : script_brochure,
	'buttonImg'      : button_image,
	'width'          : button_width,
	'height'         : button_height,
	'rollover'       : true,
	'cancelImg'      : cancel_image,
	'folder'         : brochures_upload_path,
	'scriptData'  	 : {'id':id},
	'multi'          : true,
	'auto'           : true,
	'fileExt'        : '*.pdf',
	'fileDesc'       : 'File Types (.pdf)',
	'queueID'        : 'brochure_queue',
	'queueSizeLimit' : 30,
	'simUploadLimit' : 30,
	'removeCompleted': true,
	'fileDataName'   : 'Brochuredata',
	'onSelect'   : function(event,data) {
		jQuery('#brochure_message').text(data.filesSelected + ' files have been added to the queue.');
	},
	'onComplete'  : function(event, ID, fileObj, response, data) {
		jQuery('#brochures').load(load_brochure);
    },
	'onAllComplete'  : function(event,data) {
		jQuery('#brochure_message').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
	},
	'onQueueFull'    : function (event,queueSizeLimit) {
		jQuery('#brochure_message').text("Maximum upload of brochure is 30.");
		return false;
    }
	});		
	jQuery(".image_list").sortable({});
	jQuery(".image_list: a[rel^='prettyPhoto']").prettyPhoto({allow_resize: false,deeplinking: false,social_tools: false});		
}); 