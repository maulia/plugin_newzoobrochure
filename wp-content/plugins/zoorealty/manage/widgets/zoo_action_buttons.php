<h4>Check options to display</h4>
<p>
	<input value="1" name="realty_widget_<?php echo $caption; ?>[back_link]" type="checkbox"<?php if($settings['back_link']) echo ' checked="checked"'; ?>> <label>Back Link</label><br/>
	<span style="margin-left:15px"><input type="text" name="realty_widget_<?php echo $caption; ?>[display][back_link]" value="<?php echo $settings['display']['back_link']; ?>"></span>
</p> 
<p>
	<input value="1" name="realty_widget_<?php echo $caption; ?>[new_search_link]" type="checkbox"<?php if($settings['new_search_link']) echo ' checked="checked"'; ?>> <label>New Search Link</label><br/>
	<span style="margin-left:15px"><input type="text" name="realty_widget_<?php echo $caption; ?>[display][new_search_link]" value="<?php echo $settings['display']['new_search_link']; ?>"></span>
</p>
<p>
	<input value="1" name="realty_widget_<?php echo $caption; ?>[email_self]" type="checkbox"<?php if($settings['email_self']) echo ' checked="checked"'; ?>> <label>Email to Self</label><br/>
	<span style="margin-left:15px"><input type="text" name="realty_widget_<?php echo $caption; ?>[display][email_self]" value="<?php echo $settings['display']['email_self']; ?>"></span>
</p>
<p>
	<input value="1" name="realty_widget_<?php echo $caption; ?>[email_friend]" type="checkbox"<?php if($settings['email_friend']) echo ' checked="checked"'; ?>> <label>Email to Friend</label><br/>
	<span style="margin-left:15px"><input type="text" name="realty_widget_<?php echo $caption; ?>[display][email_friend]" value="<?php echo $settings['display']['email_friend']; ?>"></span>
</p>
<p>
	<input value="1" name="realty_widget_<?php echo $caption; ?>[print_listing]" type="checkbox"<?php if($settings['print_listing']) echo ' checked="checked"'; ?>> <label>Property Brochure</label><br/>
	<span style="margin-left:15px"><input type="text" name="realty_widget_<?php echo $caption; ?>[display][print_listing]" value="<?php echo $settings['display']['print_listing']; ?>"></span>
</p>
<p>
	<input value="1" name="realty_widget_<?php echo $caption; ?>[pagination]" type="checkbox"<?php if($settings['pagination']) echo ' checked="checked"'; ?>> <label>Pagination</label><br/>
</p>

<br/>
<h4>Property Brochure</h4>
<p><input value="html" name="realty_widget_<?php echo $caption; ?>[brochure]" type="radio"<?php if($settings['brochure']=='html') echo ' checked="checked"'; ?>> <label>HTML Page</label> </p>	
<p><input value="create_pdf" name="realty_widget_<?php echo $caption; ?>[brochure]" type="radio"<?php if($settings['brochure']=='create_pdf'  || empty($settings['brochure'])) echo ' checked="checked"'; ?>> <label>Create Brochure</label></p>	
<p><input value="existing_brochure" name="realty_widget_<?php echo $caption; ?>[brochure]" type="radio"<?php if($settings['brochure']=='existing_brochure') echo ' checked="checked"'; ?>> <label>Existing Brochure</label></p>	