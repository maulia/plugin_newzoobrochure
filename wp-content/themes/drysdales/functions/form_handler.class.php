<?php
if(!class_exists('wp_page_maker'))
{
	require_once(dirname(__FILE__).'/wp_page_maker.class.php');
}


class form_handler_ex extends wp_page_maker{
	function filter_input($table,$post_vars){
		$sql = "SHOW COLUMNS FROM $table";		
		$query = mysql_query($sql) or die(mysql_error() . "<br>$sql<br>");
		
		while ($row = mysql_fetch_assoc($query)):
			$key = 	$row['Field'];
			if (array_key_exists($key,$post_vars))
				$return["`$key`"] = "'" . mysql_real_escape_string(trim($post_vars[$key])) . "'";		
		endwhile;	
		return $return;		
	}//ends function

	function get_where($table, $post_vars){
		$sql = "SHOW KEYS FROM $table";		
		$query = mysql_query($sql) or die(mysql_error() . "<br>$sql<br>");
		
		while ($row = mysql_fetch_assoc($query)){
			$Column_name = 	$row['Column_name'];
			if(array_key_exists($Column_name,$post_vars)):
				$where['where'] = "WHERE `$Column_name`='" . $post_vars[$Column_name] . "'";
				$where['primary_key'] = "`$Column_name`";				
				return $where;
			endif;
		}
		return false;			
	}//ends function

	function insert_form($table, $post_vars){	
		$filtered_input = $this->filter_input($table,$post_vars);
		$fields = implode(", ", array_keys($filtered_input));		
		$values = implode(", ", $filtered_input);		
		$sql = "INSERT INTO $table ( $fields ) VALUES ( $values )";
		return $this->query($sql);
	}//ends function

	function update_form($table, $post_vars){
		$filtered_input = $this->filter_input($table,$post_vars);
		$where = $this->get_where($table, $post_vars);
		if(!$where) return false; // No key was found in the database matching any of the input request variables. Theres nowhere to update
		$primary_key = $where['primary_key'];
		unset($filtered_input[$primary_key]);
		foreach ($filtered_input as $key=>$value)
			$updates .= "$key=$value, ";
		$updates = substr($updates,0,-2);
		$sql = "UPDATE $table SET $updates " . $where['where'];
		$this->query($sql);
		return true;
	}//ends function

	function delete_form($table, $post_vars){
		$where = $this->get_where($table, $post_vars);
		if(!$where) return false;
		$sql = "DELETE FROM $table " . $where['where'];
		$this->query($sql);
		return true;
	}//ends function
	
	function upload_file($upfile, $uploads_dir, $resize_to_thumb = 0, $resize_to_medium = 0,$allowed_types = 'jpg,gif,png'){
		$allowed_types = explode(",", $allowed_types);	
		if ( $upfile['size'] <= 0 ):
			$return['return'] = '<p>' . $upfile['name'] . " is empty.</p>"; // If the file has size 0 (is empty), leave immediately	
			return $return;
		endif;
		extract(pathinfo($upfile['name']));
		if ( array_search( strtolower($extension) , $allowed_types ) === false ):// If the file type is not in the list
			$return['return'] = "<p>The type of file ($extension) cannot be uploaded.</p>";
			return $return;
		endif;
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		$this->clean_filename($filename);
		$basename =  "$filename.$extension";
		$final_filename = $uploads_dir . $basename;

		for ($count = 1; file_exists( $final_filename ); $count++): //If the file already exists, it will be uploaded again with the _number appended to it in order to differentiate the file name.	
			$basename = $filename . "_$count" .  '.' . $extension;
			$final_filename = $uploads_dir . $basename;	 			
		endfor;	
		
		// all check has been done, upload the file that meets the requirements
		$tmp_name = $upfile['tmp_name'];
		if ( file_exists($tmp_name) ) :
		
		//$this->resize($tmp_name,400, $final_filename); return; //This will limit every file to 400px width.
	
		// test if the file can be uploaded to the REAL folder from the temporary folder
			if ( move_uploaded_file( $tmp_name , $final_filename ) ) :
				if($resize_to_thumb > 0)$this->resize($final_filename, $resize_to_thumb);		
				if($resize_to_medium > 0)$this->resize_medium($final_filename, $resize_to_medium);		
				$return ['return'] = "<p>$basename was uploaded successfully.</p>";
				$return ['path'] = $basename; 
			else:
				$return['return'] .= "<p>The file could not be saved onto the server. Please check if the upload folders exist and are set to the correct permissions.</p>"; 
			endif;
		else:
			// if it does not exist or that the file is too big for the server to handle, show error message
			$return['return'] .= '<p>Image was rejected by server. Probably for being larger than ' . get_cfg_var('upload_max_filesize') .' B.</p>';
			endif;
		return $return;		
	}//ends function 
	
	function resize($original,$new_width, $type='thumbnail') {
		if($type=='thumbnail')$thumbnail = $this->get_thumbnail($original); 
		if($type=='medium')$thumbnail = $this->get_medium($original); 
		if($type=='large')$thumbnail = $this->get_large($original); 
		$extension = pathinfo($thumbnail, PATHINFO_EXTENSION);// get information about the original file
		switch ( strtolower ( $extension ) ):
			case 'jpg':
			case 'jpeg':
				$img  = @imagecreatefromjpeg( $original );
				break;
			case 'gif':
				$img  = @imagecreatefromgif( $original );
				break;
			case 'png':
				$img  = @imagecreatefrompng( $original );
				break;
			default:
				return;
			break;
		endswitch;
		
		$ratio = $new_width / imagesx($img ); // So that the image's height is resized proportionally to the width.
		$new_height = (imagesy($img)) * $ratio;		
		$dstim = @imagecreatetruecolor($new_width, $new_height);	
		imagecopyresized($dstim, $img, 0, 0, 0, 0, $new_width, $new_height, imagesx($img), imagesy($img));
				
		switch ( strtolower ( $extension ) ):
			case 'jpg':
			case 'jpeg':
				imagejpeg($dstim, $thumbnail );
				break;
			case 'gif':
				imagegif($dstim, $thumbnail );
				break;
			case 'png':
				imagepng($dstim, $thumbnail );
				break;
			default:
				break;
		endswitch;	
		//chmod( $imgFilenameThumb , 0777 );		
	} // end function

	function get_thumbnail($image_path){ //Returns the thumbnail path of an image by examining the path of the larger image.
		extract(pathinfo($image_path));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		return "$dirname/$filename" ."_thumb.$extension";	
	}//ends function
	
	function get_medium($image_path){ //Returns the medium path of an image by examining the path of the larger image.
		extract(pathinfo($image_path));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		return "$dirname/$filename" ."_medium.$extension";	
	}

	function get_large($image_path){ //Returns the medium path of an image by examining the path of the larger image.
		extract(pathinfo($image_path));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		return "$dirname/$filename" ."_large.$extension";	
	}
	
	function build_input_array($input, $base_key=0){
		$multi_array = array();
			$i = 0;			
			foreach($input[$base_key] as $key=>$value):			
				foreach ($input as $big_key=>$big_value):
					if(!is_array($big_value)) continue;					
					$multi_array[$i][$big_key] = $big_value[$i];
				endforeach;
				++$i;
			endforeach;
		return $multi_array;
	}

	function recursive_remove_directory($directory, $empty=FALSE){		
		// if the option to empty is set to true, it will only empty the directory, but will not delete the directory folder
		/*thanks to http://lixlpixel.org/recursive_function/php/recursive_directory_delete/ 	for this function */
	
		if(substr($directory,-1) == '/')// if the path has a slash at the end we remove it here	
			$directory = substr($directory,0,-1);
				
		if(!file_exists($directory) || !is_dir($directory))	{// if the path is not valid or is not a directory we return false and exit the function
			return FALSE;	
		}elseif(!is_readable($directory)){// ... if the path is not readable we return false and exit the function		
			return FALSE;	
		}else{// ... else if the path is readable we open the directory
			$handle = opendir($directory);
			// and scan through the items inside
			while (FALSE !== ($item = readdir($handle))):
				// if the filepointer is not the current directory
				// or the parent directory
				if($item != '.' && $item != '..'):
					// we build the new path to delete
					$path = $directory.'/'.$item;
					// if the new path is a directory
					if(is_dir($path))// we call this function with the new path
						$this->recursive_remove_directory($path);				
					else// if the new path is a file we remove the file
						unlink($path);				
				endif;
			endwhile;
			// close the directory
			closedir($handle);
	
			// if the option to empty is not set to true
			if($empty == FALSE):			
				if(!rmdir($directory))// try to delete the now empty directory and return false if not possible				
					return FALSE;			
			endif;
			// return success
			return TRUE;
		}//ends big if
	}//ends function

	function name_file($relative_path){ // 
		extract(pathinfo($relative_path));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		return ucwords(preg_replace("/[^a-z0-9\\040\\.\\\\]/i"," ",$filename));
	}//ends function
	
	function multi_upload($input_name){
		require(dirname(dirname(__FILE__)). '/applications/multi_upload.php');
	
	}
	
	function format_url(&$url){
		$url = (!empty($url) && strpos($url, 'http://') === false)? 'http://' . $url : $url;
	
	}
	
}
?>