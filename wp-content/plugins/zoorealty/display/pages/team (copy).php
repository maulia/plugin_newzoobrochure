<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/update.css?v=1.00" />

<script type="text/javascript">
	function captchas_image_reload (imgId) 
	{
	var image_url = document.getElementById(imgId).src;
	image_url+= "&";
	document.getElementById(imgId).src = image_url;
	}
</script>
<?php
global $wpdb; 
// $helper = $realty->helper;
// $user = $this->user($_GET['u'], true); //$user = $this->user($this->params['u'], true);
// print_r ($user);
$params = $this->params;
$group = $params['group'];
$user_id = $params['user_id'];
$user = $this->user($user_id, true); 
?><div style="display:none"><?php print_r($group); ?></div><?php

//add agent page
$office_id = implode(",",$this->settings['general_settings']['office_id']);
$agents = $wpdb->get_results("SELECT * FROM users WHERE office_id IN ($office_id) AND (page_id IS NULL OR page_id IN ('','0')) ORDER BY id", ARRAY_A);
if($agents){
global $realty; $helper;    
    foreach($agents as $agent){
        $agent_name = trim($agent['firstname']).trim($agent['lastname']);
        $agent_suburb = $agent['firstname'];
        $description = $agent['description'];
        $group = strtolower($agent['group']);

        if ($group=='property manager') {
            $agent_page_title_seo = ucfirst(trim($agent['firstname']))." ".ucfirst(trim($agent['lastname']))." Property Manager Position Realty";
            $agent_page_desc = "Property Manager";
            $agent_page_keyword_seo = "Property Manager, Property Agent, Estate Agent, Property Rentals, Property Leasing, Real Estate Rentals, Real Estate Leasing, Lease my property, rent my property";
        } else {
            $agent_page_title_seo = ucfirst(trim($agent['firstname']))." ".ucfirst(trim($agent['lastname']))." Real Estate Agent Position Realty";
            $agent_page_desc = "Real Estate Agent";
            $agent_page_keyword_seo = "Real Estate Agent, estate agent, property agent, property sales, real estate sales, sell my property, sell my house";
        }
        
        $agent_page_desc_seo = $agent_page_desc." ".$agent_suburb." ".$description;
var_dump($agent_page_desc_seo);
        $agent_page_title = ucfirst(trim($agent['firstname']))." ".ucfirst(trim($agent['lastname']));

        $post_content = "[realty_plugin user_id=".$agent['id']." template=team]";
        $page_id      = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => 'page', 'post_title'=>$agent_page_title, 'post_name'=>$agent_name, 'post_content'=>$post_content));

        $wpdb->query("insert wp_postmeta (post_id, meta_key, meta_value) values ('".intval($page_id)."', '".$agent_page_desc_seo."','".$agent_page_keyword_seo."') ");
        die();
        // $wpdb->query("update users set page_id='".intval($page_id)."' where id=".intval($agent['id']));
        // $wpdb->query("insert wp_postmeta set post_id='".intval($page_id)."', meta_key='_aioseop_description', meta_value='". htmlspecialchars($realty->helper->_substr($agent_page_desc_seo." ".$agent_suburb." ".$description, 150)) ."'");
        // $wpdb->query("insert wp_postmeta set post_id='".intval($page_id)."', meta_key='_aioseop_title', meta_value='agent_page_title_seo'");
        // $wpdb->query("insert wp_postmeta set post_id='".intval($page_id)."', meta_key='_aioseop_keywords', meta_value='".$agent_page_keyword_seo."'");        
    }
}

if (empty($user)): //Display a list with all the users for this office 
    ?>
    <?php /*
	<select onchange="window.location = this.value">
		<option value="">City</option>
		<option value="<?php echo get_option('siteurl') . '/our-people/?office=13607'; ?>"<?php if ($_GET['office'] == '13607' ) echo " selected='selected'"; ?>>Townsville</option>
		<option value="<?php echo get_option('siteurl') . '/our-people/?office=13952'; ?>"<?php if ($_GET['office'] == '13952') echo " selected='selected'"; ?>>Mackay</option>
        <option value="<?php echo get_option('siteurl') . '/our-people/?office=14366'; ?>"<?php if ($_GET['office'] == '14366') echo " selected='selected'"; ?>>Brisbane</option>
	</select> */ ?>
    <div id="agents_list">

        <?php
	// if (isset($_GET['test']) && $_GET['test'] == 1) {		
		// if (isset($_GET['office']) && !empty($_GET['office'])) {
			// $office_id = $_GET['office'];
			// $office = 'office_id IN ('.$office_id.')';
		// }
		// else $office = 'office_id IN (13607)';
        // $users = $this->users($office, '', '', $group);
	// }
	// else  $users = $this->users('office_id = 13607', '', '', $group);
		$office = '';
		if (isset($_GET['office']) && !empty($_GET['office'])) {
			$office_id = $_GET['office'];
			$office = 'office_id IN ('.$office_id.')';
		}
        $users = $this->users($office, '', '', $group);
        $count = 0;
        $user_print = array();

        foreach ($users as $user):
            if (is_array($user))
            {
                if (key_exists('display', $user))
                {
                    if ($user['display'] != '1' || !is_array($user))
                        continue;
                } else
                {
                    if (!is_array($user))
                        continue;
                }
            } else
            {
                continue;
            }

            if ( in_array( $user['name'], $user_print ) ) {
                continue;
            }
            $user_print[$count] = $user['name'];

            if ($count % 3 == 0)
            {
                $r = true;
                echo '<div class="agent-row">';
            }            
            // var_dump($user['url']);
            ?>
            <div class="agent">
                <div class="agent-image left">
                    <?php if ($this->settings['general_settings']['display_team_member_images_as_landscape_on_the_team_member_results_page'] == '1'): 
			?>
                        <a class="agent-frame" href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo_landscape']; ?>"/></a>
                    <?php else:  ?>
                        <a class="agent-frame" href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><img class="myimage" alt="<?php echo $user['name']; ?>" src="<?php echo $user['big_photo']; ?>"/></a>
                <?php endif; ?>
                </div>
        <?php /* if($this->settings['general_settings']['display_team_member_images_as_landscape_on_the_team_member_results_page']=='1'):?><div class="clearer"></div><?php endif; */ ?>
                <div class="agent-details left">
                    <ul>
                        <li class="agent_name"><a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><?php echo $user['name']; ?></a></li>
                        <?php // <li class="agent_detail"><span><?php echo $user['position_for_display']; </span></li> ?> 
                        <?php 
                                if (!empty($user['business_phone']) and empty($user['mobile']))
                                {
                                    ?><li class="agent_phone"><strong>P</strong><span><?php echo $user['business_phone']; ?></span></li><?php } ?>
                                <?php
                            if (!empty($user['mobile']))
                            {
                                ?><li class="agent_mobile"><strong>M</strong><span><?php echo $user['mobile']; ?></span></li><?php } ?>
                            <?php /*
                            if (!empty($user['fax']))
                            {
                                ?><li class="agent_fax"><strong>F</strong><span><?php echo $user['fax']; ?></span></li><?php } ?>

                        <?php
                        if (!empty($user['facebook_username']) || !empty($user['twitter_username']) || !empty($user['linkedin_username']))
                        {
                            ?>
                            <li class="socials">
                                <?php if (!empty($user['facebook_username'])): ?><a class="network facebook" href="http://www.facebook.com/<?php echo $user['facebook_username']; ?>" title="Follow <?php echo $user['name']; ?> on Facebook" target="_blank"></a><?php endif; ?>
            <?php if (!empty($user['twitter_username'])): ?><a class="network twitter" href="http://twitter.com/<?php echo $user['twitter_username'] ?>" title="Follow <?php echo $user['name']; ?> on Twitter" target="_blank"></a><?php endif; ?>
                            <?php if (!empty($user['linkedin_username'])): ?><a class="network linkedin" href="<?php echo (strpos($user['linkedin_username'], 'http') !== false) ? $user['linkedin_username'] : 'http://www.linkedin.com/in/' . $user['linkedin_username']; ?>"  title="Follow <?php echo $user['name']; ?> on Linkedin" target="_blank"></a><?php endif; ?>
                                <div class="clear"></div>
                            </li>
                    <?php } */?>

                    </ul>
        <?php /* <p class="agent_description"><span><?php echo $helper->_substr($user['description'],$this->settings['general_settings']['team_description_chars']); ?> &hellip; <a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile">View more &raquo;</a></span></p> */ ?>
                </div>
            
            </div>
            <?php
            if ($count % 3 == 2)
            {
                $r = false;
                echo '</div>';
            }
            $count++;
        endforeach;
        if ($r == true)
        {
            echo '<div class="clear"></div></div>';
        }
        ?>

    </div><!-- end #list_of_agents -->

<?php else: //Individual user page. Display the user queried     ?>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/jquery.prettyPhoto.js?v=0,01"></script>
<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php echo $this->pluginUrl.'css/prettyPhoto.css'; ?>?v=0,01" />
<script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("a[rel^='prettyPhoto']").prettyPhoto({allow_resize: true,deeplinking: false,social_tools: false, theme:'light_square'});
        });

function send_enquiry(){
    var code = jQuery("#securitycode").val();
    var comments = document.getElementById('ccomments').value;  
    var first_name = jQuery("#cfirst_name").val();
    var last_name = jQuery("#clast_name").val(); 
    var email = jQuery("#cemail").val();
    var mobile_phone = jQuery("#cmobile_phone").val();
    var home_phone = jQuery("#chome_phone").val();
    if(first_name=='First Name' || first_name=='')alert("You have entered an invalid first name");
    else if(last_name=='Last Name' || last_name=='')alert("You have entered an invalid last name");
    else if(home_phone=='Telephone' || home_phone=='')alert("You have entered an invalid telephone");
    else if(mobile_phone=='Mobile' || mobile_phone=='')alert("You have entered an invalid mobile");
    else if ((email.indexOf('@') < 0) || ((email.charAt(email.length-4) != '.') && (email.charAt(email.length-3) != '.')))alert("You have entered an invalid email address. Please try again.");
    else{
    var url    = "<?php echo $this->pluginUrl."display/pages/js/contact_agent.php?&user_id=".$user['user_id']; ?>&code="+escape( code )+"&comments="+escape( comments )+"&first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&mobile_phone="+escape( mobile_phone )+"&home_phone="+escape( home_phone );
        jQuery('#return').load(url);
        //alert('Email was successfully sent.'); 
    }   
    return false; 
}
</script>
    <div id="agent">

        <div class="agent-details">



            <div class="agent-image left">
                <?php if ($this->settings['general_settings']['display_team_member_images_as_landscape_on_the_team_page'] == '1'): ?>
                    <span class="agent-frame"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo_landscape']; ?>" /></span>
                <?php else: ?>
                    <span class="agent-frame"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['big_photo']; ?>" /></span>
            <?php endif; ?>
            </div>
			
            <div class="_agentdesc right">
            	<?php //<a class="back" href="javascript:history.go(-1)">Back</a> ?>
				<?php if (!empty($user['description'])) { ?>
                    <p class="agent_description">
                        <span>
                        
                            <?php if(!empty($user['description'])) {
								//echo $user['description'];
                            $description = explode('Comment from', $user['description']);
                            echo $description[0];
                            if (!empty($description[1])) 
                            echo "<span class='italic-description'>Comment from".$description[1]."</span>";
                            if (isset($description[2])) 
                            echo "<span class='italic-description'>Comment from".$description[2]."</span>";
                            if (isset($description[3])) 
                            echo "<span class='italic-description'>Comment from".$description[3]."</span>";
                            if (isset($description[4])) 
                            echo "<span class='italic-description'>Comment from".$description[4]."</span>";
                            if (isset($description[5])) 
                            echo "<span class='italic-description'>Comment from".$description[5]."</span>";

                            }
							
							 //echo $user['description']; ?>
                        </span>
                    </p>
                <?php } ?>
                
            <ul class="agent-info">
                <?php
                if (!empty($property_agent_company_name))
                {
                    ?><li class="agent_company"><span><?php echo $property_agent_company_name; ?></span></li><?php } ?>
                <?php
                        if (!empty($user['business_phone']))
                        {
                            ?><li class="agent_phone"><i class="fa fa-phone-square"></i> <?php echo $user['business_phone']; ?></li><?php } ?>
                        <?php
                    if (!empty($user['mobile']))
                    {
                        ?><li class="agent_mobile"><i class="fa fa-phone-square"></i> <?php echo $user['mobile']; ?></li><?php } ?>
                    <?php
                    if (!empty($user['fax']))
                    {
                        ?><li class="agent_fax"><i class="fa fa-fax"></i> <?php echo $user['fax']; ?></li><?php } ?>
                    <?php
                    if (!empty($user['suburb']))
                    {
                        ?><li class="agent_fax"><strong>Suburbs:</strong><span>&nbsp;<?php echo $user['suburb']; ?></span></li><?php } ?>
    <?php if (!empty($user['vcard_path'])): ?><li class="agent_vcard"><span>Download Vcard: <a href="<?php echo $user['vcard_path']; ?>" target="_newtab"><?php echo ($user['vcard_title'] == '') ? 'No Title' : $user['vcard_title']; ?></a></span></li><?php endif; ?>
    <?php /* if(!empty($user['video_url'])): ?><li class="agent_video"><a href="<?php echo $user['video_url']; ?>" target="_newtab">Video</a></span></li><?php endif; */ ?>

            </ul>

            <div class="socials">
                    <a class="email" href="<?php echo $this->pluginUrl . 'display/elements/lightbox_contact_agent.php?popup=1&user_id=' . $user['user_id']; ?>&ajax=true&width=480&height=520" rel="prettyPhoto"><i class="fa fa-envelope"></i> <?php echo $user['email']; ?></a>
                <?php
                if (!empty($user['facebook_username']) || !empty($user['twitter_username']) || !empty($user['linkedin_username']))
                {
                    ?>
                    <?php if (!empty($user['facebook_username'])): ?><a class="network facebook" href="http://www.facebook.com/<?php echo $user['facebook_username']; ?>" title="Follow <?php echo $user['name']; ?> on Facebook" target="_blank"></a><?php endif; ?>
                    <?php if (!empty($user['twitter_username'])): ?><a class="network twitter" href="http://twitter.com/<?php echo $user['twitter_username'] ?>" title="Follow <?php echo $user['name']; ?> on Twitter" target="_blank"></a><?php endif; ?>
        <?php if (!empty($user['linkedin_username'])): ?><a class="network linkedin" href="<?php echo (strpos($user['linkedin_username'], 'http') !== false) ? $user['linkedin_username'] : 'http://www.linkedin.com/in/' . $user['linkedin_username']; ?>"  title="Follow <?php echo $user['name']; ?> on Linkedin" target="_blank"></a><?php endif; ?>
    <?php } ?>
                <div class="clear"></div>
            </div>                
                
			</div>
            <div class="clear"></div>

        </div>

        <?php
        wp_print_scripts('jquery-ui-core');
        wp_print_scripts('jquery-ui-tabs');
        ?>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery("#tabbed_listings").tabs();
            });
        </script>

        <div id="tabbed_listings">
            <ul class="agent-tabs">
                <?php if ($user['listings']["results_count"] > 0): ?>
                    <li class="listings"><a title="Go to My Current Listings" href="#listings">My Current Listings</a></li>
                <?php endif; ?>
                <?php if (!empty($user['posts'])): ?>
                    <li class="posts"><a href="#posts" title="My Post Articles">My Post Articles</a></li>
                <?php endif; ?>
                <?php if ($user['sold_listings']["results_count"] > 0): ?>
                    <li class="sold_team_listings"><a title="Go to My Recent Sold Listings Stories" href="#sold_team_listings">My Recent Sold Listings</a></li>
                <?php endif; ?>
                <?php if (!empty($user['testimonials'])): ?>
                    <li class="testimonial_team"><a title="Go to My Testimonials" href="#testimonial_team">Testimonials</a></li>
                <?php endif; ?>
                <?php if (!empty($user['video_url'])): ?>
                    <li class="video_url_team"><a title="Go to My Video" href="#video_url_team">Video</a></li>
                <?php endif; ?>
                <?php if (!empty($user['video_embed'])): ?>
                    <li class="video_team"><a title="Go to My Video" href="#video_team">Video</a></li>
                <?php endif; ?>
                <?php if (!empty($user['audio'])): ?>
                    <li class="audio_team"><a title="Go to My Audio" href="#audio_team">Audio</a></li>
            <?php endif; ?>
            </ul>

                <?php if ($user['listings']["results_count"] > 0): ?>

                <div id="listings">
                    <?php //echo $user['listings']['pagination_above']; ?>
                    <?php $this->element('list', $user['listings']); ?>
                    <?php if (!empty($user['listings']['pagination_below'])): ?>
            <?php echo $user['listings']['pagination_below']; ?>
                <?php endif; ?>
                </div>

            <?php endif; //$listings["results_count"] > 0  ?>

                <?php if ($user['sold_listings']["results_count"] > 0): ?>

                <div id="sold_team_listings">
                    <?php //echo $user['sold_listings']['pagination_above'];    ?>
                    <?php $this->element('list', $user['sold_listings']); ?>
                    <?php if (!empty($user['sold_listings']['pagination_below'])): ?>
            <?php echo $user['sold_listings']['pagination_below']; ?>
                <?php endif; ?>
                </div>

    <?php endif; //$listings["results_count"] > 0     ?>

            <script>
                function load_team_listing(page, status, user_id) {
                    var order = jQuery('#sorter').val();
                    if (page != "")
                        var page = "&page=" + page;
                    else
                        var page = "";
                    var url = "<?php echo get_option('siteurl') ?>/wp-content/plugins/Realty/display/elements/load_team_listings.php?&status=" + status + "&user_id=" + user_id + page;
                    if (status == '2')
                        jQuery('#sold_team_listings').load(url);
                    else
                        jQuery('#listings').load(url);
                    return false;
                }
            </script>

                <?php if (!empty($user['posts'])): ?>

                <div id="posts">
                <?php $this->element('posts_loop', $user); ?>
                </div><!-- ends #posts -->

            <?php endif; ?>

    <?php if (!empty($user['testimonials'])): ?>

                <div id="testimonial_team">
                    <ul>
                        <?php foreach ($user['testimonials'] as $item): ?>
                            <li><?php echo $item['content']; ?> <strong class="comment-from">By <?php echo $item['user_name']; ?></strong></li>
        <?php endforeach; ?>
                    </ul>
                </div>

            <?php endif; ?>

    <?php if (!empty($user['video_url'])): ?>

                <div id="video_url_team">
                    <script src="<?php bloginfo('template_url'); ?>/js/AC_RunActiveContent.js" type="text/javascript"></script>
                    <script type="text/javascript">
                    AC_FL_RunContent('codebase', 'http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0', 'width', '580', 'height', '385', 'id', 'exhibition-stand', 'src', 'http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer', 'flashvars', 'file=<?php echo $user['video_url']; ?>', 'quality', 'high', 'bgcolor', '#1E1E20', 'name', 'exhibition-stand', 'pluginspage', 'http://www.macromedia.com/go/getflashplayer', 'allowscriptaccess', 'always', 'allowfullscreen', 'true', 'movie', 'http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer'); //end AC code
                    </script>
                    <noscript>
                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="350" height="270" id="exhibition-stand">
                        <param name="movie" value="http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer.swf" />
                        <param name="quality" value="high" />
                        <param name="bgcolor" value="#1E1E20" />
                        <param name="allowscriptaccess" value="always" />
                        <param name="allowfullscreen" value="true" />
                        <param name="FlashVars" value="file=<?php echo $user['video_url']; ?>" />
                        <embed src="http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer.swf" width="580" height="385" FlashVars="file=<?php echo $user['video_url']; ?>" quality="high" bgcolor="#1E1E20" name="exhibition-stand"  type="application/x-shockwave-flash"  pluginspage="http://www.macromedia.com/go/getflashplayer" allowscriptaccess="always" allowfullscreen="true" /></embed>
                    </object>
                    </noscript>
                </div>

            <?php endif; ?>

    <?php if (!empty($user['video_embed'])): ?>

                <div id="video_team" style="width:580px;height:385px;">
                    <object width="580" height="385">
                        <param name="movie" value="<?php echo str_replace("v=", "v/", str_replace("watch_popup?", "", $user['video_embed'])); ?>"></param>
                        <param name="allowFullScreen" value="true"></param>
                        <param name="allowscriptaccess" value="always"></param>
                        <embed src="<?php echo str_replace("v=", "v/", str_replace("watch_popup?", "", $user['video_embed'])); ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="580" height="385"></embed>
                    </object>
                </div>

            <?php endif; ?>

    <?php if (!empty($user['audio'])) : ?>

                <div id="audio_team">		 
                    <script type="text/javascript">
                        AudioPlayer.embed("audio_team", {soundFile: "<?php echo (strpos($user['audio'], 'http://') === false) ? "http://agentaccount.com/" . $user['audio'] : $user['audio']; ?>"});
                    </script>
                </div>

    <?php endif; ?>


        </div><!-- end #tabbed_listings-->

    </div><!-- end #agent -->

<?php
endif; //Several users or user page ?>
