<?php 
//metadata: access_level=8

if(!is_array($realty->settings['sold_and_leased']))$realty->settings['sold_and_leased'] = array();

if(empty($_GET['list']))$list='sale'; else $list=$_GET['list'];
$properties = $realty->sold(array('limit' => 5000,'exclude_id'=>'', 'list'=>$list));
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;

?>
<div class="wrap">
	<div class="tablenav">	
	<div class="actions">
	<a class="alignleft button" href="?page=Realty/manage/pages/sold_and_leased.php&list=sale" title="Sold Listings">Sold Listings</a>
	<a class="alignleft button" href="?page=Realty/manage/pages/sold_and_leased.php&list=lease" title="Sold Listings">Leased Listings</a>
	
	</div>
</div>
<div class="clear"></div>
<h2><?php echo ucfirst($_GET['list']); ?></h2> 
<table class="widefat">
<thead><tr>
	<th scope="col"><?php _e('Address') ?></th>
    <th scope="col"><?php _e('Suburb') ?></th>
	<th scope="col"><?php _e('Property Type') ?></th>
	<th scope="col"><?php _e('Bed') ?></th>
	<th scope="col"><?php _e('Bath') ?></th>
    <th scope="col"><?php _e('Car') ?></th>
    <th scope="col"><?php _e('Price') ?></th>
	<th scope="col"><?php _e('Display Price') ?></th>
    <th scope="col"><?php echo sprintf(_(' %s Date'), ucfirst($_GET['list']));  ?></th>
</tr></thead>
	<?php if(is_array($properties)) $i=0;foreach($properties as $property): if(!is_array($property)) continue; ?> 
<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>>
	<td><?php echo $property['street_address'] ?></td>
	<td><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>" target="_blank"><?php echo $property['suburb'] ?></a></td>
	<td><?php echo $property['property_type'] ?></td>
	<td><?php echo $property['bedrooms'] ?></td>
	<td><?php echo $property['bathrooms'] ?></td>
	<td><?php echo $property['carspaces'] ?></td>
	<td><?php echo $property['last_price'] ?></td>
	<td><input type="hidden" name="realty_sold['<?php echo $property['id']; ?>']" value=""><input type="checkbox" <?php if ($property['display_sold_price']) echo 'checked="checked" '; ?>name="realty_sold['<?php echo $property['id']; ?>'][display_sold_price]" value="1"></td>
	<td><?php echo date('d/m/Y', strtotime($property['date'])); ?></td>
</tr> 
 <?php $i++;endforeach; ?>
 </table> 

</div><!-- ends .wrap -->