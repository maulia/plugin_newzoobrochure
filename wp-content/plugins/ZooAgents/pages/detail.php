<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];
$type=$_GET['type'];
$office_id=intval($_GET['office_id']);

$periods=array('Per Week','Per Month','Per Quarter','Per Year');
$price_periods=array('Monthly','Weekly','Quarterly','Yearly');
$eer=array('0','0.5','1','1.5','2','2.5','3','3.5','4','4.5','5','5.5','6','6.5','7','7.5','8');
$metrics=array('Acres','Hectares','Square Feet','Square Metres','Square Yards');
$minutes=array('00','15','30','45');
$method_of_sales=array('Exclusive', 'Open', 'Conjunctional', 'Sole Agency');
$authorities=array('For Sale', 'Auction', 'Sale by Negotiation', 'Tender', 'Expression of Interest', 'Offers to Purchase');
$countries=array('Australia','Austria','Canada','France','India','Indonesia','New Zealand','South Africa','Spain','Switzerland','Thailand','Turkey','United Kingdom','United States');
$min_stays=array('1'=>'1 night', '2'=>'2 nights', '3'=>'3 nights', '4'=>'4 nights', '5'=>'5 nights', '6'=>'6 nights', '7'=>'7 nights', '8'=>'8 nights', '9'=>'9 nights', '10'=>'10 nights', '11'=>'11 nights', '12'=>'12 nights', '13'=>'13 nights', '14'=>'2 weeks', '21'=>'3 weeks', '28'=>'4 weeks', '56'=>'8 weeks',  '90'=>'3 motnhs', '180'=>'6 motnhs', '365'=>'12 motnhs');

if($type=='ShareAccomodation')$type='ResidentialLease';
$features['internal']=$wpdb->get_col("select distinct name from features where type in ('InternalFeature') and property_type in ('$type') order by name");
$features['external']=$wpdb->get_col("select distinct name from features where type in ('ExternalFeature') and property_type in ('$type') order by name");
$features['security']=$wpdb->get_col("select distinct name from features where type in ('SecurityFeature') and property_type in ('$type') order by name");
$features['general']=$wpdb->get_col("select distinct name from features where type in ('GeneralFeature') and property_type in ('$type') order by name");
$features['location']=$wpdb->get_col("select distinct name from features where type in ('LocationFeature') and property_type in ('$type') order by name");
$features['lifestyle']=$wpdb->get_col("select distinct name from features where type in ('LifestyleFeature') and property_type in ('$type') order by name");
$features['your_features']=$wpdb->get_col("select distinct name from features where type in ('UserDefinedFeature') and property_type in ('$type')  and office_id in($office_id) order by name");

$property=$wpdb->get_results("select * from properties where id = $id $extra_condition", ARRAY_A);
$property=$property[0];

if(empty($property))return print "Listing ID $id is not exist";

$property_meta=$wpdb->get_results("select * from property_meta where property_id = $id", ARRAY_A);
foreach($property_meta as $item){
	$property[$item['meta_key']]=$item['meta_value'];
}

$property_features=$helper->get_col("select distinct feature from property_features where property_id in ($id)");
$property['features']=$property_features;

$users=$helper->get_results("select CONCAT(firstname, ' ', lastname) as name, id from users where office_id in($office_id) $extra_condition order by firstname, lastname", ARRAY_A);

include('menu_listings.php');

if(isset($_POST['submit'])){
	foreach($_POST as $key=>$value){
		if(!is_array($value)){
		if(trim($value)=='' && $type!='Commercial'){
			switch($key){
				case 'country':case 'state':case 'suburb':case 'street_number':case 'street':case 'property_type':case 'headline':case 'description':case 'price':case 'user_id':
					switch($key){
						 case 'user_id':$return.="Please select Primary Contact.<br/>";break; 
						 default: $return.=$helper->humanize($key)." can not be empty.<br/>"; break;
					}
				break;
			}
		}
		if(trim($value)=='' && $type=='Commercial'){
			switch($key){
				case 'country':case 'state':case 'suburb':case 'street_number':case 'street':case 'property_type':case 'headline':case 'description':case 'deal_type':case 'method_of_sale':case 'authority':case 'user_id':
					switch($key){
						 case 'user_id':$return.="Please select Primary Contact.<br/>";break;
						 case 'deal_type':$return.="Please select Commercial Type ( Sale/Lease/Both ).<br/>";break;						 
						 case 'method_of_sale':$return.="Please select Exclusivity.<br/>";break;						 
						 case 'authority':$return.="Please select Authority.<br/>";break;						 
						 default: $return.=$helper->humanize($key)." can not be empty.<br/>"; break;
					}
				break;
			}
		}
		}
	}
	
	if($_POST['property_type']!='Land' && $_POST['type']=='ResidentialSale' && empty($_POST['bedrooms']))$return.="<em>Bedrooms</em> can not be empty.<br/>";
	if($_POST['property_type']!='Land' && $_POST['type']=='ResidentialSale' && empty($_POST['bathrooms']))$return.="<em>Bathrooms</em> can not be empty.<br/>";
	if(($_POST['property_type']=='Land' || $_POST['property_type']=='Farmland' ) && empty($_POST['land_area']))$return.="<em>Land area</em> can not be empty.<br/>";
	if($_POST['property_type']!='Land' && $_POST['property_type']!='Farmland' &&  $_POST['type']=='Commercial' && empty($_POST['floor_area']))$return.="<em>Floor area</em> can not be empty.<br/>";
	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else{
		if($_POST['half_bedroom']==1)$_POST['bedrooms']=$_POST['bedrooms']+0.5;
		if($_POST['half_bathrooms']==1)$_POST['bathrooms']=$_POST['bathrooms']+0.5;
		if($_POST['type']!='Commercial')$_POST['deal_type']='';
		$_POST['updated_at']=date("Y-m-d h:i:s");
		if(!empty($_POST['auction_date']))$_POST['auction_date']=date("Y-m-d", strtotime($_POST['auction_date']));
		if(!empty($_POST['lease_end']))$_POST['lease_end']=date("Y-m-d", strtotime($_POST['lease_end']));
		if(!empty($_POST['date_available']))$_POST['date_available']=date("Y-m-d", strtotime($_POST['date_available']));

		if($this->update_form( "properties", $_POST )){
		
			// meta
			$query="insert into property_meta (property_id, meta_key, meta_value) values";
			foreach($_POST as $key=>$value){
				if(trim($value)!=''){
					switch($key){
						case 'building_name':case 'year_built':case 'energy_efficiency_rating':case 'land_area_metric':case 'floor_area_metric':case 'office_area_metric':case 'warehouse_area_metric':case 'retail_area_metric':case 'other_area_metric':case 'all_the_building':case 'all_the_floor':case 'zoning':case 'carport_spaces':case 'parking':case 'occupancy':case 'property_url':case 'virtual_tour':case 'ext_link_1':case 'ext_link_2':case 'price_include_tax':case 'return_percent':case 'current_rent':case 'outgoings_paid_by_tenant':case 'annual_outgoings':case 'outgoings_paid_by':case 'lease_end':case 'lease_further_option':case 'tax_rate':case 'tax_rate_period':case 'water_rate':case 'water_rate_period':case 'condo_strata_fee':case 'condo_strata_fee_period':case 'estimate_rental_return_period':case 'estimate_rental_return':case 'number_of_floors':case 'price_period':case 'mid_season_period':case 'high_season_period':case 'peak_season_period':case 'bond':case 'cleaning_fee':case 'unavailable_date':
							$query.="('".$id."','$key','".mysql_escape_string($value)."'),";
						break;
					}
				}
			}
			$query=substr($query,0,-1);
			$wpdb->query("delete from property_meta where property_id=$id");
			$wpdb->query($query);
			
			
			// features
			if(!empty($_POST['features'])){
				$feature_query="insert into property_features values";
				foreach($_POST['features'] as $value)$feature_query.="('','$id','".mysql_escape_string($value)."'),";			
				$feature_query=substr($feature_query,0,-1);
				$wpdb->query("delete from property_features where property_id=$id");
				$wpdb->query($feature_query);	
			}
			
			//rental season
			if(!empty($_POST['rental_season'])){
				$rental_query="insert into rental_seasons values";
				$i=0;
				while($i<count($_POST['rental_season'])){
					$start_date='';$end_date='';$minimum_stay='';
					$season=$_POST['rental_season'][$i]['season'];
					$position=$_POST['rental_season'][$i+1]['position'];
					if(!empty($_POST['rental_season'][$i+2]['start_date']))$start_date=date("Y-m-d", strtotime($_POST['rental_season'][$i+2]['start_date']));
					if(!empty($_POST['rental_season'][$i+3]['end_date']))$end_date=date("Y-m-d", strtotime($_POST['rental_season'][$i+3]['end_date']));
					$minimum_stay=$_POST['rental_season'][$i+4]['minimum_stay'];
					if(!empty($start_date) || !empty($end_date) || !empty($minimum_stay)){
						$rental_query.="('','$season','$start_date','$end_date','$minimum_stay','$id','$position'),";	
					}
					$i=$i+5;
				} 		
				$rental_query=substr($rental_query,0,-1);
				$wpdb->query("delete from rental_seasons where property_id=$id");
				$wpdb->query($rental_query);	
			}
			?>
            <script type="text/javascript">
                location.href = "<?php echo $realty->siteUrl . "dashboard/media/?id=$id&office_id=$office_id"; ?>";
            </script>
            <?php
		}
		else echo "<div id='return' class='red'>Listing could not be updated. please try again later</div>";
	}
	$property=$_POST;	
}

if($type=='HolidayLease'){
	$rental_seasons=$wpdb->get_results("select season, start_date, end_date, minimum_stay, position from rental_seasons where property_id in ($id) order by (season='normal') desc, (season='mid') desc, (season='high') desc, (season='peak') desc, position", ARRAY_A);
	if(!empty($rental_seasons)){
		$normal_count=0;
		$mid_count=0;
		$high_count=0;
		$peak_count=0;
		foreach($rental_seasons as $item){
			extract($item);
			if($season=='normal'){
				$normal_season['start_date']=$start_date;
				$normal_season['end_date']=$end_date;
				$normal_season['minimum_stay']=$minimum_stay;
				$normal_season['position']=$position;
				$normal_count++;
			}
			else if($season=='mid'){
				$mid_season[$mid_count]['start_date']=$start_date;
				$mid_season[$mid_count]['end_date']=$end_date;
				$mid_season[$mid_count]['minimum_stay']=$minimum_stay;
				$mid_season[$mid_count]['position']=$position;
				$mid_count++;
			}
			else if($season=='high'){
				$high_season[$high_count]['start_date']=$start_date;
				$high_season[$high_count]['end_date']=$end_date;
				$high_season[$high_count]['position']=$position;
				$high_count++;
			}
			else if($season=='peak'){
				$peak_season[$peak_count]['start_date']=$start_date;
				$peak_season[$peak_count]['end_date']=$end_date;
				$peak_season[$peak_count]['position']=$position;
				$peak_count++;
			}
		}	
	}
}

include("properties/$type.php");
?>

<script src="<?php echo $this->pluginUrl; ?>js/jquery-min.js" type="text/javascript"></script>
<script src="<?php echo $this->pluginUrl; ?>js/base_datepicker.js" type="text/javascript"></script>
<?php if($type=='HolidayLease'){ ?>
<script src="<?php echo $this->pluginUrl; ?>js/application.js" type="text/javascript"></script>
<script src="<?php echo $this->pluginUrl; ?>js/date.js" type="text/javascript"></script>
<script src="<?php echo $this->pluginUrl; ?>js/jquery.datePicker-2.1.2.js" type="text/javascript"></script>
<script src="<?php echo $this->pluginUrl; ?>js/datemonth.js" type="text/javascript"></script>
<?php }else{ ?>
<script src="<?php echo $this->pluginUrl; ?>js/timepicker.js" type="text/javascript"></script>
<?php } ?>
<script type="text/javascript">
var url_auto ="<?php echo $this->pluginUrl; ?>js/auto_address_complete.php";

jQuery(document).ready(function () {
	jQuery('#suburb').keyup(function() { 
	   document.getElementById("town_village").value='';
	   document.getElementById("state").value='';
	   document.getElementById("postcode").value='';
	   document.getElementById("town_village_span").innerHTML='';
	   document.getElementById("state_span").innerHTML='';
	   document.getElementById("postcode_span").innerHTML='';
	});	
	jQuery('#country').change(function() { 
	   document.getElementById("suburb").value='';
	   document.getElementById("town_village").value='';
	   document.getElementById("state").value='';
	   document.getElementById("postcode").value='';
	   document.getElementById("town_village_span").innerHTML='';
	   document.getElementById("state_span").innerHTML='';
	   document.getElementById("postcode_span").innerHTML='';
	});		
	<?php if($type=='HolidayLease'){ ?>
	jQuery(".datepicker").datepicker({      dateFormat:'mm/dd/yy'    });
    jQuery(".datepicker").change(function(){
      detectDateCollision(this);
      higlight_in_range();
    });

    jQuery('.inline').datePickerMultiMonth(
    {
      numMonths: 3,
      inline: true,
      selectMultiple:true

    }).bind(
    'dpMonthChanged',
    function(event, displayedMonth, displayedYear)
    {
      // higlight_in_range();
      // uncomment if you have firebug and want to confirm this works as expected...
      // console.log('dpMonthChanged', arguments);
    }
  ).bind(
    'dateSelected',
    function(event, date, $td, status)
    {
      // uncomment if you have firebug and want to confirm this works as expected...
      //console.log('dateSelected', arguments);

    }
  );
    higlight_in_range();
	<?php }else{ ?>
	jQuery('#date_available').datepicker({dateFormat:'dd-mm-yy'});    
	jQuery('#lease_end').datepicker({dateFormat:'dd-mm-yy'});    
	jQuery('#auction_date').datepicker({dateFormat:'dd-mm-yy'});    
	jQuery("#auction_time").timePicker({step: 15});	
	<?php } ?>
});	


/* form ato complete */
var delay = (function() {
var timer = 0;

return function(callback, ms) {
	clearTimeout(timer);
	timer = setTimeout(callback, ms);
	};
})();

function reloads_addres(inputString) {
	delay(function() {
		lookup_address (inputString) ;
	}, 250);
};

function lookup_address(inputString) {
	if(inputString.length == 0) {
		jQuery('#suggestionsBoxSuburb').hide();
	} else {
		jQuery.post(url_auto, {queryString: inputString, country: jQuery('#country').val()}, function(data){
			if(data.length >0) {
				jQuery('#suggestionsBoxSuburb').show();
				jQuery('#autoSuggestionsListSuburb').html(data);
			}
		});
	}
}

function fill_address(suburb, town_village, state, postcode) {
	jQuery('#suburb').val(suburb);
	jQuery('#town_village').val(town_village);
	jQuery('#town_village_span').html(town_village);
	jQuery('#state').val(state);
	jQuery('#state_span').html(state);
	jQuery('#postcode').val(postcode);
	jQuery('#postcode_span').html(postcode);
	setTimeout("jQuery('#suggestionsBoxSuburb').hide();", 200);
}

function change_area(propery_type){
	if(propery_type=='Farmland' || propery_type=='Land'){
		document.getElementById('land_area_field').innerHTML='<strong>Land Area:<span class="red"> *</span></strong>';
		<?php if($type=='Commercial'){ ?>document.getElementById('floor_area_field').innerHTML='Floor Area:';<?php } ?>
	}
	else {
		document.getElementById('land_area_field').innerHTML='Land Area:';
		<?php if($type=='Commercial'){ ?>document.getElementById('floor_area_field').innerHTML='<strong>Floor Area:<span class="red"> *</span></strong>';<?php } ?>
	}
}

function change_rooms(propery_type){
	if(propery_type=='Land'){
		document.getElementById('bedrooms_field').innerHTML='Bedrooms';
		document.getElementById('bathrooms_field').innerHTML='Bathrooms';
	}
	else {
		document.getElementById('bedrooms_field').innerHTML='<strong>Bedrooms:<span class="red"> *</span></strong>';
		document.getElementById('bathrooms_field').innerHTML='<strong>Bathrooms:<span class="red"> *</span></strong>';
	}
}

function change_category(property_type){
	jQuery('#category').load("<?php echo $this->pluginUrl; ?>js/reload_category.php?property_type="+escape(property_type));
}

/* map */
/* map */
function submit_geocode(){
	var geocoder = new google.maps.Geocoder();
	var street_number=jQuery('#street_number').val();
	var street=jQuery('#street').val();
	var suburb=jQuery('#suburb').val();
	var state=jQuery('#state').val();
	var postcode=jQuery('#postcode').val();				
	var address = "";

	if(street_number!='')address=street_number+" ";
	if(street!='')address=address+street+" ";
	if(suburb!='')address=address+suburb+" ";
	if(state!='')address=address+state+" ";
	if(postcode!='')address=address+postcode+" ";
	address=address.substr(0,address.length-1); 

	geocoder.geocode( { 'address': address}, function(results, status) {

	if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();
		document.getElementById('latitude').value=latitude;
		document.getElementById('latitude_span').innerHTML=latitude;
		document.getElementById('longitude').value=longitude;
		document.getElementById('longitude_span').innerHTML=longitude;
		load();
	}
	else 
	{
		alert('cannot allocate this address.');
	}
		
	}); 
}
var myLatLng;
function load() {
	var latitude = jQuery('#latitude').val();
	var longitude = jQuery('#longitude').val();
	myLatLng = new google.maps.LatLng(latitude, longitude);
	var myOptions = {
		zoom: 14,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	var marker = new google.maps.Marker({draggable:true,position: myLatLng, map: map});
	google.maps.event.addListener(marker, 'dragend', function() 
	{
		updateMarker(marker.getPosition());
	});

}

function updateMarker(pos){
   geocoder = new google.maps.Geocoder();
   geocoder.geocode
	({
		latLng: pos
	}, 
		function(results, status) 
		{
			if (status == google.maps.GeocoderStatus.OK) 
			{
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				document.getElementById('latitude').value = latitude;
				document.getElementById('latitude_span').innerHTML = latitude;
				document.getElementById('longitude').value = longitude;
				document.getElementById('longitude_span').innerHTML = longitude;
			} 
			else 
			{
				//alert('Cannot determine address at this location.');
				alert('cannot allocate this address.');
			}
		}
	);
}

</script>