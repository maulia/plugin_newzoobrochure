<?php 
require_once('../../../../../wp-load.php'); 
if (get_option('spam_filter')=='1')require_once('cryptographp.php');
global $wpdb;
if(isset($_POST['submit'])):
	if( isset( $_POST['securitycode'] ) ){
		$code = $_POST['securitycode'];
		if( !chk_crypt( $code ) )$return= "<p>Wrong spam code.</p>";
	}
	if ( !$helper->isValidEmail($_POST['message']['email']) )$return =  '<p>The e-mail "'.$_POST['message']['email'].'" seems to be invalid. Please correct it and try again.</p>';
	if ( isset($_POST['message']['from']) and !$helper->isValidEmail($_POST['message']['from']) )$return =  '<p>The e-mail "'.$_POST['message']['from'].'" seems to be invalid. Please correct it and try again.</p>';
	
	if(empty($return)): //If no error was detected
		$property = $realty->property($_POST['id']);
		$subject = "Details for property ID: " . $property['id'];		
		$extra_message = "<h4>$subject</h4>" . $_POST['message']['message'];
		if( isset($_POST['message']['from']))$extra_message .= "<h5>".$_POST['first_name']." ".$_POST['last_name']." of ".$_POST['message']['from']." has forwarded you the details for property ID: ".$property['id']."<h5>";
		$to = $_POST['message']['email'];
		ob_start();
		$realty->element('email_property_body', array('extra_message'=>$extra_message));		
		$message = ob_get_clean();
					
		if($_POST['keep_informed']=='1' && class_exists('subscriptions_manager')){
			$subscriber_id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_POST['message']['email'])."'");		
			$date=date("Y-m-d H:i:s");
			if(!$subscriber_id){
				$wpdb->insert('subscriptions_manager',
					array(						
						'first_name' => $_POST['first_name'],
						'last_name' => $_POST['last_name'],
						'email' => $_POST['message']['email'],						
						'referrer' => $_POST['referrer'],
						'comments' => $_POST['message']['message'],
						'property_id' => $_POST['property_id'],
						'keep_informed' => $_POST['keep_informed'],
						'created_at' => $date,
						'updated_at' => $date
						),
					array( 
						'%s', 
						'%s', 
						'%s',						 
						'%s',
						'%s',
						'%d',	
						'%d', 
						'%s', 
						'%s' 
					)
				);
			}else{
				$wpdb->update('subscriptions_manager',
					array(						
						'first_name' => $_POST['first_name'],
						'last_name' => $_POST['last_name'],		
						'referrer' => $_POST['referrer'],
						'comments' => $_POST['message']['message'],
						'property_id' => $_POST['property_id'],
						'keep_informed' => $_POST['keep_informed'],
						'updated_at' => $date
						),
					array( 'subscriber_id' => $subscriber_id), 
					array( 
						'%s', 
						'%s', 			 
						'%s',
						'%s',
						'%d',	
						'%d', 
						'%s' 
					), 
					array( '%d' ) 
				);
			}
			global $subscriptions_manager;		
			$list_alerts = $subscriptions_manager->get_site_alerts();
			$all_alerts = $subscriptions_manager->get_site_alerts(true);
			$news_alert=array();
			$news_alert=array_diff($list_alerts, $all_alerts);
			$i=0;
			if(!empty($news_alert)){
				$id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_POST['message']['email'])."'");
				$wpdb->query("DELETE FROM `site_alerts` WHERE `subscriber_id`=" . intval($id));
				foreach($news_alert as $key=>$item_value):
					$_REQUEST['alert'][$i]=$key;$i++;
				endforeach;		
				foreach( $_REQUEST['alert'] as $alert):
					if(is_array($_REQUEST['query_string'][$alert]))
						$query_string = str_replace('%2C', ',', http_build_query($_REQUEST['query_string'][$alert]));
						$alerts_sql .="(" . $id . ", '$alert', '". $query_string . "'), ";
						$query_string = '';
				endforeach;
				$alerts_sql = substr($alerts_sql,0,-2); //Remove the last comma
				$wpdb->query("INSERT INTO `site_alerts` (`subscriber_id`, `alert`, `query_string`) VALUES $alerts_sql");
			}
		}		
		
		if($helper->email($to, $subject, $message,'','',false))$return =  "The property details have been sent.";
		unset($_POST['message']);
	endif;//empty($return), no error detected
endif;//isset($_POST['submit']))

?>
<html>
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" title="Ginga" />
<title><?php bloginfo('name'); ?> - E-mail Property</title>
</head>
<body id="email_subscribe">

<div id="main_body">
	<div id="formpart">
            
        <h2>Email Property</h2>
        <form method="post" action="" name="">
        	<?php if(!empty($return)) { ?><div id="return"><?php echo $return; ?></div><?php } ?>
            <ol class="cf-ol">
                <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                
                <li><label><span>First Name:</span></label><input type="text" class="textbox" name="first_name" value="<?php echo $_POST['first_name']; ?>" /></li>
                
                <li><label><span>Last Name:</span></label><input type="text" class="textbox" name="last_name" value="<?php echo $_POST['last_name']; ?>" /></li>
                
				<?php if(!isset($_GET['to_self'])): //E-mail to self should only have one input box ?>
                <li><label><span>Your Email</span></label><input type="text" name="message[from]" class="textbox" value="<?php echo (empty($_POST['message']['from']))? "From:" : $_POST['message']['from']; ?>" onClick="if(this.value=='From:') this.value=''"></li>
                <?php endif; ?>
                
                <li><label><span>Recepient's Email:</span></label><input type="text" name="message[email]" class="textbox" value="<?php echo (empty($_POST['message']['email']))? "To:" : $_POST['message']['email']; ?>" onClick="if(this.value=='To:') this.value=''"></li>

                <li><label><span>Comments:</span></label><textarea name="message[message]" class="textarea" onClick="if(this.innerHTML=='Comments') this.innerHTML=''"><?php echo (empty($_POST['message']['message']))? "Comments" : $_POST['message']['message']; ?></textarea></li>
                
                <li>
                	<label><span>How did you find us?</span></label>
                    <select name="referrer">
                            <option value="">Please select</option>
                        <?php global $realty;
                        $all_list=$realty->all_list_referrers();foreach($all_list as $item_value): ?>
                            <option value="<?php echo $item_value['name']; ?> <?php if($_POST['referrer']==$item_value['name'])echo 'selected="selected"'; ?>"><?php echo $item_value['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
                    
                <?php if (get_option('spam_filter')=='1'):?>
                <li><label><span>Spam Code</span></label><?php display_cryptographp_self(); ?></li>
                <?php endif; ?>

                <li><label><span>Subscribe to be kept informed?</span></label><input type="checkbox" class="checkbox" name="keep_informed" value="1" <?php if($_POST['keep_informed']=='1') echo "checked";?>></li>
            </ol>
            <p class="button submit_btn"><input type="submit" name="submit" value="submit" class="btn" /></p>
        </form>
	
    </div>
</div>

</body>
</html>