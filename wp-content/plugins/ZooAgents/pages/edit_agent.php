<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;
$id=$_GET['id'];
$user=$wpdb->get_results("select * from users where id=$id", ARRAY_A);
$user=$user[0];

if(!$user)return;

$user['landscape']=$wpdb->get_var("select url from attachments where parent_id=$id and type='landscape' and description='large' and position='0'");
$user['portrait']=$wpdb->get_var("select url from attachments where parent_id=$id and type='portrait' and description='large' and position=0");

$uploaded_path=ABSPATH . 'wp-content/uploads/';
$uploaded_url=$realty->siteUrl.'wp-content/uploads/';
$logos_upload_path=$uploaded_path.'user_photos/';
$logos_upload_url=$uploaded_url.'user_photos/';

if(!@file_exists($uploaded_path."user_photos/"))@mkdir($uploaded_path."user_photos/", 0777);

$groups=array('Executive','Sales Agent','Property Manager','Admin','Industrial','Office Leasing','Office Sales','Retail','Other');

if(isset($_POST['submit'])){
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'firstname':case 'lastname':case 'email':
					 $return.=$helper->humanize($key)." can not be empty.<br/>"; break;					
				break;
			}
		}		
	}
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else{
		$_POST['landscape']=$_POST['landscape_path'];
		$_POST['portrait']=$_POST['portrait_path'];
		$userfile_name = $_FILES['portrait']['name']; 
		$userfile_tmp = $_FILES['portrait']['tmp_name']; 
		if(!empty($userfile_tmp)){
			$logo=$userfile_name;
			$logos_upload = $logos_upload_path.$_POST['id'].'/';
			//$this->remove_folder_contents($logos_upload);
			if(!$this->save_file($userfile_tmp, $logos_upload.$logo))$return.="Photo can not be saved. Please try again.<br/>";
			else{				
				$_POST['portrait']=$logos_upload_url.$_POST['id']."/$logo";
				$wpdb->query("delete from attachments where parent_id=$id and type='portrait' and description='large' and position='0'");
				$wpdb->query("insert into attachments values('','1','large','".$_POST['portrait']."','$id','portrait',NOW(),NOW(),'0','')");
			}
		}		
		$userfile_name = $_FILES['landscape']['name']; 
		$userfile_tmp = $_FILES['landscape']['tmp_name']; 
		if(!empty($userfile_tmp)){
			$logo=$userfile_name;
			$logos_upload = $logos_upload_path.$_POST['id'].'/';
			//$this->remove_folder_contents($logos_upload);
			if(!$this->save_file($userfile_tmp, $logos_upload.$logo))$return.="Photo can not be saved. Please try again.<br/>";
			else{
				$_POST['landscape']=$logos_upload_url.$_POST['id']."/$logo";
				$wpdb->query("delete from attachments where parent_id=$id and type='landscape' and description='large' and position='0'");
				$wpdb->query("insert into attachments values('','1','large','".$_POST['landscape']."','$id','landscape',NOW(),NOW(),'0','')");
			}
		}			
		
		if($_POST['landscape_flag']==1){
			$wpdb->query("delete from attachments where parent_id=$id and type='landscape' and description='large' and position='0'");
		}
		if($_POST['portrait_flag']==1){
			$wpdb->query("delete from attachments where parent_id=$id and type='portrait' and description='large' and position='0'");
		}
		
		$_POST['updated_at']=date("Y-m-d h:i:s");
		if($this->update_form( "users", $_POST ))$return.= 'Agent successfully updated';
		else $return.= 'Agent could not be updated. please try again later';
		echo "<div id='success'><p>$return</p></div>";
	}
	$user=$_POST;	
}
?>
<p class="add-agent-link"><a href="<?php echo $realty->siteUrl."dashboard/new-agent/?office_id=$office_id"; ?>">Add New Agent</a></p>
<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off" enctype="multipart/form-data" class="form-team">
    <div class="content-left-column">
	
	<div id="address_div" class="block_content">
        <h4>Profile</h4>
        <input type="hidden" name="id"  value="<?php echo $user['id']; ?>">		
        <input type="hidden" name="position"  value="<?php echo $user['position']; ?>">		
        <table>
            <tr>
                <td class="label"><strong>First Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="full" id="firstname" name="firstname"  value="<?php echo stripslashes(str_replace('\\','',$user['firstname'])); ?>">						
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Last Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="full" id="lastname" name="lastname"  value="<?php echo stripslashes(str_replace('\\','',$user['lastname'])); ?>">						
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Email<span class="red"> *</span></strong></td>
                <td>
                    <?php if($user['contributor']==1){ ?>
                    <input type="hidden" id="email" name="email" value="<?php echo $user['email']; ?>">
                    <strong><?php echo $user['email']; ?></strong>
                    <?php } else { ?>
                    <input type="text" class="full" id="email" name="email"  value="<?php echo $user['email']; ?>">
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td class="label">Phone</td>
                <td><input type="text" name="phone" class="full"  value="<?php echo $user['phone']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Mobile</td>
                <td><input type="text" name="mobile" class="full"  value="<?php echo $user['mobile']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Fax</td>
                <td><input type="text" id="fax" name="fax" class="full"  value="<?php echo $user['fax']; ?>"></td>
            </tr>
			<tr>
                <td class="label">Role</td>
                <td><input type="text" name="role" class="full"  value="<?php echo $user['role']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Group</td>
                <td>
                <select name="group" class="full">
                    <option value="">Please Select</option>
                    <?php foreach($groups as $group){ ?>
                    <option value="<?php echo $group; ?>" <?php if($group==$user['group'])echo 'selected="selected"'; ?>><?php echo $group; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr>
                <td class="label" style="vertical-align: top !important;">Description</td>
                <td>
                    <textarea name="description" rows="10" class="full"><?php echo $user['description']; ?></textarea>
                </td>
            </tr>
			<?php /*
            <tr>
                <td class="label">Suburb</td>
                <td><input type="text" id="suburb" name="suburb" class="full"  value="<?php echo $user['suburb']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Display on site?</td>
                <td><input type="checkbox" id="display" name="display" value="1" <?php if($user['display']==1)echo 'checked="checked"'; ?>></td>
            </tr>
            */ ?>
        </table>
        
        

    </div>
	
	</div>
	
	<div class="content-right-column">
		<div id="social_div" class="block_content">
			<h4>Social Networking</h4>
			<input type="hidden" name="id"  value="<?php echo $user['id']; ?>">		
			<table>				
				<tr>
					<td class="label">Facebook account</td>
					<td>www.facebook.com/ <input type="text" name="facebook_username" class="half"  value="<?php echo $user['facebook_username']; ?>"></td>
				</tr>	
				<tr>
					<td class="label">Twitter account</td>
					<td>www.twitter.com/ <input type="text" name="twitter_username" class="half"  value="<?php echo $user['twitter_username']; ?>"></td>
				</tr>	
				<tr>
					<td class="label">Linkedin account</td>
					<td>www.linkedin.com/ <input type="text" name="linkedin_username" class="half"  value="<?php echo $user['linkedin_username']; ?>"></td>
				</tr>		
			</table>			
		</div>
		<div class="clearer"></div>
		
		<div id="photo_div" class="block_content">
			<h4>Photos</h4>
			<table>				
				<tr>
					<td class="label">Portrait (100px X 125px)</td>
					<td>
						<?php if(!empty($user['portrait'])){ ?>
						<img src="<?php echo $user['portrait']; ?>" class="logo"><br/>
						<input type="hidden" value="<?php echo $user['portrait']; ?>" name="portrait_path">
						<?php } ?>
						<input type="file" name="portrait" value="">
						<?php if(!empty($user['portrait'])){ ?><br/><input type="checkbox" value="1" name="portrait_flag" /> Remove existing image<?php } ?>
					</td>
				</tr>	
				<tr>
					<td class="label">Landscape (250px X 125px)</td>
					<td>
						<?php if(!empty($user['landscape'])){ ?>
						<img src="<?php echo $user['landscape']; ?>" class="logo"><br/>
						<input type="hidden" value="<?php echo $user['landscape']; ?>" name="landscape_path">
						<?php } ?>
						<input type="file" name="landscape" value="">
						<?php if(!empty($user['landscape'])){ ?><br/><input type="checkbox" value="1" name="landscape_flag" /> Remove existing image<?php } ?>
					</td>
				</tr>	
			</table>	
			
		</div>
		<div class="clearer"></div>
		
	</div>	
	
	<div class="clearer"></div>
	<div class="block_content">
		<input type="submit" name="submit" value="Save" class="btn" id="submit_btn" >	
		<?php /*<a href="<?php echo $realty->siteUrl.'team/?u='.$user['id']; ?>" class="btn" target="_blank" id="next_btn">Preview</a>*/ ?>
	</div>
</form>