<?php
require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/wp-load.php');
$feature_settings=get_option('realty_featured_listings');
$general_settings=get_option('realty_general_settings');
$office_id=implode(",",$general_settings['office_id']);
global $helper;
if(empty($feature_settings['featured_listings']['sale']))$feature_settings['featured_listings']['sale']=array();
if(empty($feature_settings['featured_listings']['lease']))$feature_settings['featured_listings']['lease']=array();
$feature_settings['featured_listings']['sale']=array_unique($feature_settings['featured_listings']['sale']);
$feature_settings['featured_listings']['lease']=array_unique($feature_settings['featured_listings']['lease']);

if(!empty($feature_settings['featured_listings']['lease'])){
	$exist_ids=$helper->get_column("select id from properties where office_id in($office_id) and status in (1,4) and id in(".implode(',',$feature_settings['featured_listings']['lease']).")");	
	$delete_ids=array_diff($feature_settings['featured_listings']['lease'],$exist_ids);
	if(!empty($delete_ids)){
		$feature_settings['featured_listings']['lease']=array_diff($feature_settings['featured_listings']['lease'],$delete_ids);	
	}
}
if(!empty($feature_settings['featured_listings']['sale'])){
	$exist_ids=$helper->get_column("select id from properties where office_id in($office_id) and status in (1,4) and id in(".implode(',',$feature_settings['featured_listings']['sale']).")");	
	$delete_ids=array_diff($feature_settings['featured_listings']['sale'],$exist_ids);
	if(!empty($delete_ids)){
		$feature_settings['featured_listings']['sale']=array_diff($feature_settings['featured_listings']['sale'],$delete_ids);		
	}
}

if($_GET['action']=='add' || $_GET['action']=='add_sale'){
	array_push($feature_settings['featured_listings']['sale'], $_GET['id']);
	update_option('realty_featured_listings',$feature_settings);
	?>
	<a href="javascript:delFeatured('<?php echo $_GET['id']; ?>','del_sale');">- Remove from Featured Sale Listings</a>
	<?php
}
if($_GET['action']=='add_lease'){
	array_push($feature_settings['featured_listings']['lease'], $_GET['id']);
	update_option('realty_featured_listings',$feature_settings);
	?>
	<a href="javascript:delFeatured('<?php echo $_GET['id']; ?>','del_lease');">- Remove from Featured Lease Listings</a>
	<?php
}
if($_GET['action']=='del' || $_GET['action']=='del_sale'){
	foreach($feature_settings['featured_listings']['sale'] as $key=>$id){
		if($id==$_GET['id'] || empty($id))unset($feature_settings['featured_listings']['sale'][$key]);
	}
	update_option('realty_featured_listings',$feature_settings);
	?>
	<a href="javascript:addFeatured('<?php echo $_GET['id']; ?>','add_sale');">+ Mark As Featured Sale Listings</a>
	<?php	
}
if($_GET['action']=='del_lease'){
	foreach($feature_settings['featured_listings']['lease'] as $key=>$id){
		if($id==$_GET['id'] || empty($id))unset($feature_settings['featured_listings']['lease'][$key]);
	}
	update_option('realty_featured_listings',$feature_settings);
	?>
	<a href="javascript:addFeatured('<?php echo $_GET['id']; ?>','add_lease');">+ Mark As Featured Lease Listings</a>
	<?php	
}
?>