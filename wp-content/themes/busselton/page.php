<?php 
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
}
get_header();
global $realty;
$images=get_option('homepage_images');
?>

<!-- HOMEPAGE -->
<?php if (is_page('home')) { ?>
	<div class="home-top">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Top') ) : ?><?php endif; ?>
        <div class="slideshow">
		<?php if(!empty($images)){
		$photos_upload_url=get_option('siteurl').'/wp-content/uploads/slideshow/';
		$desc=get_option('blogname')." - ".get_option('blogdescription');
		foreach($images as $image){
		if(!empty($image['large'])){
		?>
		<div class="slide"><img src="<?php echo $photos_upload_url.$image['large']; ?>" alt="<?php echo $desc; ?>" title="<?php echo $desc; ?>" /></div>
		<?php } } } ?>
    </div>

        <div class="ads">
            <a href="<?php echo get_option('home'); ?>/selling/sale-appraisal/"><img src="<?php bloginfo('template_url'); ?>/img/marketAppraisal.png" /></a>
            <a href="<?php echo get_option('home'); ?>/team"><img src="<?php bloginfo('template_url'); ?>/img/ourStaff.png" /></a>
            <a href="<?php echo get_option('home'); ?>/buying"><img src="<?php bloginfo('template_url'); ?>/img/buying.png" /></a>
            <a href="<?php echo get_option('home'); ?>/renting"><img src="<?php bloginfo('template_url'); ?>/img/renting.png" /></a>
        	<div class="clear"></div>
        </div>
		<div class="clear"></div>
    </div>
   <div class="home-middle">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Middle') ) : ?><?php endif; ?>
    </div>
    <div class="home-bottom">
    	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Bottom') ) : ?><?php endif; ?>
        <div class="link-box-new">
            <a href="<?php echo get_option('home'); ?>/subscribe/" class="newsletter"><img src="<?php bloginfo('template_url'); ?>/img/home-links-newsletter.png?v=1.0" /></a>          
            <a href="<?php echo get_option('home'); ?>/contact-us/" class="contact"><img src="<?php bloginfo('template_url'); ?>/img/home-links-contact.png?v=1.0" /></a>
        </div>
        <div class="clear"></div>
    </div>

<?php } ?>
<!-- END HOMEPAGE -->

<?php if (!is_page('home')) { ?>
<!-- END CONTENT -->
<?php  get_sidebar();  } ?>

<?php if (!is_page('home')) { ?>

<!-- CONTENT -->
<div id="content"><?php } ?>

	<?php if (is_page('property')) { ?>
        <div class="page"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Page Main') ) : ?><?php endif; ?></div>
    <?php } else { ?>
	<?php if (!is_page('home')) { ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="page" id="page-<?php the_ID(); ?>">
                	
                    <?php if(is_page('teams') && $_GET['u']) { 
						global $realty;
						$user = $realty->user($_GET['u'], true); ?>
						<h1 class="page-title"><?php echo $user['name']; ?><span class="agent-title"><?php echo $user['position_for_display']; ?></span></h1>
					<?php } else { ?>
	                    <h1 class="page-title"><?php if (is_single() || is_page()) { $BodyTitle = get_post_meta($post->ID, 'BodyTitle', true); if ($BodyTitle) { echo $BodyTitle; } else { the_title(); } } ?></h1>
                    <?php } ?>
                    
                    <div class="entry"><?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?></div>
                </div>
            <?php endwhile; endif; ?>
    	<?php } ?>
	<?php } ?>
    
<?php if (!is_page('home')) { ?>
</div>
<!-- END CONTENT -->
<?php } ?>

<?php get_footer(); ?>
