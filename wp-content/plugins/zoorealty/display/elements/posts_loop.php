<?php
foreach($params['posts'] as $post) :
setup_postdata($post);

$end_link=strpos($post->post_content,"</a>")+4;
if(substr($post->post_content,0,2)=='<a')$post->post_content=substr($post->post_content,$end_link);
$end_img=strpos($post->post_content,">")+1;
if(substr($post->post_content,0,4)=='<img')$post->post_content=substr($post->post_content,$end_img);
$end_caption=strpos($post->post_content,"[/caption]")+10;
if(substr($post->post_content,0,8)=='[caption')$post->post_content=substr($post->post_content,$end_caption);
$end_caption=strpos($post->post_content,"[/caption]")+10;

if(strpos($post->post_content,'<table')!==false){
	$start=strpos($post->post_content,'<table');			
	$post->post_content=substr($post->post_content,0,$start);
}
$display_text=$helper->_substr(strip_tags($post->post_content), 165);
if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $display_text, $matches)){
	$youtube_id=$matches[0];
	$display_text='<p><object width="500" height="306"><param name="movie" value="http://www.youtube.com/v/'.$youtube_id.'"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/'.$youtube_id.'" type="application/x-shockwave-flash" width="500" height="306" allowscriptaccess="always" allowfullscreen="true"></embed></object></p>';
}
if(preg_match('#http://www.dailymotion.com/video/([A-Za-z0-9]+)#s', $display_text, $matches)){
	$id =  $matches[1];
	$display_text='<p><object width="500" height="306"><param value="http://www.dailymotion.com/swf/'.$id.'" name="movie"><param value="true" name="allowfullscreen"><param value="opaque" name="wmode"><embed width="500" height="306" wmode="opaque" allowfullscreen="true" src="http://www.dailymotion.com/swf/'.$id.'"></object></p>';
}		
if(preg_match('#http://www.dailymotion.com/embed/video/([A-Za-z0-9]+)#s', $dailymotionurl, $matches)){
	$id =  $matches[1];
	$display_text='<p><object width="500" height="306"><param value="http://www.dailymotion.com/swf/'.$id.'" name="movie"><param value="true" name="allowfullscreen"><param value="opaque" name="wmode"><embed width="500" height="306" wmode="opaque" allowfullscreen="true" src="http://www.dailymotion.com/swf/'.$id.'"></object></p>';
}	
?>
	<div class="post" id="post-<?php echo $post->ID; ?>">
<?php if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; } ?>
<h3><a href="<?php echo get_permalink($post->ID); ?>" id="post-<?php the_ID(); ?>"><?php echo $post->post_title; ?></a></h3>
		<div class="entry">
			<?php echo  $display_text;?> <a href="<?php echo get_permalink($post->ID); ?>">Continue reading &raquo;</a>
			<p class="postmetadata"><span class="postdate"><?php echo date('j.m.y',strtotime($post->post_date)); ?></span> | <span class="postauthor"><?php the_author() ?></span></p>
			<div class="clear"></div>
        </div>
	</div>
<?php endforeach; ?>