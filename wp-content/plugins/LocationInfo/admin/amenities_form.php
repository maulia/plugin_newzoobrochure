<br class="clear" />
<div class="wrap">
	<div id="poststuff">
		<div id="post-body">
		<h2><?php _e('Points of Interest'); ?></h2>
		<p>To open a point of interest, click on the arrow to the left, you can then edit or delete it.</p>
		<form enctype="multipart/form-data" method="post" action="" name="amenity_form">
			<input type="hidden" name="hidden_amenity_id" id="amenity_id" value="" /><!--for deletion purposes only-->
			<input type="hidden" name="task" value="delete_amenity" />

			<?php $location['amenities']['new'] = true; foreach($location['amenities'] as $amenity):?>
			<?php if(is_array($amenity)): ?>

			<div class="postbox closed">				
				<h3 style="cursor:pointer !important;">
					<a class="togbox">+</a> &raquo;&nbsp;&nbsp;</a><?php echo $amenity['amenity'] . ' ( ' . $amenity['type'] . ' )'; ?>
				</h3>


				<div class="inside">
					<div class="propliststuff">
					<table class="amenity_form form-table">

						<?php else: ?>
						<h2>Add New Point of Interest</h2>
						<p>To create a new point of interest simply fill in the fields below and save.</p>
						<div id="addnewdiv" class="stuffbox">
						<h3>Point of Interest Details</h3>
						<div class="inside">
						<table class="amenity_form form-table" style="width: 100%;" cellspacing="2" cellpadding="5">

						<?php endif; //empty($amenity['amenity_id']) ?>
		

						<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Point of Interest') ?></label></label></th>
							<td>
								<input type="hidden" name="amenity_id[]" value="<?php echo $amenity['amenity_id'] ?>" />
								<select name="type[]">
								<option value="">Please select one</option>
									<option value="cafe" <?php if("cafe" == $amenity['type']) echo 'selected="selected"'; ?>>Cafes & Restaurants</option>						
									<option value="landmark" <?php if("landmark" == $amenity['type']) echo 'selected="selected"'; ?>>Landmarks</option>								
									<option value="worship" <?php if("worship" == $amenity['type']) echo 'selected="selected"'; ?>>Places of Worship</option>
									<option value="school" <?php if("school" == $amenity['type']) echo 'selected="selected"'; ?>>School & Univ</option>
									<option value="shopping" <?php if("shopping" == $amenity['type']) echo 'selected="selected"'; ?>>Shops</option>
									<option value="sport" <?php if("sport" == $amenity['type']) echo 'selected="selected"'; ?>>Sporting Facilities</option>
									<?php /*<option value="link" <?php if("link" == $amenity['type']) echo 'selected="selected"'; ?>>Link</option>*/ ?>
								</select>
							</td>
						</tr>
						<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Name') ?></label></th>
							<td><input type="text" name="amenity[]" value="<?php echo $amenity['amenity'] ?>" /></td>
						</tr>
						<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Link') ?></label></th>
							<td><input type="text" name="link[]" value="<?php echo $amenity['link']; ?>" /></td>
						</tr>
						
						<?php if(is_array($amenity)): ?>
						<tr class="form-field">
							<td colspan="2">
							<a onClick="if(confirm('You are about to delete \'<?php echo $amenity['amenity']; ?>\'.\n\'OK\' to delete, \'Cancel\' to stop.' )){onclick=document.getElementById('amenity_id').value='<?php echo $amenity['amenity_id']; ?>';document.amenity_form.submit();}" class="button-secondary delete"><?php _e('Delete') ?></a>
							</td>
						</tr>
						<?php endif; ?>
			
					</table>
				</div><!-- end .propliststuff -->
		</div><!-- end .inside -->
	</div><!-- end .postbox -->
	<?php $alternate = (empty($alternate))? 'alternate':'';++$count; endforeach; ?>


	<p class="submit"><input type="submit" value="<?php _e('Save Point of Interest &raquo;') ?>" name="save_amenities" /></p>
	</form>
</div><!-- end .post-body -->
</div><!-- end .poststuff -->

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.postbox h3').click( function() { jQuery(jQuery(this).parent().get(0)).toggleClass('closed'); } );
  });
</script>