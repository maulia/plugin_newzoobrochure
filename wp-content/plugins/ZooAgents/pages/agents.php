<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_office_id=$wpdb->get_var("select extra_office_id from office where id in ($office_id)");
if(!empty($extra_office_id))$office_id.=",".$extra_office_id;

if($_GET['task']=='delete' && !empty($_GET['id'])){
	if($wpdb->query("delete from users where office_id in($office_id) and id=".$_GET['id']))echo "<div id='success'><span>User succesfully deleted.</span></div>";
}
if($_GET['x']==1){	echo "<div id='return'><span>New agent successfully created.</span></div>";}
$users=$wpdb->get_results("select * from users where office_id in($office_id) order by position", ARRAY_A);

?>
<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/table_sorter.css" type="text/css" id="" media="print, projection, screen" />
<script type="text/javascript">	
jQuery(function() {
	jQuery("#properties_table").tablesorter({     widgets: ['zebra']     });
});	
</script>	
<p class="add-agent-link"><a href="<?php echo $realty->siteUrl."dashboard/new-agent/?office_id=$office_id"; ?>">Add New Agent</a></p>
<table class="tablesorter" cellpadding="0" cellspacing="0" id="properties_table">
	<thead>
		<tr class="th">
			<th class="th_id">ID</th>
			<th class="th_name">Name</th>
			<th class="th_email">Email</th>
			<th class="th_mobile">Mobile</th>			
			<th class="th_phone">Phone</th>			
			<th class="th_edit">Edit</th>
			<th class="th_delete">Delete</th>
		</tr>
	</thead>
	<?php 
	foreach($users as $user): ?>
	<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
		<td class="td_id"><?php echo $user['id']; ?></td>
		<td class="td_name"><?php echo $user['firstname']." ". $user['lastname']; ?></td>		
		<td class="td_email"><?php echo $user['email']; ?></td>
		<td class="td_mobile"><?php echo $user['mobile']; ?></td>
		<td class="td_phone"><?php echo $user['phone']; ?></td>
		<td class="td_edit"><a href="<?php echo $realty->siteUrl."dashboard/edit-agent/?office_id=$office_id&id=".$user['id'];?>">Edit</a></td>
		<td class="td_delete">
			<a class="delete" onClick="return confirm('You are about to delete the this agent.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/agents/?office_id=$office_id&task=delete&id=".$user['id'];?>">Delete</a>
		</td>
	</tr>
	<?php endforeach; ?>
</table>