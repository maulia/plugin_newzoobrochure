<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];

$property=$wpdb->get_results("select * from properties where id = $id $extra_condition", ARRAY_A);
$property=$property[0];
if(empty($property))return print "Listing ID $id is not exist";

include('menu_listings.php');

if(isset($_POST['submit'])){
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'date':case 'start_time':case 'end_time':
					$return.=$helper->humanize($key)." can not be empty.<br/>";
				break;
			}
		}
	}
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else{		
		if(!empty($_POST['date']))$_POST['date']=date("Y-m-d", strtotime($_POST['date']));				
		if($wpdb->query("insert into opentimes values('','$id','".$_POST['date']."','".$_POST['start_time']."','".$_POST['end_time']."')"))echo "<div id='success'><span>Opentimes successfully updated</span></div>";			
		else echo "<div id='return' class='red'>Opentimes could not be saved. please try again later</div>";		
	}
}

$opentimes=$wpdb->get_results("select * from opentimes where property_id in ($id) and `date` >= curdate() order by `date` asc", ARRAY_A);
$property['opentimes']=$opentimes;
?>
<script language="JavaScript" src="<?php echo $this->pluginUrl; ?>js/base_datepicker.js"></script>
<script language="JavaScript" src="<?php echo $this->pluginUrl; ?>js/timepicker.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#opens #update_properties .next_button').appendTo('#opens .template-entry');
	});
</script>

<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div id="opentimes_div" class="add-opentimes block_content">
		<h4>Add an Open Time </h4>
		<table>
			<tr>
				<td class="label">Open Inspection Date:</td>
				<td><input type="text" class="full" id="date" name="date"  value=""></td>
			</tr>
			<tr>
				<td class="label">Commecement Time:</td>
				<td><input type="text" class="full" id="start_time" name="start_time" value=""></td>
			</tr>
			<tr>
				<td class="label">End Time:</td>
				<td><input type="text" class="full" id="end_time" name="end_time" value=""></td>
			</tr>
            <tr>
            	<td colspan="2">
                	<div class="opsave"><input type="submit" name="submit" value="Save" class="btn" id="submit_btn" ></div>
                </td>
            </tr>
		</table>
	</div>
	<div class="clearer"></div>
    
	 <div class="block_content next_button">
		<a href="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id']; ?>" class="button"  id="next_btn">Next</a>
	</div>
</form>

<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div id="opentimes_div" class="opentimes-data block_content">
		<h4>Open Times</h4>
		<input type="hidden" value="0" id="theValue" />
		<div id="myDiv">
			<?php 
			if(empty($property['opentimes']))echo "There are no Open times";
			else {
				foreach($property['opentimes'] as $times) {	?>
				<p id="open_<?php echo $times['id']; ?>">
				<?php echo date("d M y, D",strtotime($times['date']))." ".date("h:i A",strtotime($times['start_time']))." - ".date("h:i A",strtotime($times['end_time'])); ?>
				<span style="margin-left:15px;"><a onClick="return confirm('Are you sure to delete the opentimes.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="javascript:removeTimes('<?php echo $times['id']; ?>');">Remove</a></span>
				</p>
				<?php
				} 
			}
			?>
		</div>	
	</div>
</form>

<script type="text/javascript">
jQuery(document).ready(function () {
  jQuery('#date').datepicker({dateFormat:'dd-mm-yy'});    
  jQuery("#start_time").timePicker({step: 15});
  jQuery("#end_time").timePicker({step: 15});
});	

/* opens */
function addTimes(date, start_time, end_time, date_text, start_time_text, end_time_text) {
  var ni = document.getElementById('myDiv');
  var numi = document.getElementById('theValue');
  var num = (document.getElementById('theValue').value -1)+ 2;
  numi.value = num;
  var newdiv = document.createElement('div');
  var divIdName = 'my'+num+'Div';	  
  var date_name = 'opentimes['+num+'][date]';
  var start_time_name = 'opentimes['+num+'][start_time]';
  var end_time_name = 'opentimes['+num+'][end_time]';
  newdiv.setAttribute('id',divIdName);
  newdiv.innerHTML = 	'<p><input type="hidden" name="'+date_name+'" value="'+date+'">'+
						'<input type="hidden" name="'+start_time_name+'" value="'+start_time+'">'+
						'<input type="hidden" name="'+end_time_name+'" value="'+end_time+'">'+
						date_text+' '+start_time_text+' to '+end_time_text+'<span style="margin-left:15px;"><a href=\'#\' onclick=\'removePages("'+divIdName+'")\'>Remove</a></span></p>'
						;
  ni.appendChild(newdiv);
}

function removePages(divNum) {
  var d = document.getElementById('myDiv');
  var olddiv = document.getElementById(divNum);
  d.removeChild(olddiv);
}

function removeTimes( id){
	jQuery('#open_'+id).hide();
	jQuery.get("<?php echo $this->pluginUrl."js/delete_open.php?property_id=$id&id="; ?>"+id);

}
</script>