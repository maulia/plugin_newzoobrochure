<div class="wrap">
<h2><?php _e('Current Locations'); ?>&nbsp;(<a href="<?php echo $this->form_action; ?>&amp;action=add_new"><?php _e('add new'); ?></a>)</h2>
<?php
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif; ?>

<br class="clear" />

<table class="widefat">
  <thead>
  <tr>
    
    <th scope="col"><?php _e('Suburb') ?></th>
    <th scope="col"><?php _e('Population') ?></th>
	<th scope="col"><?php _e('Postcode') ?></th>
    <th scope="col"><?php _e('Council') ?></th>

	<th scope="col" colspan="3" style="text-align: center"><?php _e('Action'); ?></th>
  </tr>
  </thead>
  <tbody id="the-list">
<?php foreach($current_locations as $item): extract($item); ?>
	<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">
	<td><?php echo $suburb; ?></td>
	<td><?php echo $population; ?></td>
	<td><?php echo $postcode; ?></td>
	<td><?php echo $council; ?></td>
    <td><a class="edit" title="Edit <?php echo $suburb; ?>" href="<?php echo $this->form_action; ?>&amp;action=edit&amp;suburb_id=<?php echo $suburb_id; ?>">Edit</a></td>
    <td><a onClick="return confirm('You are about to delete the \'<?php echo $suburb; ?>\' from the list of locations.\n\'OK\' to delete, \'Cancel\' to stop.' );" class="delete" href="<?php echo $this->form_action; ?>&amp;task=delete_form&amp;suburb_id=<?php echo $suburb_id; ?>">Delete</a></td>
	</tr>
	


<?php endforeach; ?>
  </tbody>
</table>

<br class="clear" />
</div>