<?php //Configuration of number of options in the drop-down lists:			
$num_bedrooms = 10;
$num_bathrooms = 10;
$num_carspaces = 10;

$price_options = ($type=='sale')?
array(
"50000"=>"50,000","100000"=>"100,000","150000"=>"150,000","200000"=>"200,000","250000"=>"250,000","300000"=>"300,000","350000"=>"350,000","400000"=>"400,000","450000"=>"450,000","500000"=>"500,000","550000"=>"550,000","600000"=>"600,000","650000"=>"650,000","700000"=>"700,000","750000"=>"750,000","800000"=>"800,000","850000"=>"850,000","900000"=>"900,000","950000"=>"950,000","1000000"=>"1,000,000","1250000"=>"1,250,000","1500000"=>"1,500,000","1750000"=>"1,750,000","2000000"=>"2,000,000","2500000"=>"2,500,000","3000000"=>"3,000,000","4000000"=>"4,000,000","5000000"=>"5,000,000","10000000"=>"10,000,000"
):
array(
"50"=>"50","100"=>"100","150"=>"150","200"=>"200","250"=>"250","300"=>"300","350"=>"350","400"=>"400","450"=>"450","500"=>"500","550"=>"550","600"=>"600","650"=>"650","700"=>"700","750"=>"750","800"=>"800","950"=>"950","1000"=>"1,000","1100"=>"1,100","1200"=>"1,200","1300"=>"1,300","1400"=>"1,400","1500"=>"1,500","1600"=>"1,600","1700"=>"1,700","1800"=>"1,800","1900"=>"1,900","2000"=>"2,000","2500"=>"2,500","3000"=>"3,000","4000"=>"4,000","5000"=>"5,000"
);
$max_price = end($price_options);

global $helper, $realty, $wpdb;
$office_id = esc_sql(implode(', ', $realty->settings['general_settings']['office_id']));

	
$suburbs=get_option('sm_suburbs');
if(empty($suburbs)){
	$list_suburbs = $wpdb->get_col("select distinct suburb from properties where suburb!='' and office_id IN ($office_id) order by suburb ");
	$suburbs=implode(",",$list_suburbs);	
}
$suburbs=explode(",",$suburbs);

$property_types=get_option('sm_property_types');
if(empty($property_types)){
	$list_property_types = $wpdb->get_col("select distinct property_type from properties where property_type!='' and office_id IN ($office_id) order by property_type ");
	$property_types=implode(",",$list_property_types);	
}
$property_types=explode(",",$property_types);
?>
<fieldset>
<ul id="<?php echo $type; ?>_search" style="display:<?php echo 'block'; ?>">
	<li class="property_type">															
		<select name="query_string[<?php echo $type; ?>][property_type]">
			<option value="">Property Type</option>
			<?php foreach($property_types as $property_type):$property_type=ucwords(strtolower(trim($property_type))); ?>
			<option value="<?php echo $property_type; ?>"<?php if( $property_type==$this->subscriber[$type]['property_type']) echo ' selected="selected"'; ?>><?php echo $property_type; ?></option>			
			<?php endforeach; ?>										
		</select>
	</li>

	<li class="house_rooms">
		<div class="bedrooms numberofrooms">
			<select name="query_string[<?php echo $type; ?>][bedrooms]" class="bedrooms">
			<option value="">Bedrooms</option>
			<?php for ( $i = 1 ; $i <= $num_bedrooms ; ++$i ) : ?>
			<option value="<?php echo $i ?>"<?php if( $i==$this->subscriber[$type]['bedrooms']) echo ' selected="selected"'; ?>><?php echo $i; echo '+';?></option>
			<?php endfor;	?>
			</select>
		</div>
		
		<div class="bathrooms numberofrooms">
			<select name="query_string[<?php echo $type; ?>][bathrooms]" class="bathrooms">		
				<option value="">Bathrooms</option>
				<?php 	for ( $i = 1 ; $i <= $num_bathrooms ; ++$i ):	?>
				<option value="<?php echo $i ?>"<?php if( $i==$this->subscriber[$type]['bathrooms']) echo ' selected="selected"'; ?>><?php echo $i; '+'; ?></option>
			<?php endfor;?>
			</select>
		</div>
	</li>				
	
	<li class="price">
		<div class="price_min prices">
			<select name="query_string[<?php echo $type; ?>][price_min]">
				<option value="">Min Price</option>
				<?php foreach($price_options as $price_option=>$price_value): ?>
				<option value="<?php echo $price_option ?>"<?php if( $price_option>0 and $price_option==$this->subscriber[$type]['price_min']) echo ' selected="selected"'; ?>>$<?php echo $price_value; if($price_option ==$max_price) echo '+'; ?></option>
				<?php endforeach;?>
			</select>
		</div>
	
		<div class="price_max prices">
			<select name="query_string[<?php echo $type; ?>][price_max]">
			<option value="">Max Price</option>
				<?php foreach($price_options as $price_option=>$price_value): ?>
				<option value="<?php echo $price_option ?>"<?php if( $price_option>0 and $price_option==$this->subscriber[$type]['price_max']) echo ' selected="selected"'; ?>>$<?php echo $price_value; if($price_option ==$max_price) echo '+'; ?></option>
				<?php endforeach;?>
			</select>
		</div>
	</li>
	
	<li class="suburb_select">
		<select name="query_string[<?php echo $type; ?>][suburb][]" multiple="multiple" class="select_suburb">
			<option value="">All Suburbs</option>
			<?php 
			if($suburbs){
				foreach($suburbs as $suburb): $suburb=ucwords(strtolower(trim($suburb))); ?>
				<option value="<?php echo $suburb; ?>"<?php if(is_array($this->subscriber[$type]['suburb'])){ if(in_array($suburb, $this->subscriber[$type]['suburb']))echo ' selected="selected"'; } ?>><?php echo $suburb; ?></option>			
				<?php endforeach;
			} ?>										
		</select>
	</li>
</ul><!-- end #<?php echo $type; ?>_search -->
</fieldset>