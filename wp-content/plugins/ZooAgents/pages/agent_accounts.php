<?php
global $helper, $current_user, $realty, $wpdb;
if(!current_user_can('administrator')){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }

$office_id_all = $realty->settings['general_settings']['office_id'];


if (!empty($_GET['office_id'])){
    $settings = get_option('realty_general_settings');

    if ($_GET['deactivate'] == 'true')
        $settings['office_id'] = array_diff($settings['office_id'], array($_GET['office_id']));
    else if ($_GET['activate'] == 'true')
        array_push($settings['office_id'], $_GET['office_id']);

    $office_id_all = $settings['office_id'];
    update_option('realty_general_settings', $settings);
	echo "<div id='success'><span>Office deactivated.</span></div>";
}

if($_GET['task']=='give_access' && !empty($_GET['id'])){
	$agent=$wpdb->get_results("select * from office where id=".intval($_GET['id']), ARRAY_A);
	if(!empty($agent)){
		$agent=$agent[0];			
		$_POST['created_at'] = gmdate("Y-m-d H:i:s");
		$_POST['officename'] = $agent['name'];
		if(empty($agent['email']))$agent['email']=$wpdb->get_var("select email from users where email is not null and email!='' and office_id=".intval($_GET['id']));
		if(empty($agent['email']))$agent['email']=$agent['name'];
		$_POST['email'] = $user_email = $agent['email'];
		$_POST['password'] = $password = wp_generate_password();
		$this->insert_form( "agent_account", $_POST );
		
	    $wp_user_id=$wpdb->get_var("select id from agent_account where email='$user_email'");
		
		$wpdb->query("update office set wp_user_id='$wp_user_id' where id=".$_GET['id']);
		
		$settings=get_option('realty_general_settings');
		if(!in_array($_GET['id'], $settings['office_id']))array_push($settings['office_id'], $_GET['id']);
		update_option('realty_general_settings', $settings);
		
		$login=$realty->siteUrl.'agent-login/';
		$subject="[".$this->blogname."] Your username and password";
		$message="You can edit your account or create a listing at $login. Below is your login detail:\n\rUsername: $user_email\n\rPassword: $password\n\r\n\rBest Regards,\n\r\n\r".$this->blogname;				
		$this->text_email($agent['email'], $subject, $message, '', '', false);
		
		//$helper->query("delete from agent_account where id=".$_GET['id']);
		echo "<div id='success'><span>Access granted. Email sent to $user_email.</span></div>";
			
	}
}


?>
<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/table_sorter.css" type="text/css" media="all" />
<script type="text/javascript">jQuery(function() { jQuery("#properties_table").tablesorter({ widgets: ['zebra'] }); });</script>	

<?php
include('menu_admin.php'); 
/*
$offices = $helper->get_results("select id, name, created_at, email from office where 1 $condition order by name");
?>

<form name="update_properties" id="update_properties" class="form-profile" action="" method="post" autocomplete="off" enctype="multipart/form-data">
	<div class="block_content">
		<h4>Search By</h4>
		<table>
			<tr>
				<td>
					<select name="office_id" class="full">
						<option value="">Account ID</option>
						<?php foreach($offices as $office){ ?>
							<option value="<?php echo $office['id']?>" <?php if($_POST['office_id']==$office['id'])echo 'selected="selected"'; ?>><?php echo $office['id']?></option>
						<?php } ?>
					</select>					
				</td>
				<td>
					<select name="officename" class="full">
						<option value="">Account Name</option>
						<?php foreach($offices as $office){ ?>
							<option value="<?php echo $office['name']?>" <?php if($_POST['officename']==$office['name'])echo 'selected="selected"'; ?>><?php echo $office['name']?></option>
						<?php } ?>
					</select>
				</td>
				<td>
					<select name="email" class="full">
						<option value="">Account Email</option>
						<?php foreach($offices as $office){ ?>
							<option value="<?php echo $office['email']?>" <?php if($_POST['email']==$office['email'])echo 'selected="selected"'; ?>><?php echo $office['email']?></option>
						<?php } ?>
					</select>
				</td>				
				<td><input type="submit" name="submit" value="Search" class="btn"></td>				
			</tr>
		</table>
	</div>
</form>
<div class="clear"></div>

<?php

if(isset($_POST['submit'])){ 	
	if(!empty($_POST['office_id']))$condition="and office.id in (".$_POST['office_id'].")";
	else if(!empty($_POST['email']))$condition="and office.email='".$_POST['email']."'";
	else if(!empty($_POST['officename']))$condition="and officename='".$_POST['officename']."'";
}
*/
$offices = $wpdb->get_results("select * from office where name is not null and name!='' and id in (".implode(',',$office_id_all).") order by name", ARRAY_A);
?>

<table class="tablesorter" cellpadding="0" cellspacing="0" id="properties_table">
	<thead>
		<tr class="th">
			<th class="th_date">Date Added</th>
			<th>Agency Name</th>			
			<th>Contact Name</th>			
			<th>Email</th>			
			<th>Phone</th>	 
			<th class="th_listings" style="width:100px">Listings</th>			
			<th class="th_team" style="width:100px">Team</th>	
            <th class="th_del">Action</th>
		</tr>
	</thead>
	<?php 
	foreach($offices as $office): 
	$listings=$wpdb->get_var("select count(id) from properties where office_id=".$office['id']);
	$team=$wpdb->get_var("select count(id) from users where office_id=".$office['id']);
	$contact_name=$wpdb->get_var("select concat(firstname,' ',lastname) from users where contributor=1 and office_id=".$office['id']);
	if(!$contact_name)$contact_name=$wpdb->get_var("select concat(firstname,' ',lastname) from users where office_id=".$office['id']);
	if(!$office['email'])$office['email']=$wpdb->get_var("select email from users where email is not null and email!='' and office_id=".$office['id']);
	if(!$office['wp_user_id']){
		$office['wp_user_id']=$wpdb->get_var("select id from agent_account where officename='".esc_sql($office['name'])."'");
		if($office['wp_user_id'])$wpdb->query("update office set wp_user_id='".intval($office['wp_user_id'])."' where id='".intval($office['id'])."'");
	}
	?>
	<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
		<td class="td_date"><?php echo date("d-m-Y",strtotime($office['created_at'])); ?></td>
		<td><?php echo $office['name']; ?></td>		
		<td><?php echo $contact_name; ?></td>		
		<td><a href="malito:<?php echo $office['email']; ?>"><?php echo $office['email']; ?></a></td>	
		<td><?php echo $office['phone']; ?></td>	
		<td class="th_listings"><?php echo $listings; ?></td>		
		<td class="th_team"><?php echo $team; ?></td>		
        <td>
		<?php if(empty($office['wp_user_id'])){ ?>
		<a class="delete" onClick="return confirm('Are you sure to give access to this account.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl.'dashboard/agent-accounts/?task=give_access&id='.$office['id'];?>">Give Access</a> | <?php } ?><a href="<?php echo $realty->siteUrl.'dashboard/listings/?office_id='.$office['id']; ?>" title="View Detail" target="_blank">System</a> | <a class="delete" onClick="return confirm('Are you sure to deactivate this account.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl.'dashboard/agent-accounts/?deactivate=true&office_id='.$office['id'];?>">Deactivate</a>		
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<!--
<p>Note: Give Access meant the agent will be able to create private listings. The account will be move to `Private Listings`.</p>-->