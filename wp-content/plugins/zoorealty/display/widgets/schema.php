<?php
if(!$realty->is_property_page) return; 
$logo = get_option('logo');
$header_phone = get_option('header_phone');
?>
<div itemscope itemtype="http://schema.org/RealEstateAgent">	
	<meta itemprop="name" content="<?=get_option('blogname'); ?>" />
	<meta itemprop="description" content="<?=get_option('blogdescription'); ?>" />
	<meta itemprop="logo" content="<?=$realty->paths['theme']."images/logos/$logo"; ?>" />
	<meta itemprop="telephone" content="<?=$header_phone; ?>" />
	<meta itemprop="url" content="<?=$realty->siteUrl; ?>" />
</div>
<div itemscope itemtype="http://schema.org/Residence">
	<meta itemprop="image" content="<?=$realty->property['thumbnail']; ?>" />
	<meta itemprop="name" content="<?=$realty->property['headline']; ?>" />
	<meta itemprop="description" content="<?=$realty->property['description']; ?>" />
	<meta itemprop="url" content="<?=$realty->property['url']; ?>" />	
		
	<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	  <meta itemprop="streetAddress" content="<?=$realty->property['street_address']; ?>" />
	  <meta itemprop="addressLocality" content="<?=$realty->property['suburb']; ?>" />
	  <meta itemprop="addressRegion" content="<?=$realty->property['state']; ?>" />
	  <meta itemprop="postalCode" content="<?=$realty->property['postcode']; ?>" />
	</span>
	
	<?php if(!empty($realty->property['latitude'])){ ?>
	<span itemprop="address" itemscope itemtype="http://schema.org/geocoordinates">
	  <meta itemprop="latitude" content="<?=$realty->property['latitude']; ?>" />
	  <meta itemprop="longitude" content="<?=$realty->property['longitude']; ?>" />
	</span>
	<?php } 
	
	if (!empty($realty->property['auction_date'])){
		?>
		<span itemprop="events" itemscope itemtype="http://schema.org/Event">
			<meta itemprop="name" content="Auction" />
			<meta itemprop="startDate" content="<?=date('Y-m-dTH:i', strtotime($realty->property['auction_date'].' '.$realty->property['auction_time']))?>">
		</span>		
		<?php  
	}

	if (!empty($realty->property['opentimes'])){
		foreach($realty->property['opentimes'] as $opentime){
			$start_date=$opentime['date']." ".$opentime['start_time'];	$end_date=$opentime['date']." ".$opentime['end_time'];	?>
			<span itemprop="events" itemscope itemtype="http://schema.org/Event">				
				<meta itemprop="name" content="Open Time" />				
				<meta itemprop="startDate" content="<?=date('Y-m-dTH:i', strtotime($opentime['date'].' '.$opentime['start_time']))?>" />
				<meta itemprop="endDate" content="<?=date('Y-m-dTH:i', strtotime($opentime['date'].' '.$opentime['end_time']))?>" />
			</span>
		<?php } 
	}	
	?>
</div>
<div itemscope itemtype="http://schema.org/Organization">		
	<?php 
	global $wpdb;
	$office=$wpdb->get_results("select * from office where id=".$realty->property['office_id'],ARRAY_A);
	$office=$office[0];
	?>	
	<meta itemprop="brand" content="<?=($office['name'])?$office['name']:get_option('blogname'); ?>" />
	<meta itemprop="email" content="<?=$office['email']; ?>" />
	<meta itemprop="faxNumber" content="<?=$office['fax']; ?>" />
	<meta itemprop="telephone" content="<?=$office['phone']; ?>" />
	<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	  <meta itemprop="streetAddress" content="<?=$realty->property['street_number']." ".$realty->property['street']; ?>" />
	  <meta itemprop="addressLocality" content="<?=$realty->property['suburb']; ?>" />
	  <meta itemprop="addressRegion" content="<?=$realty->property['state']; ?>" />
	  <meta itemprop="postalCode" content="<?=$realty->property['zipcode']; ?>" />
	</span>
	<?php
	$users = $realty->property['user'];
	if($users){
		foreach($users as $user){ ?>
			<div itemprop="employees" itemscope itemtype="http://schema.org/Person">				
				<meta itemprop="name" content="<?=$user['name']; ?>">
				<meta itemprop="image" content="<?=$user['photo']; ?>">				
				<meta itemprop="jobTitle" content="<?=$user['position_for_display']; ?>">
				<meta itemprop="email" content="<?=$user['email']; ?>">
				<meta itemprop="telephone" content="<?=($user['mobile_phone'])?$user['mobile_phone']:$user['business_phone']; ?>">
			</div>
		<?php
		}
	}
	?>
</div>