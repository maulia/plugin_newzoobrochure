<?php

if (file_exists($folder.'/contact.php') && is_dir($folder.'/contact.php'))
	@rmdir($folder.'/contact.php');
if (file_exists($folder.'/office.php') && is_dir($folder.'/office.php'))
	@rmdir($folder.'/office.php');
if (file_exists($folder.'/property.php') && is_dir($folder.'/property.php'))
	@rmdir($folder.'/property.php');

global $wpdb;
$wpdb->show_errors();

$tables = $wpdb->get_results('SHOW TABLES', ARRAY_A);
foreach ($tables as $table)
{
	$table = array_values($table);
	if (!isset($table[0]))
		continue;
	$table = $table[0];
	
	$wpdb->query('ALTER TABLE  `'.$table.'` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci');

	$columns = $wpdb->get_results('SHOW FULL COLUMNS FROM `'.$table.'` WHERE collation IS NOT NULL', ARRAY_A);
	foreach ($columns as $column)
	{
		$wpdb->query("ALTER TABLE $table MODIFY `{$column['Field']}` {$column['Type']} CHARACTER SET utf8 COLLATE utf8_general_ci");
	}
}
