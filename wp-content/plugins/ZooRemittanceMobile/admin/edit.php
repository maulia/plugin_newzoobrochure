<div class="wrap">	
<h2>Edit Contact Data</h2>
<form action="" method="post">
<input type="hidden" value="edit" name="task"/>
<input type="hidden" id="id" name="subscriber_id" value="<?php echo $_GET['subscriber_id']; ?>">
<table class="form-table">
	<tr>
	<th><label>Title</label></th>
	<td>
		<div class="name_title">
		<select name="title">
			<option value="">Title</option>
			<option value="Miss"<?php if($subscribers['title']=='Miss') echo ' selected="selected"'; ?>>Miss</option>
			<option value="Mr"<?php if($subscribers['title']=='Mr') echo ' selected="selected"'; ?>>Mr</option>
			<option value="Mrs"<?php if($subscribers['title']=='Mrs') echo ' selected="selected"'; ?>>Mrs</option>
			<option value="Ms"<?php if($subscribers['title']=='Ms') echo ' selected="selected"'; ?>>Ms</option>
			<option value="Dr"<?php if($subscribers['title']=='Dr') echo ' selected="selected"'; ?>>Dr</option>
		</select>
	</div>
	</td>
	</tr>	 
	 
	<tr>
		<th><label>E-mail</label></th>
		<td><input type="text" name="email" size="33" class="searchbox" value="<?php echo $subscribers['email']; ?>" onClick="if(this.value=='enter email address') this.value='';" /></td>
	</tr>

	<tr>
		<th><label>First Name</label></th>
		<td><input type="text" name="first_name" size="33" class="searchbox" value="<?php echo $subscribers['first_name']; ?>" /></td>
	</tr>

	<tr>
		<th><label>Last Name</label></th>
		<td><input type="text" name="last_name" size="33" class="searchbox" value="<?php echo $subscribers['last_name']; ?>" /></td>
	</tr>
	<tr>
		<th><label>Home Number</label></th>
		<td><input type="text" name="home_number" size="33" class="searchbox" value="<?php echo $subscribers['home_number']; ?>" /></td>
	</tr>
	<tr>
		<th><label>Mobile Number</label></th>
		<td><input type="text" name="mobile_number" size="33" class="searchbox" value="<?php echo $subscribers['mobile_number']; ?>" /></td>
	</tr>
	<tr>
		<th><label>Work Number</label></th>
		<td><input type="text" name="work_number" size="33" class="searchbox" value="<?php echo $subscribers['work_number']; ?>" /></td>
	</tr>
	<tr>
		<th><label>Fax Number</label></th>
		<td><input type="text" name="fax_number" size="33" class="searchbox" value="<?php echo $subscribers['fax_number']; ?>" /></td>
	</tr>
  
	<tr>
		<th><label>Country</label></th>
		<td>
		<select id="country" name="country">
			<option value="Australia">Australia</option>
			<?php foreach ($list_countries as $item): ?>	
				<option value="<?php echo $item; ?>" <?php if($item == $subscribers['country']) echo 'selected="selected"'; ?>><?php echo $item; ?></option>
			<?php endforeach; ?>
			</select>
		</td>
	</tr>

	<tr>
		<th><label>How did you find us?</label></th>
		<td>
		<select name="heard_about_us">
				<option value="">How you heard about us?</option>
			<?php foreach($all_list as $item_value): ?>
				<option value="<?php echo $item_value; ?>"<?php if ($subscribers['heard_about_us'] == $item_value) echo 'selected="selected"'; ?>><?php echo $item_value; ?></option>
			<?php endforeach; ?>

			</select>
		</td>
	</tr>
</table>
</form>
<p class="submit"><input type="submit" value="<?php _e('Save &raquo;') ?>" name="update_subscriber" /></p>
</p><a href="<?php echo $this->form_action;?>"><?php _e('&laquo; Back To Main Page'); ?></a></p>
</div>
