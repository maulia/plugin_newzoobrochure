<?php
require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/wp-load.php');
$photos=get_option('homepage_images');
$uploaded_path=ABSPATH . 'wp-content/uploads/slideshow/';
$photos_upload_url=get_option('siteurl').'/wp-content/uploads/slideshow/';
if(!empty($photos)){ 
	if($_GET['key']!=''){
		$temp_photos=array();
		foreach($photos as $key=>$photo){
			if($key!=$_GET['key'])array_push($temp_photos, $photo);
			else{								
				$small_url=$uploaded_path.$photo['thumbnail'];
				$large_url=$uploaded_path.$photo['large'];
				@unlink($small_url);
				@unlink($large_url);				
			}
		}
		$photos=$temp_photos;
		if(!update_option('homepage_images',$photos))add_option('homepage_images',$photos);
	}
	foreach($photos as $key=>$photo):
	if(!empty($photo['large'])){
	?>
	<li id="arrayorder_<?php echo $key ?>">			
		<img src="<?php echo $photos_upload_url.$photo['thumbnail']; ?>"/>
		<a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="javascript:delete_photo('<?php echo $key; ?>');" title="Delete Image"></a>
		<br/><span>Text: <input type="text" name="homepage_images[<?php echo $key ?>][text]" value="<?php echo $photo['text']; ?>"></span>
		<br/><span>Link: <input type="text" name="homepage_images[<?php echo $key ?>][link]" value="<?php echo $photo['link']; ?>"></span>
		<input type="hidden" name="homepage_images[<?php echo $key ?>][large]" value="<?php echo $photo['large']; ?>">
		<input type="hidden" name="homepage_images[<?php echo $key ?>][thumbnail]" value="<?php echo $photo['thumbnail']; ?>">
	</li>
	<?php 
	} endforeach; 
}
$themes_folder=get_bloginfo('stylesheet_directory') .'/';
?>
<script type="text/javascript">
function slideout(){
	  setTimeout(function(){
	  jQuery("#response").slideUp("slow", function () {    });
		
		}, 2000);
	}
jQuery(function() {
	jQuery("#photo_list").sortable({ opacity: 0.8, cursor: 'move', update: function() {				
		document.getElementById('overlay').style.visibility = 'visible';
		var order = jQuery(this).sortable("serialize") ; 
		jQuery.post("<?php echo $themes_folder."uploadify/reorder_photo.php";?>", order, function(theResponse){					
			jQuery("#response").html(theResponse);
			jQuery("#response").show('slow');
			slideout();
			jQuery('#photo_list').load('<?php echo $themes_folder."uploadify/load_photo.php?key=";?>');	
			document.getElementById('overlay').style.visibility = 'hidden';
		}); 					
	}								  
	});
});
</script>