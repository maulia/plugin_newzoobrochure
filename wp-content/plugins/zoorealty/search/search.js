/**
 * AutoComplete Field - JavaScript Code
 *
 * This is a sample source code provided by fromvega.
 * Search for the complete article at http://www.fromvega.com
 *
 * Enjoy!
 *
 * @author fromvega
 *
 */

// global variables
var acListTotal   =  0;
var acListCurrent = -1;
var acDelay		  = 500;
var acURL		  = null;
var acSearchId	  = null;
var acResultsId	  = null;
var acSearchField = null;
var acResultsDiv  = null;

function setAutoComplete(field_id, results_id, get_url){

	// initialize vars
	acSearchId  = "#" + field_id;
	acResultsId = "#" + results_id;
	acURL 		= get_url;

	// create the results div
	jQuery("body").append('<div id="' + results_id + '"></div>');

	// register mostly used vars
	acSearchField	= jQuery(acSearchId);
	acResultsDiv	= jQuery(acResultsId);

	// reposition div
	repositionResultsDiv();
	
	// on blur listener
	acSearchField.blur(function(){ setTimeout("clearAutoComplete()", 200) });

	// on key up listener
	acSearchField.keyup(function (e) {

		// get keyCode (window.event is for IE)
		var keyCode = e.keyCode || window.event.keyCode;
		var lastVal = acSearchField.val();

		// check an treat up and down arrows
		if(updownArrow(keyCode)){
			return;
		}

		// check for an ENTER or ESC
		if(keyCode == 13 || keyCode == 27){
			clearAutoComplete();
			return;
		}

		// if is text, call with delay
		setTimeout(function () {autoComplete(lastVal)}, acDelay);
	});
}

// treat the auto-complete action (delayed function)
function autoComplete(lastValue)
{
	// get the field value
	var part = acSearchField.val();

	// if it's empty clear the resuts box and return
	if(part == ''){
		clearAutoComplete();
		return;
	}

	// if it's equal the value from the time of the call, allow
	if(lastValue != part){
		return;
	}

	// get remote data as JSON
	jQuery.getJSON(acURL + part, function(json){

		// get the total of results
		var ansLength = acListTotal = json.length;

		// if there are results populate the results div
		if(ansLength > 0){

			var newData = '';

			// create a div for each result
			for(i=0; i < ansLength; i++) {
				newData += '<div class="unselected">' + json[i] + '</div>';
			}

			// update the results div
			acResultsDiv.html(newData);
			acResultsDiv.css("display","block");
			
			// for all divs in results
			var divs = jQuery(acResultsId + " > div");
		
			// on mouse over clean previous selected and set a new one
			divs.mouseover( function() {
				divs.each(function(){ this.className = "unselected"; });
				this.className = "selected";
			})
		
			// on click copy the result text to the search field and hide
			divs.click( function() {
				acSearchField.val(this.childNodes[0].nodeValue);
				clearAutoComplete();
			});

		} else {
			clearAutoComplete();
		}
	});
}

// clear auto complete box
function clearAutoComplete()
{
	acResultsDiv.html('');
	acResultsDiv.css("display","none");
}

// reposition the results div accordingly to the search field
function repositionResultsDiv()
{
	// get the field position
	var sf_pos    = acSearchField.offset();
	var sf_top    = sf_pos.top;
	var sf_left   = sf_pos.left;

	// get the field size
	var sf_height = acSearchField.height();
	var sf_width  = acSearchField.width();

	// apply the css styles - optimized for Firefox
	acResultsDiv.css("position","absolute");
	acResultsDiv.css("left", sf_left - 2);
	acResultsDiv.css("top", sf_top + sf_height + 5);
	acResultsDiv.css("width", sf_width - 2);
}


// treat up and down key strokes defining the next selected element
function updownArrow(keyCode) {
	if(keyCode == 40 || keyCode == 38){

		if(keyCode == 38){ // keyUp
			if(acListCurrent == 0 || acListCurrent == -1){
				acListCurrent = acListTotal-1;
			}else{
				acListCurrent--;
			}
		} else { // keyDown
			if(acListCurrent == acListTotal-1){
				acListCurrent = 0;
			}else {
				acListCurrent++;
			}
		}

		// loop through each result div applying the correct style
		acResultsDiv.children().each(function(i){
			if(i == acListCurrent){
				acSearchField.val(this.childNodes[0].nodeValue);
				this.className = "selected";
			} else {
				this.className = "unselected";
			}
		});

		return true;
	} else {
		// reset
		acListCurrent = -1;
		return false;
	}
}

/* FORMS */
jQuery(document).ready(function(){
	setAutoComplete("buy_keywords", "results", url_auto);
	jQuery('#buy_keywords').focus(function() {
		if(jQuery(this).val() == defaultLocation) {
			jQuery(this).val('');
		}
	});
	jQuery('#buy_keywords').blur(function() {
		if(jQuery(this).val() == '') {
			jQuery(this).val(defaultLocation);
		}
	});
});

function check_all_suburb(){		
	if(eval("document.getElementById('all_suburb').checked") == true)
	var max = document.getElementsByName('suburb[]').length;		
	for(var idx = 0; idx < max; idx++){document.getElementsByName('suburb[]')[idx].checked = false;}
} 

function check_suburb(value){		
	var flag='0';
	var count='0';
	var temp='';
	var max = document.getElementsByName('suburb[]').length;
	if(eval("document.getElementById('all_suburb').checked") == true)flag='1';					
	if(flag=='1'){
		document.getElementById('all_suburb').checked = false;
		for(var idx = 0; idx < max; idx++){
			if(document.getElementsByName('suburb[]')[idx].value!=value)document.getElementsByName('suburb[]')[idx].checked = false;
			else document.getElementsByName('suburb[]')[idx].checked = true;
		}
	}
	for(var idx = 0; idx < max; idx++){
		if(document.getElementsByName('suburb[]')[idx].checked == true){ count++; temp=temp + document.getElementsByName('suburb[]')[idx].value + ", " ; }
	}
	if(count=='0')document.getElementsByName('all_suburb').checked = true; 
}

function load_tab(list){
	if(list!=current_tab){
		current_tab=list;
		document.getElementById('list_value').value=list;
		document.getElementById('select_type').innerHTML=document.getElementById(list+'_types').innerHTML;
		document.getElementById('select_property_type').innerHTML=document.getElementById(list+'_property_types').innerHTML;
		load_dropdown('',1);
		document.getElementById('price_min').innerHTML=document.getElementById(list+'_min_price').innerHTML;
		document.getElementById('price_max').innerHTML=document.getElementById(list+'_max_price').innerHTML;
		if(list=='sale'){
			jQuery('#sale_li').addClass(' ui-tabs-selected');
			jQuery('#lease_li').removeClass(' ui-tabs-selected');
			jQuery('#pet_feature').hide('');
			document.getElementById('chk_pet').checked = false;
		}
		else {
			jQuery('#lease_li').addClass(' ui-tabs-selected');
			jQuery('#sale_li').removeClass(' ui-tabs-selected');
			jQuery('#pet_feature').show('');
		}
	}
}

function load_dropdown(property_type, suburb){
	var type=document.getElementById('select_type').value;
	if(property_type=='1'){
		var url = url_load+escape( current_tab )+"&type="+escape( type )+"&cat=property_type";	
		jQuery('#select_property_type').load(url);
	}
	if(suburb=='1'){
		var url = url_load+escape( current_tab )+"&type="+escape( type )+"&cat=suburb";	
		jQuery('#select_suburb').load(url);
	}
}
 
function reset_value(){
	if(jQuery('#buy_keywords').val() == defaultLocation)jQuery('#buy_keywords').val('');
}


/* DIMENSION JS */

eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('1.a.1g=1.a.f;1.a.Y=1.a.t;1.a.f=e(){6(2[0]==g)9 r.T||1.p&&7.z.1b||7.d.1b;6(2[0]==7)9 14.11(7.d.1n,7.d.G);9 2.1g(W[0])};1.a.t=e(){6(2[0]==g)9 r.V||1.p&&7.z.U||7.d.U;6(2[0]==7)9 14.11(7.d.1j,7.d.D);9 2.Y(W[0])};1.a.T=e(){9 2[0]==g||2[0]==7?2.f():2.3(\'o\')!=\'B\'?2[0].G-(4(2.3("k"))||0)-(4(2.3("1d"))||0):2.f()+(4(2.3("N"))||0)+(4(2.3("1c"))||0)};1.a.V=e(){9 2[0]==g||2[0]==7?2.t():2.3(\'o\')!=\'B\'?2[0].D-(4(2.3("i"))||0)-(4(2.3("18"))||0):2.f()+(4(2.3("J"))||0)+(4(2.3("16"))||0)};1.a.1s=e(){9 2[0]==g||2[0]==7?2.f():2.3(\'o\')!=\'B\'?2[0].G:2.f()+(4(2.3("k"))||0)+(4(2.3("1d"))||0)+(4(2.3("N"))||0)+(4(2.3("1c"))||0)};1.a.1r=e(){9 2[0]==g||2[0]==7?2.t():2.3(\'o\')!=\'B\'?2[0].D:2.f()+(4(2.3("i"))||0)+(4(2.3("18"))||0)+(4(2.3("J"))||0)+(4(2.3("16"))||0)};1.a.h=e(){6(2[0]==g||2[0]==7)9 r.1p||1.p&&7.z.h||7.d.h;9 2[0].h};1.a.j=e(){6(2[0]==g||2[0]==7)9 r.1l||1.p&&7.z.j||7.d.j;9 2[0].j};1.a.1k=e(c,F){u x=0,y=0,8=2[0],5=2[0],s=E,C=E,n,l=0,m=0,c=1.S({R:q,P:q,1h:E,A:q},c||{});1f{x+=5.1x||0;y+=5.1w||0;6(1.b.v||1.b.M){u L=4(1.3(5,\'k\'))||0;u K=4(1.3(5,\'i\'))||0;x+=K;y+=L;6(1.b.v&&5!=8&&1.3(5,\'1a\')!=\'19\'){x+=K;y+=L}6(1.3(5,\'w\')==\'1e\')s=q;6(1.3(5,\'w\')==\'1v\')C=q}6(c.A){n=5.17;1f{l+=5.h||0;m+=5.j||0;5=5.1u;6(1.b.v&&5!=8&&5!=n&&1.3(5,\'1a\')!=\'19\'){x+=4(1.3(5,\'i\'))||0;y+=4(1.3(5,\'k\'))||0}}15(n&&5!=n)}Q 5=5.17;6(5&&(5.13.12()==\'d\'||5.13.12()==\'1q\')){6((1.b.H||(1.b.M&&1.p))&&1.3(8,\'w\')!=\'1e\'){x+=4(1.3(5,\'10\'))||0;y+=4(1.3(5,\'Z\'))||0}6((1.b.v&&!s)||(1.b.M&&1.3(8,\'w\')==\'1o\'&&(!C||!s))){x+=4(1.3(5,\'i\'))||0;y+=4(1.3(5,\'k\'))||0}1m}}15(5);6(!c.R){x-=4(1.3(8,\'10\'))||0;y-=4(1.3(8,\'Z\'))||0}6(c.P&&(1.b.H||1.b.O)){x+=4(1.3(8,\'i\'))||0;y+=4(1.3(8,\'k\'))||0}Q 6(!c.P&&!(1.b.H||1.b.O)){x-=4(1.3(8,\'i\'))||0;y-=4(1.3(8,\'k\'))||0}6(c.1h){x+=4(1.3(8,\'J\'))||0;y+=4(1.3(8,\'N\'))||0}6(c.A&&1.b.O&&1.3(8,\'o\')==\'1t\'){l-=8.h||0;m-=8.j||0}u I=c.A?{X:y-m,1i:x-l,j:m,h:l}:{X:y,1i:x};6(F){1.S(F,I);9 2}Q{9 I}};',62,96,'|jQuery|this|css|parseInt|parent|if|document|elem|return|fn|browser|options|body|function|height|window|scrollLeft|borderLeftWidth|scrollTop|borderTopWidth|sl|st|op|display|boxModel|true|self|absparent|width|var|mozilla|position|||documentElement|scroll|none|relparent|offsetWidth|false|returnObject|offsetHeight|safari|returnValue|paddingLeft|bl|bt|msie|paddingTop|opera|border|else|margin|extend|innerHeight|clientWidth|innerWidth|arguments|top|_width|marginTop|marginLeft|max|toLowerCase|tagName|Math|while|paddingRight|offsetParent|borderRightWidth|visible|overflow|clientHeight|paddingBottom|borderBottomWidth|absolute|do|_height|padding|left|scrollWidth|offset|pageYOffset|break|scrollHeight|static|pageXOffset|html|outerWidth|outerHeight|inline|parentNode|relative|offsetTop|offsetLeft'.split('|'),0,{}))