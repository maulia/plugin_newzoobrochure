<?php
class my_db{
		var $debug = false;
		var $print_query = false;
		function query($sql){		
			global $wpdb;
			$time_start = $this->microtime_float();
			$return = $wpdb->query($sql);
			$time_end = $this->microtime_float();
			if ($this->debug)
				echo '<p><font color="red">SQL: </font>' . $sql . '<br/><font color="blue" size="-3">*[Query took ' . number_format(($time_end - $time_start),6) . ' seconds ]*</font></p>';
			if ($this->print_query)
				echo '<p>' . $sql . '</p>';
			return $return;
		}//ends function
		
		function get_var($sql){			
			global $wpdb;
			return $wpdb->get_var($sql);			
		}

		function get_column($sql){
			global $wpdb;	
			return $wpdb->get_col($sql);	
		}
	
		function get_row($sql, $row=0){
			global $wpdb;	
			$return = $wpdb->get_results($sql, ARRAY_A);
			return $return[$row];
		}
		
		function get_results($sql, $return_type = 'array'){
			global $wpdb;	
			return $wpdb->get_results($sql, ARRAY_A);	
		}
		
		function error($error,$sql){
			if ($this->debug)
				die($error . '<p><font color="red">SQL: </font>' . $sql . '</p>');
		}

		function microtime_float(){
			list($usec, $sec) = explode(" ", microtime());
			return ((float)$usec + (float)$sec);
		}
		
		function my_stripslashes(&$string){
			$string = stripslashes($string);
		}
		
		function parseArrayToObject(&$array) {
		   $object = new stdClass();
		   if (is_array($array) && count($array) > 0) {
			  foreach ($array as $name=>$value) {
				 $name = trim($name);
				 if (!empty($name)) {
					$object->$name = $value;
				 }
			  }
		   }
		   $array = $object;
		   return $object;
		}
		
		function parseObjectToArray(&$object) {
		   $array = array();
		   if (is_object($object)) {
			  $array = get_object_vars($object);
		   }
		   $object = $array;
		   return $array;
		}	
}
?>