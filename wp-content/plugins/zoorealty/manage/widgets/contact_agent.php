<p><input value="1" name="realty_widget_<?php echo $caption; ?>[photo]" type="checkbox"<?php if($settings['photo']) echo ' checked="checked"'; ?>> <label>Photo</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[name]" type="checkbox"<?php if($settings['name']) echo ' checked="checked"'; ?>> <label>Name</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[business_phone]" type="checkbox"<?php if($settings['business_phone']) echo ' checked="checked"'; ?>> <label>Phone</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[email]" type="checkbox"<?php if($settings['email']) echo ' checked="checked"'; ?>> <label>E-mail</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[mobile]" type="checkbox"<?php if($settings['mobile']) echo ' checked="checked"'; ?>> <label>Mobile</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[fax]" type="checkbox"<?php if($settings['fax']) echo ' checked="checked"'; ?>> <label>Fax</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[role]" type="checkbox"<?php if($settings['role']) echo ' checked="checked"'; ?>> <label>Role</label> </p>
<p>Display image:</p>	
<p><input value="portrait" name="realty_widget_<?php echo $caption; ?>[image_mode]" type="radio"<?php if($settings['image_mode']=='portrait') echo ' checked="checked"'; ?>> <label>Portrait</label> </p>	
<p><input value="landscape" name="realty_widget_<?php echo $caption; ?>[image_mode]" type="radio"<?php if($settings['image_mode']=='landscape') echo ' checked="checked"'; ?>> <label>Landscape</label> </p>	
<br/>
<p>Display contact agent form:</p>	
<p><input value="popup" name="realty_widget_<?php echo $caption; ?>[agent_form_mode]" type="radio"<?php if($settings['agent_form_mode']=='popup') echo ' checked="checked"'; ?>> <label>Pop up new window</label> </p>	
<p><input value="slider" name="realty_widget_<?php echo $caption; ?>[agent_form_mode]" type="radio"<?php if($settings['agent_form_mode']=='slider') echo ' checked="checked"'; ?>> <label>Slider</label> </p>	
<p><input value="display_directly" name="realty_widget_<?php echo $caption; ?>[agent_form_mode]" type="radio"<?php if($settings['agent_form_mode']=='display_directly') echo ' checked="checked"'; ?>> <label>Display Form Directly</label> </p>	
<br/>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[not_contact_form]" type="checkbox" <?php if($settings['not_contact_form']) echo ' checked="checked"'; ?>> <label>Not Display Contact Agent Form</label> </p>	
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[not_agent_link]" type="checkbox" <?php if($settings['not_agent_link']) echo ' checked="checked"'; ?>> <label>Not Display Contact Agent Name as a Link</label> </p>	
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[download_vcard]" type="checkbox" <?php if($settings['download_vcard']) echo ' checked="checked"'; ?>> <label>Display a link to download the vcard</label> </p>