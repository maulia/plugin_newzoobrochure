<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
require($path.'/wp-load.php');
global $realty;
$settings=$realty->settings['widgets']['quick_search'];
$cat=$_GET['cat'];
$list=$_GET['list'];
$type=$_GET['type'];
if(strpos(strtolower($type), 'lease') !== false && strpos(strtolower($type), 'commercial') === false)$type='lease';
if(strpos(strtolower($type), 'commercial') !== false)$type='commercials';
if(strpos(strtolower($type), 'commercial') === false && strpos(strtolower($type), 'lease') === false)$type='sale';
$state=$_GET['state'];
$status=$_GET['status'];
if($cat=='price'){ $price_options = $settings[$type]; $max_price = end($price_options);
?>
	<div class="price_min prices">
		<select name="price_min">
			<option value="">Min Price</option>
			<?php foreach($price_options as $price_option): 
			$delimiter = strpos($price_option, ':'); 
			$value = substr($price_option, 0, $delimiter); ?>
			<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_min']) echo ' selected="selected"'; ?>><?php echo '$'.substr($price_option, $delimiter+1);// if($price_option ==$max_price) echo '+'; ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<div class="price_max prices">
		<select name="price_max">
			<option value="">Max Price</option>
			<?php foreach($price_options as $price_option): 
			$delimiter = strpos($price_option, ':'); 
			$value = substr($price_option, 0, $delimiter); ?>
			<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_max']) echo ' selected="selected"'; ?>><?php echo '$'.substr($price_option, $delimiter+1); if($price_option ==$max_price) echo '+'; ?></option>
			<?php endforeach;?>
		</select>
</div>
<?php } 
if($cat=='suburb') { $suburbs = $realty->property_search('suburb',$list,$state,$status); ?>
	<option value="">All Suburbs</option>
	<?php foreach ($suburbs as $suburb): if(!is_array($suburb)) continue; ?>
	<option value="<?php echo $suburb['tag']; ?>"<?php if(is_array($_GET['suburb']) and in_array($suburb['tag'], $_GET['suburb'])) echo ' selected="selected"'; ?>><?php echo $suburb['tag']; ?></option>
	<?php endforeach; } 
	
if($cat=='suburb2') { 
	$suburbs = $realty->property_search('suburb',$list,$state,$status); ?>
	<option value="">Any</option>
<?php	foreach ($suburbs as $suburb): 
	if(!is_array($suburb)) continue; ?>
	<option value="<?php echo $suburb['tag']; ?>"><?php echo $suburb['tag']; ?></option>
	<?php endforeach; 
} 	

if($cat=='suburb3') { 
	$suburbs = $realty->property_search('suburb',$list,$state,$status); ?>
	<p class="chk-protype"><input type="checkbox" class="checkbox" value="1" id="all_suburb" onClick="check_all_suburb();" checked> All Suburbs</p>
	<?php foreach($suburbs as $suburb){ if(!is_array($suburb)) continue;?>
	<p class="chk-protype"><input name="suburb[]" type="checkbox" class="checkbox" value="<?php echo ucwords(strtolower($suburb['tag'])); ?>" onClick="check_suburb('<?php echo ucwords(strtolower($suburb['tag'])); ?>');"> <?php echo ucwords(strtolower($suburb['tag'])); ?>(<?php echo ucwords(strtolower($suburb['number_of_properties'])); ?>)</p>
	<?php } 
} 

if($cat=='property_type') { $property_types = $realty->property_search('property_type',$list,$state,$status); ?>
	<option value="">Property Type</option>
 <?php foreach($property_types as $property_type): if(!is_array($property_type)) continue; ?>
	<option value="<?php echo $property_type['tag']; ?>"<?php if( $property_type['tag']==$_GET['property_type']) echo ' selected="selected"'; ?>><?php echo $property_type['tag']; ?></option>
<?php endforeach; } 

if($cat=='property_type2') { $property_types = $realty->property_search('property_type',$list,$state,$status); ?>
	<option value="">Any Listing Types</option>
 <?php foreach($property_types as $property_type): if(!is_array($property_type)) continue; ?>
	<option value="<?php echo $property_type['tag']; ?>"<?php if( $property_type['tag']==$_GET['property_type']) echo ' selected="selected"'; ?>><?php echo $property_type['tag']; ?></option>
<?php endforeach; }

if($cat=='category') { 
	if(!empty($_GET['property_type']))$condition=" and property_type='".$_GET['property_type']."'";
	$categories = $wpdb->get_results("select distinct category from properties where category is not null and category not in ('') $condition order by category", ARRAY_A); ?>
	<option value="">Any</option>
	<?php 
	foreach($categories as $item): 
		?>
		<option value="<?php echo $item['category']; ?>"><?php echo $item['category']; ?></option>
<?php endforeach; 
} ?>