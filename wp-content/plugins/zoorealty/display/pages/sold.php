<?php
$condition = $_GET;
$condition['list'] = $this->params['list'];
if ($this->settings['general_settings']['sold_/_leased_pages_same_layout_as_search_results_page']):
    $listings = $this->sold($condition);
    require('search_results.php');
else:
    $condition['limit'] = 5000;
    $listings = $this->sold($condition);
    if ($listings['results_count'] < 1):
        ?>
        <p>Sorry, no properties were found for your search criteria.</p>
        <?php
        return;
    endif;
    $sale_information = $this->settings['sold_and_leased'];
    ?>
    <table class="results" cellpadding="0" cellspacing="0" summary="Sales Results">
        <tr class="th">
            <th class="th_address">Address</th>
            <th class="th_suburb">Suburb</th>
            <th class="th_date"><?php echo ($this->params['list'] == 'sale') ? 'Sold' : 'Leased'; ?> Date</th>
            <th class="th_bed">Bed</th>
            <th class="th_bath">Bath</th>
            <th class="th_car">Car</th>
            <th class="th_price"><?php echo ($this->params['list'] == 'sale') ? 'Sale' : 'Lease'; ?> Price</th>
            <th class="th_view"></th>
            <?php if ($property['is_for_sale']): ?><th class="th_image">Subscriber?</th><?php endif; ?>
        </tr>
        
        <?php
        foreach ($listings as $property):
            if (!is_array($property) || !$property['display'])
                continue;
            ?>
            <tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">

                <td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></span></a></td>
                <td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
                <td class="td_date"><span class="day"><span class="date"><?php if (!empty($property['date']) && $property['date'] != '0000-00-00') echo date("j-m-Y", strtotime($property['date'])); ?></span></td>
                <td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
                <td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
                <td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
                <td class="td_price"><span class="td_price"><?php echo ($property['display_sold_price']) ? $property['last_price'] : 'Undisclosed'; ?></span></td>
                <td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
            </tr>

        <?php endforeach; //properties   ?>
    </table>
<?php
endif; //sold_/_leased_pages_same_layout_as_search_results_page ?>