<?php get_header(); ?>


<div id="content" class="default-post single">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<?php/* <h1 class="page-title"><?php the_title(); ?></h1>*/ ?>
			<?php if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; } ?><p class="post_data">Posted in <?php the_category(', ') ?> on <?php the_time('jS F, Y') ?> | <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>

			<div class="entry">
				<?php the_content(); ?>
				<?php global $wpdb, $table_prefix; $plugin_table=$table_prefix . "property_ads"; $q=$wpdb->get_results("select * from $plugin_table where post_id=".$id."");
				if(!empty($q)){ ?>
					<p class="post-button"><a class="btn" href="<?php echo get_option('siteurl')."/".$q[0]->property_id;?>">View <?php echo $q[0]->street.", ".ucwords(strtolower($q[0]->suburb)); ?> </a></p>
				<?php } ?>
                <div class="clear"></div>
			</div>

		</div>

	<?php comments_template('', true); ?>

	<?php $tags = wp_get_post_tags($post->ID);
	if ($tags) {
		$tag_ids = array();
		foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
	
		$args=array(
			'tag__in' => $tag_ids,
			'post__not_in' => array($post->ID),
			'showposts'=>5, // Number of related posts that will be shown.
			'caller_get_posts'=>1
		);
		$my_query = new wp_query($args);
		if( $my_query->have_posts() ) {
			echo '<div id="wp_related_posts">';
			echo '<h3>Related Posts</h3><ul>';
			while ($my_query->have_posts()) {
				$my_query->the_post();
			?>
				<li><?php the_time('jS F, Y') ?> - <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> (<?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>)</li>
			<?php
			}
			echo '</ul>';
			echo '</div>';
		}
	}
	?>
	
	
		<?php endwhile; else: ?>
	
			<p>Sorry, no posts matched your criteria.</p>
	
	<?php endif; ?>
</div><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>