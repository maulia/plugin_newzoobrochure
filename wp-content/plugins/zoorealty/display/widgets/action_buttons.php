<?php
global $realty;
if (!$realty->is_property_page)
    return; /* check if this is the property page before displaying the widget */
?>

<?php echo $before_title; ?>Property Tools<?php echo $after_title; ?>
<script type="text/javascript">
jQuery(document).ready(function(){
	// jQuery("a[rel^='ajax']").prettyPhoto({allow_resize: false,deeplinking: false,social_tools: false});
    jQuery("a[rel^='ajax'], a[rel^='iframe']").prettyPhoto({allow_resize:false, deeplinking:false, social_tools:false, allow_expand:false, show_title:false, theme:'light_square', keyboard_shortcuts: false});
});
function send_email(){
	var first_name = jQuery("#first_name").val();
	var last_name = jQuery("#last_name").val(); 
	var from = jQuery("#from").val();
	var first_name1 = jQuery("#first_name1").val();
	var last_name1 = jQuery("#last_name1").val(); 
	var email = jQuery("#email").val();
	var message = jQuery("#comments").val();
	var property_id = jQuery("#property_id").val();
	if(first_name=='First Name' || first_name=='')alert("You have entered an invalid first name");
	else if ((from.indexOf('@') < 0) || ((from.charAt(from.length-4) != '.') && (from.charAt(from.length-3) != '.')))alert("You have entered an invalid your email address. Please try again.");
	else if(first_name1=='First Name' || first_name1=='')alert("You have entered an invalid friend's first name");
	else if ((email.indexOf('@') < 0) || ((email.charAt(email.length-4) != '.') && (email.charAt(email.length-3) != '.')))alert("You have entered an invalid friend's email address. Please try again.");	
	else{

		// var url    = "<?php echo get_option('siteurl')?>/wp-content/plugins/Realty/display/pages/js/email_property_lightbox.php?message="+escape( message )+"&first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&property_id="+escape( property_id )+"&from="+escape( from )+"&first_name1="+escape( first_name1 )+"&last_name1="+escape( last_name1 );

		// jQuery("#return").load(url);

        var url = "<?php echo $realty->pluginUrl; ?>display/pages/js/email_property_lightbox.php?message="+escape( message )+"&first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&property_id="+escape( property_id )+"&from="+escape( from )+"&first_name1="+escape( first_name1 )+"&last_name1="+escape( last_name1 );
            jQuery("#return").load(url);	
	}
}
function send_email_self(){
	var first_name = jQuery("#sfirst_name").val();
	var last_name = jQuery("#slast_name").val(); 
	var email = jQuery("#semail").val();
	var property_id = jQuery("#property_id_self").val();
	if(first_name=='First Name' || first_name=='')alert("You have entered an invalid first name");
	else if ((email.indexOf('@') < 0) || ((email.charAt(email.length-4) != '.') && (email.charAt(email.length-3) != '.')))alert("You have entered an invalid friend's email address. Please try again.");	
	else{

		// var url    = "<?php echo get_option('siteurl')?>/wp-content/plugins/Realty/display/pages/js/email_property_lightbox.php?first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&property_id="+escape( property_id );
		// jQuery("#return_self").load(url);
        var url = "<?php echo $realty->pluginUrl; ?>display/pages/js/email_property_lightbox.php?first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&property_id="+escape( property_id );
            jQuery("#return_self").load(url);	
	}
}
</script>
<div class="wrap">
    <link rel="stylesheet" type="text/css" href="<?php echo $realty->pluginUrl; ?>css/prettyPhoto-rev1.css" />
    <script type="text/javascript" src="<?php echo $realty->pluginUrl; ?>js/jquery.prettyPhoto-rev1.js"></script>
    <script type="text/javascript" charset="utf-8">
        jQuery(document).ready(function() {
            jQuery("a[rel^='prettyPhoto']").prettyPhoto({deeplinking: false, social_tools:false});
        });
    </script>
    <div id="property_tools">
       	<?php if($settings['email_self']=='1'){ ?>
	<p class="button email_to_self"><a href="<?php echo $realty->pluginUrl.'display/elements/email_property_lightbox.php?ajax=true&width=600&height=500&id='.$realty->property['id']; ?>" rel="prettyPhoto" title="Email to Yourself" class="btn">Email</a></p>
	<?php }
	
	if($settings['email_friend']=='1'){ ?>
	<p class="button email_to_friend"><a href="<?php echo $realty->pluginUrl.'display/elements/email_friend_lightbox.php?ajax=true&width=600&height=500&id='.$realty->property['id']; ?>" rel="prettyPhoto" title="Email to a Friend" class="btn">Email</a></p>
	<?php } 

        if ($settings['print_listing'] == '1'): ?>
            <p class="button"><a class="brochure" href="<?php echo $realty->pluginUrl; ?>display/elements/print_property_pdf.php?property_id=<?php echo $realty->property['id']; ?>" rel="width=820, height=960" title="Property Brochure" target="_blank">Brochure</a></p>
        <?php endif; ?>	

        <?php if ($settings['fav'] == '1') : ?>
            <div id="add_to_favs">
                <?php
                global $favourite, $realty;
                $helper;
                $id = $realty->property['id'];
                if ($favourite->check_prop_id($id) == false):
                    ?>
                    <p class="button"><a class="add_favs" href="javascript:saveProp('<?php echo get_option('siteurl'); ?>','<?php echo $id; ?>');" title="Add to Favourites">Save</a></p>
                <?php else: ?>
                    <p class="button"><a class="add_remove_favs" onClick="return confirm('Are you sure want to remove from your Favourite ?');" href="javascript:delProp('<?php echo get_option('siteurl'); ?>','<?php echo $id; ?>');" title="View Favourites">Remove</a></p>
                <?php
                endif;
                if (!empty($_COOKIE["fav_cookie"]))
                {
                    if (!isset($_COOKIE["fav_visit"]))
                    {
                        ?>
                        <p class="button"><a class="view_favs" href="<?php echo $realty->get_permalink('favourite_property'); ?>" title="View Favourites">Saved</a></p>
                        <?php
                    }
                }
                ?>
            </div>
        <?php endif; ?>

        <?php
        $photos = array();
        $j = '0';
        $k = '0';
        $l = '0';
        for ($i = 0; $i < count($realty->property['photos']); $i++)
        {
            if ($realty->property['photos'][$i]['small'] != "")
            {
                $photos[$j]['small'] = $realty->property['photos'][$i]['small'];
                $j++;
            }
            if ($realty->property['photos'][$i]['medium'] != "")
            {
                $photos[$k]['medium'] = $realty->property['photos'][$i]['medium'];
                $k++;
            }
            if ($realty->property['photos'][$i]['large'] != "")
            {
                $photos[$l]['large'] = $realty->property['photos'][$i]['large'];
                $l++;
            }
        }
        $setting = $realty->settings['widgets']['media'];
        if (stripos($setting['mode'], 'lightbox') !== false && !empty($realty->property['photos'])):
            ?>
            <p class="button"><a class="photo_btn view_photos" href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[photo_gallery]" title="View Photo Gallery">Slide Show</a></p>
            <div style="display:none;">
                <?php
                for ($i = 1; $i < count($photos); $i++)
                {
                    ?>
                    <a href="<?php echo $photos[$i]['large']; ?>" onclick="return false;" rel="prettyPhoto[photo_gallery]"></a>
                <?php } ?>
            </div>
        <?php elseif (stripos($setting['mode'], 'normal') !== false && !empty($realty->property['photos'])): ?>
            <p class="button"><a class="photo_btn" href="<?php echo $realty->pluginUrl . "display/widgets/media.php?property_id=" . $realty->property['id'] . '&amp;settings=normal'; ?>" class="popup_window" rel="width=860, height=800" title="View Photo Gallery">Slide Show</a></p>
        <?php elseif (stripos($setting['mode'], 'ssp_large') !== false && !empty($realty->property['photos'])): ?>
            <p class="button"><a class="photo_btn" href="<?php echo $realty->pluginUrl . "display/widgets/media.php?property_id=" . $realty->property['id'] . '&amp;settings=ssp_large'; ?>" class="popup_window" rel="width=840, height=680" title="View Photo Gallery">Slide Show</a></p>
            <?php
        endif;

        if ($settings['map'] == '1' && !empty($realty->property['latitude']) && !empty($realty->property['longitude']))
        {
            $text = ($settings['display']['map'] == '') ? 'Map' : $settings['display']['map'];
            ?>
            <?php /* <div id="filter_map" class="filter_lightbox"></div>
              <div id="box_map" class="box_lightbox">
              <span id="boxtitle_map" class="boxtitle_lightbox"></span>
              <div id="load_form_map"></div>
              </div> */ ?>
            <?php
            /*
            <p class="button"><a class="map" href="#" onclick="openbox('<?php echo $realty->property['street_address']; ?>', 'filter_map', 'box_map', 'boxtitle_map', 'load_form_map', '<?php echo $realty->pluginUrl . 'display/elements/lightbox_map.php?property_id=' . $realty->property['id']; ?>')" title="<?php echo $text; ?>"><?php echo $text; ?></a></p>
            */
            ?>
            <p class="button btnmap"><a class="map" href="<?php echo $realty->pluginUrl . 'display/elements/property_map.php?property_id=' . $realty->property['id']; ?>&iframe=true&width=610&height=410" rel="prettyPhoto" title="<?php echo $text; ?>"><?php echo $text; ?></a></p>
            <?php
        }

        if ($settings['walkability'] == '1' && !empty($realty->property['street_address']))
        {
            $text = ($settings['display']['walkability'] == '') ? 'Walk' : $settings['display']['walkability'];
            ?>
            <?php /* <div id="filter_walk" class="filter_lightbox"></div>
              <div id="box_walk" class="box_lightbox">
              <span id="boxtitle_walk" class="boxtitle_lightbox"></span>
              <div id="load_form_walk"></div>
              </div> */ ?>
            <?php
            /*
            <p class="button"><a class="walkscore" href="#" onclick="openbox('Walkscore', 'filter_walk', 'box_walk', 'boxtitle_walk', 'load_form_walk', '<?php echo $realty->pluginUrl . 'display/elements/lightbox_walk.php?property_id=' . $realty->property['id']; ?>')" title="<?php echo $text; ?>"><?php echo $text; ?></a></p>
            */
            ?>
            <p class="button btnwalkscore"><a class="walkscore" href="<?php echo $realty->pluginUrl . 'display/elements/property_walk.php?property_id=' . $realty->property['id']; ?>&iframe=true&width=680" rel="prettyPhoto" title="<?php echo $text; ?>"><?php echo $text; ?></a></p>
            <?php
        }

        if ($settings['video'] == '1' && !empty($realty->property['property_video']))
        {
            $text = ($settings['display']['video'] == '') ? 'Video' : $settings['display']['video'];
            ?>
            <?php /* <div id="filter_video" class="filter_lightbox"></div>
              <div id="box_video" class="box_lightbox">
              <span id="boxtitle_video" class="boxtitle_lightbox"></span>
              <div id="load_form_video"></div>
              </div> */ ?>
            <p class="button"><a class="video" href="#"  onclick="openbox('Video', 'filter_video', 'box_video', 'boxtitle_video', 'load_form_video', '<?php echo $realty->pluginUrl . 'display/elements/lightbox_video.php?property_id=' . $realty->property['id']; ?>')" title="<?php echo $text; ?>"><?php echo $text; ?></a></p>
            <?php
        }

        if ($settings['rental'] == '1' && $realty->property['type'] == 'ResidentialLease' && ($realty->property['status'] == '1' || $realty->property['status'] == '4')):
            ?>
            <script type="text/javascript" src="<?php echo $realty->pluginUrl . "display/pages/js/1form.js"; ?>"></script>
            <script language="JavaScript">
                oneformbtn = new OneForm_button(729);
                oneformbtn.papf_realestateco = '<?php echo $realty->property['office']['name']; ?>';
                oneformbtn.papf_realestateag = '<?php
        echo $realty->property['user']['0']['name'];
        if (!empty($realty->property['user']['1']['name']))
            echo '/' . $realty->property['user']['0']['name'];
        ?>';
                oneformbtn.papf_realestatem = '<?php
        echo $realty->property['user']['0']['email'];
        if (!empty($realty->property['user']['1']['email']))
            echo ";" . $realty->property['user']['1']['email'];
        ?>';
                oneformbtn.papf_propid = '<?php echo $realty->property['id']; ?>';
                oneformbtn.papf_propadd = '<?php echo $realty->property['street_address']; ?>';
                oneformbtn.papf_propsub = '<?php echo $realty->property['suburb']; ?>';
                oneformbtn.papf_proppc = '<?php echo $realty->property['postcode']; ?>';
                oneformbtn.papf_propstat = '<?php echo $realty->property['state']; ?>';
                oneformbtn.papf_rent = '<?php echo $realty->property['based_price']; ?>';
                oneformbtn.papf_bond = '<?php $realty->property['bond']; ?>';
                oneformbtn.papf_proptype = '<?php $realty->property['property_type']; ?>';
                oneformbtn.papf_propnobed = '<?php $realty->property['bedrooms']; ?>';
                oneformbtn.newpage = true;
                oneformbtn.display();
            </script>
        <?php endif; ?>	
        <div class="clear"></div>
    </div>

</div>
