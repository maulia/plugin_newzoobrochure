<h4>Sales</h4>
	
<p><label>Number</label> <br><input value="<?php echo $settings['sale']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[sale][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['sale']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[sale][Title]" type="text"></p>


<h4>Rentals</h4>

<p><label>Number</label> <br><input value="<?php echo $settings['lease']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[lease][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['lease']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[lease][Title]" type="text"></p>


<h4>Both</h4>

<p><label>Number</label>
<br><input value="<?php echo $settings['Both']['Number']; ?>" title="*If a value is entered here, then Featured Sales and Featured Rentals will be ignored and the widget will display a combination of the two instead." name="realty_widget_<?php echo $caption; ?>[Both][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['Both']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Both][Title]" type="text"></p>

<h4>Information to Display</h4>
<?php 
$items = array (
  'full_address', 'street_suburb', 'suburb', 'street_address', 'property_type', 'price', 'type', 'bedrooms', 'bathrooms','carspaces','description');
global $helper;
foreach ($items as $key=>$item): ?>
<label><?php echo (empty($settings[$item]['title']) || $settings[$item]['title'] =="1")? $helper->humanize($item):$settings[$item]['title']; ?></label>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][display]" type="checkbox"<?php if($settings[$item]['display']) echo ' checked="checked"'; ?>> 
<input value="<?php echo $settings[$item]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][title]" type="text"></p>
<?php endforeach; ?>