<p><label>Please choose the options that you want to display first in the table.</label> </p>
<input type ="radio" value="map" name="realty_widget_<?php echo $caption; ?>[display]"<?php if($settings['display']=='map') echo ' checked="checked"'; ?>> Map First<br />
<input type ="radio" value="walkscore" name="realty_widget_<?php echo $caption; ?>[display]"<?php if($settings['display']=='walkscore') echo ' checked="checked"'; ?>> Walk Score First<br/><br/>

<p>Below are the options for map</p>
<p><input type ="radio" value="street_view" name="realty_widget_<?php echo $caption; ?>[map_mode]"<?php if($settings['map_mode']=='street_view') echo ' checked="checked"'; ?>> Street View<br />
<input type ="radio" value="normal" name="realty_widget_<?php echo $caption; ?>[map_mode]"<?php if($settings['map_mode']=='normal') echo ' checked="checked"'; ?>> Normal<br/>
<label>Width</label> <br><input value="<?php echo $settings['map_width']; ?>" name="realty_widget_<?php echo $caption; ?>[map_width]" type="text"><br/>
<label>Height</label> <br><input value="<?php echo $settings['map_height']; ?>" name="realty_widget_<?php echo $caption; ?>[map_height]" type="text"><br/>
</p>

<p>Below are the options for walk score</p>
<p><label>Width</label> <br><input value="<?php echo $settings['width']; ?>" name="realty_widget_<?php echo $caption; ?>[width]" type="text"></p>
<p><label>Iframe CSS</label> <br><input value="<?php echo $settings['iframe_css']; ?>" name="realty_widget_<?php echo $caption; ?>[iframe_css]" type="text"></p>
<p><label>Map Frame Color</label> <br><input value="<?php echo $settings['map_frame_color']; ?>" name="realty_widget_<?php echo $caption; ?>[map_frame_color]" type="text"></p>
<p><label>Score Color</label> <br><input value="<?php echo $settings['score_color']; ?>" name="realty_widget_<?php echo $caption; ?>[score_color]" type="text"></p>
<p><label>Headline Color</label> <br><input value="<?php echo $settings['headline_color']; ?>" name="realty_widget_<?php echo $caption; ?>[headline_color]" type="text"></p>
<p><label>Category Color</label> <br><input value="<?php echo $settings['category_color']; ?>" name="realty_widget_<?php echo $caption; ?>[category_color]" type="text"></p>
<p><label>Result Color</label> <br><input value="<?php echo $settings['result_color']; ?>" name="realty_widget_<?php echo $caption; ?>[result_color]" type="text"></p>