<?php
//follow http://brainfart.com.ua/post/lesson-wordpress-multi-widgets/
$realty->element('add_tag_script');
?>

<div id="sales_price_options">
<label>Price Options for Sales</label><br />
<?php realty_add_tag_box('realty_widget_' . $caption. '[sale][]',$settings['sale'] ); ?>
</div>

<div id="lease_price_options" class="clear">
<label>Price Options for Lease</label><br />
<?php realty_add_tag_box('realty_widget_' . $caption. '[lease][]',$settings['lease'] ); ?>
</div>

<p class="clear"><label>Max Beds</label>
<input type="text" name="realty_widget_<?php echo $caption; ?>[max_beds]" size="2" value="<?php echo $settings['max_beds']; ?>" /></p>

<p><label>Max Baths</label>
<input type="text" name="realty_widget_<?php echo $caption; ?>[max_baths]" size="2" value="<?php echo $settings['max_baths']; ?>" /></p>

<p><label>Max Cars</label>
<input type="text" name="realty_widget_<?php echo $caption; ?>[max_cars]" size="2" value="<?php echo $settings['max_cars']; ?>" /></p>