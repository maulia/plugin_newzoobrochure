var rwoMap,rwomaplarge;jQuery(document).ready(function(){
	if(jQuery("#rwomap").length&&jQuery('meta[name="geo.position"]').length){
		rwoMap=new rwoMaps();
		rwoMap.load("rwomap");
		jQuery("#loadLargeMap").colorbox({
			onOpen:function(){
				jQuery("#rwomaplarge").show();
				jQuery("#rwomaplarge").width("100%");
				jQuery("#rwomaplarge").height("100%")},onComplete:function(){rwomaplarge=new rwoMaps();rwomaplarge.load("rwomaplarge","large")}
				,onCleanup:function(){jQuery("#rwomaplarge").hide()}
				,width:(jQuery(window).width()-50),height:(jQuery(window).height()-50),inline:true,href:"#rwomaplarge"}
		);
		jQuery("#loadStreetView").click(function(){rwoMap.toggleStreetView("#loadStreetView")})
	}
});
rwoMaps=function(){};
jQuery.extend(rwoMaps.prototype,{
		load:function(c,b){
		var d=this.getPropertyLatLng();var a=(b=="large")?this.getLargeOptions(d):this.getSmallOptions(d);this.map=new google.maps.Map(document.getElementById(c),a);this.setMarker();this.setPanorama()}
		,getLargeOptions:function(b){
		var a={scrollwheel:false,mapTypeControl:true,overviewMapControl:true,zoomControl:true,panControl:true,streetViewControl:true,zoom:14,center:b,mapTypeId:google.maps.MapTypeId.ROADMAP};return a
		}
		,getSmallOptions:function(b){
		var a={scrollwheel:false,mapTypeControl:false,overviewMapControl:false,zoomControl:false,panControl:false,streetViewControl:false,zoom:13,center:b,mapTypeId:google.maps.MapTypeId.ROADMAP};return a
		}
		,getPropertyLatLng:function(){
		var a=jQuery('meta[name="geo.position"]').attr("content");var b=a.split(";");var d=b[0];var c=b[1];var e=new google.maps.LatLng(d,c);return e
		}				
		,getPropertyPlaceName:function(){
		var a=jQuery('meta[name="geo.placename"]').attr("content");return a
		}
		,setMarker:function(){
		var c=new google.maps.MarkerImage(icon_marker,new google.maps.Size(22,33),new google.maps.Point(0,0),new google.maps.Point(11,16));var e=new google.maps.MarkerImage(shadow_marker,new google.maps.Size(39,33),new google.maps.Point(0,0),new google.maps.Point(11,16));var d=this.getPropertyLatLng();var b=this.getPropertyPlaceName();var a=new google.maps.Marker({position:d,map:this.map,shadow:e,icon:c,title:b,zIndex:1,clickable:false})
		}
		,setPanorama:function(){
		this.panorama=this.map.getStreetView();this.panorama.setPosition(this.map.getCenter());this.panorama.setPov({heading:265,zoom:1,pitch:0})
		}
		,toggleStreetView:function(c){
		try{this.panorama.setPosition(this.map.getCenter())}catch(b){}var a=this.panorama.getVisible();if(a==false){try{this.panorama.setVisible(true)}catch(b){}jQuery(c).attr("value","Map View");jQuery(c).attr("class","MapView")}else{try{this.panorama.setVisible(false)}catch(b){}jQuery(c).attr("value","Street View");jQuery(c).attr("class","StreetView")}
		}
});