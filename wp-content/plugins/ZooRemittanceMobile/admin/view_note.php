<div class="wrap">	
	<h2>Notes For Contact <?php echo $subscribers['first_name'] . ' ' . $subscribers['last_name']; ?></h2>
	<?php if(!empty($details)): ?>
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col"><?php _e('Delete') ?></th>
			<th>Date</th>
			<th>Note Type</th>
			<th>Property Address</th>
			<th>Note</th>
		</tr>
		</thead>
		<?php $i=0;foreach($details as $detail){ $i++; ?>
		<tr <?php if(i%2==0)echo 'class="alternate"'; ?>>
			<td>
			<a href="<?php echo $this->form_action;?>&amp;task=delete_detail&amp;id=<?php echo $detail['id']; ?>" onClick="return confirm('You are about to delete the this note.\n\'OK\' to delete, \'Cancel\' to stop.' );">Delete</a>
			</td>
			<td><?php echo date("d-m-Y", strtotime($detail['data_added'])); ?></td>
			<td><?php 
			switch($detail['note_type']){
				case '2':$detail['note_type']='Sales Appraisal';break;
				case '3':$detail['note_type']='Rental Appraisal';break;
				case '4':$detail['note_type']='Alert- Created';break;
				case '6':$detail['note_type']='Property Enquiry';break;
				case '13':$detail['note_type']='General';break;			
			}
			echo $detail['note_type']; ?>
			
			</td>
			<td><?php if(!empty($detail['property_id'])){ ?><a href="<?php echo get_option('siteurl').'/'.$detail['property_id']; ?>"><?php } ?>
				<?php echo $detail['address']; ?>
				<?php if(!empty($detail['property_id'])){ ?></a><?php } ?>
			</td>
			<td><?php echo $detail['note']; ?></td>
		</tr>
		<?php } ?>
	</table> 
	<?php else : echo '<p>There are no notes for this contact</p>';	endif ; ?>
	<p><a href="<?php echo $this->form_action;?>"><?php _e('&laquo; Back To Main Page'); ?></a></p>
</div>