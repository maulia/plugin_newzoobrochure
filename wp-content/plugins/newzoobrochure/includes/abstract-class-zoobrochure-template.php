<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * Abstract class for createing template pdf
 * 
 * @author Jony <jony@agentpoint.com.au>
 * @package zoobrochure
 * @subpackage zoobrochure_template
 * @since 2.0.0
 */
abstract class zoobrochure_template {

    public $pdf;
    public $orientation = 'P';
    public $unit = 'mm';
    public $size = 'A4';
    public $property = [];
    public $setting = [];
    public $property_id;
    public $current_template;

    /**
     * We did all preparation in here
     * main variable in here is $this->proeprty and $this->current_template
     * if both of those template is empty, pdf will not run properly
     * 
     * @since 2.0.0
     */
    public function __construct($property = [], $setting = []) {
        // Create single blank PDF page
        $this->pdf = new zoobrochure_pdf($this->orientation, $this->unit, $this->size);

        // Fill var property and setting
        $this->property = $property;
        $this->setting = $setting;        
    }

    /**
     * Used to render PDF page
     * All pdf code should be placed under this method on sub class
     * without this method, pdf only display blank page or no display at all
     * 
     * @since 2.0.0
     * @return void
     */
    public function render() {
        
    }

    /**
     * Call all defined code and make it as PDF
     * 
     * @since 2.0.0
     * @return void
     */
    public function __output() {
        $name = !empty($this->property) ? $this->property['street_address'] . ' ' . $this->property['suburb'] . ' ' . $this->property['state'] : __('Brochure');
        $this->pdf->Output(sprintf('%s.pdf', apply_filters( 'zoobrochure_pdf_filename', $name )), 'I');
    }

}
