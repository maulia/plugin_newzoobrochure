<?php
global $helper;
$realty_settings=get_option('realty_general_settings');
$office_id=implode(",",$realty_settings['office_id']);

$request = $_GET + $_POST;
extract($_POST);
$offices=$helper->get_results("select id, name from office where id in ($office_id)");
$temp_office=array();
foreach($offices as $office){
	extract($office);
	$temp_office[$id]=$name;
}
$offices=$temp_office;

if($request['action'] =='edit' && !empty($request['user_id'])):// If the form is being updated
	$id=$request['user_id'];
	$users=$helper->get_results("select * from users where id=$id");
	$user = $users[0];
	$user['landscape']=$helper->get_var("select url from attachments where parent_id=$id and type='landscape' and description='large' and position='0'");
	$user['portrait']=$helper->get_var("select url from attachments where parent_id=$id and type='portrait' and description='large' and position=0");
	$user['testimonials']=$helper->get_results("select id, user_name, content from testimonials where user_id=$id order by user_name");
	include('admin-team-edit.php');
elseif($request['action'] =='add_new'): 
	include('admin-team-new.php');
else:			
	include('admin-team-list.php');
endif;
?>