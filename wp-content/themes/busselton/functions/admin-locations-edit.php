<script type="text/javascript">
function openhide(divcontent){
	jQuery("#"+divcontent).slideToggle("slow");
}
</script>
 <div class="leftside">	
	<form action="" method="post">
		<input type="hidden" name="suburb_id" value="<?php echo $_GET['suburb_id']; ?>" />
		<input type="hidden" name="task" value="<?php echo $task; ?>" />
		<input type="hidden" id="user-id" name="user_ID" value="<?php global $user_ID; echo (int) $user_ID ?>" />
		<table class="widefat maintable" cellspacing="0">
			<thead>
			<tr>
				<th colspan="2" scope="col" class="manage-column column-title">Location Information</th>
			</tr>
			</thead> 
			<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Suburb') ?></label></th>
				<td><input type="text" name="suburb" id="suburb" value="<?php echo $suburb; ?>" /></td>
			</tr>
				<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Population') ?></label></th>
				<td><input type="text" name="population" id="population" value="<?php echo $population; ?>" /></td>
			</tr>
				<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Postcode') ?></label></th>
				<td><input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" /></td>
			</tr>
				<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Council') ?></label></th>
				<td><input type="text" name="council" id="council" value="<?php echo $council; ?>" /></td>
			</tr>
				<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Link') ?></label></th>
				<td><input type="text" name="link" value="<?php echo $link ?>" /></td>
			</tr>
		</table>		
		<div class="clear"></div>
		<h4><?php _e('Description') ?></h4>
		<?php
		$args = array("textarea_name" => "description");
		wp_editor( $description, "content", $args );
		?>
		<p class="submit"><input name="save" type="submit" value="Save changes" /></p>

	</form>

	<form action="" method="post" name="amenity_form">
		<input type="hidden" name="hidden_amenity_id" id="amenity_id" value="" /><!--for deletion purposes only-->
		<input type="hidden" name="task" value="delete_amenity" />
		<table class="widefat maintable" cellspacing="0">
			<thead>
			<tr>
				<th colspan="2" scope="col" class="manage-column column-title">Points of Interest</th>
			</tr>
			</thead> 
			<tr>
				<th colspan="2">To create a new point of interest simply fill in the fields below and save.</th>
			</tr>
			<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Point of Interest') ?></label></th>
				<td>
					<input type="hidden" name="amenity_id[]" value="<?php echo $amenity['amenity_id'] ?>" />
					<select name="type[]">
					<option value="">Please select one</option>
						<option value="cafe" <?php if("cafe" == $amenity['type']) echo 'selected="selected"'; ?>>Cafes & Restaurants</option>						
						<option value="landmark" <?php if("landmark" == $amenity['type']) echo 'selected="selected"'; ?>>Landmarks</option>								
						<option value="worship" <?php if("worship" == $amenity['type']) echo 'selected="selected"'; ?>>Places of Worship</option>
						<option value="school" <?php if("school" == $amenity['type']) echo 'selected="selected"'; ?>>School & Univ</option>
						<option value="shopping" <?php if("shopping" == $amenity['type']) echo 'selected="selected"'; ?>>Shops</option>
						<option value="sport" <?php if("sport" == $amenity['type']) echo 'selected="selected"'; ?>>Sporting Facilities</option>
					</select>
				</td>
			</tr>
			<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Name') ?></label></th>
				<td><input type="text" name="amenity[]" value="<?php echo $amenity['amenity'] ?>" /></td>
			</tr>
			<tr class="form-field">
				<th valign="top"  scope="row"><label><?php _e('Link') ?></label></th>
				<td><input type="text" name="link[]" value="<?php echo $amenity['link']; ?>" /></td>
			</tr>
		</table>		
		
		<?php $amenities['new'] = true; 
		foreach($amenities as $key=>$amenity){
			if(is_array($amenity)){ ?>		
				<h3>
					<a href="javascript:openhide('amenity_<?php echo $key; ?>')">+ &raquo;&nbsp;&nbsp;</a><?php echo $amenity['amenity'] . ' ( ' . $amenity['type'] . ' )'; ?>
				</h3>
				<table class="widefat maintable" cellspacing="0" id="amenity_<?php echo $key; ?>" style="display:none;">
					<tr class="form-field">
						<th valign="top"  scope="row"><label><?php _e('Point of Interest') ?></label></th>
						<td>
							<input type="hidden" name="amenity_id[]" value="<?php echo $amenity['amenity_id'] ?>" />
							<select name="type[]">
							<option value="">Please select one</option>
								<option value="cafe" <?php if("cafe" == $amenity['type']) echo 'selected="selected"'; ?>>Cafes & Restaurants</option>						
								<option value="landmark" <?php if("landmark" == $amenity['type']) echo 'selected="selected"'; ?>>Landmarks</option>								
								<option value="worship" <?php if("worship" == $amenity['type']) echo 'selected="selected"'; ?>>Places of Worship</option>
								<option value="school" <?php if("school" == $amenity['type']) echo 'selected="selected"'; ?>>School & Univ</option>
								<option value="shopping" <?php if("shopping" == $amenity['type']) echo 'selected="selected"'; ?>>Shops</option>
								<option value="sport" <?php if("sport" == $amenity['type']) echo 'selected="selected"'; ?>>Sporting Facilities</option>
							</select>
						</td>
					</tr>
					<tr class="form-field">
						<th valign="top"  scope="row"><label><?php _e('Name') ?></label></th>
						<td><input type="text" name="amenity[]" value="<?php echo $amenity['amenity'] ?>" /></td>
					</tr>
					<tr class="form-field">
						<th valign="top"  scope="row"><label><?php _e('Link') ?></label></th>
						<td><input type="text" name="link[]" value="<?php echo $amenity['link']; ?>" /></td>
					</tr>
					<tr class="form-field">
						<td colspan="2">
						<p class="submit"><a onClick="if(confirm('You are about to delete \'<?php echo $amenity['amenity']; ?>\'.\n\'OK\' to delete, \'Cancel\' to stop.' )){onclick=document.getElementById('amenity_id').value='<?php echo $amenity['amenity_id']; ?>';document.amenity_form.submit();}" class="button-secondary delete"><?php _e('Delete') ?></a></p>
						</td>
					</tr>
				</table>
			<?php }
			$alternate = (empty($alternate))? 'alternate':'';++$count; 
		} ?>	
		<p class="submit"><input name="save_amenities" type="submit" value="Save changes" /></p>
	</form>
	
	<?php
	$themes_folder=get_bloginfo('stylesheet_directory') .'/';
	$uploaded_path=ABSPATH . 'wp-content/uploads/LocationInfoPlugin/';
	?>
	<link rel="stylesheet" href="<?php echo $themes_folder;?>uploadify/uploadify.css" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $themes_folder; ?>uploadify/swfobject.js"></script>
	<script type="text/javascript" src="<?php echo $themes_folder; ?>uploadify/jquery.uploadify.v2.1.4.js"></script>
	<script type="text/javascript">
	var uploader='<?php echo $themes_folder; ?>uploadify/uploadify.swf';
	var script_photo='<?php echo $themes_folder; ?>uploadify/uploadify_location_photo.php';
	var photos_upload_path='<?php echo $uploaded_path; ?>';
	var load_photo='<?php echo $themes_folder."uploadify/load_location_photo.php?suburb_id=".$_GET['suburb_id']."&key=";?>';
	var button_image='<?php echo $themes_folder; ?>uploadify/select_files.png';
	var button_width='110';
	var button_height='26';
	var suburb_id="<?php echo $_GET['suburb_id']; ?>";
	var cancel_image='<?php echo $themes_folder; ?>uploadify/cancel.png';
	jQuery(function() {
		jQuery('#photo_upload').uploadify({
		'uploader'       : uploader,
		'script'         : script_photo,
		'buttonImg'      : button_image,
		'width'          : button_width,
		'height'         : button_height,
		'cancelImg'      : cancel_image,
		'rollover'       : true,
		'folder'         : photos_upload_path,
		'scriptData'  	 : {'suburb_id':suburb_id},
		'multi'          : true,
		'auto'           : true,
		'fileExt'        : '*.jpg;*.gif:;*.png',
		'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
		'queueID'        : 'photo_queue',
		'queueSizeLimit' : 30,
		'simUploadLimit' : 30,
		'removeCompleted': true,
		'onSelect'   : function(event,data) {
			jQuery('#photo_message').text(data.filesSelected + ' files have been added to the queue.');
		},
		'onComplete'  : function(event, ID, fileObj, response, data) {
			jQuery('#photo_list').load(load_photo);
		},
		'onAllComplete'  : function(event,data) {
			jQuery('#photo_message').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
		},
		'onQueueFull'    : function (event,queueSizeLimit) {
			jQuery('#photo_message').text("Maximum upload of photos is 30.");
			return false;
		}
		});		
	}); 
	function delete_photo(key){
		jQuery('#photo_list').load(load_photo+key);
	}
	
	</script>
	<table class="widefat maintable" cellspacing="0">
		<thead>
		<tr>
			<th colspan="3" scope="col" class="manage-column column-title">Images</th>
		</tr>
		</thead> 
		<tr>
			<td colspan="3">
				<ul class="image_list" id="photo_list">
				<?php 			
				if(!empty($photos)){ 
					foreach($photos as $key=>$photo):
					if(!empty($photo['path'])){ ?>
					<li>
						<img src="<?php echo $LocationInfo->get_thumbnail($photo['path']); ?>"/>
						<a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="javascript:delete_photo('<?php echo $photo['photo_id']; ?>');" title="Delete Image"></a>
					</li>
					<?php 
					} endforeach; 

				}	?>

				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="photo-upload-box">
					<div id="photo_message" class="upload_message">Select some files to upload:</div>
					<div id="photo_queue" class="custom-queue"></div>
					<div class="photo-upload-btn"><input id="photo_upload" type="file" name="Filedata" /></div>
				</div>
			</td>
		</tr>
	</table>	
</div>
		
<div id="poststuff">
	<div class="postbox">
		<h3>Actions</h3>		
		<div class="inside">
			<p class="submit">
			<a href="themes.php?page=theme-settings&tab=locations" class="button">Back</a>
			</p>			
		</div>	
	</div>
</div>