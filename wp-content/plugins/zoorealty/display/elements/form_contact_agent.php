<?php
$user_id = (empty($params['user_id']))? $_GET['user_id']: $params['user_id'];
$user_id1 = (empty($params['user_id1']))? $_GET['user_id1']: $params['user_id1'];
if ($params['popup']): ?>
<p id="agent_contact_form"><a class="email_alert" target="_blank" href="<?php echo $this->path(__FILE__, 'url') . "?user_id=$user_id&amp;popup=".$params['popup']; ?>" title="Contact Agent", rel="width=500, height=500">Contact Agent</a></p>
<?php 
	return; 
endif; 
if($_GET['popup']): 
	require_once('../../../../../wp-blog-header.php');
	extract($_GET);
?>

<html>
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" title="Ginga" />
<title><?php bloginfo('name'); ?> - Contact Agent</title>
<?php wp_print_scripts('jquery'); ?>
</head>
<body id="email_subscribe">
<div id="main_body">
<?php endif; //$_GET['popup') 
if (get_option('spam_filter')=='1')require_once('cryptographp.php');
?>

<div id="formpart">
	<h2>Contact Agent</h2>
	<div id="return"></div>
	<form name="" method="post" action="#contact_agent_form" class="contact_agent_form">
		<ol class="cf-ol">
			<li><label><span>First Name:</span></label>
			<input type="text" class="textbox" id="first_name" name="first_name" value="<?php echo $_POST['first_name']; ?>" /></li>
			<li><label><span>Last Name:</span></label>
			<input type="text" class="textbox" id="last_name" name="last_name" value="<?php echo $_POST['last_name']; ?>" /></li>	
			<li><label><span>Telephone:</span></label>
			<input type="text" class="textbox" id="home_phone" name="home_phone" value="<?php echo $_POST['home_phone']; ?>" /></li>
			<li><label><span>Mobile:</span></label>
			<input type="text" class="textbox" id="mobile_phone" name="mobile_phone" value="<?php echo $_POST['mobile_phone']; ?>" /></li>
			<li><label><span>Email:</span></label>
			<input type="text" class="textbox" id="email" name="email" value="<?php echo $_POST['email']; ?>" /></li>
			<li><label><span>How did you find us?</span></label>
			<select name="referrer" id="referrer"  style="font-size:0.8em;">
					<option value="">Please select</option>
				<?php global $realty;
				$all_list=$realty->all_list_referrers();
				foreach($all_list as $item_value): ?>
					<option value="<?php echo $item_value['name']; ?> <?php if($_REQUEST['referrer']==$item_value['name'])echo 'selected="selected"'; ?>"><?php echo $item_value['name']; ?></option>
				<?php endforeach; ?>
			</select></li>
			<li class="contact_message"><label><span>Comments</span></label>
			<textarea name="comments" id="comments" cols="22" rows="3" onClick="if(this.value=='Comments') this.value='';"><?php echo (empty($_POST['comments']))? "Comments" : $_POST['comments']; ?></textarea></li>
		
		<?php if (get_option('spam_filter')=='1'):?>
		<li><label><span>Spam Code</span></label><?php display_cryptographp(); ?></li>
		<?php endif; ?>
		<li><label><span>Subscribe to be kept informed?</span></label><input type="checkbox" class="checkbox" id="keep_informed" name="keep_informed" value="1" <?php if($_POST['keep_informed']=='1') echo "checked";?> /></li>
		</ol>

		<input type="hidden" id="property_id" value="<?php echo  ($realty->property['id']=='')?$_GET['property_id']:$realty->property['id']; ?>" />
		<p class="button submit_btn"><!--<input type="submit"  name="submit" value="submit" class="btn" />--><a href="" class="btn" id="contact_agent_submit">Submit</a></p>
		<p class="requi">All fields required.</p>
	</form>
	<div class="clearer"></div>
</div><!-- end #formpart -->
<?php if($_GET['popup']): ?>
</body>
</html>
<?php endif; ?>

<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery("#contact_agent_submit").click(function(){
	var code = jQuery("#securitycode").val();
	var comments = document.getElementById('comments').value;	
	var first_name = jQuery("#first_name").val();
	var last_name = jQuery("#last_name").val(); 
	var email = jQuery("#email").val();
	var mobile_phone = jQuery("#mobile_phone").val();
	var home_phone = jQuery("#home_phone").val();
	if(document.getElementById('keep_informed').checked)var keep_informed = jQuery("#keep_informed").val();else keep_informed = '0';
	var property_id = jQuery("#property_id").val();
	var property_address = jQuery("#property_address_sub").val();
	if(first_name=='First Name' || first_name=='')alert("You have entered an invalid first name");
	else if(last_name=='Last Name' || last_name=='')alert("You have entered an invalid last name");
	else if(home_phone=='Telephone' || home_phone=='')alert("You have entered an invalid telephone");
	else if(mobile_phone=='Mobile' || mobile_phone=='')alert("You have entered an invalid mobile");
	else if ((email.indexOf('@') < 0) || ((email.charAt(email.length-4) != '.') && (email.charAt(email.length-3) != '.')))alert("You have entered an invalid email address. Please try again.");
	else{
	<?php if (get_option('spam_filter')=='1'):?>
	var url    = "<?php echo $realty->pluginUrl; ?>display/pages/js/contact_agent.php?code="+escape( code )+"&comments="+escape( comments )+"&keep_informed="+escape( keep_informed )+"&first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&mobile_phone="+escape( mobile_phone )+"&home_phone="+escape( home_phone )+"&property_id="+escape( property_id )+"&user_id=<?php echo $user_id; ?>&user_id1=<?php echo $user_id1; ?>";
	<?php else: ?>
	var url    = "<?php echo $realty->pluginUrl; ?>display/pages/js/contact_agent.php?comments="+escape( comments )+"&keep_informed="+escape( keep_informed )+"&first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&mobile_phone="+escape( mobile_phone )+"&home_phone="+escape( home_phone )+"&property_id="+escape( property_id )+"&user_id=<?php echo $user_id; ?>&user_id1=<?php echo $user_id1; ?>";
	<?php endif; ?>
	jQuery('#return').load(url);
	alert('Email was successfully sent.'); 
	}	
	return false; 
	});	
});
</script>