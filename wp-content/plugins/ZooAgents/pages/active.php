<?php
global $helper, $current_user, $realty, $wpdb;
if (!is_user_logged_in()){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }
include('menu.php');

if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1; }

if(!$office_id)return;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];
$type=$_GET['type'];
$office_id=intval($_GET['office_id']);

$property=$wpdb->get_results("select * from properties where id = $id $extra_condition", ARRAY_A);
$property=$property[0];
if(empty($property))return print "Listing ID $id is not exist";

include('menu_listings.php');

if(isset($_POST['submit'])){
	$_POST['updated_at']=date("Y-m-d h:i:s");
	if(!empty($_POST['expire_at']))$_POST['expire_at']=date("Y-m-d", strtotime($_POST['expire_at']));
	if($property['active']!='Active' && $_POST['active']=='Active' && $_POST['expire_at']!=$property['expire_at']){ // activate, but the expire date not change
		$_POST['expire_at']=$this->add_date(date("Y-m-d"),30);
	}
	
	if($this->update_form( "properties", $_POST ))echo "<div id='success'><span>Updated</span></div>";
	else echo "<div id='return' class='red'>Update failed. please try again later</div>";
	
	$property=$_POST;	
}
?>
<script language="JavaScript" src="<?php echo $this->pluginUrl; ?>js/base_datepicker.js"></script>

<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div id="active_div" class="block_content">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<table>
			<tr>				
				<td width="80px"><input type="radio" name="active" <?php if ($property['active']=='Active') echo 'checked="checked"'; ?> value="Active">Active</td>
				<td width="80px"><input type="radio" name="active" <?php if ($property['active']=='Expired') echo 'checked="checked"'; ?> value="Expired">Expired</td>
				<td width="80px"><input type="radio" name="active" <?php if ($property['active']=='Suspended') echo 'checked="checked"'; ?> value="Suspended">Suspended</td>				
				<td width="80px"><input type="radio" name="active" <?php if ($property['active']=='Audit') echo 'checked="checked"'; ?> value="Audit">Audit</td>
				<td width="80px"><input type="radio" name="active" <?php if ($property['active']=='Inactive') echo 'checked="checked"'; ?> value="Inactive">Inactive</td>
			</tr>			
		</table>
		<table id="expire_detail">
			<tr>
				<td>&nbsp;</td>
				<td class="label">Expire Date:</td>
				<td><input type="text" id="expire_at" name="expire_at" class="full" value="<?php if(!empty($property['expire_at']) && $property['expire_at']!='0000-00-00')echo date("d-m-Y",strtotime($property['expire_at'])); ?>"></td>
			</tr>
		</table>
	</div>	
	<div class="block_content">
		<input type="submit" name="submit" value="Save" class="btn" id="submit_btn" >
		<a href="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id']; ?>" class="btn"  id="next_btn">Next</a>
	</div>
	<p>Note: If you don't make any change to `Expire Date` while activate the property, the `Expire Date` will automatically change 30 days from now.</p>
	<div class="clearer"></div>
</form>


<script type="text/javascript">
jQuery(document).ready(function () {
  jQuery('#expire_at').datepicker({dateFormat:'dd-mm-yy'});    
});	
</script>