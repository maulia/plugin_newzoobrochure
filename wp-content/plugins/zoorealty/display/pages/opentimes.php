<?php
if($this->params['list']!=""){
	$opendates = $this->opendates($this->params['list']);
}
else{
	$opendates = $this->opendates($_GET['list']);
}
if(empty($opendates)): ?><p class="return">No properties are available for viewing. Please check again later.</p><?php
else: ?>
<?php if($this->settings['general_settings']['display_pdf_button_in_search_result']=='1'){ ?>
<p class="pdf btn"><a href="javascript:void(0)" onclick="return window.open('<?php echo $this->pluginUrl."display/elements/search_brochure.php?opentimes=1&list=".$this->params['list']; ?>', 'Brochure', 'width=820, height=960,resizable=1,scrollbars=1'); return false;" title="Print Results" class="btn">PDF</a></p>
<?php }
	$count = 0;
	foreach($opendates as $date=>$times):
		$count++; /* Array of dates */ 
?>
<table cellspacing="0" id="date_container_<?php echo $count; ?>" class="date_container" summary="Open Times for <?php echo $date; ?>">
	<tr>
		<th colspan="6" class="opentimes_date"><?php echo date('l jS F Y', strtotime($date)); ?></th>
	</tr>
	<?php
		foreach($times as $property):/* Array of properties within each time */
		//$opentimes = $this->opentimes($property['id']," AND opentimes.date='$date'");
?>
	<tr<?php echo $odd_class = empty($odd_class) ? ' class="alt"' : ''; ?>>
		<td class="time_cell">
			<?php $start_date=$date." ".$property['start_time'];	$end_date=$date." ".$property['end_time'];	?>				
			<a href="<?php echo $this->pluginUrl."display/elements/crm.php?property_id=".$property['id']."&start_date=$start_date&end_date=$end_date"; ?>" title="Add to Calendar" class="calendar">cal</a>
			<span>
			<?php 
				if($property['start_time']=='00:00:00')$start_time='12:00am';else $start_time=date('g:ia', strtotime($property['start_time']));
				if($property['end_time']=='00:00:00')$end_time='12:00pm';else $end_time=date('g:ia', strtotime($property['end_time']));
				echo $start_time . ' - ' .$end_time. "<br />";  
			?>
			</span>
		</td>
		<td class="suburb_cell"><span><?php echo $property['suburb']; ?></span></td>
		<td class="address_cell"><span><a title="View Property" href="<?php echo $property['url']; ?>"><?php echo ($property['street_address']=='')?'(no data provided)':$property['street_address']; ?></a></span></td>
		<td class="property_type_cell"><span><?php echo $property['property_type']; ?></span></td>
		<td class="bedrooms_cell"><span><?php echo $property['bedrooms'].'B'; ?></span></td>
		<?php if(!empty($property['price'])) { ?><td class="price_cell"><span><?php echo $property['price']; ?></span></td><?php } ?>
	</tr>
<?php 
	
endforeach; ?>
</table><!--date_container_-->
<?php endforeach; // properties_date ?>
<?php endif;//opentimes is empty?>