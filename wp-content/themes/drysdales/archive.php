<?php get_header(); ?>

<div id="content">
	
	<?php if (have_posts()) : ?>

	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
    <?php /* If this is a category archive */ if (is_category()) { ?>
    	<h1 class="page-title">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
    <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
    	<h1 class="page-title">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
    <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
    	<h1 class="page-title">Archive for <?php the_time('F jS, Y'); ?></h1>
    <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
    	<h1 class="page-title">Archive for <?php the_time('F, Y'); ?></h1>
    <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
    	<h1 class="page-title">Archive for <?php the_time('Y'); ?></h1>
    <?php /* If this is an author archive */ } elseif (is_author()) { ?>
    	<h1 class="page-title">Author Archive</h1>
    <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
    	<h1 class="page-title">Blog Archives</h1>
    <?php } ?>
    
    <div id="blog-div" class="default-post posts-index archive">

		<?php while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<?php /*if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; }*/ ?>
            <div class="post_title_date<?php if(!function_exists('userphoto_the_author_thumbnail')) { echo ' no_author_thumb'; } ?>">
                <h2 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            </div>

            <div class="entry">
				<?php //$output=preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
                //$first_img=$matches [1][0]; ?>
                <?php the_excerpt_reloaded(30, '<a>', 'excerpt', TRUE, 'Read more <span class="aquo">&#187;</span>'); ?>
                <?php /*global $wpdb, $table_prefix; $plugin_table=$table_prefix . "property_ads"; $q=$wpdb->get_results("select * from $plugin_table where post_id=".$id."");
                if(!empty($q)){ ?>
                    <p class="post-button"><a class="btn" href="<?php echo get_option('siteurl')."/".$q[0]->property_id;?>">View <?php echo $q[0]->street.", ".ucwords(strtolower($q[0]->suburb)); ?> </a></p>
                    <div class="clear"></div>
                <?php }*/ ?>
            </div>
            
            <div class="post_data"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> on <?php the_time('jS F, Y') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments <span class="aquo">&#187;</span>', '1 Comment <span class="aquo">&#187;</span>', '% Comments <span class="aquo">&#187;</span>'); ?></div>

        </div>

		<?php endwhile; ?>

		<div class="navigation">
			<p class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></p>
			<p class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
		</div>

	<?php else :

		if ( is_category() ) { // If this is a category archive
			printf("<h5 class='center'>Sorry, but there aren't any posts in the %s category yet.</h5>", single_cat_title('',false));
		} else if ( is_date() ) { // If this is a date archive
			echo("<h5>Sorry, but there aren't any posts with this date.</h5>");
		} else if ( is_tag() ) { // If this is a tag archive
			echo("<h5>Sorry, but there aren't any posts with this tag.</h5>");
		} else if ( is_author() ) { // If this is a category archive
			$userdata = get_userdatabylogin(get_query_var('author_name'));
			printf("<h5 class='center'>Sorry, but there aren't any posts by %s yet.</h5>", $userdata->display_name);
		} else {
			echo("<h5 class='center'>No posts found.</h5>");
		}
		get_search_form();

	endif;
?>

	</div><!-- end #blog-div -->
</div><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
