<?php 
$listings = $params; 
global $favourite,$realty,$helper,$wpdb;
$src=str_replace('wp-content/plugins/Realty/style/stickers/','',$realty->settings['property_stickers']['sticker_src']);
$style="style".(str_replace("sticker","",str_replace(".png","",$src))+1);
switch($realty->settings['property_stickers']['sticker_colour']){
case '#2b497d';$colors='dk_blue';break;
case '#2b7d36';$colors='dk_green';break;
case '#857a82';$colors='grey';break;
case '#70c141';$colors='lite_green';break;
case '#4cb2c2';$colors='lite_blue';break;
case '#d78c23';$colors='orange';break;
case '#7e2a1d';$colors='red';break;
case '#7d5f2b';$colors='tan';break;
case '#d7ce31';$colors='yellow';break;
}

?>
<?php global $realty;if($realty->settings['general_settings']['display_search_result_in_thumbnail_format']=='1'):?>
<!-- thumbnail format -->
<div id="thumbnail_format" class="grid-max-3 thumbview" <?php if($realty->settings['general_settings']['display_search_result_in_thumbnail_format']!='1') echo 'style="display:none;"';?>>
<?php $i=0;	
foreach($listings as $listing):
	if(!is_array($listing)) continue;
	if ($count % 3==0){
	?>
		<div class="listing column">
	<?php 
	} 
	if(is_array($realty->settings['search_results_thumbnail'])) ?>
	
		<div class="container">		
			 <?php /* <div class="item-wrap"> */ ?>
			 <div id="outside"> 
                <div class="image cycle-slideshow" 
                	data-cycle-fx=scrollHorz
    				data-cycle-timeout=0
    				data-cycle-prev="#prev-<?php echo $count?>"
        			data-cycle-next="#next-<?php echo $count?>"
    				>
    				<span class="inbox-border"><span></span></span>
                	<?php $listing_photos = $realty->property( $listing['id'] );
                	$photos = $listing_photos['photos']; 
                	//echo '<pre>'; print_r($listing_photos); echo '</pre>'; ?>                	
					<?php /* <a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>"> */ ?>
						<?php /*<img src="<?php echo $listing['medium']; ?>" alt="<?php echo $listing['headline']; ?>"  /> */ ?>
						<?php foreach ($photos as $key => $photo) { ?>
							<img class ="slide" src="<?php echo $photo['large'] ?>" title="<?php echo $listing['headline'] ?>">	
						<?php
						} ?>
                    <?php /* </a> */ ?>
						<?php /*  if ($listing['new']):?>
						<div class="fresh_place">
						</div>
						<?php*/ if ($listing['is_sold']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker sold ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo /*$realty->settings['property_stickers']['sticker_text']['sold']; */"SOLD";?></span>
							</div>
						</div>
						<?php elseif ($listing['is_leased']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker leased ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo /*$realty->settings['property_stickers']['sticker_text']['leased']*/"LEASED";?></span>
							</div>
						</div>
						<?php elseif ($listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker under_offer ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo /*$realty->settings['property_stickers']['sticker_text']['under_offer']*/"UNDER OFFER";?></span>
							</div>
						</div>
						<?php elseif (!empty($listing['is_open_home']) && !$listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker open_home ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo ($realty->settings['property_stickers']['sticker_text']['open_home']=='')?'Open Home':$realty->settings['property_stickers']['sticker_text']['open_home'];?></span>
							</div>
						</div>
						<?php endif;  ?>
                </div>
                <div class="center">
        			<span id="prev-<?php echo $count?>">&lt;&lt;Prev </span>
        			<span id="next-<?php echo $count?>"> Next&gt;&gt;</span>
    			</div>
            </div>
            <div class="tdContent">
            <?php	
			foreach($realty->settings['search_results_thumbnail'] as $key=>$values):
				if($key=='thumbnail') continue;
                switch($key):
                    case 'address':
						if(is_array($values)) foreach($values as $addressPart)${$addressPart} = $listing[$addressPart];
						$address='';
						if(!empty($street_address))$address=$street_address.", ";
						if(!empty($suburb))$address.=$suburb.", ";
						if(!empty($state))$address.=$state.", "; ?>
						<p class="suburb"><a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>"><?php echo substr($address,0,-2) ;?></a></p>
					<?php						
                    break;
                    case 'description': ?>
					<p class="description">
						<?php echo $helper->_substr($listing['description'], $values); ?>&hellip;
						<a href="<?php echo $listing['url']; ?>" title="View Property">More &raquo;</a>
					</p>
					<?php
                    break;
                    case 'building_size':
						if(!empty($listing[$key])): ?>
						<div class="land_building_size">
							<p>Building: <span class="building_size"><?php echo $listing[$key]." ".$listing['floor_area_metric']; ?></span></p>
						</div>
						<?php
                        endif;				
                    break;
                    case 'land_size':						
                        if(!empty($listing[$key])): ?>
							<div class="land_building_size">
								<p class="land_size">LAND: <span class="land_size"><?php echo $listing[$key]." ".$listing['land_area_metric']; ?></span></p>
							</div>
						<?php
                        endif;                              
                    break;
                    case 'auction':
						if (!empty($listing[$key]) && $listing[$key]!='00:00:00'): ?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php
						endif;
                    break;
                    case 'number_of_rooms': 
                        if ( !$listing['is_land'] and is_array($values) and !empty($values)): ?>
							<ul class="rooms">
							<?php  
							foreach($values as $param):
								if(!empty($listing[$param])):		 ?>	
								<li class="<?php echo $param; ?>">
									<span class="room_count"><?php echo $listing[$param]; ?></span> <span class="room_type"><?php echo $param; ?></span>
								</li>
								<?php
								endif;
							endforeach; ?>
							</ul>
							<?php 
                        endif;
                    break;
                    case 'primary_contact': 
                        $user = $realty->user($listing['user_id']);
                        if(!empty($user)):  ?>
						<p>Contact</p>
						<?php
                        foreach($values as $param):
							if(!empty($user[$param])):?>
							<p>
								<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $user[$param]; ?>"><?php echo $user[$param]; ?></a>
								<?php else: ?>
								<?php echo $user[$param]; ?>
								<?php endif; ?>
								</span>
							</p>
							<?php
                            endif;
                            endforeach;
                        endif;						
                    break;
                    case 'opentimes':
                        $opentimes = $this->opentimes($listing['id']); 
                        if(!empty($opentimes)) : ?>	
							<p><span>Open</span> Times</p>
							<ul>
							<?php foreach($opentimes as $opentime): ?>
								<li><span class="open_date"><strong>
								<?php echo date('l, j M Y', strtotime($opentime['date'])); ?></strong></span><br />
								<span class="open_time"><?php echo $opentime['start_time']; ?> - <?php echo $opentime['end_time']; ?></span></li>
							<?php endforeach; ?>
							</ul>
						<?php
                        endif;
                    break;
                    case 'available_at':
                        if(!empty($listing[$key]) && $listing[$key]!='0000-00-00'):	 ?>	
						<p>Available on: </p>						
                        <p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
                        <?php
                        endif;                        
                    break;        
                    case 'secondary_contact':
                        if(!empty($listing['secondary_user']) && $listing['secondary_user']!= $listing['user_id'])
						$secondary_user = $this->user($listing['secondary_user']);
                        if(!empty($secondary_user)):
                            foreach($values as $param):
                                if(!empty($user[$param])): ?>
								<p>
									<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
									<?php if($param=='email'): ?><a href="mailto:<?php echo $secondary_user[$param]; ?>"><?php echo $secondary_user[$param]; ?></a>
									<?php else: ?>
									<?php echo $secondary_user[$param]; ?>
									<?php endif; ?>
									</span>
								</p>
								<?php
                                endif;
                            endforeach;
                        endif;					
                    break; 
					case 'price':
						if (!$listing["is_auction"] && $listing["status"]!='2'):?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php elseif($listing["status"]=='2'): ?>
							<?php /* <p class="<?php echo $key; ?>"><?php echo ($listing['display_sold_price'])? $listing['last_price']:'Undisclosed'; ?></p> */ ?>
							<p class="<?php echo $key; ?>"><?php echo ($listing['price_display_value']=='')? 'Asking price' : $listing[$key]; ?></p>
						<?php else : ?>
							<p class="<?php echo $key; ?>">Auction: <?php 
								if(!empty($listing["auction_place"]))echo $listing["auction_place"].", ";							
								echo $listing["auction_date"]; 
								if(!empty($listing["auction_time"]))echo " at ".$listing["auction_time"]; ?>
							</p>
						<?php endif;
					break;						
                    default:
                        if(!empty($listing[$key])):	?>							
                            <p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
                        <?php
                        endif;
                    break;
                endswitch;
            endforeach; ?>
            <div class="clear"></div>
            </div><?php /* ends .tdContent */ ?>
                
                <?php
				/*
					$photo = $realty->photos($listing['id'], "photos", array('800x600'));
					if(!empty($photo)){
				?>
                <div class="thumb-slideshow"><ul>
                <?php
						$photos = array();
						$j='0';$k='0';$l='0';
						for ($i=0;$i<count($photo);$i++){
							if($photo[$i]['small']!=""){ $photos[$j]['small']=$photo[$i]['small']; $j++; }
							if($photo[$i]['medium']!=""){ $photos[$k]['medium']=$photo[$i]['medium']; $k++; }
							if($photo[$i]['large']!=""){ $photos[$l]['large']=$photo[$i]['large']; $l++; }
						} 
						 ?>
						<li class="button photo_btn">
							<a class="link-lightbox view_photos" href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="lightbox[photo_gallery<?php echo $listing['id']?>]" title="View Slide Show">Slide Show</a>
							<?php for ($i=1;$i<count($photos);$i++){ ?>
							<a href="<?php echo $photos[$i]['large']; ?>" onclick="return false;" rel="lightbox[photo_gallery<?php echo $listing['id']?>]"></a>
							<?php } ?>
						</li>				
					</ul></div>
					<?php
					}
					*/
				?>
        </div> <!--ends .container-->

		<?php
		if ( $count % 3 == 2 or $count == ($listings['results_count'] - 1) ) {
		    ?><div class="clear"></div></div><?php }$count++; ?>
    
<?php /* if ($count % 3==2 or $count==($listings['results_count']-1)){ ?></div><?php }$count++; */ ?>
<?php endforeach; ?>	
</div>
<!-- thumbnail format end -->            
<?php endif ;

if($realty->settings['general_settings']['not_display_search_result_in_list_format']==''){ ?>
	<?php /*if ($realty->settings['search_results']['action_buttons']){ ?>
	<div id="filter_map" class="filter_lightbox"></div>
	<div id="box_map" class="box_lightbox">
	  <span id="boxtitle_map" class="boxtitle_lightbox"></span>
	  <div id="load_form_map"></div>    
	</div>		
	<div id="filter_video" class="filter_lightbox"></div>
	<div id="box_video" class="box_lightbox">
	  <span id="boxtitle_video" class="boxtitle_lightbox"></span>
	  <div id="load_form_video"></div>    
	</div>
	<?php }*/ ?>
<!-- List Format -->
<div id="list_format">
<?php /* <table class="property" width="100%" border="0" cellspacing="0" cellpadding="0"> */ ?>
<?php			
foreach($listings as $listing):
	if(!is_array($listing)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned			
	$printedTd = false;
	$count = 0;
	?>
	<div class="list-wrap" <?php echo ($alt_class = ($alt_class == ' class="alt"') ? '' : ' class="alt"'); ?>>
	<?php /* <tr <?php echo ($alt_class = ($alt_class==' class="alt"') ? '':' class="alt"'); ?>>
		<td class="listCont">
    	<div class="listWrap"> */ ?>
		<?php
		 if(is_array($realty->settings['search_results'])) 
			foreach($realty->settings['search_results'] as $key=>$values):
				if($key =='thumbnail'):					
					if( $count > 0 ):
						echo '</div>';
						$printedTd = false;
					endif;
				?>
					<div class="image">
				<?php
					elseif($key !='thumbnail' and !$printedTd):
				?>	
					<?php /* <div class="tdContent"> */ ?>
					<div class="details">
				<?php 
					$printedTd = true;
				endif;	
				switch($key):
					case 'thumbnail': ?>
                    <?php //$image_attributes = image_attributes($listing['medium'], 490, 326, 164); ?>					
					<a class="<?php echo $image_attributes[1]; ?>" href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>">
                        <img src="<?php echo $listing['large']; ?>" alt="<?php echo $listing['headline']; ?>"  <?php /*style="margin-top:<?php echo $image_attributes[0]; ?>"*/ ?>/>
					</a>
						<?php /*  if ($listing['new']):?>
						<div class="fresh_place">
						</div>
						<?php */ if ($listing['is_sold']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker sold ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo /*$realty->settings['property_stickers']['sticker_text']['sold']*/ "SOLD";?></span>
							</div>
						</div>
						<?php elseif ($listing['is_leased']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker leased ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo /*$realty->settings['property_stickers']['sticker_text']['leased']*/ "LEASED";?></span>
							</div>
						</div>
						<?php elseif ($listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker under_offer ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo /*$realty->settings['property_stickers']['sticker_text']['under_offer']*/ "UNDER OFFER";?></span>
							</div>
						</div>
						<?php elseif (!empty($listing['is_open_home']) && !$listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker open_home ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo ($realty->settings['property_stickers']['sticker_text']['open_home']=='')?'Open Home':$realty->settings['property_stickers']['sticker_text']['open_home'];?></span>
							</div>
						</div>
						<?php endif;
					break;
					case 'address':
						if(is_array($values)) foreach($values as $addressPart)${$addressPart} = $listing[$addressPart];
						$address='';
						if(!empty($street_address))$address=$street_address.", ";
						if(!empty($suburb))$address.=$suburb.", ";
						if(!empty($state))$address.=$state.", "; ?>
						<div class="detail-1 left col-2">
						<p class="street-address"><a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>"><?php echo substr($address,0,-2) ;?></a></p>						<?php
					break;
					case 'description':?>
					<p class="description"><?php echo $helper->_substr($listing['description'], $values); ?>&hellip;<a href="<?php echo $listing['url']; ?>" title="View Property">More &raquo;</a></p></div>
					<?php
					break;
					case 'building_size':
						if(!empty($listing[$key])): ?>
							<div class="land_building_size">
								<p>Building Size: <span class="building_size"><?php echo $listing[$key]." ".$listing['floor_area_metric']; ?></span></p>
							</div>
							<?php
                            endif;				
					break;
					case 'land_size':						
						if(!empty($listing[$key])): ?>
								<div class="land_building_size">
									<p class="land_size">LAND: <span class="land_size">
									<?php //echo $listing[$key]." ".$listing['land_area_metric']; 
									if ($listing['condo_strata_fee']==''){
										echo $listing[$key]." ".$listing['land_area_metric'];
									}else{
										 echo 'Strata Unit';
									}
									?></span></p>
								</div>
							<?php
                            endif;      			
					break;
					case 'current_rent':?>
						<p class="current_rent">Current Rent : <?php echo '$'.number_format($listing['current_rent']); ?></p>
					<?php
					break;
					case 'price':
						if (!$listing["is_auction"] && $listing["status"]!='2'):?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php elseif($listing["status"]=='2'): ?>
							<?php /* <p class="<?php echo $key; ?>"><?php echo ($listing['display_sold_price'])? $listing['last_price']:'Undisclosed'; ?></p> */ ?>
							<p class="<?php echo $key; ?>"><?php echo ($listing['price_display_value']=='')? 'Asking price' : $listing[$key]; ?></p>
						<?php else : ?>
							<p class="<?php echo $key; ?>">Auction: <?php 
								if(!empty($listing["auction_place"]))echo $listing["auction_place"].", ";							
								echo $listing["auction_date"]; 
								if(!empty($listing["auction_time"]))echo " at ".$listing["auction_time"]; ?>
							</p>
						<?php endif;?>
						</div><?php
					break;
					case 'action_buttons': ?>
						<div class="more-buttons">
							<ul class="action-buttons">
								<?php 
								foreach($values as $param){
									switch($param){
										case 'photos':
											$photo = $realty->photos($listing['id'], "photos", array('800x600'));
											if(!empty($photo)){
												$photos = array();
												$j='0';$k='0';$l='0';
												for ($i=0;$i<count($photo);$i++){
													if($photo[$i]['small']!=""){ $photos[$j]['small']=$photo[$i]['small']; $j++; }
													if($photo[$i]['medium']!=""){ $photos[$k]['medium']=$photo[$i]['medium']; $k++; }
													if($photo[$i]['large']!=""){ $photos[$l]['large']=$photo[$i]['large']; $l++; }
												} 
												 ?>
												<li class="button photo_btn">
													<a class="link-lightbox view_photos" href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[photo_gallery<?php echo $listing['id']?>]" title="View SLIDE SHOW">SLIDE SHOW</a>
													<?php for ($i=1;$i<count($photos);$i++){ ?>
													<a href="<?php echo $photos[$i]['large']; ?>" onclick="return false;" rel="prettyPhoto[photo_gallery<?php echo $listing['id']?>]"></a>
													<?php } ?>
												</li>				
											<?php
											}
										break;
										case 'floorplans':
											$floorplans = $realty->photos($listing['id'], "floorplans", array('800x600'));	
											if(!empty($floorplans)){
												$floorplan = array();
												$j='0';$k='0';$l='0';
												for ($i=0;$i<count($floorplans);$i++){
													if($floorplans[$i]['small']!=""){ $floorplan[$j]['small']=$floorplans[$i]['small']; $j++; }
													if($floorplans[$i]['medium']!=""){ $floorplan[$k]['medium']=$floorplans[$i]['medium']; $k++; }
													if($floorplans[$i]['large']!=""){ $floorplan[$l]['large']=$floorplans[$i]['large']; $l++; }
												}
												 ?>
												<li class="button floorplan">
													<a class="link-lightbox view_photos" href="<?php echo $floorplan[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[floorplan<?php echo $listing['id']?>]" title="View FLOOR PLAN">FLOOR PLAN</a>
													<?php for ($i=1;$i<count($floorplan);$i++){ ?>
													<a href="<?php echo $floorplan[$i]['large']; ?>" onclick="return false;" rel="prettyPhoto[floorplan<?php echo $listing['id']?>]"></a>
													<?php } ?>
												</li>					
											<?php
											}
										break;
										case 'map':
											if(!empty($listing['latitude']) && !empty($listing['longitude'])){ ?>								
												<li class="button map">
													<a class="link-lightbox" href="#" onclick="openbox('Map', 'filter_map', 'box_map', 'boxtitle_map', 'load_form_map', '<?php echo $realty->pluginUrl.'display/elements/lightbox_map.php?property_id='.$listing['id'].'&siteurl='.$realty->siteUrl; ?>')" title="MAP">MAP</a>
												</li>								
											<?php }
										break;		
										case 'video':
											if(!empty($listing['property_video'])){ ?>								
												<li class="button video">
													<a class="link-lightbox" href="#" onclick="openbox('Video', 'filter_video', 'box_video', 'boxtitle_video', 'load_form_video', '<?php echo $realty->pluginUrl.'display/elements/lightbox_video.php?property_id='.$listing['id']; ?>')" title="VIDEO">VIDEO</a>
												</li>								
											<?php }
										break;											
									}
								} ?>
							</ul>
						</div>
					<?php
					break;
					case 'number_of_rooms': 
						if ( !$listing['is_land'] and is_array($values) and !empty($values)): 
						?>
						<div class="detail-2 left col-2">
						<div class="rooms-wrap">							
							<ul class="rooms">
							<?php  
							foreach($values as $param):
								if(!empty($listing[$param])): ?>
								<li class="<?php echo $param; ?>">
									<span class="room_count"><?php echo $listing[$param]; ?></span> <span class="room_type"><?php //echo $param; ?></span>
								</li>
								<?php
								endif;
							endforeach; ?>
							</ul>
						</div>
						<?php 
						endif;	
					break;
					case 'primary_contact': 
						$user = $realty->user($listing['user_id']);
						if(!empty($user)):?>
						<p>Contact</p>
						<?php
							foreach($values as $param):
								if(!empty($user[$param])): ?>
								<p>
								<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $user[$param]; ?>"><?php echo $user[$param]; ?></a>
								<?php else: ?>
								<?php echo $user[$param]; ?>
								<?php endif; ?>
								</span>
								</p>
								<?php
								endif;
							endforeach;
						endif;						
					break;
					case 'secondary_contact':
						if(!empty($listing['secondary_user']) && $listing['secondary_user']!= $listing['user_id'])$secondary_user = $this->user($listing['secondary_user']);
						if(!empty($secondary_user)):
							foreach($values as $param):
								if(!empty($user[$param])):?>
								<p><span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $secondary_user[$param]; ?>"><?php echo $secondary_user[$param]; ?></a>
								<?php else: ?>
								<?php echo $secondary_user[$param]; ?>
								<?php endif; ?>
								</span>
								</p>
								<?php
								endif;
							endforeach;
						endif;					
					break;	
					case 'contact': 
						$office=$wpdb->get_results("select logo1 , name from office where id=".intval($listing['office_id']), ARRAY_A);
						$logo=$office[0]['logo1'];
						$office_name=$office[0]['name'];
						$user='';$user2='';
						if(!empty($listing['user_id']))$user=$wpdb->get_var("select CONCAT(firstname, ' ', lastname) as name from users where id=".$listing['user_id']);
						if(!empty($listing['secondary_user']))$user2=$wpdb->get_var("select CONCAT(firstname, ' ', lastname) as name from users where id=".$listing['secondary_user']);
						?>
						<div class="contact">
						<?php 
						if(!empty($logo))echo "<div class='office-logo'><img width='120px' height='90px' src='$logo'></div>";
						if(!empty($office_name))echo "<div class='office-name'>$office_name</div>";
						//echo "<p>Contact</p>";
						if(!empty($user))echo "$user";
						//if(!empty($user2))echo "$user2<br/>";
						?>
						</div>
					<?php break;
					case 'fav': ?>
						<div class="add-fav" id="add_to_favs_<?php echo $listing['id'] ; ?>">
							<?php $id=$listing['id'];	if($favourite->check_prop_id($id) == false):	?>
							<a class="link-add" href="javascript:saveProp('<?php echo $realty->siteUrl;?>','<?php echo $id ; ?>');" title="Add Favourites">Save</a>
							<?php	else: ?>
							<a class="link-del" href="javascript:delProp('<?php echo $realty->siteUrl;?>','<?php echo $id ; ?>');" title="Remove Favourites">Saved</a>
							<?php endif; ?>
						</div>				
					<?php break;
					case 'opentimes':
						$opentimes = $this->opentimes($listing['id']); 
						if(!empty($opentimes)) :?>	
						<p class="opentimes">
							<strong>Open for Inspection</strong><br />
							<span class="open_date"><?php echo date('D j M', strtotime($opentimes[0]['date'])); ?></span> <span class="open_time"><?php echo date('g:iA', strtotime($opentimes[0]['start_time'])); ?> - <?php echo date('g:iA', strtotime($opentimes[0]['end_time']));?></span>
						</p>
						<?php
						endif;
					break;
					case 'auction':
						if (!empty($listing['auction_date']) && $listing['auction_date']!='0000-00-00'):	?>
							<p class="<?php echo $key; ?>">
							<strong>Auction</strong><br />

							<?php
							echo date("l j F", strtotime($listing['auction_date']));
							if(!empty($listing['auction_time']) && $listing['auction_time']!='00:00:00')echo ' at '.date("ga", strtotime($listing['auction_time']));
							if(!empty($listing['auction_place']))echo ', '.$listing['auction_place'];
							?>						
							
							</p>
						<?php
						endif;
					break;
					case 'url': ?>
						<p class="url"><a href="<?php echo $listing['url']; ?>" class="btn" title="more detail">view property</a></p>				
					<?php break;
					case 'available_at':
						if(!empty($listing[$key]) && $listing[$key]!='0000-00-00'):	?>	
							<p>Available on: </p>						
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
						endif;					
					break;												
					default:
						if(!empty($listing[$key])):		?>							
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
						endif;
					break;
				endswitch;
				if($key =='thumbnail')
					echo '</div>';
				++$count;
			endforeach; 
			if($key !='thumbnail') ?>
				<div class="clear"></div>
				</div> <?php /* ends .tdContent */ ?>
				<div class="clear"></div>
				</div><!--ends .listWrap-->            
			<?php /* </td>
		</tr> */ ?><!-- ends property <?php //echo $listing['id']; ?> -->
	<?php endforeach; ?>	
	<?php /* </table> */ ?>
	
</div>
<!-- list format end-->
<?php } ?>
