<?php
/*
Plugin Name: Subscriptions Manager NEW
Version: 1.9
Plugin URI: http://www.zooproperty.com/online-store
Description: Based on a property alerts plugin and incorporating features from Subscribe2, this plugin unifies email subscriptions to the blog posts, sale, lease, sold and leased listings. Features a log-in system for the end user to manage their subscriptions with a management interface for the site administrator to send e-mail alerts, view, edit, backup and add new subscribers. Zoo Subscriptions Manager is licensed under the <a href="http://www.zooproperty.com/plugin-license/">Zoo Property License</a>.
Author: Agentpoint
Author URI: http://www.agentpoint.com.au
*/
$helper;
class subscriptions_manager{
	var $plugin_name = "Subscriptions Manager";
	var $subscriber;
	var $list_referrers = array('Magazine', 'Newspaper', 'Realestate.com.au', 'Referral, Web - Other', 'Word of Mouth');
	var $country = array ('AFGHANISTAN', 'ALAND ISLANDS', 'ALBANIA', 'ALGERIA',' AMERICAN SAMOA',' ANDORRA',' ANGOLA',' ANGUILLA',' ANTARCTICA',' ANTIGUA AND BARBUDA',' ARGENTINA',' ARMENIA','ARUBA','AUSTRALIA','AUSTRIA','AZERBAIJAN','BAHAMAS','BAHRAIN','BANGLADESH','BARBADOS','BELARUS','BELGIUM','BELIZE','BENIN','BERMUDA','BHUTAN','BOLIVIA','BOSNIA AND HERZEGOVINA','BOTSWANA','BOUVET ISLAND','BRAZIL','BRUNEI DARUSSALAM','BULGARIA','BURKINA FASO','BURUNDI','CAMBODIA','CAMEROON','CANADA','CAPE VERDE','CAYMAN ISLANDS','CENTRAL AFRICAN REPUBLIC','CHAD','CHILE','CHINA','CHRISTMAS ISLAND','COCOS (KEELING) ISLANDS','COLOMBIA','COMOROS','CONGO','CONGO','COOK ISLANDS','COSTA RICA','COTE DIVOIRE','CROATIA','CUBA','CYPRUS','CZECH REPUBLIC','DENMARK','DJIBOUTI','DOMINICA','DOMINICAN REPUBLIC','ECUADOR','EGYPT','EL SALVADOR','EQUATORIAL GUINEA','ERITREA','ESTONIA','ETHIOPIA','FALKLAND ISLANDS (MALVINAS)','FAROE ISLANDS','FIJI','FINLAND','FRANCE','FRENCH GUIANA','FRENCH POLYNESIA','FRENCH SOUTHERN TERRITORIES','GABON','GAMBIA','GEORGIA','GERMANY','GHANA','GIBRALTAR','GREECE','GREENLAND','GRENADA','GUADELOUPE','GUAM','GUATEMALA','GUERNSEY','GUINEA','GUINEA-BISSAU','GUYANA','HAITI','HEARD ISLAND AND MCDONALD ISLANDS','HOLY SEE','HONDURAS','HONG KONG','HUNGARY','ICELAND','INDIA','INDONESIA','IRAN','IRAQ','IRELAND','ISLE OF MAN','ISRAEL','ITALY','JAMAICA','JAPAN','JERSEY','JORDAN','KAZAKHSTAN','KENYA','KIRIBATI','KOREA','KUWAIT','KYRGYZSTAN','LAO','LATVIA','LEBANON','LESOTHO','LIBERIA','LIBYAN ARAB JAMAHIRIYA','LIECHTENSTEIN','LITHUANIA','LUXEMBOURG','MACAO','MACEDONIA','MADAGASCAR','MALAWI','MALAYSIA','MALDIVES','MALI','MALTA','MARSHALL ISLANDS','MARTINIQUE','MAURITANIA','MAURITIUS','MAYOTTE','MEXICO','MICRONESIA','MOLDOVA','MONACO','MONGOLIA','MONTENEGRO','MONTSERRAT','MOROCCO','MOZAMBIQUE','MYANMAR','NAMIBIA','NAURU','NEPAL','NETHERLANDS','NETHERLANDS ANTILLES','NEW CALEDONIA','NEW ZEALAND','NICARAGUA','NIGER','NIGERIA','NIUE','NORFOLK ISLAND','NORTHERN MARIANA ISLANDS','NORWAY','OMAN','PAKISTAN','PALAU','PALESTINIAN TERRITORY','PANAMA','PAPUA NEW GUINEA','PARAGUAY','PERU','PHILIPPINES','PITCAIRN','POLAND','PORTUGAL','PUERTO RICO','QATAR','REUNION','ROMANIA','RUSSIAN FEDERATION','RWANDA','SAINT BARTHÉLEMY','SAINT HELENA','SAINT KITTS AND NEVIS','SAINT LUCIA','SAINT MARTIN','SAINT PIERRE AND MIQUELON','SAINT VINCENT AND THE GRENADINES','SAMOA','SAN MARINO','SAO TOME AND PRINCIPE','SAUDI ARABIA','SENEGAL','SERBIA','SEYCHELLES','SIERRA LEONE','SINGAPORE','SLOVAKIA','SLOVENIA','SOLOMON ISLANDS','SOMALIA','SOUTH AFRICA','SOUTH GEORGIA','SPAIN','SRI LANKA','SUDAN','SURINAME','SVALBARD AND JAN MAYEN','SWAZILAND','SWEDEN','SWITZERLAND','SYRIAN ARAB REPUBLIC','TAIWAN',' PROVINCE OF CHINA','TAJIKISTAN','TANZANIA','THAILAND','TIMOR-LESTE','TOGO','TOKELAU','TONGA','TRINIDAD AND TOBAGO','TUNISIA','TURKEY','TURKMENISTAN','TURKS AND CAICOS ISLANDS','TUVALU','UGANDA','UKRAINE','UNITED ARAB EMIRATES','UNITED KINGDOM','UNITED STATES','URUGUAY','UZBEKISTAN','VANUATU','VENEZUELA','VIET NAM','VIRGIN ISLANDS',' BRITISH','VIRGIN ISLANDS',' U.S.','WALLIS AND FUTUNA','WESTERN SAHARA','YEMEN','ZAMBIA','ZIMBABWE');
	var $default_password = 'bw56xl4t3';
	var $debug_mode = false; //Debug mode will not send any emails, and will set the days to consider a property as new to 100
	var $alerts_available = array('sale'=>'New Sales Listings','latest_sale_update'=>'Latest Sales Updates','opentimes_sales'=>'Sales Open Times','lease'=>'New Rental Listings','latest_lease_update'=>'Latest Rental Updates','opentimes_lease'=>'Rental Open Times','listing_sold'=>'Sold Listings','listing_lease'=>'Leased Listings','latest_auction'=>'Latest Auctions');
	var $pages = array('subscribe'=>'[subscriptions_manager_plugin: template:subscribe]',
	'sales_appraisal'=>'[subscriptions_manager_plugin: template:sales_appraisal]',
	'management_appraisal'=>'[subscriptions_manager_plugin: template:management_appraisal]',
	'contact_us'=>'[subscriptions_manager_plugin: template:contact_us]');
	var $display_element_page= array('title'=>'Title', 'first_name'=>'First Name', 'last_name'=>'Last Name', 'email'=>'Email', 'home_phone'=>'Home Number', 'mobile_phone'=>'Mobile Number', 'referrer'=>'How did you find us?', 'country'=>'Country', 'property_alert'=>'Property Alert', 'news_alert'=>'News Alert', 'property_address'=>'Property Address', 'comments'=>'Comments', 'issubscribe'=>'Subscribe to be kept informed?');
	
	function subscriptions_manager(){
		$this->start_session(3); //Session will last for 3 hours	
		$this->form_action = "?page=" . $_GET['page'];
		$this->debug = $_GET['debug_email'];
		add_action('admin_menu', array($this, 'admin_menu'));
		add_action('init', array($this, 'init'));
		add_action('the_content', array($this, 'content_filter'));
		register_activation_hook(__FILE__, array($this, 'install'));
		
		//All hooks for when a post is published.
		if (get_option('included_new_alert')==''){ // individual news alerts send every post published
			add_action('new_to_publish', array(&$this, 'publish_post'));
			add_action('draft_to_publish', array(&$this, 'publish_post'));
			add_action('pending_to_publish', array(&$this, 'publish_post'));
			add_action('private_to_publish', array(&$this, 'publish_post'));
			add_action('future_to_publish', array(&$this, 'publish_post'));
			add_action('publish_phone', array(&$this, 'publish_post'));
		}
		
		//********************************Configurations
		//Converts "http://www.site.com.au" to info@site.com.au
		$this->sitename = preg_replace('/(http:\\/\\/)|(www\\.)/is', '', strtolower( get_option('siteurl') ));	
		$this->blog_name = str_replace("&amp;","&",get_option('blogname'));
		$this->from_email = '"' . $this->blog_name . '" <info@' . $this->sitename . ">";

		$this->site_url = get_option('siteurl');
		if ( '/' != $this->site_url[strlen($this->site_url)-1] )
			$this->site_url .= '/';
		$this->plugin_folder = substr( dirname( __FILE__ ), strlen(ABSPATH . PLUGINDIR) + 1 ) . '/';
		$this->plugin_url = $this->site_url . PLUGINDIR . '/' . $this->plugin_folder;
		$wpmu=$this->check_this_is_multsite();
		if($wpmu){
			global $table_prefix;
			$this->plugin_table=$table_prefix.'subscriptions_manager';
			$this->alerts_table=$table_prefix.'site_alerts';
		}
		else{
			$this->plugin_table='subscriptions_manager';
			$this->alerts_table='site_alerts';
		}		
	}

	function check_this_is_multsite() {
		global $wpmu_version;
		if (function_exists('is_multisite')){
			if (is_multisite()) {
				return true;
			}
			if (!empty($wpmu_version)){
				return true;
			}
		}
		return false;
	}
	
	function logfile($logfile){
		if( !file_exists( $this->log_directory ) ){
			$ourFileHandle = fopen($this->log_directory, 'w') or die("can't open file");
			fclose($ourFileHandle);						
		}	
		else{
			$file_content  = file_get_contents( $this->log_directory );
			$logfile = "$file_content<hr/>$logfile" ;			
			unlink ($this->log_directory);
			$ourFileHandle = fopen($this->log_directory, 'w') or die("can't open file");
			fclose($ourFileHandle);			
		}	
		file_put_contents( $this->log_directory, $logfile ); 
	}	
		
	function start_session($expire = 0) {
		if ($expire == 0) {
			$expire = ini_get("session.gc_maxlifetime");
		} else {
			$expire *= 60*60; //Set the function to handle hours instead of seconds
			ini_set("session.gc_maxlifetime", $expire);
		}
		if (empty($_COOKIE['PHPSESSID'])) {
			session_set_cookie_params($expire);
			session_start();
		} else {
			session_start();
			setcookie("PHPSESSID", session_id(), time() + $expire);
		}
	}

	function install(){
		global $wpdb, $helper; 
		add_option('subscription_manager_included_categories', $this->alerts_available);
		
		$wpdb->query("CREATE TABLE IF NOT EXISTS `$this->plugin_table` (
			  `subscriber_id` bigint(20) NOT NULL auto_increment,
			  `title` varchar(10) NOT NULL,					  
			  `first_name` varchar(50) NOT NULL,
			  `last_name` varchar(50) NOT NULL,
			  `email` varchar(50) NOT NULL,
			  `password` varchar(50) NULL,	
			  `home_phone` varchar(30) NOT NULL,
			  `mobile_phone` varchar(30) NOT NULL,					  
			  `referrer` varchar(50) NOT NULL,
			  `country` varchar(50) NOT NULL,
			  `comments` text,
			  `property_id` varchar(10),
			  `property_address` varchar(100),
			  `active` tinyint(1) NOT NULL,
			  `keep_informed` varchar(1),
			  `created_at` datetime NOT NULL,	
			  `updated_at` datetime NOT NULL,	
			  PRIMARY KEY  (`subscriber_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;");

		$wpdb->query("CREATE TABLE IF NOT EXISTS `$this->alerts_table` (
			  `alert_id` mediumint(9) NOT NULL auto_increment,
			  `subscriber_id` bigint(20) NOT NULL,
			  `alert` varchar(200) NOT NULL,					  
			  `query_string` text,
			  PRIMARY KEY  (`alert_id`),
			  KEY `subscriber_id` (`subscriber_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;");			
			
		if(get_page_by_title('login')==false)$helper->wp_insert_post(array('post_title'=>'login','post_content'=> '[subscriptions_manager_plugin: template:login]'));
		if(get_page_by_title('unsubscribe')==false)$helper->wp_insert_post(array('post_title'=>'unsubscribe','post_content'=> '[subscriptions_manager_plugin: template:unsubscribe]'));

		$wpdb->query("CREATE TABLE IF NOT EXISTS `list_referrers` (
			  `id` mediumint(9) NOT NULL auto_increment,
			  `name` varchar(50) NOT NULL,
			  PRIMARY KEY  (`id`)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;");
		$row=$wpdb->get_results("select * from list_referrers", ARRAY_A);
		if(count($row)=='0'){
			foreach($this->list_referrers as $item_value):
				$wpdb->query("insert into list_referrers values('','".trim(esc_sql($item_value))."')");
			endforeach;
		}
		
		$wpdb->query("CREATE TABLE IF NOT EXISTS `country` (
			  `id` mediumint(9) NOT NULL auto_increment,
			  `country` varchar(50) NOT NULL,
			  PRIMARY KEY  (`id`)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;");
		$row1=$wpdb->get_results("select * from country", ARRAY_A);
		if(count($row1)=='0'){
			foreach($this->country as $item_value):
				$wpdb->query("insert into country values('','".esc_sql($item_value)."')");
			endforeach;
		}
	}

	function reinstall(){
		global $wpdb; 
		$row=$wpdb->get_results("select ID, post_content from wp_posts where post_content like '%<!--subscriptions_manager%'", ARRAY_A);
		if(!empty($row)){
			foreach($row as $item){
				$post_content=str_replace('<!--','[',$item['post_content']);
				$post_content=str_replace('-->',']',$post_content);
				$wpdb->query("update wp_posts set post_content ='$post_content' where ID=".intval($item['ID']));
			}
		}
		$subscribers=$wpdb->get_results("select * from $this->plugin_table where mobile_phone!='123456'", ARRAY_A);
		$alerts=$wpdb->get_results("select * from $this->alerts_table", ARRAY_A);
		$list_referrers=$wpdb->get_results("select * from list_referrers", ARRAY_A);
		$wpdb->query("drop table $this->plugin_table");
		$wpdb->query("drop table $this->alerts_table");
		$wpdb->query("drop table list_referrers");
		$this->install();
		if($subscribers){
			foreach($subscribers as $subscriber){
				$wpdb->insert($this->plugin_table,
					array(						
						'subscriber_id' => $subscriber['subscriber_id'],
						'first_name' => $subscriber['first_name'],
						'last_name' => $subscriber['last_name'],
						'email' => $subscriber['email'],						
						'home_phone' => $subscriber['home_phone'],						
						'mobile_phone' => $subscriber['mobile_phone'],						
						'referrer' => $subscriber['referrer'],
						'country' => $subscriber['country'],
						'comments' => $subscriber['comments'],
						'property_id' => $subscriber['property_id'],
						'property_address' => $subscriber['property_address'],
						'keep_informed' => $subscriber['keep_informed'],
						'created_at' => $subscriber['created_at'],
						'updated_at' => $subscriber['updated_at']
						),
					array( 
						'%d', 
						'%s', 
						'%s', 
						'%s',						 
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%d',	
						'%s',	
						'%d', 
						'%s', 
						'%s' 
					)
				);
			}
		}
		if($alerts){
			 foreach($alerts as $alert){
				$wpdb->insert($this->alerts_table,
					array(						
						'subscriber_id' => $alert['subscriber_id'],
						'alert' => $alert['alert'],
						'query_string' => $alert['query_string']
						),
					array( 
						'%d', 
						'%s', 
						'%s'
					)
				);
			}
		}
		if($list_referrers){
			foreach($list_referrers as $item){
				$wpdb->insert('list_referrers',
					array(						
						'name' => $item['name']						
						),
					array( 
						'%s'
					)
				);
			}
		}
				
	}

	function init(){
		global $helper, $realty;
		$is_user_page = strpos($_SERVER['REQUEST_URI'], 'client') !==false;		
		// $helper = new helper;
		$this->user_login_page = $realty->get_permalink('login');
		if($_GET['task'] =='subscriptions_manager_create_csv')$this->download_subscribers();
		if($_GET['task'] =='send_email_alerts'):	// Only run if the task has been set to send_email_alerts				
			echo $this->send_alerts_by_subscribers($_GET);
			exit(0);
		endif;
	}
	
	function admin_menu(){
		add_menu_page('Zoo Subscribers Plugin', 'Zoo Subscribers', 8, __FILE__, array($this, 'setting_page'));
		add_submenu_page(__FILE__,htmlspecialchars(__('Zoo Subscribers: Settings','Settings')), htmlspecialchars(__('Settings','Settings')), 8, __FILE__, array($this, 'setting_page'));				
		add_submenu_page(__FILE__,htmlspecialchars(__('Zoo Subscribers: Subscribers','Subscribers')), htmlspecialchars(__('Subscribers','Subscribers')), 8, 'Subscribers', array($this, 'subscriber_page'));				
		add_submenu_page(__FILE__,htmlspecialchars(__('Zoo Subscribers: Add New','Add New')), htmlspecialchars(__('Add New','Add New')), 8, 'AddNew', array($this, 'add_new_page'));				
		add_submenu_page(__FILE__,htmlspecialchars(__('Zoo Subscribers: Quick Add','Quick Add')), htmlspecialchars(__('Quick Add','Quick Add')), 8, 'QuickAdd', array($this, 'quick_add_page'));	
		add_submenu_page(__FILE__,htmlspecialchars(__('Zoo Subscribers: Send Alerts','Send Alerts')), htmlspecialchars(__('Send Alerts','Send Alerts')), 8, 'SendAlerts', array($this, 'send_alerts_page'));	
	}			

	function setting_page(){
		global $realty;
		if(($return = $this->process_management_page()) != false):
		?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
		endif;		
		require_once( 'admin/setting_page.php');			
	}
	
	function subscriber_page(){
		global $realty;
		if(($return = $this->process_management_page()) != false):
		?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
		endif;		
		$request = $_REQUEST;
		$list_alerts = $this->get_site_alerts();
		$all_alerts = $this->get_site_alerts(true);
		$news_alerts = $this->get_news_alerts();
		$all_list=$this->all_list_referrers();
		$list_countries = $this->list_countries();
		switch($_GET['action']):		
			case 'edit': case 'add_new':
				$list_countries = $this->list_countries();
				if (isset($_GET['subscriber_id']))$subscriber=$this->get_subscriber($_GET['subscriber_id']);
				require_once( 'admin/edit_subscribers_form.php');
			break;
			
			default:
				if(!empty($request['keywords'])){
					$request['keywords']=esc_sql($request['keywords']);
					$condition = "(first_name like '%".$request['keywords']."%' or last_name like '%".$request['keywords']."%' or email like '%".$request['keywords']."%' or home_phone like '%".$request['keywords']."%' or mobile_phone like '%".$request['keywords']."%')";
				}
				$subscribers = $this->get_subscribers($condition,$_GET['pg'], 50,$request['search_by_alert']);
				require_once( 'admin/subscribers_page.php');
			break;
		endswitch;					
	}
	
	function add_new_page(){
		global $realty;
		if(($return = $this->process_management_page()) != false):
		?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
		endif;		
		$request = $_REQUEST;
		$list_alerts = $this->get_site_alerts();
		$all_alerts = $this->get_site_alerts(true);
		$news_alerts = $this->get_news_alerts();
		$all_list=$this->all_list_referrers();
		$list_countries = $this->list_countries();
		require_once( 'admin/add_new_page.php');		
	}
	
	function quick_add_page(){
		global $realty;
		if(($return = $this->process_management_page()) != false):
		?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
		endif;		
		$request = $_REQUEST;
		$list_alerts = $this->get_site_alerts();
		$all_alerts = $this->get_site_alerts(true);
		$news_alerts = $this->get_news_alerts();
		require_once( 'admin/quick_add_page.php');		
	}
	
	function send_alerts_page(){
		global $realty;
		if(($return = $this->process_management_page()) != false):
		?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php 
		endif;		
		$request = $_REQUEST;
		$list_alerts = $this->get_site_alerts();
		$all_alerts = $this->get_site_alerts(true);
		$news_alerts = $this->get_news_alerts();
		require_once( 'admin/send_alerts_page.php');				
	}

	function content_filter($content){
		$matches = array();
		if(preg_match('[subscriptions_manager_plugin: template:subscribe]', $content, $matches))return $this->user_page('subscribe.php',preg_replace('/\[subscriptions_manager_plugin:.*?(.*?)template:(.*?)\]/', '', $content));
		else if(preg_match('[subscriptions_manager_plugin: template:sales_appraisal]', $content, $matches))return $this->user_page('sales_appraisal.php',preg_replace('/\[subscriptions_manager_plugin:.*?(.*?)template:(.*?)\]/', '', $content));
		else if(preg_match('[subscriptions_manager_plugin: template:management_appraisal]', $content, $matches))return $this->user_page('management_apraisal.php',preg_replace('/\[subscriptions_manager_plugin:.*?(.*?)template:(.*?)\]/', '', $content));
		else if(preg_match('[subscriptions_manager_plugin: template:contact_us]', $content, $matches))return $this->user_page('contact_us.php',preg_replace('/\[subscriptions_manager_plugin:.*?(.*?)template:(.*?)\]/', '', $content));
		else if(preg_match('[subscriptions_manager_plugin: template:login]', $content, $matches))return $this->user_page('login.php',preg_replace('/\[subscriptions_manager_plugin:.*?(.*?)template:(.*?)\]/', '', $content));
		else if(preg_match('[subscriptions_manager_plugin: template:unsubscribe]', $content, $matches))return $this->user_page('unsubscribe.php',preg_replace('/\[subscriptions_manager_plugin:.*?(.*?)template:(.*?)\]/', '', $content));
		return $content;
	}
	
	function list_countries(){
		global $wpdb;
		return $wpdb->get_col("SELECT `country` FROM country ORDER BY `country` ASC");
	}

	function update_profile(&$request){
		global $helper, $wpdb;
		if(empty($request['password']))unset($request['password']);
		else $password = ",password";
		
		if( ($error = $this->validate_user_form($request,"first_name,last_name,alert" . $password) . $this->validate_email($request['email'], $request['subscriber_id'])) != '')return $error;
			
		$sql = "UPDATE `$this->plugin_table` SET `title`= '".esc_sql($request['title']). "',`referrer` ='" . esc_sql($request['referrer']). "',`first_name`='" . esc_sql($request['first_name']). "',`last_name`='". esc_sql($request['last_name']). "',`home_phone`='". esc_sql($request['home_phone']). "',`mobile_phone`='". esc_sql($request['mobile_phone']). "',`email`='". esc_sql($request['email']). "',`country`='". esc_sql($request['country']). "',`property_id`='". intval($request['property_id']). "',`property_address`='". esc_sql($request['property_address']). "',`comments`='". esc_sql($request['comments']). "',`updated_at`=now(),`keep_informed`='". intval($request['keep_informed']). "'";
	
		if(!empty($request['password']))$sql .= ", `password`='" . esc_sql($request['password']) . "'";

		return ($wpdb->query("$sql WHERE `subscriber_id`=" .intval($request['subscriber_id'])))? "Details updated successfully.":"Something went wrong. Please try again later.";	
	
	}

	function validate_email(&$email, $check_against_user = false){
		global $realty, $helper;
		$email = trim($email);
		if(empty($email))
			return '<p>E-mail field is empty.</p>';		
		if(!$helper->isValidEmail($email))
			return "<p>The e-mail \"$email\" appears to be invalid.</p>";	
		//Check if this is simply an update and the email already exists, but it belongs to this user
		if(($found_user = $this->is_subscribed_email($email)) == false) // This e-mail does not exist in the database. Good to go
			return;
		if($check_against_user != $found_user)
			return "<p>This e-mail address is already in use.</p>";
	}
	
	function is_subscribed_email($email){
		global $wpdb;
		return $wpdb->get_var("SELECT `subscriber_id` FROM `$this->plugin_table` WHERE `email`='".esc_sql($email)."'");		
	}

	function reset_password($request){	
		global $helper, $realty, $wpdb; 
		if(!empty($request['email'])):
			if(($error = $this->validate_user_form($request,'email')) !='')
				return $error;
			if ($this->is_subscribed_email($request['email']) == false)
				return "This e-mail address is not registered.<br />";
			
			$new_password = wp_generate_password(8, false);
			//$hashed_password = wp_hash_password($new_password); 
			$wpdb->query("UPDATE `$this->plugin_table` SET `password`='$new_password' WHERE `email`= '".esc_sql($request['email'])."'");
	
			ob_start();
			require('email_content/reset_password_email.php');
			$message = ob_get_contents();
			ob_end_clean();
			if($this->debug)//We dont want to send emails when testing: return immediately
				return;
			if($helper->email($request['email'], "Your password to $this->blog_name",$message))
				return "An e-mail has been sent to you with your new password.";	
		endif;	
	}

	function register_user(&$request, $admin = false){
		global $helper,$realty, $wpdb; 
		
		$validate = "password,date";
		if(!$admin)$validate .= ",first_name,last_name,alert";
		if( ($error = $this->validate_user_form($request, $validate)  . $this->validate_email($request['email'])) != '')return $error;
		
		$return = $wpdb->query("INSERT INTO `$this->plugin_table` VALUES('', '".esc_sql($request['title']). "','" . esc_sql($request['first_name']). "','". esc_sql($request['last_name']). "','". esc_sql($request['email']). "','". esc_sql($request['password']). "','". esc_sql($request['home_phone']). "','". esc_sql($request['mobile_phone']). "','". esc_sql($request['referrer']). "','". esc_sql($request['country']). "','". esc_sql($request['comments']). "','". intval($request['property_id']). "','". esc_sql($request['property_address']). "','1','".intval($request['keep_informed'])."',NOW(),NOW())");
		
		if($return != true)return $return;
		$_POST = array(); //Empty the $_POST array so that they don't submit twice.
		if($admin)return '<a href="'.$this->form_action . '&amp;action=edit&amp;subscriber_id='. $wpdb->insert_id. '">' .$request['email'] . " added.</a><br />";
		$this->set_session($wpdb->insert_id);
	}
	
	function register_user_page(&$request,$template){
		global $helper, $realty, $wpdb;
		if(empty($request['first_name']))$return.= '<p>First Name field is empty.</p>';
		if(empty($request['last_name']))$return.= '<p>Last Name field is empty.</p>';
		if(empty($request['email']))$return.= '<p>E-mail field is empty.</p>';			
		if(isset($request['password']) && empty($request['password']))$return.= '<p>Password field is empty.</p>';		
		if(isset($request['password']) && $request['password']!=$request['verify_password'])$return.= '<p>Password not match.</p>';	
		
		if(get_option('spam_filter')=='1'){
			if(empty($request['securitycode']))$return.= '<p>Captcha field is empty.</p>';
			if( $_SESSION['security_code'] != $_REQUEST['securitycode'] )$return.='<p>Wrong spam code.</p>';
			if( $_SESSION['security_code'] == $_REQUEST['securitycode']  )unset($_SESSION['security_code']);
		}
		if(!$helper->isValidEmail($request['email']))$return.= "<p>The e-mail ".$request['email']." appears to be invalid.</p>";		
		if(!empty($return))return $return;
		
		//if(class_exists('ZooRemittance') && ($request['keep_informed']=='1' || !empty($request['alert']))){
		if(class_exists('ZooRemittance')){
			if($template=='subscribe.php')$note_type='4';
			if($template=='sales_appraisal.php' || $template=='market_appraisal.php')$note_type='2';
			if($template=='management_appraisal.php')$note_type='3';
			if($template=='contact_us.php')$note_type='13';			
			$comments = trim($request['comments']);
			$comments = str_replace("\n", ";", $comments);
			$comments = str_replace("\r", ";", $comments);
			if(!empty($request['alert'])){
				$temp_array=array();
				foreach( $request['alert'] as $alert):
					$others = '';
					if(is_array($request['query_string'][$alert])){						
						$others=str_replace('%2C', ',', http_build_query($request['query_string'][$alert]));
						$others=str_replace("type_group","property_type_$alert",$others);
						$others=str_replace("bedrooms","min_bedroom_$alert",$others);
						$others=str_replace("bathrooms","min_bathroom_$alert",$others);
						$others=str_replace("carspaces","min_carspace_$alert",$others);
						$others=str_replace("price_min","min_price_$alert",$others);
						$others=str_replace("price_max","max_price_$alert",$others);
						$others=str_replace("suburb","suburbs_$alert",$others);
						$condition.=$others."&";
					}
					 if(!in_array($alert,$temp_array))array_push($temp_array, $alert);										
				endforeach;		
				$temp_array=implode(",",$temp_array);	
			}
			?>
			<script type="text/javascript">
				remmit_back();
				
				function remmit_back(){
					var url    = "<?php echo get_option('siteurl').'/wp-content/plugins/ZooRemittanceMobile/js/contact.php?title='.$request['title'].'&first_name='.$request['first_name'].'&last_name='.$request['last_name'].'&email='.$request['email'].'&home_phone='.$request['home_phone'].'&mobile_phone='.$request['mobile_phone'].'&work_phone='.$request['work_phone'].'&fax='.$request['fax'].'&country='.$request['country'].'&referrer='.$request['referrer'].'&address='.$request['property_address'].'&comments='.$comments.'&keep_informed='.$request['keep_informed'].'&note_type='.$note_type.'&alerts='.$temp_array.'&'.$condition;?>";
					//alert(url);
					jQuery.get(url);
				}
			</script>
		<?php 
		}
		
		$chk=$wpdb->get_results("select * from $this->plugin_table where email='".esc_sql($request['email'])."'");
	
		if(!$chk){
			$wpdb->query("INSERT INTO `$this->plugin_table` VALUES('', '".esc_sql($request['title']). "','" . esc_sql($request['first_name']). "','". esc_sql($request['last_name']). "','". esc_sql($request['email']). "','". esc_sql($request['password']). "','". esc_sql($request['home_phone']). "','". esc_sql($request['mobile_phone']). "','". esc_sql($request['referrer']). "','". esc_sql($request['country']). "','". esc_sql($request['comments']). "','". intval($request['property_id']). "','". esc_sql($request['property_address']). "','1','".intval($request['keep_informed'])."',NOW(),NOW())");			
		}
		else{
			$set="first_name='".esc_sql($request['first_name'])."',last_name='".esc_sql($request['last_name'])."',keep_informed='".intval($request['keep_informed'])."'";
			if(isset($request['title']))$set.=",title='".esc_sql($request['title'])."'";
			if(isset($request['password']))$set.=",password='".esc_sql($request['password'])."'";
			if(isset($request['referrer']))$set.=",referrer='".esc_sql($request['referrer'])."'";
			if(isset($request['home_phone']))$set.=",home_phone='".esc_sql($request['home_phone'])."'";
			if(isset($request['mobile_phone']))$set.=",mobile_phone='".esc_sql($request['mobile_phone'])."'";
			if(isset($request['country']))$set.=",country='".esc_sql($request['country'])."'";
			if(isset($request['property_address']))$set.=",property_address='".esc_sql($request['property_address'])."'";
			if(isset($request['comments']))$set.=",comments='".esc_sql($request['comments'])."'";
			$wpdb->query("update $this->plugin_table set $set where email='".esc_sql($request['email'])."'");
		}
		
		if(!empty($request['alert']) || $request['keep_informed']=='1'){
			$id=$wpdb->get_var("select subscriber_id from $this->plugin_table where email='".esc_sql($request['email'])."'");
			if(!empty($request['keep_informed'])){
				$list_alerts = $this->get_site_alerts();$all_alerts = $this->get_site_alerts(true);
				$news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);
				$i=count($request['alert']);
				foreach($news_alert as $key=>$item_value):
				$request['alert'][$i]=$key;$i++;
				endforeach;
			}
			if(!empty($request['alert'])){
				foreach( $request['alert'] as $alert):
					if(is_array($request['query_string'][$alert]))$query_string = str_replace('%2C', ',', http_build_query($request['query_string'][$alert]));
					$alerts_sql .="(" . intval($id) . ", '".esc_sql($alert)."', '". esc_sql($query_string) . "'), ";
					$query_string = '';
				endforeach;
				$wpdb->query("DELETE FROM `$this->alerts_table` WHERE `subscriber_id`=" . intval($id));
				$alerts_sql = substr($alerts_sql,0,-2); //Remove the last comma
				$wpdb->query("INSERT INTO `$this->alerts_table` (`subscriber_id`, `alert`, `query_string`) VALUES $alerts_sql");
			}
		}
	
		/* sent email*/
		$to_bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
		$subject = "Notification for a new user who registered on ".$request['post_title']." page";
		$bg_color="style='background-color:lightgrey;'";
		$user_format=get_option('sm_email_user_format');
		$admin_format=get_option('sm_email_admin_format');
		$is_use_password=get_option('is_use_password');
		$sm_logo=get_option('sm_logo');
		$sm_bg_color=get_option('sm_bg_color');
		$sm_heading_color=get_option('sm_heading_color');
		if(empty($sm_logo))$sm_logo=get_option('logo');
		if(empty($sm_bg_color))$sm_bg_color='#fff';
		if(empty($sm_heading_color))$sm_heading_color='#fff';
	
		if($template=='subscribe.php'){
			// send email to users
			$user_subject ='Thank you for registering to '.str_replace('amp;','',get_option('blogname'));
			$message = "<table width='760px'>
					<tr><td colspan='2'>Dear ".$request['first_name']." ".$request['last_name'].",</td></tr>
					<tr>
						<td colspan='2'>Thank you for subscribing to our website.</td>
					</tr>
					<tr $bg_color>
						<td width='30%'>Email</td><td>".$request['email']."</td>
					</tr>";
			if ($is_use_password=='1')$message.="<tr><td>Password</td><td>".$request['password']."</td></tr>";
			
			$text_message="Dear ".$request['first_name']." ".$request['last_name'].", \n\r\n\rThank you for subscribing to our website. \n\r\n\rEmail = ".$request['email'];		
			if ($is_use_password=='1')$text_message.="\n\rPassword = ".$request['password'];			
			
			if(!empty($request['alert'])){
				$text_message.="\n\rAlerts:\n\r";
				$message.="<tr><td width='30%' valign='top'>Alerts</td><td>";				
				foreach( $request['alert'] as $alert):
					$text_message.=' - '.$helper->humanize($alert);
					$message.='<div>-'.$helper->humanize($alert);
					if(is_array($request['query_string'][$alert])){
						foreach($request['query_string'][$alert] as $key=>$value){
							if(!is_array($value) && !empty($value)){
								$text_message.="\n\r    ".$helper->humanize($key).": $value";
								$message.="<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$helper->humanize($key).": $value";								
							}
							if(is_array($value) && !empty($value)){
								$text_message.="\n\r    ".$helper->humanize($key).": ".implode(", ",$value);	
								$message.="<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$helper->humanize($key).": ".implode(", ",$value);									
							}						
						}
					}
					$text_message.="\n\r";
					$message.="</div>";
				endforeach;
				$message.="</td></tr>";
			}		
			$message.="</table>";
			
			if($user_format=='html' || empty($user_format)){
				ob_start();
				include("email_content/new_subscribers_email.php");
				$message = ob_get_clean();
				$helper->email($request['email'], $user_subject, $message, '', '', false);
			}
			else $this->text_email($request['email'], $subject, $text_message, '', '', false);				
		}
				
		// sent email to admin				
		if($template=='subscribe.php'){
			$message = "<h2>Here are the registration details for a new user who signed up to ".get_option('blogname')."</h2>
						<table width='760px'>
							<tr $bg_color><td width='30%'>Name</td><td>".$request['first_name']." ".$request['last_name']."</td></tr>
							<tr><td>Email</td><td>".$request['email']."</td></tr>";	
			$text_message= "Here are the registration details for a new user who signed up to ".get_option('blogname')."\n\r\n\rName = ".$request['first_name']." ".$request['last_name']." \n\r\n\rEmail = ".$request['email'];	
		} else {
			$message= "<table width='760px'><tr $bg_color><td width='30%'>Name</td><td>".$request['first_name']." ".$request['last_name']."</td></tr><tr><td>Email</td><td>".$request['email']."</td></tr>";
			$text_message= "Name = ".$request['first_name']." ".$request['last_name']." \n\r\n\rEmail = ".$request['email'];				
		}
		
		if(!empty($request['home_phone'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			$message.="<tr $bg><td>Home Number</td><td>".$request['home_phone']."</td></tr>";
			$text_message.= "\n\rHome Number = ".$request['home_phone'];
			$count_row++;
		}
		if(!empty($request['mobile_phone'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			$message.="<tr $bg><td>Mobile</td><td>".$request['mobile_phone']."</td></tr>";
			$text_message.= "\n\rMobile = ".$request['mobile_phone'];
			$count_row++;
		}
		if(!empty($request['referrer'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			$message.="<tr $bg><td>How did you find us</td><td>".$request['referrer']."</td></tr>";
			$text_message.= "\n\rHow did you find us = ".$request['referrer'];
			$count_row++;
		}
		if(!empty($request['country'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			$message.="<tr $bg><td>Country</td><td>".$request['country']."</td></tr>";
			$text_message.= "\n\rCountry = ".$request['country'];
			$count_row++;
		}
		if(!empty($request['property_address'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			$message.="<tr $bg><td>Property Address</td><td>".$request['property_address']."</td></tr>";
			$text_message.= "\n\rProperty Address = ".$request['property_address'];
			$count_row++;
		}
		if(!empty($request['comments'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			$message.="<tr $bg><td>Comments</td><td>".nl2br($request['comments'])."</td></tr>";
			$text_message.= "\n\rComments = ".$request['comments'];
			$count_row++;
		}
		if(isset($request['keep_informed'])){
			$bg=($count_row%2==0)?$bg_color:''; 
			if(!empty($request['keep_informed'])){
				$message.="<tr $bg><td>Subscribe to be keep informed = yes</td></tr>";$text_message.= "\n\rSubscribe to be keep informed = yes"; 
			}
			else { $message.="<tr $bg><td>Subscribe to be keep informed = no</td></tr>"; $text_message.= "\n\rSubscribe to be keep informed = no";  }
		}
		if(!empty($request['alert'])){
			$text_message.="\n\rAlerts:\n\r";
			$message.="<tr><td width='30%' valign='top' >Alerts</td><td>";				
			foreach( $request['alert'] as $alert):
				$text_message.=' - '.$helper->humanize($alert);
				$message.='<div>-'.$helper->humanize($alert);
				if(is_array($request['query_string'][$alert])){
					foreach($request['query_string'][$alert] as $key=>$value){
						if(!is_array($value) && !empty($value)){
							$text_message.="\n\r    ".$helper->humanize($key).": $value";
							$message.="<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$helper->humanize($key).": $value";								
						}
						if(is_array($value) && !empty($value)){
							$text_message.="\n\r    ".$helper->humanize($key).": ".implode(", ",$value);	
							$message.="<br/>&nbsp;&nbsp;&nbsp;&nbsp;".$helper->humanize($key).": ".implode(", ",$value);									
						}						
					}
				}
				$text_message.="\n\r";
				$message.="</div>";
			endforeach;
			$message.="</td></tr>";
		}
		$message.="</table>";
		
		if($template=='subscribe.php')$email_sent=get_option('subscribe_add_subscribers');
		if($template=='sales_appraisal.php')$email_sent=get_option('sales_appraisal_add_subscribers');
		if($template=='management_apraisal.php')$email_sent=get_option('management_appraisal_add_subscribers');
		if($template=='contact_us.php')$email_sent=get_option('contact_us_add_subscribers');
		
		
		if($admin_format=='html' || empty($admin_format)){
			ob_start();
			include("email_content/new_subscribers_email.php");
			$message = ob_get_clean();
			if(!empty($to_bcc))$helper->email($to_bcc, $subject, $message,'','',false);
			if(!empty($email_sent)){
				foreach($email_sent as $emails):
					if($emails != $to_bcc)$helper->email($emails, $subject, $message,'','',false);
				endforeach;
			}
		}
		else {
			if(!empty($to_bcc))$this->text_email($to_bcc, $subject, $text_message,'','',false);
			if(!empty($email_sent)){
				foreach($email_sent as $emails):
					if($emails != $to_bcc)$this->text_email($emails, $subject, $text_message,'','',false);
				endforeach;
			}
		}
	
		if($template=='subscribe.php'){
			//$this->set_session($id);
			return "Thank you for enquiring about our properties, you are now added to our Property Alert list and receive our newest listings	.";
		}
		else return "Thank you for your enquiry. Our staff will get back to you shortly."; 
	}
	
	function user_page($template,$content){
		global $realty, $helper, $wpdb;
		$list_alerts = $this->get_site_alerts();
		$list_countries = $this->list_countries();
		$request = $_POST;
		$all_alerts = $this->get_site_alerts(true);
		$news_alerts = $this->get_news_alerts();
		$all_list=$this->all_list_referrers();
		session_start();
		if($this->is_logged_in()){
			if(isset($request['unsubscribe']) and $this->unsubscribe($_SESSION['subscriber_id'])){
				$this->logout();
				return "You have been unsubscribed.";
			} else if($request['task']=='register_or_edit'){
				$this->insert_alerts($request);
				$return = $this->update_profile($request);
			} else if($_GET['action']=='logout'){ $this->logout(); }
				
		} else {
			switch($request['task']):
				case 'login':					
					$is_use_password=get_option('is_use_password');
					if(empty($request['email']))$return = "Please fill your email.";
					else if(empty($request['password']) && $is_use_password==1)$return = "Please fill your password.";
					else{
						$subscriber_id = $this->check_login($request['email'],$request['password']);
						if($subscriber_id !=false):							
							$this->set_session($subscriber_id);
							$subscriber=$this->get_subscriber($_SESSION['subscriber_id']);
						else:
							$return = ($is_use_password==1)?"The email or password entered are incorrect.":"The email entered is incorrect.";
						endif;	
					}
				break;					
				case 'register_or_edit':	
					$return = $this->register_user_page($request,$template);				
				break;
				case 'forgot_password':
					$return = $this->reset_password($request);
				break;
				case 'unsubscribe':
						$subscriber_id=$wpdb->get_var("select subscriber_id from $this->plugin_table where email='".esc_sql($request['email'])."'");
						if($subscriber_id){ if($this->unsubscribe($subscriber_id, $request['email']))$return= "You have been unsubscribed."; }
						else $return= "The email '".$request['email']."' is not found on our subscription list.";
					break;
			endswitch;		
		}
		if(!empty($_SESSION['subscriber_id'])){ $subscriber=$this->get_subscriber($_SESSION['subscriber_id']); }
		require('templates/'.$template);
		ob_flush();
	}
		
	function text_email($to, $subject, $message, $from='', $bcc='', $flag=true){
		global $realty;
		if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
		if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\r\n";		
		if($flag){
		if(empty($bcc)) $bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
		if(!empty($bcc)) $bcc = "bcc: $bcc\r\n";		
		}	
		return @wp_mail($to, $subject, $message, $from . $bcc . "Content-Type: text/plain\n");		
	}
	
	function check_login($username, $password=''){ //Return a subscriber_id if the login is correct, otherwise, return false
		global $helper, $wp_hasher, $wpdb;
		
		if(get_option('is_use_password')=='1')
		$row = $wpdb->get_results("SELECT `subscriber_id`, `password` FROM `$this->plugin_table` WHERE `email`='".esc_sql($username)."' and password='".esc_sql($password)."' LIMIT 1", ARRAY_A);		
		else $row = $wpdb->get_results("SELECT `subscriber_id`, `password` FROM `$this->plugin_table` WHERE `email`='".esc_sql($username)."' LIMIT 1", ARRAY_A);
		if($row != false) // If the email was found , return the subscriber_id for that email
			return $row[0]['subscriber_id'];
		else
		return false;
	}

	function is_logged_in(){
		return (!empty($_SESSION['subscriber_id']));			
	}
	
	function logout(){
		$_SESSION = array();// Unset all of the session variables.
		// If it's desired to kill the session, also delete the session cookie. Note: This will destroy the session, and not just the session data!
		if (isset($_COOKIE[session_name()]))
			setcookie(session_name(), '', time()-42000, '/');		
		session_destroy();// Finally, destroy the session.
		unset($this->subscriber);
	}
	
	function set_session($subscriber_id ='') {
		if(empty($subscriber_id)) //Passing an empty value will cause the session to be unset. Used to log out.
			unset($_SESSION['subscriber_id']);
		$_SESSION['subscriber_id'] = $subscriber_id;
	}
	
	function send_alerts_by_subscribers($args=array()){
		global $helper, $realty, $wpdb,$post;
		ignore_user_abort(true);
		set_time_limit(0); //Run this script for as long as it takes to send all the emails.		
		$site_alerts = $this->get_site_alerts();
		$news_alerts = $this->get_news_alerts();
		
		if($args['task']=='send_email_alerts' && empty($args['test_email']))echo "Please fill your email";
		else{
			if(!empty($args['test_email']))$condition="email='".$args['test_email']."'";			
			if($args['alert']=='all_alerts')$alert='';else $alert=$args['alert'];
			$subscribers = $this->get_subscribers($condition, $args['pg'],$limit, $alert);
			
			if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
			if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\n";		
			
			$number_of_subscribers=0;
			if(empty($subscribers) && !empty($alert))return "<p>No subscribers were found for ".$site_alerts[$alert] . "</p>";
			if(empty($subscribers) && !empty($args['test_email']))return "<p>No subscribers were found with email `".$args['test_email']."`</p>";
			
			$days_limit=(get_option('sm_days_limit')=='')?'1':get_option('sm_days_limit');
			$included_new_alert=get_option('included_new_alert');
			$sm_logo=get_option('sm_logo');
			$sm_bed=get_option('sm_bed');
			$sm_bath=get_option('sm_bath');
			$sm_cars=get_option('sm_cars');
			$sm_bg_color=get_option('sm_bg_color');		
			$sm_heading_color=get_option('sm_heading_color');
			if(empty($sm_logo))$sm_logo=get_option('logo');
			if(empty($sm_bed))$sm_bed=$this->plugin_url.'images/room-bed.png';
			if(empty($sm_bath))$sm_bath=$this->plugin_url.'images/room-bath.png';
			if(empty($sm_cars))$sm_cars=$this->plugin_url.'images/room-car.png';

			if(empty($sm_bg_color))$sm_bg_color='#fff';
			if(empty($sm_heading_color))$sm_heading_color='#fff';
			$sm_heading_font_color=($sm_heading_color=='#fff')?'000':'fff';
			
			/* stop sending alerts multiple times */
            $sm_logs = get_option('sm_logs');
            $sm_log_time = get_option('sm_log_time');
            $stop = 0;
            $email_logs = '';
            foreach ($subscribers as $subscriber) {
                $email_logs.=$subscriber['email'] . "<br/>";
            }     
			if ($sm_logs == $subscribers[0]['email'] && date('d-m-y') == $sm_log_time)$stop = 1;
			if (!add_option('sm_logs', $subscribers[0]['email']))update_option('sm_logs', $subscribers[0]['email']);
			if (!add_option('sm_log_time', date('d-m-y')))update_option('sm_log_time', date('d-m-y'));
			@wp_mail('dewi@agentpoint.com.au', $realty->siteUrl . ' logs', "Run at " . date('d-m-Y h:i:s') . "<br/>Stop : $stop.<br/>First Email : $sm_logs.<br/>Log time : $sm_log_time.<br/>Total: ".count($subscribers). " subscribers.<br/>List :<br/>$email_logs", $from . $bcc . "Content-Type: text/html\n");		
			if ($stop=='0') { /* end */
			
			foreach($subscribers as $subscriber){
				$emails_sent=0;
				$flag='0';
				$sent_mails='';
				if(empty($subscriber['alert']) || !is_array($subscriber))continue;
				$alerts=explode(",",$subscriber['alert']);
				require('email_content/header.php');
				
				if(count($alerts)>1)$subject = "Latest Alerts - $this->blog_name";
				else $subject = $site_alerts[$alerts[0]] . " & Updates - $this->blog_name";
				
				foreach($alerts as $alert):
					$return='';
					switch($alert):
						case 'opentimes_sales': case 'opentimes_lease': 
							$template = 'opentimes';
							if($alert=='opentimes_lease')$open_alert='lease';else $open_alert='sale';
							$opendates = $realty->opendates($open_alert,$days_limit);
							if(empty($opendates)) 
								$return .= '<p class="return">No '. $site_alerts[$alert] .  ' found. No e-mails sent.</p>';
						break;
						case 'listing_sold':case 'listing_lease':
							$template = 'listing';
							if(substr($alert,8)=='sold')$type='sale';else $type='lease';					
							if($alert=='listing_sold')$list = $realty->properties(array( 'list'=> 'latest_sold', 'days_limit'=>$days_limit, 'order'=>'sold_at', 'order_direction'=>'DESC'));
							else $list = $realty->properties(array( 'list'=> 'latest_leased', 'days_limit'=>$days_limit, 'order'=>'leased_at', 'order_direction'=>'DESC'));
							if(empty($list) or $list['results_count']=='0' ) 
								$return .= '<p class="return">No '. $site_alerts[$alert] .  ' found. No e-mails sent.</p>';
						break;				
						case 'latest_auction':
							$template = 'latest_auction';
							$params = array('list'=> 'latest_auction','days_limit'=>$days_limit);
							$properties = $realty->properties($params);
							if($properties['results_count']=='0')
								$return .= '<p class="return">No '. $site_alerts[$alert] .  ' found. No e-mails sent.</p>';
						break;			
						case 'sale':case 'lease': case 'latest_sale_update':case 'latest_lease_update':
							$template = 'latest_listings';							
							$param=array();
							if(isset($subscriber[$alert]['type_group']) && !empty($subscriber[$alert]['type_group']))$param['property_type']=$subscriber[$alert]['type_group'];
							if(isset($subscriber[$alert]['property_type']) && !empty($subscriber[$alert]['property_type']))$param['property_type']=$subscriber[$alert]['property_type'];
							if(isset($subscriber[$alert]['bedrooms']) && !empty($subscriber[$alert]['bedrooms']))$param['bedrooms']=$subscriber[$alert]['bedrooms'];
							if(isset($subscriber[$alert]['bathrooms']) && !empty($subscriber[$alert]['bathrooms']))$param['bathrooms']=$subscriber[$alert]['bathrooms'];
							if(isset($subscriber[$alert]['price_min']) && !empty($subscriber[$alert]['price_min']))$param['price_min']=$subscriber[$alert]['price_min'];
							if(isset($subscriber[$alert]['price_max']) && !empty($subscriber[$alert]['price_max']))$param['price_max']=$subscriber[$alert]['price_max'];	
							if(isset($subscriber[$alert]['town_village']) && !empty($subscriber[$alert]['town_village']))$param['town_village']=$subscriber[$alert]['town_village'];					
							if(isset($subscriber[$alert]['suburb']) && !empty($subscriber[$alert]['suburb']))$param['suburb']=$subscriber[$alert]['suburb'];	
							if(isset($subscriber[$alert]['pool']) && !empty($subscriber[$alert]['pool']))$param['condition']="properties.id IN (select property_id from property_features where feature IN ('Swimming Pool'))";								
							$param['days_limit']=$days_limit;
							if($alert=='sale')$alert='latest_sale';	
							if($alert=='lease')$alert='latest_lease';
							$param['list']=$alert;		
							$param['status']='1,4';	
							$properties = $realty->properties($param);
							if ($properties['results_count'] <1)$return .= "<p>No e-mail sent to " . $subscriber['email'] . " because no " . $alert . " listings were found for this user's settings.</p>";
						break;
						default: //The default is assumed to be a category and will search posts within that category
							$template = 'posts_loop';	
							$subject = "Latest News & Updates - $this->blog_name";	
							if($included_new_alert==''){ //individual news alert, send everytime post published
								if(isset($args['published_post']) && is_object($args['published_post']))://The post data has already been passed. There's no need to perform another search. Set the posts and break from the switch to call the template file.
									$posts = array($args['published_post']);				
									break;
								else:
									$sql = "SELECT SQL_CALC_FOUND_ROWS $wpdb->posts.* FROM $wpdb->posts INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) WHERE 1=1 AND $wpdb->term_taxonomy.taxonomy = 'category' AND $wpdb->term_taxonomy.term_id IN (SELECT term_id FROM $wpdb->terms WHERE `slug`='". esc_sql($alert) . "') AND $wpdb->posts.post_type = 'post' AND DATEDIFF(CURDATE(),`post_date`) <= 1 AND ($wpdb->posts.post_status = 'publish') GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.post_modified DESC";
									$posts = $wpdb->get_results($sql);										
								endif;	
							}
							if($included_new_alert=='1'){ // news alert including in property alert
								if(isset($args['published_post']) && is_object($args['published_post'])):
									$posts = array($args['published_post']);				
									break;
								else:
									$sql = "SELECT SQL_CALC_FOUND_ROWS $wpdb->posts.* FROM $wpdb->posts INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) WHERE 1=1 AND $wpdb->term_taxonomy.taxonomy = 'category' AND $wpdb->term_taxonomy.term_id IN (SELECT term_id FROM $wpdb->terms WHERE `slug`='". esc_sql($alert) . "') AND $wpdb->posts.post_type = 'post' AND DATEDIFF(CURDATE(),`post_date`) <= ".$days_limit." AND ($wpdb->posts.post_status = 'publish') GROUP BY $wpdb->posts.ID ORDER BY $wpdb->posts.post_modified DESC";
									$posts = $wpdb->get_results($sql);						
								endif;	
							}
							if(empty($posts))$return .= "<p>No posts found. No e-mails sent.</p>";
						break;			
					endswitch;		
					if(empty($return)){require("email_content/" . $template . ".php");$flag++;}else {}
					
				endforeach;
				
				require('email_content/footer.php');	
				$return .= $output;
				
				if(isset($args['test_only']) && $flag!='0')://We dont want to send emails when testing: return immediately
					$return .= "<p>If this wasn't a test, the alert would have been sent to ". $subscriber['email'];
				endif;	
				
				$number_of_subscribers++;
				if (empty($args['test_only']) && $flag!='0'){
					if(@wp_mail($subscriber['email'], $subject,$message, $from . $bcc . "Content-Type: text/html\n")):
						++$emails_sent;
						$sent_mails .=  '<li>' . $subscriber['email'] . '</li>';
					endif;	
				}
				if($emails_sent > 0)
					$return .= "<p>Success! The alert was sent to ". $emails_sent. " subscribers. Here is the list:<ul>$sent_mails</ul>";
				if(!empty($args['test_email']) and !empty($args['test_only'])){
					$return .= "<p>Sent a sample of this alert to " . $subscriber['email'] . ".</p>";
				}		
				if(!isset($args['published_post']) && !empty($args['test_only']))echo $return;
			}
			}
		}
	}

	function download_subscribers(){
		if(!empty($_REQUEST['keywords']) && $_REQUEST['keywords']!='Search by keywords'){
			$_REQUEST['keywords']=esc_sql($_REQUEST['keywords']);
			$condition = "(first_name like '%".$_REQUEST['keywords']."%' or last_name like '%".$_REQUEST['keywords']."%' or email like '%".$_REQUEST['keywords']."%' or home_phone like '%".$_REQUEST['keywords']."%' or mobile_phone like '%".$_REQUEST['keywords']."%')";
		}
		$caption[] = array('First Name', 'Last Name', 'Email', 'Home Phone', 'Mobile Phone', 'Comments', 'Alert(s)', 'Subscribe to be kept Informed?');
		$subscribers = $this->get_subscribers($condition, '','', $_REQUEST['search_by_alert'], "first_name, last_name, email, home_phone, mobile_phone, comments, GROUP_CONCAT(`alert` SEPARATOR ', ') as alert,  keep_informed");
		$subscribers = array_merge($caption, $subscribers);
		$this->print_csv($this->create_csv($subscribers), 'subscriptions_manager_subscribers(' . date('j-n-Y') .').csv');
	}
	
	function create_csv($rows, $end_of_line ="\r\n", $pre_value='"',$post_value='",'){
		foreach($rows as $row): //A double-dimensional array. Usually an array of rows of an SQL statement, with each value being an array of fileds from each column in that row.
			if(!is_array($row))
				continue;
			foreach($row as $field):
				if(!is_array($field)):
					str_replace(',','\,',$row); // Commas must be escapedin UNIX
					$contents .= 	$pre_value . $field . $post_value;
				endif;
			endforeach;
			$contents .= $end_of_line;
		endforeach;
		return $contents;
	}

	function print_csv($output, $filename){
			header('Content-type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'. $filename . '"');		
			echo $output;
			exit(0);
	}

	function format_user_alerts(&$user){
		if(!is_array($user))
			return;
		$alerts_array = explode(',',$user['alert']);
		$query_string_array = explode(';',$user['query_string']);
		foreach($alerts_array as $key=>$alert)
			//$user[$alert] =  $this->parse_str_to_array($query_string_array[$key]);
			parse_str($query_string_array[$key],$user[$alert]);	
	}
	// function get_subscribers($condition ='', $limit_start=0,$limit='', $alert='', $select = $this->plugin_table.'.*, GROUP_CONCAT(`alert`) as alert, GROUP_CONCAT(`query_string` SEPARATOR ";") as query_string'){
	// function get_subscribers($condition ='', $limit_start=0,$limit='', $alert='', $select = '`subscriptions_manager`.*, GROUP_CONCAT(`alert`) as alert, GROUP_CONCAT(`query_string` SEPARATOR ";") as query_string'){
	
	function get_subscribers($condition ='', $limit_start=0,$limit='', $alert='', $select = '`subscriptions_manager`.*, GROUP_CONCAT(`alert`) as alert, GROUP_CONCAT(`query_string` SEPARATOR ";") as query_string'){
		global $helper, $wpdb;
		// print_r($select);
		// print_r("<br />");
		// print_r($this->plugin_table);
		// print_r("<br />");
		// print_r($this->plugin_table);
		if(!empty($condition))$condition = "WHERE $condition"; 
		if(!empty($alert))$condition .= (empty($condition))? "WHERE `alert`='".esc_sql($alert)."'": " AND `alert`='".esc_sql($alert)."'";
			
		$sql = "SELECT $select FROM `$this->plugin_table` LEFT JOIN `$this->alerts_table` USING (`subscriber_id`) $condition GROUP BY `$this->plugin_table`.`subscriber_id` ORDER BY `email`";
		$results_count=$wpdb->get_var("select count(subscriber_id) from $this->plugin_table  LEFT JOIN `$this->alerts_table` USING (`subscriber_id`)  $condition");
		if(!empty($limit)){
			$return=$this->paginate($limit, $limit_start, $results_count, $sql); 
			$sql=$return['sql'];
		}
		$subscribers =$wpdb->get_results($sql, ARRAY_A);
		if(!empty($limit))$subscribers['pagination_label'] = $return['pagination_label'];	
		array_walk($subscribers,array( $this,'format_user_alerts'));
		return $subscribers;
	}
	
	function get_subscriber($subscriber_id){
		global $helper;
		$subscriber = $this->get_subscribers("$this->plugin_table.`subscriber_id`=".intval($subscriber_id));	
		$this->subscriber = $subscriber[0];		
		$this->subscriber['referrer'] = trim($this->subscriber['referrer']);
		return $this->subscriber;
	}	
	
	function paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $wpdb;
		if($page < 1) //Avoid issuing a negative limit to the SQL statement
			$page = 1;
			//print $sql;
		//Classic pagination		
		if(!empty($sql) && $results_count==0) //Count the results based on the SQL statement provided
			if(($query =$wpdb->query($sql)) != false)
				$results_count = count($query);
		if ($limit < $results_count ):				
			$limit_start = $page * $limit - ($limit);
			if($limit_start > $results_count)
				$limit_start = $results_count - $limit; //Prevent the script from inadvertently going to an out of range result
			$sql_limit = " LIMIT $limit_start, $limit";			
		endif;
		$return['sql'] = $sql . $sql_limit;
		//ends classic pagination		
		
		$max_page_links = 10; //Set how many links to show in each page
		
		
		$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
		
		$current_query = "?" . preg_replace('/&(pg=)([-+]?\\d+)/is', '', $_SERVER['QUERY_STRING']) . "&amp;pg=";
		
		$prev_page = $page - 1;
		$next_page = $page + 1;
		ob_start();
		require('admin/pagination_label.php');//Front page pagination
		$return['pagination_label'] = ob_get_clean();
		return $return;
	}//ends function

	function publish_post($post = 0){
		if (!$post)return $post;
		$post_cats = get_the_category($post->ID);
		$list_alerts = $this->get_site_alerts();
		if(empty($list_alerts))$list_alerts=array();
		foreach($post_cats as $cat): //Check if the category of the post is in our list of alerts
			if(array_key_exists($cat->slug, $list_alerts) && $sent_flag!='1'){
				$this->send_alerts_by_subscribers(array('alert'=>$cat->slug, 'published_post'=>$post));
				$sent_flag=1;
			}
		endforeach;
		return $post;			
	}
	
	function unsubscribe($subscriber_id, $email=''){
		global $helper, $realty, $wpdb;
		$to_bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
		//$subscriber_id=intval($subscriber_id);		
		if(empty($email))$email=$wpdb->get_var("select email from $this->plugin_table WHERE `subscriber_id` IN ($subscriber_id)");
		$wpdb->query("DELETE FROM $this->plugin_table WHERE `subscriber_id` IN ($subscriber_id)");
		$wpdb->query("DELETE FROM $this->alerts_table WHERE `subscriber_id` IN ($subscriber_id)");	
		if(strpos($subscriber_id,',')===false){
			
			$email_sent=get_option('subscribe_add_subscribers');
			$subject="[".$this->blog_name."] Unsubscribe";
			$text_message="Hi,\n\nUser with email $email had been unsubscribed from your newsletter alert.\n\nSent Out from ".$this->sitename;
			if(!empty($email_sent)){
				foreach($email_sent as $emails):
					$this->text_email($emails, $subject, $text_message,'','',false);
				endforeach;
			}else if(!empty($to_bcc))$this->text_email($to_bcc, $subject, $text_message,'','',false);
			return true;
		}
	}

	function validate_user_form(&$request, $validate){
		global $helper, $realty; 
		$validate = explode(',', $validate);
		if(!isset($request['alert']))
			$request['alert'] =''; //When all the checkboxes were left unchecked
		foreach($validate as $key=>$value):
			if(!isset($request[$value]) and $value!='date')//If that particular field wasn't chosen to be validated, don't run
					continue;
			switch($value):
				case 'date':
					$request['date'] = date ("Y-m-d H:i:s");
					 //Do not validate the date
				break;
				case 'alert':	//Do not validate alert, just make it an array					
					$this->insert_alerts($request);
				break;
				case 'password':
					$request[$value] = trim($request[$value]); //First trim the password
					if(empty($request[$value]))
						$return .= '<p>Password field is empty.</p>';
					if($request[$value] != trim($request['verify_password']))
						$return .= '<p>The passwords typed do not match.</p>';
					//else $request[$value] = wp_hash_password($request[$value]);
				break;
				default:
					if(!is_array($request[$value])) // Do not trim alert or date
						$request[$value]= trim($request[$value] ); // Trim each value of the request
					if(empty($request[$value]))
						$return .= '<p>'. $helper->humanize($value) . ' field is empty.</p>';		
				break;
			endswitch;
		endforeach;
		return $return;	
	}

	function insert_alerts(&$request){
		global $helper, $wpdb;

		if(empty($request['subscriber_id'] ))$request['subscriber_id'] = $wpdb->insert_id;			
		
		if(!empty($request['keep_informed'])){
			$list_alerts = $this->get_site_alerts();$all_alerts = $this->get_site_alerts(true);
			$news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);
			$i=count($request['alert']);
			foreach($news_alert as $key=>$item_value):
			$request['alert'][$i]=$key;$i++;
			endforeach;
		}
		if(!is_array($request['alert']))return;		
			
		if(!empty($request['alert'])){
			$wpdb->query("DELETE FROM `site_alerts` WHERE `subscriber_id`=" . intval($request['subscriber_id']));	
			foreach( $request['alert'] as $alert):
				if(is_array($request['query_string'][$alert]))$query_string = str_replace('%2C', ',', http_build_query($request['query_string'][$alert]));
				$alerts_sql .="(" . intval($request['subscriber_id']) . ", '".esc_sql($alert)."', '". esc_sql($query_string) . "'), ";
				$query_string = '';
			endforeach;			
			$alerts_sql = substr($alerts_sql,0,-2); //Remove the last comma
			$wpdb->query("INSERT INTO `$this->alerts_table` (`subscriber_id`, `alert`, `query_string`) VALUES $alerts_sql");
		}
	}	
	
	function quick_add(&$request, $admin = false){
		global $helper,$realty, $wpdb; 
		
		$validate = "password,date";
		if(!$admin)	$validate .= ",first_name,last_name,alert";
		if( ($error = $this->validate_user_form($request, $validate)  . $this->validate_email($request['email'])) != '')return $error;
		
		$wpdb->query("INSERT INTO `$this->plugin_table` VALUES('', '".esc_sql($request['title']). "','" . esc_sql($request['first_name']). "','". esc_sql($request['last_name']). "','". esc_sql($request['email']). "','". esc_sql($request['password']). "','". esc_sql($request['home_phone']). "','". esc_sql($request['mobile_phone']). "','". esc_sql($request['referrer']). "','". esc_sql($request['country']). "','". esc_sql($request['comments']). "','". intval($request['property_id']). "','". esc_sql($request['property_address']). "','1','".intval($request['keep_informed'])."',NOW(),NOW())");			
		 
		 $request['subscriber_id']='';
		 if(empty($request['subscriber_id'] ))$request['subscriber_id'] = $wpdb->insert_id;

		$wpdb->query("DELETE FROM `$this->alerts_table` WHERE `subscriber_id`=" . intval($request['subscriber_id']));			
		
		$alerts = $_SESSION['alert_quick_add'];
		if(!empty($alerts)):
			foreach( $alerts as $alert):
				if(is_array($request['query_string'][$alert]))$query_string = str_replace('%2C', ',', http_build_query($request['query_string'][$alert]));
				$alerts_sql .="(" . intval($request['subscriber_id']) . ", '".esc_sql($alert)."', '". esc_sql($query_string) . "'), ";
				$query_string = '';
			endforeach;
		endif;
		$alerts_sql = substr($alerts_sql,0,-2); //Remove the last comma
		$wpdb->query("INSERT INTO `$this->alerts_table` (`subscriber_id`, `alert`, `query_string`) VALUES $alerts_sql");
		
		if($return != true)return $return;
		$_POST = array(); 
		if($admin)return '<a href="'.$this->form_action . '&amp;action=edit&amp;subscriber_id='. $wpdb->insert_id. '">' .$request['email'] . " added.</a><br />";
	}
	
	function process_management_page(){
			global $helper, $wpdb;
			$request = $_REQUEST ; 	
			switch($request['task']):
				case 'edit':
					if(empty($request['alert']))$wpdb->query("DELETE FROM `$this->alerts_table` WHERE `subscriber_id`=" . intval($request['subscriber_id']));
					$return = $this->update_profile($request);
				break;
				case 'send_email_alerts':
					echo $this->send_alerts_by_subscribers($_GET);
					exit(0);					
				break;
				case 'add_new':
					$return = $this->register_user($request, true);
					$this->insert_alerts($request);	
					if(class_exists('ZooRemittance')){
						$note_type='4';
						$comments = trim($request['comments']);
						$comments = str_replace("\n", ";", $comments);
						$comments = str_replace("\r", ";", $comments);
						if(!empty($request['alert'])){
							$temp_array=array();
							foreach( $request['alert'] as $alert):
								$others = '';
								if(is_array($request['query_string'][$alert])){						
									$others=str_replace('%2C', ',', http_build_query($request['query_string'][$alert]));
									$others=str_replace("type_group","property_type_$alert",$others);
									$others=str_replace("bedrooms","min_bedroom_$alert",$others);
									$others=str_replace("bathrooms","min_bathroom_$alert",$others);
									$others=str_replace("carspaces","min_carspace_$alert",$others);
									$others=str_replace("price_min","min_price_$alert",$others);
									$others=str_replace("price_max","max_price_$alert",$others);
									$others=str_replace("suburb","suburbs_$alert",$others);
									$condition.=$others."&";
								}
								 if(!in_array($alert,$temp_array))array_push($temp_array, $alert);										
							endforeach;		
							$temp_array=implode(",",$temp_array);	
						}
						?>
						<script type="text/javascript">
							remmit_back();
							
							function remmit_back(){
								var url    = "<?php echo get_option('siteurl').'/wp-content/plugins/ZooRemittanceMobile/js/contact.php?title='.$request['title'].'&first_name='.$request['first_name'].'&last_name='.$request['last_name'].'&email='.$request['email'].'&home_phone='.$request['home_phone'].'&mobile_phone='.$request['mobile_phone'].'&work_phone='.$request['work_phone'].'&fax='.$request['fax'].'&country='.$request['country'].'&referrer='.$request['referrer'].'&address='.$request['property_address'].'&comments='.$comments.'&keep_informed='.$request['keep_informed'].'&note_type='.$note_type.'&alerts='.$temp_array.'&'.$condition;?>";
								//alert(url);
								jQuery.get(url);
							}
						</script>
					<?php 
					}
				break;
				
				case 'include_categories':					
					if(update_option('subscription_manager_included_categories',$request['included_categories']))
						$return ="<p>Categories included in the available subscriptions.</p>";
				break;
				case 'reinstall':					
					$this->reinstall();$return ="<p>Subscription Managers table was reinstalled successfully.</p>";
				break;
				case 'list_referrers':				
					$list=array();
					$list=explode(",",$_REQUEST['list_referrer']);
					for($i=0;$i<count($list);$i++){
						$chk=$wpdb->get_results("select * from list_referrers where trim(name)='".esc_sql(ucwords(strtolower(trim($list[$i]))))."'");
						$all_list_referer.="'".ucwords(strtolower(trim($list[$i])))."',";
						if(!$chk)$wpdb->query("insert into list_referrers values('','".esc_sql(ucwords(strtolower(trim($list[$i]))))."')");
					}
					$all_list_referer=substr($all_list_referer,0,-1);
					if(!empty($all_list_referer))$wpdb->query("delete from list_referrers where name NOT IN (".$all_list_referer.")");
					else $wpdb->query("delete from list_referrers");
						$return ="<p>New list included in the available list referrers.</p>";
				break;
				case 'form_subscriptions_manager':
					if(!empty($_REQUEST['delete_subscriber_ids'])):  // If at least one email has been checked to be deleted
						$delete_subscriber_ids = implode(', ',$_REQUEST['delete_subscriber_ids']);
						if($this->unsubscribe($delete_subscriber_ids))
							$return .= "<p>Users Deleted.</p>";
					endif;					
					if(!empty($_REQUEST['add_subscribers'])):
						$add_subscribers = explode(",",$_REQUEST['add_subscribers']);
						$_SESSION['alert_quick_add']= $request['alert'];
						foreach($add_subscribers as $email):
							$request['email'] = $email; 	
							if (get_option('is_use_password')=='1')$request['password'] = $request['verify_password'] = $this->default_password;
							$return .= $this->quick_add($request, true);							
						endforeach;
					endif;
				break;
			endswitch;
		return $return;
	}
	
	function all_list_referrers(){
		global $wpdb;
		$result=$wpdb->get_results("select * from list_referrers order by `name`", ARRAY_A);
		return $result;
	}
	
	function get_site_alerts($show_all=false){
		$site_alerts = get_option('subscription_manager_included_categories');
		if($show_all)$site_alerts=$this->alerts_available;
		return $site_alerts;
	}
	
	function get_news_alerts(){
		if(is_array($categories = get_categories("orderby=name"))):			
			$site_alerts=array();
			foreach($categories as $category)
				$site_alerts[$category->slug] = trim(str_replace("&amp;","&",$category->name));
				$site_alerts = array_merge($site_alerts);
			endif;
		return $site_alerts;
	}

	function get_form_query_string($type){
		if($type!='sale' and $type!='lease')// Only these two alerts will have the option to form a query string
			return;
		ob_start();
		require('templates/form_query_string.php');		
		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
	}
}
$subscriptions_manager = new subscriptions_manager;
?>