<?php
global $realty;
$groups=array('Executive','Sales Agent','Property Manager','Admin','Industrial','Office Leasing','Office Sales','Retail','Other');

if(isset($_POST['submit'])){
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'firstname':case 'lastname':case 'email':
					 $return.=$helper->humanize($key)." can not be empty.<br/>"; break;					
				break;
			}
		}		
	}
	if(empty($return)){
		$id=$helper->get_var("select max(id) from users");
		$id=($id<1000000)?1000000:$id+1;
		$position=$helper->get_var("select max(position) from users where office_id in ($office_id)");
		$position=$position+1;
		$created_at=$updated_at=date("Y-m-d h:i:s");		
		$user['firstname']=stripslashes(str_replace('\\','',$user['firstname']));
		$user['lastname']=stripslashes(str_replace('\\','',$user['lastname']));
		$user['description']=stripslashes(str_replace('\\','',$user['description']));

		$helper->query("insert into users (id, firstname, lastname, phone, mobile, fax, email, position, role, `group`, description, facebook_username, twitter_username, linkedin_username, office_id, display, is_zoo, created_at, updated_at, disable_update) values('$id', '".mysql_escape_string($_POST['firstname'])."', '".mysql_escape_string($_POST['lastname'])."', '".$_POST['phone']."', '".$_POST['mobile']."', '".$_POST['fax']."', '".$_POST['email']."', '$position', '".mysql_escape_string($_POST['role'])."', '".$_POST['group']."', '".mysql_escape_string($_POST['description'])."', '".$_POST['facebook_username']."', '".$_POST['twitter_username']."', '".$_POST['linkedin_username']."', '".$_POST['office_id']."', '1', '0', '$created_at', '$updated_at', '1')");
		$return="New agent was successfully saved.";
		unset($_POST);

	}
	else $user=$_POST;	
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
}

$user['firstname']=stripslashes(str_replace('\\','',$user['firstname']));
$user['lastname']=stripslashes(str_replace('\\','',$user['lastname']));
?>
<form method="post" action="">
	<div class="leftside">		
		<table class="widefat">
			<thead>
			<tr>
				<th colspan="2" scope="col" class="manage-column column-title">Agent Data</th>
			</tr>
			</thead> 
			<tr>
                <td class="label"><strong>First Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="widefat" id="firstname" name="firstname"  value="<?php echo $user['firstname']; ?>">						
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Last Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="widefat" id="lastname" name="lastname"  value="<?php echo $user['lastname']; ?>">						
                </td>
            </tr>
           <tr>
                <td class="label"><strong>Office Name<span class="red"> *</span></strong></td>
                <td class="selectOffice">
					<select class="widefat" name="office_id">
						<?php foreach($offices as $office_id=>$name){ ?>
						<option value="<?php echo $office_id; ?>" <?php if($office_id==$user['office_id'])echo 'selected="selected"'; ?>><?php echo $name; ?></option>
						<?php } ?>
					</select>
                </td>
            </tr>
            <tr>
                <td class="label">Role</td>
                <td><input type="text" name="role" class="widefat"  value="<?php echo $user['role']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Group</td>
                <td>
                <select name="group" class="widefat">
                    <option value="">Please Select</option>
                    <?php foreach($groups as $group){ ?>
                    <option value="<?php echo $group; ?>" <?php if($group==$user['group'])echo 'selected="selected"'; ?>><?php echo $group; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Email<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="widefat" id="email" name="email"  value="<?php echo $user['email']; ?>">
                </td>
            </tr>
            <tr>
                <td class="label">Phone</td>
                <td><input type="text" name="phone" class="widefat"  value="<?php echo $user['phone']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Mobile</td>
                <td><input type="text" name="mobile" class="widefat"  value="<?php echo $user['mobile']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Fax</td>
                <td><input type="text" id="fax" name="fax" class="widefat"  value="<?php echo $user['fax']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Description</td>
                <td>
                    <textarea name="description" rows="20" class="widefat"><?php echo $user['description']; ?></textarea>
                </td>
            </tr>
			<tr>
				<td class="label">Facebook account</td>
				<td>www.facebook.com/ <input type="text" name="facebook_username" class="widefat"  value="<?php echo $user['facebook_username']; ?>"></td>
			</tr>	
			<tr>
				<td class="label">Twitter account</td>
				<td>www.twitter.com/ <input type="text" name="twitter_username" class="widefat"  value="<?php echo $user['twitter_username']; ?>"></td>
			</tr>	
			<tr>
				<td class="label">Linkedin account</td>
				<td>www.linkedin.com/ <input type="text" name="linkedin_username" class="widefat"  value="<?php echo $user['linkedin_username']; ?>"></td>
			</tr>		
		 </table> 
		 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</div>
			
	<div id="poststuff">
		<div class="postbox">
			<h3>Actions</h3>		
			<div class="inside">
				<p class="submit">
				<input name="submit" type="submit" value="Save changes" class="button"/>
				</p>
				<a href="themes.php?page=theme-settings&tab=team" class="button">Back</a>
				</p>			
			</div>	
		</div>
	</div>
</form>