<?php 
global $helper;
?>
<div style="display:none;">
	<div itemscope itemtype="http://schema.org/Product">
	  <span itemprop="brand"><?php echo $realty->property['headline']; ?></span>
	  <span itemprop="name"><?php echo $realty->property['headline']; ?></span>
	  <img itemprop="image" src="<?php echo $realty->property['photos'][0]['medium']; ?>" alt="<?php echo $realty->property['headline']; ?>" />
	  <span itemprop="description"><?php echo $helper->_substr($realty->property['description'], 200); ?> </span>
	  Product #: <span itemprop="mpn"><?php echo $realty->property['id']; ?></span>
	  <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<span itemprop="ratingValue">4.4</span> stars, based on <span itemprop="reviewCount">89
		  </span> reviews
	  </span>
	  <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<?php echo $realty->property['real_price']; ?>
		<meta itemprop="priceCurrency" content="USD" />
		<span itemprop="price"><?php echo $realty->property['real_price']; ?></span>
	  </span>
	</div>
</div>