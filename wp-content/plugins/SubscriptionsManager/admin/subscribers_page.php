<?php //metadata: access_level=1 ?>
<div class="wrap">
<form id="posts-filter" action="" method="get">
<input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
<h2><?php _e('Manage Subscribers '); ?>	<a title="Download subscribers as a CSV file" class="button" href="<?php echo $this->form_action; ?>&amp;task=subscriptions_manager_create_csv">Download List as a CSV File</a>
</h2>
<p id="post-search">
	<input type="text" value="<?php echo $request['keywords']; ?>" name="keywords" />
	<input type="submit" class="button" name="search_subscribers" value="Search Subscribers"/>
</p>
<p>
</p>


<div class="tablenav">
<div class="alignleft">
<select name="search_by_alert">
	<option value="">Filter by Alert</option>
		<?php foreach($list_alerts as $key=>$value): ?>
		<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
		<?php endforeach; ?>
	</select>
<input type="submit" class="button" name="filter_alert" value="Filter" />

</div><!-- end .alignleft-->

<?php //echo $subscribers['pagination_label']; ?>
</div><!-- end .tablenav-->
</form>
<br class="clearer" />
<form name="form_subscriptions_manager" method="post" action="<?php echo $this->form_action; ?>">
<input type="hidden" name="task" id="task" value="form_subscriptions_manager" />
<p title="Delete checked users"><input type="submit" class="button-secondary delete" value="Delete" name="" onclick="return confirm('You are about to delete the users checked.\n\'OK\' to delete, \'Cancel\' to stop.' );" /></p>
<div class="tablenav">
<?php echo $subscribers['pagination_label']; ?>
</div>
<table class="widefat">
  <thead>
  <tr>
  	<th scope="col"><?php _e('Delete') ?></th>   
    <th scope="col"><?php _e('E-mail') ?></th>
	<th scope="col"><?php _e('Name') ?></th>
	<th scope="col"><?php _e('Alert(s)') ?></th>
	<th scope="col"><?php _e('Mobile') ?></th>
	<th scope="col"><?php _e('Home Phone') ?></th>
	<th scope="col"><?php _e('Date Added') ?></th>
	<th scope="col"><?php _e('Action'); ?></th>
  </tr>
  </thead>
  <tbody id="the-list">
<?php if(!empty($subscribers)) foreach($subscribers as $item): if(!is_array($item)) continue;//pagination_label data ?>
	<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">
	
	<td><input type="checkbox" value="<?php echo $item['subscriber_id']; ?>" name="delete_subscriber_ids[]" /></td>
	<td><a href="mailto:<?php echo $item['email']; ?>" title=""><?php echo $item['email']; ?></a></td>
	<td>	<?php echo $item['first_name'] . ' ' . $item['last_name']; ?></td>
	<td>	<?php echo (strlen($item['alert'])>30)?substr($item['alert'], 0, 30).' ...':$item['alert']; ?></td>
	<td>	<?php echo $item['mobile_phone']; ?></td>
	<td>	<?php echo $item['home_phone']; ?></td>
	<td>	<?php echo date("d-m-Y", strtotime($item['updated_at'])); ?></td>
	<td><a href="<?php echo $this->form_action; ?>&amp;action=edit&amp;subscriber_id=<?php echo $item['subscriber_id']; ?>">Edit</a></td>
	</tr>
	
<?php endforeach; ?>
  </tbody>
</table>
</form>
</div>