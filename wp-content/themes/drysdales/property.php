<?php
	/* Template Name: Property */
	global $options, $realty;
	foreach ($options as $value) {if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }}
	get_header();
?>

<div id="content"><?php dynamic_sidebar('Property Page Main Left'); ?></div>
<div id="sidebar"><?php dynamic_sidebar('Property Page Main Right'); ?></div>
<div class="clear"></div>
<div id="bottom"><?php dynamic_sidebar('Map and Walkability'); ?></div>
<?php get_footer(); ?>