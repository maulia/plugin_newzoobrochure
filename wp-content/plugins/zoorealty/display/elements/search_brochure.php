<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require($path.'/wp-load.php');

global $helper, $favourite, $realty, $wpdb;
$params=$_GET;

if(empty($params['status'])){
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']=='1')$params['status']='1,2,4,6';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']!='1')$params['status']='1,2,4';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']=='1')$params['status']='1,4,6';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']!='1')$params['status']='1,4';
}
if($params['opentimes']!=''){
	extract($params);		
	extract($realty->search($params));		
	if(!empty($days))$condition.=" AND DATEDIFF(opentimes.date,CURDATE()) <= ".intval($days);	
	//if(!empty($weekday))$condition.=" AND DAYNAME(`date`) = '$weekday' ";					
	$condition.=" and properties.id=opentimes.property_id AND opentimes.date >= CURDATE() ";
	
	if($params['order']=='created_at' || empty($params['order']))$order='ORDER BY `date` , `start_time` , `suburb`'; else $order='ORDER BY '.$params['order'];
	
	$tbl_properties = "properties";
	$sql="SELECT  properties.id, type, user_id, secondary_user, latitude, longitude,  description, (garage + carport + off_street_park) AS carspaces, bedrooms, show_price as display_sold_price, bathrooms, headline, postcode, property_type ,display_price as price_display, price, display_price_text as price_display_value, floor_area as building_size, land_area as land_size, status, deal_type, display_address, unit_number, street_number, street, state, suburb, `date` as open_date, `start_time`, `end_time`, 
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,			
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_date else '' END)as auction_date,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_time	else '' END)as auction_time,
			(Case when auction_date >= curdate() and forthcoming_auction='1' Then auction_place	else '' END)as auction_place,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='floor_area_metric' LIMIT 1) AS floor_area_metric,
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='land_area_metric' LIMIT 1) AS land_area_metric,		
			(SELECT meta_value FROM property_meta WHERE property_meta.property_id = properties.id and meta_key='price_period' LIMIT 1) AS price_period
			from properties , opentimes where $condition $order";	

	$results_count = $wpdb->get_var("SELECT COUNT(*) AS results_count From properties , opentimes where $condition");
	$properties = $wpdb->get_results($sql, ARRAY_A);
	if(!empty($properties)){
		$placeholder_image= $realty->paths['theme'].'images/download.png';
		foreach($properties as $key=>$row){
			$thumbnail=$wpdb->get_var("SELECT url FROM attachments WHERE parent_id = ".intval($row['id'])." and description IN('thumb') and type='photo' and is_user='0' and position='0' LIMIT 1");
			$properties[$key]['thumbnail']=($thumbnail=='')?$placeholder_image:$thumbnail;
		}
	}
	if(is_array($properties))array_walk($properties,array( $realty, 'format_property'));		
	$properties['results_count'] = $results_count;	
}
if($params['fav']!=''){ 
	if($favourite->fav_string() > 0)$query = $favourite->fav_string();
	if(empty($query))return print "Sorry, no property were saved as favourites.";
	$params['id']=explode(",",$query);
}
if(is_null($properties))$properties = $realty->properties($params);

if($properties['results_count']<1)return print "Sorry, no property found.";

require('fpdf.php');
require('fpdf_cache.php');
ob_start();
function remove_weird_characters($text){
	$text = iconv('UTF-8', 'windows-1252', $text);
	return $text;
}

class PDF extends FPDF_Cache{
	function Header(){
		global $realty;
		$logo='images/logo.jpg';
		$this->SetFillColor(59,59,59);
		$this->Rect(0,0,210,20,'F');
		$this->Image($logo,5,5,'',10);	
	}
	
	function Footer(){
		$this->SetFillColor(59,59,59);
		$this->Rect(0,295,210,2,'F');
	}
}	

$pdf=new PDF('P','mm','A4');
$odd_class = "";
$pdf->SetAutoPageBreak(True, 5); //Earlier 40
$pdf->AliasNbPages();
$pdf->AddPage();

$y=22;
$i=1;
$current_date='';
if($_GET['opentimes']!='1'){
$pdf->SetTextColor(59,59,59);
$pdf->SetFont('Helvetica','B',14);
$pdf->SetXY(5, $y+2);
if($_GET['fav']=='1')$text='Favourite Listings';
else if($_GET['opentimes']=='1')$text="Open For Inspection ".$helper->humanize($_GET['list']);
else $text=$helper->humanize($_GET['list'])." Listings";
$pdf->MultiCell(210, 5, $text, 0, 'C');	
$y=$y+8;
}
$placeholder = "images/download.jpg";
foreach($properties as $property){
	if(!is_array($property))continue;
	extract($property);
	
	if($current_date!=$open_date){
		$current_date=$open_date;
		$pdf->SetTextColor(59,59,59);
		$pdf->SetFont('Helvetica','B',14);
		$pdf->SetXY(5, $y+2);
		$pdf->MultiCell(210, 5, "Open Inspection for ".date("l j F",strtotime($open_date)), 0, 'C');	
		$y=$y+8;
	}
	
	$pdf->SetDrawColor(0,0,0);
	$pdf->Line(5,$y,205,$y);
	
	//image
	$type = image_type_to_extension(exif_imagetype($thumbnail));
	if (preg_match('/jpg|jpeg/i',$type))$pdf->Image($thumbnail,5,$y+2,28,21, 'JPG');
	else $pdf->Image($placeholder,5,$y+2,28,21, 'JPG');
	
	//subuub
	$pdf->SetTextColor(59,59,59);
	$pdf->SetFont('Helvetica','B',7);
	$pdf->SetXY(35, $y+3);
	$pdf->MultiCell(25, 4, $suburb, 0, 'L');	
	
	//headline, desc & contacts
	$pdf->SetXY(62, $y+3);
	$pdf->MultiCell(70, 4, $headline, 0, 'L');	
	
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Helvetica','',7);		
	$pdf->SetXY(62, $pdf->GetY());
	$description = preg_replace("[\t\n\r]", "", $description);
	$desc_length=150-((floor(strlen($headline)/50))*50);
	$pdf->MultiCell(75, 4, $helper->_substr(remove_weird_characters($description),$desc_length)."..", 0, 'L');	

	$user_id_all='';
	if(!empty($user_id))$user_id_all=$user_id.",";
	if(!empty($secondary_user))$user_id_all.=$secondary_user.",";
	$user_id_all=substr($user_id_all,0,-1);
	if(!empty($user_id_all)){
		$users=$wpdb->get_results("select phone, CONCAT(firstname, ' ', lastname) as name from users where id in(".esc_sql($user_id_all).")", ARRAY_A);
		if(!empty($users)){
			$contact='';
			foreach($users as $user){
				extract($user);
				if(!empty($name))$contact.="$name $phone ";				
			}
			$pdf->SetFont('Helvetica','B',7);		
			$pdf->SetXY(62, $y+19);
			$pdf->MultiCell(75, 4, $contact, 0, 'L');	
		}		
	}	
	
	//rooms
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Helvetica','',7);
	$pdf->SetXY(139, $y+7);
	$pdf->MultiCell(6, 4, $bedrooms, 0, 'L');
	$pdf->SetXY(139, $y+11);
	$pdf->MultiCell(6, 4, $bathrooms, 0, 'L');
	$pdf->SetXY(139, $y+15);
	$pdf->MultiCell(6, 4, $carspaces, 0, 'L');
	$rooms='images/rooms.jpg';
	$pdf->Image($rooms,143.5,$y+7,'',12);
	
	//price or auction
	$pdf->SetXY(152, $y+7);
	if(!empty($auction_date)){
		$price="Auction\n".date("D j M",strtotime($auction_date));
		if(!empty($auction_place))$price.="\n".$auction_place;
		if(!empty($auction_time) && $auction_time!='00:00:00')$price.=", ".date('g:ia', strtotime($auction_time));		
	}
	if(!empty($price))$pdf->MultiCell(25, 4, $price, 0, 'L');
	
	if($_GET['opentimes']!='1')$opentimes=$wpdb->get_results("select `date`, start_time, end_time from opentimes where property_id=".intval($id)." and `date` >= CURDATE() and DATEDIFF(opentimes.date,CURDATE()) <= 7 order by `date` limit 3", ARRAY_A);
	else $opentimes=$wpdb->get_results("select `date`, start_time, end_time from opentimes where property_id=".intval($id)." and datediff(`date`,'$open_date')=0", ARRAY_A);
	if(!empty($opentimes)){		
		$inspect="Inspect\n";
		foreach($opentimes as $times){
			extract($times);
			$inspect.=date("D",strtotime($date))." ".date('g:i', strtotime($start_time))."-".date('g:ia', strtotime($end_time))."\n";		
		}	
		$pdf->SetXY(179, $y+7);
		$pdf->MultiCell(30, 4, $inspect, 0, 'L');
	}
	
	if($i%10==0 && $_GET['opentimes']!='1' && $i!=$realty->params['limit']){
		$pdf->AddPage();
		$y=22;
	}
	else if($i%8==0 && $_GET['opentimes']=='1' && $i!=$realty->params['limit']){
		$pdf->AddPage();
		$y=22;
	}
	else $y=$y+25;
	$i++;
}
$pdf->Output();
?>