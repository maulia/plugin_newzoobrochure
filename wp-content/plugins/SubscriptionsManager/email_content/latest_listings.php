<tr>
	<td align="center" colspan="2" style="background:<?php echo $sm_heading_color; ?>; border-top:solid 1px #fff;color:#<?php echo $sm_heading_font_color; ?>;"><h3 style="margin:10px 0; padding:0; font-size:16px; "><?php if($alert=='latest_sale')$alert='sale';if($alert=='latest_lease')$alert='lease';echo $site_alerts[$alert]; ?></h3></td>
</tr>
<?php 
foreach($properties as $property): if(!is_array($property)) continue; extract($property,EXTR_PREFIX_ALL,"property"); 
?>
    <tr>    
        <td style="width:224px; padding:20px 0; border-bottom: 1px solid #d2cebb; vertical-align: top;">
            <div><a href="<?php echo $property_url; ?>" title="<?php echo $property_headline; ?>" style="padding:2px; border:solid 1px #ccc; display:block; width:200px; max-height:150px; margin:5px 16px 0 0;"><img src="<?php echo $property_thumbnail; ?>" alt="<?php echo $property_street_address; ?>" border="0" /></a></div>
        </td>        
        <td style="padding:20px 0; border-bottom:1px solid #d2cebb; vertical-align:top;">
        	<h4 style="font-size:14px; text-transform:capitalize; border-bottom:dotted 1px #ddd; padding:0 0 5px; margin:0 0 5px; "><?php echo ($property_street_address!='')?"$property_street_address, $property_suburb":$property_suburb; ?></h4>
            <p style="margin:0 0 20px; padding:0; font-style:italic; float:left; font-size:12px;"><?php echo $property_property_type; ?></p>
            
            <?php if ( !$property_is_land): ?>
            <ul style="margin:0; padding:0; list-style: none; float:right; font-size:12px;">
            	<?php if(!empty($property_bedrooms)){ ?><li style="float:left; margin:0 10px 0 0;"><span style="float:left; padding:0 10px 0 0;"><?php echo $property_bedrooms; ?></span><span style="float:left;"><img src="<?php echo $sm_bed; ?>" style="margin:0 5px 0 0;" /></span></li><?php } ?>
                <?php if(!empty($property_bathrooms)){ ?><li style="float:left; margin:0 10px 0 0;"><span style="float:left; padding:0 10px 0 0;"><?php echo $property_bathrooms; ?></span><span style="float:left;"><img src="<?php echo $sm_bath; ?>" style="margin:0 5px 0 0;" /></span></li><?php } ?>
                <?php if(!empty($property_carspaces)){ ?><li style="float:left; margin:0 10px 0 0;"><span style="float:left; padding:0 10px 0 0;"><?php echo $property_carspaces; ?></span><span style="float:left;"><img src="<?php echo $sm_cars; ?>" style="margin:0 5px 0 0;" /></span></li><?php } ?>
            </ul>
            <?php endif; ?>
            
            <p style="margin:0; padding:0; clear:both; line-height:18px; font-size:12px; ">
            <?php echo substr($property_description,0, 190); ?> &hellip;
            <a href="<?php echo $property_url; ?>" style="color: #8e855e; text-decoration:none;">View more <span style="font-family:Georgia, 'Times New Roman', Times, serif">&raquo;</span></a>
            </p>
            
            <p class="property_price" style="font-size:12px;"><?php echo (empty($property_auction_date)) ? $property_price : "Auction, $property_auction_date " . strtolower ($property_auction_time); ?></p>
            
            <div class="land_building_size">
            <?php if(!empty($property_land_size)): ?>
            <p style="margin:0 0 6px;padding: 0; font-size: 15px;color: #484848;">Land Size: <span style="color: #969698;"><?php echo $property_land_size.$property_land_area_metric ; ?></span></p>
            <?php endif;?>
            <?php if(!empty($property_building_size)): ?>
            <p style="margin:0;padding: 0; font-size: 15px;color: #484848;">Building Size: <span style="color: #969698;"><?php echo $property_building_size.$property_floor_area_metric ; ?></span></p>
            <?php endif; ?>
            </div>
        </td>
	</tr>
<?php endforeach; //properties ?>