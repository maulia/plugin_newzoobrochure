<div class="wrap">	
	<h2>Alerts For Contact <?php echo $subscribers['first_name'] . ' ' . $subscribers['last_name']; ?></h2>
	<?php if(!empty($details)): ?>
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col"><?php _e('Delete') ?></th>
			<th>Date</th>
			<th>Alert Type</th>
			<th>Property Type</th>
			<th>Min Price</th>
			<th>Max Price</th>
			<th>Suburbs</th>
		</tr>
		</thead>
		<?php $i=0;foreach($details as $detail){ $i++; ?>
		<tr <?php if(i%2==0)echo 'class="alternate"'; ?>>
			<td>
			<a href="<?php echo $this->form_action;?>&amp;task=delete_detail&amp;id=<?php echo $detail['id']; ?>" onClick="return confirm('You are about to delete the this alert.\n\'OK\' to delete, \'Cancel\' to stop.' );">Delete</a>
			</td>
			<td><?php echo date("d-m-Y", strtotime($detail['data_added'])); ?></td>
			<td><?php echo $detail['alert_type']; ?></td>
			<td><?php echo ($detail['listing_type']=='')?' - ':$detail['listing_type']; 
			echo ($detail['property_type']=='')?' - ':$detail['property_type']; 
			echo "<br>";
			echo "Min Bed: ";echo ($detail['min_bedroom']=='0')?' - ':$detail['min_bedroom']; 
			echo "Min Bath: ";echo ($detail['min_bathroom']=='0')?' - ':$detail['min_bathroom']; 
			echo "Min Car: ";echo ($detail['min_carspace']=='0')?' - ':$detail['min_carspace']; 
			?>
			</td>
			<td><?php echo ($detail['min_price']=='0')?' - ':$detail['min_bedroom']; ?></td>
			<td><?php echo ($detail['max_price']=='0')?' - ':$detail['max_price'];?></td>
			<td><?php echo $detail['suburbs']; ?></td>

		</tr>
		<?php } ?>
	</table> 
	<?php else : echo '<p>There are no alerts for this contact</p>';	endif ; ?>
	<p><a href="<?php echo $this->form_action;?>"><?php _e('&laquo; Back To Main Page'); ?></a></p>
</div>