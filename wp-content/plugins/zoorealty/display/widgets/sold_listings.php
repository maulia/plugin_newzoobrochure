<div id="sold_listings">
<?php
foreach($settings as $type=>$values):
	if($values['Number'] <1 || $type =='display') continue; //Only if any properties were requested to be displayed for this type.
		$sold_listings = $realty->sold(array('limit'=>$values['Number'], 'list'=>$type));
if($sold_listings['results_count'] >0): 
?>
<?php echo $before_title; ?><?php echo $values['Title'];?><?php echo $after_title; ?>
<div id="sold_<?php echo $type; ?>" class="block_content">
<table>
<tr>
	<?php foreach($settings['display'] as $display=>$empty): if($display=='thumbnail')continue; ?>
	<th><?php 
	switch($display):
		case 'street_address': $display='Address'; break;	
		case 'suburb': $display='Suburb'; break;	
		case 'property_type': $display='Property Type'; break;	
		case 'bedrooms': $display='Beds'; break;	
		case 'bathrooms': $display='Baths'; break;	
		case 'carspaces': $display='Cars'; break;	
		case 'last_price': $display='Price'; break;	
	endswitch;
	echo $display; ?></th>
	<?php endforeach; ?>
</tr>
<?php
		$count=0; 
		foreach ($sold_listings as $property): 
			if(!is_array($property)) continue;
?>
<tr<?php echo ($count % 2== 0)? '': ' class="alt"'; ?>>
<?php foreach($settings['display'] as $display=>$empty):  ?>
<td class="<?php echo $display; ?>">
<?php if($display =='thumbnail'): ?>
<a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>"><img src="<?php echo $property['thumbnail']; ?>"  alt="<?php echo $property['headline']; ?>" /></a>
<?php elseif($display =='suburb'): ?>
<a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>"><?php echo $property[$display]; ?></a>
<?php elseif($display =='last_price'): ?>
<?php echo ($property['display_sold_price'])? $property['last_price']:'Undisclosed'; ?>
<?php else: ?>
	<?php echo $property[$display]; ?>
<?php endif;?></td>
<?php endforeach; ?>
</tr>
<?php
		$count++; 
		endforeach;
?>
</table>
<div class="clearer"></div>
<p class="button view_<?php echo $type; ?>_btn"><a title="View all recent <?php echo $type; ?>" href="<?php echo $realty->get_permalink(($type=='sale')? 'sold':'leased'); ?>" class="btn">View All</a></p>
</div><!-- ends #sold_<?php echo $type; ?> -->

<?php
	endif; //$sold_listings['results_count'] >0): 
endforeach; //$settings ?>
</div>