<?php
function register_menu() {
	register_nav_menus( array(
		'primary'=>'Primary Menu',
		'secondary'=>'Secondary Menu'
	));
}
add_action( 'init','register_menu' );

if ( function_exists( 'add_theme_support' ) ) {
add_theme_support( 'post-thumbnails' );
}

function body_css_id() {
	global $post, $wpdb;
	if (is_404()) { echo "fourOfour"; } 
	elseif ( is_home() ) { echo "home"; }
	elseif ( is_home() || is_category() || is_archive() || is_search() || is_single() || is_date() ) { echo "blog"; } 
	elseif (is_page()) {
		$parent_name = $wpdb->get_var("SELECT post_name FROM $wpdb->posts WHERE ID = '$post->post_parent;'");
		echo $post->post_name;
	} else { echo 'notes'; }
}

function customize_image($orientation, $width, $height, $max_width, $max_height, $limit=0, $image_source='') {
    if(empty($width) && !empty($image_source))list($width, $height, $type, $attr) = getimagesize($image_source);
	if(empty($width))return $attributes = array('width' => $max_width, 'height' => $max_height, 'class' => 'landscape', 'margin_top' => '0px');
	
	switch($orientation) {
		case 'horizontal':
			$resized_ratio = $width / $max_width;
    		$resized_width = $max_width;
    		$resized_height = $height / $resized_ratio;
    		$view_type = ($resized_height > ($max_height + $limit)) ? 'portrait' : 'landscape';
    		$cropped_size = ($resized_height > $max_height) ? '-'.(($resized_height - $max_height) / 2).'px' : '0px';
			$attributes = array('width' => $resized_width, 'height' => $resized_height, 'class' => $view_type, 'margin_top' => $cropped_size);
			break;
		case 'vertical':
			$resized_ratio = $height / $max_height;
    		$resized_height = $max_height;
    		$resized_width = $width / $resized_ratio;
    		$cropped_size = ($resized_width > $max_width) ? '-'.(($resized_width - $max_width) / 2).'px' : '0px';
			$attributes = array('width' => $resized_width, 'height' => $resized_height, 'margin_left' => $cropped_size);
			break;
	}
	
    return $attributes;
}

?>