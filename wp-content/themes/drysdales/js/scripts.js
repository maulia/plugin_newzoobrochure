(function ($) {
    $(function() {
		if ($.browser.msie) {
			if ($.browser.version < 7) $("html").addClass("die lt-ie9 lt-ie7");
			if ($.browser.version == 7) $("html").addClass("die lt-ie9 ie7");
			if ($.browser.version == 8) $("html").addClass("die lt-ie9 ie8");
			if ($.browser.version == 9) $("html").addClass("die ie9");
			if ($.browser.version == 10) $("html").addClass("die ie10");
		}
		if ($.browser.webkit) { $("html").addClass('webkit'); }
		if ($.browser.mozilla) { $("html").addClass('ff'); }
		
		$('li:first-child').addClass('first');
		$('li:last-child').addClass('last');
		
		var a=$('#featured_listings a.image, #realty_widget_media a#slide_photo_ref, #listings .image a');
		var b=a.outerHeight(true);
		$(a).find('img').each(function() {
            var c=$(this).outerHeight(true);
			if(c>b){
				var d=(c-b)/2;
				$(this).css({'margin-top':-d});
			}
        });
	});
}(jQuery));

function center_image(container,image) {
	var c=$(this).outerHeight(true);
	if(c>b){
		var d=(b-a)/2;
		$(this).css({'margin-top':-d});
	}
}


jQuery(function() {
	var bodyClass='';
	if (jQuery.browser.mozilla) bodyClass='ff';
	if (jQuery.browser.msie) {
		if (jQuery.browser.version == 7) bodyClass='ie lt-ie9 lt-ie11 ie7';
		if (jQuery.browser.version == 8) bodyClass='ie lt-ie9 lt-ie11 ie8';
		if (jQuery.browser.version == 9) bodyClass='ie lt-ie11 ie9';
		if (jQuery.browser.version == 10) bodyClass='ie lt-ie11 ie10';
	}
	if (navigator.userAgent.indexOf('WebKit') + 1) { 
		bodyClass='webkit';
		bodyClass+=((navigator.vendor || '').indexOf('Apple') + 1)?' safari':' chrome';
	}
	if (navigator.userAgent.match(/rv:11.0/)) bodyClass='ie ie11';
	jQuery('body').addClass(bodyClass);
});



jQuery(function() {
	var w = jQuery(window).width();
	if (w < 1024) {
		jQuery('#menu-main-nav li').each(function() {
			if(jQuery('.sub-menu',this).length) {
				jQuery('> a',this).attr('href','javascript:void(0);').addClass('toggle-subnav');
				jQuery('<i class="fa fa-angle-down m"></i>').appendTo(jQuery('> a',this));
			}
		});
		
		jQuery('<li class="moved_link"></li>').prependTo(jQuery('.menu li.menu-item-467 .sub-menu'));
		jQuery('<a>',{
			text: 'Selling',
			title: 'Selling',
			href: 'http://www.exploreproperty.com.au/selling/',
			click: function(){ BlahFunc( options.rowId );return false;}
		}).appendTo('.moved_link');
		
		
		jQuery('.menu li.menu-item-469 .fa-angle-down').on('click', function(e){
			jQuery('.menu li.menu-item-469 .sub-menu').toggleClass('slide');
		});	
		jQuery('.menu li.menu-item-471 .fa-angle-down').on('click', function(e){
			jQuery('.menu li.menu-item-471 .sub-menu').toggleClass('slide');
		});	
		jQuery('.menu li.menu-item-467 .fa-angle-down').on('click', function(e){
			jQuery('.menu li.menu-item-467 .sub-menu').toggleClass('slide');
		});	
		jQuery('.menu li.menu-item-665 .fa-angle-down').on('click', function(e){
			jQuery('.menu li.menu-item-665 .sub-menu').toggleClass('slide');
		});		
	}
});



//nav
jQuery(function() {	

	//href replace
	jQuery('#menu-main-menu li').each(function() {
		if(jQuery('.sub-menu',this).length) {
			jQuery('> a',this).attr('href','javascript:void(0);').addClass('toggle-subnav');
			jQuery('<i class="fa fa-angle-down m"></i>').appendTo(jQuery('> a',this));
		}
	});
	
	jQuery('ol.cf-ol ul.prop_alerts_ul > li').attr('id', function(i) {
	   return 'list'+(i+1);
	});
	
	//nav toggle
	jQuery('.toggle-menu').click(function(e){
		e.stopPropagation();
		jQuery('nav[role="navigation"]').toggleClass('shrink');
	});
	
	//subnav toggle
	jQuery('a.toggle-subnav').on('click', function(e){
		e.stopPropagation();
		jQuery('#menu-main-menu li').not(jQuery(this).parents('li')).removeClass('shrink');
		jQuery(this).closest('li').toggleClass('shrink');
	});
	
	//close nav
	jQuery('body').on('click',function(e){
		var a=jQuery(e.target),b=a.parents();
		if(!a.is('a')&&!b.is('a')) {
			if(jQuery('nav[role="navigation"]').hasClass('shrink')) jQuery('nav[role="navigation"]').removeClass('shrink');
			else return;
		}
	});

	//Property Alert
	jQuery('.opt_sale').wrapAll("<div class='opt_sale_Wrap' />");
	jQuery('.opt_lease').wrapAll("<div class='opt_lease_Wrap' />");
});
