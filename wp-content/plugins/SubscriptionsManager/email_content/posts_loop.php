<tr>
	<td align="center" colspan="2" style="background:<?php echo $sm_heading_color; ?>; border-top:solid 1px #fff; color:#<?php echo $sm_heading_font_color; ?>">
		<h4 style="margin:10px 0; padding:0; font-size:16px; ">Latest News</h4>
	</td>
</tr>
<?php 
foreach($posts as $post):
 setup_postdata($post);
 ?>
	<tr>
		<td><a href="<?php the_permalink() ?>" id="post-<?php the_ID(); ?>" style="font-size: 18px;color: #ee3224;"><?php the_title(); ?></a></td>
	</tr>
	<tr>
		<td style="font-size:15px;line-height: 24px;padding: 0 0 5px;"><?php the_excerpt_reloaded(50, '<a><img>[caption', 'excerpt', TRUE, 'Continue reading &#187;'); ?> </td>
	</tr>
	<tr>
		<td style="font-size:15px;padding: 0 0 5px;border-bottom: 1px solid #d3d3d3;"><span style="color: #969698;"><?php the_time('jS F, Y') ?>&nbsp;&#8226;&nbsp;<?php the_author() ?></span></td>
	</tr>
<?php
endforeach; ?>