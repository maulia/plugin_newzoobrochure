<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require($path.'/wp-load.php');
global $realty, $wpdb; 
if(!empty($_REQUEST['property_id'])){
	$properties=$realty->properties(array('id'=> $_REQUEST['property_id'], 'limit'=>1));
	$property= $properties[0];
}
if(!empty($_REQUEST['office_id'])){
	$properties=$wpdb->get_results("select latitude, longitude from office where id=".$_REQUEST['office_id'], ARRAY_A);
	$property= $properties[0];
}
wp_print_scripts('jquery');
?>
<html>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<style type="text/css">
	body { margin:0; }
	h2.map-address { background:#3b3b3b; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:normal; height:35px; line-height:35px; margin:0 0 5px; padding:0 0 0 5px; }
</style>
<body>
<h2 class="map-address"><?php echo $property['street_address']; ?></h2>
<div class="property_map">
	<div id="map_canvas" style="width:592px;height:366px;">
	</div>
</div>
</body>
<script type="text/javascript">
/* <![CDATA[ */
var map;
jQuery(document).ready(function(){
	load_map ();
});
function load_map (){
	var myLatLng = new google.maps.LatLng(<?php echo $property['latitude']; ?>,<?php echo $property['longitude']; ?>);
	var myOptions = {
		zoom: 12,
		center:myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};	
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);	
	var Marker = new google.maps.Marker({  position: myLatLng, map: map });
}
/* ]]> */
</script>
</html>