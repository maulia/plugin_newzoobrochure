<?php
$path = dirname(dirname(dirname(dirname(__FILE__))));
require($path.'/wp-load.php');
global $wpdb;

// read agent page
$sql_posts = $wpdb->get_results( "SELECT * FROM wp_posts WHERE post_content LIKE '%template=office%' AND post_status = 'publish'", ARRAY_A );

$insert_data=0; $delete_data=0;

foreach ($sql_posts as $sql_post) {
	
	// AIO data for page
	$page_title_seo = $sql_post['post_title'].' compare properties and real estate listings';
	$page_desc_seo = 'Compare properties of '.$sql_post['post_title'];
	$page_keyword_seo = 'Real Estate Agent, estate agent, property agent, property sales, real estate sales, sell my property, sell my house, rent my house';

	// cek meta data for agent pages
	$meta_ceks = $wpdb->get_results( "SELECT DISTINCT post_id FROM wp_postmeta WHERE post_id ='".intval($sql_post['ID'])."' AND meta_key = '_aioseop_title' OR post_id ='".intval($sql_post['ID'])."' AND meta_key = '_aioseop_description' OR post_id ='".intval($sql_post['ID'])."' AND meta_key = '_aioseop_keywords'", ARRAY_A );

	// if agent page in array and have seo data
	if ( $meta_ceks ){
		
		// delete existing seo data first
		$wpdb->delete( 
            'wp_postmeta', 
            array( 
                'post_id' => intval($sql_post['ID']), 
                'meta_key' => '_aioseop_title' 
            ), 
            array( 
                '%s', 
                '%s',  
                '%s' 
            ) 
        );
		$wpdb->delete( 
            'wp_postmeta', 
            array( 
                'post_id' => intval($sql_post['ID']), 
                'meta_key' => '_aioseop_description'
            ), 
            array( 
                '%s', 
                '%s',  
                '%s' 
            ) 
        );
        $wpdb->delete( 
            'wp_postmeta', 
            array( 
                'post_id' => intval($sql_post['ID']), 
                'meta_key' => '_aioseop_keywords' 
            ), 
            array( 
                '%s', 
                '%s',  
                '%s' 
            ) 
        );

        // count
		$delete_data++;
	}

	// then insert new seo data			
    $wpdb->insert( 
        'wp_postmeta', 
        array( 
            'post_id' => intval($sql_post['ID']), 
            'meta_key' => '_aioseop_title',  
            'meta_value' => $page_title_seo 
        ), 
        array( 
            '%s', 
            '%s',  
            '%s' 
        ) 
    );
	$wpdb->insert( 
        'wp_postmeta', 
        array( 
            'post_id' => intval($sql_post['ID']), 
            'meta_key' => '_aioseop_description',  
            'meta_value' => $page_desc_seo 
        ), 
        array( 
            '%s', 
            '%s',  
            '%s' 
        ) 
    );
    $wpdb->insert( 
        'wp_postmeta', 
        array( 
            'post_id' => intval($sql_post['ID']), 
            'meta_key' => '_aioseop_keywords',  
            'meta_value' => $page_keyword_seo 
        ), 
        array( 
            '%s', 
            '%s',  
            '%s' 
        ) 
    );

    // count
    $insert_data++;	
}

// print result
echo 'complete!!!<br />';
echo 'delete data: '.$delete_data.'<br />';
echo 'insert data: '.$insert_data;
?>