<?php
class realty_rss{
	function realty_rss($list, $title){
		$this->listing= $list;
		$this->title = $title;			
		$this->description = "New Real Estate properties from " . $this->title;	
		ob_start();
		ob_get_clean();
		ob_end_clean();
		header("Content-Type: text/xml; charset=ISO-8859-1");
		// stats rss start
		if($this->listing !='stats')echo ($this->listing =='google')? $this->g_build():$this->build(); 
		else{
			$this->title = str_replace(" for $list","",$this->title);
			$this->description = str_replace(" for $list","",$this->description);
			echo $this->stat_build(); 
		}
		// stats rss end
		exit(0); //terminate so the html doesn't output
	}

	// stats rss start
	function stat_build(){
		global $realty, $helper, $table_prefix, $wpdb; 
		echo '<?xml version="1.0" encoding="ISO-8859-1"?><?xml-stylesheet type="text/css" href="'.$this->pluginUrl.'style/rss.css" ?>';?>
		<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
		<channel>
			<title><?php echo $this->title ; ?></title>
			<link><?php echo  $realty->siteUrl ; ?></link>
			<description><?php echo  $this->description ; ?></description>
			<language>en-gb</language>
    		<lastBuildDate><?php echo  date("r" ) ; ?></lastBuildDate>			
			<atom:link href="<?php echo $realty->siteUrl; ?>?rss" rel="self" type="application/rss+xml" />
<?php		
		$office_id=implode(', ', $realty->settings['general_settings']['office_id']);
		$results=$wpdb->get_results("SELECT id, suburb, state, postcode, unit_number, street_number, street, created_at, property_type, type, deal_type FROM  properties WHERE status in (1,4) and office_id in($office_id) order by suburb, street", ARRAY_A);
		ob_start();			
		if($results) foreach($results as $result):
			if(!is_array($result)) continue;
			if(class_exists('zoo_analytics')){
				$plugin_table = $table_prefix . "property_analytics";
				$result['grand_total']=$wpdb->get_var("select count(id) from $plugin_table where property_id in (".$result['id'].") and category in ('visit')");
			}
			if(!class_exists('zoo_analytics') && class_exists('visits_tracker')){
				$plugin_table = $table_prefix . "visits_tracker";
				$result['grand_total']= $wpdb->get_var("SELECT COUNT(id) from $plugin_table where property_id=".$result['id']);
			}
			
	?>
	<item>
		<title><?php if(!empty($result['unit_number']))echo $result['unit_number']."/";
		if(!empty($result['street']))echo htmlspecialchars(trim($result['street_number']." ".$result['street'])).", ";
		echo $result['suburb'].", ".$result['state']." ".$result['postcode']; ?></title>						
		<link><?php echo $realty->siteUrl.$result['id'].'/'; ?></link>
		<property_id><?php echo $result['id']; ?></property_id>
		<view><?php echo $result['grand_total']; ?></view>
		<description><![CDATA[
		<?php 
		echo "Property ID: ".$result['id']."<br/>Type: ";
		echo (strpos($result['type'], 'Sale') || strtolower($result['deal_type'])=='sale') ? 'For Sale':'For Lease';
		echo "<br/>Property Type: ".$result['property_type']."<br/>View: ".$result['grand_total']; ?>
		]]></description>
		<property_type><?php echo $result['property_type']; ?></property_type>
		<for_sale><?php echo strpos($result['type'], 'Sale') ? '1':'0'; ?></for_sale>
		<pubDate><?php 
		echo date("r", strtotime($result['created_at']));
		?>					
		</pubDate>					
		<guid isPermaLink="false"><?php echo $realty->siteUrl.$result['id'].'/'; ?></guid>
	</item>
	<?php
	endforeach;
	?>
</channel>
</rss>	
<?php return ob_get_clean();

	}
	// stats rss end
	
	function build(){
		global $realty, $helper; 
		echo '<?xml version="1.0" encoding="ISO-8859-1"?><?xml-stylesheet type="text/css" href="'.$this->pluginUrl.'style/rss.css" ?>';
?><rss version="2.0">
		<channel>
			<title><?php echo $this->title ; ?></title>
			<link><?php echo  $realty->siteUrl ; ?></link>
			<description><?php echo  $this->description ; ?></description>
			<language>en-gb</language>
    		<lastBuildDate><?php echo  date("r" ) ; ?></lastBuildDate>
<?php		
		//$results = $realty->properties(array('list'=>$this->listing,'limit'=>500));
		$results = $realty->properties(array('list'=>$this->listing,'limit'=>500, 'status'=>'1,4')); //20131030
		
		ob_start();			
		if($results['results_count'] > 0) foreach($results as $result):
			if(!is_array($result)) continue;
			
?><item>
		<?php 
			$text_icon = '';
			if (!empty($result['bedrooms'])) $text_icon .= " - Bedrooms: ". $result['bedrooms'];
			if (!empty($result['bathrooms'])) $text_icon .= " - Bathrooms: ". $result['bathrooms']; 
			if (!empty($result['carspaces'])) $text_icon .= " - Carspaces: ". $result['carspaces'];
		?>
		<title><?php echo $result['property_type'] . ' - ' . htmlspecialchars($result['street_address']) . ' ' . $result['suburb'] . ' ' . $result['state'] . $text_icon; ?></title>						
		<link><?php echo $result['url']; ?></link>
		<description><![CDATA[
		<?php echo htmlspecialchars($result['street_address']) . ' ' .  $result['suburb'] . ' '. $result['state']; ?>
		<h2><?php echo $result['headline']; ?></h2>
		<?php					
		
		if ($result['bedrooms'] >0) echo "Bedrooms: ". $result['bedrooms'];
		if ($result['bathrooms'] >0) echo " - Bathrooms: ". $result['bathrooms']; 
		if ($result['carspaces'] >0) echo " - Carspaces: ". $result['carspaces'];
		if(!$result['is_auction'])
			echo $property['price'].'<br><br>'; 
		echo $helper->_substr($result['description'], 150)."...";?><br><br><img src="<?php echo  $result['thumbnail']; ?>" alt="<?php echo $result['headline'] ; ?>" title="<?php echo  $result['headline']; ?>" />
		]]></description>
		<pubDate><?php echo date("r", time() ) ; //date("r", strtotime ($result['created_at']) ) ; ?></pubDate>				
		<guid isPermaLink="false"><?php echo $result['url']; ?></guid>
	</item><?php
			endforeach;
?></channel>
</rss>
<!-- <atom:link href="<?php echo $realty->siteUrl; ?>?rrss=<?php echo  $this->listing ; ?>" rel="self" type="application/rss+xml" />-->			
<?php return ob_get_clean();

	}
	
function g_build(){
		global $realty, $helper; 
		echo '<?xml version="1.0"?>
<rss version ="2.0" xmlns:g="http://base.google.com/ns/1.0">';
?><channel>
	<title><?php echo $this->title ; ?></title>
			<link><?php echo  $realty->siteUrl ; ?></link>
			<description><?php echo  $this->description ; ?></description>

<?php		
		$results = $realty->properties(array('list'=>$this->listing,'limit'=>500));
		ob_start();			
		if($results['results_count'] > 0) foreach($results as $result):
			if(!is_array($result)) continue;
			$result['photos'] = $realty->photos($result['id'],"photos", array('800x600'));
			$result['user'][] = $realty->user($result['user_id']);
			
?><item>
		<title><?php echo htmlspecialchars($result['headline']); ?></title>
		<description><?php echo htmlspecialchars($result['description']); ?></description>
		<link rel="alternate" type="text/html" href="<?php echo $result['url']; ?>"/>						
		<link><?php echo $result['url']; ?></link>
		<g:location><?php echo $result['street_address'] . ', ' . $result['suburb'] . ', ' .$result['state'] . ' ' . $result['postcode']; ?></g:location>
<?php switch($result['property_type']):
			case "Retail":
				echo  "<g:property_type>commercial</g:property_type>";
				break;
			case "Block of Unit / Flats":
			case "Warehouse":
				echo  "<g:property_type>building</g:property_type>";
				break;			
			case "Duplex / Semi Detached":
				echo  "<g:property_type>duplex</g:property_type>";
				break;		
			case "Holiday Home":case "Acreage / Semi Rural":case "Alpine":	case "FlatHoliday Home":case "New Homes":
				echo  "<g:property_type>house</g:property_type>";
				break;			
			case "Retirement Living":
			case "Villa":
				echo  "<g:property_type>villa</g:property_type>";
				break;			
			case "Serviced Apartment":
				echo  "<g:property_type>apartment</g:property_type>";
				break;	
			case "Studio":
				echo  "<g:property_type>unit</g:property_type>";
				break;	
			case "Terrace":
				echo  "<g:property_type>terrace</g:property_type>";
				break;	
			default:
				echo  "<g:property_type>" . $result['property_type'] ."</g:property_type>";
			break;
		endswitch;
		
		?>
		<g:listing_type>residential for <?php echo strpos($result['property_type'], 'sale') ? 'sale':'lease'; ?>->listing; ?></g:listing_type>";
		<g:agent_firm><?php echo $realty->blogname; ?></g:agent_firm>		
<?php if ($result['bedrooms'] >0) echo "<g:bedrooms>". $result['bedrooms'] . "</g:bedrooms>";
		if ($result['bathrooms'] >0) echo "<g:bathrooms>". $result['bathrooms'] . "</g:bathrooms>" ; 
		if ($result['carspaces'] >0) echo "<g:parking>". $result['carspaces'] ."</g:parking>";
?>
<g:id><?php echo $result['id']; ?></g:id>
<g:price><?php echo (int) $result['price']; ?></g:price>
<?php foreach($result['photos'] as $photo) : ?>
<g:image_link><?php echo $photo['large']; ?></g:image_link>
<?php endforeach; 
if(!empty($result['land_size'])) echo "<g:total_size>" . $result['land_size'] . "</g:total_size>";
?>
<g:primary_contact_person><?php echo $result['user'][0]['name'];?></g:primary_contact_person>
<g:primary_contact_phone><?php echo $result['user'][0]['business_phone'];?></g:primary_contact_phone>
	</item><?php
			endforeach;
?></channel>
</rss>
<!-- <atom:link href="<?php echo $realty->siteUrl; ?>?rrss=<?php echo  $this->listing ; ?>" rel="self" type="application/rss+xml" />-->			
<?php return ob_get_clean();

	}	
}
?>