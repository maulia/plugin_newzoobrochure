<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
require($path.'/wp-load.php');
global $realty, $helper, $wpdb;

$email=$_GET['email'];
if(!$helper->isValidEmail($_REQUEST['email']))$return.="You have entered an invalid email.<br/>";
		
if(empty($return)){
	$timezone_format= date("Y-m-d G:i:s");
	$id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_REQUEST['email'])."'");
	if(!$id){
		$wpdb->insert('subscriptions_manager',
			array(						
				'first_name' => $_REQUEST['first_name'],
				'email' => $_REQUEST['email'],						
				'created_at' => $timezone_format,
				'updated_at' => $timezone_format
				),
			array( 
				'%s', 
				'%s', 
				'%s', 
				'%s' 
			)
		);
	}
	
	$id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_REQUEST['email'])."'");
	
	if(!empty($id)){
		$wpdb->query("DELETE FROM `site_alerts` WHERE `subscriber_id`=" . $id);
		$alerts_sql ="('$id', 'sale'),('$id', 'latest_auction'), ('$id', 'opentimes_sales'),('$id', 'lease'),('$id', 'opentimes_lease')";
		$helper->query("INSERT INTO `site_alerts` (`subscriber_id`, `alert`) VALUES $alerts_sql");	
	}	
	
	$user_subject ="Thank you for subscribing to buyMyplace.";
	$subject ="New user register [$realty->blogname]";
	$user_nessage = "Dear $email,<br/><br/>Thank you for subscribing to our website.<br/><br/>Regards,<br/><br/>$realty->blogname";
	$message = "Dear admin,<br/><br/>
	New user have been registered to this site. Below is the detail:<br/>
	Name: ".$_REQUEST['first_name']."<br/>
	Email: $email<br/>
	Postcode: ".$_REQUEST['postcode']."<br/><br/><br/> 
	Regards,<br/><br/>
	$realty->blogname";
	$to_bcc=get_option('subscribe_add_subscribers');
	if(empty($to_bcc))$to_bcc=$realty->settings['general_settings']['bcc_all_mail_to'];
	
	$helper->email($email, $user_subject, $user_nessage, '', '', false);
	if(!empty($to_bcc))$helper->email($to_bcc, $subject, $message, '', '', false);
	$return.="Thank you for subscribing to $realty->blogname.<br/>";
	
}
echo $return;
?>
