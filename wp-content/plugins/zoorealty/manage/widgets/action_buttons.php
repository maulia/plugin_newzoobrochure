<p><label>Please check the options that you want to display in action button widget.</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[email_self]" type="checkbox"<?php if($settings['email_self']) echo ' checked="checked"'; ?>> <label>Email to Self</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[email_friend]" type="checkbox"<?php if($settings['email_friend']) echo ' checked="checked"'; ?>> <label>Email to Friend</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[print_listing]" type="checkbox"<?php if($settings['print_listing']) echo ' checked="checked"'; ?>> <label>Print Listing</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[fav]" type="checkbox"<?php if($settings['fav']) echo ' checked="checked"'; ?>> <label>Favourites</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[map]" type="checkbox"<?php if($settings['map']) echo ' checked="checked"'; ?>> <label>Map</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[video]" type="checkbox"<?php if($settings['video']) echo ' checked="checked"'; ?>> <label>Video</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[rental]" type="checkbox"<?php if($settings['rental']) echo ' checked="checked"'; ?>> <label>Rental Application</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[walkability]" type="checkbox"<?php if($settings['walkability']) echo ' checked="checked"'; ?>> <label>Walkabilty</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[twitter]" type="checkbox"<?php if($settings['twitter']) echo ' checked="checked"'; ?>> <label>Share on Twitter</label>
</p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[facebook]" type="checkbox"<?php if($settings['facebook']) echo ' checked="checked"'; ?>> <label>Share on Facebook</label>
</p>
<br/>
<p><label>Below is the option for Rental Application.</label> </p>
<p><label>Rental Application ID</label> <br><input value="<?php echo $settings['rental_app_id']; ?>" name="realty_widget_<?php echo $caption; ?>[rental_app_id]" type="text"></p>