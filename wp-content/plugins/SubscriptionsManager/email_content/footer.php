</table>
<div style="width:765px;">

<p style="font-family: Verdana, Arial, sans-serif; font-size: 11px; color: #888;">You are receiving this email because you are a subscriber of <?php echo $this->blog_name; ?>. You can <a href="<?php echo $realty->siteUrl; ?>login/" title="login">login</a> at any time to update your subscription or to <a href="<?php echo $realty->siteUrl; ?>unsubscribe/" title="unsubscribe">un-subscribe</a>. Your username is <span style="color:#007ca5;"><?php echo $subscriber['email']; ?></span><?php  if(get_option('is_use_password')=='1'):?> and your password is <span style="color: 484848;"><?php echo ($subscriber['password']=='')?$this->default_password:$subscriber['password'];?></span> (case
sensitive)<?php endif; ?>.</p>

<p style="font-family: Verdana, Arial, sans-serif; font-size: 11px; color: #888;">
<p style="font-family: Verdana, Arial, sans-serif; font-size: 11px; color: #888;">Copyright &copy; <?php echo date('Y') . ', ' . $this->blog_name; ?> All rights reserved. Email: <a href="mailto:<?php echo $realty->settings['general_settings']['bcc_all_mail_to']; ?>" style="color:#007ca5;"><?php echo $realty->settings['general_settings']['bcc_all_mail_to']; ?></a>.</p>

<p>&nbsp;</p>

</div>

<?php
$message = ob_get_contents();
ob_end_clean();

if($emails_sent==0)//Only print output for e-mail sent to the first subscriber on the screen.
	$output = " Below is a sample of the e-mail generated for the first subscriber ( ". $subscriber['email'] . ").</p><hr/><h3>Subject: $subject </h3>
<p>-------------------------------<span style='font-size:9px'>E-mail Content Starts</span>-------------------------------</p>
$message
<p>-------------------------------<span style='font-size:9px'>E-mail Content Ends</span>--------------------------------</p>";
?>