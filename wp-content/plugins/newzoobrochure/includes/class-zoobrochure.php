<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * The core zoobrochure class.
 *
 * @since      2.0.0
 * @package    zoobrochure
 * @subpackage zoobrochure/includes
 * @author     Agentpoint
 */
class zoobrochure {

    protected static $instance = null;

    /**
     * The unique identifier of this plugin.
     *
     * @since    2.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    2.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * The current pluginPath of the plugin.
     *
     * @since    2.0.0
     * @access   protected
     * @var      string    $pluginPath    The current pluginPath of the plugin.
     */
    protected $pluginPath;

    /**
     * The current pluginUrl of the plugin.
     *
     * @since    2.0.0
     * @access   protected
     * @var      string    $pluginUrl    The current pluginUrl of the plugin.
     */
    protected $pluginUrl;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    2.0.0
     */
    public function __construct() {

        $this->plugin_name = 'Zoo Brochure V2';
        $this->version = '2.0.0';
        $this->pluginPath = plugin_dir_path(dirname(__FILE__));
        $this->pluginUrl = plugin_dir_url(dirname(__FILE__));

        $this->load_dependencies();
        $this->load_hooks();
    }

    /**
     * Main zoobrochure Instance.
     *
     * Ensures only one instance of zoobrochure is loaded or can be loaded.
     *
     * @since 2.0.0
     * @static
     * @return object zoobrochure Main instance
     */
    public static function instance() {
        if (null == self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Load the required dependencies for this plugin.
     *
     *
     * @since    2.0.0
     * @access   private
     */
    private function load_dependencies() {
        /**
         * Load main PDF library (FPDF) also load other dependencies
         * 
         */
        require_once( $this->pluginPath . 'includes/abstract-class-zoobrochure-template.php' );
        require_once( $this->pluginPath . 'includes/class-zoobrochure-routes.php' );
        if (!is_admin()) {
            require_once( $this->pluginPath . 'includes/fpdf/fpdf.php' );
            require_once( $this->pluginPath . 'includes/fpdf_cache/fpdf_cache.php' );
            require_once( $this->pluginPath . 'includes/fpdf_alpha/fpdf_alpha.php' );
            require_once( $this->pluginPath . 'includes/fpdf_curve/fpdf_curve.php' );
            require_once( $this->pluginPath . 'includes/qrcode/phpqrcode.php' );
            require_once( $this->pluginPath . 'includes/zoobrochure-formating-functions.php' );
            require_once( $this->pluginPath . 'includes/class-zoobrochure-property.php' );
            require_once( $this->pluginPath . 'includes/class-zoobrochure-pdf.php' );
        }

        /**
         * The class responsible for defining all actions that occur in the admin area or both.
         */
        require_once( $this->pluginPath . 'includes/settings/zoobrochure-settings.php' );
        require_once( $this->pluginPath . 'includes/class-zoobrochure-fields.php' );
        require_once( $this->pluginPath . 'includes/class-zoobrochure-setting.php' );
    }

    /**
     * Load all of the hooks related to brochure functionality
     * of the plugin.
     *
     * @since    2.0.0
     * @access   private
     */
    private function load_hooks() {
        $this->setting();
        $this->routes();
    }

    public function flush_rules() {
        flush_rewrite_rules();
    }

    /**
     * The url of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     2.0.0
     * @return    string    The url of the plugin.
     */
    public function get_plugin_url() {
        return $this->pluginUrl;
    }

    /**
     * The path of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     2.0.0
     * @return    string    The path of the plugin.
     */
    public function get_plugin_path() {
        return $this->pluginPath;
    }

    /**
     * Get property class.
     * 
     * @since 2.0.0
     * @return object zoobrochure_property
     */
    public function property() {
        return zoobrochure_property::instance();
    }

    /**
     * Get setting class.
     * 
     * @since 2.0.0
     * @return object zoobrochure_setting
     */
    public function setting() {
        return zoobrochure_setting::instance();
    }

    /**
     * Get routes class.
     * 
     * @since 2.0.0
     * @return object zoobrochure_routes
     */
    public function routes() {
        return zoobrochure_routes::instance();
    }

    /**
     * Get template folder name
     * 
     * @since 2.0.0
     * @return string Name of folder template
     */
    public function get_template_path() {
        return apply_filters('zoobrochure_template_path', 'templates/');
    }

}
