<div class="wrap">
	<h2><?php _e('Zoo Contact Data '); ?></h2>
	<form id="posts-filter" action="<?php echo $this->form_action;?>" method="post">
	<p id="post-search">
	<input type="text" value="<?php echo $request['keywords']; ?>" name="keywords" />
	<input type="submit" class="button" value="Search Contact"/>
	</p>
	</form>
	<div class="tablenav">
	<?php echo $subscribers['pagination_label']; ?>
	</div>
	<table class="widefat">
	  <thead>
	  <tr> 
		<th scope="col"><?php _e('Delete') ?></th>
		<th scope="col"><?php _e('E-mail') ?></th>
		<th scope="col"><?php _e('Name') ?></th>	
		<th scope="col"><?php _e('Home No.') ?></th>
		<th scope="col"><?php _e('Mobile No.') ?></th>		
		<th scope="col"><?php _e('Action'); ?></th>
	  </tr>
	  </thead>
	  <tbody id="the-list">
	<?php if(!empty($subscribers)) foreach($subscribers as $item): if(!is_array($item)) continue;//pagination_label data ?>
		<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">	
		<td>
			<a href="<?php echo $this->form_action;?>&amp;task=delete_contact&amp;subscriber_id=<?php echo $item['subscriber_id']; ?>" onClick="return confirm('You are about to delete the this contact.\n\'OK\' to delete, \'Cancel\' to stop.' );">Delete</a>
		</td>
		<td><a href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a></td>
		<td>	<?php echo $item['first_name'] . ' ' . $item['last_name']; ?></td>	
		<td>	<?php echo $item['home_number']; ?></td>
		<td>	<?php echo $item['mobile_number']; ?></td>
		<td>
			<a href="<?php echo $this->form_action;?>&amp;action=alerts&amp;subscriber_id=<?php echo $item['subscriber_id']; ?>">Alerts</a> | <a href="<?php echo $this->form_action;?>&amp;action=notes&amp;subscriber_id=<?php echo $item['subscriber_id']; ?>">Notes</a>
		</td>
		</tr>
		
	<?php endforeach; ?>
	  </tbody>
	</table>
	<div class="tablenav">
	<?php echo $subscribers['pagination_label']; ?>
	</div>
	
	<h2>Settings</h2>
	<?php
	if(isset($_POST['submit_pass'])){
		if(!update_option('zm_count_viewer',$_POST['zm_count_viewer']))add_option('zm_count_viewer',$_POST['zm_count_viewer']);
		if(!update_option('zr_office_id',$_POST['zr_office_id']))add_option('zr_office_id',$_POST['zr_office_id']);
		$message_pass = 'Settings Updated.';
	}
	if(!empty($message_pass)):
		?><div id="message_pass" class="updated fade"><p><strong><?php echo $message_pass; ?></strong></p></div><?php
	endif;
	?>
	<form  action="" method="post">
		<table class="widefat">			
			<tr class="alternate">
				<td>Send Count Viewer</td>
				<td>
				<input type="radio" name="zm_count_viewer" <?php if (get_option('zm_count_viewer')=='1') echo 'checked="checked"'; ?> value="1">Yes
				<span style="margin-left:10px"><input type="radio" name="zm_count_viewer" <?php if (get_option('zm_count_viewer')=='') echo 'checked="checked"'; ?> value="">No</span>
				</td>
			</tr>	
			<tr>
				<td>Remit to Office ID</td>
				<td>
				<input type="text" name="zr_office_id" value="<?php echo get_option('zr_office_id'); ?>">
				</td>
			</tr>
		</table>
		<p><input class="button" name="submit_pass" type="submit" value="Save" name=""  /></p>
	</form>
</div>