<?php
class my_db{
		var $debug = false;
		var $print_query = false;
		
		function connect(){
			$dbhost='localhost';
			$dbuser='randwcor';
			$dbpassword='vnhJCEw7An.E4zW7';
			$db='blog_randwcor';
			$link=@mysql_connect($dbhost, $dbuser, $dbpassword, true);
			return $db;
		}
		
		function logging( $content ){
			$file = dirname(dirname(__FILE__)) ."/logfile.log";
			if( file_exists( $file ) ){
				$file_content  = file_get_contents( $file );
				$new_content = $file_content ."\r\n" .$content ;
			}
			else
			{
				$new_content .= $content;
			}
			file_put_contents( $file, $new_content ); 
		}

		function query($sql){		
			$time_start = $this->microtime_float();
			$return = mysql_query( $sql) or $this->error(mysql_error(),$sql);
			$time_end = $this->microtime_float();
			if ($this->debug)
				echo '<p><font color="red">SQL: </font>' . $sql . '<br/><font color="blue" size="-3">*[Query took ' . number_format(($time_end - $time_start),6) . ' seconds ]*</font></p>';
			if ($this->print_query)
				echo '<p>' . $sql . '</p>';

			return $return;
		}
		
		function get_var($sql){			
			$query = $this->query($sql);			
			if(!$query || mysql_num_rows($query) <1)
				return false;
			else
				return stripslashes(mysql_result($query,0));
		}
		
		function get_column($sql, $column){
			$result = $this->query($sql);
			$return = array();
			while ($row = mysql_fetch_assoc($result))
				array_push($return,stripslashes($row[$column]));
			return $return;	
		}
		
		function get_row($sql, $row=0){
			$return = $this->get_results($sql);
			return $return[$row];
		}
		
		function get_results($sql, $return_type = 'array'){
			$result = $this->query($sql);
			$return = array();
			if($result){
			while ($row = mysql_fetch_assoc($result)):
				array_walk($row,array( 'my_db', 'my_stripslashes'));
				if($return_type == 'object')
					$this->parseArrayToObject($row);
				array_push($return,$row);
			endwhile;	
			}
			return $return;
		}
		
		function error($error,$sql){
			if ($this->debug)
				die($error . '<p><font color="red">SQL: </font>' . $sql . '</p>');
		}

		function microtime_float(){
			list($usec, $sec) = explode(" ", microtime());
			return ((float)$usec + (float)$sec);
		}
		
		function my_stripslashes(&$string){
			$string = stripslashes($string);
		}
		
		function parseArrayToObject(&$array) {
		   $object = new stdClass();
		   if (is_array($array) && count($array) > 0) {
			  foreach ($array as $name=>$value) {
				 $name = trim($name);
				 if (!empty($name)) {
					$object->$name = $value;
				 }
			  }
		   }
		   $array = $object;
		   return $object;
		}
		
		function parseObjectToArray(&$object) {
		   $array = array();
		   if (is_object($object)) {
			  $array = get_object_vars($object);
		   }
		   $object = $array;
		   return $array;
		}	
			
}
?>
