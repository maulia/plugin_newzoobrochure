<?php

add_filter('dynamic_sidebar_params', 'my_widget_class');
function my_widget_class($params) {
	global $widget_num;

	// Widget class
	$class = array();
	$class[] = 'widget';

	// Iterated class
	$widget_num++;
	$class[] = 'widget-' . $widget_num;

	// Alt class
	if ($widget_num % 2) :
		$class[] = '';
	else :
		$class[] = 'alt';
	endif;

	// Join the classes in the array
	$class = join(' ', $class);

	// Interpolate the 'my_widget_class' placeholder
	$params[0]['before_widget'] = str_replace('my_widget_class', $class, $params[0]['before_widget']);
	return $params;
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Top', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Middle', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Homepage Bottom', // main content area of the Homepage
	'before_widget' => '<div id="%1$s" class="content_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div>'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Main', // main content area of the Property page
	'before_widget' => "\n".'<div id="%1$s" class="property_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .property_block -->'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Side', // sidebar content for Property page
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .side_block -->'."\n",
	'before_title' => '<h3>',
	'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Tag Search', // goes on the Search Results pages, perhaps in addition to the Static Sidebar
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .side_block -->'."\n",
	'before_title' => '<h3>',
	'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Static Sidebar', //goes on every page except home and blog
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .side_block -->'."\n",
	'before_title' => '<h3>',
	'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Blog Sidebar', // guess what this is for!!
	'before_widget' => "\n".'<div id="side_%1$s" class="side_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div></div><!-- end .side_block -->'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2><div class="block_content">',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	'name' => 'Property Page Media', // media content area of the Property page
	'before_widget' => "\n".'<div id="%1$s" class="property_block %2$s my_widget_class">'."\n",
	'after_widget' => '</div><!-- end .property_block -->'."\n",
	'before_title' => '<h2 class="section_title">',
	'after_title' => '</h2>',
	));
}

/**********************************************************************************************************************************************
Get Absolute Ancestor
**********************************************************************************************************************************************/
function get_absolute_ancestor($post_id){//Returns the absolute ancestor (parent, grandparent, great-grandparent if there is, etc.) of a post. The absolute ancestor is defined as a page that doesnt have further parents, that is, its post parent is '0' 
		global $wpdb;		 
		 $parent = $wpdb->get_var("SELECT `post_parent` FROM $wpdb->posts WHERE `ID`= $post_id");		 
		 if($parent == 0) //Return from the recursion with the title of the absolute ancestor post.
			return $wpdb->get_var("SELECT * FROM $wpdb->posts WHERE `ID`= $post_id");
		 return get_absolute_ancestor($parent);		 	
	}//ends function
/**********************************************************************************************************************************************
Custom Comments Template
**********************************************************************************************************************************************/
function mytheme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>">

		<div class="comment-author vcard">
			<p><?php echo get_avatar($comment,$size='32'); ?><br />
			<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?><br />
    
            <span class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(),  get_comment_time()) ?></a><br /><?php edit_comment_link('(Edit)','  ','') ?></span>
    
            <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
            <?php endif; ?>
            </p>
		</div>
        
        <div class="commentcontent">
			<?php comment_text() ?>
    
            <div class="clearer"></div>
            
            <div class="reply">
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
		</div>

	<div class="clearer"></div>
		</div>
<?php
}
/**********************************************************************************************************************************************
LIMIT POSTS
**********************************************************************************************************************************************/
/*
Plugin Name: Limit Posts
Plugin URI: http://labitacora.net/comunBlog/limit-post.phps
Description: Limits the displayed text length on the index page entries and generates a link to a page to read the full content if its bigger than the selected maximum length. 
Usage: the_content_limit($max_charaters, $more_link)
Version: 1.1
Author: Alfonso Sanchez-Paus Diaz y Julian Simon de Castro
Author URI: http://labitacora.net/
License: GPL
Download URL: http://labitacora.net/comunBlog/limit-post.phps
Make: 
    In file index.php 
    replace the_content() 
    with the_content_limit(1000, "more")
*/

function the_content_limit($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);

   if (strlen($_GET['p']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo $content . ' &hellip; ' ;
        echo "<a href='";
        the_permalink();
        echo "'>".$more_link_text."</a></p>";
   }
   else {
      echo $content;
   }
}

/* Recent Comments */
function list_recent_comments ($number_comments = 5) {
		global $wpdb;
		$comments = $wpdb->get_results("SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type,comment_author_url, SUBSTRING(comment_content,1,70) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID =$wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $number_comments");
		foreach ($comments as $comment) {
		$comment->comment_date_gmt = date("jS F Y",strtotime($comment->comment_date_gmt));
		$output .= "\n<li class=\"li_border\"><strong><a href=\"" . get_permalink($comment->ID) . "#comment-" . $comment->comment_ID . "\" title=\"on " . $comment->post_title . "\">".strip_tags($comment->comment_author)."</a> - ". $comment->comment_date_gmt ."</strong><br />" . strip_tags($comment->com_excerpt) ." &hellip;</li>";
		}
		echo $output;
} //ends function list_recent_comments
/**********************************************************************************************************************************************
THE EXCERPT RELOADED
**********************************************************************************************************************************************/
/*
Plugin Name: the_excerpt Reloaded
Plugin URI: http://guff.szub.net/2005/02/26/the_excerpt-reloaded/
Description: This mod of WordPress' template function the_excerpt() knows there is no spoon.
Version: R1
Author: Kaf Oseo
Author URI: http://szub.net

	Copyright (c) 2004, 2005 Kaf Oseo (http://szub.net)
	the_excerpt Reloaded is released under the GNU General Public
	License: http://www.gnu.org/licenses/gpl.txt
*/
function wp_the_excerpt_reloaded($args='') {
	parse_str($args);
	if(!isset($excerpt_length)) $excerpt_length = 120; // length of excerpt in words. -1 to display all excerpt/content
	if(!isset($allowedtags)) $allowedtags = '<a>'; // HTML tags allowed in excerpt, 'all' to allow all tags.
	if(!isset($filter_type)) $filter_type = 'none'; // format filter used => 'content', 'excerpt', 'content_rss', 'excerpt_rss', 'none'
	if(!isset($use_more_link)) $use_more_link = 1; // display
	if(!isset($more_link_text)) $more_link_text = "(more...)";
	if(!isset($force_more)) $force_more = 1;
	if(!isset($fakeit)) $fakeit = 1;
	if(!isset($fix_tags)) $fix_tags = 1;
	if(!isset($no_more)) $no_more = 0;
	if(!isset($more_tag)) $more_tag = 'div';
	if(!isset($more_link_title)) $more_link_title = 'Continue reading this entry';
	if(!isset($showdots)) $showdots = 1;

	return the_excerpt_reloaded($excerpt_length, $allowedtags, $filter_type, $use_more_link, $more_link_text, $force_more, $fakeit, $fix_tags, $no_more, $more_tag, $more_link_title, $showdots);
}

function the_excerpt_reloaded($excerpt_length=120, $allowedtags='<a>', $filter_type='none', $use_more_link=true, $more_link_text="(more...)", $force_more=true, $fakeit=1, $fix_tags=true, $no_more=false, $more_tag='div', $more_link_title='Continue reading this entry', $showdots=true) {
	if(preg_match('%^content($|_rss)|^excerpt($|_rss)%', $filter_type)) {
		$filter_type = 'the_' . $filter_type;
	}
	echo get_the_excerpt_reloaded($excerpt_length, $allowedtags, $filter_type, $use_more_link, $more_link_text, $force_more, $fakeit, $no_more, $more_tag, $more_link_title, $showdots);
}

function get_the_excerpt_reloaded($excerpt_length, $allowedtags, $filter_type, $use_more_link, $more_link_text, $force_more, $fakeit, $no_more, $more_tag, $more_link_title, $showdots) {
	global $post;

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_'.COOKIEHASH] != $post->post_password) { // and it doesn't match cookie
			if(is_feed()) { // if this runs in a feed
				$output = __('There is no excerpt because this is a protected post.');
			} else {
	            $output = get_the_password_form();
			}
		}
		return $output;
	}

	if($fakeit == 2) { // force content as excerpt
		$text = $post->post_content;
	} elseif($fakeit == 1) { // content as excerpt, if no excerpt
		$text = (empty($post->post_excerpt)) ? $post->post_content : $post->post_excerpt;
	} else { // excerpt no matter what
		$text = $post->post_excerpt;
	}

	if($excerpt_length < 0) {
		$output = $text;
	} else {
		if(!$no_more && strpos($text, '<!--more-->')) {
		    $text = explode('<!--more-->', $text, 2);
			$l = count($text[0]);
			$more_link = 1;
		} else {
			$text = explode(' ', $text);
			if(count($text) > $excerpt_length) {
				$l = $excerpt_length;
				$ellipsis = 1;
			} else {
				$l = count($text);
				$more_link_text = '';
				$ellipsis = 0;
			}
		}
		for ($i=0; $i<$l; $i++)
				$output .= $text[$i] . ' ';
	}

	if('all' != $allowed_tags) {
		$output = strip_tags($output, $allowedtags);
	}

//	$output = str_replace(array("\r\n", "\r", "\n", "  "), " ", $output);

	$output = rtrim($output, "\s\n\t\r\0\x0B");
	$output = ($fix_tags) ? $output : balanceTags($output);
	$output .= ($showdots && $ellipsis) ? '...' : '';

	switch($more_tag) {
		case('div') :
			$tag = 'span';
		break;
		case('span') :
			$tag = 'span';
		break;
		case('p') :
			$tag = 'p';
		break;
		default :
			$tag = 'span';
	}

	if ($use_more_link && $more_link_text) {
		if($force_more) {
			$output .= ' <' . $tag . ' class="more-link"><a href="'. get_permalink($post->ID) . '#more-' . $post->ID .'" title="' . $more_link_title . '">' . $more_link_text . '</a></' . $tag . '>' . "\n";
		} else {
			$output .= ' <' . $tag . ' class="more-link"><a href="'. get_permalink($post->ID) . '" title="' . $more_link_title . '">' . $more_link_text . '</a></' . $tag . '>' . "\n";
		}
	}

	$output = apply_filters($filter_type, $output);

	return $output;
}
/**********************************************************************************************************************************************
PAGE CUSTOM FIELDS UI
**********************************************************************************************************************************************/
$page_custom_fields =
array(
	"redirect" => array(
		"name" => "redirect",
		"std" => "",
		"title" => "Redirect Page:",
		"description" => "Insert the page-slug of the page you wish the navigation link to direct."
	),
	"nav_text" => array(
		"name" => "nav_text",
		"std" => "",
		"title" => "Navigation Extra/Alt Text:",
		"description" => "Keep this short. It will under the nav link and in the link title."
	),
);

function page_custom_fields() {
	global $post, $page_custom_fields;

	foreach($page_custom_fields as $meta_box) {
		$meta_box_value = stripslashes(get_post_meta($post->ID, $meta_box['name'].'_value', true));

		if($meta_box_value == "")
			$meta_box_value = $meta_box['std'];

			echo '<p style="margin-bottom:10px;">';
			echo'<input type="hidden" name="'.$meta_box['name'].'_noncename" id="'.$meta_box['name'].'_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';
			echo'<strong>'.$meta_box['title'].'</strong> (<em>'.$meta_box['description'].'</em>)';
			echo'<input type="text" name="'.$meta_box['name'].'_value" value="'.attribute_escape($meta_box_value).'" style="width:98%;" /><br />';
			echo '</p>';
	}
}

function create_meta_box() {
	global $theme_name;
		if ( function_exists('add_meta_box') ) {
			add_meta_box( 'new-meta-boxes', 'Navigation Menu Redirect', 'page_custom_fields', 'page', 'normal', 'high' );
	}
}

function save_postdata( $post_id ) {
	global $post, $page_custom_fields;

	foreach($page_custom_fields as $meta_box) {
		// Verify
		if ( !wp_verify_nonce( $_POST[$meta_box['name'].'_noncename'], plugin_basename(__FILE__) )) {
			return $post_id;
	}

	if ( 'page' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post_id ))
			return $post_id;
		} else {
		if ( !current_user_can( 'edit_post', $post_id ))
			return $post_id;
	}

	$data = $_POST[$meta_box['name'].'_value'];

	if(get_post_meta($post_id, $meta_box['name'].'_value') == "")
		add_post_meta($post_id, $meta_box['name'].'_value', $data, true);
	elseif($data != get_post_meta($post_id, $meta_box['name'].'_value', true))
		update_post_meta($post_id, $meta_box['name'].'_value', $data);
	elseif($data == "")
		delete_post_meta($post_id, $meta_box['name'].'_value', get_post_meta($post_id, $meta_box['name'].'_value', true));
	}
}

add_action('admin_menu', 'create_meta_box');
add_action('save_post', 'save_postdata');
/**********************************************************************************************************************************************
	B2 LINKS PAGE
***********************************************************************************************************************************************/
function my_list_bookmarks($args = '') {
	$defaults = array(
		'orderby' => 'name', 'order' => 'ASC',
		'limit' => -1, 'category' => '', 'exclude_category' => '',
		'category_name' => '', 'hide_invisible' => 1,
		'show_updated' => 0, 'echo' => 1,
		'categorize' => 1, 'title_li' => __('Bookmarks'),
		'title_before' => '<h2>', 'title_after' => '</h2>',
		'category_orderby' => 'name', 'category_order' => 'ASC',
		'class' => 'linkcat', 'category_before' => '<li id="%id" class="%class">',
		'category_after' => '</li>'
	);

	$r = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	$output = '';

	if ( $categorize ) {
		//Split the bookmarks into ul's for each category
		$cats = get_terms('link_category', array('name__like' => $category_name, 'include' => $category, 'exclude' => $exclude_category, 'orderby' => $category_orderby, 'order' => $category_order, 'hierarchical' => 0));

		foreach ( (array) $cats as $cat ) {
			$params = array_merge($r, array('category'=>$cat->term_id));
			$bookmarks = get_bookmarks($params);
			if ( empty($bookmarks) )
				continue;
			$output .= str_replace(array('%id', '%class'), array("linkcat-$cat->term_id", $class), $category_before);
			$catname = apply_filters( "link_category", $cat->name );
			$catdesc = $cat->description;
			$output .= "$title_before$catname$title_after<p>$catdesc</p>\n\t<ul class='xoxo blogroll'>\n";
			$output .= _walk_bookmarks($bookmarks, $r);
			$output .= "\n\t</ul>\n$category_after\n";
		}
	} else {
		//output one single list using title_li for the title
		$bookmarks = get_bookmarks($r);

		if ( !empty($bookmarks) ) {
			if ( !empty( $title_li ) ){
				$output .= str_replace(array('%id', '%class'), array("linkcat-$category", $class), $category_before);
				$output .= "$title_before$title_li$title_after\n\t<ul class='xoxo blogroll'>\n";
				$output .= _walk_bookmarks($bookmarks, $r);
				$output .= "\n\t</ul>\n$category_after\n";
			} else {
				$output .= _walk_bookmarks($bookmarks, $r);
			}
		}
	}

	$output = apply_filters( 'wp_list_bookmarks', $output );

	if ( !$echo )
		return $output;
	echo $output;
}
/**********************************************************************************************************************************************
Custom Login
***********************************************************************************************************************************************/
function custom_login() { 
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
}

echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/styles/login-style.css" />'."\n".'<style type="text/css">';
include('styles/login-colors.php');
echo '</style>'; 
}
function custom_headerurl() { 
return get_bloginfo('siteurl');//return the current wp blog url 
}
function custom_headertitle() { 
return 'Powered by '. get_bloginfo('name');//return the current wp blog name; 
}
add_action('login_head', 'custom_login');
add_filter('login_headerurl', 'custom_headerurl');
add_filter('login_headertitle', 'custom_headertitle');

/**********************************************************************************************************************************************
Start of Theme Options Page
***********************************************************************************************************************************************/
$themename = get_option('stylesheet');
$fonts = array("type1", "type2", "type3","type4","type5","type6","type7","type8","type9");
$options = array (
	array(    
		"name" => "Logo",
		"type" => "open"),
	array(
		"name" => "Logo Image",
		"id" => "logo",
		"type" => "text",
		"std" => "logo_carbon.gif",
		"description" => "Image file name, with extension."),
	array(
		"name" => "Image Preview",
		"id" => "logo",
		"type" => "image",
		"std" => "logo.png"),
	array(
		"name" => "Logo Width",
		"id" => "logo_width",
		"type" => "text",
		"std" => "210",
		"description" => "Key in number ONLY. "),
	array(
		"name" => "Logo Height",
		"id" => "logo_height",
		"type" => "text",
		"std" => "70",
		"description" => "Key in number ONLY."),
	array(
		"type" => "close"),

	array(    
		"name" => "Navigation Menus",
		"type" => "open"),
	array(
		"name" => "Top Page IDs",
		"id" => "main_menu",
		"type" => "text",
		"std" => "25,3,4,30,32,2,26",
		"description" => "Enter a max of 7 Page IDs of the main menu items, separated by commas. Drop down menus will be generated automatically if you set the menu order in the Page Manager. Main menu items will link to the post id noted in custom fields."),
	array(
		"name" => "Include dropdown menu?",
		"id" => "drop_down_menu",
		"std" => "",
		"status" => 'checked',
		"type" => "checkbox",
		"description" => "If you want child pages to show up as a drop down menu, check this box."),
	array(
		"name" => "Repeat main menu item in dropdown?",
		"id" => "repeat_main_item",
		"std" => "",
		"status" => 'checked',
		"type" => "checkbox",
		"description" => "If you want main menu item repeated as the first item in the drop down menu, check this box."),
	array(
		"name" => "Want a sidebar menu?",
		"id" => "sidebar_menu",
		"std" => "",
		"status" => 'checked',
		"type" => "checkbox",
		"description" => "If you want the menu in the sidebar, check this box."),
	array(
		"name" => "Repeat main menu at bottom of page?",
		"id" => "repeat_main_menu",
		"std" => "",
		"status" => 'checked',
		"type" => "checkbox",
		"description" => "If you want main menu repeated at the bottom of each page, check this box."),
	array(
		"type" => "close"),

	array(    
		"name" => "Contact Information",
		"type" => "open"),
	array(
		"name" => "Address",
		"id" => "header_address",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "City",
		"id" => "header_city",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "State",
		"id" => "header_state",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Postal Code",
		"id" => "header_zip",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Phone",
		"id" => "header_phone",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Fax",
		"id" => "header_fax",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Email",
		"id" => "header_email",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"type" => "close"),

	/*array(    
		"name" => "Banner pic and text",
		"type" => "open"),
	array(
		"name" => "Banner headline",
		"id" => "banner_headline",
		"type" => "text",
		"std" => "Let Your Dream Home Find You &hellip;",
		"description" => "Enter a maximum of 6 words as your headline. Enter even if you have an image replacement."),
	array(
		"name" => "Banner text",
		"id" => "banner_text",
		"type" => "text",
		"std" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non nisi vitae lorem pretium cursus ac vitae ipsum. Maecenas adipiscing blandit orci at interdum.",
		"description" => "Enter a maximum of 22 words as your text. Enter even if you have an image replacement."),
	array(
		"name" => "Banner background image",
		"id" => "banner_bg_pic",
		"type" => "text",
		"std" => "banner_pic.jpg",
		"description" => "Upload your banner background image to this directory: wp-content/themes/carbon/images/banner. Enter the name of your background image here. (i.e., banner_pic.jpg)"),
	array(
		"name" => "Banner text image",
		"id" => "banner_text_pic",
		"type" => "text",
		"std" => "banner_text.png",
		"description" => "Did you make up pretty text to display instead of the text above? Upload your banner text image to this directory: wp-content/themes/carbon/images/banner. Enter the name of your banner text image here. (i.e., banner_pic.jpg; must be 550px X 75px)"),
	array(
		"type" => "close"),*/

	/*array(    
		"name" => "Fonts",
		"type" => "open"),
	array(
		"name" => "Select a font set",
		"id" => "fonts",
		"type" => "select",
		"std" => "type7",
		"options" => $fonts),
	array(
		"type" => "close"),*/

	/*array(    
		"name" => "Colors",
		"type" => "open"),
	array(
		"name" => "To set the color, click in each box, choose your color and click the color wheel in the bottom right.",
		"type" => "title"),
	array(
		"name" => "Body Background",
		"id" => "bg_color",
		"type" => "text",
		"std" => "edf8f8"),
	array(
		"name" => "Content Area Background",
		"id" => "content_bg_color",
		"type" => "text",
		"std" => "ffffff"),
	array(
		"name" => "Links",
		"id" => "link_color",
		"type" => "text",
		"std" => "3c95bd"),
	array(
		"name" => "Links hover",
		"id" => "link_hover_color",
		"type" => "text",
		"std" => "77c24b"),
	array(
		"name" => "Border",
		"id" => "border_color",
		"type" => "text",
		"std" => "dedede"),
	array(
		"name" => "Accent",
		"id" => "accent_color",
		"type" => "text",
		"std" => "eeeeee"),
	array(
		"name" => "Logo (Header) Background",
		"id" => "logo_bg_color",
		"type" => "text",
		"std" => "fff"),
	array(
		"name" => "Header Text",
		"id" => "header_text_color",
		"type" => "text",
		"std" => "000000"),
	array(
		"name" => "Header Borders",
		"id" => "header_border_color",
		"type" => "text",
		"std" => "000000"),
	array(
		"name" => "Nav Dropdown Menu Background",
		"id" => "nav_bg_color",
		"type" => "text",
		"std" => "edf8f8"),
	array(
		"name" => "Navigation Links",
		"id" => "nav_links_color",
		"type" => "text",
		"std" => "000000"),
	array(
		"name" => "Navigation Links (Selected)",
		"id" => "nav_links_selected_color",
		"type" => "text",
		"std" => "3c95bd"),
	array(
		"name" => "Main Page titles",
		"id" => "page_title_color",
		"type" => "text",
		"std" => "1c1c1c"),
	array(
		"name" => "Main text",
		"id" => "main_text_color",
		"type" => "text",
		"std" => "636363"),
	array(
		"name" => "Side Titles",
		"id" => "side_titles_color",
		"type" => "text",
		"std" => "000000"),
	array(
		"name" => "Side Text",
		"id" => "side_text_color",
		"type" => "text",
		"std" => "4c4c4c"),
	array(
		"name" => "Extra Menu Background",
		"id" => "extra_bg_color",
		"type" => "text",
		"std" => "252525"),
	array(
		"name" => "Extra Menu Text",
		"id" => "extra_text_color",
		"type" => "text",
		"std" => "d7d7d7"),
	array(
		"name" => "Extra Menu Links",
		"id" => "extra_links_color",
		"type" => "text",
		"std" => "ffffff"),
	array(
		"name" => "Footer Background",
		"id" => "footer_bg_color",
		"type" => "text",
		"std" => "252525"),
	array(
		"name" => "Footer Text",
		"id" => "footer_text_color",
		"type" => "text",
		"std" => "d7d7d7"),
	array(
		"name" => "Footer Links",
		"id" => "footer_links_color",
		"type" => "text",
		"std" => "ffffff"),
	array(
		"name" => "Bottom Banner Background",
		"id" => "footer_bg_color",
		"type" => "text",
		"std" => "252525"),
	array(
		"type" => "close"),*/

	/*array(    
		"name" => "Other Colors",
		"type" => "open"),
	array(    
		"name" => "Here's your opportunity to put in other styles. Be sure to use correct CSS syntax.",
		"type" => "title"),
	array(
		"name" => "Your CSS",
		"id" => "user_css",
		"type" => "textarea",
		"std" => "#contact_info p, .action_buttons p a, p.button .btn, p.big a, ul.shadetabs li a:hover, ul.shadetabs li.ui-state-default a, .flag a, span.help_tab { color: #fff; }
#content .land_building_size p, #data_table_div small, span.dates_view { color: #838383; }
.field { color: #737373; }
.side_block { border-color: #fff; }
#sidebar .side_block h3 { border-color: #c7c7c7; }
#details .block_content { border-top-color: #fff; border-bottom-color: #c7c7c7; }
#details .block_bottom { border-color: #fff; }
#subscribe_block ul { border-color: #fff; }
table.DataTable td { background-color: #fff; }
#content #property_stats table tr.DataTableHeader td, table.sales_data_table th { color: #fff; }
#details table td.value, #subscribe_block ul li a, .realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs li.ui-state-active a { color: #525252; }"),
	array(
		"type" => "close"),*/

	/*array(    
		"name" => "Post Category flag colors",
		"type" => "open"),
	array(    
		"name" => "In the blog section of the site, each post category title has a unique background color. Enter the colors you want here. Post Categories are listed alphabetically; if you want the \"Apples\" category to have a red (#FF0000) background and the \"Bananas\" category to have yellow (#FFFF00), enter your hex values like this: FF0000,FFFF00.",
		"type" => "title"),
	array(
		"name" => "Category flag colors",
		"id" => "cat_flag_color",
		"type" => "text",
		"description" => "Separate your HEX colors with a comma (no spaces)",
		"std" => "3c95bd,77c24b,feb228"),
	array(
		"type" => "close"),*/

	array(    
		"name" => "Social Networking",
		"type" => "open"),
	array(    
		"name" => "Choose the button type.",
		"type" => "title"),
	array(
		"name" => "Type",
		"id" => "button_type",
		"std" => "circle",
		"type" => "radio",
		"options" => array("badge","circle","flower","flower2","house")),
	array(    
		"name" => "Enter links to your social networking sites.",
		"type" => "title"),
	array(
		"name" => "Facebook",
		"id" => "facebook",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "Twitter",
		"id" => "twitter",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "Digg",
		"id" => "digg",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "MySpace",
		"id" => "myspace",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "LinkedIn",
		"id" => "linkedin",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "YouTube",
		"id" => "youtube",
		"type" => "input",
		"std" => ""),
	array(
		"type" => "close"),

	array(    
		"name" => "Page Slugs or IDs",
		"type" => "open"),
	array(    
		"name" => "This is a highly customized theme, with links to pages or content displayed from pages. Enter the page ID or page slug for these pages.",
		"type" => "title"),
		array(
		"name" => "Footer Links Page IDs",
		"id" => "extra_links",
		"type" => "input",
		"std" => "25,26",
		"description" => "Enter the Page IDs of the extra pages you want linked in the footer, separated by commas. Twitter and Agent Login will be included in the list."),
	array(
		"type" => "close"),

);
function theme_add_admin() {
	global $themename, $shortname, $options;

	if( $_GET['page'] == basename(__FILE__) ) {
    	if( $_REQUEST['save'] ) {
			foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] ); 
			}
			
                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { 
											if( $value['type'] == 'checkbox' ) {
												if( $value['status'] == 'checked' ) {
													update_option( $value['id'], 1 );
												} else { 
													update_option( $value['id'], 0 ); 
												}	
											} elseif( $value['type'] != 'checkbox' ) {
												update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); 
											} else { 
												delete_option( $value['id'] ); 
											}
										}
									}
			
			header("Location: admin.php?page=functions.php&saved=true");
			
			die;
        } elseif ( $_REQUEST['reset'] ) {
			foreach ( $options as $value ) {
				delete_option( $value['id'] );
			}
			
			header("Location: admin.php?page=functions.php&saved=true");
			
			die;
		}
	}

	$themename = ucfirst($themename);
	add_theme_page($themename." Theme Options", $themename." Theme Options", 'edit_themes', basename(__FILE__), 'theme_admin');

} 
function theme_admin() {
	
	global $themename, $shortname, $options;
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/styles/themeoptions.css" />'. "\n";
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/styles/colorpicker/colorpicker.css" />'. "\n";
	echo '<script type="text/javascript" src="' . get_bloginfo('template_directory') . '/js/colorpicker.js"></script>'. "\n";
	echo '<script type="text/javascript" src="' . get_bloginfo('template_directory') . '/js/eye.js"></script>'. "\n";
	echo '<script type="text/javascript" src="' . get_bloginfo('template_directory') . '/js/layout.js"></script>'. "\n";
	echo '<script type="text/javascript" src="' . get_bloginfo('template_directory') . '/js/utils.js"></script>'. "\n";
	echo '<style type="text/css">';
	echo '.badge { background: url(' . get_bloginfo('template_directory') . '/images/follow/badge/facebook.png) no-repeat 100% 0; }';
	echo '.circle { background: url(' . get_bloginfo('template_directory') . '/images/follow/circle/facebook.png) no-repeat 100% 0; }';
	echo '.flower { background: url(' . get_bloginfo('template_directory') . '/images/follow/flower/facebook.png) no-repeat 100% 0; }';
	echo '.flower2 { background: url(' . get_bloginfo('template_directory') . '/images/follow/flower2/facebook.png) no-repeat 100% 0; }';
	echo '.house { background: url(' . get_bloginfo('template_directory') . '/images/follow/house/facebook.png) no-repeat 100% 0; }';
	echo '</style>';

	if ( $_GET['saved'] ) 
		echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings updated.</strong></p></div>';

	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
 
?>
<div class="wrap">
<h2><?php echo $themename; ?> Theme Option Settings</h2>
 
<form method="post">
<div class="leftside">

<?php foreach ($options as $value) {
switch ( $value['type'] ) {
 
case "open":
?>
<table class="widefat maintable" cellspacing="0">
<thead>
<tr>
	<th colspan="3" scope="col" class="manage-column column-title"><?php echo $value['name']; ?></th>
</tr>
</thead>
<?php break;
 
case "close":
?>
</table>
<?php break;
 
case "title":
?>
<tr><td colspan="3"><h3><?php echo $value['name']; ?></h3></td></tr>
 
<?php break;
 
case 'text':
?>
 
<tr>
	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field"><input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo get_option( $value['id'] ); } else { echo $value['std']; } ?>" /></td>
	<td class="instruction"><?php echo $value['description']; ?></td>
</tr>
 
<?php break;
 
case 'image':
?>
 
<tr>
	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field" colspan="2"><img src="<?php bloginfo('template_directory'); ?>/images/logos/<?php if ( get_option( $value['id'] ) != "") { echo stripslashes( get_option( $value['id'] ) ); } else { echo stripslashes( $value['std'] ); } ?>"  /></td>
</tr>
 
<?php
break;
 
case 'textarea':
?>
 
<tr>
	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field" colspan="2">
		<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="80" rows="100"><?php if ( get_option( $value['id'] ) != "") { echo get_option( $value['id'] ); } else { echo $value['std']; } ?></textarea>
	</td>
</tr>
<tr>
	<td class="instruction" colspan="3"><?php echo $value['description']; ?></td>
</tr>
 
<?php
break;
 
case 'select':
?>
<tr>
	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field">
		<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php if ( get_option( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ( get_option( $value['id'] ) == '' && $option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select>
	</td>
	<td class="instruction"><?php echo $value['description']; ?></td>
</tr>
 
<?php
break;
 
case "checkbox":
?>
<tr>

	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field">
							<?php
								if ( get_option( $value['id'] ) != "" ) { 
									$status= get_option( $value['id'] );
								} else { 
									$status= $value['std']; 
								}
							?>
	            <input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo get_option( $value['id'] ); } else { echo $value['std']; } ?>" <?php if( $status == 1 ) { echo 'checked'; } ?>/>
	</td>
	<td class="instruction"><?php echo $value['description']; ?></td>
</tr>
 
<?php
break;
 
case "radio":
?>
<tr>
	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field followme" colspan="2">
		<?php foreach ($value['options'] as $option) { ?>
		<label class="<?php echo $option; ?>"><span><?php echo $option; ?></span><input name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" class="<?php echo $value['type']; ?>" value="<?php echo $option; ?>"<?php if ( get_option( $value['id'] ) == $option) { echo ' checked="checked"'; } elseif ( get_option( $value['id'] ) == '' && $option == $value['std']){ echo ' checked="checked"'; } ?> /></label>
		<?php } ?>
	<div style="clear:both"></div></td>
</tr>
 
<?php
break;
 
case "input":
?>
<tr>
	<th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
	<td class="field">
		<input size="30" type="<?php echo $value['type']; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id'] )); } else { echo stripslashes($value['std']); } ?>" />
	</td>
	<td class="instruction"><?php echo $value['description']; ?></td>
</tr>
 
<?php break;
 
}
}
?>
<p class="submit">
<input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" />
</p>

</div>

<div id="poststuff">
<div class="postbox">
<h3>Update Data</h3>

<div class="inside">
<p class="submit">
<input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" />
</p>
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="Reset" />
<input type="hidden" name="action" value="reset" />
</p>
</form>
</div>

<div class="clear"></div>
</div>
</div>
<?php
}

add_action('admin_menu', 'theme_add_admin');

?>
<?php
/**
Made with the help of a tutorial at WPShout.com => http://wpshout.com.

Courtesy of the Hybrid theme - themehybrid.com

 * Adds the Hybrid Settings meta box on the Write Post/Page screeens
 *
 * @package Hybrid
 * @subpackage Admin
 */

/* Add a new meta box to the admin menu. */
	add_action( 'admin_menu', 'hybrid_create_meta_box' );

/* Saves the meta box data. */
	add_action( 'save_post', 'hybrid_save_meta_data' );

/**
 * Function for adding meta boxes to the admin.
 * Separate the post and page meta boxes.
 *
 * @since 0.3
 */
function hybrid_create_meta_box() {
	global $theme_name;

	add_meta_box( 'post-meta-boxes', __('Post options'), 'post_meta_boxes', 'post', 'normal', 'high' );
	add_meta_box( 'page-meta-boxes', __('Post options'), 'page_meta_boxes', 'page', 'normal', 'high' );
}

/**
 * Array of variables for post meta boxes.  Make the 
 * function filterable to add options through child themes.
 *
 * @since 0.3
 * @return array $meta_boxes
 */
function hybrid_post_meta_boxes() {

	/* Array of the meta box options. */
	$meta_boxes = array(
		'bodytitle' => array( 'name' => 'BodyTitle', 'title' => __('BodyTitle', 'hybrid'), 'type' => 'text' ),
	

	);

	return apply_filters( 'hybrid_post_meta_boxes', $meta_boxes );
}

/**
 * Array of variables for page meta boxes.  Make the 
 * function filterable to add options through child themes.
 *
 * @since 0.3
 * @return array $meta_boxes
 */
function hybrid_page_meta_boxes() {

	/* Array of the meta box options. */
	$meta_boxes = array(
		'bodytitle' => array( 'name' => 'BodyTitle', 'title' => __('BodyTitle', 'hybrid'), 'type' => 'text' ),

	);

	return apply_filters( 'hybrid_page_meta_boxes', $meta_boxes );
}

/**
 * Displays meta boxes on the Write Post panel.  Loops 
 * through each meta box in the $meta_boxes variable.
 * Gets array from hybrid_post_meta_boxes().
 *
 * @since 0.3
 */
function post_meta_boxes() {
	global $post;
	$meta_boxes = hybrid_post_meta_boxes(); ?>

	<table class="form-table">
	<?php foreach ( $meta_boxes as $meta ) :

		$value = get_post_meta( $post->ID, $meta['name'], true );

		if ( $meta['type'] == 'text' )
			get_meta_text_input( $meta, $value );
		elseif ( $meta['type'] == 'textarea' )
			get_meta_textarea( $meta, $value );
		elseif ( $meta['type'] == 'select' )
			get_meta_select( $meta, $value );

	endforeach; ?>
	</table>
<?php
}

/**
 * Displays meta boxes on the Write Page panel.  Loops 
 * through each meta box in the $meta_boxes variable.
 * Gets array from hybrid_page_meta_boxes()
 *
 * @since 0.3
 */
function page_meta_boxes() {
	global $post;
	$meta_boxes = hybrid_page_meta_boxes(); ?>

	<table class="form-table">
	<?php foreach ( $meta_boxes as $meta ) :

		$value = stripslashes( get_post_meta( $post->ID, $meta['name'], true ) );

		if ( $meta['type'] == 'text' )
			get_meta_text_input( $meta, $value );
		elseif ( $meta['type'] == 'textarea' )
			get_meta_textarea( $meta, $value );
		elseif ( $meta['type'] == 'select' )
			get_meta_select( $meta, $value );

	endforeach; ?>
	</table>
<?php
}

/**
 * Outputs a text input box with arguments from the 
 * parameters.  Used for both the post/page meta boxes.
 *
 * @since 0.3
 * @param array $args
 * @param array string|bool $value
 */
function get_meta_text_input( $args = array(), $value = false ) {

	extract( $args ); ?>

	<tr>
		<th style="width:10%;">
			<label for="<?php echo $name; ?>"><?php echo $title; ?></label>
		</th>
		<td>
			<input type="text" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo wp_specialchars( $value, 1 ); ?>" size="30" tabindex="30" style="width: 97%;" />
			<input type="hidden" name="<?php echo $name; ?>_noncename" id="<?php echo $name; ?>_noncename" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
		</td>
	</tr>
	<?php
}

/**
 * Outputs a select box with arguments from the 
 * parameters.  Used for both the post/page meta boxes.
 *
 * @since 0.3
 * @param array $args
 * @param array string|bool $value
 */
function get_meta_select( $args = array(), $value = false ) {

	extract( $args ); ?>

	<tr>
		<th style="width:10%;">
			<label for="<?php echo $name; ?>"><?php echo $title; ?></label>
		</th>
		<td>
			<select name="<?php echo $name; ?>" id="<?php echo $name; ?>">
			<?php foreach ( $options as $option ) : ?>
				<option <?php if ( htmlentities( $value, ENT_QUOTES ) == $option ) echo ' selected="selected"'; ?>>
					<?php echo $option; ?>
				</option>
			<?php endforeach; ?>
			</select>
			<input type="hidden" name="<?php echo $name; ?>_noncename" id="<?php echo $name; ?>_noncename" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
		</td>
	</tr>
	<?php
}

/**
 * Outputs a textarea with arguments from the 
 * parameters.  Used for both the post/page meta boxes.
 *
 * @since 0.3
 * @param array $args
 * @param array string|bool $value
 */
function get_meta_textarea( $args = array(), $value = false ) {

	extract( $args ); ?>

	<tr>
		<th style="width:10%;">
			<label for="<?php echo $name; ?>"><?php echo $title; ?></label>
		</th>
		<td>
			<textarea name="<?php echo $name; ?>" id="<?php echo $name; ?>" cols="60" rows="4" tabindex="30" style="width: 97%;"><?php echo wp_specialchars( $value, 1 ); ?></textarea>
			<input type="hidden" name="<?php echo $name; ?>_noncename" id="<?php echo $name; ?>_noncename" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
		</td>
	</tr>
	<?php
}

/**
 * Loops through each meta box's set of variables.
 * Saves them to the database as custom fields.
 *
 * @since 0.3
 * @param int $post_id
 */
function hybrid_save_meta_data( $post_id ) {
	global $post;

	if ( 'page' == $_POST['post_type'] )
		$meta_boxes = array_merge( hybrid_page_meta_boxes() );
	else
		$meta_boxes = array_merge( hybrid_post_meta_boxes() );

	foreach ( $meta_boxes as $meta_box ) :

		if ( !wp_verify_nonce( $_POST[$meta_box['name'] . '_noncename'], plugin_basename( __FILE__ ) ) )
			return $post_id;

		if ( 'page' == $_POST['post_type'] && !current_user_can( 'edit_page', $post_id ) )
			return $post_id;

		elseif ( 'post' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id ) )
			return $post_id;

		$data = stripslashes( $_POST[$meta_box['name']] );

		if ( get_post_meta( $post_id, $meta_box['name'] ) == '' )
			add_post_meta( $post_id, $meta_box['name'], $data, true );

		elseif ( $data != get_post_meta( $post_id, $meta_box['name'], true ) )
			update_post_meta( $post_id, $meta_box['name'], $data );

		elseif ( $data == '' )
			delete_post_meta( $post_id, $meta_box['name'], get_post_meta( $post_id, $meta_box['name'], true ) );

	endforeach;
}

	function image_ratio($image_source, $max_width, $max_height) {
		if(strpos($image_source,"wp-content")===false)return '0';
		$image_array=explode("wp-content",$image_source);
		$image_source=ABSPATH."wp-content".$image_array[1];
		list($width, $height, $type, $attr) = getimagesize($image_source);
		$img_ratio = $width / $max_width;
		$img_height = $height / $img_ratio;
		$margin_top = '0px';
		if ($img_height > $max_height) {
			$margin_top = '-' . round(($img_height - $max_height) / 2) . 'px';
		}
		return $margin_top;
	}
?>