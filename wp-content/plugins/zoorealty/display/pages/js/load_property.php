<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
require($path.'/wp-load.php');
global $wpdb , $helper, $realty;
$id = $_GET['id'];
if(empty($id))return;
if(!empty($id))$listings=$realty->property($id);
?>

<?php
if(!empty($listings['photos'])): 
	$photos = array();
	$j='0';$k='0';$l='0';
	for ($i=0;$i<count($listings['photos']);$i++){
		if($listings['photos'][$i]['small']!=""){ $photos[$j]['small']=$listings['photos'][$i]['small']; $j++; }
		if($listings['photos'][$i]['medium']!=""){ $photos[$k]['medium']=$listings['photos'][$i]['medium']; $k++; }
		if($listings['photos'][$i]['large']!=""){ $photos[$l]['large']=$listings['photos'][$i]['large']; $l++; }
	}
?>
<div class="property-image">
    <script type="text/javascript">function swapImage(id){document.getElementById('main_image').src = id;jQuery("#main_image").show("fast");}</script>
    <div class="image"><img src="<?php echo $photos[0]['large']; ?>" id="main_image" /></div>
    <ul class="image-thumbs">
        <?php $j=0; foreach($photos as $photo):$x++;if($x=='5')break; ?>
        <li class="thumb<?php echo $j; ?>"><a href="javascript:swapImage('<?php echo $photo['large']; ?>')" title="<?php $photo['name']; ?>"><img src="<?php echo $photo['small']; ?>" alt="" /></a></li>
        <?php $j++; endforeach; ?>
    </ul>
    <ul class="rooms">
        <li class="bedrooms"><span class="room_count"><?php echo $listings['bedrooms']; ?></span><span class="room_type">bedrooms</span></li>
        <li class="bathrooms"><span class="room_count"><?php echo $listings['bathrooms']; ?></span><span class="room_type">bathrooms</span></li>
        <li class="carspaces"><span class="room_count"><?php echo $listings['carspaces']; ?></span><span class="room_type">carspaces</span></li>
    </ul>
    
    <p class="map-back-link"><a href="javascript:void(0)" onClick="close_property_detail('<?php $listings['latitude']?>','<?php $listings['longitude']?>')">Return To Map</a></p>

</div>
<?php endif; ?>

<div class="property-info">
    <p class="headline"><strong><?php echo $listings['headline']; ?></strong></p>
    <p class="description"><?php echo $helper->_substr($listings['description'], 200); ?>&hellip;<a href="<?php echo $listings['url']; ?>" title="View Property">More &raquo;</a></p>
    <p class="auction"><strong><?php echo $listings['price']; ?></strong><br/><strong><?php if(!empty($listings['street_address']))$address=$listings['street_address'].", ";if(!empty($listings['suburb']))$address.=$listings['suburb'].", ";if(!empty($listings['state']))$address.=$listings['state'].", ";echo substr($address,0,-2) ;?></strong></p>
    
    <?php  if(!empty($listings['itemized_features'])):	?>
    <div id="property_features_detail">
        <h4>Features</h4>
        <p><span><?php echo implode('<small>&nbsp;&#8226;&nbsp;</small></span><span>', $listings['itemized_features']) . "<small>&nbsp;&#8226;&nbsp;</small></span>";	?></p>
    </div>
    <?php endif; ?>
    
    <div id="agent_info">
        <?php for($i=0;$i<=1;$i++){
			$user = $listings['user'][$i]; 
			if(empty($user)) continue; ?>
			<div class="map-agent-wrap">
                <h4><a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><?php echo $user['name']; ?></a></h4>
                <?php if(!empty($user['business_phone'])): ?><p><strong>P:</strong><span>&nbsp;<?php echo $user['business_phone']; ?></span></p><?php endif; ?>
                <?php if(!empty($user['mobile'])): ?><p><strong>M:</strong><span>&nbsp;<?php echo $user['mobile']; ?></span></p><?php endif; ?>
                <?php if(!empty($user['email'])): ?><p><strong>E:</strong><span>&nbsp;<a href="mailto:<?php echo $user['email']; ?>" title="Email Agent"><?php echo $user['email']; ?></a></span></p><?php endif; ?>
                <?php if(!empty($user['fax'])): ?><p><strong>F:</strong><span>&nbsp;<?php echo $user['fax']; ?></span></p><?php endif; ?>
        	</div>
		<?php } ?>

        <?php if(!empty($listings['user'][0])): ?><p class="button email_agent_btn"><a  href="#"  onclick="javascript:window.open('<?php echo get_option('siteurl').'/wp-content/plugins/Realty/display/elements/form_contact_agent.php/?popup=1&user_id='.$listings['user'][0]['user_id'].'&user_id1='.$listings['user'][1]['user_id'].'&property_id='.$listings['id']; ?>', '', 'width=500, height=500,resizable=1,scrollbars=1');return false;" title="Email Agent" class="btn">Email Agent</a></p><?php endif; ?>
    </div>
</div>
<div class="clear"></div>