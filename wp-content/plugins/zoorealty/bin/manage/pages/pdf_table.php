<?php
//metadata: access_level=1
if(isset($_POST['submit'])):
	$total=0;
	foreach($_POST['realty_pdf_table'] as $key=>$item):
		if(isset($item['display']) && is_array($item))$total++;
	endforeach;
	if($total<16){
	update_option('realty_pdf_table', $_POST['realty_pdf_table']);
	$return = 'Settings Updated.';
	}
	else{
	$return = 'The maximum number of items checked is 15.';
	}
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
add_option('realty_pdf_table',array(
 'street_address' => array ('display' => '1','title' => 'Street Address',),
 'state' => array ('display' => '1','title' => 'State',),
 'town_village' => array ('display' => '1','title' => 'Region',),
 'suburb' => array ('display' => '1','title' => 'Suburb',), 
 'postcode' => array ('display' => '1','title' => 'Postcode',),
 'property_type' => array ('display' => '1','title' => 'Property Type',),
 'price' => array ('display' => '1','title' => 'price',),
 'bedrooms' => array ('display' => '1','title' => 'Beds',),
 'bathrooms' => array ('display' => '1','title' => 'Bathrooms',),
 'carspaces' => array ('display' => '1','title' => 'Cars',),
 'building_size' => array ( 'title' => 'Building Size', 'display' => array ('Sqms' )),
));
$settings = get_option('realty_pdf_table');
?>
<?php wp_print_scripts('jquery'); ?>
<div class="wrap">
<form name="" method="post" action="" id="elements">
<?php $total=0;
	foreach($settings as $key=>$item):
		if(isset($item['display']) && is_array($item))$total++;
	endforeach;
?>
<h2>Current Display</h2>
<p>You are displaying <?php echo $total; ?> items in your pdf table.</p>
<p>You can "Check" items in the table, make sure you save your changes.(max 15 items)</p>
<?
$items = array (
  'street_address','type','price', 'suburb', 'town_village','state', 'postcode', 'country', 'property_type', 'bedrooms', 'bathrooms',
 'carspaces', 'carport', 'garage','off_street_park','land_size'=>array('Sqms'),
  'building_size'=>array('Sqms'), 'headline','available_at', 'auction_date', 'auction_time',  'sold_at','sold_price','contract_at', 'auction','leased_price', 'leased_at','maximum_persons','high_season_price','mid_season_price','peak_season_price','estimate_rental_return','category','additional_notes','year_built','virtual_tour','vendor_email','vendor_first_name','vendor_last_name','vendor_phone','annual_ebit','annual_gross_profit','annual_turnover','business_name','tax_rate','condo_strata_fee','energy_efficiency_rating','current_outgoings','current_rent','business_name','lease_commencement','lease_end','lease_option','lease_plus_another','number_of_floors','parking','patron_capacity','rent_review','return_percent','zoning','garage_area','porch_terrace_area',
);
global $helper;?>
<table class="widefat">
<?php $i=0;foreach ($items as $key=>$item):
if(is_array($item)): ?>
<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>>
<td><?php echo $helper->humanize($key); ?></td>
<td><input value="<?php echo (empty($settings[$key]['title']))? $helper->humanize($key):$settings[$key]['title']; ?>" name="realty_pdf_table[<?php echo $key; ?>][title]" type="text" size="30">
<?php foreach ($item as $subitem): ?>
<br /><input value="<?php echo $subitem; ?>" name="realty_pdf_table[<?php echo $key; ?>][display][]" type="checkbox"<?php if(is_array($settings[$key]['display']) and in_array($subitem, $settings[$key]['display'])) echo ' checked="checked"'; ?>><label> <?php echo $helper->humanize($subitem); ?>
<?php endforeach; ?></td></tr>
<?php else: ?>
<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>>
<td><?php echo $helper->humanize($item); ?></td> 
<td><input value="1" name="realty_pdf_table[<?php echo $item; ?>][display]" type="checkbox"<?php if($settings[$item]['display']) echo ' checked="checked"'; ?> > 
<input value="<?php echo (empty($settings[$item]['title']) || $settings[$item]['title'] =="1")? $helper->humanize($item):$settings[$item]['title']; ?>" name="realty_pdf_table[<?php echo $item; ?>][title]" type="text" size="30"></td></tr>
<?php endif;$i++; endforeach; ?>
<tr class="alternate">
	<td width="50%">You can put the logo in the pdf, but please place the logo in <?php echo get_option('siteurl')?>/wp-content/plugins/Realty/display/elements/images and the logo must be in .jpeg image format. Write the filename of the logo in here. (eg logo.jpg)</td>
	<td><input value="<?php echo $settings['logo']; ?>" name="realty_pdf_table[logo]" type="text" size="30"></td>
</tr>
<tr>
	<td>You can put the text in the right top of the pdf. Write the text in here.</td>
	<td><textarea name="realty_pdf_table[text_header]" cols="50" rows="3"><?php echo $settings['text_header']; ?></textarea></td>
</tr>
<tr class="alternate">
	<td>You can put the text in the bottom of the pdf. Write the text in here.</td>
	<td><textarea name="realty_pdf_table[text_bottom]" cols="50" rows="3"><?php echo $settings['text_bottom']; ?></textarea></td>
</tr>
</table>
 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->