<?php $listings = $params; ?>
<?php global $realty; 
$src=str_replace('wp-content/plugins/Realty/style/stickers/','',$realty->settings['property_stickers']['sticker_src']);
$style="style".(str_replace("sticker","",str_replace(".png","",$src))+1);
switch($realty->settings['property_stickers']['sticker_colour']){
case '#2b497d';$colors='dk_blue';break;
case '#2b7d36';$colors='dk_green';break;
case '#857a82';$colors='grey';break;
case '#70c141';$colors='lite_green';break;
case '#4cb2c2';$colors='lite_blue';break;
case '#d78c23';$colors='orange';break;
case '#7e2a1d';$colors='red';break;
case '#7d5f2b';$colors='tan';break;
case '#d7ce31';$colors='yellow';break;
}
?>
<div id="thumbnail_format" <?php if($realty->settings['general_settings']['display_search_result_in_thumbnail_format']!='1') echo 'style="display:none;"';?>>
<table class="property" width="100%" border="0" cellspacing="0" cellpadding="0">
<?php	
foreach($listings as $listing):
	if(!is_array($listing)) continue; 
	if ($count % 3==0){
	?>
	<tr <?php /*echo ($count % 4=='0') ? ' class="alt"':'';*/ ?>>
	<?php } 
	 if(is_array($realty->settings['search_results_thumbnail'])) ?>
		<td class="thumbCont<?php echo ($count % 3==1) ? ' alt':''; ?>">
			 <div class="item-wrap">
                <div class="image">
					<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>">
						<img src="<?php echo $listing['medium']; ?>" alt="<?php echo $listing['headline']; ?>" />
						<span class="inbox-border"><span></span></span>
					</a>
						<?php  if ($listing['new']):?>
						<div class="fresh_place">
						</div>
						<?php elseif ($listing['is_sold']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker sold ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo $realty->settings['property_stickers']['sticker_text']['sold'];?></span>
							</div>
						</div>
						<?php elseif ($listing['is_leased']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker leased ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo $realty->settings['property_stickers']['sticker_text']['leased'];?></span>
							</div>
						</div>
						<?php elseif ($listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker under_offer ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo $realty->settings['property_stickers']['sticker_text']['under_offer'];?></span>
							</div>
						</div>
						<?php elseif (!empty($listing['is_open_home']) && !$listing['under_offer']):?>
						<div class="image_overlay">
							<div class="<?php echo "sticker open_home ".$colors." ".$style;?>">
								<a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['street_suburb']; ?>"></a>
								<span><?php echo ($realty->settings['property_stickers']['sticker_text']['open_home']=='')?'Open Home':$realty->settings['property_stickers']['sticker_text']['open_home'];?></span>
							</div>
						</div>
						<?php endif;  ?>
                </div>
                <div class="tdContent">
                <?php	
				foreach($realty->settings['search_results_thumbnail'] as $key=>$values):
					if($key=='thumbnail') continue;
                    switch($key):
                        case 'number_of_rooms': 
                            if ( !$listing['is_land'] and is_array($values) and !empty($values)): ?>
								<ul class="rooms">
								<?php  
								foreach($values as $param):
									if(!empty($listing[$param])):		 ?>	
									<li class="<?php echo $param; ?>">
										<span class="room_count"><?php echo $listing[$param]; ?></span> <span class="room_type"><?php echo $param; ?></span>
									</li>
									<?php
									endif;
								endforeach; ?>
								</ul>
								<?php 
                            endif;
                        break;
                        case 'address':
							if(is_array($values)) foreach($values as $addressPart)${$addressPart} = $listing[$addressPart];
							$street_address=ucwords(strtolower($street_address));
							$address='';
							if(!empty($street_address))$address=$street_address.", ";
							if(!empty($suburb))$address.=$suburb.", ";
							if(!empty($state))$address.=$state.", "; ?>
							<p class="suburb"><a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>"><?php echo substr($address,0,-2) ;?></a></p>
						<?php						
                        break;
                        case 'description': ?>
						<p class="description">
							<?php echo $helper->_substr($listing['description'], $values); ?>&hellip;
							<a href="<?php echo $listing['url']; ?>" title="View Property">More &raquo;</a>
						</p>
						<?php
                        break;
                        case 'building_size':
							if(is_array($listing[$key]) and is_array($values) and !empty($listing[$key]) and $listing['is_for_sale']): ?>
							<div class="land_building_size">
								<p class="building_size">Building Size: 
								<?php foreach($values as $unit): ?>	<span><?php echo $listing[$key][$unit]; ?></span>
								<?php endforeach;?>
								</p>
							</div>
							<?php
                            endif;						
                        break;
                        case 'land_size':						
                            if(is_array($listing[$key]) and is_array($values) and !empty($listing[$key]) and $listing['is_for_sale']): ?>
								<div class="land_building_size">
									<p class="land_size">Land Size: 
									<?php foreach($values as $unit): ?>	<span><?php echo $listing[$key][$unit]; ?></span>
									<?php endforeach;?>
									</p>
								</div>
							<?php
                            endif;                            
                        break;
                        case 'auction':
							if (!empty($listing[$key]) && $listing[$key]!='00:00:00'): ?>
								<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
							endif;
                        break;
                        case 'primary_contact': 
                            $user = $realty->user($listing['user_id']);
                            if(!empty($user)):  ?>
							<p>Contact</p>
							<?php
                            foreach($values as $param):
								if(!empty($user[$param])):?>
								<p>
									<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
									<?php if($param=='email'): ?><a href="mailto:<?php echo $user[$param]; ?>"><?php echo $user[$param]; ?></a>
									<?php else: ?>
									<?php echo $user[$param]; ?>
									<?php endif; ?>
									</span>
								</p>
								<?php
                                endif;
                                endforeach;
                            endif;						
                        break;
                        case 'opentimes':
                            $opentimes = $this->opentimes($listing['id']); 
                            if(!empty($opentimes)) : ?>	
								<p><span>Open</span> Times</p>
								<ul>
								<?php foreach($opentimes as $opentime): ?>
									<li><span class="open_date"><strong>
									<?php echo date('l, j M Y', strtotime($opentime['date'])); ?></strong></span><br />
									<span class="open_time"><?php echo $opentime['start_time']; ?> - <?php echo $opentime['end_time']; ?></span></li>
								<?php endforeach; ?>
								</ul>
							<?php
                            endif;
                        break;
                        case 'available_at':
                            if(!empty($listing[$key]) && $listing[$key]!='0000-00-00'):	 ?>	
							<p>Available on: </p>						
                            <p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
                            <?php
                            endif;                        
                        break;        
                        case 'secondary_contact':
                            if(!empty($listing['secondary_user']) && $listing['secondary_user']!= $listing['user_id'])
							$secondary_user = $this->user($listing['secondary_user']);
                            if(!empty($secondary_user)):
                                foreach($values as $param):
                                    if(!empty($user[$param])): ?>
									<p>
										<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
										<?php if($param=='email'): ?><a href="mailto:<?php echo $secondary_user[$param]; ?>"><?php echo $secondary_user[$param]; ?></a>
										<?php else: ?>
										<?php echo $secondary_user[$param]; ?>
										<?php endif; ?>
										</span>
									</p>
									<?php
                                    endif;
                                endforeach;
                            endif;					
                        break;        
						case 'price':
							if (!$listing["is_auction"] && $listing["status"]!='2'):?>
								<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php elseif($listing["status"]=='2'): ?>
								<p class="<?php echo $key; ?>"><?php echo ($listing['display_sold_price'])? $listing['last_price']:'Undisclosed'; ?></p>
							<?php else : ?>
								<p class="<?php echo $key; ?>">Auction: <?php 
									if(!empty($listing["auction_place"]))echo $listing["auction_place"].", ";							
									echo $listing["auction_date"]; 
									if(!empty($listing["auction_time"]))echo " at ".$listing["auction_time"]; ?>
								</p>
							<?php endif;
						break;	
                        default:
                            if(!empty($listing[$key])):	?>							
                                <p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
                            <?php
                            endif;
                        break;
                    endswitch;
                endforeach; ?>
                <div class="clear"></div>
                </div><!--ends .tdContent-->
            </div><!--ends .item-wrap--></td>

<?php if ($count % 3==2 or $count==($listings['results_count']-1)){ ?></tr><?php }$count++; ?>
<?php endforeach;
?>	</table>
</div>