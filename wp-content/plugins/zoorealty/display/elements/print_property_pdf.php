<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require($path.'/wp-load.php');

global $helper, $favourite, $realty, $wpdb;
$condition=$_GET;
$properties = $realty->property();
$id=$properties['id'];
if(class_exists('zoo_analytics') && ($properties['status']=='1' || $properties['status']=='4' )){ global $zoo_analytics; $zoo_analytics->save_trackers($_REQUEST['property_id'],'brochure'); }//save into zoo analytics
require('fpdf.php');
ob_start();
function remove_weird_characters($text){
	$text = iconv('UTF-8', 'windows-1252', $text);
	return $text;
}

class PDF extends FPDF
{
	function print_detail($title, $value, $y){
		$this->SetFont('Helvetica','B',8);
		$this->SetXY(116.5, $y);
		$this->MultiCell(60, 4, $title.":", 0, 'L');
		$this->SetFont('Helvetica','',8);
		$this->SetXY(138, $y);
		$this->MultiCell(60, 4, $value, 0, 'L');
	}
}

$pdf=new PDF('P','mm','A4');
$odd_class = "";
$pdf->SetAutoPageBreak(True, 5); //Earlier 40
$pdf->AliasNbPages();
$pdf->AddPage();

$logo=$realty->pluginUrl.'display/elements/images/logo.jpg';

/* image */
$y=20;
if(!empty($properties['photos'])){
	$photos = array();
	$i=0;
	foreach($properties['photos'] as $photo){
		$size=($i==0)?'large':'medium';
		$photos[$i]=$photo[$size];
		$i++;
	}
	
	$pdf->SetFillColor(255,255,255);
	
	if(!empty($photos[0])){  // Main image; The big one		
		$sizes = $wpdb->get_results("select width, height from attachments where url='".$photos[0]."'", ARRAY_A);
		$height=$sizes[0]['height'];
		$width=$sizes[0]['width'];
		if($height>$width)$pdf->Image($photos[0],20,10,170,150);
		else $pdf->Image($photos[0],20,10,170);
		$pdf->Rect(0,9,210,8,'F');	
		$pdf->Rect(190,10,20,125,'F');
		$pdf->Rect(0,120,210,200,'F');			
	}
	$y=130;
	
	if(!empty($photos[1])){ // 1st thumb
		$sizes = $wpdb->get_results("select width, height from attachments where url='".$photos[1]."'", ARRAY_A);
		$height=$sizes[0]['height'];
		$width=$sizes[0]['width'];
		if($height>$width)$pdf->Image($photos[1],0,125,110,90); 
		else $pdf->Image($photos[1],0,125,110); 
	}
	if(!empty($photos[2])){ // 2nd thumb	
		$sizes = $wpdb->get_results("select width, height from attachments where url='".$photos[2]."'", ARRAY_A);
		$height=$sizes[0]['height'];
		$width=$sizes[0]['width'];
		if($height>$width)$pdf->Image($photos[2],115.5,125,110,90); 
		else $pdf->Image($photos[2],115.5,125,110); 
	}
	
	if(!empty($photos[1])){		
		$y=205;
		$pdf->Rect(0,120,20,90,'F');	
		$pdf->Rect(190,120,20,90,'F');	
		$pdf->Rect(0,196,210,120,'F');	
	}	
	
}


/* address */
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(12,177,75);
// $pdf->SetFillColor(53,53,55);
$pdf->Rect(20,$y-4,95,15,'F');	
$pdf->SetFont('Helvetica','B',17);
$pdf->SetXY(23, $y);
$pdf->MultiCell(100, 2, $properties['suburb'].", ".$properties['state'], 0, 'L');
$pdf->SetFont('Helvetica','',13);
$pdf->SetXY(23, $y+5);
$pdf->MultiCell(100, 2, $properties['street_address'], 0, 'L');

/* beth, bath, car */
$pdf->SetFillColor(12,177,75);
// $pdf->SetFillColor(255,119,1);	
$pdf->SetFont('Helvetica','B',33);
$pdf->Rect(116.5,$y-4,11,15,'F');
$beds=floor($properties['bedrooms']);
$baths=floor($properties['bathrooms']);
if(strpos($properties['bedrooms'],'.5')!==false){  $half_bed='1'; }
if(strpos($properties['bathrooms'],'.5')!==false){  $half_bath='1'; }
$pdf->SetXY(118, $y+1);
$pdf->MultiCell(100, 2, $beds, 0, 'L');	
$pdf->Rect(129,$y-4,11,15,'F');	
$pdf->SetXY(130.5, $y+1);
$pdf->MultiCell(100, 2, $baths, 0, 'L');	
$pdf->Rect(141.5,$y-4,11,15,'F');
$pdf->SetXY(143, $y+1);
$pdf->MultiCell(100, 2, floor($properties['carspaces']), 0, 'L');

$pdf->SetTextColor(53,53,55);
$pdf->SetFont('Helvetica','',10);
$pdf->SetXY(117.5, $y+7);
$pdf->MultiCell(100, 2, 'BED', 0, 'L');	
$pdf->SetXY(129, $y+7);
$pdf->MultiCell(100, 2, 'BATH', 0, 'L');	
$pdf->SetXY(142.5, $y+7);
$pdf->MultiCell(100, 2, 'CAR', 0, 'L');	

if($half_bed=='1' || $half_bath=='1'){
	$pdf->SetDrawColor(255,255,255);	
	$pdf->SetFillColor(53,53,55);
	if($half_bed=='1'){
		$pdf->Rect(124.5,$y-3,1,3,'DF'); // vertikal
		$pdf->Rect(123.5,$y-2,3,1,'DF'); // horizontal
		$pdf->SetDrawColor(53,53,55);
		$pdf->Line(124.6,$y-2,125.4,$y-2);
		$pdf->Line(124.6,$y-1,125.4,$y-1);
		
	}
	$pdf->SetDrawColor(255,255,255);
	if($half_bath=='1'){
		$pdf->Rect(137,$y-3,1,3,'DF'); // vertikal
		$pdf->Rect(136,$y-2,3,1,'DF'); // horizontal
		$pdf->SetDrawColor(53,53,55);
		$pdf->Line(137.1,$y-2,137.9,$y-2);
		$pdf->Line(137.1,$y-1,137.9,$y-1);
	}
}
	
$pdf->SetFillColor(46,49,146);
$pdf->Rect(154,$y-4,36,15,'F');	
$pdf->Image($logo,154,$y-1,36);

/* headline */
$pdf->SetFont('Helvetica','B',10);
$pdf->SetXY(20, $y+16);
$pdf->MultiCell(95, 4, $properties['headline'], 0, 'L');
$headline_row=ceil(strlen($properties['headline'])/48);
$next=$y+16+($headline_row*4);
$desc_length=1200-($headline_row*100);

/* description */
$pdf->SetFont('Helvetica','',8);
$pdf->SetTextColor(0,0,0);
$pdf->SetXY(20, $pdf->getY());
$desc = str_replace("\t", " ", $properties['description']);
$desc = preg_replace("[\t\n\r]", "\n", remove_weird_characters($desc));
$selective_desc = $helper->_substr($desc,$desc_length);
$desc_rows=explode("\n",$selective_desc);
foreach($desc_rows as $desc_row){
	$y_desc=$pdf->GetY();
	if(	$y_desc<=275){
		$pdf->SetXY(20, $y_desc);
		$pdf->MultiCell(95, 4, trim($desc_row), 0, 'L'); 
	}
}

/* price */
$pdf->SetFont('Helvetica','B',10);
$pdf->SetTextColor(53,53,55);
$pdf->SetXY(116.5, $y+16);
if(!empty($properties['auction_date'])){
	$auction_date='Auction: '.date("D, j M Y",strtotime($properties['auction_date']));
	if(!empty($properties['auction_time']) && $properties['auction_time']!='00:00:00')$auction_date.=" @".date('g:ia', strtotime($properties['auction_time']));
	if(!empty($properties['auction_place']))$auction_date.="\n".$properties['auction_place'];	
	$pdf->MultiCell(60, 4,  $auction_date, 0, 'L');
}
else{	
	$pdf->MultiCell(60, 4, $properties['price'], 0, 'L');
}

/* detail */
$pdf->SetTextColor(0,0,0);
$next_detail=$pdf->getY();
$details=array('tax_rate'=>'Council Rates','condo_strata_fee'=>'Water Rates','opentimes'=>'View','user'=>'Contact','property_type'=>'Type','available_at'=>'Date Available','sold_at'=>'Sold Date','leased_at'=>'Leased Date','land_size'=>'Land','bond'=>'Bond','year_built'=>'Year Built');
foreach($details as $key=>$values){
	if(empty($properties[$key])) continue;
	switch($key){
		case 'sold_at' : case 'leased_at' : case 'available_at' :
			if ($properties[$key]!='0000-00-00')$pdf->print_detail($values, date("d/m/Y",strtotime($properties[$key])), $next_detail);
		break;
		case 'sold_price' :case 'leased_price' :case 'bond' : 
			$pdf->print_detail($values, "$".$properties[$key], $next_detail);
		break;
		case 'opentimes': 
			$i=0;
			$detail='';
			foreach($properties['opentimes'] as $opentime){
				$detail.=date('D, j M Y', strtotime($opentime['date']))." @".date('g:i a',strtotime($opentime['start_time']))." - ".date('g:i a',strtotime($opentime['end_time']));
				$detail.="\n";
				$i++;
			}
			$pdf->print_detail($values, $detail, $next_detail);
			if($i>0)$next_detail=$next_detail+(($i-1)*4);
		break;
		case 'user';
			$detail=$properties['user'][0]['name'];
			$i=0;
			if(!empty($properties['user'][0]['mobile'])){ $detail.="\n".$properties['user'][0]['mobile'];$i++; }
			if(!empty($properties['user'][1]['name'])){ $detail.="\n".$properties['user'][1]['name'];$i++; }
			if(!empty($properties['user'][1]['mobile'])){ $detail.="\n".$properties['user'][1]['mobile'];$i++; }
			$pdf->print_detail($values, $detail, $next_detail);
			if($i>0)$next_detail=$next_detail+($i*4);
		break;
		case 'land_size';
			$pdf->print_detail($values, $realty->property[$key].$realty->property['land_area_metric'], $next_detail);
		break;
		case 'tax_rate': case 'condo_strata_fee':
			$key_period=$properties[$key.'_period'];
			switch($key_period){
				case 'Per Week': $period='/weeky (approx)';break;
				case 'Per Month': $period='/month (approx)';break;
				case 'Per Quarter': $period='/qtr (approx)';break;
				case 'Per Year': $period='/year (approx)';break;
				default :$period=$key_period;break;
			}
			$pdf->print_detail($values, '$'.number_format($properties[$key]).$period, $next_detail);
		break;
		default:
			$pdf->print_detail($values, $properties[$key], $next_detail);
		break;
		
	}	
	$next_detail=$next_detail+4;
}

$pdf->SetFont('Helvetica','B',8);
$pdf->SetXY(116.5, $next_detail);
$pdf->MultiCell(60, 4, get_option('siteurl'), 0, 'L');

/* florplan */
/*
if(!empty($properties['floorplans'])){	
	$floorplans = array();
	$i=0;
	foreach($properties['floorplans'] as $floorplan){
		$floorplans[$i]=$floorplan['large'];
		$i++;
	}
} */

if (!empty($properties['floorplans']))
{
    $floorplans = array();
    $i = 0;
    foreach ($properties['floorplans'] as $floorplan)
    {
        $floorplans[$i] = $floorplan['large'];
        $i++;
    }
}

if ($_GET['debug'] == 'true')
{
    var_dump($floorplans[0]);
    var_dump(basename($floorplans[0]));
}

/** Convert Image Function **/
function convertImage($inputImage, $outputImage, $quality)
{
	if (!file_exists('temp')) {
	    mkdir('temp', 0777, true);
	}
    $imageTmp=imagecreatefromgif($inputImage);
    if(!empty($imageTmp)) {
	    imagejpeg($imageTmp, $outputImage, $quality);
	    imagedestroy($imageTmp);
	} else {
		$imageTmp=imagecreatefromjpeg($inputImage);
		imagejpeg($imageTmp, $outputImage, $quality);
	    imagedestroy($imageTmp);
	}
	return $outputImage;
}
    
$floorplanss = convertImage($floorplans[0], 'temp/thumb.jpg', 100);

if(!empty($floorplans[0])){	
	$pdf->AddPage();
	$pdf->Image($floorplanss,20,20,170,240);

	$pdf->SetFont('Helvetica','',7);
	$pdf->SetXY(20, 257);
	$pdf->MultiCell(100, 2, "Plans shown are only indicative of layout. Dimensions are approximate." , 0, 'L');
	$pdf->SetTextColor(255,255,255);
	$pdf->SetFillColor(12,177,75);
	// $pdf->SetFillColor(0,0,0);
	$pdf->Rect(20,261,170,15,'F');	
	$pdf->SetFont('Helvetica','B',17);
	$pdf->SetXY(23, 265);
	$pdf->MultiCell(100, 2, $properties['suburb'].", ".$properties['state'], 0, 'L');
	$pdf->SetFont('Helvetica','',13);
	$pdf->SetXY(23, 270);
	$pdf->MultiCell(100, 2, $properties['street_address'], 0, 'L');
}

$pdf_name=str_replace(" ","-",str_replace(" - ","-",$properties['street_address'])." ".ucwords(strtolower($properties['suburb']))." ".$properties['state'].".pdf");
$pdf->Output($pdf_name,'I'); 
?>
