<?php
$realty->element('add_tag_script');
?>

<p>Leave the tags you don't want displayed blank.</p>

<h4>State</h4>
	
<p><label>Title</label> <br><input value="<?php echo $settings['state']; ?>" name="realty_widget_<?php echo $caption; ?>[state]" type="text"></p>


<h4>Suburb</h4>

<p><label>Title</label> <br><input value="<?php echo $settings['suburb']; ?>" name="realty_widget_<?php echo $caption; ?>[suburb]" type="text"></p>


<h4>Property Type</h4>

<p><label>Title</label> <br><input value="<?php echo $settings['property_type']; ?>" name="realty_widget_<?php echo $caption; ?>[property_type]" type="text"></p>

<h4>Bedrooms</h4>

<p><label>Title</label> <br><input value="<?php echo $settings['bedrooms']; ?>" name="realty_widget_<?php echo $caption; ?>[bedrooms]" type="text"></p>

<h4>Price</h4>

<p><label>Title</label> <br><input value="<?php echo $settings['price']; ?>" name="realty_widget_<?php echo $caption; ?>[price]" type="text"></p>

<div id="price_options">
<label>Price Options</label><br />
<?php realty_add_tag_box('realty_widget_' . $caption. '[price_option][]',$settings['price_option'] ); ?>
</div>

<h4>Status</h4>

<p><label>Title</label> <br><input value="<?php echo $settings['status']; ?>" name="realty_widget_<?php echo $caption; ?>[status]" type="text"></p>

<p><label>Number of days that categories in new properties</label> <br><input value="<?php echo $settings['new_day']; ?>" name="realty_widget_<?php echo $caption; ?>[new_day]" type="text"></p>