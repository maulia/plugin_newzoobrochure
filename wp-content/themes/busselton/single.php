<?php get_header(); ?>
<div id="content">
<div id="blog-div" class="single">
<div class="entry">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<?php if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; } ?>
				<div class="post_title_date<?php if(!function_exists('userphoto_the_author_thumbnail')) { echo ' no_author_thumb'; } ?>">
					<h2 class="post_title"><?php the_title(); ?></h2>
					<p class="postdate"><?php the_author_posts_link(); ?> | <?php the_time('jS F, Y') ?><br /><?php the_tags( '<span class="tagged">Tagged: ', ', ', '</span> | '); ?><span class="comments_popup_link"><?php comments_popup_link('Make a Comment &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span></p>
				</div>
				<?php $category = get_the_category(); $category_link = get_category_link( $category[0]->cat_ID ); ?><p class="flag cat-<?php echo $category[0]->category_nicename; ?>"><span class="filed_cats"><a href="<?php echo $category_link; ?>" title="<?php echo $category[0]->cat_name; ?>"><?php echo $category[0]->cat_name; ?></a></span></p>

			<div class="post-content">
				<?php the_content(); ?>
				<?php the_tags( '<p class="tagged_single"><strong>Tagged:</strong> ', ', ', '</p>'); ?>
				<p><strong>Follow:</strong> <?php if (function_exists('display_twitter')) { ?><?php display_twitter(); ?> :: <?php } ?><span class="comments_rss_link"><?php post_comments_feed_link('Subscribe to this post\'s comments'); ?></span></p>

			<div class="navigation">
				<p class="alignleft"><?php previous_post_link('&#171; %link') ?></p>
				<p class="alignright"><?php next_post_link('%link &#187;') ?></p>
                <div class="clear"></div>
			</div>

			</div>
		</div>

	<?php comments_template('', true); ?>

	<!-- <?php $tags = wp_get_post_tags($post->ID);
if ($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

    $args=array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'showposts'=>5, // Number of related posts that will be shown.
        'caller_get_posts'=>1
    );
    $my_query = new wp_query($args);
    if( $my_query->have_posts() ) {
		echo '<div id="wp_related_posts">';
        echo '<h3>Related Posts</h3><ul>';
        while ($my_query->have_posts()) {
            $my_query->the_post();
        ?>
            <li><?php the_time('jS F, Y') ?> - <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> (<?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>)</li>
        <?php
        }
        echo '</ul>';
        echo '</div>';
    }
}
?>


	<?php endwhile; else: ?>

		<p>Sorry, no posts matched your criteria.</p>

<?php endif; ?> -->
	</div><!-- end .entry-->
    </div><!-- end #blog-div -->
	</div><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>