<?php if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ ?>
<?php if (!empty($realty->property['opentimes'])): ?>
<div id="open_times">
<?php echo $before_title; ?><span>Open</span> Times<?php echo $after_title; ?>

<div class="block_content">
	<ul><?php foreach($realty->property['opentimes'] as $opentime): ?>
		<li><span class="open_date"><strong>
		<?php echo date('l, j M Y', strtotime($opentime['date'])); ?></strong></span><br />
		<span class="open_time"><?php echo date('g:i a',strtotime($opentime['start_time'])); ?>- <?php echo date('g:i a',strtotime($opentime['end_time'])); ?></span></li>
	<?php endforeach; ?></ul>
</div>
<div class="clearer"></div>
</div><!-- end #open_times -->
<?php endif; ?>

<?php  if(!empty($realty->property['itemized_features'])):	?>
<div id="property_features">
<?php echo $before_title; ?><span>Property</span> Features<?php echo $after_title; ?>
<div class="block_content">
	<p>
	<span><?php echo implode('<small>&nbsp;&#8226;&nbsp;</small></span><span>', $realty->property['itemized_features']) . "<small>&nbsp;&#8226;&nbsp;</small></span>";	?>
	</p>
</div>
<div class="clearer"></div>
</div><!-- end #property_feaures -->
<?php else: ?>
<style type="text/css">.realty_widget_property_features { display:none; } </style>
<?php endif; ?>