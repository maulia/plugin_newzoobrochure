<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');
global $helper, $zoo_agents, $wpdb;
$office_id=$_REQUEST['main_office_id'];
$_REQUEST['name']=$_REQUEST['office_name'];
$_REQUEST['id']=$wpdb->get_var("select max(id) from office");
$_REQUEST['id']=$_REQUEST['id']+1;
$_REQUEST['main_office_id']=$office_id;
$_REQUEST['created_at']=$_REQUEST['updated_at']=date("Y-m-d h:i:s");

$zoo_agents->insert_form( "office", $_REQUEST );
// created office page
$id=$_REQUEST['id'];
$short_code="<!--realty_plugin: office_id=$id template:office-->";
$postID=$wpdb->get_var("select ID from $wpdb->posts where post_content='$short_code'");
$post_title=($_REQUEST['page_name']=='')?$_REQUEST['name']:$_REQUEST['page_name'];

// if page not exist
if(!$postID){
	$page_id = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => 'page', 'post_title'=>$post_title, 'post_content'=>$short_code));
	update_post_meta($page_id, '_wp_page_template', 'search_results_page.php');
	$_REQUEST['page_id']= $page_id;
	$wpdb->query("update office set page_id='$page_id', page_name='$post_title' where id=$id");
}
			
$other_offices=$wpdb->get_results("select id, name from office where id not in($office_id) order by name", ARRAY_A);
?>
<option value="">Please Select</option>
<?php foreach($other_offices as $office){ ?>
<option value="<?php echo $office['id']; ?>"><?php echo $office['name']; ?></option>
<?php } ?>