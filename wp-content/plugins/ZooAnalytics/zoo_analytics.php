<?php
/*
Plugin Name: Zoo Analytics New
Version: 1.3
Plugin URI: http://www.agentpoint.com.au/
Description: Tracks down how many times a property page has been visited  number of emails sent from that page, number of Brochure downloads and number of Social bookmarking/sharing from that page. A header filter is triggered everytime a property page is viewed and the information is logged into the database. From the administration panel, the site administrator is able to see reports stating each property viewed and how many hits during the past week, month and total hits that property has had. An input box is provided to enter e-mails to receive reports. A cron job can be set up to run a script that will produce reports and e-mail them to the subscribed administrators.
Author: Agentpoint
Author URI: http://www.agentpoint.com.au

Release History:
1.1 use $wpdb function to query
1.2 automcatically load Office ID from zoo realty / general settings, if there is no Office ID selected on this plugin page setting
*/

class zoo_analytics{
	function zoo_analytics(){
		global $table_prefix;		
		$this->plugin_table = $table_prefix . "property_analytics";
		$this->site_url = get_option('siteurl');
		if ( '/' != $this->site_url[strlen($this->site_url)-1] )$this->site_url .= '/';
		$this->plugin_folder = substr( dirname( __FILE__ ), strlen(ABSPATH . PLUGINDIR) + 1 ) . '/';
		$this->plugin_url = $this->site_url . PLUGINDIR . '/' . $this->plugin_folder;
		$this->form_action = "?page=" . $_GET['page'];
		$this->blogname = str_replace("&amp;","&",get_option('blogname'));
		add_action('admin_menu', array($this, 'admin_menu'));
		add_action('wp_head', array($this, 'head'),1);
		register_activation_hook(__FILE__, array($this, 'install'));

		$wpmu=$this->check_this_is_multsite();
		if($wpmu){
			global $wpdb;
			$blog_id=$wpdb->blogid;
			$this->log = $this->plugin_url.$blog_id."_log.html";
			$this->log_directory = dirname( __FILE__ )."/".$blog_id."_log.html";
		}
		else{
			$this->log = $this->plugin_url."/log.html";
			$this->log_directory = dirname( __FILE__ )."/log.html";		
		}
	}
	
	function check_this_is_multsite() {
		global $wpmu_version;
		if (function_exists('is_multisite')){
			if (is_multisite()) {
				return true;
			}
			if (!empty($wpmu_version)){
				return true;
			}
		}
		return false;
	}
	
	function install(){
		global $wpdb;			
		$sql = "CREATE TABLE IF NOT EXISTS `$this->plugin_table` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`property_id` INT( 11 ) NOT NULL,
		`visited_at` DATE NOT NULL ,
		`category` varchar(20) NOT NULL  COMMENT 'visit, enquiry, brochure, email_friend, email_self, share_on_twitter, share_on_facebook, favorite  ',
		 KEY `property_id` (`property_id`)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
		$wpdb->query($sql);
	}
	
	function admin_menu(){
		add_management_page('Property Performance', 'Property Performance', 8, __FILE__, array($this,"setting")); 		
	}
	
	function setting(){			
		$return  = $this->direct_task($_REQUEST);
		$office_id = intval($_REQUEST['id']);
		$property_id = intval($_REQUEST['property_id']);
		$offices=$this->get_offices($office_id);
		switch( $_REQUEST["action"] ):
			case 'view_weekly' :
				$properties=$this->get_report($office_id,$_GET['pg'],7);
				$title=$offices[0]['name']."'s Weekly Report";
				include(dirname(__FILE__).'/admin/report.php');
			break;				
			case 'view_report' :
				$properties=$this->get_dynamic_report($_GET['pg'],$_GET['date_from'],$_GET['date_to']);
				$title="Report";
				if(!empty($_GET['date_from']))$title.=" from ".date("d-m-Y",strtotime($_GET['date_from']));
				if(!empty($_GET['date_to']))$title.=" until ".date("d-m-Y",strtotime($_GET['date_to']));
				include(dirname(__FILE__).'/admin/report.php');
			break;
			case 'view_summary' :
				$properties=$this->get_report($office_id,$_GET['pg']);
				$title=$offices[0]['name']."'s Summary Report";
				include(dirname(__FILE__).'/admin/report.php');
			break;
			case 'view_detail' :
				$properties=$this->get_detail($property_id);				
				$total=$this->get_total($property_id);
				$property=$this->get_property($property_id);
				$title="$property_id - ".$property['address'].", ".$property['suburb']." Weekly Report";
				include(dirname(__FILE__).'/admin/detail.php');
			break;
			default:				
				$cron="50  23  *  *  7  curl ".get_option('siteurl').'/wp-content/plugins/ZooAnalytics/cron_job.php';	
				require( 'admin/setting.php');		
			break;
		endswitch;						
	}
	
	function direct_task( $request ){
		switch($_REQUEST['task']):						
			case 'send_weekly_reports' :
				$return .= $this->send_weekly_reports();
				break;
			case 'send_summary_reports' :
				$return .= $this->send_summary_reports();
			break;
			case 'send_report_email' :
				$return .= $this->send_report_email($_REQUEST);
			break;
			case 'send_report_agent' :
				$return .= $this->send_report_email($_REQUEST);
			break;
			default :	break;
		endswitch;
		return (!empty($return))? $return:false;
	}
		
	function send_weekly_reports(){
		$date=date("d/m/Y");
		global $helper, $wpdb, $realty;
		$office_ids=get_option('office_ids_report');
		if(!$office_ids)$office_ids=$realty->settings['general_settings']['office_id'];
		if(!is_array($office_ids))$office_ids=array();		
		if(empty($office_ids))return;
		$return="$date<hr/>";
		foreach($office_ids as $office_id){
			if(empty($office_id))continue;
			$office_id=intval($office_id);
			$users=$wpdb->get_results("select id, email,CONCAT(firstname, ' ', lastname) as name from users where office_id in($office_id)", ARRAY_A);
			$offices=$wpdb->get_results("select name, url from office where id in($office_id)", ARRAY_A);
			$office_name=$offices[0]['name'];
			$office_website=$offices[0]['url'];	
			foreach($users as $user){
				$user_id=intval($user['id']);
				$email=$user['email'];
			
				$name=$user['name'];
				if(!empty($user['email'])){
					$properties=$wpdb->get_results("select id from properties where (user_id in ($user_id) or secondary_user in ($user_id)) and id in(select distinct property_id from `$this->plugin_table` where DATEDIFF(CURDATE(),`visited_at`)<= 7) and status in(1,4)", ARRAY_A);
					if(!empty($properties)){
						ob_start();
						require( 'includes/weekly_report.php');
						$message = ob_get_contents();
						ob_end_clean();
						$helper->email($email,"$office_name Property Report for $name" ,$message,'','',false);
						$return .= "Weekly report to $name with email '$email' has been sent.<br />";
					}
				}
			}
		}
		$this->logfile($return);
		return $return;	
	}	
		
	function send_summary_reports(){
		$date=date("d/m/Y");
		global $helper, $wpdb, $realty;
		$emails_to_summary_report=get_option('emails_to_summary_report');
		if(!is_array($emails_to_summary_report))$emails_to_summary_report=array();
		if(empty($emails_to_summary_report))return 'Please fill the email address field.';
		
		$office_ids=get_option('office_ids_report');
		if(!$office_ids)$office_ids=$realty->settings['general_settings']['office_id'];
		if(!is_array($office_ids))$office_ids=array();		
		if(empty($office_ids))return;
		
		$return="$date<hr/>";
		
		foreach($office_ids as $office_id){
			if(empty($office_id))continue;	
			$office_id=intval($office_id);
			$properties=$this->get_report($office_id,'','',false);
			if(!empty($properties)){
				$offices=$wpdb->get_results("select name, url from office where id in($office_id)", ARRAY_A);
				$office_name=$offices[0]['name'];
				$office_website=$offices[0]['url'];
				ob_start();
				require( 'includes/summary_report.php');
				$message = ob_get_contents();
				ob_end_clean();
					
				foreach($emails_to_summary_report as $email){
					if(empty($email))continue;
					$email=trim($email);					
					if($this->email2($email,"$office_name Summary Property Report" ,$message,''))$return .= "$office_name summary report to '$email' has been sent.<br />";
					else $return .="Email can not be sent to '$email'.<br />";						
				}

				//$this->email2('darkxiela@gmail.com',"$office_name Summary Property Report" ,$message,'');
			}			
		}	
		$this->logfile($return);
		return $return;	
	}
	
	function send_report_email($request){
		global $helper, $wpdb, $realty;
		$email=$_REQUEST['email'];
		if(empty($email))return 'Please fill the email address field.';
		$date=date("d/m/Y");		
		$office_ids=get_option('office_ids_report');
		if(!$office_ids)$office_ids=$realty->settings['general_settings']['office_id'];
		if(!is_array($office_ids))$office_ids=array();		
		if(empty($office_ids))return;
		
		$return="$date<hr/>";
		
		foreach($office_ids as $office_id){
			if(empty($office_id))continue;		
			$office_id=intval($office_id);
			$properties=$this->get_dynamic_report('',$_REQUEST['date_from'],$_REQUEST['date_to']);
			if(!empty($properties)){
				$offices=$wpdb->get_results("select name, url from office where id in($office_id)", ARRAY_A);
				$office_name=$offices[0]['name'];
				$office_website=$offices[0]['url'];
				ob_start();
				require( 'includes/summary_report.php');
				$message = ob_get_contents();
				ob_end_clean();
					
				$email=trim($email);					
				if($this->email2($email,"$office_name Summary Property Report" ,$message,''))$return .= "$office_name summary report to '$email' has been sent.<br />";
				else $return .="Email can not be sent to '$email'.<br />";		
			}			
		}	
		$this->logfile($return);
		return $return;	
	}
	
	function send_report_agent($request){
		$date=date("d/m/Y");
		global $helper, $wpdb, $realty;
		$office_ids=get_option('office_ids_report');
		if(!$office_ids)$office_ids=$realty->settings['general_settings']['office_id'];
		if(!is_array($office_ids))$office_ids=array();				
		if(empty($office_ids))return;
		$return="$date<hr/>";
		foreach($office_ids as $office_id){
			if(empty($office_id))continue;
			$office_id=intval($office_id);
			$users=$wpdb->get_results("select id, email,CONCAT(firstname, ' ', lastname) as name from users where office_id in($office_id)", ARRAY_A);
			$offices=$wpdb->get_results("select name, url from office where id in($office_id)", ARRAY_A);
			$office_name=$offices[0]['name'];
			$office_website=$offices[0]['url'];	
			foreach($users as $user){
				$user_id=intval($user['id']);
				$email=$user['email'];
				$name=$user['name'];
				if(!empty($user['email'])){					
					if(!empty($_REQUEST['date_from']))$condition="where visited_at >= '".esc_sql($_REQUEST['date_from'])."'";
					if(!empty($_REQUEST['date_to']))$condition=($condition=='')?"where visited_at <= '".esc_sql($_REQUEST['date_to'])."'":"$condition and visited_at <= '".mysql_real_escape_string($_REQUEST['date_to'])."'";
					$properties=$wpdb->get_results("select id from properties where (user_id in ($user_id) or secondary_user in ($user_id)) and id in(select distinct property_id from `$this->plugin_table` $condition) and status in(1,4)", ARRAY_A);
					if(!empty($properties)){
						ob_start();
						require( 'includes/weekly_report.php');
						$message = ob_get_contents();
						ob_end_clean();
						$helper->email($email,"$office_name Property Report for $name" ,$message,'','',false);
						$return .= "Weekly report to $name with email '$email' has been sent.<br />";
					}
				}
			}
		}
		$this->logfile($return);
		return $return;	
	}
	
	function logfile($logfile){
		if( !file_exists( $this->log_directory ) ){
			$ourFileHandle = fopen($this->log_directory, 'w') or die("can't open file");
			fclose($ourFileHandle);						
		}	
		else{
			$file_content  = file_get_contents( $this->log_directory );
			$logfile = "$file_content<hr/>$logfile" ;			
			unlink ($this->log_directory);
			$ourFileHandle = fopen($this->log_directory, 'w') or die("can't open file");
			fclose($ourFileHandle);			
		}	
		file_put_contents( $this->log_directory, $logfile ); 
	}
	
	function email2($to, $subject, $message, $from=''){
		global $realty;
		if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
		if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\r\n";		
		return @wp_mail($to, $subject, $message, $from . "Content-Type: text/html\n");		
	}
	
	function get_offices($office_id=''){
		global $realty, $helper, $wpdb;
		if(empty($office_id))$office_id=implode(",",$realty->settings['general_settings']['office_id']);
		if(empty($office_id))return;
		$office_id=intval($office_id);
		$offices=$wpdb->get_results("select id, name, unit_number, street_number, street , suburb, state from office where id in ($office_id)", ARRAY_A);
		foreach($offices as $key=>$office){
			if(!empty($offices[$key]['unit_number']))$offices[$key]['address'] = $offices[$key]['unit_number'] .'/';
			if(!empty($offices[$key]['street_number']))$offices[$key]['address'] .=  $offices[$key]['street_number'] . ' ' ;
			if(!empty($offices[$key]['street']))$offices[$key]['address'] .=  $offices[$key]['street']  ;
			if(!empty($offices[$key]['suburb']))$offices[$key]['address'] .=  ($offices[$key]['address']=='')?$offices[$key]['suburb']:', '.$offices[$key]['suburb']  ;
			if(!empty($offices[$key]['state']))$offices[$key]['address'] .= ($offices[$key]['address']=='')?$offices[$key]['state']:', '.$offices[$key]['state']  ;
		}
		return $offices;
	}
	
	function get_dynamic_report($page='1',$date_from='',$date_to='',$admin=true){
		global $wpdb, $realty;
		$office_ids=get_option('office_ids_report');
		if(!$office_ids)$office_ids=$realty->settings['general_settings']['office_id'];
		if(empty($office_ids))return;
		$office_ids=implode($office_ids);
		if(!empty($date_from))$condition.="visited_at >= '$date_from' and ";
		if(!empty($date_to))$condition.="visited_at <= '$date_to' and ";
		$sql="select distinct property_id, unit_number, street_number, street, suburb from `$this->plugin_table` a, properties b where $condition property_id =b.id and office_id in($office_ids) and status in (1,4) order by suburb, street";
		if($admin){
			$return=$this->paginate('50', $page, 0, $sql); 
			$sql=$return['sql'];
		}
		$properties =$wpdb->get_results($sql, ARRAY_A);
		if(isset($_GET['debug']))echo $sql;
		foreach($properties as $key=>$property){
			$property_id=intval($property['property_id']);			
			$properties[$key]['address']=$this->property_address($property);
			$properties[$key]['suburb']=ucwords(strtolower($property['suburb']));
			$properties[$key]['url']=$this->site_url."$property_id/";
			$properties[$key]['visit']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('visit')");
			$properties[$key]['enquiry']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('enquiry')");
			$properties[$key]['brochure']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('brochure')");
			$properties[$key]['email_friend']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('email_friend')");
			$properties[$key]['email_self']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('email_self')");
			$properties[$key]['share_on_twitter']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('share_on_twitter')");
			$properties[$key]['share_on_facebook']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('share_on_facebook')");
			$properties[$key]['favorite']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('favorite')");
		}		
		if($admin)$properties['pagination_label'] = $return['pagination_label'];	
		return $properties;		
	}
	
	function get_report($office_id='', $page='1',$days='',$admin=true){
		global $wpdb, $realty;
		if(empty($office_id))return;
		if(!empty($days))$condition="DATEDIFF(CURDATE(),`visited_at`)<= $days and ";
		$sql="select distinct property_id, unit_number, street_number, street, suburb from `$this->plugin_table` a, properties b where $condition property_id =b.id and office_id in(".intval($office_id).") and status in (1,4) order by suburb, street";
		if($admin){
			$return=$this->paginate('50', $page, 0, $sql); 
			$sql=$return['sql'];
		}
		$properties =$wpdb->get_results($sql, ARRAY_A);
		foreach($properties as $key=>$property){
			$property_id=intval($property['property_id']);			
			$properties[$key]['address']=$this->property_address($property);
			$properties[$key]['suburb']=ucwords(strtolower($property['suburb']));
			$properties[$key]['url']=$this->site_url."$property_id/";
			$properties[$key]['visit']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('visit')");
			$properties[$key]['enquiry']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('enquiry')");
			$properties[$key]['brochure']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('brochure')");
			$properties[$key]['email_friend']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('email_friend')");
			$properties[$key]['email_self']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('email_self')");
			$properties[$key]['share_on_twitter']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('share_on_twitter')");
			$properties[$key]['share_on_facebook']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('share_on_facebook')");
			$properties[$key]['favorite']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('favorite')");
		}
		if($admin)$properties['pagination_label'] = $return['pagination_label'];	
		return $properties;		
	}
	
	function get_detail($property_id=''){
		global $wpdb, $realty;
		if(empty($property_id))return;
		$property_id=intval($property_id);
		$sql="select distinct visited_at from `$this->plugin_table` where property_id = $property_id and DATEDIFF(CURDATE(),`visited_at`)<= 7 order by visited_at";
		$properties =$wpdb->get_results($sql, ARRAY_A);
		if(empty($properties))return $properties;	
		foreach($properties as $key=>$property){
			$date=$property['visited_at'];
			$condition="visited_at='$date' and ";
			$properties[$key]['visit']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('visit')");
			$properties[$key]['enquiry']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('enquiry')");
			$properties[$key]['brochure']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('brochure')");
			$properties[$key]['email_friend']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('email_friend')");
			$properties[$key]['email_self']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('email_self')");
			$properties[$key]['share_on_twitter']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('share_on_twitter')");
			$properties[$key]['share_on_facebook']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('share_on_facebook')");
			$properties[$key]['favorite']=$wpdb->get_var("select count(id) from `$this->plugin_table` where $condition property_id in ($property_id) and category in ('favorite')");
		}		
		return $properties;		
	}
	
	function get_total($property_id=''){
		global $wpdb, $realty;
		if(empty($property_id))return;
		$sql="select count(id) as number, category from `$this->plugin_table` where property_id = ".intval($property_id)." and DATEDIFF(CURDATE(),`visited_at`)<= 7 group by category";
		$total =$wpdb->get_results($sql, ARRAY_A);	
		$return=array();
		foreach($total as $item){
			$category=$item['category'];
			$return[$category]=$item['number'];
		}
		return $return;		
	}
	
	function get_property($property_id=''){
		global $wpdb;
		if(empty($property_id))return;
		$property=$wpdb->get_results("select unit_number, street_number, street, suburb from properties where id=".intval($property_id), ARRAY_A);
		$property=$property[0];
		$return['address']=$this->property_address($property);
		$return['suburb']=ucwords(strtolower($property['suburb']));
		return $return;	
	}
	
	function property_address($properties){
		if(!empty($properties['unit_number']))$address = $properties['unit_number'] .'/';
		if(!empty($properties['street_number']))$address .=  $properties['street_number'] . ' ' ;
		if(!empty($properties['street']))$address .=  $properties['street']  ;
		return $address;
	}
	
	function paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		if($page < 1) 	$page = 1;
		if(!empty($sql) && $results_count==0) 
			if(($query =mysql_query($sql)) != false)
				$results_count = mysql_num_rows($query);
		if ($limit < $results_count ):				
			$limit_start = $page * $limit - ($limit);
			if($limit_start > $results_count)
				$limit_start = $results_count - $limit; 
			$sql_limit = " LIMIT $limit_start, $limit";			
		endif;
		$return['sql'] = $sql . $sql_limit;

		$max_page_links = 10; 
		
		
		$page_link = $page - floor($max_page_links / 2);
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
		
		$current_query = "?" . preg_replace('/&(pg=)([-+]?\\d+)/is', '', $_SERVER['QUERY_STRING']) . "&amp;pg=";
		
		$prev_page = $page - 1;
		$next_page = $page + 1;
		ob_start();
		require('admin/pagination_label.php');
		$return['pagination_label'] = ob_get_clean();
		return $return;
	}
	
	function head(){
		global $realty, $wpdb;	
		if(is_page('property')){
			$id=$realty->property['id'];
			$status=$realty->property['status'];
			?>
			<script type="text/javascript">
			function share_trackers(id,category) {
				jQuery.get( "<?php echo $this->plugin_url; ?>includes/share_trackers.php?property_id="+id+"&category="+category );
			}
			<?php if($status=='1' || $status=='4'){ ?>
			share_trackers('<?php echo $id;?>','visit');
			<?php } ?>
			</script>
			<?php			
			//if(!empty($id) && ($status=='1' || $status=='4'))$this->save_trackers($id,'visit');
		}
	}
		
	function save_trackers($property_id='',$category=''){
		global $wpdb, $realty;
		if(empty($property_id) || empty($category))return;
		$timezone_format = _x('Y-m-d', 'timezone date format');
		$timezone_format= date_i18n($timezone_format); 
		$wpdb->query("INSERT INTO $this->plugin_table VALUES ('', '".intval($property_id)."', '$timezone_format','".esc_sql($category)."')");
		if(class_exists('action_plan') && $_COOKIE['log_id']){
			global $action_plan;
			$logs=$action_plan->get_cookie();
			
			$current_date=date('d/m/Y');
			if($current_date != $_COOKIE['log_timestamp'])array_push($logs, "Date : $current_date");
			
			$link=$realty->siteUrl.$property_id.'/';
			if($category!='visit')$link.=" : $category";
			array_push($logs, $link);
			
			$action_plan->set_cookie($logs);	
		}
	}
	
	function get_visits_by_id($property_id){
		global $wpdb;
		if(empty($property_id))return;
		$sql = "SELECT COUNT(*) AS grand_total,  (SELECT COUNT(property_id) FROM $this->plugin_table WHERE DATEDIFF(CURDATE(),`visited_at`) <= 7 AND properties.id=$this->plugin_table.property_id  and $this->plugin_table.category in ('visit')) AS week_total,	(SELECT COUNT(property_id) FROM $this->plugin_table WHERE DATEDIFF(CURDATE(),`visited_at`) < 31 AND properties.id=$this->plugin_table.property_id  and $this->plugin_table.category in ('visit')) AS month_total FROM $this->plugin_table, properties WHERE	properties.id=$this->plugin_table.property_id and property_id=".intval($property_id)." and $this->plugin_table.category in ('visit')";
		$return= $wpdb->get_results($sql, ARRAY_A );			
		return $return;	
	}	
}

$zoo_analytics = new zoo_analytics();
?>