<?php
/*
Plugin Name: Location Info New
Plugin URI: http://www.zooproperty.com/online-store
Description: Location Info introduces location information onto individual property pages matching with suburbs. As an exmple if the property is in New York then the suburb profile for New York will appear.
Version: 1.6
Author: Agentpoint
Author URI: http://www.agentpoint.com.au
*/
if(!class_exists('form_handler') && is_dir(dirname(__FILE__) .'/core/'))require_once('core/loader.php');
		
class LocationInfo extends form_handler{
	var $plugin_name = 'Location Info';
	function LocationInfo(){
		//Configure the types of amenities here
		$this->amenities = array('cafe'=>'Cafes & Restaurants','landmark'=>'Landmarks','worship'=>'Places of Worship','school'=>'School & Univ','shopping'=>'Shops','sport'=>'Sporting Facilities');
		
		$this->form_action = "?page=" . $_GET['page'];
		
		$this->site_url = get_option('siteurl');
		if ( '/' != $this->site_url[strlen($this->site_url)-1] )$this->site_url .= '/';
		$this->plugin_folder = substr( dirname( __FILE__ ), strlen(ABSPATH . PLUGINDIR) + 1 ) . '/';
		$this->complete_plugin_folder = ABSPATH . PLUGINDIR . '/' . $this->plugin_folder;
		$this->plugin_url = $this->site_url . PLUGINDIR . '/' . $this->plugin_folder;
		$this->uploads_dir = ABSPATH . 'wp-content/uploads/LocationInfoPlugin/';

		$wpmu=$this->check_this_is_multsite();
		if($wpmu){
			global $table_prefix;
			$this->plugin_table=$table_prefix.'LocationInfo_suburbs';
			$this->amenities_table=$table_prefix.'LocationInfo_amenities';
			$this->photo_table=$table_prefix.'LocationInfo_photos';
		}
		else{
			$this->plugin_table='LocationInfo_suburbs';
			$this->amenities_table='LocationInfo_amenities';
			$this->photo_table='LocationInfo_photos';
		}
		
		add_action('admin_menu', array($this, 'admin_menu'));
		add_action('admin_head', array($this, 'admin_head'));
		add_action('the_content', array($this, 'content_filter'));
		register_activation_hook(__FILE__, array($this, 'install'));
	}//ends function
	
	function check_this_is_multsite() {
		global $wpmu_version;
		if (function_exists('is_multisite')){
			if (is_multisite()) {
				return true;
			}
			if (!empty($wpmu_version)){
				return true;
			}
		}
		return false;
	}
	
	function content_filter($content){
		$matches = array();
		if(preg_match('[location_info_plugin]', $content, $matches))return $this->location_info_page();
		if(preg_match('[location_info_individual_page_plugin]', $content, $matches))return $this->location_info_individual_page_plugin();
		return $content;
	}
	
	function location_info_individual_page_plugin(){
		wp_print_scripts('jquery');
		global $realty, $post, $wpdb;
		$placeholder_image= $realty->paths['theme'].'images/download.png';
		if(!empty($_REQUEST['suburb_id']))$extra_condition="where suburb_id=".intval($_REQUEST['suburb_id']);
		if(!empty($post->ID))$extra_condition="where page_id = '".intval($post->ID)."'";		
			
		$location = $wpdb->get_results("SELECT * FROM $this->plugin_table $extra_condition", ARRAY_A);
		if(!$location)$location = $wpdb->get_results("SELECT * FROM $this->plugin_table where suburb='".$post->post_title."'", ARRAY_A);

		if(!$location){ echo "<div class='clearer'></div>We are sorry, no suburb were found to match this criteria, please check back again soon or contact us with your requirements.";	return; }	
		
		$i=0;
		foreach($location as $key=>$row):				
			$suburb_id=$location[$key]['suburb_id'];
			if(empty($suburb_id))continue;
			$photos = $wpdb->get_results("SELECT * FROM $this->photo_table WHERE `suburb_id` =".intval($suburb_id)." ORDER BY `ord`", ARRAY_A);
			if(!empty($photos)){
				foreach ($photos as $key=>$photo):
					$this->fill_url($photos[$key]['path']);
					$photos[$key]['thumbnail'] = $this->get_thumbnail($photos[$key]['path']);
				endforeach;		
			}
			else{
				$photos[0]['thumbnail']=$photos['0']['path']=$placeholder_image;				
			}

			$location[$i]['photos']=$photos;		
			$location[$i]['amenities'] = $wpdb->get_results("SELECT * FROM $this->amenities_table WHERE `suburb_id`=" . intval($suburb_id) . " order by amenity", ARRAY_A);			
			$i++;
		endforeach;
		
		require_once(dirname(__FILE__).'/view/individual_page.php');
	}
	
	function location_info_page(){
		wp_print_scripts('jquery');
		global $realty, $wpdb, $helper;
		$request=$_REQUEST;
		$placeholder_image= $realty->paths['theme'].'images/download.png';
		if(!empty($_REQUEST['suburb_id']))$extra_condition="where suburb_id=".intval($_REQUEST['suburb_id']);
		global $wp_query;
		$page=($wp_query->query_vars['page']=='')?'1':$wp_query->query_vars['page'];
		if(!empty($wp_query->query_vars['page']))$limit_start = $page * 20 - (20) . ", ";			
		$limit = " LIMIT $limit_start 20";
			
		$location = $wpdb->get_results("SELECT * FROM $this->plugin_table $extra_condition order by suburb $limit", ARRAY_A);
		
		//if(empty($_GET))require_once(dirname(__FILE__).'/view/search.php');
	
		if(!$location){ echo "<div class='clearer'></div>We are sorry, no suburb were found to match this criteria, please check back again soon or contact us with your requirements.";	return; }	
		
		$i=0;
		foreach($location as $key=>$row):				
			$suburb_id=$location[$key]['suburb_id'];
			if(empty($suburb_id))continue;
			$photos = $wpdb->get_results("SELECT * FROM $this->photo_table WHERE `suburb_id` =".intval($suburb_id)." ORDER BY `ord`", ARRAY_A);
			if(!empty($photos)){
				foreach ($photos as $key=>$photo):
					$this->fill_url($photos[$key]['path']);
					$photos[$key]['thumbnail'] = $this->get_thumbnail($photos[$key]['path']);
				endforeach;		
			}
			else{
				$photos[0]['thumbnail']=$photos['0']['path']=$placeholder_image;				
			}
			$location[$i]['photos']=$photos;		
			if(!empty($_REQUEST['suburb_id'])){
				$amenities = array('school'=>'schools', 'shopping'=>'shoppings', 'landmark'=>'landmarks','link'=>'interesting links');
				$location['amenities'] = array();
				foreach($amenities as $number=>$amenity): //Get each type of amenity in a separate array
					$location[$amenity] = $wpdb->get_results("SELECT * FROM $this->amenities_table WHERE `suburb_id`=" . intval($suburb_id) . " AND `type`='$number' ORDER BY `type` DESC $limit", ARRAY_A);			
					$location[$i]['amenities'] = array_merge($location['amenities'], $location[$amenity]);
				endforeach;
			}
			$i++;
		endforeach;
		
		$results_count = $wpdb->get_var( "SELECT COUNT(*) AS results_count FROM $this->plugin_table $extra_condition");		
		$pagination = $this->paginate(20, $page, $results_count);
		$location['pagination_above']=$pagination;
		
		require_once(dirname(__FILE__).'/view/location_info_page.php');
	}
	
	function paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $realty;
		//Retrieve how many links to show in each page
		$max_page_links = 	$realty->settings['general_settings']['number_of_page_links_on_pagination'];
		$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
	
		//$current_query =  "/" . $_SERVER['QUERY_STRING']; p($_SERVER['REQUEST_URI']);
		
		$prev_page = $page - 1;
		$next_page = $page + 1;
		if($next_page == 1) $next_page = 2;
		ob_start();
			require('view/pagination.php');
		$return = ob_get_clean();
		return $return;
	}
	
	function replacePageLabel($replacement){
		 if(!empty($_SERVER['QUERY_STRING'])):
			$queryString ='?' . $_SERVER['QUERY_STRING'];
			$url = substr($_SERVER['REQUEST_URI'], 0, -strlen($queryString));
		else:
			$url = $_SERVER['REQUEST_URI'];
		endif;
		$newUrl = preg_replace('/\/([-+]?\\d+)\//is', "/$replacement/", $url, 1, $replacesMade);
		if($replacesMade < 1) //If no replacements were made, it was the first page, just add it.
			$newUrl .= "$replacement/";
		$newUrl .= $queryString; 
		return $newUrl;
	}
	
	function install(){
		global $wpdb;
		if (!is_dir($this->uploads_dir) && !mkdir( $this->uploads_dir, 0755 ))echo "Could not create directory $this->uploads_dir. Please create the directory and set it to the correct permissions.";
		$wpdb->query("
		CREATE TABLE IF NOT EXISTS `$this->plugin_table` (
		  `suburb_id` mediumint(9) NOT NULL auto_increment,
		  `suburb` varchar(100) NOT NULL,
		  `population` varchar(100) NOT NULL,
		  `postcode` varchar(50) NOT NULL,
		  `council` varchar(100) NOT NULL,
		  `link` VARCHAR( 100 ) NOT NULL,
		  `description` longtext NOT NULL,
		  `page_id` int(11),
		  PRIMARY KEY  (`suburb_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS `$this->amenities_table` (
		  `amenity_id` mediumint(9) NOT NULL auto_increment,
		  `suburb_id` mediumint(9) NOT NULL,
		  `type` varchar(50) NOT NULL COMMENT 'Valid values are school, shopping, landmark or link',
		  `amenity` varchar(200) NOT NULL,
		  `link` varchar(200) NOT NULL,
		  PRIMARY KEY  (`amenity_id`),
		 KEY `suburb_id` (`suburb_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		");
		$wpdb->query("
		CREATE TABLE IF NOT EXISTS `$this->photo_table` (
		  `photo_id` mediumint(9) NOT NULL auto_increment,
		  `suburb_id` mediumint(9) NOT NULL,
		  `photo_title` varchar(100) NOT NULL,
		  `description` varchar(200) NOT NULL,
		  `path` varchar(150) NOT NULL,
		  `ord` tinyint(4) NOT NULL,
		  PRIMARY KEY  (`photo_id`),
		 KEY `suburb_id` (`suburb_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		");	
	}
	
	function admin_menu(){
		add_management_page($this->plugin_name, $this->plugin_name, 8, __FILE__, array($this, 'management_page'));
	}
	
	function admin_head(){ 
		if($_GET['action'] =='edit' && strpos($_GET['page'],'LocationInfo') === false){ 			
			wp_print_scripts('jquery-ui-core');
			wp_print_scripts('jquery-ui-sortable');		
		}
	}
		
	function management_page(){
		global $wpdb;
		$return = $this->direct_task($_REQUEST);
		if($return != false){ ?>
			<div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div>
		<?php 
		}
			
		if($_REQUEST['action'] =='edit' && !empty($_REQUEST['suburb_id'])):// If the form is being updated
			$form_title = 'Edit';
			$task = 'update_form';
			$location=$this->get_suburb("`suburb_id`= " . intval($_REQUEST['suburb_id']));
			require_once( 'admin/management_page.php');
			require('admin/amenities_form.php');
			//require('admin/photos_form.php');		
		elseif($_REQUEST['action'] =='add_new'): // If a new location is being added
			$form_title = 'Add';
			$task = 'insert_form';
			require_once( 'admin/management_page.php');
		else:
			if($_REQUEST['action'] =='create_page'){
				$this->create_page();
				$return='Pages was succesfully created.';
			}			
			$current_locations = $wpdb->get_results("SELECT * FROM $this->plugin_table $extra_condition ORDER BY `suburb` ASC", ARRAY_A);					
			require('admin/list_locations.php');
		endif;
	
	}
	
	function create_page($suburb=''){
		global $wpdb, $helper;
		if(!empty($suburb)){
			$suburbs=$wpdb->get_results($wpdb->prepare("select distinct suburb from $this->plugin_table where suburb=%s order by suburb", $suburb), ARRAY_A);
		}else{
			$suburbs=$wpdb->get_results("select distinct suburb from $this->plugin_table order by suburb", ARRAY_A);
		}
		if(!empty($suburbs)){
			$post_parent=$wpdb->get_var("select ID from $wpdb->posts where post_content='[location_info_plugin]' and post_status='publish' and post_type='page' order by ID limit 1");
			if(!$post_parent){
				$this->wp_insert_post(array('post_title'=>'Suburb Profile', 'post_content'=>'[location_info_plugin]'), true); 
			}
			foreach($suburbs as $suburb){
				$page_name=preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "_",strtolower($suburb['suburb'])));
				if(!empty($page_name)){ 
					$page_id=$wpdb->get_var($wpdb->prepare("select page_id from $this->plugin_table where suburb=%s limit 1", $suburb['suburb']));

					if(empty($page_id)){
						$page_id=$this->wp_insert_post(array('post_title'=>$page_name, 'post_content'=>'[location_info_individual_page_plugin]','post_parent'=>$post_parent,'post_template'=>'template_suburb_snapshots.php'), true); 
						$wpdb->query("update $this->plugin_table set page_id='".intval($page_id)."' where suburb='".$suburb['suburb']."'");		
					}					
				}				
			}
		}
	}
	
	function delete_page($suburb_id){		
		if(empty($suburb_id))return;		
		global $wpdb;
		$extra_condition.=($extra_condition=='')?" where suburb_id='".intval($suburb_id)."' ":" and suburb_id='".intval($suburb_id)."' ";
		
		$page_id=$wpdb->get_var(  "select page_id from $this->plugin_table $extra_condition order by suburb limit 1");
		if(empty($page_id))return;
		
		$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->posts WHERE ID = %d",$page_id));
	}
		
	function wp_insert_post($post, $unique=false){
		global $helper;
		if(empty($post['post_type'])) $post['post_type'] = 'page';
		$postID = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => $post['post_type'], 'post_title'=>$helper->humanize($post['post_title']), 'post_content'=>$post['post_content'], 'post_parent'=>$post['post_parent']));

		update_post_meta($postID, '_permanent_name',  $helper->underscore($post['post_title']));
		if(!empty($post['post_template']))update_post_meta($postID, '_wp_page_template', $post['post_template']);
		if(is_array($post['custom'])){
			foreach ($post['custom'] as $meta_key=>$meta_value)update_post_meta($postID, $meta_key, $meta_value);
		}
		return $postID;		
	}
	
	function upload_photos(){
		global $wpdb;
		$suburb_id = $_GET['suburb_id'];
		$input_photos = $_FILES['location_photos']; //var_dump($input_photos);
		if(empty($input_photos))return;		
		$upfiles = $this->build_input_array($input_photos,'size');	
		foreach($upfiles as $upfile):
			if($upfile['size'] <= 0) continue;
			$file_uploaded = $this->upload_file($upfile, $this->uploads_dir, 200, 400); //with thumbnail
			$photo_paths[]= $file_uploaded['path'];
			$return .= $file_uploaded['return'];			
		endforeach;
		
		if(!empty($photo_paths)){
			foreach($photo_paths as $photo_path){
				$wpdb->insert($this->photo_table, array('suburb_id' => $suburb_id, 'path' => $photo_path, 'photo_title' => $this->name_file($photo_path)));
			}
		}		
		return $return . "Photos Updated.";
	}
	
	function direct_task($post_vars){ 
		global $wpdb;
		$return .= $this->save_amenities($post_vars);
		switch($post_vars['task']):		
			case 'insert_form':
				$wpdb->insert($this->plugin_table,
					array(
						'suburb' => $post_vars['suburb'],
						'population' => $post_vars['population'],
						'postcode' => $post_vars['postcode'],
						'council' => $post_vars['council'],
						'link' => $post_vars['link'],
						'description' => stripslashes($post_vars['description']),
						'page_id' => $post_vars['page_id']
						),
					array( 
						'%s', 
						'%s', 
						'%d', 
						'%s', 
						'%s', 
						'%s', 
						'%d' 
					)
				);
				$this->create_page($post_vars['suburb']);
				$return .= "Location successfully added.";
			break;
			case 'update_form':
				$wpdb->update($this->plugin_table,
					array(
						'suburb' => $post_vars['suburb'],
						'population' => $post_vars['population'],
						'postcode' => $post_vars['postcode'],
						'council' => $post_vars['council'],
						'link' => $post_vars['link'],
						'description' => stripslashes($post_vars['description']),
						'page_id' => $post_vars['page_id']
						),
					array( 'suburb_id' => $post_vars['suburb_id']), 
					array( 
						'%s', 
						'%s', 
						'%d', 
						'%s', 
						'%s', 
						'%s', 
						'%d' 
					), 
					array( '%d' ) 
				);
				if($post_vars['delete_photo'])$wpdb->query("delete from $this->photo_table where suburb_id=".$post_vars['suburb_id']);
				if($post_vars['photos']){
					$i=0;
					foreach($post_vars['photos'] as $photo){
						$wpdb->insert($this->photo_table, array('suburb_id' => $post_vars['suburb_id'], 'path' => $photo['path'], 'ord'=>$i));
						$i++;
					}
				}
				$return .= "Location updated successfully.";				
			break;			
			case 'edit_photos':
				$order = 0;	
				if(!empty($post_vars['location_photos']))			
					foreach($post_vars['location_photos'] as $location_photo)://Update image description and order
						$photo_title = $post_vars['location_photo_names'][$order];
						$order++; 
						$wpdb->update($this->photo_table, 
							array(
								'photo_title' => $photo_title,	
								'ord' => $order	
							), 	
							array( 'photo_id' => $location_photo ),
							array( 
								'%s', 
								'%d', 
							), 
							array( '%d' ) 
						);
					endforeach;
				$return .= $this->upload_photos(); //Upload new images
			break;
			case 'delete_form':				
				$this->delete_location($post_vars['suburb_id']);
				$this->delete_page($post_vars['suburb_id']);
				$wpdb->query( $wpdb->prepare( "DELETE FROM $this->plugin_table WHERE suburb_id = %d",	$post_vars['suburb_id']));
				$return .= "Location deleted successfully.";
			break;
			case 'delete_photo':
				$this->delete_photo($post_vars['photo_id']);
				$wpdb->query( $wpdb->prepare( "DELETE FROM $this->photo_table WHERE photo_id = %d",	$post_vars['photo_id']));
				$return .= "Photo deleted successfully.";
			break;
			case 'delete_amenity':
				if(!empty($post_vars['hidden_amenity_id'])){
					$wpdb->query( $wpdb->prepare( "DELETE FROM $this->amenities_table WHERE amenity_id = %d",	$post_vars['hidden_amenity_id']));
					$return .= "Point of interest deleted successfully.";
				}
			break;
			case 'create_page':
				$this->create_page();
				$return.='Pages was succesfully created.';
			break;
			default:
				
			break;
		endswitch;
		return ($return!='')? $return:false;
	}//ends function
	
	function delete_photo($photo, $photo_type = 'path'){	
		global $wpdb;
		$path = $wpdb->get_var($wpdb->prepare("SELECT `$photo_type` FROM $this->photo_table WHERE `photo_id`=%d",$photo));
		return $this->_unlink($path, true);
	}
	
	function _unlink($path, $thumb = false){		
		if(empty($path))return true;
		$path = $this->uploads_dir . $path;	
		if(!file_exists($path))	return true;
			
		if(!$thumb)return unlink($path);
			
		$paths=pathinfo($path); // If it got all the way down here, the photo has a thumbnail
		if(!isset($paths['filename']))$paths['filename']=str_replace(".".$paths['extension'],"",$paths['basename']); // filename only since php 5.2
		$paths['filename']=strtolower (preg_replace("/[^a-z0-9-]/", "-", $paths['filename']));
		$thumb = $paths['filename'] . '_thumb.' . $paths['extension'];
		$medium = $paths['filename'] . '_medium.' . $paths['extension'];
		return unlink($path) && unlink($this->uploads_dir . $thumb) && unlink($this->uploads_dir . $medium); // Delete the file and run the function again, this time deleting its respective thumbnail

	}
	
	function delete_location($suburb_id){
		global $wpdb;
		$paths = $wpdb->get_col( $wpdb->prepare( "SELECT `path` FROM $this->photo_table WHERE `suburb_id` =%d", $suburb_id)); 
		if($paths){
			foreach ($paths as $path):
				$this->_unlink($path, true);
			endforeach;
		}
		$wpdb->query( $wpdb->prepare( "DELETE FROM $this->photo_table WHERE suburb_id = %d",$suburb_id));
		$wpdb->query( $wpdb->prepare( "DELETE FROM $this->amenities_table WHERE suburb_id = %d",$suburb_id));
		return;
	}
	
	function get_photos($suburb_id){
		global $wpdb;
		$photos = $wpdb->get_results("SELECT * FROM $this->photo_table WHERE `suburb_id` =".intval($suburb_id)." ORDER BY `ord`", ARRAY_A);
		foreach ($photos as $key=>$photo):
			$this->fill_url($photos[$key]['path']);
			$photos[$key]['thumbnail'] = $this->get_thumbnail($photos[$key]['path']);
		endforeach;		
		return $photos;	
	}//ends function
	
	function get_suburb($condition){	
		global $wpdb;
		$return = $wpdb->get_row("SELECT * FROM $this->plugin_table WHERE $condition LIMIT 1", ARRAY_A);
		if(!$return)return false;
		$return['photos'] =  $this->get_photos($return['suburb_id']);
		$return['amenities'] = array();
		foreach($this->amenities as $key=>$amenity): //Get each type of amenity in a separate array
			$return[$amenity] = $wpdb->get_results("SELECT * FROM $this->amenities_table WHERE `suburb_id`=" . intval($return['suburb_id']) . " AND `type`='".esc_sql($key)."' ORDER BY `type` DESC $limit", ARRAY_A);			
			$return['amenities'] = array_merge($return['amenities'], $return[$amenity]);
		endforeach;
			
		return $return;
	}
	
	function fill_url(&$url){
		if(!empty($url) && strpos($url,'http')===false)$url = $this->site_url . "wp-content/uploads/LocationInfoPlugin/$url";
		else $url=$url;
	}
	
	function save_amenities($post_vars){
		global $wpdb;
		$base_key = 'type';//Indicates that this key must be present in every property input. It will serve as a base to count the values in each property set of data
			
		if(!isset($post_vars['save_amenities']) || empty($post_vars[$base_key]))
			return;
			
		$sanitized_values = $this->build_input_array($post_vars,$base_key);
		$properties_updated = 0;
		foreach($sanitized_values as $values):
			$values['suburb_id'] = $post_vars['suburb_id'];
			$this->format_url($values['link']);
			$wpdb->update($this->amenities_table,
				array( 
					'suburb_id' => $values['suburb_id'], 
					'type' => $values['type'], 
					'amenity' => $values['amenity'], 
					'link' => $values['link'] 
				),
				array( 'amenity_id' => $values['amenity_id']),
				array( 
					'%d', 
					'%s', 
					'%s', 
					'%s', 
				), 
				array( '%d' ) 
			);
			++$properties_updated;
			if(empty($values['amenity_id']) && !empty($values[$base_key])):
				$wpdb->insert($this->amenities_table,$values);
					$return .="Point of Interest added successfully.<br />";
			endif;
		endforeach;
		if($properties_updated > 0)
			$return .= $properties_updated - 1 ." points of interest were successfully updated.";
		return $return;
	
	}	

}//ends class
	
$LocationInfo = new LocationInfo;
?>