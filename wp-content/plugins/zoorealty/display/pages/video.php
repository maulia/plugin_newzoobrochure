<?php
$condition=$_GET;
global $helper;
$url=get_permalink($post->ID);
if(is_null($listings))$listings = $this->properties_video($condition);
$queryString = "?" .  preg_replace('/(&?status=)([\w]+&?)/is', '', urldecode($_SERVER['QUERY_STRING']));
if($listings['results_count'] < 1){ ?>
<p class="no_properties_error">We are sorry, no video were found to match this criteria. Click <a href="<?php echo $url; ?>">here</a> to see all video listings.</p>
<?php 
} 
else{ ?>	 
<script src="<?php echo $this->pluginUrl; ?>display/pages/js/AC_RunActiveContent.js" type="text/javascript"></script> 
<div id="search_results_container">
	<div id="search_results">
		<div id="sorter_pagination">
			<p class="number_properties">
				There <?php echo ($listings['results_count'] == 1) ? 'is': 'are'; ?> <strong><?php echo $listings['results_count']-1; ?></strong>
				<?php if(!empty($_GET['status']))echo ($_GET['status'] == 2) ? 'sold ': 'available ';
				echo ($listings['results_count'] == 1) ? 'property video.': 'properties video.'; ?>		
			</p>
			<div class="sorter_search_quick">
				<select id="status" onchange="window.location=this.value">
					<option value="">All Properties</option>
					<option value="<?php echo $url . "?status=1,4";?>"<?php if($_GET['status']=='1,4') echo ' selected="selected"';?>>Available</option>
					<option value="<?php echo $url . "?status=2";?>"<?php if($_GET['status']=='2') echo ' selected="selected"';?>>Sold</option>
				</select>
				<div class="clear"></div>
			</div>	
			
			<?php echo $listings['pagination_above']; ?>

		</div> <!-- #sorter_pagination -->	
		<?php $this->element('video_listings', $listings);

		if(!empty($listings['pagination_below'])): ?>
		<div id='bottom_pagination'><?php echo $listings['pagination_below']; ?></div>
		<?php endif;?>
	</div>
</div>
<?php } ?>