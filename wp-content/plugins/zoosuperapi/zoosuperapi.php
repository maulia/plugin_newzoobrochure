<?php
/*
Plugin Name: Zoo Super API
Plugin URI: http://www.agentpoint.com.au/
Description: A updated version of the Zoo Property API
Version: 0.7.13
Author: Agentpoint
*/

define('ZOOSUPERAPI_VERSION', '0.7.13');

register_activation_hook(__FILE__, 'zoosuperapi_activate');
register_uninstall_hook(__FILE__, 'zoosuperapi_deactivate');

add_action("init", 'zoosuperapi_api');
add_action('admin_init', 'zoosuperapi_init' );
add_action('admin_menu', 'zoosuperapi_add_options_page');

$zooapi_upload_dir = wp_upload_dir();
define('ZOOSUPERAPI_UPLOADDIR', $zooapi_upload_dir['basedir'].'/zoosuperapi');
define('ZOOSUPERAPI_UPLOADURL', $zooapi_upload_dir['baseurl'].'/zoosuperapi');

function zoosuperapi_deactivate()
{
	zoosuperapi_delete_dir(ZOOSUPERAPI_UPLOADDIR);
}

function zoosuperapi_activate()
{
	global $wpdb;
	
	$tmp = get_option('zoosuperapi_options');

	if (empty($tmp))
	{
		$tmp = array(
			'save_user_photos' => 0,
			'save_property_data' => 0,
			'convert_to_sqm' => 1,
			'remove_sold' => 0,
			'remove_leased' => 0,
			'remove_withdrawn' => 1,
			'skip_fields_office' => '',
			'skip_fields_user' => '',
			'skip_fields_property' => '',
			'remove_office_ids' => '',
			'remove_user_ids' => '',
			'remove_property_ids' => '',
			'save_wpuser_contact' => 0,
			'save_wpuser_office' => 0,
			'developer_key' => '',
			'debug' => 0,
			);

		update_option('zoosuperapi_options', $tmp);
	}

	$width = false;
	$attachments = zoosuperapi_get_columns('attachments');
	foreach ($attachments as $column)
	{
		if (strtolower($column) == 'width')
			$width = true;
	}
	if (!$width)
		$wpdb->query("ALTER TABLE `attachments` ADD  `width` SMALLINT UNSIGNED NOT NULL DEFAULT '0' AFTER `type`, ADD `height` SMALLINT UNSIGNED NOT NULL DEFAULT '0' AFTER `width`");

	// Make folders
	$folders = array(
		ZOOSUPERAPI_UPLOADDIR,
		ZOOSUPERAPI_UPLOADDIR.'/agents',
		ZOOSUPERAPI_UPLOADDIR.'/property',
		ZOOSUPERAPI_UPLOADDIR.'/logs',
		);
	foreach ($folders as $folder)
	{
		if (!file_exists($folder))
		{
			zoosuperapi_make_dir($folder);
		}
	}
}

function zoosuperapi_init()
{
	register_setting('zoosuperapi_plugin_options', 'zoosuperapi_options', 'zoosuperapi_validate_options');
}

function zoosuperapi_add_options_page()
{
	add_menu_page('Zoo Super API', 'Zoo Super API', 8, __FILE__, 'zoosuperapi_render_form');
	add_submenu_page(__FILE__, 'Zoo Super API Options', 'Options', 'manage_options', __FILE__, 'zoosuperapi_render_form');
}

function zoosuperapi_render_form()
{
	$options = get_option('zoosuperapi_options');
	
	$form = array(
		'save_user_photos' => array('type' => 'checkbox', 'label' => 'Save user photos'),
		'save_property_data' => array('type' => 'checkbox', 'label' => 'Save property data locally'),
		'convert_to_sqm' => array('type' => 'checkbox', 'label' => 'Convert Floor/Land Area to Sqm'),
		'remove_sold' => array('type' => 'checkbox', 'label' => 'Remove sold properties'),
		'remove_leased' => array('type' => 'checkbox', 'label' => 'Remove leased properties'),
		'remove_withdrawn' => array('type' => 'checkbox', 'label' => 'Remove withdrawn properties'),
		'skip_fields_office' => array('type' => 'text', 'label' => 'Skip updating these office fields'),
		'skip_fields_user' => array('type' => 'text', 'label' => 'Skip updating these user fields'),
		'skip_fields_property' => array('type' => 'text', 'label' => 'Skip updating these property fields'),
		'remove_office_ids' => array('type' => 'text', 'label' => 'Delete the following offices'),
		'remove_user_ids' => array('type' => 'text', 'label' => 'Delete the following users'),
		'remove_property_ids' => array('type' => 'text', 'label' => 'Delete the following properties'),
		'save_wpuser_contact' => array('type' => 'checkbox', 'label' => 'Save contacts as WP user'),
		'save_wpuser_office' => array('type' => 'checkbox', 'label' => 'Save office as WP user'),
		'developer_key' => array('type' => 'text', 'label' => 'Zoo Developer Key'),
		'debug' => array('type' => 'checkbox', 'label' => 'Debug mode'),
		);

	$field_id = 0;
	?>
	<div class="wrap">
		<div class="icon32" id="icon-options-general"><br></div>
		<h2>Zoo Super API Options</h2>

		<form method="post" action="options.php">
			<?php settings_fields('zoosuperapi_plugin_options'); ?>
			<?php $options = get_option('zoosuperapi_options'); ?>
			<table class="form-table">
				<?php
				foreach ($form as $field => $opts)
				{
					$type = isset($opts['type']) ? $opts['type'] : 'text';
					$label = isset($opts['label']) ? $opts['label'] : '';
					$field_id++;

					echo '<tr valign="top"><th scope="row" style="width:300px"><label for="zoofield-'.$field_id.'">'.$label.'</label></th><td>';

					switch ($type)
					{
						case 'checkbox':
							echo '<input name="zoosuperapi_options['.$field.']" id="zoofield-'.$field_id.'" type="checkbox" value="1"'.(isset($options[$field]) && !empty($options[$field]) ? ' checked="checked"' : '').'  />';
							break;
						case 'select':
							echo '<select name="zoosuperapi_options['.$field.']">';
							if (empty($opts['values']))
								$opts['values'] = array();

							foreach ($opts['values'] as $value => $txt)
							{
								echo '<option value="'.$value.'" '.($value == $options[$field] ? ' selected="selected"' : '').'>'.$txt.'</option>';
							}
							echo '</select>';
							break;
						case 'text':
							echo '<input type="text" name="zoosuperapi_options['.$field.']" value="'.$options[$field].'" />';
							break;
						case 'plain':
							echo $options[$field];
							break;
					}

					echo '</td></tr>';
				}
				?>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
			</p>
		</form>
	</div>
	<?php
}

function zoosuperapi_validate_options($input)
{
	return $input;
}

function zoosuperapi_api()
{
	if (empty($_REQUEST['zooapi']))
		return;

	ini_set('display_errors', "On");
	error_reporting(E_ALL & ~E_NOTICE);

	global $wpdb, $wp_version;
	$options = get_option('zoosuperapi_options');

	set_error_handler('zoosuperapi_handle_errors', E_ALL & ~E_NOTICE);
	date_default_timezone_set('UTC');

	zoosuperapi_disable_quotes();
	if (isset($_REQUEST['s']))
	{
		while (strlen($_REQUEST['s']) > 1)
		{
			$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
		}
	}
	
	define( 'DIEONDBERROR', true );
	$wpdb->show_errors();

	$return = array();
	$status = false;
	$log = array();
	$log_type = 'general';
	$logging = true;

	$start_time = microtime(true);

	switch ($_REQUEST['zooapi'])
	{
		case 'ping':
			$table_cache = zoosuperapi_get_tables();
			
			if (!isset($table_cache['office_columns']))
				$table_cache = zoosuperapi_get_tables(true);
			
			$status = true;
			$status = $status & !empty($table_cache['attachments_columns']);
			$status = $status & $table_cache['extras_exists'];
			$status = $status & $table_cache['office_exists'];
			$status = $status & $table_cache['office_specialities_exists'];
			$status = $status & !empty($table_cache['properties_columns']);
			$status = $status & $table_cache['property_features_exists'];
			$status = $status & $table_cache['property_meta_exists'];
			$status = $status & $table_cache['property_testimonials_exists'];
			$status = $status & $table_cache['opentimes_exists'];
			$status = $status & $table_cache['users_exists'];
			
			$contacts_supported = true;
			$contacts_supported = $contacts_supported & $table_cache['contacts_exists'];
			$contacts_supported = $contacts_supported & $table_cache['contact_assignments_exists'];
			$contacts_supported = $contacts_supported & $table_cache['notes_exists'];
			$contacts_supported = $contacts_supported & $table_cache['notes_sources_exists'];
			
			$sent_enquiries = 0;
			if ($table_cache['property_enquiry_form_exists'])
			{
				$changed = false;
				if (!in_array('sent_to_zoo', $table_cache['property_enquiry_form_columns']))
				{
					$wpdb->query('ALTER TABLE  `property_enquiry_form` ADD  `sent_to_zoo` INT UNSIGNED NOT NULL DEFAULT "0";');
					$changed = true;
				}
				if (!in_array('zoo_note_id', $table_cache['property_enquiry_form_columns']))
				{
					$wpdb->query('ALTER TABLE  `property_enquiry_form` ADD  `zoo_note_id` INT UNSIGNED NULL DEFAULT NULL;');
					$changed = true;
				}
				if (!in_array('private_key', $table_cache['office_columns']))
				{
					$wpdb->query('ALTER TABLE  `office` ADD  `private_key` CHAR( 40 ) NULL DEFAULT NULL ;');
					$changed = true;
				}
				if ($changed)
					$table_cache = zoosuperapi_get_tables(true);

				require('modules/enquiry.php');
				$sent_enquiries = zoo_send_enquiry();
			}
			
			$return = array(
				'version' => ZOOSUPERAPI_VERSION,
				'wp_version' => $wp_version,
				'server' => file_get_contents('http://agentaccount.com/xml_file/api_daemon/get_ip.php'),
				'contact_notes_supported' => $contacts_supported,
				'enquiries_sent' => $sent_enquiries,
				);
			$logging = false;
			break;
		case 'listproperties':
			$office_sql = '';
			if (isset($_REQUEST['office_id']) && intval($_REQUEST['office_id']) > 0)
				$office_sql = ' AND office_id = '.intval($_REQUEST['office_id']);
			
			$properties = $wpdb->get_results("SELECT properties.id, properties.updated_at FROM properties WHERE 1=1 $office_sql GROUP BY properties.id", ARRAY_A);

			if (!is_array($properties))
				$properties = array();
			
			$property_ids = array(-1);
			foreach ($properties as $property)
			{
				$property_ids[] = $property['id'];
			}
			
			$image_results = array();
			$results = $wpdb->get_results("SELECT parent_id, MAX(updated_at) m, COUNT(id) c FROM attachments WHERE (is_user = 0 OR is_user IS NULL) AND description IN ('large', 'medium', 'thumb') AND type IN ('photo', 'floorplan') AND parent_id IN (".implode(',', $property_ids).") GROUP BY parent_id", ARRAY_A);
			if (empty($results))
				$results = array();
			foreach ($results as $row)
			{
				$image_results[$row['parent_id']] = array(floor($row['c'] / 3), strtotime($row['m']));
			}
			
			$results = $wpdb->get_results("SELECT parent_id, MAX(updated_at) m, COUNT(id) c FROM attachments WHERE (is_user = 0 OR is_user IS NULL) AND type NOT IN ('photo', 'floorplan') AND parent_id IN (".implode(',', $property_ids).") GROUP BY parent_id", ARRAY_A);
			if (empty($results))
				$results = array();
			foreach ($results as $row)
			{
				$count = 0;
				$updated = 0;
				if (isset($image_results[$row['parent_id']]))
				{
					$count = $image_results[$row['parent_id']][0];
					$updated = $image_results[$row['parent_id']][1];
				}
				
				$image_results[$row['parent_id']] = array($row['c'] + $count, max(strtotime($row['m']), $updated));
			}
			
			$return['properties'] = array();
			foreach ($properties as $property)
			{
				$return['properties'][] = array(
					intval($property['id']),
					strtotime($property['updated_at']),
					intval(isset($image_results[$property['id']]) ? $image_results[$property['id']][0] : 0),
					intval(isset($image_results[$property['id']]) ? $image_results[$property['id']][1] : 0),
					);
			}

			$return['exclude']['sold'] = (isset($options['remove_sold']) && $options['remove_sold']) ? 1 : 0;
			$return['exclude']['leased'] = (isset($options['remove_leased']) && $options['remove_leased']) ? 1 : 0;
			$return['exclude']['withdrawn'] = (isset($options['remove_withdrawn']) && $options['remove_withdrawn']) ? 1 : 0;

			$status = true;
			$logging = false;
			break;
		case 'listoffices':
			$offices = $wpdb->get_results("SELECT id, updated_at FROM office", ARRAY_A);

			if (!is_array($offices))
				$offices = array();

			$return['offices'] = array();
			foreach ($offices as $office)
			{
				$return['offices'][] = array(
					intval($office['id']),
					strtotime($office['updated_at']),
					);
			}

			$status = true;
			$logging = false;
			break;
		case 'listcontacts':
			$office_sql = '';
			if (isset($_REQUEST['office_id']) && intval($_REQUEST['office_id']) > 0)
				$office_sql = 'WHERE office_id = '.intval($_REQUEST['office_id']);
			
			$contacts = $wpdb->get_results("SELECT id, updated_at FROM users $office_sql", ARRAY_A);

			if (!is_array($contacts))
				$contacts = array();

			$return['contacts'] = array();
			foreach ($contacts as $contact)
			{
				$return['contacts'][] = array(
					intval($contact['id']),
					strtotime($contact['updated_at']),
					);
			}

			$status = true;
			$logging = false;
			break;
		case 'listpeople':
			$office_sql = '';
			if (isset($_REQUEST['office_id']) && intval($_REQUEST['office_id']) > 0)
				$office_sql = 'WHERE office_id = '.intval($_REQUEST['office_id']);
			
			$contacts = $wpdb->get_results("SELECT contacts.id, contacts.updated_at, MAX(notes.updated_at) n FROM contacts JOIN notes ON notes.contact_id = contacts.id $office_sql GROUP BY contacts.id", ARRAY_A);

			if (!is_array($contacts))
				$contacts = array();

			$return['people'] = array();
			foreach ($contacts as $contact)
			{
				$return['people'][] = array(
					intval($contact['id']),
					strtotime($contact['updated_at']),
					strtotime($contact['n']),
					);
			}

			$status = true;
			$logging = false;
			break;
		case 'office':
			require(dirname(__FILE__).'/modules/office.php');

			$office = json_decode($_REQUEST['office'], true);

			$status = zoosuperapi_insert_office($log, $office, $options);

			$log_type = 'office_'.$office['id'];
			break;
		case 'contact':
			require(dirname(__FILE__).'/modules/contact.php');

			$contact = json_decode($_REQUEST['contact'], true);

			$status = zoosuperapi_insert_contact($log, $contact, $options);
			
			$log_type = 'contact_'.$contact['id'];
			break;
		case 'property':
			require(dirname(__FILE__).'/modules/property.php');

			$property = json_decode($_REQUEST['property'], true);

			$status = zoosuperapi_insert_property($log, $property, $options);
			
			$log_type = 'property_'.$property['id'];
			break;
		case 'person':
			require(dirname(__FILE__).'/modules/people.php');

			$person = json_decode($_REQUEST['person'], true);

			$status = zoosuperapi_insert_people($log, $person, $options);

			$log_type = 'person_'.$office['id'];
			break;
	}

	$end_time = microtime(true);

	//$return['log'] = $log;
	$return['status'] = $status;

	if (isset($options['debug']) && $options['debug'])
	{
		$log['REQUEST'] = $_REQUEST;
	}
	$log['duration'] = $end_time - $start_time;

	if ($logging && $options['debug'])
	{
		zoosuperapi_make_dir(ZOOSUPERAPI_UPLOADDIR.'/logs/');
		file_put_contents(ZOOSUPERAPI_UPLOADDIR.'/logs/'.$log_type.'.log', json_encode($log));
	}

	echo json_encode($return);
	die();
}

function zoosuperapi_get_columns($table)
{
	global $wpdb;

	$columns = array();
	$return = $wpdb->get_results("SHOW COLUMNS FROM `$table`", ARRAY_A);

	foreach ($return as $row)
	{
		if (!empty($row['Field']))
			$columns[] = $row['Field'];
	}

	return $columns;
}

function zoosuperapi_make_dir($path)
{
	if (!file_exists($path))
		mkdir($path, 0777, true);
}

function zoosuperapi_delete_dir($path)
{
	if (!file_exists($path))
		return;

	$files = @scandir($path);

	foreach ($files as $file)
	{
		if ($file == '.' || $file == '..')
			continue;

		$file_path = $path.'/'.$file;

		if (is_dir($file_path))
			zoosuperapi_delete_dir($file_path);
		elseif (is_file($file_path))
			@unlink($file_path);
	}

	@rmdir($path);
}

function zoosuperapi_handle_errors($errno, $errstr, $errfile, $errline)
{
	switch ($errno) {
		case E_ERROR:
			$errno = "Error";
			break;
		case E_WARNING:
			$errno = "Warning";
			break;
		case E_NOTICE:
			$errno = "Notice";
			break;
	}

	file_put_contents('zoosuperapi.txt', "$errno\n$errstr\n$errfile\n$errline");
	echo "<b>$errno</b>: $errstr in $errfile on line $errline\n";

	return true;
}

function zoosuperapi_disable_quotes()
{
	if (get_magic_quotes_gpc())
	{
		$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
		while (list($key, $val) = each($process))
		{
			foreach ($val as $k => $v)
			{
				unset($process[$key][$k]);
				if (is_array($v)) {
					$process[$key][stripslashes($k)] = $v;
					$process[] = &$process[$key][stripslashes($k)];
				} else {
					$process[$key][stripslashes($k)] = stripslashes($v);
				}
			}
		}
		unset($process);
	}
}

class MultiRequests
{
	private $pending = array();
	private $parameters = array();
	private $waiting = 0;
	private $mh;
	private $opts = array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_TIMEOUT => 600,
		CURLOPT_USERAGENT => 'AgentPoint-ZooSuperAPI',
		);
	private $max_per_server = 4;

	function __construct()
	{
		$this->mh = curl_multi_init();
	}

	function request($url, $post = array(), $callback = array(), $params = array())
	{

		if ($this->waiting > $this->max_per_server)
		{
			$this->pending[] = array(
				$url,
				$post,
				$callback,
				$params,
				);
			return;
		}

		$this->waiting++;

		$ch = curl_init();
		curl_setopt_array($ch, $this->opts);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (!empty($post))
		{
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}

		curl_multi_add_handle($this->mh, $ch);

		$key = (int) $ch;

		$this->parameters[$key] = array(
			'url' => $url,
			'callback' => $callback,
			'params' => $params,
			);
	}

	function wait()
	{
		$pending = $this->pending;
		$this->pending = array();
		foreach ($pending as $row)
		{
			$this->request($row[0], $row[1], $row[2], $row[3]);
		}

		if (curl_multi_select($this->mh, 3) === -1)
			return;

		$active = false;

		do {
			$mrc = curl_multi_exec($this->mh, $active);
		} while ($mrc == CURLM_CALL_MULTI_PERFORM);

		while ($info = curl_multi_info_read($this->mh))
		{
			$ch = $info['handle'];
			$key = (int) $ch;

			if (!isset($this->parameters[$key]))
				die('Weird MultiRequests error');

			$request = $this->parameters[$key];

			$this->waiting--;

			$data = curl_multi_getcontent($ch);

			if (!empty($data))
			{
				call_user_func($request['callback'], $data, $request['params']);
			}

			unset($this->parameters[$key]);

			curl_multi_remove_handle($this->mh, $ch);
		}
	}

	function active_requests()
	{
		return count($this->parameters);
	}
}

function store_img($data, $params)
{
	$path = $params['path'];
	$dirname = dirname($path);

	zoosuperapi_make_dir($dirname);

	return file_put_contents($path, $data);
}


function delete_images($imgs, $del_row = true, $is_user = 0)
{
	global $wpdb;

	if (!is_array($imgs))
		$attachments = $wpdb->get_results('SELECT * FROM attachments WHERE is_user = '.$is_user.' AND parent_id = '.$imgs, ARRAY_A);
	elseif (empty($imgs))
		return true;
	else
		$attachments = $wpdb->get_results('SELECT * FROM attachments WHERE id IN ('.implode(',', $imgs).')', ARRAY_A);

	foreach ($attachments as $attachment)
	{
		if (stripos($attachment['url'], ZOOSUPERAPI_UPLOADURL) !== false)
		{
			$file = str_replace(ZOOSUPERAPI_UPLOADURL, ZOOSUPERAPI_UPLOADDIR, $attachment['url']);

			if (file_exists($file) && is_writable($file))
				unlink($file);
		}

		if ($del_row)
			$wpdb->query('DELETE FROM attachments WHERE id = '.$attachment['id']);
	}
}

function filter_columns($columns, $data)
{
	$output = array();
	$keys = array_keys($data);

	foreach ($columns as $column)
	{
		if (in_array($column, $keys))
			$output[$column] = $data[$column];
	}

	return $output;
}

function zoosuperapi_insert($table, $data)
{
	global $wpdb;

	$fields = array_keys($data);
	$formatted_fields = array();
	$real_data = array();
	foreach ( $fields as $field )
	{
		if ($data[$field]===null)
		{
			$formatted_fields[] = 'NULL';
			continue;
		}
		$formatted_fields[] = "'%s'";
		$real_data[] = $data[$field];
	}
	$sql = "INSERT INTO $table (`".implode('`,`', $fields)."`) VALUES (".implode(",", $formatted_fields).")";
	return $wpdb->query( $wpdb->prepare( $sql, $real_data) );
}

function zoosuperapi_update($table, $data, $where)
{
	if ( !is_array( $where ) )
		return false;

	global $wpdb;

	$bits = $wheres = array();
	$fields = (array) array_keys($data);
	$real_data = array();
	foreach ( $fields as $field )
	{
		if ($data[$field]===null)
		{
			$bits[] = "`$field` = NULL";
			continue;
		}
		$bits[] = "`$field` = %s";
		$real_data[] = $data[$field];
	}

	$fields = (array) array_keys($where);
	foreach ( $fields as $field )
	{
		$wheres[] = "$field = %s";
	}

	$sql = "UPDATE $table SET ".implode(', ', $bits).' WHERE '.implode( ' AND ', $wheres);

	return $wpdb->query( $wpdb->prepare( $sql, array_merge($real_data, array_values($where))) );
}

function zoosuperapi_get_tables($force = false)
{
	global $wpdb;
	
	$debug_value = $wpdb->show_errors;
	$wpdb->show_errors = false;
	
	$table_cache = get_option('zoosuperapi_table_cache', '');
	if (empty($table_cache))
		add_option('zoosuperapi_table_cache', '', '', 'no');
	else
		$table_cache = json_decode($table_cache, true);
	
	if ($force || empty($table_cache) || !isset($table_cache['last_update']) || $table_cache['last_update'] < time() - 86400)
	{
		$table_cache['last_update'] = time();
		
		$table_cache['properties_columns'] = zoosuperapi_get_columns('properties');
		$table_cache['attachments_columns'] = zoosuperapi_get_columns('attachments');
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "property_features"');
		$table_cache['property_features_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "property_meta"');
		$table_cache['property_meta_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "property_testimonials"');
		$table_cache['property_testimonials_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "opentimes"');
		$table_cache['opentimes_exists'] = !empty($tmp);
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "holiday_lease_rates"');
		$table_cache['holiday_lease_rates_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "holiday_lease_rates"');
		$table_cache['holiday_lease_bookings_exists'] = !empty($tmp);
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "new_development_details"');
		$table_cache['new_development_details_exists'] = !empty($tmp);
		$table_cache['new_development_details_columns'] = $table_cache['new_development_details_exists'] ? zoosuperapi_get_columns('new_development_details') : array();
	
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "livestock_sale_details"');
		$table_cache['livestock_sale_details_exists'] = !empty($tmp);
		$table_cache['livestock_sale_details_columns'] = $table_cache['livestock_sale_details_exists'] ? zoosuperapi_get_columns('livestock_sale_details') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "clearing_sale_details"');
		$table_cache['clearing_sale_details_exists'] = !empty($tmp);
		$table_cache['clearing_sale_details_columns'] = $table_cache['clearing_sale_details_exists'] ? zoosuperapi_get_columns('clearing_sale_details') : array();
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "clearing_sales_event_details"');
		$table_cache['clearing_sales_event_details_exists'] = !empty($tmp);
		$table_cache['clearing_sales_event_details_columns'] = $table_cache['clearing_sales_event_details_exists'] ? zoosuperapi_get_columns('clearing_sales_event_details') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "extras"');
		$table_cache['extras_exists'] = !empty($tmp);
		$table_cache['extras_columns'] = $table_cache['extras_exists'] ? zoosuperapi_get_columns('extras') : array();
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "extra_details"');
		$table_cache['extra_details_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "extra_options"');
		$table_cache['extra_options_exists'] = !empty($tmp);
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "property_exports"');
		$table_cache['property_exports_exists'] = !empty($tmp);
		$table_cache['property_exports_columns'] = $table_cache['property_exports'] ? zoosuperapi_get_columns('property_exports') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "property_video"');
		$table_cache['property_video_exists'] = !empty($tmp);
		$table_cache['property_video_columns'] = $table_cache['property_video_exists'] ? zoosuperapi_get_columns('property_video') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "pds"');
		$table_cache['pds_exists'] = !empty($tmp);
		$table_cache['pds_columns'] = $table_cache['pds_exists'] ? zoosuperapi_get_columns('pds') : array();
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "pds_investment"');
		$table_cache['pds_investment_exists'] = !empty($tmp);
		$table_cache['pds_investment_columns'] = $table_cache['pds_investment_exists'] ? zoosuperapi_get_columns('pds_investment') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "specific_field_values"');
		$table_cache['specific_field_values_exists'] = !empty($tmp);
		$table_cache['specific_field_values_columns'] = $table_cache['specific_field_values_exists'] ? zoosuperapi_get_columns('specific_field_values') : array();
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "specific_field_values"');
		$table_cache['specific_field_dropdowns_exists'] = !empty($tmp);
		$table_cache['specific_field_dropdowns_columns'] = $table_cache['specific_field_dropdowns_exists'] ? zoosuperapi_get_columns('specific_field_dropdowns') : array();
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "specific_field_values"');
		$table_cache['specific_fields_exists'] = !empty($tmp);
		$table_cache['specific_fields_columns'] = $table_cache['specific_fields_exists'] ? zoosuperapi_get_columns('specific_fields') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "land_home_designs"');
		$table_cache['land_home_designs_exists'] = !empty($tmp);
		$table_cache['land_home_designs_columns'] = $table_cache['land_home_designs_exists'] ? zoosuperapi_get_columns('land_home_designs') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "rental_seasons"');
		$table_cache['rental_seasons_exists'] = !empty($tmp);
		$table_cache['rental_seasons_columns'] = $table_cache['rental_seasons_exists'] ? zoosuperapi_get_columns('rental_seasons') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "office"');
		$table_cache['office_exists'] = !empty($tmp);
		$table_cache['office_columns'] = $table_cache['office_exists'] ? zoosuperapi_get_columns('office') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "office_specialities"');
		$table_cache['office_specialities_exists'] = !empty($tmp);
		$table_cache['office_specialities_columns'] = $table_cache['office_specialities_exists'] ? zoosuperapi_get_columns('office_specialities') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "contacts"');
		$table_cache['contacts_exists'] = !empty($tmp);
		$table_cache['contacts_columns'] = $table_cache['contacts_exists'] ? zoosuperapi_get_columns('contacts') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "contact_assignments"');
		$table_cache['contact_assignments_exists'] = !empty($tmp);
		$table_cache['contact_assignments_columns'] = $table_cache['contact_assignments_exists'] ? zoosuperapi_get_columns('contact_assignments') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "notes"');
		$table_cache['notes_exists'] = !empty($tmp);
		$table_cache['notes_columns'] = $table_cache['notes_exists'] ? zoosuperapi_get_columns('notes') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "notes_sources"');
		$table_cache['notes_sources_exists'] = !empty($tmp);
		$table_cache['notes_sources_columns'] = $table_cache['notes_sources_exists'] ? zoosuperapi_get_columns('notes_sources') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "users"');
		$table_cache['users_exists'] = !empty($tmp);
		$table_cache['users_columns'] = $table_cache['users_exists'] ? zoosuperapi_get_columns('users') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "user_groups"');
		$table_cache['user_groups_exists'] = !empty($tmp);
		$table_cache['user_groups_columns'] = $table_cache['user_groups_exists'] ? zoosuperapi_get_columns('user_groups') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "testimonials"');
		$table_cache['testimonials_exists'] = !empty($tmp);
		$table_cache['testimonials_columns'] = $table_cache['testimonials_exists'] ? zoosuperapi_get_columns('testimonials') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "property_enquiry_form"');
		$table_cache['property_enquiry_form_exists'] = !empty($tmp);
		$table_cache['property_enquiry_form_columns'] = $table_cache['property_enquiry_form_exists'] ? zoosuperapi_get_columns('property_enquiry_form') : array();
		
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "custom_fields"');
		$table_cache['custom_fields_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "custom_values"');
		$table_cache['custom_values_exists'] = !empty($tmp);
		$tmp = $wpdb->get_results('SHOW TABLES LIKE "custom_properties"');
		$table_cache['custom_properties_exists'] = !empty($tmp);
		
		update_option('zoosuperapi_table_cache', json_encode($table_cache));
	}
	
	$wpdb->show_errors = $debug_value;
	
	return $table_cache;
}

function zoo_api2($method, $query = array(), $post = array())
{
	$url = "http://api2.agentaccount.com/$method?".http_build_query($query);

	$ch = curl_init($url);                                                                      
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	if (!empty($post))
	{
		$post_str = json_encode($post);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_str);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($post_str)
			));                                                                                                                   
	}
	$result = curl_exec($ch);

	$output = json_decode($result, true);
	
	if ($output == null)
	{
		$options = get_option('zoosuperapi_options');
		if (isset($options['debug']) && $options['debug'])
			var_dump($result);
	}
	
	return $output;
}