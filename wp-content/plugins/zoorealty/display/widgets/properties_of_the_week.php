<?php
if(!function_exists('print_item')):
	function print_item($key, $value, $printKey = true, $keyName =''){ 
			global $helper;
			if(is_array($value)):
				foreach($value as $innerKey=>$innerValue)
					print_item($innerKey, $innerValue);
			else:
				if(empty($keyName) || $keyName==1) 
					$keyName = $helper->humanize($key);
				if($helper->validUrl($value))
					$value = '<a target="_blank" href="'.$value.'" title="'.$keyName.'" class="btn">'.$keyName.'</a>';
?>
<tr class="<?php echo $key; ?>">
<?php if($printKey): ?>
		<td class="field"><?php echo $keyName; ?>: </td>
<?php endif; ?>
		<td class="value"><?php if($key=='type'){if(strpos($value,'Sale')!==false)$value="For Sale"; else $value="For Lease";} echo $value; ?></td>
</tr>

<?php endif; } endif; 
$count = 0;
foreach($settings as $type=>$title):
		$property = $realty->property($realty->settings['featured_listings']['properties_of_the_week'][$type][0]);
	if(!$property) continue;
?>
<div id="week_<?php echo $type; ?>" class="block">
	<?php echo $before_title; ?><span class="title_<?php echo $type;?>"><?php echo $title;?></span><?php echo $after_title; ?>

	<?php
?>
<div class="feature_photo float-<?php echo ($count % 2== 0)? "left": "right"; ?>">
<div><a href="<?php echo $property['url']; ?>" title="<?php echo htmlspecialchars($property['headline']); ?>"><img src="<?php echo $property['thumbnail']; ?>" alt="<?php echo htmlspecialchars($property['headline']); ?>" /></a>
<div>
<?php
	foreach($settings as $key=>$values):
	if(( !$values['display'])  ||empty($property[$key]) || $key =='house' || $key=='apartment' || $key=='rental') continue;
	switch($key):
		default:
		?>
	<span class="<?php echo $key; ?>"><?php if(!empty($settings[$key]['title'] )) echo $settings[$key]['title'].": ";?><?php if($key=='type'){if(strpos($property[$key],'Sale')!==false)$property[$key]="For Sale"; else $property[$key]="For Lease";} echo $property[$key]; ?></span>
		<?php 
	break;
	endswitch;
	endforeach;
	?><div class="clear"></div></div></div></div><?php
			$count++;
?></div><!-- ends #week_<?php echo $type; ?> --><?php
endforeach;
?>