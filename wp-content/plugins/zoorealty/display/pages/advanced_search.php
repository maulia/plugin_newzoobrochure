<style type="text/css">
.features td select{width:90%;}
</style>
<?php  
global $helper;
if(!empty($this->params['list']))$list=$this->params['list'];
else $list=($_GET['list']=='')?'sale':$_GET['list'];

$type = (strpos(strtolower($list), 'lease') !== false)? 'lease': 'sale';
$settings=$this->settings['widgets']['quick_search'];
$price_options = $settings[$type];
$max_price = end($price_options);
$size_options = array("0","100","200","300","400","500","600","700","800","900","1000","2000","3000","4000","5000","10000");
$max_size = end($size_options);

$property_types = $this->property_search('property_type',$list);
$suburbs = $this->property_search('suburb',$list);
$office_id_all= esc_sql(implode(', ', $this->settings['general_settings']['office_id']));
switch(trim($this->params['list'])){
	case '':		
		$lists=array();
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('ResidentialSale') limit 1", ARRAY_A);
		if($chk_exists)$lists['ResidentialSale']='Residential Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('ResidentialLease') limit 1", ARRAY_A);
		if($chk_exists)$lists['ResidentialLease']='Residential Lease';
		if($chk_exists)$lists['commercial_sale']='Commercial Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('Commercial') and deal_type in ('lease') limit 1", ARRAY_A);
		if($chk_exists)$lists['commercial_lease']='Commercial Lease';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('BusinessSale') limit 1", ARRAY_A);
		if($chk_exists)$lists['BusinessSale']='Business Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('ProjectSale') limit 1", ARRAY_A);
		if($chk_exists)$lists['ProjectSale']='Project Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('Commercial') and deal_type IN ('sale','both') limit 1", ARRAY_A);		
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('HolidayLease') limit 1", ARRAY_A);
		if($chk_exists)$lists['HolidayLease']='Holiday';		
	break;
	case 'sale':case 'Sale':
		$list_condition .= " and properties.type IN ('ResidentialSale', 'BusinessSale', 'ProjectSale')";
		$lists=array('sale'=>'Sale');
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('ResidentialSale') limit 1", ARRAY_A);
		if($chk_exists)$lists['ResidentialSale']='Residential Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('BusinessSale') limit 1", ARRAY_A);
		if($chk_exists)$lists['BusinessSale']='Business Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('ProjectSale') limit 1", ARRAY_A);
		if($chk_exists)$lists['ProjectSale']='Project Sale';
	break;
	case 'lease':case 'Lease':
		$list_condition .= " and properties.type IN ('ResidentialLease', 'HolidayLease')";
		$lists=array('lease'=>'Lease');
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('ResidentialLease') limit 1", ARRAY_A);
		if($chk_exists)$lists['ResidentialLease']='Residential Lease';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('HolidayLease') limit 1", ARRAY_A);
		if($chk_exists)$lists['HolidayLease']='Holiday';
	break;
	case 'commercial':case 'Commercial':
		$list_condition .= " and properties.type IN ('Commercial')";
		$lists=array('commercial'=>'Commercial');
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('Commercial') and deal_type IN ('sale','both') limit 1", ARRAY_A);
		if($chk_exists)$lists['commercial_sale']='Commercial Sale';
		$chk_exists=$wpdb->get_results("select id from properties where office_id in ($office_id_all) and status in (1,4) and type in ('Commercial') and deal_type in ('lease') limit 1", ARRAY_A);
		if($chk_exists)$lists['commercial_lease']='Commercial Lease';
	break;
	case 'ResidentialSale':
		$list_condition .= " and properties.type IN ('ResidentialSale')";
		$lists=array('ResidentialSale'=>'Residential Sale');
	break;
	case 'BusinessSale':
		$list_condition .= " and properties.type IN ('BusinessSale')";
		$lists=array('BusinessSale'=>'Business Sale');
	break;
	case 'ProjectSale':
		$list_condition .= " and properties.type IN ('ProjectSale')";
		$lists=array('ProjectSale'=>'Project Sale');
	break;
	case 'ResidentialLease':
		$list_condition .= " and properties.type IN ('ResidentialLease')";
		$lists=array('ResidentialLease'=>'Residential Lease');
	break;
	case 'HolidayLease':
		$list_condition .= " and properties.type IN ('HolidayLease')";
		$lists=array('HolidayLease'=>'Holiday');
	break;
	case 'commercial_sale':
		$list_condition .= " and properties.type IN ('Commercial') and deal_type in ('sale','both')";
		$lists=array('commercial_sale'=>'Commercial Sale');
	break;
	case 'commercial_lease':
		$list_condition .= " and properties.type IN ('Commercial') and deal_type in ('lease')";
		$lists=array('commercial_lease'=>'Commercial Lease');
	break;
	case 'ResidentialSale-BusinessSale':
		$list_condition .= " and properties.type IN ('ResidentialSale','BusinessSale')";
		$lists=array('ResidentialSale'=>'Residential Sale','BusinessSale'=>'Business Sale');
	break;
	case 'ResidentialLease-HolidayLease':
		$list_condition .= " and properties.type IN ('ResidentialLease','HolidayLease')";
		$lists=array('ResidentialLease'=>'Residential Lease','HolidayLease'=>'Holiday');
	break;
}

$list_features = $wpdb->get_results("select distinct feature from property_features where property_id in (select id from properties where office_id IN ($office_id_all) $list_condition) order by feature", ARRAY_A);

?>

<div id="advanced_search">
	<div id="block_content">        
        <?php if($request['search_error']=='1'): ?><p class="alert">Please correct the items as indicated below, and press "Search" again.<br/></p><?php endif; ?>
        <div id="smart_search">
			<p>
            	<a href="<?php echo $this->siteUrl; ?>search-results/?list=<?php echo $list; ?>&hour_limit=1">Properties added in the last 24 hours</a> | 
				<a href="<?php echo $this->siteUrl; ?>search-results/?list=<?php echo $list; ?>&hour_limit=3">Properties added in the last 3 days</a>          
			</p>
		</div>
        <form id="search_advanced" name="search_advanced" action="<?php echo $this->siteUrl; ?>search-results/" method="get" onsubmit="return submit_form();";>            
            <div class="adv-search">
                <div class="adv-search-option as-list-type">
                    <label>Listing Type</label>
					<select name="list" id="select_type" onChange="showtab_ajax(this.value)">
  					 <?php foreach($lists as $key=>$value):	?>
						<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
                    <div class="clear"></div>
                </div>
                <div class="adv-search-option as-property-type">
                    <label>Property Type</label>
                    <div class="adv-search-item">
                        <div  id="property_type_list" onClick="expand_propertyType();">	
                            <input id='propertyType' value="All" readonly="true" /> 
                            <span class="span-arrow"><img class="adv-search-arrow" src="<?php echo $this->pluginUrl; ?>display/pages/js/dd_arrow_default.png" style="margin:0 !important;"></span>
                        </div>
                        <div id="select_property_type" style="display:none;">
                            <p class="chk-protype"><input type="checkbox" value="" id="all_type" onClick="check_all_type();" <?php if(empty($_GET['property_type']))echo 'checked="checked"';?>> All</p>
                            <?php foreach($property_types as $property_type):if(!is_array($property_type)) continue;?>
                            <p class="chk-protype"><input name="property_type[]" type="checkbox" value="<?php echo $property_type['tag']; ?>" onClick="check_type('<?php echo $property_type['tag']; ?>');" <?php if(is_array($_GET['property_type'])){ if(in_array($property_type['tag'],$_GET['property_type'])) echo 'checked="checked"';}?>> <?php echo $property_type['tag']; ?></p>
                            <?php endforeach; ?>
                              <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="adv-search-option as-suburb">                   
				   <label>Suburbs</label>
                    <div class="adv-search-item">
                        <div  id="suburb_list"  onClick="expand_suburb();">	
                            <input id='suburb' value="All" readonly="true" /> 
                            <span class="span-arrow"><img class="adv-search-arrow" src="<?php echo $this->pluginUrl; ?>display/pages/js/dd_arrow_default.png" style="margin:0 !important;"></span>
                        </div>
                        
                        <div id="select_suburb" style="display:none;">
                            <p class="chk-protype"><input type="checkbox" class="checkbox" value="1" id="all_suburb" onClick="check_all_suburb();" <?php if(empty($_GET['suburb']))echo 'checked="checked"';?>> All</p>
                            <?php foreach($suburbs as $suburb){ if(!is_array($suburb)) continue;?>
                            <p class="chk-protype"><input name="suburb[]" type="checkbox" class="checkbox" value="<?php echo ucwords(strtolower($suburb['tag'])); ?>" <?php if(is_array($_GET['suburb'])){ if(in_array(ucwords(strtolower($suburb['tag'])),$_GET['suburb'])) echo 'checked="checked"'; }?> onClick="check_suburb('<?php echo ucwords(strtolower($suburb['tag'])); ?>');"> <?php echo ucwords(strtolower($suburb['tag'])); ?></p>
                            <?php } ?>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
					<?php if($settings['display_mode']=='button_type_state'){ ?>
					<div class="states">
						<select name="state" id="select_state" class="state" onChange="suburb_type(this.value);">
							<option value="">All States</option>
							<?php foreach($states as $state){ if(!is_array($state)) continue;?>
							<option value="<?php echo $state['tag'] ?>"<?php if( $state['tag']==$_GET['state']) echo ' selected="selected"'; ?>><?php echo $state['tag']; ?></option>
							<?php }	?>
						</select>
					</div>				
					<?php } ?>
                    <div class="clear"></div>
                </div>

                <div class="adv-search-option other">	
                    <div class="other-features">
                        <div id="price_for_sale" class="adv-search-item">
                            <p class="p1">
							 <label>Price</label>
                            <select name="price_min" id="cbo_price_min">
                                <option value="">Min Price</option>
                                <?php foreach($price_options as $price_option): 
                                $delimiter = strpos($price_option, ':'); 
                                $value = substr($price_option, 0, $delimiter); ?>
                                <option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_min']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1); //if($price_option ==$max_price) echo '+'; ?></option>
                                <?php endforeach;?>
                            </select>
                            </p>
                            <p class="p2">
                            <select name="price_max" id="cbo_price_max">
                                <option value="">Max Price</option>
                                <?php foreach($price_options as $price_option): 
                                $delimiter = strpos($price_option, ':'); 
                                $value = substr($price_option, 0, $delimiter); ?>
                               <option value="<?php if($price_option ==$max_price)$value.="+";echo $value; ?>"<?php if( $value ==$_GET['price_max']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1); if($price_option ==$max_price) echo '+';  ?></option>	
                                <?php endforeach;?>
                            </select>
                            </p>
                            <div class="clear"></div>
                        </div>
                            
                        <div class="adv-search-item as-rooms">
                            <?php if($list!='commercial'){ ?>
							<p class="p1">
                                <label>Rooms</label>
								<select name="bedrooms" class="bedrooms"  id="cbo_room_bed">
                                    <option value="">Bedrooms</option>
                                    <?php for ( $i = 1 ; $i <= $settings['max_beds']; ++$i ) : ?>
                                    <option value="<?php echo $i ?>"<?php if( $i==$_GET['bedrooms']) echo ' selected="selected"'; ?>><?php echo $i; if ($i == $settings['max_beds']) echo '+';?></option>
                                    <?php endfor;	?>
                                </select>
                            </p>
                            <p class="p2">
								 <select name="bathrooms" class="bathrooms" id="cbo_room_bath">				
                                    <option value="">Bathrooms</option>
                                    <?php 	for ( $i = 1 ; $i <= $settings['max_baths']; ++$i ):	?>
                                    <option value="<?php echo $i ?>"<?php if( $i==$_GET['bathrooms']) echo ' selected="selected"'; ?>><?php echo $i; if ( $i == $settings['max_baths'] ) echo '+'; ?></option>
                                    <?php endfor;?>
                                </select>
                            </p>
							<?php } else { ?>
							<p class="p1">
                                <label>Carspaces</label>
								<select name="carspaces" class="carspaces"  id="cbo_room_car">
                                    <option value="">Any</option>
                                    <?php for ( $i = 1 ; $i <= 10; ++$i ) : ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; if ($i == 10) echo '+';?></option>
                                    <?php endfor;	?>
                                </select>
                            </p>
							<?php } ?>
                            <div class="clear"></div>
                        </div>
    
                        <div class="adv-search-item as-land">
                            <p class="p1">
                                 <label>Land Size</label>
								 <select id="land_min" name="land_min" >
                                    <option value="">Land Min</option>
                                    <?php foreach($size_options as $item): ?>
                                    <option value="<?php echo $item ?>"><?php echo $item. 'sqms'; if($item ==$max_size) echo '+'; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </p>
                            <p class="p2">
								 <select id="land_max" name="land_max" >
                                    <option value="">Land Max</option>
                                    <?php foreach($size_options as $item): ?>
                                    <option value="<?php echo $item; if($item ==$max_size) echo '+'; ?>"><?php echo $item. 'sqms'; if($item ==$max_size) echo '+'; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </p>
                            <div class="clear"></div>
                        </div>					
                    
                        <div class="adv-search-item as-building">
                            <p class="p1">
                               <label>Building Size</label>
							   <select id="building_min" name="building_min">
                                    <option value="">Building Min</option>
                                    <?php foreach($size_options as $item): ?>
                                    <option value="<?php echo $item ?>"><?php echo $item. 'sqms'; if($item ==$max_size) echo '+'; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </p>
                            <p class="p2">
							   <select id="building_max" name="building_max" >
                                    <option value="">Building Max</option>
                                    <?php foreach($size_options as $item): ?>
                                    <option value="<?php echo $item; if($item ==$max_size) echo '+'; ?>"><?php echo $item. 'sqms'; if($item ==$max_size) echo '+'; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </p>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="adv-search-option as-features">
                    <label>Features</label>
                    <div class="adv-search-item">
                        <div  id="feature_list"  onClick="expand_feature();">	
                            <input id='feature' value="All" readonly="true" /> 
                            <span class="span-arrow"><img class="adv-search-arrow" src="<?php echo $this->pluginUrl; ?>display/pages/js/dd_arrow_default.png" style="margin:0 !important;"></span>
                        </div>
                        <div id="select_feature" style="display:none;">
                            <p class="chk-protype"><input type="checkbox" class="checkbox" value="1" id="all_feature" onClick="check_all_feature();" <?php if(empty($_GET['features']))echo 'checked="checked"';?>> All Features</p>
                            <?php foreach($list_features as $item):?>
                            <p class="chk-protype"><input type="checkbox" class="checkbox" name="features[]" value="<?php echo str_replace("\n",'',$item['feature']); ?>" <?php if(is_array($_GET['features'])){ if(in_array($item['feature'],$_GET['features'])) echo 'checked="checked"';}?>  onClick="check_feature('<?php echo str_replace("\n",'',$item['feature']); ?>');"> <?php echo ucwords(strtolower($item['feature'])); ?></p>
                            <?php endforeach; ?>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
				
                <div class="no-label-wrap">
                    <div class="adv-search-option no-label"><input type="text" id="keywords" name="keywords" value="<?php echo ($_GET['keywords']=='')?'Enter property ID, suburb, or street': $_GET['keywords'];?>" onblur="if(this.value=='') this.value='Enter property ID, suburb, or street';" onfocus="if(this.value=='Enter property ID, suburb, or street') this.value='';" size="34"/></div>                        
                    <div class="adv-search-option no-label"><input type="submit" value="Search" class="btn" /></div>
                    <div class="clear"></div>
                </div>
            </div>
		</form>
	</div>
</div>

<script type="text/javascript">
function submit_form(){
	if(jQuery("#keywords").val()=='Enter property ID, suburb, or street' || jQuery("#keywords").val()=='Enter Suburb' )document.getElementById('keywords').value='';
	return true;
}

document.onclick = function (e) {
e = e || event
var target = e.target || e.srcElement
var select_suburb = document.getElementById("select_suburb")
var suburb_list = document.getElementById("suburb_list")
var select_property_type = document.getElementById("select_property_type")
var property_type_list = document.getElementById("property_type_list")
var select_feature = document.getElementById("select_feature")
var feature_list = document.getElementById("feature_list")

do {
	if ( select_suburb == target || suburb_list == target || select_property_type == target || property_type_list == target  || select_feature == target || feature_list == target ) {
		// Click occured inside the box, do nothing.
		return
	}
	target = target.parentNode
} while (target)

// Click was outside the box, hide it.
select_property_type.style.display = "none"
select_feature.style.display = "none"
}

function showtab_ajax(type) {
	var list=jQuery('#select_type').val();	
	var url2 = "<?php echo $this->pluginUrl; ?>display/pages/js/advanced_search_ajax.php?list="+escape( list )+"&type="+escape( type )+"&cat=property_type";	
	jQuery('#select_property_type').load(url2);

	var url = "<?php echo $this->pluginUrl; ?>display/pages/js/advanced_search_ajax.php?type="+escape( type )+"&cat=price";
	jQuery('#price_for_sale').load(url);

	var url1 = "<?php echo $this->pluginUrl; ?>display/pages/js/advanced_search_ajax.php?list="+escape( list )+"&type="+escape( type )+"&cat=suburb";
	jQuery('#select_suburb').load(url1);	
	
	<?php if(!empty($list_features)){ ?>
	var url3 = "<?php echo $this->pluginUrl; ?>display/pages/js/advanced_search_ajax.php?list="+escape( list )+"&type="+escape( type )+"&cat=feature";	
	jQuery('#select_feature').load(url3);
	<?php } ?>

	return false; 
}

function expand_propertyType(){
	jQuery("#select_suburb").hide();
	jQuery("#select_feature").hide(); 
	if(jQuery("#select_property_type").is(":visible") == true )  jQuery("#select_property_type").hide(); 
	else  jQuery("#select_property_type").show(); 
}
	
function check_all_type(){		
	if(eval("document.getElementById('all_type').checked") == true)
	var max = document.getElementsByName('property_type[]').length;				
	for(var idx = 0; idx < max; idx++){document.getElementsByName('property_type[]')[idx].checked = false;}
	document.getElementById('propertyType').value="All";
}

function check_type(value){		
	var flag='0';
	var count='0';
	var temp='';
	if(eval("document.getElementById('all_type').checked") == true)flag='1';	
	var max = document.getElementsByName('property_type[]').length;			
	if(flag=='1'){
		document.getElementById('all_type').checked = false;
		for(var idx = 0; idx < max; idx++){
			if(document.getElementsByName('property_type[]')[idx].value!=value)document.getElementsByName('property_type[]')[idx].checked = false;
			else document.getElementsByName('property_type[]')[idx].checked = true;
		}
	}
	for(var idx = 0; idx < max; idx++){
		if(document.getElementsByName('property_type[]')[idx].checked == true){ count++; temp=document.getElementsByName('property_type[]')[idx].value; }
	}
	if(count=='0') { document.getElementById('propertyType').value="All";document.getElementsByName('all_type').checked = true; }
	if(count=='1')document.getElementById('propertyType').value=temp;
	if(count > 1)document.getElementById('propertyType').value="Multiple";
}

function expand_suburb(){
	jQuery("#select_property_type").hide(); jQuery("#select_feature").hide(); 
	if(jQuery("#select_suburb").is(":visible") == true )  jQuery("#select_suburb").hide(); 
	else  jQuery("#select_suburb").show(); 
}

function check_all_suburb(){		
	if(eval("document.getElementById('all_suburb').checked") == true)
	var max = document.getElementsByName('suburb[]').length;		
	for(var idx = 0; idx < max; idx++){document.getElementsByName('suburb[]')[idx].checked = false;}
	document.getElementById('suburb').value="All";
} 

function check_suburb(value){		
	var flag='0';
	var count='0';
	var temp='';
	var max = document.getElementsByName('suburb[]').length;
	if(eval("document.getElementById('all_suburb').checked") == true)flag='1';					
	if(flag=='1'){
		document.getElementById('all_suburb').checked = false;
		for(var idx = 0; idx < max; idx++){
			if(document.getElementsByName('suburb[]')[idx].value!=value)document.getElementsByName('suburb[]')[idx].checked = false;
			else document.getElementsByName('suburb[]')[idx].checked = true;
		}
	}
	for(var idx = 0; idx < max; idx++){
		if(document.getElementsByName('suburb[]')[idx].checked == true){ count++; temp=document.getElementsByName('suburb[]')[idx].value; }
	}
	if(count=='0') { document.getElementById('suburb').value="All";document.getElementsByName('all_suburb').checked = true; }
	if(count=='1')document.getElementById('suburb').value=temp;
	if(count > 1)document.getElementById('suburb').value="Multiple";
}

function expand_feature(){
	jQuery("#select_property_type").hide(); 
	jQuery("#select_suburb").hide();
	if(jQuery("#select_feature").is(":visible") == true )  jQuery("#select_feature").hide(); 
	else  jQuery("#select_feature").show(); 
}

function check_all_feature(){		
	if(eval("document.getElementById('all_feature').checked") == true)
	var max = document.getElementsByName('features[]').length;		
	for(var idx = 0; idx < max; idx++){document.getElementsByName('features[]')[idx].checked = false;}
	document.getElementById('feature').value="All";
} 

function check_feature(value){		
	var flag='0';
	var count='0';
	var temp='';
	var max = document.getElementsByName('features[]').length;
	if(eval("document.getElementById('all_feature').checked") == true)flag='1';					
	if(flag=='1'){
		document.getElementById('all_feature').checked = false;
		for(var idx = 0; idx < max; idx++){
			if(document.getElementsByName('features[]')[idx].value!=value)document.getElementsByName('features[]')[idx].checked = false;
			else document.getElementsByName('features[]')[idx].checked = true;
		}
	}
	for(var idx = 0; idx < max; idx++){
		if(document.getElementsByName('features[]')[idx].checked == true){ count++; temp=document.getElementsByName('features[]')[idx].value; }
	}
	if(count=='0') { document.getElementById('feature').value="All";document.getElementsByName('all_feature').checked = true; }
	if(count=='1')document.getElementById('feature').value=temp;
	if(count > 1)document.getElementById('feature').value="Multiple";
}
</script>