<?php
// google schema 
if (!empty($realty->property['opentimes'])) {
	foreach($realty->property['opentimes'] as $opentime) { ?>
	<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Event",
		  "name": "<?php echo date('D jS M Y', strtotime($opentime['date'])); ?>",
		  "startDate" : "<?php echo date('Y-m-d', strtotime($opentime['date'])) .'T'. date('g:i', strtotime($opentime['start_time'])) ; ?>",
		  "url" : "<?php echo $realty->property['url']; ?>",
		  "location" :
		  {
			"@type" : "Place",
			"sameAs" : "<?php echo $realty->property['url']; ?>",
			"name" : "<?php echo date('D jS M Y', strtotime($opentime['date'])); ?>",
			"address" :
			{
			  "@type" : "PostalAddress",
			  "streetAddress" : "<?php echo $realty->property['street_address']; ?>",
			  "addressLocality" : "<?php echo $realty->property['suburb']; ?>",
			  "addressRegion" : "<?php echo $realty->property['state']; ?>",
			  "postalCode" : "<?php echo $realty->property['postcode']; ?>"
			}
		  }
		}
	</script>
	<?php
	}
}

if ($realty->property['forthcoming_auction'] == '1') { ?>
	<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Event",
		  "name": "AUC",
		  "startDate" : "<?php echo date('Y-m-d', strtotime($realty->property['auction_date'])) .'T'. date('g:i', strtotime($realty->property['auction_time'])) ; ?>",
		  "url" : "<?php echo $realty->property['url']; ?>",
		  "location" :
		  {
			"@type" : "Place",
			"sameAs" : "<?php echo $realty->property['url']; ?>",
			"name" : "<?php echo $realty->property['auction_place']; ?>",
			"address" :
			{
			  "@type" : "PostalAddress",
			  "streetAddress" : "<?php echo $realty->property['street_address']; ?>",
			  "addressLocality" : "<?php echo $realty->property['suburb']; ?>",
			  "addressRegion" : "<?php echo $realty->property['state']; ?>",
			  "postalCode" : "<?php echo $realty->property['postcode']; ?>"
			}
		  }
		}
	</script>
<?php } ?>