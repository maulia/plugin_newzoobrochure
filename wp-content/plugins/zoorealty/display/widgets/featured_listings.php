<?php
foreach($settings as $type=>$values):
	if(!is_array($values))continue; 
	if($values['Number'] <1) continue; 
	$select="SELECT id, type,(garage + carport + off_street_park) AS carspaces,  bedrooms, show_price as display_sold_price, bathrooms, headline, property_type, display_price as price_display, price, display_price_text as price_display_value, status, display_address, unit_number, street_number, street, state, suburb, description";	
	$current_listings = $realty->properties(array('list' =>$type,'condition'=>"properties.id in (select property_id from extras where featured_property='1')", 'limit'=>$values['Number']), true, $select);
	if($current_listings['results_count'] >0):  ?>
	<div id="featured_<?php echo $type; ?>" class="block">
		<?php echo $before_title; ?><span class="title_current_<?php echo $type;?>"><?php echo $values['Title'];?></span><?php echo $after_title; ?>
		<ul id="featured_listings">
			<?php
			$count=1; 
			foreach ($current_listings as $property):
				if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned	
				$property['street_suburb']=$property['street_address'];
				if(!empty($property['suburb']))$property['street_suburb'].=', '.$property['suburb'];
				$property['full_address']=$property['street_suburb'];
				if(!empty($property['state']))$property['full_address'].=', '.$property['state'];	
				?>
				<li class="feature_photo feature<?php echo $count; ?>">
				<p><a href="<?php echo $property['url']; ?>" title="<?php echo htmlspecialchars($property['street_suburb']); ?>"><img src="<?php echo $property['medium']; ?>"  alt="<?php echo htmlspecialchars($property['street_suburb']); ?>" /></a>
				<?php
					foreach($settings as $key=>$values):
						if(!is_array($values))continue; 
						if(( !$values['display'])  ||empty($property[$key]) || $key =='sale' || $key=='lease' || $key=='Both') continue;
						?>
						<br/><span class="<?php echo $key; ?>"><?php if(!empty($settings[$key]['title'] )) echo $settings[$key]['title'].": ";?><?php if($key=='type'){if(strpos($property[$key],'Sale')!==false)$property[$key]="For Sale"; else $property[$key]="For Lease";} echo $property[$key]; ?></span>
						<?php 				
					endforeach;
					?>
				</p>
				</li>
				<?php
				$count++;
			endforeach;
			?>
		</ul>
		<div class="clearer"></div>
	</div><!-- ends #<?php echo $type; ?> -->
	<?php
	endif; //$current_listings['results_count'] >0): 
endforeach;
?>