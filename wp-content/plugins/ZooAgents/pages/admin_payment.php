<?php
global $helper, $current_user, $realty, $wpdb;
if (!is_user_logged_in()){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }

$condition=$_GET;
if(!current_user_can('administrator')){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'dashboard/listings/'; ?>'; 
	</script>
<?php }

$row=$wpdb->get_results("select created_at, property_id, order_amount from property_payment order by created_at desc", ARRAY_A);
?>
<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/table_sorter.css" type="text/css" media="all" />
<script type="text/javascript">jQuery(function() { jQuery("#properties_table").tablesorter({ widgets: ['zebra'] }); });</script>	

<?php include('menu_admin.php'); ?>

<table class="tablesorter" cellpadding="0" cellspacing="0" id="properties_table">
	<thead>
		<tr class="th">
			<th class="th_date">Date</th>
			<th>Property ID</th>
			<th>Address</th>
			<th>Type</th>
			<th>Amount</th>			
			<th>Agent Name</th>								
		</tr>
	</thead>
	<?php 
	foreach($row as $item): 
	$property=$wpdb->get_results("select street_number, unit_number, street, state, suburb, type, office_id from properties where id=".$item['property_id'], ARRAY_A);
	$property=$property[0];
	if(is_array($property))extract($property);
	$address=trim("$street_number $street $suburb, $state");
	$name=$wpdb->get_var("select name from office where id=".$property['office_id']);
	?>
	<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
		<td class="td_date"><?php echo date("d-m-Y",strtotime($item['created_at'])); ?></td>
		<td><a href="<?php echo $realty->siteUrl.$item['property_id']."/";?>" title="view property" target="_blank"><?php echo $item['property_id']; ?></a></td>
		<td><?php echo $address; ?></td>			
		<td><?php echo $property['type']; ?></td>	
		<td><?php echo "$ ".$item['order_amount']; ?></a></td>
		<td><a href="<?php echo $realty->siteUrl.'dashboard/listings/?office_id='.$property['office_id']; ?>" title="View Agent" target="_blank"><?php echo $name; ?></a></td>
	</tr>
	<?php endforeach; ?>
</table>