<?php
global $zoo_analytics;
$request=$_GET+$_POST;	
$return  = $zoo_analytics->direct_task($request);
$office_id=$request['id'];
$property_id=$request['property_id'];
$offices=$zoo_analytics->get_offices($office_id);
switch( $request["action"] ):
	case 'view_weekly' :
		$properties=$zoo_analytics->get_report($office_id,$_GET['pg'],7);
		$title=$offices[0]['name']."'s Weekly Report";
		include('admin-analytics-report.php');
	break;				
	case 'view_report' :
		$properties=$zoo_analytics->get_dynamic_report($_GET['pg'],$_GET['date_from'],$_GET['date_to']);
		$title="Report";
		if(!empty($_GET['date_from']))$title.=" from ".date("d-m-Y",strtotime($_GET['date_from']));
		if(!empty($_GET['date_to']))$title.=" until ".date("d-m-Y",strtotime($_GET['date_to']));
		include('admin-analytics-report.php');
	break;
	case 'view_summary' :
		$properties=$zoo_analytics->get_report($office_id,$_GET['pg']);
		$title=$offices[0]['name']."'s Summary Report";
		include('admin-analytics-report.php');
	break;
	case 'view_detail' :
		$properties=$zoo_analytics->get_detail($property_id);				
		$total=$zoo_analytics->get_total($property_id);
		extract($zoo_analytics->get_property($property_id));
		$title="$property_id - $address, $suburb Weekly Report";
		include('admin-analytics-detail.php');
	break;
	default:				
		$cron="50  23  *  *  7  curl ".get_option('siteurl').'/wp-content/plugins/ZooAnalytics/cron_job.php';	
		require( 'admin-analytics-list.php');		
	break;
endswitch;	
?>