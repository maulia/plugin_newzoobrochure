<p><label>Number</label> <br><input value="<?php echo $settings['limit']; ?>" name="realty_widget_<?php echo $caption; ?>[limit]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Title]" type="text"></p>
<p><label>Price Range (Criteria: returns current property price + price range or current property price - price range )</label> <br><input value="<?php echo $settings['price_range']; ?>" name="realty_widget_<?php echo $caption; ?>[price_range]" type="text"></p>

<h4>Search listings with the same:</h4>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][type]" type="checkbox"<?php if($settings['display']['type']) echo ' checked="checked"'; ?>> <label>Type</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][suburb]" type="checkbox"<?php if($settings['display']['suburb']) echo ' checked="checked"'; ?>> <label>Suburb</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][property_type]" type="checkbox"<?php if($settings['display']['property_type']) echo ' checked="checked"'; ?>> <label>Property Type</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][bedrooms]" type="checkbox"<?php if($settings['display']['bedrooms']) echo ' checked="checked"'; ?>> <label>Bedrooms</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][bathrooms]" type="checkbox" <?php if($settings['display']['bathrooms']) echo 'checked="checked"'; ?>> <label>Bathrooms</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][carspaces]" type="checkbox"<?php if($settings['display']['carspaces']) echo ' checked="checked"'; ?>> <label>Carspaces</label></p>