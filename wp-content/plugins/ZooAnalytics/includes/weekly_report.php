<h2>Property Performance Report</h2>
Statistics for <?php echo $office_name; ?> - <a href="<?php echo $office_website; ?>"><?php echo $office_website; ?></a><br/>
Generated For: <?php echo $name; ?>  (<?php echo $email; ?>)<br/>
Generated: <?php echo $date; ?><br/>
<table width="100%" cellspacing="0" cellpadding="0" style="margin:0 0 15px; padding:0; font-family:Arial, Helvetica, sans-serif; line-height:normal; color:#444; font-size:12px;">
<?php 
foreach($properties as $property){ 
	$property_id=$property['id'];
	$reports=$this->get_detail($property_id);	
	if(!empty($reports)){
	$total=$this->get_total($property_id);
	extract($this->get_property($property_id));
	?>
	<tr><td class="property-address"><p style="margin:15px 0; color:#000; font-weight:bold;"><?php echo "$property_id - $address, $suburb"; ?></p></td></tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0 0 15px;">
				<tr style="background-color:#003D7D ; color:#fff; font-style:bold;">
					<td style="width:200px; padding:10px 5px; border-bottom:solid 1px #eee;">Date</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Num<br/>Hits</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Num<br/>Emails</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Brochure<br/>Views</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Email to<br/>Friend</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Email to<br/>Self</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Share on<br/>Twitter</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Share on<br/>Facebook</td>
					<td style="width:70px;text-align: center; padding:15px 5px; border-bottom:solid 1px #eee;">Shortlisted</td>
				</tr>
				<?php $i=0;foreach ($reports as $report): if(!is_array($report))continue; ?>
				<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
					<td style="border-bottom:solid 1px #eee;"><?php echo date("l, F d, Y", strtotime($report['visited_at'])); ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['visit']; ?> </td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['enquiry']; ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['brochure']; ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['email_friend']; ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['email_self']; ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['share_on_twitter']; ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['share_on_facebook']; ?></td>
					<td style="text-align: center; padding:5px; border-bottom:solid 1px #eee;"><?php echo $report['favorite']; ?></td>
				</tr>
				<?php $i++; endforeach;  ?>
				<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
					<td style="padding:5px; font-weight:bold; background-color:#eee; color:#000;">TOTAL</td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['visit']=='')?'0':$total['visit']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['enquiry']=='')?'0':$total['enquiry']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['brochure']=='')?'0':$total['brochure']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['email_friend']=='')?'0':$total['email_friend']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['email_self']=='')?'0':$total['email_self']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['share_on_twitter']=='')?'0':$total['share_on_twitter']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['share_on_facebook']=='')?'0':$total['share_on_facebook']; ?></td>
					<td style="text-align: center; padding:5px; font-weight:bold; background-color:#eee;color:#000;"><?php echo ($total['favorite']=='')?'0':$total['favorite']; ?></td>
				</tr>
			</table>
		</td>
	</tr>
<?php }
} ?>
</table>