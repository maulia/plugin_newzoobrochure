<?php 
//metadata: access_level=8

$general_settings = array(
'office_id'=>'',
'agent_id'=>'',
'display_pagination'=>array('above', 'below'),
'number_of_listings_on_each_page'=>'',
'number_of_page_links_on_pagination'=>'',
'by_default_all_sold_properties_are_marked_to_be_displayed'=>1,
'sold_/_leased_pages_same_layout_as_search_results_page'=>1,
'display_sold_listings_for_days'=>'',
'display_leased_listings_for_days'=>'',
'expire_date'=>'',
'walk_score_key'=>'',
'emails_sent_from'=>'',
'bcc_all_mail_to'=>'',
'team_description_chars'=>'',
'team_contact_form'=>array('inline', 'popup'),
'display_team_member_images_as_landscape_on_the_team_member_results_page'=>1,
'display_team_member_images_as_landscape_on_the_team_page'=>1,
'display_search_result_in_thumbnail_format'=>1,
'display_map_in_search_result'=>1,
'not_display_search_result_in_list_format'=>1,
'display_sold_properties_in_search_result'=>1,
'display_leased_properties_in_search_result'=>1,
'display_pdf_button_in_search_result'=>1,
'display_team_tab_on_theme_options'=>1,
'display_featured_listings_tab_on_theme_options'=>1,
);
/* In the plugin context, use echo $realty->settings['general_settings']['bcc_all_mail_to']; */

if(isset($_POST['submit'])):
	$realty->settings['general_settings'] = $_POST['realty_general_settings'];
	update_option('realty_general_settings', $_POST['realty_general_settings']);
	$return = 'Settings Updated.';
elseif(isset($_POST['reset_plugin'])):
	global $wpdb;
	$wpdb->query("DELETE FROM $wpdb->options WHERE `option_name` LIKE 'realty_%'");
	$realty->install();	
	$return = 'Plugin Reset.';
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
$settings = get_option('realty_general_settings');
$realty->element('add_tag_script');
?>
<div class="wrap">
<form name="" method="post" action="" id="elements">
<h2>General</h2> 
<table class="widefat"> 
	<?php $i=0;foreach($general_settings as $general_setting=>$values):?> 
<tbody><tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
	<td><label for="<?php echo $general_setting; ?>"><?php echo $helper->humanize($general_setting); ?></label>
	<?php if($general_setting =='google_maps_key'): ?>(<a href="http://www.google.com/apis/maps/" title="Get your own Google Maps Key " target="_blank">http://www.google.com/apis/maps/</a>)
	<?php elseif($general_setting =='walk_score_key'): ?>
	(<a href="http://www.walkscore.com/tile.shtml" title="Get your Walk Score Key" target="_blank">http://www.walkscore.com/tile.shtml</a>)
	<?php elseif($general_setting =='maximum_display_sold_listings' || $general_setting =='maximum_display_leased_listings'): ?>
	(leave it blank to display all sold/leased listings)
	<?php endif; ?>
	</td> 
	<td>
	<?php if($general_setting == 'office_id' or $general_setting =='agent_id'): ?>
	<?php realty_add_tag_box('realty_general_settings['.$general_setting.'][]', $settings[$general_setting] ); ?>	</td>
	<?php 
	elseif(is_array($values)):
			foreach($values as $value):
	?>	<input type="checkbox" <?php if (is_array($settings[$general_setting]) and in_array($value, $settings[$general_setting])) echo 'checked="checked" '; ?>name="realty_general_settings[<?php echo $general_setting; ?>][]" value="<?php echo $value; ?>"><?php echo $value; 
			endforeach;
	elseif($values == 1): ?>
		<input type="checkbox" <?php if ($settings[$general_setting]) echo 'checked="checked" '; ?>name="realty_general_settings[<?php echo $general_setting; ?>]" value="1">
<?php else: ?>
		<input type="text" name="realty_general_settings[<?php echo $general_setting; ?>]" id="<?php echo $general_setting; ?>" value="<?php echo $settings[$general_setting]; ?>" class="regular-text" <?php if($general_setting == 'google_maps_key' or $general_setting =='walk_score_key' or $general_setting =='bcc_all_mail_to' or $general_setting =='emails_sent_from') echo 'size="100"';?>/>
		<?php
	endif; //is_array($values)
	?>
	</td> 
</tr> 
 <?php $i++;endforeach; ?>
   <tr> 
	<td><label for="default_listing">Default Listings</label></td> 
	<td>
	<select name="realty_general_settings[default_listing]">
		<option value="" <?php if ($settings['default_listing']=='') echo 'selected="selected" '; ?>>All</option>
		<option value="commercial" <?php if ($settings['default_listing']=='commercial') echo 'selected="selected" '; ?>>Commercial</option>
		<option value="projects" <?php if ($settings['default_listing']=='projects') echo 'selected="selected" '; ?>>Projects</option>
		<option value="holiday_lease" <?php if ($settings['default_listing']=='holiday_lease') echo 'selected="selected" '; ?>>Holiday</option>
	</select>

	</td> 
</tr>
  <tr class="alternate"> 
	<td><label for="search_results_display">Default Search Results Display</label></td> 
	<td>
	<select name="realty_general_settings[search_results_display]">
		<option value="thumb" <?php if ($settings['search_results_display']=='thumb') echo 'selected="selected" '; ?>>Thumbnail</option>
		<option value="list" <?php if ($settings['search_results_display']=='list') echo 'selected="selected" '; ?>>List</option>		
		<option value="map" <?php if ($settings['search_results_display']=='map') echo 'selected="selected" '; ?>>Map</option>
	</select>
	</td> 
</tr>
 <tr> 
	<td><label for="sort_listing_by">Sort Listing By</label></td> 
	<td>
	<select name="realty_general_settings[sort_listing_by]">
		<option value="price" <?php if ($settings['sort_listing_by']=='price') echo 'selected="selected" '; ?>>Lowest To Highest</option>
		<option value="price_desc" <?php if ($settings['sort_listing_by']=='price_desc') echo 'selected="selected" '; ?>>Highest To Lowest</option>
		<option value="suburb" <?php if ($settings['sort_listing_by']=='suburb') echo 'selected="selected" '; ?>>Suburb</option>
		<option value="created_at" <?php if ($settings['sort_listing_by']=='created_at') echo 'selected="selected" '; ?>>Most recent Listings</option>
		<option value="updated_at" <?php if ($settings['sort_listing_by']=='updated_at') echo 'selected="selected" '; ?>>Last Edited</option>
	</select>
	<input type="checkbox" value="1" name="realty_general_settings[sort_order]" <?php if (!empty($settings['sort_order'])) echo 'checked="checked" '; ?>> Ascending
	</td> 
</tr>

 <tr>
 <td>WARNING: Clicking this button will restore all settings and widgets created by this plugin to default. To reset the plugin pages, click <a href="?page=Realty/manage/pages/pages.php" title="Pages">here.</a></td>
 <td class="submit"><input onclick="return confirm('Are you sure you want to reset the plugin widgets and settings?\n\'OK\' to confirm, \'Cancel\' to stop.' );" type="submit" class="button-primary" value="<?php _e('Reset Plugin'); ?>" name="reset_plugin" /></td>
 </tr></tbody>
 </table>
 
 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->