<?php 
//metadata: access_level=8
?>
<h2>Slideshow settings for the property page</h2>
<p>Set the options for Thumbgrid and Slideshow Pro. The plugin Grid Manager version 1.6 or above must be installed and active.</p>
<?php if(!class_exists('photo_manager')) return;
$identifier = substr(pathinfo(__FILE__, PATHINFO_BASENAME), 0, -4); //The name of the file is used as unique identifier for options in the plugin.
global $photo_manager;
if(!isset($photo_manager))
	$photo_manager = new photo_manager;

if(!empty($_POST)):
	update_option('realty_'.$identifier, $photo_manager->encode_settings($_POST));
	$return = 'Settings Updated.';
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
$settings = get_option('realty_'. $identifier);

?>
<div class="wrap">
<?php if(isset($photo_manager)): ?>
<form method="post" action="" id="widget-controls">
<?php
	require(ABSPATH . '/'.PLUGINDIR . '/GridManager/admin/ssp_settings.php');
?></form><?php
else:
	echo '<h2>You must install the plugin Grid Manager first.</h2>';
endif;
?>
</div><!-- ends .wrap -->