<?php

function zoosuperapi_insert_people(&$log, $contact, $options)
{
	global $wpdb;

	if (empty($contact['id']))
	{
		$log[] = 'No data transmitted? ID was blank';
		return false;
	}

	$table_cache = zoosuperapi_get_tables();
	
	if (!$table_cache['contacts_exists'] || !$table_cache['contact_assignments_exists'] || !$table_cache['notes_exists'] || !$table_cache['notes_sources_exists'])
	{
		$log[] = 'People not supported';
		return false;
	}

	$columns = $table_cache['contacts_columns'];
	
	if (!empty($contact['deleted_at']))
	{
		$log[] = "Deleting contact {$contact['id']}";

		$wpdb->query("DELETE FROM contacts WHERE id = ".$contact['id']);
		$wpdb->query("DELETE FROM contact_assignments WHERE contact_id = ".$contact['id']);
		$wpdb->query("DELETE FROM notes WHERE contact_id = ".$contact['id']);
		
		return true;
	}

	$existing_contact = $wpdb->get_row('SELECT * FROM contacts WHERE id = '.$contact['id'], ARRAY_A);

	$data = array();
	foreach ($columns as $column)
	{
		if (isset($contact[$column]))
			$data[$column] = $contact[$column];
	}

	if (empty($existing_contact))
	{
		$log[] = 'Person does not exist';

		if (zoosuperapi_insert('contacts', $data) === false)
		{
			$log[] = 'Insert Failed!';
			$log[] = $wpdb->print_error();
			return false;
		}

		$log[] = 'Person created';
	} else {
		$skip_fields = explode(',', $options['skip_fields_contact']);

		$log[] = 'Person does already exist.';
		$log[] = "Last updated at {$existing_contact['updated_at']}, updating to {$data['updated_at']}";

		if (zoosuperapi_update('contacts', $data, array('id' => $contact['id'])) === false)
		{
			$log[] = 'Update Failed!';
			$log[] = $wpdb->print_error();
			return false;
		}

		$log[] = 'Person updated';
	}
	
	if (!empty($table_cache['contact_assignments_exists']))
	{
		$columns = $table_cache['contact_assignments'];

		if (!empty($columns))
		{
			$wpdb->query('DELETE FROM contact_assignments WHERE contact_id = '.$contact['id']);

			if (!empty($contact['assignments']))
			{
				foreach ($contact['specialities'] as $speciality)
				{
					$data = array();
					foreach ($columns as $column)
					{
						if (isset($speciality[$column]))
							$data[$column] = $speciality[$column];
					}

					zoosuperapi_insert('contact_assignments', $data);
				}
			}
		}
	}
	
	if (isset($contact['assignments']))
	{
		$delete = array();
		$existing_assignments = $wpdb->get_results('SELECT id, assigned_to FROM contact_assignments WHERE contact_id = '.$contact['id'], ARRAY_A);

		foreach ($existing_assignments as $assignment)
			$delete[$assignment['id']] = true;

		$log[] = count($existing_assignments).' existing assignments';
		$log[] = count($contact['assignments']).' assignments sent';

		foreach ($contact['assignments'] as $assignment)
		{
			$found = false;

			foreach ($existing_assignments as $existing_assignment)
			{
				if ($existing_assignment['assigned_to'] == $assignment['assigned_to'])
				{
					$found = true;
					$delete[$existing_assignment['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('contact_assignments', $assignment);
			}
		}

		foreach ($delete as $assignment_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM contact_assignments WHERE id = '.$assignment_id);
			}
		}
	} else {
		$wpdb->query('DELETE FROM contact_assignments WHERE contact_id = '.$contact['id']);
	}
	
	if (isset($contact['notes']))
	{
		$delete = array();
		$existing_notes = $wpdb->get_results('SELECT * FROM notes WHERE contact_id = '.$contact['id'], ARRAY_A);

		foreach ($existing_notes as $note)
			$delete[$note['id']] = true;

		$log[] = count($existing_notes).' existing notes';
		$log[] = count($contact['notes']).' notes sent';

		foreach ($contact['notes'] as $note)
		{
			$found = false;

			foreach ($existing_notes as $existing_note)
			{
				if (
					$existing_note['contact_id'] == $note['contact_id'] &&
					$existing_note['property_id'] == $note['property_id'] &&
					$existing_note['agent_id'] == $note['agent_id'] &&
					$existing_note['note_type_id'] == $note['note_type_id'] &&
					$existing_note['source_id'] == $note['source_id'] &&
					$existing_note['address'] == $note['address'] &&
					$existing_note['description'] == $note['description'] &&
					$existing_note['note_date'] == $note['note_date'] &&
					$existing_note['updated_at'] == $note['updated_at']
					)
				{
					$found = true;
					$delete[$existing_note['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('notes', $note);
			}
		}

		foreach ($delete as $note_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM notes WHERE id = '.$note_id);
			}
		}
	} else {
		$wpdb->query('DELETE FROM notes WHERE contact_id = '.$contact['id']);
	}
	
	return true;
}