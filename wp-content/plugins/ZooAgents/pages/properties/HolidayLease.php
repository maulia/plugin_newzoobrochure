<?php
$property_types=array('Alpine', 'Apartment', 'Backpacker-Hostel', 'Bed & Breakfast', 'Campground', 'Caravan-HolidayPark', 'Duplex Semi-detached', 'Executive Rental', 'Farm Stay', 'Flat',  'Hotel', 'House', 'House Boat', 'Motel', 'Lodge','Resort', 'Retreat', 'Rural', 'Self Contained Cottage', 'Serviced Apartment', 'Studio', 'Terrace', 'Townhouse', 'Unit', 'Villa', );
?>

<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div class="content-left-column">
		<input type="hidden" name="type" value="HolidayLease">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div id="address_div" class="block_content">
			<h4>Property Address</h4>
			<table>
				<tr>
					<td class="label"><strong>Country<span class="red"> *</span></strong></td>
					<td>
						<select name="country" id="country" class="full">
						<?php foreach($countries as $country){ ?>
						<option value="<?php echo $country; ?>" <?php if($property['country']==$country)echo 'selected="selected"'; ?>><?php echo $country; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Suburb<span class="red"> *</span></strong></td>
					<td><input onkeyup="reloads_addres(this.value);" type="text" name="suburb" id="suburb" class="full" value="<?php echo $property['suburb']; ?>">
					<div class="suggestionsBoxSuburb" id="suggestionsBoxSuburb" style="display: none;">
						<div class="autoSuggestionsListSuburb" id="autoSuggestionsListSuburb">
							&nbsp;
						</div>
					</div>  
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Region<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="town_village" name="town_village"  value="<?php echo $property['town_village']; ?>">
						<span id="town_village_span"><?php echo $property['town_village']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>State<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="state" name="state"  value="<?php echo $property['state']; ?>">
						<span id="state_span"><?php echo $property['state']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Postcode<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="postcode" name="postcode"  value="<?php echo $property['postcode']; ?>">
						<span id="postcode_span"><?php echo $property['postcode']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label">Unit Number</td>
					<td><input type="text" name="unit_number" class="full"  value="<?php echo $property['unit_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Street Number<span class="red"> *</span></strong></td>
					<td><input type="text" id="street_number" name="street_number" class="full"  value="<?php echo $property['street_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Street Name/Type<span class="red"> *</span></strong></td>
					<td><input type="text" id="street" name="street" class="full"  value="<?php echo $property['street']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Display Address:</td>
					<td>
						<input type="radio" name="display_address" <?php if ($property['display_address']=='1' || !isset($property['display_address'])) echo 'checked="checked"'; ?> value="1">All Address Details
						<span style="margin-left:10px"><input type="radio" name="display_address" <?php if ($property['display_address']=='0') echo 'checked="checked"'; ?> value="0">Suburb Only</span>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="datas_div" class="block_content">
			<h4>Property Data</h4>
			<table>
				<tr>
					<td class="label"><strong>Property Type:<span class="red"> *</span></strong></td>
					<td colspan="2">
						<select name="property_type" class="full" onChange="change_area(this.value)">
						<option value="">Please Select</option>
						<?php foreach($property_types as $property_type){ ?>
							<option value="<?php echo $property_type; ?>" <?php if($property_type==$property['property_type']) echo 'selected="selected"'; ?>><?php echo $property_type; ?></option>
						<?php } ?>
						</select>		
					</td>
				</tr>
				<tr>
					<td class="label">Unique Property ID#:</td>
					<td colspan="2"><input type="text" class="full"  name="unique_id" value="<?php echo $property['unique_id']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Property Headline:<span class="red"> *</span></strong></td>
					<td colspan="2"><input type="text" class="full" name="headline" value="<?php echo $property['headline']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Property Description:<span class="red"> *</span></strong></td>
					<td colspan="2"><textarea name="description" cols="39" rows="15"><?php echo $property['description']; ?></textarea></td>
				</tr>				
				<tr>
					<td class="label">Year built:</td>
					<td colspan="2"><input type="text" class="half" name="year_built"  value="<?php echo $property['year_built']; ?>"></td>
				</tr>
			</table>
		</div>

		<div id="rooms_div" class="block_content">
			<h4>Rooms</h4>
			<table>
				<tr>
					<td class="label" id="bedrooms_field"><?php echo($property['property_type']!='Land')?'<strong>Bedrooms<span class="red"> *</span></strong>':'Bedrooms'; ?></td>
					<td>
						<select name="bedrooms" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['bedrooms'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Max Persons</td>
					<td>
						<select name="maximum_persons" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['maximum_persons'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Half Bedroom:</td>
					<td><input type="checkbox" name="half_bedroom" value="1" <?php if(strpos($property['bedrooms'],'.5')!==false)echo 'checked';?>></td>
				</tr>
				<tr>
					<td class="label" id="bathrooms_field"><?php echo($property['property_type']!='Land')?'<strong>Bathrooms<span class="red"> *</span></strong>':'Bathrooms'; ?></td>
					<td>
						<select name="bathrooms" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['bathrooms'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Half Bathroom:</td>
					<td><input type="checkbox" name="half_bathrooms" value="1" <?php if(strpos($property['bathrooms'],'.5')!==false)echo 'checked';?>></td>
				</tr>
				<tr>
					<td class="label">Carport Spaces:</td>
					<td>
						<select name="carport" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['carport'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Garage Spaces:</td>
					<td>
						<select name="garage" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['garage'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Off Street Spaces:</td>
					<td>
						<select name="off_street_park" class="full">
							<option value="">Please Select</option>
							<?php for($i=1;$i<20;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if(floor($property['off_street_park'])==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="eer_div"class="block_content">
			<h4>Energy Efficiency Rating </h4>
			<table>
				<tr>
					<td class="label">Select Rating:</td>
					<td>
						<select name="energy_efficiency_rating" class="full">
							<option value="">Please Select</option>
							<?php foreach($eer as $i){ ?>
							<option value="<?php echo $i; ?>" <?php if($property['energy_efficiency_rating']==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>

		<div id="areas_div" class="block_content">
			<h4>Land/Block Floor Areas (Whole Numbers Only)</h4>
			<table>
				<tr>
					<td class="label" id="land_area_field"><?php echo ($property['property_type']=='Land' || $property['property_type']=='Farmland')?'<strong>Land Area:<span class="red"> *</span></strong>':'Land Area'; ?></td>
					<td><input type="text" class="half"  name="land_area" value="<?php if(!empty($property['land_area']))echo $property['land_area']; ?>"></td>
					<td>
						<select name="land_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['land_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label" id="floor_area_field"><?php echo($property['property_type']=='Land' || $property['property_type']=='Farmland' || $property['type']!='Commercial')?'Floor Area':'<strong>Floor Area:<span class="red"> *</span></strong>'; ?></td>
					<td><input type="text"  class="half" name="floor_area" value="<?php if(!empty($property['floor_area']))echo $property['floor_area']; ?>"></td>
					<td>
						<select name="floor_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['floor_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Number of Floors/Levels:</td>
					<td colspan="2">
						<select name="number_of_floors">	
							<?php for($i=1;$i<6;$i++){ ?>
							<option value="<?php echo $i; ?>" <?php if($property['number_of_floors']==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>

		<div id="others_div" class="block_content">
			<h4>External Links</h4>
            <table>
				<?php /*<tr>
					<td class="label">Property Url:</td>
					<td><input type="text" class="full"  name="property_url" value="<?php echo $property['property_url']; ?>"></td>
				</tr*/ ?>
				<tr>
					<td class="label">Virtual Tour 1 Url:</td>
					<td><input type="text" class="full"  name="virtual_tour" value="<?php echo $property['virtual_tour']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Virtual Tour 2 Url:</td>
					<td><input type="text" class="full"  name="ext_link_1" value="<?php echo $property['ext_link_1']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Video Url:</td>
					<td><input type="text" class="full"  name="ext_link_2" value="<?php echo $property['ext_link_2']; ?>"></td>
				</tr>
			</table>
		</div>

		<div id="map_div"class="block_content">
			<h4>Mapping Coordinates</h4>
			<p><strong><a class="submit_geocode" href="javascript:submit_geocode();">Set Map Location</a></strong></p>
			<p>
				<strong>Longitude:<span id="longitude_span" class="value"><?php echo ($property['longitude']=='')?'&nbsp':$property['longitude']; ?></span></strong>
				<strong>Latitude:<span id="latitude_span" class="value"><?php echo ($property['latitude']=='')?'&nbsp':$property['latitude']; ?></span></strong>
				<input type="hidden" name="longitude" id="longitude" value="<?php echo $property['longitude']; ?>">
				<input type="hidden" name="latitude" id="latitude" value="<?php echo $property['latitude']; ?>">
			</p>

			<div class="map-wrapper">
				<div id="map_canvas" style="width:370px !important;height:300px !important;"></div>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
				<?php if(!empty($property['latitude'])){ ?>
				<script type="text/javascript">
				jQuery(document).ready(function () {
					load();
					});	
				</script>
				<?php } ?>
			</div>
		</div>

	</div>

	<div class="content-right-column">
		<div id="features_div" class="block_content">
			<h4>Features</h4>
			<fieldset>
				<ul class="three_col">
				<?php if(!empty($features['internal'])){	?>
					<li>
						<label><strong>Internal:</strong></label>
						<ul>
							<?php							foreach($features['internal'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php	}	?>
						</ul>
					</li>
				<?php	}		?>
				</ul>
				<ul class="three_col">
				<?php 	if(!empty($features['external'])){					?>
					<li>
						<label><strong>External:</strong></label>
						<ul>
							<?php							foreach($features['external'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php							}	?>
						</ul>
					</li>
				<?php
				}
				if(!empty($features['security'])){			?>
					<li>
						<label><strong>Security:</strong></label>
						<ul>
							<?php							foreach($features['security'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php							}	?>
						</ul>
					</li>
				<?php
				}
				if(!empty($features['general'])){					?>
					<li>
						<label><strong>General:</strong></label>
						<ul>
							<?php							foreach($features['general'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php							}	?>
						</ul>
					</li>
				<?php				}				?>
				</ul>
				<ul class="three_col">
				<?php 
				if(!empty($features['location'])){					?>
					<li>
						<label><strong>Location:</strong></label>
						<ul>
							<?php							foreach($features['location'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php							}	?>
						</ul>
					</li>
				<?php
				}
				if(!empty($features['lifestyle'])){					?>
					<li>
						<label><strong>Lifestyle:</strong></label>
						<ul>
							<?php							foreach($features['lifestyle'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php							}	?>
						</ul>
					</li>
				<?php
				}
				if(!empty($features['your_features'])){					?>
					<li>
						<label><strong>Your Features:</strong></label>
						<ul>
							<?php							foreach($features['your_features'] as $feature){ ?>
							<li><input type="checkbox" value="<?php echo $feature; ?>" name="features[]" <?php if(is_array($property['features'])){ if(in_array($feature,$property['features']))echo 'checked'; } ?>>
							<label><span><?php echo $feature; ?></span></label></li>
							<?php							}	?>
						</ul>
					</li>
				<?php				}				?>
				</ul>
			</fieldset>
		</div>

        <div id="price_div"  class="block_content">
			<h4>Price</h4>
			<table>
				<tr>
					<td class="label"><strong>Normal Season<span class="red"> *</span></strong></td>
					<td><input type="text" class="half"  name="price" value="<?php if(!empty($property['price']))echo $property['price']; ?>"></td>
					<td>
						<select name="price_period" class="half">
							<option value="">Please Select</option>
							<option value="Nightly" <?php if($property['price_period']=='Nightly')echo 'selected="selected"'; ?> >Nightly</option>
							<option value="Weekly" <?php if($property['price_period']=='Weekly')echo 'selected="selected"'; ?>>Weekly</option>
							<option value="Monthly" <?php if($property['price_period']=='Monthly')echo 'selected="selected"'; ?>>Monthly</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Mid Season</td>
					<td><input type="text" class="half"  name="mid_season_price" value="<?php if(!empty($property['mid_season_price']))echo $property['mid_season_price']; ?>"></td>
					<td>
						<select name="mid_season_period" class="half">
							<option value="">Please Select</option>
							<option value="Nightly" <?php if($property['mid_season_period']=='Nightly')echo 'selected="selected"'; ?> >Nightly</option>
							<option value="Weekly" <?php if($property['mid_season_period']=='Weekly')echo 'selected="selected"'; ?>>Weekly</option>
							<option value="Monthly" <?php if($property['mid_season_period']=='Monthly')echo 'selected="selected"'; ?>>Monthly</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">High Season</td>
					<td><input type="text" class="half"  name="high_season_price" value="<?php if(!empty($property['high_season_price']))echo $property['high_season_price']; ?>"></td>
					<td>
						<select name="high_season_period" class="half">
							<option value="">Please Select</option>
							<option value="Nightly" <?php if($property['high_season_period']=='Nightly')echo 'selected="selected"'; ?> >Nightly</option>
							<option value="Weekly" <?php if($property['high_season_period']=='Weekly')echo 'selected="selected"'; ?>>Weekly</option>
							<option value="Monthly" <?php if($property['high_season_period']=='Monthly')echo 'selected="selected"'; ?>>Monthly</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Peak Season</td>
					<td><input type="text" class="half"  name="peak_season_price" value="<?php if(!empty($property['peak_season_price']))echo $property['peak_season_price']; ?>"></td>
					<td>
						<select name="peak_season_period" class="half">
							<option value="">Please Select</option>
							<option value="Nightly" <?php if($property['peak_season_period']=='Nightly')echo 'selected="selected"'; ?> >Nightly</option>
							<option value="Weekly" <?php if($property['peak_season_period']=='Weekly')echo 'selected="selected"'; ?>>Weekly</option>
							<option value="Monthly" <?php if($property['peak_season_period']=='Monthly')echo 'selected="selected"'; ?>>Monthly</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Bond</td>
					<td colspan="2"><input type="text" class="half"  name="bond" value="<?php if(!empty($property['bond']))echo $property['bond']; ?>"></td>					
				</tr>
				<tr>
					<td class="label">Cleaning Fee</td>
					<td colspan="2"><input type="text" class="half"  name="cleaning_fee" value="<?php if(!empty($property['cleaning_fee']))echo $property['cleaning_fee']; ?>"></td>					
				</tr>
			</table>

			<p><strong>Displaying Price</strong></p>
			<table>
				<tr>
					<td class="label">Display Above Price:</td>
					<td colspan="2"><input type="radio" name="display_price"  <?php if ($property['display_price']=='1' || !isset($property['display_price'])) echo 'checked="checked"'; ?> value="1"></td>	
				</tr>
				<tr>
					<td class="label">Display text in place of Price:</td>
					<td><input type="radio" name="display_price" <?php if ($property['display_price']=='2') echo 'checked="checked"'; ?> value="2"></td>
					<td><input type="text"  class="full display-price"  name="display_price_text" value="<?php echo $property['display_price_text']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Hide Price:</td>
					<td><input type="radio" name="display_price" <?php if ($property['display_price']=='0') echo 'checked="checked"'; ?> value="0"></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		
		<div id="season_div"  class="block_content">
			<h4>Availability </h4>
			<table>
				<tr class="first_cell">
					<td class="label"></td>
					<td>start</td>
					<td>end</td>
					<td>min. stay</td>
					<td></td>
				</tr>
				<tr class="li--41">
					<td class="label"><strong>Available to Lease:</strong></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="normal" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="0" />
						<input class="datepicker normal_rental_date three" type="text" id="normal_start_date" name="rental_season[][start_date]" value="<?php if(!empty($normal_season['start_date']) && $normal_season['start_date']!='0000-00-00')echo date("m/d/Y",strtotime($normal_season['start_date'])); ?>">
					</td>
					<td>
						<input class="datepicker normal_rental_date three" type="text" id="normal_end_date" name="rental_season[][end_date]" value="<?php if(!empty($normal_season['end_date']) && $normal_season['end_date']!='0000-00-00')echo date("m/d/Y",strtotime($normal_season['end_date'])); ?>">
					</td>
					<td colspan="2">
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$normal_season['minimum_stay'])echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="5">[Dates above not included below will be priced as Normal Season]</td>
				</tr>
				
				<!--mid-->
				<tr class="li--42">
					<td class="label"><strong>Mid season:</strong></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="mid" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="1" />
						<input class="datepicker mid_rental_date_1 three" type="text" id="mid_start_date_1" name="rental_season[][start_date]" value="<?php if(!empty($mid_season[0]['start_date']) && $mid_season[0]['start_date']!='0000-00-00')echo date("m/d/Y",strtotime($mid_season[0]['start_date'])); ?>">
					</td>
					<td>
						<input class="datepicker mid_rental_date_1 three" type="text" id="mid_end_date_1" name="rental_season[][end_date]" value="<?php if(!empty($mid_season[0]['end_date']) && $mid_season[0]['end_date']!='0000-00-00')echo date("m/d/Y",strtotime($mid_season[0]['end_date'])); ?>">
					</td>
					<td>
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$mid_season[0]['minimum_stay'])echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
					<td><a href="#" onclick="addRentalSeason(this, 'mid'); return false;">Add Another</a></td>
				</tr>
				<?php for($count=1;$count<$mid_count;$count++){ extract($mid_season[$count]); ?>
				<tr class="li--42" id="mid_<?php echo $position; ?>">
					<td class="label"></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="mid" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="<?php echo $position; ?>" />
						<input class="datepicker mid_rental_date_<?php echo $position; ?> three" type="text" id="mid_start_date_<?php echo $position; ?>" name="rental_season[][start_date]" value="<?php if(!empty($start_date) && $start_date!='0000-00-00')echo date("m/d/Y",strtotime($start_date)); ?>">
					</td>
					<td>
						<input class="datepicker mid_rental_date_<?php echo $position; ?> three" type="text" id="mid_end_date_<?php echo $position; ?>" name="rental_season[][end_date]" value="<?php if(!empty($end_date) && $end_date!='0000-00-00')echo date("m/d/Y",strtotime($end_date)); ?>">
					</td>
					<td>
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$minimum_stay)echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
					<td><a onclick="removeRentalSeason(this); return false;" title="" href="#">Remove</a></td>
				</tr>			
				<?php } ?>
				
				<!--high-->
				<tr class="li--43">
					<td class="label"><strong>High season:</strong></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="high" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="1" />
						<input class="datepicker high_rental_date_1 three" type="text" id="high_start_date_1" name="rental_season[][start_date]" value="<?php if(!empty($high_season[0]['start_date']) && $high_season[0]['start_date']!='0000-00-00')echo date("m/d/Y",strtotime($high_season[0]['start_date'])); ?>">
					</td>
					<td>
						<input class="datepicker high_rental_date_1 three" type="text" id="high_end_date_1" name="rental_season[][end_date]" value="<?php if(!empty($high_season[0]['end_date']) && $high_season[0]['end_date']!='0000-00-00')echo date("m/d/Y",strtotime($high_season[0]['end_date'])); ?>">
					</td>
					<td>
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$high_season[0]['minimum_stay'])echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
					<td><a href="#" onclick="addRentalSeason(this, 'high'); return false;">Add Another</a></td>
				</tr>
				<?php for($count=1;$count<$high_count;$count++){ extract($high_season[$count]); ?>
				<tr class="li--42" id="high_<?php echo $position; ?>">
					<td class="label"></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="high" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="<?php echo $position; ?>" />
						<input class="datepicker high_rental_date_<?php echo $position; ?> three" type="text" id="high_start_date_<?php echo $position; ?>" name="rental_season[][start_date]" value="<?php if(!empty($start_date) && $start_date!='0000-00-00')echo date("m/d/Y",strtotime($start_date)); ?>">
					</td>
					<td>
						<input class="datepicker high_rental_date_<?php echo $position; ?> three" type="text" id="high_end_date_<?php echo $position; ?>" name="rental_season[][end_date]" value="<?php if(!empty($end_date) && $end_date!='0000-00-00')echo date("m/d/Y",strtotime($end_date)); ?>">
					</td>
					<td>
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$minimum_stay)echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
					<td><a onclick="removeRentalSeason(this); return false;" title="" href="#">Remove</a></td>
				</tr>			
				<?php } ?>
				
				<!--peak-->
				<tr class="li--44">
					<td class="label"><strong>Peak season:</strong></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="peak" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="1" />
						<input class="datepicker peak_rental_date_1 three" type="text" id="peak_start_date_1" name="rental_season[][start_date]" value="<?php if(!empty($peak_season[0]['start_date']) && $peak_season[0]['start_date']!='0000-00-00')echo date("m/d/Y",strtotime($peak_season[0]['start_date'])); ?>">
					</td>
					<td>
						<input class="datepicker peak_rental_date_1 three" type="text" id="peak_end_date_1" name="rental_season[][end_date]" value="<?php if(!empty($peak_season[0]['end_date']) && $peak_season[0]['end_date']!='0000-00-00')echo date("m/d/Y",strtotime($peak_season[0]['end_date'])); ?>">
					</td>
					<td>
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$peak_season[0]['minimum_stay'])echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
					<td><a href="#" onclick="addRentalSeason(this, 'peak'); return false;">Add Another</a></td>
				</tr>
				<?php for($count=1;$count<$peak_count;$count++){ extract($peak_season[$count]); ?>
				<tr class="li--42" id="peak_<?php echo $position; ?>">
					<td class="label"></td>
					<td>
						<input id="rental_season__season" name="rental_season[][season]" type="hidden" value="peak" />
						<input id="rental_season__position" name="rental_season[][position]" type="hidden" value="<?php echo $position; ?>" />
						<input class="datepicker peak_rental_date_<?php echo $position; ?> three" type="text" id="peak_start_date_<?php echo $position; ?>" name="rental_season[][start_date]" value="<?php if(!empty($start_date) && $start_date!='0000-00-00')echo date("m/d/Y",strtotime($start_date)); ?>">
					</td>
					<td>
						<input class="datepicker peak_rental_date_<?php echo $position; ?> three" type="text" id="peak_end_date_<?php echo $position; ?>" name="rental_season[][end_date]" value="<?php if(!empty($end_date) && $end_date!='0000-00-00')echo date("m/d/Y",strtotime($end_date)); ?>">
					</td>
					<td>
						<select name="rental_season[][minimum_stay]" id="rental_season__minimum_stay" class="three">
						<?php foreach ($min_stays as $i=>$display){ ?>
							<option value="<?php echo $i; ?>" <?php if($i==$minimum_stay)echo 'selected="selected"'; ?>><?php echo $display; ?></option>
						<?php } ?>
						</select>
					</td>
					<td><a onclick="removeRentalSeason(this); return false;" title="" href="#">Remove</a></td>
				</tr>			
				<?php } ?>
				
			</table>
			
			<div id="availcalendardiv" class="set">
				<input id="unavailable_date" name="unavailable_date" type="hidden" value="<?php echo $property['unavailable_date']; ?>"/>
				<h6 class="full">Availability Calendar</h6>
				<p>Click on Available date to make it unavailable. Double click on Unavailable date to make it available.</p>
				<fieldset class="pf_fs13">
				  <a id="refresh_link" onclick="higlight_in_range();return false;" href="#">Refresh</a>
				  <div id="calendar_date_display" class="inline">
					<input id="e_date" type="hidden" name="e_date" style="display:none;"/>
					<span id="cds_placeholder_50831" style="display: none; position: absolute;"></span>
				  </div>
				</fieldset>
				<br/><br/>
				<div>
				  <fieldset class="pf_fs12">
				  <table class="pf-table" id="season_container">
					<!-- NOTE TO DEVELOPER: All input boxes in this table should be calendars. -->
					<tr>
					  <th class="first_cell" colspan="3" style="text-align:left;"><b>Color Description</b></th>
					</tr>
					<tr>
					  <td style="width:30%;"><div class="color_block avail_desc" style="background-color:cyan;"></div><div>Mid Season</div></td>
					  <td style="width:30%;"><div class="color_block avail_desc" style="background-color:chartreuse;"></div><div>High Season</div></td>
					  <td><div class="color_block avail_desc" style="background-color:HotPink;"></div><div>Peak Season</div></td>
					</tr>
					<tr>
					  <td style="width:30%;"><div class="color_block avail_desc" style="background-color:yellow;"></div><div>Available</div></td>
					  <td><div class="color_block avail_desc" style="background-color:#CCCCCC;"></div><div>Unavailable</div></td>
					</tr>
				  </table>
				  </fieldset>
				</div>
			</div>
		</div>
				
		<div id="users_div" class="block_content newRes">
			<h4>Property Contacts</h4>
			<table>
				<tr>
					<td class="label"><strong>Primary Contact:<span class="red"> *</span></strong></td>
					<td>
						<select name="user_id" class="full">
							<option value="">Please Select</option>
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['user_id']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Secondary Contact:</td>
					<td>
						<select name="secondary_user" class="full">
							<option value="">Please Select</option>	
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['secondary_user']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Third Contact:</td>
					<td>
						<select name="third_user" class="full">
							<option value="">Please Select</option>
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['third_user']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>

	</div>

    <div class="clear"></div>	

    <div class="block_content profile-save"><h4>Save</h4><p class="button"><input type="submit" name="submit" value="Save" class="btn"></p></div>

</form>