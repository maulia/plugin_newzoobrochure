<?php
/*
Plugin Name: Fave Property
Plugin URI: http://www.zooproperty.com/online-store
Description: The Favorite property Plug-in allows you to add your favorite property so you can view it anytime during a session with cookies turned on. Fave property is licensed under the <a href="http://www.zooproperty.com/plugin-license/">Zoo Property License</a>.
Version: 1.1
Author: Agentpoint
Author URI: http://www.agentpoint.com.au

Note:
Added more security on the code
*/

define('WP_DEBUG',true);

if (!class_exists('favourite')) 
{
	class favourite
	{
	
		/*
		 *	PHP 4 Contructor
		 */
		function favourite()
		{
			ob_start();
			$site_url = get_option('siteurl');			
			if ( '/' != $this->site_url[strlen($this->site_url)-1] )$this->site_url .= '/';						
			$plugin_name = 'Favourite';
			$plugin_url  = $site_url . PLUGINDIR . '/' . $plugin_name . '/';
			add_action('template_redirect',array($this,'fav_task'));
		}
		
		function fav_count()
		{
			if(isset($_COOKIE["fav_cookie"]))
			{
				if(strchr($_COOKIE["fav_cookie"],"^"))
				{
					$comp_id = explode("^",$_COOKIE["fav_cookie"]);
					return count($comp_id);
				}
				else
				{
					return 1;
				}
			}
			else return 0;
		}
		
		function fav_string() // Create string for SQL query, does it need numeric checking?
		{
			$id = array();
			if(isset($_COOKIE["fav_cookie"]))
			{
				if(strchr($_COOKIE["fav_cookie"],"^"))
				{
					if($this->check_cookie($_COOKIE["fav_cookie"]))
					{
						$query = str_replace( "^" , "," , $_COOKIE["fav_cookie"] ); //replace character ^ with , to be used in SQL query .
						return $query;
					}
					else return 0;
				}
			else
				{
					if($this->check_cookie($_COOKIE["fav_cookie"]))
					{
						$query = $_COOKIE["fav_cookie"];
						return $query;
					}
					else return 0;
				}
			}
			else return 0;
		}
		
		function save_fav_cookie($data_fav)
		{
			//use 6 param so it can be used in 5.2.0 lower
			$result = setcookie("fav_cookie" ,$data_fav, time()+(60*60*24*42), COOKIEPATH, COOKIE_DOMAIN,0); 
			//$result = setcookie("fav_cookie" ,$data_fav, time()+(60*60*24*42), COOKIEPATH, COOKIE_DOMAIN,0,1); // 5 hours period; 6 weeks = 60*60*24*42
		}
		
		function set_visit_cookie()
		{
			$result = setcookie("fav_visit" ,'true', time()+(60*60*24*42), COOKIEPATH, COOKIE_DOMAIN,0);
		}
		
		function del_visit_cookie()
		{
			$result = setcookie("fav_visit" ,'', time() - 3600, COOKIEPATH, COOKIE_DOMAIN,0);
		}
		
		function insert_prop_id($id)
		{
				if(isset($_COOKIE["fav_cookie"]))
				{

					if(strchr($_COOKIE["fav_cookie"],"^")){
						$data_id = explode("^",$_COOKIE["fav_cookie"]);
						array_push($data_id,$id);
						$new_data_fav = implode("^",$data_id);
					}
					else {
						$new_data_fav = $_COOKIE["fav_cookie"];
						$new_data_fav = $new_data_fav."^".$id;
					}
					$_COOKIE["fav_cookie"] = $new_data_fav;
					$this->save_fav_cookie($new_data_fav);
					return true;
				}
				else if(!isset($_COOKIE["fav_cookie"]))
				{
					$new_data_fav = $id;//."|".$post_id;
					$_COOKIE["fav_cookie"] = $new_data_fav;
					$this->save_fav_cookie($new_data_fav);
					return true;
				}
		}
		
		function delete_prop_id($id)
		{
			$data_id = explode("^",$_COOKIE["fav_cookie"]);
			$key = array_search($id, $data_id);
			$new_data_fav = array_splice($data_id, $key, 1);
			$new_data_fav = implode("^",$data_id);
			$_COOKIE["fav_cookie"]=$new_data_fav;
			$this->save_fav_cookie($new_data_fav);
			return true;
		}
		
		function fav_task()
		{	
			if( isset( $_REQUEST["action"] ) && !empty( $_REQUEST["action"] ) )
			{ 
				$jobs = $_REQUEST["action"];
				$id = $_REQUEST["id"];
				if($jobs == 'add_prop'){
					if($this->check_prop_id($id) == false)
						$this->insert_prop_id($id);
					$this->fav_link($id);				
					die();
				}
				else if($jobs == 'remove_prop'){
					if($this->check_prop_id($id) == true)
						$this->delete_prop_id($id);
					$this->fav_link($id);				
					die();
				}
			}
		}
	
		function check_prop_id($id)
		{
			$exist_post = 0;
			if(isset($_COOKIE["fav_cookie"])){
				if(strchr($_COOKIE["fav_cookie"],"^")){
					$comp_id = explode("^",$_COOKIE["fav_cookie"]);
					$total_fav = count($comp_id);

					for($c=0;$c<=$total_fav;$c++){
						if($id == $comp_id[$c])$exist_post++;
					}
				}
				else {
					$comp_id = $_COOKIE["fav_cookie"];
					if($id == $comp_id)
						$exist_post++;

				}

				if($exist_post > 0)
					return true;
				else 
					return false;
			}
			else return false;
			
		}
		
		function check_cookie($var_check)
		{
			$check = 0;
			if(is_string($var_check))
			{
				if(strchr($var_check,"^"))
				{
					$test_data = explode("^",$var_check);
					//print_r($test_data);
					for($i=0;$i<count($test_data);$i++)
					{
						if(!is_numeric($test_data[$i]))$check++;
					}
				}
				else
				{
					if(!is_numeric($var_check))$check++;
				}
				if($check > 0)return false;
				else return true;
			}
			else return false;
		}
		
		function fav_link($id)
		{
			echo "<div id='add_to_favs'>";
			if($this->check_prop_id($id) == false){
				echo '<p class="button add_favs"><a class="btn" href="javascript:saveProp(\''.get_bloginfo('wpurl').'\',\''.$id.'\');" title="Add to Favourites">Save</a></p>';
			}
			else 
			{
				echo '<p class="button add_remove_favs"><a class="btn" href="javascript:delProp(\''.get_bloginfo('wpurl').'\',\''.$id.'\');" title="Remove Favourites">Remove</a></p>'; // closing </p> removed 07 August 2009
			}

			
			if(!empty($_COOKIE["fav_cookie"]))
			{
				if(!isset($_COOKIE["fav_visit"])){echo '<p class="button view_favs"><a class="btn" id="view_fav" href ="'. get_bloginfo('wpurl') .'/favourite-property/" title="View Favourites">Saved</a></p>';}
				
			}	
			echo "</div>";
		}
		
		
	}//ends class
}



if (class_exists('favourite'))
{
	$favourite = new favourite();
	if(empty($_COOKIE["fav_cookie"]))
		$favourite->del_visit_cookie();

}
?>