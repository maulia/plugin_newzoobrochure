<?php
global $helper, $current_user, $realty, $wpdb;
if (!is_user_logged_in()){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;
if(current_user_can('administrator'))$office_id=$_GET['office_id'];
else $office_id=$wpdb->get_var("select office_id from users where email='".$current_user->user_email."' and contributor=1");

if(!$office_id)return;

if(isset($_POST['submit'])){
	$_POST['name']=$_POST['office_name'];
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'name':case 'email':
					 $return.=$helper->humanize($key)." can not be empty.<br/>"; break;					
				break;
			}
		}		
	}
	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else{
		$_POST['id']=$wpdb->get_var("select max(id) from office");
		if($_POST['id']<100000)$_POST['id']=100000;
		$_POST['id']=$_POST['id']+1;
		$_POST['main_office_id']=$office_id;
		$_POST['created_at']=$_POST['updated_at']=date("Y-m-d h:i:s");
		if($this->insert_form( "office", $_POST )){
			// created office page
			$id=$_POST['id'];
			$short_code="<!--realty_plugin: office_id=$id template:office-->";
			$postID=$wpdb->get_var("select ID from $wpdb->posts where post_content='$short_code'");
			$post_title=($_POST['page_name']=='')?$_POST['name']:$_POST['page_name'];
			
			// if page not exist
			if(!$postID){
				$page_id = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => 'page', 'post_title'=>$post_title, 'post_content'=>$short_code));
				update_post_meta($page_id, '_wp_page_template', 'search_results_page.php');
				$_POST['page_id']= $page_id;
				$wpdb->query("update office set page_id='$page_id', page_name='$post_title' where id=$id");
			}
			?>
			<script type="text/javascript">
				location.href="<?php echo $realty->siteUrl."dashboard/edit-office/?id=$id"; ?>";
			</script>
			<?php
		}
		else $return.= 'Office could not be saved. please try again later';
		echo "<div id='return'>$return</div>";
	}
	$office=$_POST;	
}
?>

<form name="update_properties" id="update_properties" class="form-profile" action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="content-left-column">
		<div class="block_content profile-block">
        	<h4>Office Detail</h4>
			<table>
				<tr>
					<td class="label"><strong>Name<span class="red"> *</span></strong></td>
					<td>
						<input type="text" class="full" id="name" name="office_name"  value="<?php echo $office['name']; ?>">						
					</td>
				</tr>
				<tr>
					<td class="label"><strong>State</strong></td>
					<td>
						<input type="text" class="full" id="state" name="state" value="<?php echo $office['state']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Suburb</strong></td>
					<td>
						<input type="text" class="full" id="suburb" name="suburb"  value="<?php echo $office['suburb']; ?>">
					</td>
				</tr>				
				<tr>
					<td class="label"><strong>Postcode</strong></td>
					<td>
						<input type="text" class="full" id="postcode" name="zipcode"  value="<?php echo $office['zipcode']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Country</strong></td>
					<td>
						<input type="text" class="full" id="country" name="country"  value="<?php echo $office['country']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label">Unit Number</td>
					<td><input type="text" name="unit_number" class="full"  value="<?php echo $office['unit_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Street Number</td>
					<td><input type="text" id="street_number" name="street_number" class="full"  value="<?php echo $office['street_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Street Name/Type</td>
					<td><input type="text" id="street" name="street" class="full"  value="<?php echo $office['street']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Email<span class="red"> *</span></strong></td>
					<td>
						<input type="text" class="full" id="email" name="email"  value="<?php echo $office['email']; ?>">
					</td>
				</tr>
				<tr>
					<td class="label">Phone</td>
					<td><input type="text" name="phone" class="full"  value="<?php echo $office['phone']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Fax</td>
					<td><input type="text" id="fax" name="fax" class="full"  value="<?php echo $office['fax']; ?>"></td>
				</tr>
			</table>
        </div>
	</div>

	<div class="content-right-column">
		<div id="map_div" class="block_content">
			<h4>Mapping Coordinates</h4>
			<p><strong><a class="submit_geocode" href="javascript:submit_geocode();">Set Map Location</a></strong></p>
			<p>
				<strong>Longitude:<span id="longitude_span" class="value"><?php echo ($office['longitude']=='')?'&nbsp':$office['longitude']; ?></span></strong>
				<strong>Latitude:<span id="latitude_span" class="value"><?php echo ($office['latitude']=='')?'&nbsp':$office['latitude']; ?></span></strong>
				<input type="hidden" name="longitude" id="longitude" value="<?php echo $office['longitude']; ?>">
				<input type="hidden" name="latitude" id="latitude" value="<?php echo $office['latitude']; ?>">
			</p>
			<div class="map-wrapper">
                <div id="map_canvas"></div>
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <?php if(!empty($office['latitude'])){ ?>
                    <script type="text/javascript"> jQuery(document).ready(function () { load(); });	</script>
                <?php } ?>
			</div>
		</div>  
		 <div class="block_content profile-save">
			<h4>Save Changes</h4>
			<p class="button"><input type="submit" name="submit" value="Save" class="btn"></p>
		</div>	
	</div>    
    <div class="clear"></div>    	
</form>
<script type="text/javascript">
/* map */
function submit_geocode(){
	var geocoder = new google.maps.Geocoder();
	var street_number=jQuery('#street_number').val();
	var street=jQuery('#street').val();
	var suburb=jQuery('#suburb').val();
	var state=jQuery('#state').val();
	var postcode=jQuery('#postcode').val();				
	var address = "";

	if(street_number!='')address=street_number+" ";
	if(street!='')address=address+street+" ";
	if(suburb!='')address=address+suburb+" ";
	if(state!='')address=address+state+" ";
	if(postcode!='')address=address+postcode+" ";
	address=address.substr(0,address.length-1); 

	geocoder.geocode( { 'address': address}, function(results, status) {

	if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();
		document.getElementById('latitude').value=latitude;
		document.getElementById('latitude_span').innerHTML=latitude;
		document.getElementById('longitude').value=longitude;
		document.getElementById('longitude_span').innerHTML=longitude;
		load();
	}
		
	}); 
}
var myLatLng;
function load() {
	var latitude=jQuery('#latitude').val();		
	var longitude=jQuery('#longitude').val();		
	myLatLng = new google.maps.LatLng(latitude,longitude);
	var myOptions = {
		zoom: 14,
		center:myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};	
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);	
	var Marker = new google.maps.Marker({  position: myLatLng, map: map });
}

</script>