<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];
$type=$_GET['type'];
$office_id=intval($_GET['office_id']);

$property=$wpdb->get_results("select * from properties where id = $id $extra_condition", ARRAY_A);
$property=$property[0];
if(empty($property))return print "Listing ID $id is not exist";

include('menu_listings.php');

if(isset($_POST['submit'])){
	$_POST['updated_at']=date("Y-m-d h:i:s");
	if(!empty($_POST['sold_at']))$_POST['sold_at']=date("Y-m-d", strtotime($_POST['sold_at']));
	if(!empty($_POST['leased_at']))$_POST['leased_at']=date("Y-m-d", strtotime($_POST['leased_at']));
	
	if($this->update_form( "properties", $_POST ))echo "<div id='success'><span>Status successfully updated</span></div>";
	else echo "<div id='return' class='red'>Status could not be updated. please try again later</div>";
	
	$property=$_POST;	
}
?>
<script language="JavaScript" src="<?php echo $this->pluginUrl; ?>js/base_datepicker.js"></script>

<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div id="status_div" class="block_content">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<h4>Property Status</h4>
		<table>
			<tr>				
				<td width="80px"><input type="radio" onClick="javascript:show_sold('0');" name="status" <?php if ($property['status']=='1') echo 'checked="checked"'; ?> value="1">Available</td>
				<td width="80px"><input type="radio" onClick="javascript:show_sold('0');" name="status" <?php if ($property['status']=='4') echo 'checked="checked"'; ?> value="4">Under Offer</td>
				<?php if(strpos($property['type'],'Lease')===false && strtolower($property['deal_type'])!='lease'){ ?>
				<td width="80px"><input type="radio" onClick="javascript:show_sold('1');" name="status" <?php if ($property['status']=='2') echo 'checked="checked"'; ?> value="2">Sold</td>
				<?php } else { ?>
				<td width="80px"><input type="radio" onClick="javascript:show_leased('1');" name="status" <?php if ($property['status']=='6') echo 'checked="checked"'; ?> value="6">Leased</td>
				<?php } ?>
				<td width="80px"><input type="radio" onClick="javascript:show_sold('0');" name="status" <?php if ($property['status']=='5') echo 'checked="checked"'; ?> value="5">Withdrawn</td>
				<td width="80px"><input type="radio" onClick="javascript:show_sold('0');" name="status" <?php if ($property['status']=='3') echo 'checked="checked"'; ?> value="5">Draft</td>
			</tr>			
		</table>
		<table id="sold_detail" <?php if($property['status']!=2)echo'style="display:none;"'; ?>>
			<tr>
				<td>&nbsp;</td>
				<td class="label">Sold Date:</td>
				<td><input type="text" id="sold_at" name="sold_at" class="full" value="<?php if(!empty($property['sold_at']) && $property['sold_at']!='0000-00-00')echo date("d-m-Y",strtotime($property['sold_at'])); ?>"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="label">Sold Amount:</td>
				<td><input type="text" name="sold_price" class="full" value="<?php if(!empty($property['sold_price']))echo $property['sold_price']; ?>"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="label">Display Sold Price:</td>
				<td><input type="checkbox" name="show_price" value="1" <?php if($property['show_price']=='1')echo 'checked';?>></td>
			</tr>
		</table>
		<table id="leased_detail" <?php if($property['status']!=6)echo'style="display:none;"'; ?>>
			<tr>
				<td>&nbsp;</td>
				<td class="label">Leased Date:</td>
				<td><input type="text" name="leased_at" class="full" value="<?php if(!empty($property['leased_at']) && $property['leased_at']!='0000-00-00')echo date("d-m-Y",strtotime($property['leased_at'])); ?>"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="label">Leased Amount:</td>
				<td><input type="text" name="leased_price" class="full" value="<?php if(!empty($property['leased_price']))echo $property['leased_price']; ?>"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="label">Display Sold Price:</td>
				<td><input type="checkbox" name="show_price" value="1" <?php if($property['show_price']=='1')echo 'checked';?>></td>
			</tr>
		</table>
	</div>
	<div class="clearer"></div>
	<div class="block_content">
		<input type="submit" name="submit" value="Save" class="btn" id="submit_btn" >
	</div>
</form>

 

<script type="text/javascript">
jQuery(document).ready(function () {
  jQuery('#sold_at').datepicker({dateFormat:'dd-mm-yy'});    
  jQuery('#leased_at').datepicker({dateFormat:'dd-mm-yy'});    
});	

function show_sold(value){
	if(value=='1'){
		jQuery('#sold_detail').show();
		jQuery('#leased_detail').hide();
	}
	else{	
		jQuery('#sold_detail').hide();
		jQuery('#leased_detail').hide();
	}
}

function show_leased(value){
	if(value=='1'){
		jQuery('#leased_detail').show();
		jQuery('#sold_detail').hide();
	}
	else{
		jQuery('#sold_detail').hide();
		jQuery('#leased_detail').hide();
	}
}
</script>