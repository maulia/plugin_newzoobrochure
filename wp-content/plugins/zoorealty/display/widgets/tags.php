<? function convert_price($price_option){
if(!is_array($price_option))
{
	$price = 'price ';
}
else{
	$price="(CASE";
	foreach($price_option as $price_value){
		$new_price_value=format_price(trim($price_value));
		if(strpos($price_value,'<')!==false || strpos($price_value,'>')!==false)$price.= " WHEN price ".$price_value." then '".$new_price_value."'";
		if(strpos($price_value,'-')!==false){
			$prices=explode("-",$price_value);
			$price_value= '>='.$prices[0].' AND ';
			$price_value.= 'price <='.$prices[1];			
		$price.= " WHEN price ".$price_value." then '".$new_price_value."'";
		}
	}
	$price.=" END)AS tag";
}
return $price;
}
function format_price($price_value){
if(strpos($price_value,"-")!==false){
	$prices=explode("-",$price_value);
	$new_prices= array();
	foreach($prices as $value):
		if(strpos($price_value,'000000')!==false)$new_price = '$'.substr($value,0,-6).'M';
		else $new_price = '$'.substr($value,0,-3).'K';
		array_push($new_prices,$new_price);
	endforeach;
	$new_price_value = implode ("-",$new_prices);
}
else{
if(strpos($price_value,"<")!==false)$new_price_value=str_replace("<","<$",$price_value);
if(strpos($price_value,">")!==false)$new_price_value=str_replace(">",">$",$price_value);
if(strpos($price_value,">")===false && strpos($price_value,"<")===false)$new_price_value="$".$price_value;
if(strpos($price_value,'000000')!==false)$new_price_value = substr($new_price_value,0,-6).'M';
else $new_price_value = substr($new_price_value,0,-3).'K';
}
return $new_price_value;
}

function all_url($tag){
$url=urldecode($_SERVER['QUERY_STRING']);
$tags=$_GET[$tag];
if(!empty($tags)):
foreach($tags as $tagz):
	$url=str_replace($tagz,"",$url);
	$url=str_replace('&'.$tag.'[]=',"",$url);
	$url=str_replace('?'.$tag.'[]=',"",$url);
	$url=str_replace($tag.'[]=',"",$url);
	$url=str_replace('&enchanced[]=',"",$url);
	$url=str_replace('?enchanced[]=',"",$url);
	$url=str_replace('enchanced[]=',"",$url);
endforeach;
endif;
return '?'.$url;
}

function get_status($new_day){
if(empty($new_day))$new_day='7';
$status=" (CASE WHEN status IN(1,4) and DATEDIFF(CURDATE(),`created_at`) > $new_day THEN 'Current'
WHEN status = '2' THEN 'Sold'
WHEN status IN(1,4) and DATEDIFF(CURDATE(),`created_at`) <= $new_day THEN 'New'
END) as tag ";
return $status;
}


?>
<?php 
$request = $_GET;
global $helper;
$url = (is_page('home') or is_home())? $realty->get_permalink('sales'): $realty->get_permalink();
$type = (strpos($realty->currentPage, 'lease') !== false)? 'lease': 'sale';	
if(!empty($settings['price']) && !empty($settings['price_option']))$price_condition=convert_price($settings['price_option']);
if(!empty($settings['status']))$status_condition=get_status($settings['new_day'])
?>
<?php foreach($settings as $tag=>$title):
if(empty($title) || $tag=='price_option' || $tag=='price' || $tag=='status' || $tag=='new_day') continue;//Leave the tags you don't want displayed blank in the widget admin interface.
?>
<div id="cloud_<?php echo $tag; ?>" class="cloud">
<?php echo $before_title . $title . $after_title; ?>	
<?php 
$array_price=array();
$array_price=array('list'=>$type, 'order'=>'price', 'group'=>'tag', 'order_direction'=>'ASC', 'limit'=>50, 'page'=>'1');
if($realty->settings['general_settings']['not_displaying_sold_and_leased_in_search_result']=='1')$array_price['status']='1,4';
$tags = $realty->property_search($tag,'');
if(!empty($settings['price']) && !empty($settings['price_option']))$prices = $realty->properties($array_price, true, "SELECT $price_condition, COUNT(*) AS number_of_properties");
if(!empty($settings['status']))$status = $realty->properties($array_price, true, "SELECT $status_condition, COUNT(*) AS number_of_properties");
?>

<div class="block_content">
<p class="all_tags"><a href="<?php echo $url . $helper->sanitizeUrl($tag . '[]=all'); ?>" <?php if(!isset($request[$tag] )) echo 'class="selected"'; ?>>All <span class="tag_count">(<?php echo $tags['results_count']; ?>)</span></a></p>
<?php foreach($tags as $items):
	if($items['number_of_properties'] < 1 || !is_array($items)) continue; //Only print items with more than one property
?>
<p class="each_tag"><a href="<?php echo $url . $helper->sanitizeUrl($tag . '[]=' . $items['tag']);?>&amp;enchanced[]="<?php if(is_array($request[$tag]) && in_array( $items['tag'], $request[$tag]) ) echo ' class="enhanced_active"'; ?> title="<?php echo ($items['number_of_properties'] >1)? $items['number_of_properties'] . ' properties': $items['number_of_properties'] . ' property'; ?>"><span<?php if(is_array($request[$tag]) && in_array( $items['tag'], $request[$tag]) ) echo ' class="enhanced_active"'; ?>><?php echo $items['tag'];
	if($tag == 'bedrooms' || $tag =='bathrooms')
		echo ($items['tag'] >1)? " $tag": " " . substr($tag, 0, -1);
	echo "<span class=\"tag_count\">(" . $items['number_of_properties'] . ")</span>"; ?></span></a></p>
<?php endforeach; ?>
</div><!-- end .cloud_content -->
<div class="clearer"></div>
</div><!-- end #cloud_<?php echo $tag; ?> -->
<?php endforeach; ?>

<?php if(!empty($settings['status'])): ?>
<div id="cloud_price" class="cloud"><?php echo $before_title . $settings['status'] . $after_title; ?>	
<div class="block_content">
<p class="all_tags"><a href="<?php echo all_url('status'); ?>"<?php if(!isset($request['status'] )) echo ' class="selected"'; ?>>All <span class="tag_count">(<?php echo $status['results_count']; ?>)</span></a></p>
<?php foreach($status as $items):
	if($items['number_of_properties'] < 1 || !is_array($items)) continue; //Only print items with more than one property
?>
<p class="each_tag"><a href="<?php echo $url . $helper->sanitizeUrl('status' . '[]=' . $items['tag']);?>&enchanced[]="<?php if(is_array($request['status']) && in_array( $items['tag'], $request['status']) ) echo 'class=" enhanced_active"'; ?> title="<?php echo ($items['number_of_properties'] >1)? $items['number_of_properties'] . ' properties': $items['number_of_properties'] . ' property'; ?>"><span <?php if(is_array($request['status']) && in_array( $items['tag'], $request['status']) ) echo ' class="enhanced_active"'; ?>><?php echo $items['tag'];echo "<span class=\"tag_count\">(" . $items['number_of_properties'] . ")</span>"; ?></span></a></p>
<?php endforeach; ?>
</div><!-- end .cloud_content -->
<div class="clearer"></div>
</div><!-- end #cloud_price -->
<?php endif; ?>


<?php if(!empty($settings['price'])): ?>
<div id="cloud_price" class="cloud"><?php echo $before_title . $settings['price'] . $after_title; ?>	
<div class="block_content">
<p class="all_tags"><a href="<?php echo all_url('price'); ?>"<?php if(!isset($request['price'] )) echo ' class="selected"'; ?>>All <span class="tag_count">(<?php echo $prices['results_count']; ?>)</span></a></p>
<?php foreach($prices as $items):
	if($items['number_of_properties'] < 1 || !is_array($items)) continue; //Only print items with more than one property
?>
<p class="each_tag"><a href="<?php echo $url . $helper->sanitizeUrl('price' . '[]=' . $items['tag']);?>&enchanced[]="<?php if(is_array($request['price']) && in_array( $items['tag'], $request['price']) ) echo 'class=" enhanced_active"'; ?> title="<?php echo ($items['number_of_properties'] >1)? $items['number_of_properties'] . ' properties': $items['number_of_properties'] . ' property'; ?>"><span<?php if(is_array($request['price']) && in_array( $items['tag'], $request['price']) ) echo ' class="enhanced_active"'; ?>><?php echo $items['tag'];echo "<span class=\"tag_count\">(" . $items['number_of_properties'] . ")</span>"; ?></span></a></p>
<?php endforeach; ?>
</div><!-- end .cloud_content -->
<div class="clearer"></div>
</div><!-- end #cloud_price -->
<?php endif; ?>
<p class="button cloud_btn"><a href="<?php echo $url; ?>" class="btn">Reset Selection</a></p>