<?php

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments">This post is password protected. Enter the password to view comments.</p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->

<?php if ( have_comments() ) : ?>
<div id="comments">
	<h4><?php comments_number('No Responses', 'One Response', '% Responses' );?> to &#8220;<?php the_title(); ?>&#8221;</h4>

	<ul class="commentlist">
		<?php wp_list_comments('type=comment&callback=mytheme_comment'); ?>
	</ul>

	<div class="navigation">
		<p class="alignleft"><?php previous_comments_link() ?></p>
		<p class="alignright"><?php next_comments_link() ?></p>
	</div>
</div>
<?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments">Comments are closed.</p>

	<?php endif; ?>
<?php endif; ?>
<?php if ( ! empty($comments_by_type['pings']) ) : ?>
<div id="trackbacks">
	<h4>Trackbacks &amp; Pingbacks</h4>
	<ul id="pings" class="commentlist">
		<?php wp_list_comments('type=pings&callback=mytheme_comment'); ?>
	</ul>
</div>
<?php endif; ?>

<?php if ( comments_open() ) : ?>

<div id="commentform-container">

<h4><?php comment_form_title( 'Leave a Reply', 'Leave a Reply to %s' ); ?></h4>

<div class="cancel-comment-reply">
	<small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p>You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>">logged in</a> to post a comment.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( is_user_logged_in() ) : ?>

<p class="logged">Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>

<?php else : ?>

<p>
	<label for="author">Name <?php if ($req) echo "*"; ?></label>
	<input type="text" name="author" id="author" class="textbox" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
</p>

<p>
	<label for="email">Mail <!--(will not be published) --> <?php if ($req) echo "*"; ?></label>
	<input type="text" name="email" id="email" class="textbox" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
</p>

<p>
	<label for="url">Website</label>
	<input type="text" name="url" id="url" class="textbox" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
</p>

<?php endif; ?>

<!--<p><small><strong>XHTML:</strong> You can use these tags: <code><?php echo allowed_tags(); ?></code></small></p>-->

<p>
	<label for="message">Message <?php if ($req) echo "*"; ?></label>
	<textarea name="comment" id="comment" cols="50" rows="10" tabindex="4"></textarea>
</p>

<p class="button"><input name="submit" type="submit" id="submit" class="btn" tabindex="5" value="Submit" />
<?php comment_id_fields(); ?>
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>
