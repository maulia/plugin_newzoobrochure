<?php
function scripts_tools_init() {
    if (!is_admin()) { ?>
    	
        <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('stylesheet_url'); ?>?v=1.01" />

		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/ddsmoothmenu.js?v=0.01"></script>
        <script type="text/javascript">
			ddsmoothmenu.init({mainmenuid: "smoothmenu", orientation: 'h', classname: 'ddsmoothmenu', contentsource: "markup"})
			jQuery(document).ready(function(){
				jQuery('ul li:last-child').addClass('last');
			});
        </script>
        
        <?php if (is_page('home')) { ?>
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.cycle.all.js"></script>
        <script type="text/javascript">jQuery(document).ready(function() {jQuery('.slideshow').cycle({fx: 'fade', slideResize: 0});});</script>
        <?php } ?>
        
<?php
	} 
}
add_action('wp_head', 'scripts_tools_init');

function body_css_id() {
	global $post, $wpdb;
	if (is_404()) { echo "fourOfour"; } 
	elseif ( is_home() ) { echo "home"; }
	elseif ( is_home() || is_category() || is_archive() || is_search() || is_single() || is_date() ) { echo "blog"; } 
	elseif (is_page()) {
		$parent_name = $wpdb->get_var("SELECT post_name FROM $wpdb->posts WHERE ID = '$post->post_parent;'");
		echo $post->post_name;
	} else { echo 'notes'; }
}

/*function image_attributes($image_source, $max_width, $max_height, $limit) {
	global $wpdb;
	if(strpos($image_source,"wp-content")===false)return '0px';
	
	/* load data from db */
	/*$image_array=explode("wp-content",$image_source);
	$image_source=ABSPATH."wp-content".$image_array[1];	
	$margin_top=$wpdb->get_var("select margin_top from images_detail where url='$image_source'");
	if(!empty($margin_top))return $margin_top;
	
	$image_array=explode("wp-content",$image_source);
	$image_source=ABSPATH."wp-content".$image_array[1];	
	list($width, $height, $type, $attr) = getimagesize($image_source);

	if ($width && $height) {
		$image_ratio = $width / $max_width;
		$image_height = floor($height / $image_ratio);

		$image_margin_top = '0px';
		if ($image_height > $max_height) {
			$image_margin_top = '-' . floor(($image_height - $max_height) / 2) . 'px';
		}
		
		$image_class = 'landscape';
		if ($image_height > ($max_height + $limit)) {
			$image_class = 'portrait';
		}
		
		$image_attributes = array($image_margin_top, $image_class);
		$wpdb->query("insert into images_detail values('','$image_source','$width','$height','$image_class','$image_margin_top')");
	} else {
		$image_attributes = array('0px', 'noImage');
	}
	
	return $image_attributes;
}

function image_ratio($image_source, $max_width, $max_height) {	
	global $wpdb;
	if(strpos($image_source,"wp-content")===false)return '0px';
	
	/* load data from db */
	/*$image_array=explode("wp-content",$image_source);
	$image_source=ABSPATH."wp-content".$image_array[1];	
	$margin_top=$wpdb->get_var("select margin_top from images_detail where url='$image_source'");
	if(!empty($margin_top))return $margin_top;
	
	$image_array=explode("wp-content",$image_source);
	$image_source=ABSPATH."wp-content".$image_array[1];	
	list($width, $height, $type, $attr) = getimagesize($image_source);
	$img_ratio = $width / $max_width;
	$img_height = ceil($height / $img_ratio);
	$margin_top = '0px';
	
	if (($width > $max_width) && ($img_height > $max_height)) {
		$margin_top = '-' . round(($img_height - $max_height) / 2) . 'px';
	} elseif(($width < $max_width) && ($height > $max_height)) {
		$margin_top = '-' . round(($height - $max_height) / 2) . 'px';
	}
	
	$image_class = 'landscape';
	if ($image_height > ($max_height + $limit)) {
		$image_class = 'portrait';
	}
	
	$wpdb->query("insert into images_detail values('','$image_source','$width','$height','$image_class','$margin_top')");
	return $margin_top;
}

function image_viewtype($image_source, $max_width, $max_height, $limit) {
	global $wpdb;
	if(strpos($image_source,"wp-content")===false)return 'landscape';
	
	/* load data from db */
	/*$image_array=explode("wp-content",$image_source);
	$image_source=ABSPATH."wp-content".$image_array[1];	
	$image_class=$wpdb->get_var("select dimension from images_detail where url='$image_source'");
	if(!empty($image_class))return $image_class;	
	
	list($width, $height, $type, $attr) = getimagesize($image_source);
	$img_ratio = $width / $max_width;
	$img_height = ceil($height / $img_ratio);
	$margin_top = '0px';
	
	if (($width > $max_width) && ($img_height > $max_height)) {
		$margin_top = '-' . round(($img_height - $max_height) / 2) . 'px';
	} elseif(($width < $max_width) && ($height > $max_height)) {
		$margin_top = '-' . round(($height - $max_height) / 2) . 'px';
	}
	
	$image_class = 'landscape';
	if ($image_height > ($max_height + $limit)) {
		$image_class = 'portrait';
	}
	
	$wpdb->query("insert into images_detail values('','$image_source','$width','$height','$image_class','$margin_top')");
	return $image_class;
}*/

function image_attributes($width, $height, $max_width, $max_height, $limit) {
	if(empty($width) || empty($height))return array('0px', 'landscape');

	$image_ratio = $width / $max_width;
	$img_height = floor($height / $image_ratio);

	$image_margin_top = '0px';
	if ($img_height > $max_height) {
		$image_margin_top = '-' . floor(($img_height - $max_height) / 2) . 'px';
	}
	
	$image_class = 'landscape';
	if ($img_height > ($max_height + $limit)) {
		$image_class = 'portrait';
	}
	
	$image_attributes = array($image_margin_top, $image_class);
	
	return $image_attributes;
}

function image_ratio($width, $height, $max_width, $max_height) {	
	if(empty($width) || empty ($height))return '0px';
	
	$img_ratio = $width / $max_width;
	$img_height = ceil($height / $img_ratio);
	$margin_top = '0px';
	
	if (($width > $max_width) && ($img_height > $max_height)) {
		$margin_top = '-' . round(($img_height - $max_height) / 2) . 'px';
	} elseif(($width < $max_width) && ($height > $max_height)) {
		$margin_top = '-' . round(($height - $max_height) / 2) . 'px';
	}

	return $margin_top;
}

function image_viewtype($width, $height, $max_width, $max_height, $limit) {
	if(empty($width) || empty ($height))return 'landscape';
	
	$img_ratio = $width / $max_width;
	$img_height = ceil($height / $img_ratio);	

	$image_class = 'landscape';
	if ($img_height > ($max_height + $limit)) {
		$image_class = 'portrait';
	}
	
	return $image_class;
}

?>