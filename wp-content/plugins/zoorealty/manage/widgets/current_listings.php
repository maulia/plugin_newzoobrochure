<h4>Sales</h4>
	
<p><label>Number</label> <br><input value="<?php echo $settings['sale']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[sale][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['sale']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[sale][Title]" type="text"></p>


<h4>lease</h4>

<p><label>Number</label> <br><input value="<?php echo $settings['lease']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[lease][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['lease']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[lease][Title]" type="text"></p>


<h4>Both</h4>

<p><label>Number</label> <br><input value="<?php echo $settings['Both']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[Both][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['Both']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Both][Title]" type="text"></p>

<?php wp_print_scripts('jquery'); ?>
<h4>Information to Display</h4>
<?php 
$items = array (
  'suburb', 'property_type','price', 'type', 'bedrooms', 'bathrooms','carspaces');
global $helper;
foreach ($items as $key=>$item):
if(is_array($item)): ?>
<p><input value="<?php echo (empty($settings[$key]['title']))? $helper->humanize($key):$settings[$key]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $key; ?>][title]" type="text">
<?php foreach ($item as $subitem): ?>
<br /><input value="<?php echo $subitem; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $key; ?>][display][]" type="checkbox"<?php if(is_array($settings[$key]['display']) and in_array($subitem, $settings[$key]['display'])) echo ' checked="checked"'; ?>><label> <?php echo $helper->humanize($subitem); ?></label> 
<?php endforeach;
else: ?>
<label><?php echo (empty($settings[$item]['title']) || $settings[$item]['title'] =="1")? $helper->humanize($item):$settings[$item]['title']; ?></label>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][display]" type="checkbox"<?php if($settings[$item]['display']) echo ' checked="checked"'; ?>> 
<input value="<?php echo $settings[$item]['title']; ?>" name="realty_widget_<?php echo $caption; ?>[<?php echo $item; ?>][title]" type="text"></p>
<?php endif; endforeach; ?>