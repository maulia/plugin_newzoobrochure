<?php

/*Version 1.0 */
class utilities extends form_handler{

	function create_csvold($rows, $end_of_field =';', $pre_field='"',$post_field='"'){
		foreach($rows as $row):
			if(is_array($row))
				foreach($row as $column)
						$contents .=  $pre_field . $column . $post_field . $end_of_field;
			else
				$contents .= $pre_field . $row . $pre_field . $end_of_field;
		endforeach;
		return substr($contents,0,-1);	
	}
	
	function create_csv($rows, $end_of_line =';', $pre_value='"',$post_value='"'){
		foreach($rows as $row)		
			if(is_array($row)) // A double-dimensional array. Usually an array of rows of an SQL statement, with each value being an array of fileds from each column in that row.
				$contents .= substr($this->create_csv($row,$end_of_line, $pre_value,$post_value),0,-strlen($post_value)) . $end_of_line;
			else //Just one array of values to be enclosed
				$contents .= $pre_value . $row . $post_value;		
		return $contents;
	}
	function print_csv($output, $filename){
			header('Content-type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'. $filename . '"');		
			echo $output;
			die();
	}
	
	function encrypt($string, $key) {
		$result = '';
		for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)+ord($keychar));
		$result.=$char;
		}
	
		return base64_encode($result);
	}
	
	function decrypt($string, $key) {
		$result = '';
		$string = base64_decode($string);
		
		for($i=0; $i<strlen($string); $i++) {
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)-ord($keychar));
		$result.=$char;
		}
	
		return $result;
	}
	function email($to,$from,$message,$subject){
		//The bcc is the email of the blog administrator
			$headers =  "From: " . $from . "\n" .
			"bcc: " . get_option('admin_email') . "\n" .
			"Content-Type: text/html\n";
			return @wp_mail($to, $subject, $message,$headers);
	
	}//ends function
	/*
	function email($to,$from,$message,$subject){

	
			$notice_text = "This is a multi-part message in MIME format.";
			$plain_text = strip_tags($message, '');
			
			$semi_rand = md5(time());
			$mime_boundary = "==MULTIPART_BOUNDARY_$semi_rand";
			$mime_boundary_header = chr(34) . $mime_boundary . chr(34);
			
			//The bcc is the email of the blog administrator
			$bcc = get_option('admin_email');


$body = "$notice_text

--$mime_boundary
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit

$plain_text

--$mime_boundary
Content-Type: text/html; charset=us-ascii
Content-Transfer-Encoding: 7bit

$message

--$mime_boundary--";

$headers =  "From: " . $from . "\n" .
    "bcc: " . $bcc . "\n" .
    "MIME-Version: 1.0\n" .
    "Content-Type: multipart/alternative;\n" .
    "     boundary=" . $mime_boundary_header;
	return @wp_mail($to, $subject, $body,$headers);
	
	}//ends function
	*/
	function check_email_address($email) {
		
		/*Many thanks to http://www.ilovejackdaniels.com/php/email-address-validation/ 
		 for this function 
		*/
		 // First, we check that there's one @ symbol, and that the lengths are right
		 if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))	// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		 // Split it into sections to make life easier
		 $email_array = explode("@", $email);
		 $local_array = explode(".", $email_array[0]);
		 for ($i = 0; $i < sizeof($local_array); $i++):
			 if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i]))
				return false;		
		 endfor;
		 if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) : // Check if domain is IP. If not, it should be valid domain name
			 $domain_array = explode(".", $email_array[1]);
			 if (sizeof($domain_array) < 2) 
				return false; // Not enough parts to domain
			
			 for ($i = 0; $i < sizeof($domain_array); $i++):
				 if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i]))
					return false;	 		 
			 endfor;
		 endif;
		 return true;
	}//ends function


}

?>