<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');
global $helper, $realty, $wpdb;
$id=$_GET['id'];
$office_id=$_GET['office_id'];
$photos=$realty->photos($id,'photos');
if(!empty($photos)){ 
	foreach($photos as $key=>$photo):
	$ids=$wpdb->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='photo'", ARRAY_A);
	$ids=$ids[0]['ids'];
	?>
	<li>			
		<input type="hidden" name="photo_ids[]" value="<?php echo $ids; ?>">
		<a href="<?php echo $photo['large']; ?>" rel="prettyPhoto[photo]"><img src="<?php echo $photo['small']; ?>"/></a>
		<a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=photo&office_id=$office_id&task=delete&id=$id&ids=$ids";?>" title="Delete Image"></a>
	</li>
	<?php endforeach; 
}
?>
<script type="text/javascript">
jQuery(function() {
	jQuery(".image_list").sortable({});
	jQuery(".image_list: a[rel^='prettyPhoto']").prettyPhoto({allow_resize: false,deeplinking: false,social_tools: false});		
});
</script>