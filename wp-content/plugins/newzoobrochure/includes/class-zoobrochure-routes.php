<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * Setting zoobrochure for admin page
 * 
 * @author Jony <jony@agentpoint.com.au>
 * @package zoobrochure
 * @subpackage zoobrochure_routes
 * @since 2.0.0
 */
class zoobrochure_routes {

    private static $instance = null;
    public $prefix = 'zoobrochure_template_';

    /**
     * Constructor routes class
     * Add hook for redirect brochure url
     * 
     * @since 2.0.0
     * @return void
     */
    public function __construct() {
        add_action('init', array($this, 'init'));
        add_action('init', array($this, 'set_rewrite_wp_rules'));
        add_filter('redirect_canonical', array($this, 'redirect_canonical'), 10, 2);
        add_action('template_redirect', array($this, 'display_brochure'));
    }

    /**
     * Main zoobrochure_routes Instance.
     *
     * Ensures only one instance of zoobrochure_routes is loaded or can be loaded.
     *
     * @since 2.0.0
     * @static
     * @return object zoobrochure_routes Main instance
     */
    public static function instance() {
        if (null == self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Initialize & prepage page for brochure printing
     * 
     * @since 2.0.0
     * @return void
     */
    public function init() {
        $page = get_page_by_path('brochure');
        if (!$page) {
            $postarr = array(
                'post_title' => 'Brochure',
                'post_status' => 'publish',
                'post_type' => 'page',
                'post_content' => 'Just place holder...'
            );
            wp_insert_post($postarr);
        }
    }

    /**
     * Set Up rewrite rules for pretty permalink
     * 
     * @since 2.0.0
     * @return void
     */
    public function set_rewrite_wp_rules() {
        $page = get_page_by_path('brochure');
        add_rewrite_tag('%zoobrochure_action%', '([a-z]+)');
        add_rewrite_tag('%property_id%', '([0-9]+)');
        add_rewrite_rule('^brochure-([0-9]+).pdf/?', sprintf('index.php?page_id=%d&zoobrochure_action=print&property_id=$matches[1]', $page->ID), 'top');
    }

    /**
     * Displaying brochure
     * 
     * @global type $zoobrochure
     * @since 2.0.0
     * @return void
     */
    public function display_brochure() {
        $property_id = get_query_var('property_id');
        $is_print = get_query_var('zoobrochure_action') == 'print';
        if ($property_id && $is_print) {
            global $zoobrochure;

            // We check property first, if it not present we stop it imediately
            if (empty($property_id)) {
                wp_die('Property ID doesn\'t exists');
            }

            // Get Selected Template from Setting 
            $template = get_option('zoobrochure_current_template');
            
            // Overide setting if preview is present
            if (isset($_GET['zoobrochure_preview']) && ($_GET['zoobrochure_preview'] == 1)) {
                $template = $_GET['current_template'];
            }
            
            // We check template is already setup or not
            $haystack = $zoobrochure->setting()->get_template_list(true);
            if (!$template || !in_array($template, $haystack)) {
                wp_die('Please select template first');
            }

            // Get path of template
            $filename = $this->prefix . $template;
            $path = sprintf('zoobrochure/%s%s.php', $zoobrochure->get_template_path(), $filename);
            $location = locate_template($path);

            // We looking for template on theme first and then on plugin it self
            $template_file = ($location) ? $location : sprintf('%s%s%s.php', $zoobrochure->get_plugin_path(), $zoobrochure->get_template_path(), $filename);

            // Check file is exist 
            if (!file_exists($template_file)) {
                wp_die('Template not found');
            }
            // load the template
            require_once($template_file);
            

            // Check the class template
            $class_name = $this->prefix . $template;
            if(!class_exists($class_name)){
                wp_die(sprintf('Class %s not found', $class_name));
            }


            // Prepare Setting
            $defaults = get_option('zoobrochure_setting_' . $template, $zoobrochure->setting()->get_default_formated_setting($template));
            $setting = array(
                'bed_icon' => ($defaults['bed_icon']) ? $defaults['bed_icon'] : $zoobrochure->get_plugin_path() . 'assets/images/icons/bed_icon.png',
                'bath_icon' => ($defaults['bath_icon']) ? $defaults['bath_icon'] : $zoobrochure->get_plugin_path() . 'assets/images/icons/bath_icon.png',
                'car_icon' => ($defaults['car_icon']) ? $defaults['car_icon'] : $zoobrochure->get_plugin_path() . 'assets/images/icons/car_icon.png',
            );
            
            // handle color to RGB array
            foreach ($defaults as $key_item => $value_item) {
                if(strpos($key_item, '_color') !== false){
                    $defaults[$key_item] = empty($defaults[$key_item]) ? [255,255,255] : zoobrochure_hex2rgb($defaults[$key_item]);
                } else {
                    continue;
                }
            }
            
            $brochure_setting = wp_parse_args($setting, $defaults);

            // Overide setting if preview is present
            if (isset($_GET['zoobrochure_preview']) && ($_GET['zoobrochure_preview'] == 1)) {
                $query = $_GET;
                foreach ($query as $key => $item) {
                    if (strpos($key, '_color') !== false) {
                        $query[$key] = zoobrochure_hex2rgb($item);
                    }
                }
                $brochure_setting = wp_parse_args($query, $brochure_setting);
            }

            // Prepare Property Data
            $brochure_property = $zoobrochure->property()->property($property_id);

            
            // Create instance & cek the instance is under correct class
            $object = new $class_name($brochure_property, $brochure_setting);
            if(!is_subclass_of($object, 'zoobrochure_template')){
                wp_die(sprintf('Class %s should be sub class of \'zoobrochure_template\'', $class_name));
            }
            
            // Check main method
            if(!method_exists($object, 'render')){
                wp_die(sprintf('Method %s under class %s not found', 'output', $class_name));
            }
            
            // Do render PDF
            $object->render();
            $object->__output();
        }
    }

    /**
     * Redirect brochure printing URL (remove slash on last url)
     * 
     * @since 2.0.0
     * @param string $redirect_url
     * @param string $requested_url
     * @return string
     */
    public function redirect_canonical($redirect_url, $requested_url) {
        $property_id = get_query_var('property_id');
        $is_print = get_query_var('zoobrochure_action') == 'print';
        if ($property_id && $is_print) {
            $redirect_url = $requested_url;
        }
        return $redirect_url;
    }

}
