<?php if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ 
global $realty,$helper;
if($realty->property['is_for_sale'])$is_sale='1';else $is_lease='1';
$settings['video_width']=($settings['video_width']=='')?'580':$settings['video_width'];$settings['video_height']=($settings['video_height']=='')?'385':$settings['video_height'];
?>
<div class="sales_data">
	<div class="block_content">
		<?php wp_print_scripts('jquery-ui-core');wp_print_scripts('jquery-ui-tabs');?>

		
		<script type="text/javascript">
jQuery(document).ready(function(){
jQuery("#tabbed_listings").tabs();
jQuery(".sales_data li a").click(function(){
zoogooglemaps_resize();
});
});
</script>
		
		<div id="tabbed_listings">
			<ul class="shadetabs sales_data" id="stattabs sales_data">
				<?php if($settings['map']=='1' && !empty($realty->property['latitude']) && !empty($realty->property['longitude'])) : ?>
				<li class="stat7"><a href="#stat7">Map</a></li>
				<?php endif; ?>
				
				<?php if($settings['sales_data']=='1') : ?>
				<li class="stat1"><a href="#stat1">Overview</a></li>
				<li class="stat2"><a href="#stat2">Demographics</a></li>
				<?php endif; ?>			

				<?php if($settings['walkability']=='1') : ?>
				<li class="stat6"><a href="#stat6">Walkability</a></li>
				<?php endif; ?>
				
				<?php if($settings['map']=='1' && !empty($realty->property['latitude']) && !empty($realty->property['longitude']) && $settings['map_mode']=='street_view') : ?>
				<li class="stat9"><a href="#stat9">Street View</a></li>
				<?php endif; ?>
				
				<?php if($settings['suburb_profile']=='1' && class_exists('LocationInfo')) : ?>
				<?php $suburb_profile=$realty->get_suburb();if(!empty($suburb_profile)){ ?>
				<li class="stat3"><a href="#stat3">Suburb Profile</a></li>
				<?php } endif; ?>
				
				<?php if($settings['sold_and_leased']=='1') : ?>
				<?php $sold_listings = $realty->sold(array('list'=>'sale','suburb'=>$realty->property['suburb']));if($sold_listings['results_count'] >1 && $is_sale=='1'){ ?>
				<li class="stat4"><a href="#stat4">Recent Sales</a></li>
				<?php } ?>
				<?php $lease_listings = $realty->sold(array('list'=>'lease','suburb'=>$realty->property['suburb']));if($lease_listings['results_count'] >1 && $is_lease=='1'){ ?>
				<li class="stat5"><a href="#stat5">Recent Leased</a></li>
				<?php }endif; ?>								

				<?php if($settings['similar_listings']=='1') : ?>
				<?php $similar_listings = $realty->similar_listings($settings);if($similar_listings['results_count'] >0){?>
				<li class="stat8"><a href="#stat8"><?php echo ($settings['Title']=='')?'Similar Property':$settings['Title']; ?></a></li>
				<?php } endif; ?>
				
				<?php if( strpos($realty->property['ext_link_2'],'youtu')!==false || !empty($realty->property['property_video'])): ?>
				<li class="stat10"><a href="#stat10">Video</a></li>		
				<?php endif; ?>				

			</ul>
			<div id="property_stats">
				<?php if($settings['map']=='1' && !empty($realty->property['latitude']) && !empty($realty->property['longitude'])) : ?>
				<div class="tabcontent" id="stat7"><?php map($before_title,$after_title); ?></div>
				<?php endif; ?>
				
				<?php if($settings['sales_data']=='1') : ?>
				<div class="tabcontent" id="stat1"><?php showgeneral($realty->property); ?></div>
				<div class="tabcontent" id="stat2"><?php showdemographic($realty->property); ?></div>
				<?php endif; ?>
				
				<?php if($settings['walkability']=='1') : ?>
				<div class="tabcontent" id="stat6"><?php walkability(); ?></div>
				<?php endif; ?>	
				
				<?php if($settings['map']=='1' && !empty($realty->property['latitude']) && !empty($realty->property['longitude']) && $settings['map_mode']=='street_view') : ?>
				<div class="tabcontent" id="stat9"  style="width:<?php echo $settings['map_width']; ?>px;height:<?php echo $settings['map_height']; ?>px;"><?php street_view($before_title,$after_title); ?></div>
				<?php endif; ?>		
				
				<?php if($settings['suburb_profile']=='1') : ?>
				<?php if(!empty($suburb_profile)){ ?>
				<div class="tabcontent" id="stat3"><?php location_info($suburb_profile); ?></div>
				<?php } endif; ?>
				
				<?php if($settings['sold_and_leased']=='1') : ?>
				<?php if($sold_listings['results_count'] >1 && $is_sale=='1'){ ?>
				<div class="tabcontent" id="stat4"><?php resent_sale($sold_listings); ?></div>
				<?php } ?>
				<?php if($lease_listings['results_count'] >1  && $is_lease=='1'){ ?>
				<div class="tabcontent" id="stat5"><?php resent_leased($lease_listings); ?></div>
				<?php } endif;?>		
					
				
				<?php if($settings['similar_listings']=='1') : ?>
				<?php if($similar_listings['results_count'] >0){?>
				<div class="tabcontent" id="stat8"><?php similar_listings($similar_listings,$realty->property['is_for_sale']); ?></div>
				<?php } endif; ?>	
				
				<?php if( strpos($realty->property['ext_link_2'],'youtu')!==false || !empty($realty->property['property_video'])){ ?>
				<div class="tabcontent" id="stat10">
					<?php 
					$property_video=$realty->property['property_video'];
					if(strpos($realty->property['ext_link_2'],'youtu')!==false ){
						$youtube_id= trim($realty->property['ext_link_2']);
						$youtube_id=str_replace("http://www.youtube.com/embed/","",$youtube_id); 
						if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtube_id, $matches)){
							$youtube_id=$matches[0];
						}
					 ?>
					 <object width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>">
						<param name="movie" value="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>">
						<param name="allowFullScreen" value="true">
						<param name="allowscriptaccess" value="always">
						<param name="wmode" value="transparent" />
						<embed src="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>" wmode="transparent" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>"></embed>
					</object>
					<?php }elseif(!empty($property_video['embed_code'])){
						$embed_code=$property_video['embed_code'];
						
						$embed_code=preg_replace('/(width=")([\d]+")/is', 'width="'.$settings['video_width'].'"', $embed_code);
						$embed_code=preg_replace('/(height=")([\d]+")/is', 'height="'.$settings['video_height'].'"', $embed_code);
						echo $embed_code ;
					}elseif(!empty($property_video['youtube_id'])){
					$youtube_id= trim($property_video['youtube_id']);
					if(preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtube_id, $matches)){
						$youtube_id=$matches[0];
					}
					if($youtube_id){
					$youtube_id=str_replace("http://www.youtube.com/embed/","",$youtube_id);
					 ?>
					 <object width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>">
						<param name="movie" value="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>">
						<param name="allowFullScreen" value="true">
						<param name="allowscriptaccess" value="always">
						<param name="wmode" value="transparent" />
						<embed src="<?php echo str_replace("youtube_id",$youtube_id,"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>" wmode="transparent" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" width="<?php echo $settings['video_width']; ?>" height="<?php echo $settings['video_height']; ?>"></embed>
					</object>
					<?php }else{ ?>
						<iframe align="top" border="0" frameborder="0" height="450"  src="<?php echo $realty->pluginUrl.'display/elements/video.php?property_id='.$_REQUEST['property_id']; ?>" target="_top" width="100%">Your browser does not support inline frames or is currently configured not to display inline frames.</iframe>
					<?php }
					} ?>								
				</div>
				<?php } ?>
				
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<?php
function video($url, $width, $height){ 
$url=str_replace("watch_popup?","",$url);
$url=str_replace("v=","v/",$url);
$url=str_replace("embed","v",$url);
?>
	<object width="<?php echo $width;?>" height="<?php echo $height;?>">
		<param name="movie" value="<?php echo $url; ?>">
		<param name="allowFullScreen" value="true">
		<param name="allowscriptaccess" value="always">
		<embed src="<?php echo $url; ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $width;?>" height="<?php echo $height;?>"></embed>
	</object>
<?php
}

function street_view($before_title,$after_title){
global $realty;
?>
<?php
if (class_exists('zoomap'))
{
zoomap::getInstance()->load_streetview('stat9', $realty->property['latitude'], $realty->property['longitude']);
} else {
die('No Map Available!');
}
?>

<?php 
 }
 
function location_info($results){
extract($results[0]);
?>
<div id="location_info" class="sim_content">
<?php echo "<h3>" .$suburb . "</h3>"; ?>
<div class="block_content">
<?php if(!empty($results['photos'])): ?>
<div class="image">
	<a href="<?php echo $results['photos'][0]['path']; ?>" onclick="return false;" rel="lightbox[suburb_photos]" title="<?php echo $suburb." photos"; ?>"><img src="<?php echo  $results['photos'][0]['thumbnail']; ?>" alt=""></a>
	<div style="display:none">
		<?php  for ($i=1;$i<count($results['photos']);$i++){?>
		<a href="<?php echo $results['photos'][$i]['path']; ?>" onclick="return false;" rel="lightbox[suburb_photos]"></a>
		<?php } ?>
	</div>
</div>
<?php endif; // !empty($photos) ?>
	
<div class="similar_property_info">
	<ul class="location_info">
		<li class="sim_suburb suburb"><span class="field">Suburb:</span> <?php echo $suburb; ?></li>
		<?php if(!empty($population)): ?><li class="sim_price price"><span class="field">Population:</span><?php echo $population; ?></li><?php endif; ?>
		<?php if(!empty($council)): ?><li class="sim_type type"><span class="field">Municipality:</span> <?php if(!empty($link)) echo "<a href='$link' target='blank' rel='nofollow'>"; ?><?php echo $council; ?><?php if(!empty($link)) echo "</a>"; ?></li><?php endif; ?>
	</ul>
</div>
</div>
<hr />
	<p class="location_description"><?php echo $description; ?></p>

<hr />

<div class="location_amenities">
<?php 
	$amenities = array('school'=>'schools', 'shopping'=>'shoppings', 'landmark'=>'landmarks','link'=>'interesting links');
	foreach($amenities as $key=>$title){
		if(!empty($results[$title]))
		{	
			echo "<h3>$suburb Points of Interest</h3>";
			break;
		}	
	}
	foreach($amenities as $key=>$title): if(!empty($results[$title])):// var_dump($results[$title]); ?>
	<div class="interesting_location location_<?php echo $key; ?>">
	<p class="<?php echo $key; ?>_title">
		<b><?php switch ($title){
				case 'schools': echo "$suburb Schools"; break;
				case 'shoppings': echo "$suburb Shoppings"; break;
				case 'landmarks': echo "$suburb Landmarks"; break;
				case 'interesting links': echo "$suburb Links"; break;
			}
		?></b>
	</p>
	<ul class="<?php echo $key; ?>_list">
	<?php foreach ($results[$title] as $item): ?>
	<li>		
		<?php if(!empty($item['link'])) echo '<a href="'.$item['link'].'" title="'.$item['amenity'].'" rel="nofollow">';?><?php echo $item['amenity']; ?><?php if(!empty($item['link'])) echo'</a>'; ?>
	</li>

	<?php endforeach; ?>
	</ul>
	</div><!--end .location_<?php echo $key; ?>-->
<?php endif; endforeach; ?>
</div><!--end #location_amenities-->

<hr />

<div style="display:block">
<?php if(!empty($photos)) foreach ($photos as $photo): ?>
<a href="<?php echo $photo['path']; ?>" class="thickbox" rel="LocationInfo_photos" title="<?php echo $photo['name']; ?>" onclick="return false;"></a>
<?php endforeach; ?>
</div>
</div><!-- end .location_info  -->
<?php 
}

function walkability(){
global $realty;
$settings=$realty->settings['widgets']['sales_data'];
?>
<div id="walk_score">
	<script type="text/javascript">
	/* <![CDATA[ */
	  var ws_address = "<?php echo $realty->property['street_address'] . ' ' . $realty->property['suburb'] . ' ' . $realty->property['state'] .' ' . $realty->property['postcode'] . ' Australia'; ?>";
	  var ws_width = "<?php echo $settings['width']; ?>";
	  var ws_iframe_css ="<?php echo $settings['iframe_css']; ?>";
	  var ws_map_frame_color = "<?php echo $settings['map_frame_color']; ?>";
	  var ws_score_color = "<?php echo $settings['score_color']; ?>";
	  var ws_headline_color = "<?php echo $settings['headline_color']; ?>";
	  var ws_category_color = "<?php echo $settings['category_color']; ?>";
	  var ws_result_color = "<?php echo $settings['result_color']; ?>";
	/* ]]> */
	</script>

	<script type="text/javascript" src="http://walkscore.com/tile/show-tile.php?wsid=<?php echo $realty->settings['general_settings']['walk_score_key']; ?>"></script>
</div><!-- end #walk_score -->
<?php
}

function resent_sale($sold_listings){
global $realty;
if($sold_listings['results_count'] >1){ ?>
<div id="sold_properties">
<h4>Sold Properties: <?php echo $suburb;?></h4>
<table class="results" cellpadding="0" cellspacing="0" summary="Sales Results">
<tr class="th">
	<th class="th_address">Address</th>
	<th class="th_suburb">Suburb</th>
	<th class="th_date">Sold Date</th>
	<th class="th_bed">Bed</th>
	<th class="th_bath">Bath</th>
	<th class="th_car">Car</th>
	<th class="th_price">Sale Price</th>
	<th class="th_view"></th>
</tr>
<?php $i=0;foreach($sold_listings as $property):  if(!is_array($property))continue;$i++;if($row['id']==$property['id'])continue;?>
<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">

	<td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></a></span></td>
	<td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
	<td class="td_date"><span class="day"><span class="date"><?php if(!empty($property['date']) && $property['date']!='0000-00-00')echo date("j-m-Y",strtotime($property['date'])); else echo 'not provided'; ?></span></td>
	<td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
	<td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
	<td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
	<td class="td_price"><span class="td_price"><?php echo ($property['display_sold_price'])? $property['last_price']:'Undisclosed'; ?></span></td>
	<td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
</tr>
<?php endforeach; echo "</table></div>";} ?>
<?php
}

function resent_leased($lease_listings){
global $realty;
if($lease_listings['results_count'] >1){ ?>
<hr />
<div id="leased_properties">
<h4>Leased Properties: <?php echo $suburb;?></h4>
<table class="results" cellpadding="0" cellspacing="0" summary="Lease Results">
<tr class="th">
	<th class="th_address">Address</th>
	<th class="th_suburb">Suburb</th>
	<th class="th_date">Leased Date</th>
	<th class="th_bed">Bed</th>
	<th class="th_bath">Bath</th>
	<th class="th_car">Car</th>
	<th class="th_price">Lease Price</th>
	<th class="th_view"></th>
</tr>
<?php $i=0;foreach($lease_listings as $property):   if(!is_array($property))continue;$i++;if($row['id']==$property['id'])continue;?>
<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">

	<td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></span></a></td>
	<td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
	<td class="td_date"><span class="day"><span class="date"><?php if(!empty($property['date']) && $property['date']!='0000-00-00')echo date("j-m-Y",strtotime($property['date'])); else echo 'not provided'; ?></span></td>
	<td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
	<td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
	<td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
	<td class="td_price"><span class="td_price"><?php echo ($property['display_sold_price'])? $property['last_price']:'Undisclosed'; ?></span></td>
	<td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
</tr>
<?php endforeach; echo "</table></div>";} ?>
<?php
}

function similar_listings($similar_listings, $is_sale=false){
global $realty;
if($similar_listings['results_count'] >0):?>
<div id="similar_listings" class="sim_content">
<h4><?php echo $before_title; ?><span class="title_similar_listings">Current <?php echo ($is_sale)?'Sales':'Leases' ;?></span><?php echo $after_title; ?></h4>
<div class="block_content">
	<?php
		$count=0; 
		foreach ($similar_listings as $property):
		 if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned			
		  $simlist_count++;
?>
<div class="block<?php echo " block".$simlist_count;?>">
<div class="image">
	<a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>"><img src="<?php echo $property['thumbnail']; ?>" alt="<?php echo $property['headline']; ?>" /></a>
</div>
<div class="similar_property_info">
	<ul>
		<li class="sim_suburb suburb"><span class="field">Suburb:</span> <?php echo $property['suburb']; ?></li>
		<li class="sim_type type"><span class="field">Type:</span> <?php echo $property['property_type']; ?></li>
		<li class="sim_price price"><span class="field">Price:</span><?php echo $property['price']; ?></li>
		<li><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">View Property &#187;</a></li>
	</ul>
</div>
<div class="clearer"></div>
</div>
<?php	endforeach; ?>
</div>
<div class="clearer"></div>
</div><!-- end #similar_listings -->
<?php endif;
?>
<?php
}

function map($before_title,$after_title){
global $realty;
$settings=$realty->settings['widgets']['sales_data'];
if(!empty($realty->property['latitude']) and !empty($realty->property['longitude'])): ?>
<div class="property_map">
	<div id="map_canvas" style="width:<?php echo $settings['map_width']; ?>px;height:<?php echo $settings['map_height']; ?>px;">
	</div>
</div><!-- .property_map ends -->
<?php
if (class_exists('zoomap'))
{
zoomap::getInstance()->load_map('map_canvas', $realty->property['latitude'], $realty->property['longitude'], 13, true);
} else {
die('No Map Available!');
}
?>
<div class="clearer"></div>
<?php endif; 
}
?>