<?php
/**********************************************************************************************************************************************
Start of Theme Options Page
***********************************************************************************************************************************************/
add_action('admin_menu', 'zoo_settings_page_init');
add_action('admin_head', 'zoo_admin_head');

function zoo_settings_page_init() {
	$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );
	$settings_page = add_theme_page('Theme Settings', 'Theme Settings', 'edit_theme_options', 'theme-settings', 'zoo_settings_page' );
	add_action( "load-{$settings_page}", 'zoo_load_settings_page' );
}

function zoo_load_settings_page() {
	if ( $_POST["zoo-settings-submit"] == 'Y' ) {
		zoo_save_theme_settings();
		$url_parameters = isset($_GET['tab'])? 'updated=true&tab='.$_GET['tab'] : 'updated=true';
		wp_redirect(admin_url('themes.php?page=theme-settings&'.$url_parameters));
		exit;
	}
}

function zoo_save_theme_settings() {
	global $options;
	if ( $_GET['page'] == 'theme-settings' ){ 
		if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab']; 
	    else $tab = 'homepage'; 
		if( $tab == 'homepage' ) {			
			/* for image upload
			$quick_link_1_image=get_option('quick_link_1_image');
			$targetPath=dirname(dirname(dirname(dirname(__FILE__))))."/uploads/homepage_images/";
			
			if(!empty($quick_link_1_image))$_REQUEST['quick_link_1_image']=$quick_link_1_image;
			$userfile_tmp = $_FILES['quick_link_1_image']['tmp_name']; 			
			
			if(!empty($userfile_tmp)){
				extract(pathinfo( $_FILES['quick_link_1_image']['name']));
				if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
				$filename = strtolower (preg_replace("/[^a-z0-9-]/", "-", $filename));
				$basename = "$filename.$extension";
				$final_filename = $targetPath . $basename;
				if( !save_file($userfile_tmp, $final_filename)){
					$message_pass= "Error uploading image";		
				}
				else{
					$_REQUEST['quick_link_1_image']=$basename;
				}
			}
			
			$videoPath=dirname(dirname(dirname(dirname(__FILE__))))."/uploads/homepage_videos/";
			
			// top right video & image 
			if(!empty($top_right_video))$_REQUEST['top_right_video']=$top_right_video;
			$userfile_tmp = $_FILES['top_right_video']['tmp_name']; 			
			
			if(!empty($userfile_tmp)){
				extract(pathinfo( $_FILES['top_right_video']['name']));
				if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
				$filename = $blog_id."_".strtolower (preg_replace("/[^a-z0-9-]/", "-", $filename));
				$basename = "$filename.$extension";
				$final_filename = $videoPath . $basename;
				if( !save_file($userfile_tmp, $final_filename)){
					$message_pass= "Error uploading image";		
				}
				else{
					$_REQUEST['top_right_video']=$basename;
				}
			}
			*/
			
			foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] ); 
			}
			foreach ($options as $value) {
				if( isset( $_REQUEST[ $value['id'] ] ) ) { 
					if( $value['type'] == 'checkbox' ) {
						if( $value['status'] == 'checked' ) {
							update_option( $value['id'], 1 );
						} else { 
							update_option( $value['id'], 0 ); 
						}	
					} elseif( $value['type'] != 'checkbox' ) {
						$_REQUEST[ $value['id'] ]=stripslashes(str_replace('\\','',$_REQUEST[ $value['id'] ]));
						update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); 
					} else { 
						delete_option( $value['id'] ); 
					}
				}
			}	
			if(!empty($_POST['homepage_images']))update_option('homepage_images',$_POST['homepage_images']);		
        }
	}
}

if(!function_exists('save_file')){
	function save_file($url, $file){
		if(!@file_exists(dirname($file)))
			@mkdir(dirname($file), 0777);
		else{
			if(@file_exists($file)){
				@chmod($file);
				@unlink($file);
			}
		}
		
		$content = @file_get_contents($url);
		if($content)
			$result = @file_put_contents($file, $content);
		else
			$result = false;
		
		if($result)
			@chmod($file, 0777);
		
		return $result;
	}
}

function zoo_admin_tabs( $current = 'homepage' ) { 
    $tabs = array( 'homepage' => 'Homepage' ); 
	$realty_settings=get_option('realty_general_settings');
	$tabs['featured']='Featured Listings';
	$tabs['team']='Team';
	if(class_exists('subscriptions_manager'))$tabs['subscribers']='Subscribers';
	if(class_exists('LocationInfo'))$tabs['locations']='Location Info';
	if(class_exists('visits_tracker'))$tabs['visits']='Visits Tracker';
	if(class_exists('zoo_analytics'))$tabs['analytics']='Property Performance';
    $links = array();
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-settings&tab=$tab'>$name</a>";
        
    }
    echo '</h2>';
}

function zoo_admin_head(){ 
	if($_GET['action'] =='edit' && $_GET['tab']=='locations'){
		wp_print_scripts('jquery-ui-core');
		wp_print_scripts('jquery-ui-sortable');		
	}
}
	
function zoo_settings_page() {
	global $pagenow;
	$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );
	?>
	
	<div class="wrap">
		<h2><?php echo $theme_data['Name']; ?> Theme Settings</h2>		
		<?php
		if ( 'true' == esc_attr( $_GET['updated'] ) ) echo '<div class="updated" ><p>Theme Settings updated.</p></div>';			
		if ( isset ( $_GET['tab'] ) ) zoo_admin_tabs($_GET['tab']); else zoo_admin_tabs('homepage');
		wp_nonce_field( "zoo-settings-page" ); 			
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/styles/themeoptions.css" />'. "\n";
		if ( $pagenow == 'themes.php' && $_GET['page'] == 'theme-settings' ){ 			
			if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab']; 
			else $tab = 'homepage'; 				
			switch ( $tab ){
				case 'homepage' : 
					include('admin-homepage.php');
				break;
				case 'featured' : 
					include('admin-featured.php');
				break;
				case 'team' : 
					include('admin-team.php');
				break;
				case 'subscribers' :
					include('admin-subscribers.php');
				break; 
				case 'locations' : 
					include('admin-locations.php');
				break;
				case 'visits' : 
					include('admin-visits.php');
				break;	
				case 'analytics' : 
					include('admin-analytics.php');
				break;	
			}
		}
		?>
	</div>
<?php
}
?>