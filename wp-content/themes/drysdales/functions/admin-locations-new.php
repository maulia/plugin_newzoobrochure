<form action="" method="post" enctype="multipart/form-data">
	<div class="leftside">		
		<input type="hidden" name="project_id" value="<?php echo $_GET['project_id']; ?>" />
		<input type="hidden" name="task" value="<?php echo $task; ?>" />
		<input type="hidden" id="user-id" name="user_ID" value="<?php global $user_ID; echo (int) $user_ID ?>" />
		<table class="widefat maintable" cellspacing="0">
		<thead>
		<tr>
			<th colspan="2" scope="col" class="manage-column column-title">Location Information</th>
		</tr>
		</thead> 
		<tr class="form-field">
			<th valign="top"  scope="row"><label><?php _e('Suburb') ?></label></th>
			<td><input type="text" name="suburb" id="suburb" value="<?php echo $suburb; ?>" /></td>
		</tr>
			<tr class="form-field">
			<th valign="top"  scope="row"><label><?php _e('Population') ?></label></th>
			<td><input type="text" name="population" id="population" value="<?php echo $population; ?>" /></td>
		</tr>
			<tr class="form-field">
			<th valign="top"  scope="row"><label><?php _e('Postcode') ?></label></th>
			<td><input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" /></td>
		</tr>
			<tr class="form-field">
			<th valign="top"  scope="row"><label><?php _e('Council') ?></label></th>
			<td><input type="text" name="council" id="council" value="<?php echo $council; ?>" /></td>
		</tr>
			<tr class="form-field">
			<th valign="top"  scope="row"><label><?php _e('Link') ?></label></th>
			<td><input type="text" name="link" value="<?php echo $link ?>" /></td>
		</tr>
		</table>		
		<div class="clear"></div>
		<h4><?php _e('Description') ?></h4>
		<?php
		$args = array("textarea_name" => "description");
		wp_editor( $description, "content", $args );
		?>
		<p class="submit"><input name="save" type="submit" value="Save changes" /></p>	
	</div>
			
	<div id="poststuff">
		<div class="postbox">
			<h3>Actions</h3>		
			<div class="inside">
				<p class="submit">
				<input name="save" type="submit" value="Save changes" class="button"/>
				</p>
				<a href="themes.php?page=theme-settings&tab=locations" class="button">Back</a>
				</p>			
			</div>	
		</div>
	</div>
</form>