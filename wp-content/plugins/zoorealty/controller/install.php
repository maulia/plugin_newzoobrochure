<?php
function get_widget_title($widget){
global $helper, $realty;
$number = 1;
$widget_files = $helper->getTemplates($realty->path('display/widgets/'));
foreach($widget_files as $caption=>$file):
$prefix = "realty_widget_". $caption;
if ($caption==$widget)$widget_title=$prefix.'-'.$number;
++$number;
endforeach;
return $widget_title;
}
global $wpdb, $helper;

$wpdb->query("CREATE TABLE IF NOT EXISTS `past_opentimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opentime_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `total_attended` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_property_id` (`property_id`)
) ENGINE=InnoDB");

$sql = "CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `home_number` varchar(255) DEFAULT NULL,
  `work_number` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `fax_number` varchar(255) DEFAULT NULL,
  `unit_number` varchar(255) DEFAULT NULL,
  `street_number` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `suburb` varchar(255) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'active',
  `owner_occupier` tinyint(1) default '0',
  `investor` tinyint(1) default '0',
  `looking_to_buy` tinyint(1) default '0',
  `looking_to_sell` tinyint(1) default '0',
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  KEY `office_id` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
$wpdb->query($sql);		

$sql = "CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `note_type_id` tinyint(4) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` text,
  `note_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email_1_flag` tinyint(1) default '0',
  `email_2_flag` tinyint(1) default '0',
  `email_3_flag` tinyint(1) default '0',
  `email_4_flag` tinyint(1) default '0',
  `email_5_flag` tinyint(1) default '0',
  `unsubscribe` tinyint(1) default '0',
  `unsubscribe_date` datetime,
  `unsubscribe_note` varchar(255),
  `call` tinyint(1) default '0',
  `new` tinyint(1) default '1',
  PRIMARY KEY (`id`),
  KEY `index_notes_on_contact_id` (`contact_id`),
  KEY `index_notes_on_property_id` (`property_id`),
  KEY `index_notes_on_agent_id` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
$wpdb->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `notes_sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
$wpdb->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `note_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `status` enum('contact_note','property_note','system') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;";
$wpdb->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `contact_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;";
$wpdb->query($sql);	

$wpdb->query("CREATE TABLE IF NOT EXISTS `property_enquiry_form` (
`enquiry_id` MEDIUMINT( 9 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`customer_name` VARCHAR( 100 ) NOT NULL ,
`email` VARCHAR( 100 ) NOT NULL ,
`phone` VARCHAR( 50 ) NOT NULL ,
`message` LONGTEXT NOT NULL ,
`agent_id` MEDIUMINT( 9 ) NOT NULL ,
`property_id` MEDIUMINT( 9 ) NOT NULL ,
`property_url` VARCHAR( 100 ) NOT NULL,
`date` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;");

$sql="CREATE TABLE IF NOT EXISTS `extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT NULL,
  `featured_property` tinyint(1) DEFAULT NULL,
  `property_of_week` tinyint(1) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `dropdown_name` varchar(255) DEFAULT NULL,
  `dropdown_value` int(11),
  `office_id` int(11),
  `projects` tinyint(1),
  PRIMARY KEY (`id`),
  KEY `extra_property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
$wpdb->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `extra_detail_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_id` int(11) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;";
$wpdb->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `extra_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT NULL,
  `field_value` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `extra_detail_name_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `details_office_id` (`office_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6453 ;";
$wpdb->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `extra_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT NULL,
  `option_title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `option_order` int(11),
  `office_id` int(11),
  PRIMARY KEY (`id`),
  KEY `options_property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
$wpdb->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `new_development_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new_development_id` int(11) DEFAULT NULL,
  `development_name` varchar(255) DEFAULT NULL,
  `development_website` varchar(255) DEFAULT NULL,
  `year_built` int(11),
  `number_of_floors` int(11),
  `number_of_properties` int(11),
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `architect_name` varchar(255) DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `completion_date` varchar(255) DEFAULT NULL,
  `distance` varchar(255) DEFAULT NULL,
  `bedroom_from` int(11),
  `bedroom_to` int(11),
  `bathroom_from` int(11),
  `bathroom_to` int(11),
  `carspace_from` int(11),
  `carspace_to` int(11),
  `size_from` int(11),
  `size_to` int(11),
  `listing_privacy` tinyint(1),
  PRIMARY KEY (`id`),
  KEY `new_development_id` (`new_development_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
$wpdb->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `new_development_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,  
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `development_property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
$wpdb->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `property_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,  
  `property_id` int(11) DEFAULT NULL,  
  `contact_name` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `note` text,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `testimonial_property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
$wpdb->query($sql);

global $wpdb;
// create tables
$q1="CREATE TABLE IF NOT EXISTS `rental_seasons` (
`id` mediumint(9) NOT NULL auto_increment,
`season` varchar(15),
`start_date` date,
`end_date` date,
`minimum_stay` varchar(15),
`property_id` bigint(20),
`position` mediumint(9),
PRIMARY KEY  (`id`),
KEY `rental_property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";			
$wpdb->query($q1);

$q3="CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL auto_increment,
  `is_user` tinyint(1) default '0' COMMENT '1=Yes, this attachment is for a user. 0=No, this attachment is for a property.',
  `description` varchar(255) default NULL,
  `url` varchar(255) default NULL COMMENT 'Only one, eliminate large, medium and thumb. These can be deducted from the path in the code.',
  `parent_id` int(11) default NULL COMMENT 'if is_user=0, references properties.id. if is_user=1, references users.id',
  `type` varchar(50) default NULL COMMENT 'Valid values are: photo, video, floorplan, brochure or sound',
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `position` int(11) default '0',
   title varchar(100),
   width int(11),
   height int(11),
  PRIMARY KEY  (`id`),
  KEY `parent_id` (`parent_id`,`type`(1),`description`(1))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q3);

$q4="CREATE TABLE IF NOT EXISTS `opentimes` (
  `id` int(11) NOT NULL auto_increment,
  `property_id` int(11) default NULL COMMENT 'References properties.id',
  `date` date default NULL,
  `start_time` time default NULL,
  `end_time` time default NULL,
  PRIMARY KEY  (`id`),
  KEY `opentimes_property_id_index` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q4);

$q5="CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL auto_increment,
  `unique_id` varchar(100)  COMMENT 'The custom ID/Reference given by the user to this record/property',
  `status` int(11) default '1' COMMENT '1=available, 2=sold,4=under offer, 6=leased',
  `type` varchar(100)  COMMENT 'E.g. ResidentialLease, BusinessSale.',
  `property_type` varchar(32) ,  
  `property_type2` varchar(50),
  `property_type3` varchar(50),
  `category` varchar(100)  COMMENT 'E.g. House, apartment, unit.',
  `deal_type` varchar(30) ,
  `method_of_sale` varchar(50),
  `authority` varchar(50),
  `display_address` tinyint(1) default '1' COMMENT '1=Yes, 0=No',
  `unit_number` varchar(20) ,
  `street_number` varchar(20) ,
  `street` varchar(100) ,
  `suburb` varchar(25) ,
  `town_village` varchar(50) ,
  `state` varchar(100) ,
  `country` varchar(50) ,  
  `longitude` varchar(50) ,
  `latitude` varchar(50) ,
  `postcode` varchar(50) ,  
  `headline` text,
  `description` text,
  `highlight1` text,
  `highlight2` text,
  `highlight3` text,  
  `bedrooms` varchar(5),
  `bathrooms` varchar(5),
  `carport` int(11) ,
  `garage` int(11) ,
  `off_street_park` int(11) ,
  `price` int(11)  COMMENT 'The absolute price of a property, including rental, holiday, commercial and sale.',
  `to_price` int(11) ,
  `display_price` tinyint(1) default '1' COMMENT '1=Yes, 0=No',
  `display_price_text` varchar(50)  COMMENT 'When display_price=0, display this text instead of price.',  
  `floor_area` varchar(10) ,
  `land_area` varchar(10) ,
  `office_area` varchar(10),
  `warehouse_area` varchar(10),
  `retail_area` varchar(10),
  `other_area` varchar(10),
  `area_range` varchar(10),
  `area_range_to` varchar(10), 
  `forthcoming_auction` varchar(15) ,
  `auction_date` date ,
  `auction_time` time ,
   auction_place varchar(200) ,
  `show_price` tinyint(1) default '1',
  `sold_at` date ,
  `sold_price` int(11) ,
  `leased_price` int(11) ,
  `leased_at` date ,  
  `office_id` int(11) ,
   agent_id int(11),
  `user_id` int(11)  COMMENT 'Agent ID assigned to this property, references users.id',
  `secondary_user` int(11)  COMMENT 'Second Agent ID assigned to this property, references users.id',
  `third_user` int(11)  COMMENT 'Third Agent ID assigned to this property, references users.id',
  `fourth_user` int(11),
  `secondary_user_type` varchar(30),
  `third_user_type` varchar(30),
  `fourth_user_type` varchar(30),  
  `estimate_rental_return` int(11) ,
  `max_persons` int(11) ,
  `date_available` date , 
  `high_season_price` int(11),  
  `mid_season_price` int(11),
  `peak_season_price` int(11),  
  `rental_price` int(11),
  `rental_price_to` int(11),
  `sale_price_from` int(11),
  `sale_price_to` int(11),  
  `video_url` text,   
  `feature_property` tinyint(1) default '0',
  `new_project_rea` tinyint(1) default '0',
  `new_construction` tinyint(1) default '0',
  `xml_id` varchar(20),
  `agent_user_id` int(11),  
  `project_development_id` int(11),
  `new_development_name` varchar(100),
  `capital_growth` decimal(2.0),
  `rental_yield` decimal(2.0),
  `created_at` datetime ,
  `updated_at` datetime ,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `properties_id_index` (`id`),
  KEY `property_user_id_index` (`user_id`),
  KEY `property_secondary_user_index` (`secondary_user`),
  KEY `property_type_index` (`type`,`id`),
  KEY `properties_office_id_index` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q5);

$q6="CREATE TABLE IF NOT EXISTS `property_features` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `property_id` bigint(20) default NULL COMMENT 'References properties.id',
  `feature` varchar(100) default NULL COMMENT 'Internal Laundry',
  PRIMARY KEY  (`id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q6);

$q7="CREATE TABLE IF NOT EXISTS `property_meta` (
  `meta_id` bigint(20) unsigned NOT NULL auto_increment,
  `property_id` bigint(20) default NULL COMMENT 'References properties.id',
  `meta_key` varchar(100) default NULL COMMENT 'E.g.: bond',
  `meta_value` longtext COMMENT 'E.g.: 1000',
  PRIMARY KEY  (`meta_id`),
  KEY `property_id` (`property_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q7);

$q8="CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL COMMENT 'References users.id',
   user_name varchar(30)default NULL,
   user_name_id int(11),
  `content` text,
  image_url varchar(255),
  `created_on` datetime default NULL,
  `updated_on` datetime default NULL,	
  PRIMARY KEY  (`id`),
  KEY `testimonial_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q8);

$q9="CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `firstname` varchar(100) default '',
  `lastname` varchar(100) default '',
  `phone` varchar(25) default NULL,
  `mobile` varchar(25) default NULL,
  `fax` varchar(25) default NULL,
  `email` varchar(100) default NULL,
  `position` int(11),
   role varchar(50)  default NULL,
  `group` varchar(50)  default NULL,
  `description` text,
   suburb text,
   audio text,
   video_url text,
   video_embed text,  
   facebook_username varchar(100)  default NULL,
   twitter_username varchar(100) default NULL,
   linkedin_username varchar(100) default NULL,	
   `website` varchar(100) default NULL, 
   vcard_title varchar(100)  default NULL,
   vcard_path varchar(100)  default NULL,
   display tinyint(1),
  `office_id` int(11) default NULL,
   agent_id int(11),
  `developer_id` int(11) default NULL,   
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,	
   is_zoo tinyint(1) default '1',  
   display_on_team_page tinyint(1) default '1',
  PRIMARY KEY  (`id`),
  KEY `users_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q9);

$q4="CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_user_id` int(11) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
$wpdb->query($q4);

$q10="CREATE TABLE IF NOT EXISTS `office` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `state` varchar(25) default NULL,
  `suburb` varchar(25) default NULL,
  `zipcode` varchar(25) default NULL,
  `country` varchar(30) default NULL,	
  `street` varchar(50) default NULL,
  `street_number` varchar(20) default NULL,
  `unit_number` varchar(20) default NULL,
  `street_type` varchar(20) default NULL,
  `latitude` varchar(20) default NULL,
  `longitude` varchar(20) default NULL,
  `email` varchar(50) default NULL,	
  `url` varchar(50) default NULL,	
  `fax` varchar(25) default NULL,	
  `phone` varchar(25) default NULL,
  `logo1` varchar(200),
  `logo2` varchar(200),
  `agent_id` int(11) default NULL,	
  `created_at` datetime,
  `updated_at` datetime,  
  PRIMARY KEY  (`id`),
   KEY `office_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q10);

$q10="CREATE TABLE IF NOT EXISTS `office_specialities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `office_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q10);

$q4="CREATE TABLE IF NOT EXISTS `property_video` (
  `id` int(11) NOT NULL auto_increment,
  `property_id` int(11) default NULL COMMENT 'References properties.id',
  `title` varchar(100) default NULL,
  `embed_code` text default NULL,
  `youtube_id` varchar(100) default NULL,
  `status` tinyint(1) default NULL,
  PRIMARY KEY  (`id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q4);

$q4="CREATE TABLE IF NOT EXISTS `property_audio` (
  `id` int(11) NOT NULL auto_increment,
  `property_id` int(11) default NULL COMMENT 'References properties.id',
  `url` text default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
$wpdb->query($q4);

//create pages
$default_pages = array(
'search_results'=>'[realty_plugin template=search_results]',
'sales'=>'[realty_plugin list=sale template=search_results]', 
'residential_sales'=>'[realty_plugin list=ResidentialSale template=search_results]',
'business_sale'=>'[realty_plugin list=BusinessSale template=search_results]',
'project_sale'=>'[realty_plugin list=ProjectSale template=search_results]',
'commercial_sales'=>'[realty_plugin list=commercial_sale template=search_results]',
'opentimes_sales'=>'[realty_plugin list=sale template=opentimes]',
'forthcoming_auction'=>'[realty_plugin list=latest_auction template=latest_auction]',
'sold'=>'[realty_plugin list=sale template=sold]',
'lease'=>'[realty_plugin list=lease template=search_results]',
'residential_lease'=>'[realty_plugin list=ResidentialLease template=search_results]',
'holiday'=>'[realty_plugin list=HolidayLease template=search_results]',
'commercial_lease'=>'[realty_plugin list=commercial_lease template=search_results]',
'opentimes_lease'=>'[realty_plugin list=lease template=opentimes]',
'leased'=>'[realty_plugin list=lease template=sold]',
'furnished'=>'[realty_plugin list=furnished template=search_results]',
'newly_built'=>'[realty_plugin list=newly_built template=search_results]',
'lease_prestige'=>'[realty_plugin list=lease_prestige template=search_results]',
'house_land_packages'=>'[realty_plugin list=house_land_packages template=search_results]',
'new_homes'=>'[realty_plugin list=new_homes template=search_results]',
'land_packages'=>'[realty_plugin list=new_homes template=search_results]',
'residential_block'=>'[realty_plugin list=residential_block template=search_results]',
'small_acreage_block'=>'[realty_plugin list=small_acreage_block template=search_results]',
'large_acreage_block'=>'[realty_plugin list=large_acreage_block template=search_results]',
'favourite_property'=>'[realty_plugin template=favourite_property]',
'print_property'=>'[realty_plugin template=print_property]',
'property'=>'[realty_plugin template=property]',
'property_testimonials'=>'[realty_plugin template=property_testimonials]',
'map'=>'[realty_plugin template=map_results]',
'team'=>'[realty_plugin template=team]',
'executive_team'=>'[realty_plugin group=Executive template=team]',
'sales_team'=>'[realty_plugin group=Sales Agent template=team]',
'property_manager_team'=>'[realty_plugin group=Property Manager template=team]',
'admin_team'=>'[realty_plugin group=Admin template=team]',
'other_team'=>'[realty_plugin group=Other template=team]',
'testimonial'=>'[realty_plugin template=testimonial]',
'calculators'=>'[realty_plugin template=calculator_property]',
'offices'=>'[realty_plugin template=offices]',
'office'=>'[realty_plugin template=office]',
'video'=>'[realty_plugin template=video]',
'tv'=>'[realty_plugin template=video_new]',
);

$default_add_page = array(
'calculators'=>'[realty_plugin template=calculator_property]',
'favourite_property'=>'[realty_plugin template=favourite_property]',
'property'=>'[realty_plugin template=property]',
'team'=>'[realty_plugin template=team]',
'search_results'=>'[realty_plugin template=search_results]',
);


if(get_page_by_title('property')==false):
	foreach($default_add_page as $post_title=>$post_content):
		if(get_page_by_title($post_title)==false)realty_wp_insert_post(array('post_title'=>$post_title, 'post_content'=>$post_content), true);
	endforeach;
endif;
	
function realty_wp_insert_post($post, $unique=false){
	if(empty($post['post_type'])) $post['post_type'] = 'page';
	$postID = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => $post['post_type'], 'post_title'=>humanize($post['post_title']), 'post_content'=>$post['post_content'], 'post_parent'=>$post['post_parent']));
	update_post_meta($postID, '_permanent_name',  underscore($post['post_title']));
	return $postID;		
}

function underscore($camelCasedWord) {
	return strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "", $camelCasedWord)));
}

function humanize($lowerCaseAndUnderscoredWord) {
	return ucwords(str_replace("_", " ", underscore($lowerCaseAndUnderscoredWord)));
}

function camelize($lowerCaseAndUnderscoredWord) {
	$string =str_replace(" ", "", ucwords(str_replace("_", " ", $lowerCaseAndUnderscoredWord)));
	$string{0} = strtolower($string{0});
	return $string;
}

add_option('realty_pdf_table',array(
 'street_address' => array ('display' => '1','title' => 'Street Address',),
 'state' => array ('display' => '1','title' => 'State',),
 'town_village' => array ('display' => '1','title' => 'Region',),
 'suburb' => array ('display' => '1','title' => 'Suburb',), 
 'postcode' => array ('display' => '1','title' => 'Postcode',),
 'property_type' => array ('display' => '1','title' => 'Property Type',),
 'price' => array ('display' => '1','title' => 'price',),
 'bedrooms' => array ('display' => '1','title' => 'Beds',),
 'bathrooms' => array ('display' => '1','title' => 'Bathrooms',),
 'carspaces' => array ('display' => '1','title' => 'Cars',),
 'building_size' => array ( 'title' => 'Building Size', 'display' => array ('Sqms' )),
));
		
add_option('realty_widgets', 

array(
'quick_search'=>array(
  'sale' => array (
    '100000:100,000',
    '200000:200,000',
    '300000:300,000',
    '400000:400,000',
    '500000:500,000',
    '600000:600,000',
    '700000:700,000',
    '800000:800,000',
    '900000:900,000',
    '1000000:1,000,000',
    '1500000:1,500,000',
    '2000000:2,000,000',
    '2500000:2,500,000',
	'3000000:3,000,000',
    '3500000:3,500,000',
	'4000000:4,000,000',
  ),
  'lease' => array (
    '100:100',
     '200:200',
     '300:300',
    '400:400',
     '500:500',
     '600:600',
    '700:700',
     '800:800',
    '900:900',
    '1000:1,000',
  ),
  'max_beds' => '10',
  'max_baths' => '10',
  'max_cars' => '10',
),
'quick_search_2'=>array(
  'sale' => array (
    '100000:100,000',
    '200000:200,000',
    '300000:300,000',
    '400000:400,000',
    '500000:500,000',
    '600000:600,000',
    '700000:700,000',
    '800000:800,000',
    '900000:900,000',
    '1000000:1,000,000',
    '1500000:1,500,000',
    '2000000:2,000,000',
    '2500000:2,500,000',
	'3000000:3,000,000',
    '3500000:3,500,000',
	'4000000:4,000,000',
  ),
  'lease' => array (
    '100:100',
     '200:200',
     '300:300',
    '400:400',
     '500:500',
     '600:600',
    '700:700',
     '800:800',
    '900:900',
    '1000:1,000',
  ),
  'max_beds' => '10',
  'max_baths' => '10',
  'max_cars' => '10',
),
'sold_listings'=>
  array ('sale' => array ('Number' => '5','Title' => 'Recent Sales'),
    'lease' => array ('Number' => '5', 'Title' => 'Recent Lease'),
    'display' => array (
	    'street_address'=>1,
		'suburb'=>1,
		'property_type'=>1,
		'bedrooms'=>1,
		'bathrooms'=>1,
		'carspaces'=>1,
		'sold_at'=>1
  	)
  ),//ends sold_listings
  
  'sold_listings_2'=>
  array ('sale' => array ('Number' => '5','Title' => 'Recent Sales'),
    'lease' => array ('Number' => '5', 'Title' => 'Recent Lease'),
    'display' => array (
	    'street_address'=>1,
		'suburb'=>1,
		'property_type'=>1,
		'bedrooms'=>1,
		'bathrooms'=>1,
		'carspaces'=>1,
		'sold_at'=>1
  	)
  ),//ends sold_listings2
  
  'properties_of_the_week'=>
  array (
	  'house' =>  'House of the Week',
	  'apartment' =>'Apartment of the Week',
	  'rental' => 'Rental of the Week',
  ), //ends properties_of_the_week
  
  'walk_score'=>
  array (
	  'width' =>  '620',
  ), //ends walk_score
  
  'team_member'=>
  array ( 
	'Number' => 4,
	'display' => array ( 
				'image' => '1',
				'name' => '1',
				'image_mode' => 'portrait'
				) 
	) , //ends team_member
  
   'team_member_2'=>
  array ( 
	'Number' => 4,
	'display' => array ( 
				'image' => '1',
				'name' => '1',
				'image_mode' => 'portrait'
				) 
	) , //ends team_member2
	
   'similar_listings'=>
  array (
	'limit' => '3',
	  'Title' => 'Similar  Listings',
	  'display' => array ( 
				'type' => '1',
				'suburb' => '1',
				'property_type' => '1'
				) 
	), //ends similar_listings
  
  'similar_sold_listings'=>
  array (
	  'limit' => '3',
	  'Title' => 'Similar Sold Listings',
	  'display' => array ( 
				'type' => '1',
				'suburb' => '1',
				'property_type' => '1'
				) 
	), //ends similar_sold_listings
	
	'social_widget'=>
  array (
	  'display' => array ( 
				'del.icio.us' => '1',
				'Digg' => '1',
				'Facebook' => '1',
				'LinkedIn' => '1',
				'MySpace' => '1',
				'Twitter' => '1',
				) 
	), //ends social_widget
	
	'social_widget_2'=>
  array (
	  'display' => array ( 
				'del.icio.us' => '1',
				'Digg' => '1',
				'Facebook' => '1',
				'LinkedIn' => '1',
				'MySpace' => '1',
				'Twitter' => '1',
				) 
	), //ends social_widget2
	
   'action_buttons'=>
  array (
	  'email_friend' =>'1',
	  'print_listing' => '1',
	  'fav' => '1',
	  'map' => '1',
	  'walkability' => '1',
	  'video' => '1',
  ), //ends action_buttons
  
  'current_listings'=>
  array (
	  'sale' => array ('Number' => '0','Title' => 'Current Sales'),
	  'lease' => array ('Number' => '0','Title' => 'Current Lease'),
	  'Both' => array ('Number' => '1','Title' => 'Current Listings'),
  ), //ends current_listings
  
  'featured_listings'=>
  array (
	  'sale' => array ('Number' => '0','Title' => 'Featured Sales'),
	  'lease' => array ('Number' => '0','Title' => 'Featured Lease'),
	  'Both' => array ('Number' => '1','Title' => 'Featured Listings'),
  ), //ends featured_listings
   
     'tags'=>
  array (
	  'suburb' => 'For Sale by Suburb',
	  'new_day' => '14',
	  'price_option' => array (
		    '<800000',
		     '800000-999000',
		     '1000000-2000000',
		    '>2000000',
		  ),
  ), //ends tags  
   'map'=> array (
	  'map_mode' => 'normal',
	  'width' => '580',
	  'height' => '385',
  ), 
   'media'=> array ('mode'=>'thumbnail_lightbox') , 
   'stamp_duty_and_mortgage_calculator'=> 'both' , 
 'contact_agent'=>array (
	  'photo' =>  '1',
	  'name' =>'1',
	  'business_phone' => '1',
	  'mobile' => '1',
	  'fax' => '1',
	  'role' => '1',
	  'image_mode' => 'portrait',
	  'agent_form_mode' => 'popup',
  ), 
    'sales_data'=>array (
	  'suburb_profile' =>'1',
	  'sold_and_leased' => '1',
	  'similar_listings' => '1',
	  'walkability' => '1',
	  'map' => '1',
	  'map_mode' => 'normal',
	  'map_width' => '580',
	  'map_height' => '385',
	  'width' => '600',
	  'limit' => '3',
	  'Title' => 'Similar  Listings',
	  'display' => array ( 
				'type' => '1',
				'suburb' => '1',
				'property_type' => '1'
				) 
  ), 
  'map_and_walkscore'=>array (
	  'display' => 'map',
	  'map_mode' => 'normal',
	  'map_width' => '580',
	  'map_height' => '385',
	  'width' => '600',
  ),
  'property_table'=>array (
  'property_type' => array ('display' => '1','title' => 'Property Type', ),
  'price' => array ( 'display' => '1','title' => 'Price', ),
  'sold_at' => array ( 'title' => 'Sold At',),
  'sold_price' => array ('title' => 'Sold Price',),
  'leased_price' => array ( 'display' => '1',  'title' => 'Leased Price', ),
  'leased_at' => array ( 'title' => 'Leased At',),
  'land_size' => array ( 'title' => 'Land Size'),
  'building_size' => array ( 'title' => 'Building Size'),
  'year_built' => array ( 'display' => '1', 'title' => 'Year Built', ),
  'available_at' =>  array ('title' => 'Available At',),
  'auction_date' => array ('display' => '1', 'title' => 'Auction Date', ),
  'auction_place' => array ('display' => '1', 'title' => 'Auction Place', ),
  'auction_time' => array ('display' => '1','title' => 'Auction Time',),
  'energy_efficiency_rating' => array ('display' => '1','title' => 'EER',),
  'ext_link_1' => array ('display' => '1','title' => 'External Link',),
  'ext_link_2' => array ('display' => '1','title' => 'External Link',),
  'property_url' => array ('display' => '1','title' => 'Property Url',),
  'virtual_tour' => array ('display' => '1','title' => 'Virtual Tour',),
   'floorplans' => array ( 'display' => '1', 'title' => 'Floorplans',),
  ), //ends property tablez
 )//Ends array of widgets 
);//ends add_option('realty_widgets');
add_option('realty_general_settings',
array (
  'display_pagination' => 
  array (
    'above',
     'below',
  ),
  'number_of_listings_on_each_page' => '20',
  'number_of_page_links_on_pagination' => '10',
  'sold_/_leased_pages_same_layout_as_search_results_page' => '1',
  'display_sold_listings_for_days' => '365',
  'display_leased_listings_for_days' => '365',
  'display_leased_listings_for_days' => '365',
  'expire_date' => '365',
  'by_default_mark_team_members_to_be_displayed' => '1',
  'walk_score_key' => '2fa08a222d3b0014f29f790533c3cdf6',
  'emails_sent_from' => '',
  'bcc_all_mail_to' => '',
  'team_contact_form' =>  array ('popup',),
  'team_description_chars' => '50',
  'sort_listing_by'=> 'created_at',
  'sort_order'=>'1',
  'default_currency_code'=>'$',
)

); 
	
add_option('realty_featured_listings', array ()); 
add_option('realty_property_stickers',
array (
  'sticker_colour' => '#7e2a1d',
  'sticker_text' => 
  array (
    'sold' => 'Sold',
    'leased' => 'Leased',
    'under_offer' => 'Under Offer',
  ),
  'sticker_src' => 'wp-content/plugins/Realty/style/stickers/sticker0.png',
)

); 
add_option('realty_search_results',
array (  
  'headline' => '',
  'description' => '150',
  'address' => 
  array (
    0 => 'street_address',
    1 => 'suburb',
    2 => 'state',
  ),
  'building_size' => 
  array (
    0 => 'Sqms',
    1 => 'Sqyd',
    2 => 'Sqft',
    3 => 'Sqrs',
  ),
  'land_size' => 
  array (
    0 => 'Sqms',
    1 => 'Hectares',
    2 => 'Acres',
  ),
    'price' => '',
	'property_type' => '',
  'number_of_rooms' => 
  array (
    0 => 'bedrooms',
    1 => 'bathrooms',
    2 => 'carspaces',
  ),  
  'thumbnail' => '',
)
); 

add_option('realty_search_results_thumbnail',
array (  
  'address' => 
  array (
    0 => 'street_address',
    1 => 'suburb',
    2 => 'state',
  ),
    'price' => '',
	'property_type' => '',
  'number_of_rooms' => 
  array (
    0 => 'bedrooms',
    1 => 'bathrooms',
    2 => 'carspaces',
  ),  
  'thumbnail' => '',
)
);

add_option('realty_sold_and_leased', array ()); 
add_option('realty_team', array ()); 
add_option('realty_property_meta_title', 'Real Estate %listing_type% - %address%');
add_option('realty_property_meta_description', '%headline% | %description%');
add_option('realty_property_meta_num_of_desc', '100');
add_option('realty_property_meta_keywords', '%property_type% %listing_type%, %property_type% %address% ,  Real Estate %suburb%, %listing_type% %suburb%, Sell %property_type% %suburb%, Lease %property_type% %suburb%, Buy %property_type% %suburb%, real estate agent %suburb%, agent %suburb%');
add_option('realty_property_meta_other', '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="Content-Language" content="en" />');
add_option('realty_property_meta_list', '%address%, %description%, %headline%, %listing_type%, %property_type%, %postcode%, %region%, %state%, %suburb%');
	
?>