<?php if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ ?>

<ul class="rooms">
	<?php if(!empty($realty->property['bedrooms'])){ ?><li class="bedrooms"><span class="room_count"><?php echo $realty->property['bedrooms']; ?></span><span class="room_type"></span></li><?php } ?>
   <?php if(!empty($realty->property['bathrooms'])){ ?> <li class="bathrooms"><span class="room_count"><?php echo $realty->property['bathrooms']; ?></span><span class="room_type"></span></li><?php } ?>
   <?php if(!empty($realty->property['carspaces'])){ ?> <li class="carspaces"><span class="room_count"><?php echo $realty->property['carspaces']; ?></span><span class="room_type"></span></li><?php } ?>
</ul>

<h2><?php echo $realty->property['headline'];?></h2>

<div id="property_description">
<p class="property_description"><?php echo nl2br($realty->property['description']); ?></p>
</div>