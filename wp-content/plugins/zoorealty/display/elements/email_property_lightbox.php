<div class="lightbox-wrap">
    <h2>Email to a self</h2>
    <div id="return_self" class="center"></div>
    <ol class="cf-ol">
        <li><label><span>First Name <span class="red">*</span> </span></label><input type="text" size="37" name="first_name" id="sfirst_name"/></li>
        <li><label><span>Last Name</span></label><input type="text"  size="37" name="last_name" id="slast_name" /></li>
        <li><label><span>Email <span class="red">*</span> </span></label><input type="text" size="37" name="email" id="semail" size="45" /></li>
        <li><label>&nbsp;</label><input type="button" class="btn" value="Send" onClick="send_email_self();"><div class="clear"></div></li>
    </ol>
    <input type="hidden" id="property_id_self" value="<?php echo $_GET['id']; ?>" />
</div>