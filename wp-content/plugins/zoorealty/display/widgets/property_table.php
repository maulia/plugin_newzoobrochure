<?php 	
if(!function_exists('print_item')):
	function print_item($key, $value, $printKey = true, $keyName =''){ 
		global $helper;
		if(is_array($value)):
			foreach($value as $innerKey=>$innerValue)
				print_item($innerKey, $innerValue);
		else:
			if(empty($keyName) || $keyName==1) 
				$keyName = $helper->humanize($key);
			if($helper->validUrl($value))
				$value = '<a target="_blank" href="'.$value.'" title="'.$keyName.'" class="special_link">'.$keyName.'</a>';
			?>
			<tr class="<?php echo $key; ?>">
			<?php if($printKey): ?>
					<td class="field"><?php echo $keyName; ?> </td>
			<?php endif; ?>
					<td class="value"><?php echo $value; ?></td> 
			</tr>
<?php 	endif; 
	}
 endif; 
if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ ?>

<div id="details">
	<?php if(!empty($realty->property['bedrooms']) || !empty($realty->property['bathrooms']) || !empty($realty->property['carspaces'])){?>
	<?php /*  <?php echo $before_title; ?><span>Room</span> Details<?php echo $after_title; ?> */ ?>
	<div class="block_content">	
		<ul class="rooms">
            <?php if(!empty($realty->property['bedrooms'])){ ?><li class="bedrooms"><img src="<?php bloginfo('template_url'); ?>/images/icons/bed.png"><span class="num"><?php echo $realty->property['bedrooms']; ?></span></li><?php } ?>
            <?php if(!empty($realty->property['bathrooms'])){ ?> <li class="bathrooms"><img src="<?php bloginfo('template_url'); ?>/images/icons/bath.png"><span class="num"><?php echo $realty->property['bathrooms']; ?></span></li><?php } ?>
            <?php if(!empty($realty->property['carspaces'])){ ?> <li class="carspaces"><img src="<?php bloginfo('template_url'); ?>/images/icons/car.png"><span class="num"><?php echo $realty->property['carspaces']; ?></span></li><?php } ?>
        </ul>
        <?php }?>
        <div class="clear"></div>
        
        
       
	</div>

	<?php echo $before_title; ?><span>Property</span> Details<?php echo $after_title; ?>
	<div class="block_content">
		<table class="property_details_table" width="100%" cellpadding="0" cellspacing="0">
		<?php  
		foreach($settings as $key=>$values):
			if( !is_array($values) || !$values['display']  || empty($realty->property[$key]) || $key =='street_address' || $key=='headline' || $key=='lease_plus_another') continue;
			switch($key):
				case 'land_size': case 'building_size':case 'office_area':case 'warehouse_area':case 'retail_area':case 'other_area':
				if($key=='land_size')$unit_size='land_area_metric';
				if($key=='building_size')$unit_size='floor_area_metric';
				if($key=='office_area')$unit_size='office_area_metric';
				if($key=='warehouse_area')$unit_size='warehouse_area_metric';
				if($key=='retail_area')$unit_size='retail_area_metric';
				if($key=='other_area')$unit_size='other_area_metric';

				?>
				<tr class="side_land_size">
					<td class="field"><?php echo $values['title']; ?> </td>
					<td class="value">
						<span><?php echo $realty->property[$key]." ".$realty->property[$unit_size]; ?></span>
					</td>
				</tr>	
				<?php
					break;
				case 'sold_at' : case 'leased_at' : case 'lease_end' : case 'lease_commencement' : 
					if ($realty->property[$key]!='0000-00-00' && $realty->property[$key]!="" && $realty->property[$key]!="01/01/1970"):
					?>
						<tr class="<?php echo $key; ?>">
							<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
							<td class="value"><?php 
							if(date("d/m/Y",strtotime($realty->property[$key]))!='01/01/1970')echo date("d/m/Y",strtotime($realty->property[$key]));
							else echo $realty->property[$key]; ?></td>
						</tr>	
					<?php
					endif;
				break;
				case 'available_at' :
					if ($realty->property[$key]!='0000-00-00' && $realty->property[$key]!="" && $realty->property['type']!="Commercial"):
					?>
						<tr class="<?php echo $key; ?>">
							<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
							<td class="value"><?php echo date("d/m/Y",strtotime($realty->property[$key])); ?>
							</td>
						</tr>	
					<?php
					endif;
				break;
				case 'price' :  ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value" id="property_price">
						<?php if($realty->property['status']=='2')echo ($realty->property['display_sold_price'])? $realty->property['last_price']:'Undisclosed';
						else echo $realty->property[$key];	?>
						</td>
					</tr>	
				<?php
				break;
				case 'sold_price' :case 'leased_price' :  ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value"><?php echo '$'.number_format($realty->property[$key]); ?>
						</td>
					</tr>	
				<?php
				break;
				case 'bond' :  ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value"><?php echo '$'.number_format($realty->property[$key]); ?>
						</td>
					</tr>
					<?php if($realty->property['status']!='2' && $realty->property['status']!='6'){?>
					<tr class="total">
						<td class="field">Total<span class="colon" style="display:none;"> :</span></td>
						<td class="value"><?php 
						if($realty->property['price_display']=='1'){
							$total_price=intval($realty->property['based_price'])+intval($realty->property['bond']);
							echo '$'.number_format($total_price);
						}else{
							echo 'Undisclosed';
						} ?>
						</td>
					</tr>
					<?php } ?>	
				<?php
				break;
				case 'current_rent' :case 'rent_pa' :
					if($realty->property['price_display']==1){ ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value"><?php echo '$'.number_format($realty->property[$key]); ?>
						</td>
					</tr>	
				<?php
					}
				break;
				case 'rental_price' : 
					if($realty->property['price_display']==1){ ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value">
							<?php 
							echo '$'.number_format($realty->property[$key]); 
							if(!empty($realty->property['rental_price_to']))echo ' - $'.number_format($realty->property['rental_price_to']); 
							?>
						</td>
					</tr>						
				<?php
					}
				break;
				case 'sale_price' :  ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value">
							<?php 
							echo '$'.number_format($realty->property[$key]); 
							if(!empty($realty->property['sale_price_to']))echo ' - $'.number_format($realty->property['sale_price_to']); 
							?>
						</td>
					</tr>	
				<?php
				break;
				case 'area_range' :  ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value">
							<?php 
							echo $realty->property[$key]; 
							if(!empty($realty->property['area_range_to']))echo ' - '.$realty->property['area_range_to']; 
							echo "m2";
							?>
						</td>
					</tr>	
				<?php
				break;
				case 'garage_area' : case 'land_depth': case 'land_width' : ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value"><?php echo $realty->property[$key]." m<superscript>2</superscript>"; ?>
						</td>
					</tr>	
				<?php
				break;
				case 'occupancy' :  ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value"><?php echo ($realty->property[$key]=='1')?'Tenanted Investment':'Vacant Possession'; ?>
						</td>
					</tr>	
				<?php
				break;
				case 'lease_option' : 
					if($realty->property['lease_option']!='Please select'){ ?>
					<tr class="<?php echo $key; ?>">
						<td class="field">Lease Option </td>
						<td class="value"><?php echo 
						str_replace(" Years","",$realty->property['lease_option']);
						if($realty->property['lease_plus_another']!='Please select')echo "*".str_replace(" Years","",$realty->property['lease_plus_another'])." Years";
						?>
						</td>
					</tr>	
				<?php
					}
				break;
				case 'auction_time' :
					if (!empty($realty->property[$key]) && $realty->property[$key]!='00:00:00' && $realty->property['status'] != '2' &&  $realty->property['status'] != '6'):
					?>
						<tr class="<?php echo $key; ?>">
							<td class="field"><?php echo $values['title']; ?> </td>
							<td class="value"><?php echo $realty->property[$key]; ?>
							</td>
						</tr>	
					<?php
					endif;
				break;
				case 'auction_date' :
					if (!empty($realty->property[$key]) && $realty->property[$key]!='0000-00-00' && $realty->property['status'] != '2' &&  $realty->property['status'] != '6' ):
					?>
						<tr class="<?php echo $key; ?>">
							<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
							<td class="value"><a href="<?php echo $realty->pluginUrl."display/elements/crm.php?property_id=".$realty->property['id']."&time=".$realty->property['auction_time']; ?>" title="Save to Calendar">Cal</a><?php echo date('l jS F Y',strtotime($realty->property['auction_date']));?></td>
						</tr>	
					<?php
					endif;
				break;
				case 'auction_place' :
					if ($realty->property['status'] != '2' &&  $realty->property['status'] != '6' ):
					?>
						<tr class="<?php echo $key; ?>">
							<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
							<td class="value"><?php echo $realty->property[$key];?></td>
						</tr>	
					<?php
					endif;
				break;
				case 'return_percent': ?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?> </td>
						<td class="value"><?php echo $realty->property[$key]."%"; ?>
						</td>
					</tr>	
				<?php
				break;
				case 'estimate_rental_return' :case 'tax_rate': case 'condo_strata_fee':
					$key_period=$realty->property[$key.'_period'];
					switch($key_period){
						case 'Per Week': $period=' pw';break;
						case 'Per Month': $period=' pm';break;
						case 'Per Quarter': $period=' pq';break;
						case 'Per Year': $period=' py';break;
						default :
						$period=$key_period;
						break;
					}
					
				?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?> </td>
						<td class="value"><?php echo '$'.number_format($realty->property[$key]).$period; ?>
						</td>
					</tr>	
				<?php
				break;

				case 'floorplans':
				?>
				<tr class="<?php echo $key; ?>">
					<td class="field"><?php echo $values['title']; ?> </td>
					<td class="value">
						<ul>
						<?php foreach($realty->property['floorplans'] as $floorplan){ ?>
						<li><a href="<?php echo $floorplan['large']; ?>" title="<?php echo $floorplan['title']; ?>" onclick="return false;" rel="prettyPhoto[floorplan]"><?php echo ($floorplan['title']=='')?'Slideshow':$floorplan['title']; ?></a></li>
						<?php } ?>
						</ul>
					</td>
				</tr>
				<?php
				break;
				case 'virtual_tour':
				?>
				<tr class="detail_virtual_tour">
						<td class="field"><?php echo $values['title']; ?> </td>
						<td class="vr_tour vr_tour_1"><a href="<?php echo $realty->property[$key]; ?>" target="_blank" class="special_link">VR Tours</a></td>
					</tr>
				<?php
					break;	

				case 'ext_link_1':case 'ext_link_2':case 'property_url':
				?>
				<tr class="detail_virtual_tour">
						<td class="field"><?php echo $values['title']; ?> </td>
						<td class="vr_tour vr_tour_1"><a href="<?php echo (strpos($realty->property[$key],'http')===false)?'http://'.$realty->property[$key]: $realty->property[$key]; ?>" target="_blank" class="special_link">Click</a></td>
					</tr>
				<?php
				break;
				case 'brochure': 
				// $brochures=$realty->brochure(); 
				$brochures=$realty->property['brochure']; if(empty($brochures))continue;
				?>
					<tr class="detail_brochure">
						<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
						<td class="value">
						<?php foreach($brochures as $item): ?>
							<a href="#" onclick="javascript:window.open('<?php echo $item['url'];?>', '', 'width=800, height=1000,resizable=1,scrollbars=1');return false;" title="<?php echo ($item['title']=='')?'Brochure':$item['title'];?>"><?php echo ($item['title']=='')?'Brochure':$item['title'];?></a><br/>
						<?php endforeach; ?>
						</td>
					</tr>
				<?php
				break;		
	
				case 'type':
					switch($realty->property[$key]){
					case "BusinessSale":
						$item='Business Sale';
					break;
					case "HolidayLease":
						 $item='Holiday Lease';
					break;
					case "ProjectSale":
						$item='Project Sale';
					break;	
					case "ResidentialLease":
						$item='Residential Lease';
					break;
					case "ResidentialSale":
						$item='Residential Sale';
					break;
					default:
						$item=$realty->property[$key];
					break;
				}
				?>
					<tr class="<?php echo $key; ?>">
						<td class="field"><?php echo $values['title']; ?> </td>
						<td class="value"><?php echo $item; ?> </td>
					</tr>
				<?php
				break;
				case 'plus_outgoings':
					if($realty->property[$key]=='1')$text='Yes';
					print_item($key, $text, true, $settings[$key]['title'] );
				break;
				case 'all_the_building':
					if($realty->property[$key]=='1')$text='Whole Building';
					else $text='Part Building';
					print_item($key, $text, true, $settings[$key]['title'] );
				break;
				case 'tax':
					if($realty->property[$key]=='2')$text='Exclusive';
					else $text='Inclusive';
					print_item($key, $text, true, $settings[$key]['title'] );
				break;
				default:
					print_item($key, $realty->property[$key], true, $settings[$key]['title'] );
				break;
			endswitch;
		endforeach; ?>
		<?php
		global $wpdb;
		$chk_zoning = $wpdb->get_row("SELECT * FROM apzonings WHERE property_id = ".$realty->property['id']."", ARRAY_A);
		$code_zoning = $chk_zoning['zoning_code'];
		$zoning = $wpdb->get_row("SELECT * FROM apzonings_category WHERE zoning_code = '".$code_zoning."'", ARRAY_A);
		if ($chk_zoning):		
			if (($realty->property['status']=='1' && $realty->property['listing_type']=='FOR SALE') || ($realty->property['status']=='4' && $realty->property['listing_type']=='FOR SALE')): ?>
				<tr class="<?php echo 'zoning'; ?>">
					<td class="field"><?php echo 'Zoning'; ?><span class="colon" style="display:none;"> :</span></td>
					<td class="value">
						<a href="#zoning-popup" class="open-popup-link"><?php echo $zoning['zoning_code']." ".$zoning['zoning_name']?></a>
					</td>
				</tr>
			<?php				
			endif; ?>
			<div id="zoning-popup" class="white-popup mfp-hide">
			  Zoning content
			</div>
		<script type="text/javascript">
	        jQuery(document).ready(function(){
	            jQuery('.open-popup-link').magnificPopup({
				  type:'inline',
				  midClick: true 
				});
	        });
        </script>	
		<?php endif; ?>
		<?php if (!empty($realty->property['opentimes'])): ?>
		<tr class="detail_open_times">
			<td class="field">Open Times </td>
			<td class="value">
			<?php foreach($realty->property['opentimes'] as $opentime): 
				$start_date=$opentime['date']." ".$opentime['start_time'];	$end_date=$opentime['date']." ".$opentime['end_time'];	?>
				<a href="<?php echo $realty->pluginUrl."display/elements/crm.php?property_id=".$realty->property['id']."&start_date=$start_date&end_date=$end_date"; ?>" title="Add to Calendar" class="calendar">cal</a>
				<span style="margin-left: 20px;"><strong><?php echo date('l, j M Y', strtotime($opentime['date'])); ?></strong>, <?php echo date('g:i a',strtotime($opentime['start_time'])); ?> - <?php echo date('g:i a',strtotime($opentime['end_time'])); ?></span><br/>
			<?php endforeach; ?>
			</td>
		</tr>
		<?php endif; ?>
		</table>
		<p class="link-back">
			<a href="javascript:history.go(-1);">Return to Search List</a>
			<?php
			global $helper, $wpdb;
			$check=$wpdb->get_results("SELECT page_id FROM LocationInfo_suburbs where suburb in('".esc_sql($realty->property['suburb'])."')", ARRAY_A);
			if($check){ 
				$suburb_profiles_page=get_permalink($page_id);
				?>
				<span style="margin:left:15px;"><a href="<?php echo $suburb_profiles_page; ?>"><?php echo ucwords(strtolower($realty->property['suburb'])); ?> Profile</a></span>
			<?php }
			?>
		</p>
	</div>
</div><!-- end #details -->
