<?php
if(!class_exists('my_db'))
{
	require_once(dirname(__FILE__).'/my_db.class.php');
}

class wp_page_maker extends my_db{

	function get_page_id($post_name){
		global $table_prefix;
		$sql = "SELECT `ID` FROM " . $table_prefix . "posts AS posts_1 WHERE `post_name`='$post_name' OR `post_title`='$post_name' LIMIT 1";
		return $this->get_var($sql);
	}//ends function
		
	function get_permalink($post_name){		
			return get_option('home') . "/?page_id=" . $this->get_page_id($post_name);	
	}//ends function
	function create_page($post_title, $post_content, $page_template='default', $post_name='',$post_type='page',$post_parent=0, $ID=''){//Syntax compatibility
		return make_page($post_title, $post_content, $page_template, $post_name,$post_type,$post_parent, $ID);
	}
	function make_page($post_title, $post_content, $page_template='default', $post_name='',$post_type='page',$post_parent=0, $ID=''){
			global $table_prefix;
			if(empty($post_name))
				$post_name = $this->clean_filename(trim($post_title));
			if ($this->get_page_id($post_name) != false)	return false; // If an ID has been returned by this function, the post already exist. Return immediately	
			$sql = "INSERT INTO `" . $table_prefix . "posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_category`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
			
			('$ID', 1, NOW(), '', '" . mysql_real_escape_string ($post_content) . "', '" . mysql_real_escape_string ($post_title) . "', 0, '', 'publish', 'open', 'open', '', '$post_name', '', '', '', '', '', 0, '', 1, '$post_type', '', 0)			
			";
			$this->query($sql);
			$this->query("INSERT INTO `" . $table_prefix . "postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES 
			('', '" . mysql_insert_id() . "', '_wp_page_template', '$page_template') ");
			
	}//ends function
	
	function clean_filename(&$filename){//function to clean a filename string so it is a valid filename
		//$reserved = preg_quote('\/:*?"<>|', '/');//characters that are  illegal on any of the 3 major OS's
		//replaces all characters up through space and all past ~ along with the above reserved characters
		//return strtolower (preg_replace("/([\\x00-\\x40\\x7f-\\xff{$reserved}])/e", "-", $filename));
		
		$filename = strtolower (str_replace(" ","",preg_replace("/[^a-z0-9-]/", "-", $filename)));
	}//ends function
	
	function get_post_name($identifier){
		global $table_prefix;
		$sql = "SELECT `post_name` FROM " . $table_prefix . "posts AS posts_1 WHERE `ID`='$identifier' OR `post_title`='$identifier' LIMIT 1";
		return $this->get_var($sql);
	}//ends function
	
	//Returns the current post_name, no matter how the query has been changed
	function wp_post_name(){
		global $wp_query;		
		$page_obj = $wp_query->get_queried_object(); 
		return $page_obj->post_name;

	}


}//ends class		
?>