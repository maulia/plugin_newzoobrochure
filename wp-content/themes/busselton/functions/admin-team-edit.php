<?php
global $realty;
$uploaded_path=ABSPATH . 'wp-content/uploads/zooproperty_api/attachments/user_photos/';
$uploaded_url=$realty->siteUrl.'wp-content/uploads/zooproperty_api/attachments/user_photos/';
$groups=array('Executive','Sales Agent','Property Manager','Admin','Industrial','Office Leasing','Office Sales','Retail','Other');
$url=get_bloginfo('stylesheet_directory') ."/functions/testimonials.php?user_id=$id&id=";
$load_testi=get_bloginfo('stylesheet_directory') ."/functions/testimonials_process.php?";

if(isset($_POST['submit'])){
	foreach($_POST as $key=>$value){
		if(trim($value)==''){
			switch($key){
				case 'firstname':case 'lastname':case 'email':
					 $return.=$helper->humanize($key)." can not be empty.<br/>"; break;					
				break;
			}
		}		
	}
	if(empty($return)){
		
		$_POST['landscape']=$_POST['landscape_path'];
		$_POST['portrait']=$_POST['portrait_path'];
		$userfile_name = $_FILES['portrait']['name']; 
		$userfile_tmp = $_FILES['portrait']['tmp_name']; 
		if(!empty($userfile_tmp)){
			$logo=$userfile_name;
			$logos_upload = $uploaded_path.$_POST['id'].'/';
			if(!save_file($userfile_tmp, $logos_upload.$logo))$return.="Photo can not be saved. Please try again.<br/>";
			else{				
				$_POST['portrait']=$uploaded_url.$_POST['id']."/$logo";
				$helper->query("delete from attachments where parent_id=$id and type='portrait' and description='large' and position='0'");
				$helper->query("insert into attachments (id, is_user, description, url, parent_id, type, created_at, updated_at, position ) values('','1','large','".$_POST['portrait']."','$id','portrait',NOW(),NOW(),'0')");
			}
		}		

		$userfile_name = $_FILES['landscape']['name']; 
		$userfile_tmp = $_FILES['landscape']['tmp_name']; 
		if(!empty($userfile_tmp)){
			$logo=$userfile_name;
			$logos_upload = $uploaded_path.$_POST['id'].'/';
			if(!save_file($userfile_tmp, $logos_upload.$logo))$return.="Photo can not be saved. Please try again.<br/>";
			else{
				$_POST['landscape']=$uploaded_url.$_POST['id']."/$logo";
				$helper->query("delete from attachments where parent_id=$id and type='landscape' and description='large' and position='0'");
				$helper->query("insert into attachments (id, is_user, description, url, parent_id, type, created_at, updated_at, position ) values('','1','large','".$_POST['landscape']."','$id','landscape',NOW(),NOW(),'0')");
			}
		}			
		
		$updated_at=date("Y-m-d h:i:s");		
		$user['firstname']=stripslashes(str_replace('\\','',$user['firstname']));
		$user['lastname']=stripslashes(str_replace('\\','',$user['lastname']));
		$user['description']=stripslashes(str_replace('\\','',$user['description']));
		
		$helper->query("update users set firstname='".mysql_escape_string($_POST['firstname'])."', lastname='".mysql_escape_string($_POST['lastname'])."', phone='".$_POST['phone']."', mobile='".$_POST['mobile']."', fax='".$_POST['fax']."', email='".$_POST['email']."', role='".mysql_escape_string($_POST['role'])."', `group`='".$_POST['group']."', description='".mysql_escape_string($_POST['description'])."', facebook_username='".$_POST['facebook_username']."', twitter_username='".$_POST['twitter_username']."', linkedin_username='".$_POST['linkedin_username']."', office_id='".$_POST['office_id']."', updated_at='$updated_at', disable_update='1' where id=".$_GET['user_id']);
		$return="New agent was successfully updated.";

	}
	$user=$_POST;	
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
}

$user['firstname']=stripslashes(str_replace('\\','',$user['firstname']));
$user['lastname']=stripslashes(str_replace('\\','',$user['lastname']));

function save_file($url, $file){
	if(!@file_exists(dirname($file)))
		@mkdir(dirname($file), 0777);
	else{
		if(@file_exists($file)){
			@chmod($file);
			@unlink($file);
		}
	}
	
	$content = @file_get_contents($url);
	if($content)
		$result = @file_put_contents($file, $content);
	else
		$result = false;
	
	if($result)
		@chmod($file, 0777);
	
	return $result;
}
?>
<form method="post" action="" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?php echo $_GET['user_id']; ?>">
	<div class="leftside">		
		<table class="widefat">
			<thead>
			<tr>
				<th colspan="2" scope="col" class="manage-column column-title">Agent Data</th>
			</tr>
			</thead> 
			<tr>
                <td class="label"><strong>First Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="widefat" id="firstname" name="firstname"  value="<?php echo $user['firstname']; ?>">						
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Last Name<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="widefat" id="lastname" name="lastname"  value="<?php echo $user['lastname']; ?>">						
                </td>
            </tr>
           <tr>
                <td class="label"><strong>Office Name<span class="red"> *</span></strong></td>
                <td class="selectOffice">
					<select class="widefat" name="office_id">
						<?php foreach($offices as $office_id=>$name){ ?>
						<option value="<?php echo $office_id; ?>" <?php if($office_id==$user['office_id'])echo 'selected="selected"'; ?>><?php echo $name; ?></option>
						<?php } ?>
					</select>
                </td>
            </tr>
            <tr>
                <td class="label">Role</td>
                <td><input type="text" name="role" class="widefat"  value="<?php echo $user['role']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Group</td>
                <td>
                <select name="group" class="widefat">
                    <option value="">Please Select</option>
                    <?php foreach($groups as $group){ ?>
                    <option value="<?php echo $group; ?>" <?php if($group==$user['group'])echo 'selected="selected"'; ?>><?php echo $group; ?></option>
                    <?php } ?>
                </select>
                </td>
            </tr>
            <tr>
                <td class="label"><strong>Email<span class="red"> *</span></strong></td>
                <td>
                    <input type="text" class="widefat" id="email" name="email"  value="<?php echo $user['email']; ?>">
                </td>
            </tr>
            <tr>
                <td class="label">Phone</td>
                <td><input type="text" name="phone" class="widefat"  value="<?php echo $user['phone']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Mobile</td>
                <td><input type="text" name="mobile" class="widefat"  value="<?php echo $user['mobile']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Fax</td>
                <td><input type="text" id="fax" name="fax" class="widefat"  value="<?php echo $user['fax']; ?>"></td>
            </tr>
            <tr>
                <td class="label">Description</td>
                <td>
                    <textarea name="description" rows="20" class="widefat"><?php echo $user['description']; ?></textarea>
                </td>
            </tr>
			<tr>
				<td class="label">Facebook account</td>
				<td>www.facebook.com/ <input type="text" name="facebook_username" class="widefat"  value="<?php echo $user['facebook_username']; ?>"></td>
			</tr>	
			<tr>
				<td class="label">Twitter account</td>
				<td>www.twitter.com/ <input type="text" name="twitter_username" class="widefat"  value="<?php echo $user['twitter_username']; ?>"></td>
			</tr>	
			<tr>
				<td class="label">Linkedin account</td>
				<td>www.linkedin.com/ <input type="text" name="linkedin_username" class="widefat"  value="<?php echo $user['linkedin_username']; ?>"></td>
			</tr>		
		 </table> 
		 
		<table class="widefat">
			<thead>
			<tr>
				<th colspan="2" scope="col" class="manage-column column-title">Photos</th>
			</tr>
			</thead> 
			<tr>
				<td class="label">Portrait (100px X 125px)</td>
				<td>
					<?php if(!empty($user['portrait'])){ ?>
					<img src="<?php echo $user['portrait']; ?>" class="logo"><br/>
					<input type="hidden" value="<?php echo $user['portrait']; ?>" name="portrait_path">
					<?php } ?>
					<input type="file" name="portrait" value="">
				</td>
			</tr>	
			<tr>
				<td class="label">Landscape (250px X 125px)</td>
				<td>
					<?php if(!empty($user['landscape'])){ ?>
					<img src="<?php echo $user['landscape']; ?>" class="logo"><br/>
					<input type="hidden" value="<?php echo $user['landscape']; ?>" name="landscape_path">
					<?php } ?>
					<input type="file" name="landscape" value="">
				</td>
			</tr>	
		</table>	
		
		<div id="testimonials">
			<table class="widefat">
				<thead>
				<tr>
					<th colspan="2" scope="col" class="manage-column column-title">Testimonials</th>
				</tr>
				</thead> 
				<?php if(empty($user['testimonials'])){ ?>
				<tr>
					<td class="label" colspan="2">There is no testimonial for this agent.</td>               
				</tr>			
				<?php }else{
				foreach($user['testimonials'] as $item){	?>			
				<tr>
					<td class="label"><?php echo stripslashes(str_replace('\\','',$item['user_name'])); ?></td>
					<td>
						<a href="#" onclick="open_testi('<?php echo $item['id']; ?>');">Edit</a>&nbsp;|&nbsp;<a class="delete" onClick="return confirm('You are about to delete the this testimonial. \n\'OK\' to delete, \'Cancel\' to stop.' );" href="javascript:del_testi('<?php echo $item['id']; ?>')">Delete</a>		
					</td>
				</tr>
			   <?php } } ?>
			   <tr>
					<td colspan="2"><a href="#" onclick="open_testi('');">Add Testimonial</a></td>               
				</tr>
			 </table> 
		 </div>
		 
		 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</div>
			
	<div id="poststuff">
		<div class="postbox">
			<h3>Actions</h3>		
			<div class="inside">
				<p class="submit">
				<input name="submit" type="submit" value="Save changes" class="button"/>
				</p>
				<a href="themes.php?page=theme-settings&tab=team" class="button">Back</a>
				</p>			
			</div>	
		</div>
	</div>
</form>
<div id="filter_testi" class="filter_lightbox"></div>
<div id="box_testi" class="box_lightbox">
	<div id="load_form_testi">
	
	</div>
</div>

<style type="text/css">
.filter_lightbox {
	display:none; position:fixed; top:0; left:0; width:100%; height: 1400px; z-index:1000; 
	background-color: #000; opacity:0.9; filter: alpha(opacity=90);
}

.box_lightbox {
	display:none; width:600px; position: absolute; top:130px; left:43%; padding:0; margin:0 0 0 -300px; z-index:1001;
	background:#FFFFFF; box-shadow: 0 0 2px rgba(0,0,0,.9); border-radius:3px;
}

form.form-profile { padding:25px; }
form.form-profile h3 {
	background-color:#f1f1f1;
	background-image:-ms-linear-gradient(top,#f9f9f9,#ececec);
	background-image:-moz-linear-gradient(top,#f9f9f9,#ececec);
	background-image:-o-linear-gradient(top,#f9f9f9,#ececec);
	background-image:-webkit-gradient(linear,left top,left bottom,from(#f9f9f9),to(#ececec));
	background-image:-webkit-linear-gradient(top,#f9f9f9,#ececec);
	background-image:linear-gradient(top,#f9f9f9,#ececec);
	
	margin:0; padding:8px 10px;
    border: solid 1px #DFDFDF; box-shadow: 0 1px 0 #FFFFFF inset; border-radius:3px 3px 0 0;
    font-size: 13px; color: #464646; text-shadow: 0 1px 0 #FFFFFF;
}
form.form-profile table.widefat { margin:0; border-width:0 1px; border-radius:0; }
form.form-profile table.widefat td { padding:7px; }
form.form-profile textarea { height:200px; }
form.form-profile .button_testi input.button { width:100px; }

</style>

<script type="text/javascript">
var load_testi="<?php echo $load_testi; ?>";

function open_testi(id){
  jQuery('#load_form_testi').load("<?php echo $url;?>"+id);
  document.getElementById('filter_testi').style.display='block';  
  document.getElementById('box_testi').style.display='block'; 	 
}
function del_testi(id){
  jQuery('#testimonials').load(load_testi+"<?php echo "user_id=$id&id="; ?>"+id+"&task=delete");
}
function close_testi(){
   document.getElementById('load_form_testi').innerHTML='';
   document.getElementById('filter_testi').style.display='none';
   document.getElementById('box_testi').style.display='none';
}
</script>