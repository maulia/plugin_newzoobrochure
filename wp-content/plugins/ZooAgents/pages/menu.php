<?php if(!empty($_GET['office_id']))$query_string="?office_id=".$_GET['office_id']; ?>
<div class="dashboard-nav">
    <ul class="menu">		
		<li<?php if(is_page('listings') || is_page('media') || is_page('new-listing') || is_page('detail') || is_page('opens') || is_page('status') || is_page('active')  || is_page('payment')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/listings/'.$query_string; ?>">Listings</a></li>
        <li<?php if(is_page('agents') || is_page('new-agent') || is_page('edit-agent')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/agents/'.$query_string; ?>">Team</a></li>
        <li<?php if(is_page('profile')) { echo ' class="current"'; } ?>><a href="<?php echo $realty->siteUrl.'dashboard/profile/'.$query_string; ?>">Profile</a></li>
        <li><a href="<?php echo $realty->siteUrl.'agent-login/?action=logout'; ?>">Log Out</a></li>
    </ul>
    <div class="right-round"></div>
    <div class="clear"></div>
</div>