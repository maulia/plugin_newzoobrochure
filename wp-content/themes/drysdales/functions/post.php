<?php
/**********************************************************************************************************************************************
Add Post Thumbnail Images
**********************************************************************************************************************************************/
if (function_exists('add_theme_support')) {
add_theme_support( 'post-thumbnails', array( 'post' ) );
set_post_thumbnail_size( 150, 150, true );
}
/**********************************************************************************************************************************************
Get Absolute Ancestor
**********************************************************************************************************************************************/
function get_absolute_ancestor($post_id){//Returns the absolute ancestor (parent, grandparent, great-grandparent if there is, etc.) of a post. The absolute ancestor is defined as a page that doesnt have further parents, that is, its post parent is '0' 
		global $wpdb;		 
		 $parent = $wpdb->get_var("SELECT `post_parent` FROM $wpdb->posts WHERE `ID`= $post_id");		 
		 if($parent == 0) //Return from the recursion with the title of the absolute ancestor post.
			return $wpdb->get_var("SELECT * FROM $wpdb->posts WHERE `ID`= $post_id");
		 return get_absolute_ancestor($parent);		 	
	}//ends function
	
/**********************************************************************************************************************************************
Custom Comments Template
**********************************************************************************************************************************************/
function mytheme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment">

            <div class="comment-author vcard">
            	<?php echo get_avatar($comment,$size='40'); ?>
            </div>
            
            <div class="comment-author-name">
            	<p class="comment-says"><?php printf(__('<span class="fn">%s</span> <span class="says">says:</span>'), get_comment_author_link()) ?></p>
                <?php if ($comment->comment_approved == '0') : ?>
                    <p class="comment-wait"><?php _e('Your comment is awaiting moderation.') ?></p>
                <?php endif; ?>
                <p class="comment-meta commentmetadata">
                	<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(),  get_comment_time()) ?></a>
                	<?php edit_comment_link('(Edit)','  ','') ?>
                </p>
            </div>
            
            <div class="comment-content"><?php comment_text(); ?></div>
            
            <div class="reply">
				<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>
            
		</div>
<?php
}

/**********************************************************************************************************************************************
LIMIT POSTS
**********************************************************************************************************************************************/
function the_content_limit($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);

   if (strlen($_GET['p']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo $content . ' &hellip; ' ;
        echo "<a href='";
        the_permalink();
        echo "'>".$more_link_text."</a></p>";
   }
   else {
      echo $content;
   }
}

function list_recent_comments ($number_comments = 5) {
		global $wpdb;
		$comments = $wpdb->get_results("SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type,comment_author_url, SUBSTRING(comment_content,1,70) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID =$wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $number_comments");
		foreach ($comments as $comment) {
		$comment->comment_date_gmt = date("jS F Y",strtotime($comment->comment_date_gmt));
		$output .= "\n<li class=\"li_border\"><strong><a href=\"" . get_permalink($comment->ID) . "#comment-" . $comment->comment_ID . "\" title=\"on " . $comment->post_title . "\">".strip_tags($comment->comment_author)."</a> - ". $comment->comment_date_gmt ."</strong><br />" . strip_tags($comment->com_excerpt) ." &hellip;</li>";
		}
		echo $output;
} //ends function list_recent_comments

/**********************************************************************************************************************************************
THE EXCERPT RELOADED
**********************************************************************************************************************************************/
function wp_the_excerpt_reloaded($args='') {
	parse_str($args);
	if(!isset($excerpt_length)) $excerpt_length = 120; // length of excerpt in words. -1 to display all excerpt/content
	if(!isset($allowedtags)) $allowedtags = '<a>'; // HTML tags allowed in excerpt, 'all' to allow all tags.
	if(!isset($filter_type)) $filter_type = 'none'; // format filter used => 'content', 'excerpt', 'content_rss', 'excerpt_rss', 'none'
	if(!isset($use_more_link)) $use_more_link = 1; // display
	if(!isset($more_link_text)) $more_link_text = "(more...)";
	if(!isset($force_more)) $force_more = 1;
	if(!isset($fakeit)) $fakeit = 1;
	if(!isset($fix_tags)) $fix_tags = 1;
	if(!isset($no_more)) $no_more = 0;
	if(!isset($more_tag)) $more_tag = 'div';
	if(!isset($more_link_title)) $more_link_title = 'Continue reading this entry';
	if(!isset($showdots)) $showdots = 1;

	return the_excerpt_reloaded($excerpt_length, $allowedtags, $filter_type, $use_more_link, $more_link_text, $force_more, $fakeit, $fix_tags, $no_more, $more_tag, $more_link_title, $showdots);
}

function the_excerpt_reloaded($excerpt_length=120, $allowedtags='<a>', $filter_type='none', $use_more_link=true, $more_link_text="(more...)", $force_more=true, $fakeit=1, $fix_tags=true, $no_more=false, $more_tag='div', $more_link_title='Continue reading this entry', $showdots=true) {
	if(preg_match('%^content($|_rss)|^excerpt($|_rss)%', $filter_type)) {
		$filter_type = 'the_' . $filter_type;
	}
	echo get_the_excerpt_reloaded($excerpt_length, $allowedtags, $filter_type, $use_more_link, $more_link_text, $force_more, $fakeit, $no_more, $more_tag, $more_link_title, $showdots);
}

function get_the_excerpt_reloaded($excerpt_length, $allowedtags, $filter_type, $use_more_link, $more_link_text, $force_more, $fakeit, $no_more, $more_tag, $more_link_title, $showdots) {
	global $post;

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_'.COOKIEHASH] != $post->post_password) { // and it doesn't match cookie
			if(is_feed()) { // if this runs in a feed
				$output = __('There is no excerpt because this is a protected post.');
			} else {
	            $output = get_the_password_form();
			}
		}
		return $output;
	}

	if($fakeit == 2) { // force content as excerpt
		$text = $post->post_content;
	} elseif($fakeit == 1) { // content as excerpt, if no excerpt
		$text = (empty($post->post_excerpt)) ? $post->post_content : $post->post_excerpt;
	} else { // excerpt no matter what
		$text = $post->post_excerpt;
	}
	
	$text = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $text );

	if($excerpt_length < 0) {
		$output = $text;
	} else {
		if(!$no_more && strpos($text, '<!--more-->')) {
		    $text = explode('<!--more-->', $text, 2);
			$l = count($text[0]);
			$more_link = 1;
		} else {
			$text = explode(' ', $text);
			if(count($text) > $excerpt_length) {
				$l = $excerpt_length;
				$ellipsis = 1;
			} else {
				$l = count($text);
				$more_link_text = '';
				$ellipsis = 0;
			}
		}
		for ($i=0; $i<$l; $i++)
				$output .= $text[$i] . ' ';
	}

	if('all' != $allowed_tags) {
		$output = strip_tags($output, $allowedtags);
	}

//	$output = str_replace(array("\r\n", "\r", "\n", "  "), " ", $output);

	$output = rtrim($output, "\s\n\t\r\0\x0B");
	$output = ($fix_tags) ? $output : balanceTags($output);
	$output .= ($showdots && $ellipsis) ? '...' : '';

	switch($more_tag) {
		case('div') :
			$tag = 'span';
		break;
		case('span') :
			$tag = 'span';
		break;
		case('p') :
			$tag = 'p';
		break;
		default :
			$tag = 'span';
	}

	if ($use_more_link && $more_link_text) {
		if($force_more) {
			$output .= ' <' . $tag . ' class="more-link"><a href="'. get_permalink($post->ID) . '#more-' . $post->ID .'" title="' . $more_link_title . '">' . $more_link_text . '</a></' . $tag . '>' . "\n";
		} else {
			$output .= ' <' . $tag . ' class="more-link"><a href="'. get_permalink($post->ID) . '" title="' . $more_link_title . '">' . $more_link_text . '</a></' . $tag . '>' . "\n";
		}
	}

	$output = apply_filters($filter_type, $output);

	return $output;
}
?>