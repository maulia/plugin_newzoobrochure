<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require($path.'/wp-load.php');
$_REQUEST['property_id']=intval($_REQUEST['property_id']);
global $wpdb;
if(!empty($_REQUEST['property_id'])){
	$properties=$realty->properties(array('id'=> $_REQUEST['property_id'], 'limit'=>1));
	$property= $properties[0];
	$latitude=$property['latitude'];
	$longitude=$property['longitude'];
}
if(!empty($_REQUEST['id'])){
	$offices=$wpdb->get_results("select latitude, longitude from $realty->db.office where id =".$_REQUEST['id'], ARRAY_A);
	$latitude=$offices[0]['latitude'];
	$longitude=$offices[0]['longitude'];
}
?>
<html>
	<head>
		<?php wp_print_scripts('jquery');wp_head(); ?>
		<style type="text/css">
			body { margin:0; }
			h2.map-address { background:#3b3b3b; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:normal; height:35px; line-height:35px; margin:0 0 5px; padding:0 0 0 5px; }
		</style>
	</head>
	<body>
		<?php
		if (class_exists('zoomap')){
			zoomap::getInstance()->load_map('map_canvas', $latitude, $longitude, 13, true);
		} else {
			die('No Map Available!');
		}
		?>
		<h2 class="map-address"><?php echo $property['street_address']; ?></h2>
		<div class="property_map">
			<div id="map_canvas" style="width:592px;height:366px;"></div>
		</div>
		<div class="clear"></div>
		<br/>
		<div class="buttons signup_button">
			<input type="button" name="cancel" value="" onClick="closebox('box_map','filter_map','load_form_map','boxtitle_map')">
		</div>
		<?php wp_footer(); ?>
	</body>
</html>