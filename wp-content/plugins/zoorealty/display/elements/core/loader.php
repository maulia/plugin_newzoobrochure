<?php

//Core Package. This package is required by many plugins to function properly. If you receive a message such as "Plugin could not be activated. Please upload the "Core Package" to this plugin directory.", unzip this folder and copy it to the complaining plugin folder.

if(!class_exists('my_db'))
	require_once('class_my_db.php');
if(!class_exists('wp_page_maker'))
	require_once('wp_page_maker.php');
if(!class_exists('form_handler'))
	require_once('class_form_handler.php');
	
?>