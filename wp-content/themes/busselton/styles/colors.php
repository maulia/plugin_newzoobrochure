<?php
global $options;
foreach ($options as $value) {if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }}

/*
Colors selected in the theme options page
*/ ?>

p.logo a, p.logo a img { border: 0; width: <?php echo $logo_width; ?>px; height: <?php echo $logo_height; ?>px; }
	body { background-color: #<?php echo $bg_color; ?>; }
	#header { background-color: #<?php echo $logo_bg_color; ?>; }
	#header, #nav ul li ul, #banner { border-color: #<?php echo $header_border_color; ?>; }
	#contact_info { background-color: #<?php echo $header_border_color; ?>; }
		
<?php //	Border colors ?>
hr, #sidebar hr, blockquote, img, .post .wp-caption, table, th, td, table.date_container, form.contact_form, input, select, textarea, #content ul.commentlist li, div.sociable, #nav ul, #nav ul li ul li ul, #nav ul li span.span_div, #nav ul li ul li a, #nav ul li ul li a:hover, #sidebar, h2.post_title a, h2.section_title, .post, .page_toolbar, .land_building_size p span, td.thumbCont p.suburb, #sold_listings td, #property_stats, #agents_list .agent_item, ul.ui-tabs-nav, #map_canvas { border-color: #<?php echo $border_color; ?>; }

hr { background-color: #<?php echo $content_bg_color; ?>; color: #<?php echo $content_bg_color; ?>; }

#container { background-color: #<?php echo $border_color; ?>; }

table.property td, table.media_table, table.media_table td.td_slider_photo, td.td_photo_scroller img { border-color: #<?php echo $content_bg_color; ?>; }
table.results th, table.results td, table.sales_data_table tr.alt td, #walk_score { background-color: #<?php echo $content_bg_color; ?>; }

<?php //	Accent colors ?>
#sidebar hr, blockquote, h5.pagetitle, thead, tfoot, table.property td.descCont, table.property td.thumbCont .tdContent, p.postmetadata, #content ul.commentlist li, #wp_related_posts, .navigation, #property_features, #cloud_suburb .block_content, .realty_widget_area_map_search .map_container, #other_properties .block_content, #content #property_stats table tr.DataTableSubHeader td, #content #property_stats table tr.DataTableFooter td, table.sales_data_table td, #property_stats, #formpart { background-color: #<?php echo $accent_color; ?>; }

<!--#nav ul li ul { background-color: #<?php echo $nav_bg_color; ?>; }
#nav ul li, #nav ul li a, #nav ul li.current_page_parent ul li a { color: #<?php echo $nav_links_color; ?>; }
#nav ul li a:hover, #nav ul li a:active, #nav ul li.current_page_item a, #nav ul li.current_page_parent a, #nav ul li ul li.current_page_item a, #nav ul li.current_page_parent ul li a:hover { color: #<?php echo $nav_links_selected_color; ?>; }
#nav ul li.current_page_item a , #nav ul li.current_page_parent a { border-bottom-color: #<?php echo $nav_links_selected_color; ?>; } -->

<?php //	Link colors ?>
a, a:link, a:visited, #content p.headline, body#property #property_description h4 { color: #<?php echo $link_color; ?>; }
a:hover, a:active, a:focus, a.enchanced_active, .side_block .block_content ul li.current-cat a, span.reqtxt, span.emailreqtxt, .emphasis, #content .requi, #details h2.property_headline { color: #<?php echo $link_hover_color; ?>; }
<!--
p.button, p.quick_search_btn, p.cf-sb, p.submit_btn, #property_tools p, p.view_larger_btn, span.help_tab:hover, .side_block .block_content ul li, .page_prev a, .page_next a, #details .block_bottom p a span, p#watch_podcast, ul#listing_tabs li, ul.shadetabs li, ul.ui-tabs-nav li, .realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs li.ui-state-default a:hover { background-color: #<?php echo $link_color; ?>; }

p.button:hover, p.quick_search_btn:hover, p.cf-sb:hover, #property_tools p:hover, p.submit_btn:hover, p.view_larger_btn:hover, #switch_buttons p.current, span.help_tab, p#watch_podcast:hover, .side_block .block_content ul li.current-cat, .side_block .block_content ul li:hover, .side_block .block_content ul li:active, .side_block .block_content ul li:focus, #subscribe_block #subscribe_rss ul li span, #subscribe_block #subscribe_email ul li span, ul.rooms li span.room_type, #details .block_bottom p a:hover span, ul#listing_tabs li:hover, ul.shadetabs li:hover, ul.ui-tabs-nav li:hover, ul.ui-tabs-nav li.ui-tabs-selected, ul#listing_tabs li.current, .realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs li.ui-state-default a, #scroll-controls, #content #property_stats table tr.DataTableHeader td, table.sales_data_table th { background-color: #<?php echo $link_hover_color; ?>; }
-->
<?php //	Other colors ?>
#content p, #content ul, #content ol, #content dl, cite, p.more, tbody th, p.wp_poll_dates, .map_container label.instruction, span.state_count.span_ACT, span.state_count.span_TAS { color: #<?php echo $main_text_color; ?>; }
#sidebar p, #sidebar ul, #sidebar ol, #sidebar dl, #content p.postdate, #content td.descCont p.propertyHeadline, #content .wp-caption, .value, .comment, span.link_description, body#print-property #property_description h4 { color: #<?php echo $side_text_color; ?>; }
		
#content h1, h2, h3, h4, #content blockquote p, blockquote strong, span.wp_poll_label, h5, h6, legend, input, select, textarea, #content blockquote ol, #content blockquote ul, #content li.site_alerts ul { color: #<?php echo $page_title_color; ?>; }
.media_div { background-color: #<?php echo $page_title_color; ?>; }
		
.extrawrap { background: #<?php echo $extra_bg_color; ?>; }
#extra p, #extra ul, #extra ol, #extra dl { color: #<?php echo $extra_text_color; ?>; }
#extra a { color: #<?php echo $extra_links_color; ?>; }
		
#footer { background-color: #<?php echo $footer_bg_color; ?>; }
#footer p { color: #<?php echo $footer_text_color; ?>; }
#footer a { color: #<?php echo $footer_links_color; ?>; }

#street_view_button{ background:#fff; border-color:#000; color:#000; }

<?php 
	function cat_flag_colors($n, $m) { return(".cat-$m { background-color: #$n; }"); }

	$cat_flags = explode(',',$cat_flag_color);
	$categories = get_categories('type=post');
	foreach ($categories as $cat) {
		$cat_name[] = $cat->category_nicename;
	}

	$cat_colors = array_map("cat_flag_colors", $cat_flags, $cat_name);
	foreach ( $cat_colors as $cat_color){
		print $cat_color."\n";
	}
?>

		<?php // BACKGROUND IMAGES ?>
		.agent_twitter { background: url(<?php echo get_option('siteurl'); ?>/wp-content/plugins/Realty/manage/widgets/images/twitter.png) no-repeat 0 0; }
		.agent_facebook { background: url(<?php echo get_option('siteurl'); ?>/wp-content/plugins/Realty/manage/widgets/images/facebook.png) no-repeat 0 0; }
		.agent_linkedin { background: url(<?php echo get_option('siteurl'); ?>/wp-content/plugins/Realty/manage/widgets/images/linkedin.png) no-repeat 0 0; }