<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');

$paypal_account = get_option('paypal_account');
if(empty($paypal_account) || empty($_GET['property_id']))return;

define('EMAIL_ADD', $paypal_account); // For system notification.
define('PAYPAL_EMAIL_ADD', $paypal_account);

global $wpdb, $zoo_agents, $realty;
$request=$_REQUEST;
$paymentdate = gmdate("Y-m-d H:i:s"); // current gmt date

// Setup class
require_once('paypal_class.php');  // include the class file
$p = new paypal_class( ); 				 // initiate an instance of the class.
$p -> admin_mail = EMAIL_ADD; 
//$p -> paypal_mail = PAYPAL_EMAIL_ADD;  // If set, class will verfy the receiver.
$flag=$request['txn_type'];   
if($request['txn_type']=='web_accept')
{
	if ($p->validate_ipn()) 
	{	
		extract($request);
		//$wpdb->query("delete from property_payment where property_id=$property_id");
		$property=$wpdb->get_results("select * from properties where id = $property_id", ARRAY_A);
		$property=$property[0];
		
		$days_standart = $wpdb->get_var("select time from payment_type where type='".$property['type']."'");
		$days_featured = $wpdb->get_var("select time from payment_type where type='".$property['type']."Featured'");
		$days_premium = $wpdb->get_var("select time from payment_type where type='".$property['type']."Premium'");
		
		$wpdb->query("insert into property_payment (property_id, order_amount, paypal_account, verify_sign, txn_id, type, standard, featured, premium, created_at) values('$property_id', '$order_amount', '$email', '$verify_sign','$txn_id', 'paypal', '$standard', '$featured','$premium','$paymentdate')");
		
		$update="listingpaid=1";
		$message="Listing ID:$property_id\n\rAmount: $$order_amount\n\r";
		if($standard){ $expire_at=$zoo_agents->add_date(date("Y-m-d"),$days_standart); $update.=", expire_at='$expire_at', active='Active', status='7', status_access='5,7'"; }
		if($featured){ 
			$feature_at=$zoo_agents->add_date(date("Y-m-d"),$days_featured); 
			$update.=", feature_at='$feature_at'"; 
			$message.="Featured Property: Yes\n\r";
			$extra_id=$wpdb->get_var("select id from extras where property_id=$property_id");
			if($extra_id)$wpdb->query("update extras set featured_property=1, updated_at='$paymentdate' where property_id=$property_id");
			else $wpdb->query("insert into extras (property_id, featured_property, created_at, updated_at) values('$property_id', '1', '$paymentdate','$paymentdate')");
		}
		if($premium){ 
			$premium_at=$zoo_agents->add_date(date("Y-m-d"),$days_premium); $update.=", premium_at='$premium_at'"; 
			$extra_id=$wpdb->get_var("select id from extras where property_id=$property_id");
			$message.="Premium Property: Yes\n\r";
			if($extra_id)$wpdb->query("update extras set featured_property=2, updated_at='$paymentdate' where property_id=$property_id");
			else $wpdb->query("insert into extras (property_id, featured_property, created_at, updated_at) values('$property_id', '2', '$paymentdate','$paymentdate')");
		}
		
		if($featured)$update.=", listing_type='Featured'"; 
		elseif($premium)$update.=", listing_type='Premium'"; 
		else $update.=", listing_type='Standard'"; 
		
		$wpdb->query("update properties set $update where id=$property_id");
		
		$za_email = get_option('za_email');
		if(!empty($za_email)){
			$subject="[".$realty->blogname."] You have received Listing ID $property_id payment";
			$message.="Date: $paymentdate\n\rProperty Url:".$realty->siteUrl."$property_id/ \n\r\n\rSent out from ".$realty->siteUrl;
			$zoo_agents->text_email($za_email, $subject, $message, '', '', false);
		}
			
	}	
}
?>