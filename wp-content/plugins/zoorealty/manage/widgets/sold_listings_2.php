<h4>Sold</h4>
	
<p><label>Number</label> <br><input value="<?php echo $settings['sale']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[sale][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['sale']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[sale][Title]" type="text"></p>


<h4>Leased</h4>

<p><label>Number</label> <br><input value="<?php echo $settings['lease']['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[lease][Number]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['lease']['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[lease][Title]" type="text"></p>


<h4>Display</h4>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][thumbnail]" type="checkbox"<?php if($settings['display']['thumbnail']) echo ' checked="checked"'; ?>> <label>Thumbnail</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][street_address]" type="checkbox"<?php if($settings['display']['street_address']) echo ' checked="checked"'; ?>> <label>Street Address</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][suburb]" type="checkbox"<?php if($settings['display']['suburb']) echo ' checked="checked"'; ?>> <label>Suburb</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][property_type]" type="checkbox"<?php if($settings['display']['property_type']) echo ' checked="checked"'; ?>> <label>Property Type</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][bedrooms]" type="checkbox"<?php if($settings['display']['bedrooms']) echo ' checked="checked"'; ?>> <label>Bedrooms</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][bathrooms]" type="checkbox" <?php if($settings['display']['bathrooms']) echo 'checked="checked"'; ?>> <label>Bathrooms</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][carspaces]" type="checkbox"<?php if($settings['display']['carspaces']) echo ' checked="checked"'; ?>> <label>Carspaces</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][last_price]" type="checkbox"<?php if($settings['display']['last_price']) echo ' checked="checked"'; ?>> <label>Price</label></p>

<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][sold_at]" type="checkbox" <?php if($settings['display']['sold_at']) echo 'checked="checked"'; ?>> <label>Date Sold</label></p>