<?php
global $helper, $current_user, $realty, $wpdb;
if (!is_user_logged_in()){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }

$condition=$_GET;
if(!current_user_can('administrator')){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'dashboard/listings/'; ?>'; 
	</script>
<?php } ?>

<?php include('menu_admin.php'); 

$row=$wpdb->get_results("select * from payment_type", ARRAY_A);
$payment_type = array();
foreach($row as $item){
	$payment_type[$item['type']]=$item['price'];
}

if(isset($_POST['submit'])){
	$payment_type=$_POST['payment_type'];
	foreach($payment_type as $type=>$price){
		$price = trim($price);
		$wpdb->query("update payment_type set price='$price' where type='$type'");
	}
	if(!update_option('vpc_Merchant', $_POST['vpc_Merchant']))add_option('vpc_Merchant', $_POST['vpc_Merchant']);
	if(!update_option('vpc_AccessCode', $_POST['vpc_AccessCode']))add_option('vpc_AccessCode', $_POST['vpc_AccessCode']);
	if(!update_option('paypal_account', $_POST['paypal_account']))add_option('paypal_account', $_POST['paypal_account']);
	if(!update_option('private_listing', $_POST['private_listing']))add_option('private_listing', $_POST['private_listing']);
	if(!update_option('za_email', $_POST['za_email']))add_option('za_email', $_POST['za_email']);
	echo "<div id='success'><span>Setting successfully updated</span></div>";
}
$vpc_AccessCode = get_option('vpc_AccessCode');
$vpc_Merchant = get_option('vpc_Merchant');
$paypal_account = get_option('paypal_account');
$private_listing = get_option('private_listing');
$za_email = get_option('za_email');
?>
<form name="update_properties" id="update_properties" class="form-profile" action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="content-left-column">
		<div class="block_content">
			<h4>Listing Type</h4>
			<table>
				<thead>
					<tr class="th">
						<th>Type</th>
						<th>Amount ( number only in $ )</th>
					</tr>
				</thead>	
				<tr>
					<td>Residential Sale</td>
					<td><input type="text" name="payment_type[ResidentialSale]" value="<?php echo $payment_type['ResidentialSale']; ?>"></td>
				</tr>
				<tr>
					<td>Residential Lease</td>
					<td><input type="text" name="payment_type[ResidentialLease]" value="<?php echo $payment_type['ResidentialLease']; ?>"></td>
				</tr>
				<tr>
					<td>Commercial</td>
					<td><input type="text" name="payment_type[Commercial]" value="<?php echo $payment_type['Commercial']; ?>"></td>
				</tr>
				<tr>
					<td>Business</td>
					<td><input type="text" name="payment_type[BusinessSale]" value="<?php echo $payment_type['BusinessSale']; ?>"></td>
				</tr>
				<tr>
					<td>Shared</td>
					<td><input type="text" name="payment_type[Share]" value="<?php echo $payment_type['Share']; ?>"></td>
				</tr>
			</table>
		</div>
		
		 <div class="block_content profile-save">
			<h4>Save Changes</h4>
			<p class="button"><input type="submit" name="submit" value="Save" class="btn"></p>
		</div>
	</div>
	
	<div class="content-right-column">
		<div class="block_content">
			<h4>Payment Account</h4>
			<table cellpadding="0" cellspacing="0" >
				<?php /*<tr>
					<td>CC Merchant</td>
					<td><input type="text" name="vpc_Merchant" value="<?php echo $vpc_Merchant; ?>"></td>
				</tr>
				<tr>
					<td>CC Access Code</td>
					<td><input type="text" name="vpc_AccessCode" value="<?php echo $vpc_AccessCode; ?>"></td>
				</tr>*/ ?>
				<tr>
					<td>Paypal Account</td>
					<td><input type="text" name="paypal_account" value="<?php echo $paypal_account; ?>"></td>
				</tr>
			</table>
		</div>
		
		<div class="block_content">
			<h4>Other</h4>
			<table cellpadding="0" cellspacing="0" >
				<tr>
					<td>New Private Listings / Registrations </td>
					<td>
					<input type="radio" name="private_listing" value="1" <?php if($private_listing==1)echo 'checked="checked"'; ?>>Allow
					<input type="radio" name="private_listing" value="0" <?php if($private_listing!=1)echo 'checked="checked"'; ?>>Not Allow
					
					</td>
				</tr>
				<tr>
					<td>Email to notify</td>
					<td><input type="text" name="za_email" value="<?php echo $za_email; ?>"></td>
				</tr>
			</table>
		</div>
		
			
	</div>	
</form>