<?php

if (!function_exists('zoobrochure_hex2rgb')) {

    /**
     * Convert Hexadecimal number to RGB value as array
     * 
     * @param int $hex Hexadecimal code with/without # symbol
     * @return array Color with produce as array index 0 is RED, index 1 is GREEN and last index is BLUE
     */
    function zoobrochure_hex2rgb($hex = '') {
        $hex = str_replace("#", "", $hex);
        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        return [$r, $g, $b];
    }

}

