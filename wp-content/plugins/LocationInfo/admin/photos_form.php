<br class="clear" />
<?php /*
<div class="wrap">
	<div id="poststuff">
		<div id="post-body">

			<div id="gallerydiv" class="stuffbox">
				<h3><?php _e('Gallery Management'); ?></h3>

				<div class="inside">

					<p>To set the order of the photos, click on the photo and drag it to the desired position in the list. Make sure you press 'Update Photos' to save your changes.</p>
					<form enctype="multipart/form-data"  name="location_photos_form" method="post" action="">
						<input type="hidden" name="task" id="task" value="edit_photos" />
						<input type="hidden" name="photo_id" id="photo_id" value="" /><!--for deletion purposes only-->
						<input type="hidden" name="image_ord" value="" />
							<table class="widefat">
								<thead>
									<tr>
										<th>Reorder</th>
										<th>Thumbnail</th>
										<th>Description</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody id="image_list">
									<?php $count=0; if(!empty($location['photos'])) foreach($location['photos'] as $photo):	?>
									<tr id="image_<?php echo $count; ?>" <?php echo $alternate; ?>>
										<td style="cursor:pointer"><input type="hidden" name="location_photos[]" value="<?php echo $photo['photo_id']; ?>" />&Delta;&nabla;</td>
										<td>
											<img src="<?php echo $photo['path']; ?>" width="200" title="<?php echo $photo['photo_title']; ?>" />
										</td>					
										<td><input type="text" name="location_photo_names[]" value="<?php echo $photo['photo_title']; ?>" /></td>
										<td><a onClick="if(confirm('You are about to delete \'<?php echo $photo['photo_title']; ?>\'.\n\'OK\' to delete, \'Cancel\' to stop.' )){document.getElementById('task').value='delete_photo';document.getElementById('photo_id').value='<?php echo $photo['photo_id']; ?>';document.location_photos_form.submit();}" class="button-secondary delete"><?php _e('Delete') ?></a></td>
									</tr>
									<?php $alternate = (empty($alternate))? 'class="alternate"':'';++$count; endforeach;	?>
								</tbody>

							</table>
						<p>Click on browse to look for photos in your computer. To upload multiple pictures, simply browse again. When you are done, press 'Upload'</p>
							
						<?php $input_name = 'location_photos'; require('multi_upload.php'); ?>
						<p class="submit"><input type="submit" onclick="document.getElementById('task').value='edit_photos'; return true;" value="<?php _e('Update photos &raquo;') ?>" name="update_location_photos" /></p>
					</form>

				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#image_list").sortable({});
	  });
	</script>
</div><!--.wrap-->
*/ ?>
<?php
	$themes_folder=get_bloginfo('stylesheet_directory') .'/';
	$uploaded_path=ABSPATH . 'wp-content/uploads/LocationInfoPlugin/';
	global $realty;
	?>
	<link rel="stylesheet" href="<?php echo $realty->pluginUrl;?>uploadify/uploadify.css" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $realty->pluginUrl; ?>uploadify/swfobject.js"></script>
	<script type="text/javascript" src="<?php echo $realty->pluginUrl; ?>uploadify/jquery.uploadify.v2.1.4.js"></script>
	<script type="text/javascript">
	var uploader='<?php echo $realty->pluginUrl; ?>uploadify/uploadify.swf';
	var script_photo='<?php echo $realty->pluginUrl; ?>uploadify/uploadify_location_photo.php';
	var photos_upload_path='<?php echo $uploaded_path; ?>';
	var load_photo='<?php echo $realty->pluginUrl."uploadify/load_location_photo.php?suburb_id=".$_GET['suburb_id']."&key=";?>';
	var button_image='<?php echo $realty->pluginUrl; ?>uploadify/select_files.png';
	var button_width='110';
	var button_height='26';
	var suburb_id="<?php echo $_GET['suburb_id']; ?>";
	var cancel_image='<?php echo $realty->pluginUrl; ?>uploadify/cancel.png';
	jQuery(function() {
		jQuery('#photo_upload').uploadify({
		'uploader'       : uploader,
		'script'         : script_photo,
		'buttonImg'      : button_image,
		'width'          : button_width,
		'height'         : button_height,
		'cancelImg'      : cancel_image,
		'rollover'       : true,
		'folder'         : photos_upload_path,
		'scriptData'  	 : {'suburb_id':suburb_id, 'z_authentication':1},
		'multi'          : true,
		'auto'           : true,
		'fileExt'        : '*.jpg;*.gif:;*.png',
		'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
		'queueID'        : 'photo_queue',
		'queueSizeLimit' : 30,
		'simUploadLimit' : 30,
		'removeCompleted': true,
		'onSelect'   : function(event,data) {
			jQuery('#photo_message').text(data.filesSelected + ' files have been added to the queue.');
		},
		'onComplete'  : function(event, ID, fileObj, response, data) {
			jQuery('#photo_list').load(load_photo);
		},
		'onAllComplete'  : function(event,data) {
			jQuery('#photo_message').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
		},
		'onQueueFull'    : function (event,queueSizeLimit) {
			jQuery('#photo_message').text("Maximum upload of photos is 30.");
			return false;
		}
		});		
	}); 
	function delete_photo(key){
		jQuery('#photo_list').load(load_photo+key);
	}
	
	</script>
	<table class="widefat maintable" cellspacing="0">
		<thead>
		<tr>
			<th colspan="3" scope="col" class="manage-column column-title">Images</th>
		</tr>
		</thead> 
		<tr>
			<td colspan="3">
				<ul class="image_list" id="photo_list">
				<?php 		
				$photos = $location['photos'];	
				if(!empty($photos)){ 
					foreach($photos as $key=>$photo):
					if(!empty($photo['path'])){ ?>
					<li>
						<img src="<?php echo $this->get_thumbnail($photo['path']); ?>"/>
						<a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="javascript:delete_photo('<?php echo $photo['photo_id']; ?>');" title="Delete Image"></a>
					</li>
					<?php 
					} endforeach; 

				}	?>

				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="photo-upload-box">
					<div id="photo_message" class="upload_message">Select some files to upload:</div>
					<div id="photo_queue" class="custom-queue"></div>
					<div class="photo-upload-btn"><input id="photo_upload" type="file" name="Filedata" /></div>
				</div>
			</td>
		</tr>
	</table>	