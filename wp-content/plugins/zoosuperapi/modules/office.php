<?php

function zoosuperapi_insert_office(&$log, $office, $options)
{
	global $wpdb;

	if (!isset($options['remove_office_ids']))
		$options['remove_office_ids'] = array();
	else
		$options['remove_office_ids'] = array_map('trim', explode(',', $options['remove_office_ids']));

	if (empty($office['id']))
	{
		$log[] = 'No data transmitted? ID was blank';
		return false;
	}
	
	$table_cache = zoosuperapi_get_tables();
	
	$columns = $table_cache['office_columns'];

	if (!empty($office['deleted_at']) || in_array($office['id'], $options['remove_office_ids']))
	{
		$log[] = "Deleting office {$office['id']}";

		$wpdb->query("DELETE FROM office WHERE id = ".$office['id']);
		$wpdb->query("DELETE FROM office_specialities WHERE id = ".$office['id']);
		$wpdb->query("DELETE FROM extra_detail_names WHERE office_id = ".$office['id']);
		
		return true;
	}

	$existing_office = $wpdb->get_row('SELECT * FROM office WHERE id = '.$office['id'], ARRAY_A);

	$data = array();
	foreach ($columns as $column)
	{
		if (isset($office[$column]))
			$data[$column] = $office[$column];
	}

	if (empty($existing_office))
	{
		$log[] = 'Office does not exist';

		if (zoosuperapi_insert('office', $data) === false)
		{
			$log[] = 'Insert Failed!';
			$log[] = $wpdb->print_error();
			return false;
		}

		$log[] = 'Office created';
	} else {
		$skip_fields = explode(',', $options['skip_fields_office']);

		$log[] = 'Office does already exist.';
		$log[] = "Last updated at {$existing_office['updated_at']}, updating to {$data['updated_at']}";

		foreach ($skip_fields as $field)
		{
			$field = trim($field);
			unset($data[$field]);
		}

		if (zoosuperapi_update('office', $data, array('id' => $office['id'])) === false)
		{
			$log[] = 'Update Failed!';
			$log[] = $wpdb->print_error();
			return false;
		}

		$log[] = 'Office updated';
	}

	if (!empty($table_cache['office_specialities_exists']))
	{
		$columns = $table_cache['office_specialities'];

		if (!empty($columns))
		{
			$wpdb->query('DELETE FROM office_specialities WHERE office_id = '.$office['id']);

			if (!empty($office['specialities']))
			{
				foreach ($office['specialities'] as $speciality)
				{
					$data = array();
					foreach ($columns as $column)
					{
						if (isset($speciality[$column]))
							$data[$column] = $speciality[$column];
					}

					zoosuperapi_insert('office_specialities', $data);
				}
			}
		}
	}

	if (isset($office['extra_detail_names']))
	{
		$delete = array();
		$insert_list = array();
		$existing_extra_detail_names = $wpdb->get_results('SELECT * FROM extra_detail_names WHERE office_id = '.$office['id'], ARRAY_A);

		foreach ($existing_extra_detail_names as $extra_detail_name)
			$delete[$extra_detail_name['id']] = true;

		foreach ($office['extra_detail_names'] as $extra_detail_name)
		{
			$found = false;

			foreach ($existing_extra_detail_names as $existing_extra_detail_name)
			{
				if (
					$existing_extra_detail_name['id'] == $extra_detail_name['id'] &&
					$existing_extra_detail_name['office_id'] == $extra_detail_name['office_id'] &&
					$existing_extra_detail_name['updated_at'] == $extra_detail_name['updated_at'] &&
					$existing_extra_detail_name['field_name'] == $extra_detail_name['field_name']
					)
				{
					$found = true;
					$delete[$existing_extra_detail_name['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$insert_list[] = array(
					'id' => $extra_detail_name['id'],
					'field_name' => $extra_detail_name['field_name'],
					'created_at' => $extra_detail_name['created_at'],
					'updated_at' => $extra_detail_name['updated_at'],
					'office_id' => $extra_detail_name['office_id'],
					);
			}
		}

		foreach ($delete as $extra_detail_name_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM extra_detail_names WHERE id = '.$extra_detail_name_id);
			}
		}

		foreach ($insert_list as $item)
		{
			zoosuperapi_insert('extra_detail_names', $item);
		}
	} else {
		$wpdb->query('DELETE FROM extra_detail_names WHERE property_id = '.$office['id']);
	}

	return true;
}