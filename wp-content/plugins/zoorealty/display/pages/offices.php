<?php 
$params=$_GET;
$offices = $this->offices($params);
if($offices['results_count']<1)return print "<p>We're sorry, no offices were found to match this criteria, please check back again soon or contact us with your requirements. <a href='".get_option('siteurl')."/offices/'>Show All Offices</a></p>";	
$placeholder_image=$this->paths['theme'].'images/download.png';
?>
<div id="office">

	<div id="sorter_pagination">
		<?php $number_of_office = $offices['results_count']; 
		if($number_of_office=='1')$number_of_office.=' Office found.'; else $number_of_office.=' Offices found.';
		echo '<p class="number_properties">'.$number_of_office.'</p>';?>
		
		<div class="sorter_search_quick"><?php echo $offices['pagination_above']; ?></div>
	</div>

	<div class="office-list">
	<?php $i='0';foreach($offices as $office): if(!is_array($office))continue;?>
		<div class="office_box">
			<div class="office_logo">
				<img width="200px" height="150px" src="<?php echo ($office['logo1']=='')?$placeholder_image:$office['logo1']?>" />
			</div>
			<div class="office_location">
				<h5><?php echo $office['name']; ?></h5>
				<?php 
				if(!empty($office['unit_number']))$office['street_address'] = $office['unit_number'] .'/';
				$office['street_address'] .=  $office['street_number'] . ' ' .  $office['street']  ;
				$full=($office['street_address']==" " || $office['street_address']=="")?'':$office['street_address']."<br>";
				if(!empty($office['suburb']))$full.=$office['suburb'].", ";
				if(!empty($office['state']))$full.=$office['state'].", ";
				if(!empty($office['zipcode']))$full.=$office['zipcode'].", ";
				$full=substr($full,0,-2);
				if(!empty($full))echo "<p class=\"office-address\">".$full."</p>"; 
				if(!empty($office['phone'])):?><p class="office-phone">Phone: <?php echo $office['phone'] ; ?></p><?php endif; ?>
				<?php if(!empty($office['fax'])):?><p class="office-fax">Fax: <?php echo $office['fax'] ; ?></p><?php endif; ?>
				
			</div>
			<div class="office_links">
				<ul>
					<li><a href="<?php echo $this->siteUrl; ?>office-detail/?list=sale&office_id=<?php echo $office['id'];?>">View Sales Listings</a></li>
					<li><a href="<?php echo $this->siteUrl; ?>office-detail/?list=lease&office_id=<?php echo $office['id'];?>">View Rental Listings</a></li>
					<li><a href="<?php echo $this->siteUrl; ?>office-detail/?list=sale&status=2&office_id=<?php echo $office['id'];?>">View Sold Listings</a></li>
				</ul>
			</div> 
			
			<div class="map_link">
				<?php if(!empty($office['url'])):?><a href="<?php if(strpos($office['url'],'http://')===false)echo 'http://'.$office['url']; else echo $office['url'];?>" target="_newtab">Website</a><?php endif; ?>
				<?php if(!empty($office['email'])):?><span> | </span><a class="link-email" href="mailto:<?php echo $office['email'] ; ?>" title="Email office">Email</a><?php endif; ?>
				<?php if(!empty($office['latitude']) && !empty($office['longitude'])): ?>
				<a href="#" onclick="openbox('Map', 'filter_map', 'box_map', 'boxtitle_map', 'load_form_map', '<?php echo $this->pluginUrl.'display/elements/lightbox_map.php?siteurl='.$this->siteUrl.'&office_id='.$office['id']; ?>')" title="MAP">MAP</a>
				<?php endif; ?>
			</div>
		</div><!--end office box-->
	<?php $i++; endforeach; ?>
	</div>

	<div id="bottom_pagination">
	<?php echo $offices['pagination_above']; ?>
	</div>
</div><!--end office-->
<div id="filter_map" class="filter_lightbox"></div>
<div id="box_map" class="box_lightbox">
  <span id="boxtitle_map" class="boxtitle_lightbox"></span>
  <div id="load_form_map"></div>    
</div>