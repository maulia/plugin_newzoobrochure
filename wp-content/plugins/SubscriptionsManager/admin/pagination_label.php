<div class="tablenav-pages">


<?php if ( $prev_page >= 1 ): ?>
	<a class="prev page-numbers" href="<?php echo $current_query . $prev_page; ?>" title="Previous Page">&laquo; Previous</a>
<?php endif; ?>

<?php for ( $counter=1; $counter <= $max_page_links && $page_link<=$total_pages; ++$counter,++$page_link ): ?>
	<?php if($page == $page_link): ?>
		<span class="page-numbers current"><?php echo $page_link; ?></span>
	<?php else: ?>		
		<a class="page-numbers" href="<?php echo $current_query . $page_link; ?>" title="Page <?php echo $page_link; ?>"><?php echo $page_link; ?></a>
	<?php endif; ?>
<?php endfor; ?>
<?php if($page < $total_pages): ?>
	<a class="next page-numbers" href="<?php echo $current_query . $next_page; ?>"  title="Next Page">Next &raquo;</a>
<?php endif; ?>

</div>