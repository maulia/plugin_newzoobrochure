<?php
/*
Plugin Name: Visit's Tracker new
Version: 1.5
Plugin URI: http://www.agentpoint.com.au/
Description: Tracks down how many times a property page has been visited. A header filter is triggered everytime a property page is viewed and the information is logged into the database. From the administration panel, the site administrator is able to see reports stating each property viewed and how many hits during the past week, month and total hits that property has had. An input box is provided to enter e-mails to receive reports. A cron job can be set up to run a script that will produce reports and e-mail them to the subscribed administrators.
Author: Agentpoint
Author URI: http://www.agentpoint.com.au
*/

class visits_tracker{
	function visits_tracker(){
		global $table_prefix, $wpdb;
		
		$this->plugin_table = $table_prefix . "visits_tracker";

		$this->site_url = get_option('siteurl');
		if ( '/' != $this->site_url[strlen($this->site_url)-1] )
			$this->site_url .= '/';
		$this->plugin_folder = substr( dirname( __FILE__ ), strlen(ABSPATH . PLUGINDIR) + 1 ) . '/';
		$this->plugin_url = $this->site_url . PLUGINDIR . '/' . $this->plugin_folder;
		add_action('admin_menu', array($this, 'admin_menu'));
		add_action('wp_head', array($this, 'head'));	
		register_activation_hook(__FILE__, array($this, 'install'));
	
		$wpmu=$this->check_this_is_multsite();
		if($wpmu){
			global $wpdb;
			$blog_id=$wpdb->blogid;
			$this->log = $this->plugin_url.$blog_id."_log.html";
			$this->log_directory = dirname( __FILE__ )."/".$blog_id."_log.html";
		}
		else{
			$this->log = $this->plugin_url."/log.html";
			$this->log_directory = dirname( __FILE__ )."/log.html";		
		}	
	}//ends function
	
	function check_this_is_multsite() {
		global $wpmu_version;
		if (function_exists('is_multisite')){
			if (is_multisite()) {
				return true;
			}
			if (!empty($wpmu_version)){
				return true;
			}
		}
		return false;
	}
	
	function install(){
		global $wpdb;	
		$sql = "CREATE TABLE IF NOT EXISTS `$this->plugin_table` (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`visited_at` DATETIME NOT NULL ,
		`property_id` INT( 11 ) NOT NULL,
		 KEY `property_id` (`property_id`)		
		)";
		$wpdb->query( $sql);

		//wespac mortgage click tracker
		$sql = "CREATE TABLE IF NOT EXISTS mortgage_link_counter (
		`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`property_id` INT( 11 ) NOT NULL,
		`counter` INT( 11 ) NOT NULL,
		 KEY `property_id` (`property_id`)
		)";
		$wpdb->query( $sql);

		add_option('emails_to_send_report', array());
	
	}//ends function
	
	function empty_table($table){
		global $wpdb;
		$sql = "TRUNCATE TABLE `$table`";
		$wpdb->query( $sql);
	}//ends function
	
	function log_visit(){
		$timezone_format = _x('Y-m-d G:i:s', 'timezone date format');
		$timezone_format= date_i18n($timezone_format); 
		//$timezone_format= date_i18n( $timezone_format, false, 'gmt');
		global $realty, $wpdb;
		$id=$realty->property['id'];
		if(!empty($id)){
			$sql = "INSERT INTO $this->plugin_table (visited_at, property_id) VALUES ('".$timezone_format."', " . intval($id) ." )";		
			$wpdb->query($sql);
			$this->log_westpac_calculator($id);
		}
		
	}//ends function
	
	function log_westpac_calculator($id){
		global $wpdb;
		$count = $wpdb->get_var( "SELECT count(*) FROM mortgage_link_counter WHERE property_id=".intval($id));
		if($count == 0)
		{
			$wpdb->query( "insert into mortgage_link_counter (`property_id`, `counter`) VALUES ('". intval($id) ."', '1')");
		}
		else
		{
			$wpdb->query( "update mortgage_link_counter set counter = counter + 1 where property_id = ". intval($id));
		}
	}
	
	function head(){
		global $realty;
		if(is_page('property'))	$this->log_visit();			
			
	}//ends function

	function admin_menu(){
		add_management_page('Visit\'s Tracker', 'Visit\'s Tracker', 8, __FILE__, array($this, 'management_page'));
	}

	function get_visits($is_admin=true, $admin_page='1'){
		global $realty, $wpdb;
		$office_id=esc_sql(implode(', ', $realty->settings['general_settings']['office_id']));
		$sql="SELECT id, suburb, unit_number, street_number, street FROM  properties WHERE status in (1,4) and office_id in($office_id) order by suburb, street";
		if($is_admin){
			$results_count=$wpdb->get_var("select count(id) from properties WHERE status in (1,4) and office_id in($office_id)");
			extract($realty->admin_paginate('50', $admin_page, $results_count, $sql)); 
			$properties = $wpdb->get_results($sql, ARRAY_A);
			$properties['pagination_label'] = $pagination_label;	
		}
		else $properties=$wpdb->get_results($sql, ARRAY_A);
		$return=array();
		foreach($properties as $key=>$property){
			if(!empty($property['unit_number']))$property['street_address'] = $property['unit_number'] .'/';
			$return[$key]['address'] .=  trim($property['street_number'] . ' ' .  $property['street'])  ;
			$return[$key]['suburb'] = $property['suburb'];
			$return[$key]['url'] = $this->site_url.$property['id']."/";
			$return[$key]['property_id'] = $property['id'];
			$return[$key]['grand_total']= $wpdb->get_var("SELECT COUNT(*) from $this->plugin_table where property_id=".intval($property['id']));
			if($return[$key]['grand_total']>0){
				$return[$key]['week_total']=$wpdb->get_var("SELECT COUNT(*) from $this->plugin_table where DATEDIFF(CURDATE(),`visited_at`) <= 7 and property_id=".intval($property['id']));
				$return[$key]['month_total']=$wpdb->get_var("SELECT COUNT(*) from $this->plugin_table where DATEDIFF(CURDATE(),`visited_at`)<= 31 and property_id=".intval($property['id']));
			}
			//$return[$key]['counter']=$wpdb->get_var("select counter from mortgage_link_counter where property_id = $id");
		}
		if($is_admin)$return['pagination_label']=$properties['pagination_label'];
		return $return;	
	}//ends function	

	function paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $wpdb;
		if($page < 1) //Avoid issuing a negative limit to the SQL statement
			$page = 1;
			//print $sql;
		//Classic pagination		
		if(!empty($sql) && $results_count==0) //Count the results based on the SQL statement provided
			if(($query =$wpdb->query($sql)) != false)
				$results_count = count($query);
		if ($limit < $results_count ):				
			$limit_start = $page * $limit - ($limit);
			if($limit_start > $results_count)
				$limit_start = $results_count - $limit; //Prevent the script from inadvertently going to an out of range result
			$sql_limit = " LIMIT $limit_start, $limit";			
		endif;
		$return['sql'] = $sql . $sql_limit;
		//ends classic pagination		
		
		$max_page_links = 10; //Set how many links to show in each page
		
		
		$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
		
		$current_query = "?" . preg_replace('/&(pg=)([-+]?\\d+)/is', '', $_SERVER['QUERY_STRING']) . "&amp;pg=";
		
		$prev_page = $page - 1;
		$next_page = $page + 1;
		ob_start();
		require('admin/pagination_label.php');//Front page pagination
		$return['pagination_label'] = ob_get_clean();
		return $return;
	}//ends function

	function get_visits_by_id($property_id){
		global $realty, $wpdb;
		if(empty($property_id))$property_id=$realty->property['id'];
		if(!empty($property_id)){
			$sql = "SELECT COUNT(*) AS grand_total,  (SELECT COUNT(property_id) FROM $this->plugin_table WHERE DATEDIFF(CURDATE(),`visited_at`) <= 7 AND properties.id=$this->plugin_table.property_id ) AS week_total,	(SELECT COUNT(property_id) FROM $this->plugin_table WHERE DATEDIFF(CURDATE(),`visited_at`) < 31 AND properties.id=$this->plugin_table.property_id ) AS month_total				
			 FROM $this->plugin_table, properties WHERE
					properties.id=$this->plugin_table.property_id and property_id=".intval($property_id);
			$return=$wpdb->get_results($sql, ARRAY_A);
		}
		return $return;	
	}//ends function	
	
	function management_page(){
		$visits = $this->get_visits(true, $_GET['pg']);
		
		if(isset($_POST['submit_emails'])):
			update_option('emails_to_send_report', explode(",", $_POST['emails_to_send_report']));
			?><div id="message" class="updated fade"><p><strong>E-mails saved.</strong></p></div><?php
		endif;
		if($_GET['send_reports']):
			?><div id="message" class="updated fade"><p><strong><?php echo $this->send_reports(); ?></strong></p></div><?php
		endif;
		if($_GET['empty_log_table']):
			$this->empty_table($this->plugin_table);
			?><div id="message" class="updated fade"><p><strong>Logs cleared.</strong></p></div><?php
		endif;
		
		require( 'includes/report.php');
		require( 'includes/management_page.php');
		
		echo '<div class="wrap">';
			$visit_url=$this->plugin_folder.'cron_job.php';
			echo '<h5>Cron Job Settings. (Set these url in plesk panel)</h5>
			<table class="widefat">
			<thead><tr>
				<th>Task</th>
				<th>Minute *</th>
				<th>Hour *</th>
				<th width="50">Day of the Month *</th>
				<th>Month *</th>
				<th width="50">Day of the Week *</th>
				<th>Command *</th>
			</tr>
			</thead>
			<tr class="alternate">
				<td>Automatically send visit report <br/>(Run every Friday at 17:00) :</td>
				<td>0</td><td>17</td><td>*</td><td>*</td><td>5</td><td>php '.$visit_url.'</td>
			</tr>
			</table>
			</div>';
	}
	
	function send_reports(){
		global $helper;
		$emails_to_send_report = get_option('emails_to_send_report');
		foreach ($emails_to_send_report as $email):		
			if ( !$helper->isValidEmail($email) ):
				$return .= "The e-mail '$email' seems to be invalid; please correct it and try again.<br />";
			else:
				$date=date("d-m-Y h:i:s");
				$return="$date<hr/>";
				$visits = $this->get_visits(false);
				ob_start();
				require('includes/report.php');
				$message = ob_get_contents();
				ob_end_clean();
				$helper->email($email,"Visits Report from " . get_option('blogname'),$message);
				$return .= "A report to '$email' has been sent.<br />";
				//$this->logfile($return);
			endif;
		endforeach;
		return $return;
	
	}//ends function

	function logfile($logfile){
		if( !file_exists( $this->log_directory ) ){
			$ourFileHandle = fopen($this->log_directory, 'w') or die("can't open file");
			fclose($ourFileHandle);						
		}	
		else{
			$file_content  = file_get_contents( $this->log_directory );
			$logfile = "$file_content<hr/>$logfile" ;			
			unlink ($this->log_directory);
			$ourFileHandle = fopen($this->log_directory, 'w') or die("can't open file");
			fclose($ourFileHandle);			
		}	
		file_put_contents( $this->log_directory, $logfile ); 
	}	
}//ends class

$visits_tracker = new visits_tracker();
?>