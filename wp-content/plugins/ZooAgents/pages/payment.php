<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."'");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}
else $office_id=$wpdb->get_var("select office_id from users where email='".$current_user->user_email."'");

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];
$type=$_GET['type'];
$office_id=intval($_GET['office_id']);

$property=$wpdb->get_results("select * from properties where id = $id $extra_condition", ARRAY_);
$property=$property[0];
if(empty($property))return print "Listing ID $id is not exist";

include('menu_listings.php');
if($is_admin==1){
	if(isset($_POST['submit'])){
		$_POST['updated_at']=date("Y-m-d h:i:s");
		$_POST['active']=(($_POST['listingpaid']=='0'))?'Inactive':'Active';
	
		if($this->update_form( "properties", $_POST ))echo "<div id='success'><span>Updated</span></div>";
		else echo "<div id='return' class='red'>Update failed. please try again later</div>";
		
		$property=$_POST;	
	}
	?>
	<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
		<div class="block_content">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<table style="width: 310px">
				<tr>				
					<td>Payment Status</td>
					<td width="80px"><input type="radio" name="listingpaid" <?php if ($property['listingpaid']=='1') echo 'checked="checked"'; ?> value="1">Paid</td>
					<td width="80px"><input type="radio" name="listingpaid" <?php if ($property['listingpaid']!='1') echo 'checked="checked"'; ?> value="0">Pay</td>	
				</tr>			
			</table>
		</div>
		<input type="submit" name="submit" value="Save" class="btn">
		<div class="clearer"></div>
	</form>
	<?php
}
else{	
	if($property['active']=='Expired' || $property['active']=='Inactive'){ // pay
		$amount=$wpdb->get_var("select price from payment_type where type='".$property['type']."'");
		$paypal_account = get_option('paypal_account');
		$vpc_AccessCode = get_option('vpc_AccessCode');
		$vpc_Merchant = get_option('vpc_Merchant');
		?>
		<div class="block_content">
			<table>
				<tr>
					<td width="50%">
                    If you have monthly Subscription, Click <a href="<?php echo $realty->siteUrl.'dashboard/listings/'; ?>">"Listing"</a> Tab to RETURN<br/>
					"All FREE LISTINGS are approved within 2- 4 hrs,subject to Audit."<br/><br/>
                    For Immediate activation of Listing, Please pay<br />
					Amount: <?php	echo "$".$amount;	?>
					<?php if(!empty($paypal_account)){ ?>
						<br/><br/>
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
							<input type="hidden" name="cmd" value="_xclick">
							<input type="hidden" name="business" value="<?php echo $paypal_account; ?>">
							<input type="hidden" name="item_name" id="item_name" value="<?php echo "Activate Listing ID ".$property['id']." for 30 days on ".$realty->siteUrl." (Price Includes GST)"; ?>"/>
							<input type="hidden" name="item_number" value="1" />
							<input type="hidden" name="amount" id="amount" value="<?php echo $amount; ?>"/>
							<input type="hidden" name="no_shipping" value="0"/>
							<input type="hidden" name="no_note" value="1"/>
							<input type="hidden" name="currency_code" value="AUD"/>
							<input type="hidden" name="tax" value="0">
							<input type="hidden" name="lc" value="US"/>
							<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
							<input type="hidden" name="rm" value="2">
							<input type="hidden" name="notify_url" id="notify_url" value="<?php echo $this->pluginUrl."paypal/ipn_notification.php?property_id=".$property['id']."&order_amount=$amount"; ?>">
							<input type="hidden" name="cancel_return" value="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id']; ?>">
							<input type="hidden" name="return" value="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id']; ?>">		
							<input type="submit" value="Pay with paypal" Title="Pay with Paypal" class="btn paypal" >
						</form>
					<?php } ?>
					
					<?php
					if(!empty($vpc_AccessCode) && !empty($vpc_Merchant)){ ?>
					<p><a href="">Pay with ETF</a></p>		
					<?php } ?>
					</td>
					<td>
					<div style="margin-left:20px">To Make Payments by Bank Transfer email us on<br/><br/>contact@realestatemarket.net.au<br/><br/>with your listing ID Number found next to your property under the Listing Tab. You will receive an Invoice with Bank details for the Transfer. Allow 2 Working days to confirm receipt of payment.</div>
					</td>
				</tr>
			</table>
		</div>		
		<div class="clearer"></div>
<?php }
}

$payment_details=$wpdb->get_results("select * from property_payment where property_id=".$property['id'], ARRAY_A);
if(!empty($payment_details)){ // paid
	?>
	<div class="block_content">
		<h4>Last Payment History</h4>
		<table>
			<tr>				
				<td width="140">Amount</td>
				<td><?php echo "$".$payment_details[0]['order_amount']; ?></td>
			</tr>	
			<tr>				
				<td width="140">Paid</td>
				<td><?php echo "by ".$payment_details[0]['type']." at ".date("d-m-Y",strtotime($payment_details[0]['created_at'])); ?></td>
			</tr>
			<tr>				
				<td width="140">Expire at</td>
				<td><?php echo date("Y-m-d",strtotime($property['expire_at'])); ?></td>
			</tr>
		</table>
	</div>
	<?php
}
 ?> 