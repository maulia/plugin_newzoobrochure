<?php
foreach( $_REQUEST AS $key=>$val )
{
$_REQUEST[$key] = urldecode( $val );
}
$path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
require($path.'/wp-load.php');
global $realty, $helper, $subscriptions_manager, $wpdb;
if(!$helper->isValidEmail($_REQUEST['email']))$return.="You have entered an invalid email.<br/>";
if( $_SESSION['security_code'] != $_REQUEST['code'] )$return.='<script type="text/javascript">alert("Wrong spam code");</script>';
if( $_SESSION['security_code'] == $_REQUEST['code']  )unset($_SESSION['security_code']);

$user_id=$_GET['user_id'];
if(empty($return) && !empty($user_id)){	
	$date=date("Y-m-d H:i:s");
	if(!empty($_REQUEST['property_id'])){
		$subject = 'Property Enquiry - Property id '. $_REQUEST['property_id'];
		$property =  $realty->properties( array('id'=> $_REQUEST['property_id'], 'limit'=>1));
		$property=$property[0];	
		$name= $_REQUEST['first_name']." ".$_REQUEST['last_name'];
		$wpdb->insert('property_enquiry_form',
			array(						
				'customer_name' => $name,
				'email' => $_REQUEST['email'],
				'phone' => $_REQUEST['mobile_phone'],
				'message' => $_REQUEST['comments'],
				'agent_id' => $user_id,
				'property_id' => $_REQUEST['property_id'],
				'property_url' => $property['url'],
				'date' => $date
				),
			array( 
				'%s', 
				'%s', 
				'%s',						 
				'%s',
				'%d',	
				'%d', 
				'%s', 
				'%s' 
			)
		);
	}
	else $subject = 'Enquiry Email';
	
	ob_start();
	?>
	<h5><?php echo $subject; ?></h5>
	<p>	Name: <?php echo $_REQUEST['first_name']." ".$_REQUEST['last_name']; ?></p>	
	<p>	Email: <?php echo $_REQUEST['email']; ?></p>		
	<p>	Mobile: <?php echo $_REQUEST['mobile_phone']; ?></p>	
	<p>	Home Number: <?php echo $_REQUEST['home_phone']; ?></p>	
	<?php if(!empty($_REQUEST['property_id'])){ ?>
	<p>	Property Id: <?php echo $_REQUEST['property_id']; ?></p>
	<p>	Property Address: <?php echo $property['street_address']." ".$property['suburb']; ?></p>
	<?php } ?>			
	<p>	Comment: <?php echo $_REQUEST['comments']; ?></p>			
	<?php
	$extra_message = ob_get_clean();
	ob_start();
	$realty->element('email_property_body_agent', array('extra_message'=>$extra_message));
	$message = ob_get_clean();

	if(class_exists('zoo_analytics') ){ global $zoo_analytics; $zoo_analytics->save_trackers($_REQUEST['property_id'],'enquiry'); }
	
				
	if($_REQUEST['keep_informed']=='1' && class_exists('subscriptions_manager')){			
		$subscriber_id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_REQUEST['email'])."'");

		if(!$subscriber_id){
		$wpdb->insert('subscriptions_manager',
				array(						
					'first_name' => $_REQUEST['first_name'],
					'last_name' => $_REQUEST['last_name'],
					'email' => $_REQUEST['email'],						
					'home_phone' => $_REQUEST['home_phone'],						
					'mobile_phone' => $_REQUEST['mobile_phone'],						
					'referrer' => $_REQUEST['referrer'],
					'comments' => $_REQUEST['comments'],
					'property_id' => $_REQUEST['property_id'],
					'property_address' => $property['street_address'],
					'keep_informed' => $_REQUEST['keep_informed'],
					'created_at' => $date,
					'updated_at' => $date
					),
				array( 
					'%s', 
					'%s', 
					'%s',						 
					'%s',
					'%s',
					'%s',
					'%s',
					'%d',	
					'%s',	
					'%d', 
					'%s', 
					'%s' 
				)
			);
		}else{
			$wpdb->update('subscriptions_manager',
				array(						
					'first_name' => $_REQUEST['first_name'],
					'last_name' => $_REQUEST['last_name'],		
					'home_phone' => $_REQUEST['home_phone'],						
					'mobile_phone' => $_REQUEST['mobile_phone'],						
					'referrer' => $_REQUEST['referrer'],
					'comments' => $_REQUEST['comments'],
					'property_id' => $_REQUEST['property_id'],
					'property_address' => $property['street_address'],
					'keep_informed' => $_REQUEST['keep_informed'],
					'created_at' => $date,
					'updated_at' => $date
					),
				array( 'subscriber_id' => $subscriber_id), 
				array( 
					'%s', 
					'%s', 						 
					'%s',
					'%s',
					'%s',
					'%s',
					'%d',	
					'%s',	
					'%d', 
					'%s', 
					'%s' 
				), 
				array( '%d' ) 
			);
		}
	
		$list_alerts = $subscriptions_manager->get_site_alerts();
		$all_alerts = $subscriptions_manager->get_site_alerts(true);
		$news_alert=array();
		$news_alert=array_diff($list_alerts, $all_alerts);
		$i=0;
		if(!empty($news_alert)){
			$id=$wpdb->get_var("select subscriber_id from subscriptions_manager where email='".esc_sql($_POST['message']['email'])."'");
			$wpdb->query("DELETE FROM `site_alerts` WHERE `subscriber_id`=" . intval($id));
			foreach($news_alert as $key=>$item_value):
			$_REQUEST['alert'][$i]=$key;$i++;
			endforeach;
			foreach( $_REQUEST['alert'] as $alert):
				if(is_array($_REQUEST['query_string'][$alert]))
					$query_string = str_replace('%2C', ',', http_build_query($_REQUEST['query_string'][$alert]));
					$alerts_sql .="(" . $id . ", '$alert', '". $query_string . "'), ";
					$query_string = '';
			endforeach;
			$alerts_sql = substr($alerts_sql,0,-2); //Remove the last comma
			$wpdb->query("INSERT INTO `site_alerts` (`subscriber_id`, `alert`, `query_string`) VALUES $alerts_sql");
		}
	}

	if(class_exists('ZooRemittance')){
		// send to zoo api
		if(!empty($_REQUEST['property_id'])){
		$zoo_api="http://agentpoint.agentaccount.com/agents/".$property['agent_id']."/offices/".$property['office_id']."/contact_listeners?";
		$paramaters="first_name=".$_REQUEST['first_name']."&last_name=".$_REQUEST['last_name']."&email=".$_REQUEST['email']."&mobile_number=".$_REQUEST['mobile_phone']."&home_number=".$_REQUEST['home_phone']."&note=".$_REQUEST['comments']."&property_id=".$_REQUEST['property_id']."&address=".$property['street_address']."&heard_from_id=7&note_type=6";

		$keys=$wpdb->get_results("select access_key,private_key from property_xml where office_id=".$property['office_id'], ARRAY_A);
		}
		else
		{
		if(empty($user['agent_id'])){
			$get_agents=$wpdb->get_results("select distinct agent_id from properties where agent_id!='' and agent_id is not null and office_id=".$user['office_id'], ARRAY_A);
			$user['agent_id']=$get_agents[0]['agent_id'];
		}
		$zoo_api="http://agentpoint.agentaccount.com/agents/".$user['agent_id']."/offices/".$user['office_id']."/contact_listeners?";
		$paramaters="first_name=".$_REQUEST['first_name']."&last_name=".$_REQUEST['last_name']."&email=".$_REQUEST['email']."&mobile_number=".$_REQUEST['mobile_phone']."&home_number=".$_REQUEST['home_phone']."&note=".$_REQUEST['comments']."&heard_from_id=7&note_type=6";

		$keys=$wpdb->get_results("select access_key,private_key from property_xml where office_id=".$user['office_id'], ARRAY_A);
		}
		
		$_REQUEST['referrer']=trim(strtolower($_REQUEST['referrer']));
		switch($_REQUEST['referrer']){
			case '': case 'undefined':$heard_about_us='';break;
			case 'newspaper': $heard_about_us='1';break;
			case 'referral': $heard_about_us='2';break;
			case 'web': $heard_about_us='3';break;
			case 'past client': $heard_about_us='4';break;
			case 'sign board': $heard_about_us='5';break;
			default: $heard_about_us='6';break;		
		}
	
		$params=array();
		$params['first_name']=$_REQUEST['first_name'];
		$params['last_name']=$_REQUEST['last_name'];
		$params['email']=$_REQUEST['email'];
		$params['mobile_number']=$_REQUEST['mobile_phone'];
		$params['home_number']=$_REQUEST['home_phone'];
		$params['note']=$_REQUEST['comments'];
		$params['property_id']=$_REQUEST['property_id'];
		$params['address']=$property['street_address'];
		$params['note_type']='6';
		
		if(!empty($heard_about_us)){
			$paramaters.="&heard_about_us=$heard_about_us";
			$params['heard_about_us']=$heard_about_us;
		}
	
		if($_REQUEST['keep_informed']=='1'){
			$paramaters.='&alert=1&alert_type=Latest News';
			$params['alert']='1';
			$params['alert_type']='Latest News';
		}
		else{
			$paramaters.='&alert=0';
			$params['alert']='0';
		}
	
		$accesskey=$keys[0]['access_key'];
		$privatekey=$keys[0]['private_key'];

		//echo $zoo_api." ".$paramaters;die();
		global $ZooRemittance;
		if(!empty($accesskey) && !empty($privatekey))$ZooRemittance->send_api($zoo_api,$paramaters,$accesskey,$privatekey,$params);
	}
	
	$to = $wpdb->get_var("select email from users where id in (".intval($_GET['user_id']).")");
	if($_REQUEST['email']!='aa@aa.com')$helper->email($to,$subject, $message, $_REQUEST['email']);else $helper->email('dewi@agentpoint.com.au',$subject, $message, $_REQUEST['email']);
	if(!empty($_GET['user_id1']) && $_REQUEST['email']!='aa@aa.com'){	
		$to = $wpdb->get_var("select email from users where id=".intval($_GET['user_id1']));
		$to = $user["email"];
		$helper->email($to,$subject, $message,$_REQUEST['email'],'',false);
	}
	
	/* send e,ail to customers
	ob_start();
	$subject = 'Thank you for your enquiry';
	$extra_message='Thank you for your enquiry. Our staff will get back to you shortly';
	$realty->element('email_property_body_agent', array('extra_message'=>$extra_message));
	$message = ob_get_clean();
	$helper->email($_REQUEST['email'],$subject, $message,'','',false);
	 */
	 if(class_exists('action_plan'))file_get_contents($realty->siteUrl.'?zooapi=ping');
	?>
	<script type="text/javascript">
		alert('Thank you for your enquiry. Our staff will get back to you shortly'); 
		jQuery.prettyPhoto.close();
		</script>
	<?php
}
echo $return;
?>