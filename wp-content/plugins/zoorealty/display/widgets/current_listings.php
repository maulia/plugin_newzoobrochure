<?php
if(!function_exists('print_item')):
	function print_item($key, $value, $printKey = true, $keyName =''){ 
			global $helper;
			if(is_array($value)):
				foreach($value as $innerKey=>$innerValue)
					print_item($innerKey, $innerValue);
			else:
				if(empty($keyName) || $keyName==1) 
					$keyName = $helper->humanize($key);
				if($helper->validUrl($value))
					$value = '<a target="_blank" href="'.$value.'" title="'.$keyName.'" class="btn">'.$keyName.'</a>';
?>
<tr class="<?php echo $key; ?>">
<?php if($printKey): ?>
		<td class="field"><?php echo $keyName; ?>: </td>
<?php endif; ?>
		<td class="value"><?php if($key=='type'){if(strpos($value,'Sale')!==false)$value="For Sale"; else $value="For Lease";} echo $value; ?></td>
</tr>

<?php endif; } endif; 
if($settings['Both']['Number']>0) //If both properties number is set, that means we choose to display sale and lease together, so there's no need to show both separately again
	$settings['sale']['Number'] = $settings['lease']['Number'] = 0;
foreach($settings as $type=>$values):
	if(!is_array($values))continue; if($values['Number'] <1) continue; //Only if any properties were requested to be displayed for this type.
	$placeholder_image= $realty->paths['theme'].'images/download.png';
	$tbl_properties='properties';
	$select="SELECT  $tbl_properties.id, $tbl_properties.type,($tbl_properties.garage + $tbl_properties.carport + $tbl_properties.off_street_park) AS carspaces,  $tbl_properties.bedrooms, $tbl_properties.show_price as display_sold_price,$tbl_properties.bathrooms, $tbl_properties.headline, $tbl_properties.property_type,$tbl_properties.display_price as price_display, $tbl_properties.price, $tbl_properties.display_price_text as price_display_value, 
			(Case when status='2' Then sold_at	else '' END)as sold_at,
			(Case when status='2' Then sold_price else '' END)as sold_price,
			(Case when status='6' Then leased_at else '' END)as leased_at,
			(Case when status='6' Then leased_price	else '' END)as leased_price,
			 IFNULL((SELECT attachments.url FROM attachments WHERE attachments.parent_id = $tbl_properties.id and description='thumb' and type='photo' and is_user='0' ORDER BY `position` ASC LIMIT 1 ),'$placeholder_image') AS thumbnail,
			 $tbl_properties.status,$tbl_properties.display_address, $tbl_properties.unit_number, $tbl_properties.street_number, $tbl_properties.street, $tbl_properties.state, $tbl_properties.suburb";	
	 $current_listings = $realty->properties(array('list' =>$type, 'limit'=>$values['Number'], 'order'=>'created_at' ,'order_direction'=> 'desc'), true, $select);
	 $count++;
if($current_listings['results_count'] >0): ?>
<div id="current_<?php echo $type; ?>" class="latest_listings">
	<?php echo $before_title; ?><span class="title_current_<?php echo $type;?>"><?php echo $values['Title'];?></span><?php echo $after_title; ?>
	<?php
		$count=0; 
		foreach ($current_listings as $property):
		 if(!is_array($property)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned	
			$property['street_suburb']=$property['street_address'];
			if(!empty($property['suburb']))$property['street_suburb'].=', '.$property['suburb'];
?>

<div class="photo photo<?php echo $count; ?>">
    <div class="photo-wrap"><a href="<?php echo $property['url']; ?>" title="<?php echo htmlspecialchars($property['street_suburb']); ?>"><img src="<?php echo $property['medium']; ?>" alt="<?php echo htmlspecialchars($property['street_suburb']); ?>" /></a>
    	<div class="photo-desc">
		<?php
        foreach($settings as $key=>$values):
			if(( !$values['display'])  ||empty($property[$key]) || $key =='sale' || $key=='lease' || $key=='Both') continue;
			switch($key):
				default:
				//print_item($key, $property[$key], true, $settings[$key]['title'] );
				?>
			<p class="<?php echo $key; ?>"><?php if(!empty($settings[$key]['title'] )) echo '<span>'.$settings[$key]['title'].'</span>'.": ";?><?php if($key=='type'){if(strpos($property[$key],'Sale')!==false)$property[$key]="For Sale"; else $property[$key]="For Lease";} echo $property[$key]; ?></p>
				<?php 
			break;
			endswitch;
		endforeach;
        ?>
        	<div class="clear"></div>
        </div>
    </div>
</div>
<?php
			$count++;
		endforeach;
?>
<div class="clearer"></div>
</div><!-- ends #<?php echo $type; ?> --><?php
	endif; //$current_listings['results_count'] >0): 	
endforeach;
?>