<div id="location_info">
	<div class="location-side"><?php dynamic_sidebar('Suburb Snapshot Sidebar'); ?></div>
    <div class="location-page">

		<?php  
		$location=($location[0]);
		global $post;
		echo str_replace('[location_info_individual_page_plugin]', '',$post->post_content); 
		$suburb=$location['suburb'];
		$sold_listings = $realty->sold(array('list'=>'sale', 'suburb'=>$suburb));
		$leased_listings = $realty->sold(array('list'=>'lease', 'suburb'=>$suburb)); 
		$sale_listings = $realty->properties(array('list'=>'sale', 'status'=>'1,4','suburb'=>$suburb)); 
		$lease_listings = $realty->properties(array('list'=>'lease', 'status'=>'1,4','suburb'=>$suburb)); 
		wp_print_scripts('jquery-ui-core');
		wp_print_scripts('jquery-ui-tabs');

        ?>
        
        <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery("#tabbed_listings").tabs();
            });
        </script>
    
        <div id="tabbed_listings">
            <ul>
                <li class="information"><a title="Information" href="#suburb-profile">Profile</a></li>
                <li class="information"><a title="Map" href="#map_suburb">Map</a></li>
				<?php if (!empty($location[0]['amenities'])){ ?>
                <li class="interesting_place"><a title="Interesting Place" href="#interesting_place">Interesting Place</a></li>
                <?php } ?>
				<?php if ($sale_listings['results_count'] >=1): ?>
                <li class="sale_listings"><a title="Places to buy" href="#sale_listings">Places to buy</a></li>
                <?php endif; ?>
                <?php if ($lease_listings['results_count'] >=1): ?>
                <li class="lease_listings"><a title="Places to lease" href="#lease_listings">Places to lease</a></li>
                <?php endif; ?>
				<?php if ($sold_listings['results_count'] >=1): ?>
                <li class="sold_properties"><a title="Sold places" href="#sold_properties">Sold places</a></li>
                <?php endif; ?>
                <?php if ($leased_listings['results_count'] >=1): ?>
                <li class="leased_properties"><a title="Leased places" href="#leased_properties">Leased places</a></li>
                <?php endif; ?>
            </ul>
            
            <div id="suburb-profile">
            	
                <?php if (!empty($location['description'])): ?>
                <div id="description">
					<?php if(!empty($photos)): ?>
                        <script type="text/javascript" src="<?php echo $realty->pluginUrl?>display/pages/js/prototype.js"></script>
						<script type="text/javascript" src="<?php echo $realty->pluginUrl?>display/pages/js/scriptaculous.js?load=effects,builder"></script>
						<script type="text/javascript" src="<?php echo $realty->pluginUrl?>display/pages/js/lightbox.js"></script>
                        <div class="location-image">
                            <div class="image">
                                <a href="<?php echo $photos[0]['path']; ?>" onclick="return false;" rel="lightbox[suburb_photos]" title="<?php echo $suburb." photos"; ?>"><img src="<?php echo  $photos[0]['path']; ?>" alt=""></a>
                                <div style="display:none">
                                    <?php  for ($i=1;$i<count($photos);$i++){?>
                                    <a href="<?php echo $photos[$i]['path']; ?>" onclick="return false;" rel="lightbox[suburb_photos]"></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($location['description'])) { echo nl2br($location['description']); } ?>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
                
                <div class="clear"></div>
    
            </div>
            
            <div id="map_suburb">
                <?php $center="$suburb,NSW,Australia" ; ?>
                <div class="map-image"><div class="image"><img src="http://maps.google.com/maps/api/staticmap?center=<?php echo $center; ?>&zoom=15&size=640x395&markers=<?php echo $center; ?>&sensor=false&format=jpg&maptype=satellite"></div></div>
            	<div class="shortinfo-amenities">
                    <div class="location_short_info">
                        <ul class="location_info">
                            <li class="sim_suburb suburb"><span class="field">Suburb</span><span class="colon">:</span><?php echo ucwords(strtolower($suburb)); ?></li>
                            <?php if(!empty($location['postcode'])){ ?><li class="sim_suburb postcode"><span class="field">Postcode</span><span class="colon">:</span><?php echo $location['postcode']; ?></li><?php } ?>
                            <?php if(!empty($location['population'])){ ?><li class="sim_price price"><span class="field">Population</span><span class="colon">:</span><?php echo $location['population']; ?></li><?php } ?>
                            <?php if(!empty($location['council'])){ ?><li class="sim_type type"><span class="field">Municipality</span><span class="colon">:</span><?php if(!empty($location['link'])) echo "<a href='".$location['link']."' target='blank'>"; ?><?php echo $location['council']; ?><?php if(!empty($location['link'])) echo "</a>"; ?></li><?php } ?>
                        </ul>
                    </div>
                    <div class="location_amenities">
                    <?php 
                        $amenities = array('cafe'=>'cafes & restaurants','landmark'=>'landmarks','worship'=>'Places of Worship','school'=>'School & Univ','shopping'=>'Shops', 'sport'=>'Sporting Facilities');
                        foreach($amenities as $key=>$title){
                            if(!empty($location[$title]))
                            {	
                                echo "<h4>Interesting Places</h4>";
                                break;
                            }	
                        }
                        foreach($amenities as $key=>$title): if(!empty($location[$title])):// var_dump($location[$title]); ?>
                        <div class="interesting_location location_<?php echo $key; ?>">
                            <p class="<?php echo $key; ?>_title"><?php echo $title; ?></p>
                            <ul class="<?php echo $key; ?>_list">
                                <?php foreach ($location[$title] as $item): ?>
                                <li>		
                                    <?php if(!empty($item['link'])) echo '<a href="'.$item['link'].'" title="'.$item['amenity'].'">';?><?php echo $item['amenity']; ?><?php if(!empty($item['link'])) echo'</a>'; ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div><!--end .location_<?php echo $key; ?>-->
                        <?php endif; endforeach; ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            
            <?php if (!empty($amenities)){ ?>
            <div id="interesting_place">
				<ul>
				<?php 
				foreach($location['amenities'] as $amenity): 
					$print_text='<li>';
					if(!empty($amenity['link']))$print_text.="<a href='".$amenity['link']."' title='".$amenity['type']."' target='_blank'>";
					$print_text.=$amenity['amenity'];
					if(!empty($amenity['link']))$print_text.="</a>";
					$print_text.="</li>";
					echo $print_text;
				endforeach; ?>
				</ul>
			</div>
            <?php } ?>
				
			<?php 
            if($sale_listings['results_count'] >=1){ ?>
            <div id="sale_listings">
                <table class="results" cellpadding="0" cellspacing="0" summary="Sales Results" width="100%">
                <tr class="th">
                    <th class="th_address">Address</th>
                    <th class="th_suburb">Suburb</th>
                    <th class="th_bed"><span>Bed</span></th>
                    <th class="th_bath"><span>Bath</span></th>
                    <th class="th_car"><span>Car</span></th>
                    <th class="th_price">Price</th>
                    <th class="th_view"></th>
                </tr>
                <?php $i=0;foreach($sale_listings as $property): if (!is_array($property))continue;?>
                <tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
        
                    <td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></a></span></td>
                    <td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
                    <td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
                    <td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
                    <td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
                    <td class="td_price"><span class="td_price"><?php echo $property['price']; ?></span></td>
                    <td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
                </tr>
                <?php endforeach; ?> 
                </table>
            </div>
            <?php } ?>

			<?php 
            if($lease_listings['results_count'] >=1){ ?>
            <div id="lease_listings">
                <table class="results" cellpadding="0" cellspacing="0" summary="Sales Results" width="100%">
                <tr class="th">
                    <th class="th_address">Address</th>
                    <th class="th_suburb">Suburb</th>
                    <th class="th_bed"><span>Bed</span></th>
                    <th class="th_bath"><span>Bath</span></th>
                    <th class="th_car"><span>Car</span></th>
                    <th class="th_price">Price</th>
                    <th class="th_view"></th>
                </tr>
                <?php $i=0;foreach($lease_listings as $property): if (!is_array($property))continue;?>
                <tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
        
                    <td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></a></span></td>
                    <td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
                    <td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
                    <td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
                    <td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
                    <td class="td_price"><span class="td_price"><?php echo $property['price']; ?></span></td>
                    <td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
                </tr>
                <?php endforeach; ?> 
                </table>
            </div>
            <?php } ?>
			
			<?php 
            if($sold_listings['results_count'] >=1){ ?>
            <div id="sold_properties">
                <table class="results" cellpadding="0" cellspacing="0" summary="Sales Results" width="100%">
                <tr class="th">
                    <th class="th_address">Address</th>
                    <th class="th_suburb">Suburb</th>
                    <th class="th_date">Sold Date</th>
                    <th class="th_bed"><span>Bed</span></th>
                    <th class="th_bath"><span>Bath</span></th>
                    <th class="th_car"><span>Car</span></th>
                    <th class="th_price">Sale Price</th>
                    <th class="th_view"></th>
                </tr>
                <?php $i=0;foreach($sold_listings as $property): if (!is_array($property))continue;?>
                <tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
        
                    <td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></a></span></td>
                    <td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
                    <td class="td_date"><span class="day"><?php if(!empty($property['date']) && $property['date']!='0000-00-00')echo date("j-m-Y",strtotime($property['date'])); ?></span></td>
                    <td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
                    <td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
                    <td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
                    <td class="td_price"><span class="td_price"><?php echo ($property['display_sold_price'])? $property['last_price']:'Undisclosed'; ?></span></td>
                    <td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
                </tr>
                <?php endforeach; ?> 
                </table>
            </div>
            <?php } ?>
    
            <?php 
            if($leased_listings['results_count'] >=1){ ?>
    
            <div id="leased_properties">
            <table class="results" cellpadding="0" cellspacing="0" summary="Lease Results" width="100%">
                <tr class="th">
                    <th class="th_address">Address</th>
                    <th class="th_suburb">Suburb</th>
                    <th class="th_date">Leased Date</th>
                    <th class="th_bed"><span>Bed</span></th>
                    <th class="th_bath"><span>Bath</span></th>
                    <th class="th_car"><span>Car</span></th>
                    <th class="th_price">Lease Price</th>
                    <th class="th_view"></th>
                </tr>
                <?php $i=0;foreach($leased_listings as $property):if (!is_array($property))continue;?>
                <tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
        
                    <td class="td_address"><span class="property_link"><a href="<?php echo $property['url']; ?>" title="<?php echo $property['headline']; ?>">	<?php echo $property['street_address']; ?></span></a></td>
                    <td class="td_suburb"><span class="suburb"><?php echo strtoupper($property['suburb']); ?></span></td>
                    <td class="td_date"><span class="day"><?php if(!empty($property['date']) && $property['date']!='0000-00-00')echo date("j-m-Y",strtotime($property['date'])); ?></span></td>
                    <td class="td_bed"><span class="beds"><?php echo $property['bedrooms']; ?></span></td>
                    <td class="td_bath"><span class="bath"><?php echo $property['bathrooms']; ?></span></td>
                    <td class="td_car"><span class="car"><?php echo $property['carspaces']; ?></span></td>
                    <td class="td_price"><span class="td_price"><?php echo ($property['display_sold_price'])? $property['last_price']:'Undisclosed'; ?></span></td>
                    <td class="td_view"><span class="td_view"><a href="<?php echo $property['url']; ?>" title="View Property">View Property</a></span></td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php } ?>
        </div>
        
	</div><!-- .location-page -->
	<div class="clear"></div>
</div>