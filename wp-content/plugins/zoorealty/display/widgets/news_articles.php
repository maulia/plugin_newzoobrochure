<?php
global $helper;
$args=array();
if(is_array($settings['categories']))$args['category']=implode(',', $settings['categories']);
$args['numberposts']=$settings['Number']; 
$posts = get_posts($args);
if(!empty($settings['rotate'])){ 
?>
<link rel="stylesheet" href="<?php echo $realty->pluginUrl ?>css/jquery.jcarousel.css" type="text/css" />
<script type="text/javascript" src="<?php echo $realty->pluginUrl ?>js/jquery.jcarousel.pack.js"></script>
<script src="<?php echo $realty->pluginUrl ?>js/jquery.tabs.pack.js" type="text/javascript"></script>
<script type="text/javascript">
function mycarousel_initCallback(carousel)
{
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.bind('click', function() {
		carousel.startAuto(0);
	});

	carousel.buttonPrev.bind('click', function() {
		carousel.startAuto(0);
	});

	// Pause autoscrolling if the user moves with the cursor over the clip.
	carousel.clip.hover(function() {
		carousel.stopAuto();
	}, function() {
		carousel.startAuto();
	});
};
jQuery(document).ready(function() {
	jQuery('#news_articles_ul').jcarousel({
		scroll: 1,
		auto: 3,
		wrap: 'last',
		initCallback: mycarousel_initCallback
	});
});
</script>
<?php } ?>
<ul id="news_articles_ul" class="jcarousel-skin-featured-images">
	<?php if(!empty($settings['Title'])) echo $before_title . $settings['Title'] . $after_title; 
	foreach($posts as $post) :
		setup_postdata($post);
		$end_link=strpos($post->post_content,"</a>")+4;
		if(substr($post->post_content,0,2)=='<a')$post->post_content=substr($post->post_content,$end_link);
		$end_img=strpos($post->post_content,">")+1;
		if(substr($post->post_content,0,4)=='<img')$post->post_content=substr($post->post_content,$end_img);
		$end_caption=strpos($post->post_content,"[/caption]")+10;
		if(substr($post->post_content,0,8)=='[caption')$post->post_content=substr($post->post_content,$end_caption);
		$end_caption=strpos($post->post_content,"[/caption]")+10;
		?>
		<li class="post" id="post-<?php echo $post->ID ?>">
			<h4 class="post_title"><?php echo $post->post_title; ?></h4>
			<div class="entry">
				<?php echo $helper->_substr($post->post_content, $settings['excerpt_size']); ?> ... <a class="read-more" href="<?php echo get_permalink($post->ID); ?>">Continue Reading <span class="continue">&raquo;</span></a>
				<div class="clear"></div>
			</div>
		</li>
	<?php endforeach; ?>
</ul>