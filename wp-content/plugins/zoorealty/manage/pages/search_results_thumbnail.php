<?php
//metadata: access_level=8
if(isset($_POST['submit'])): //p($_POST['search_results_thumbnail']);
	add_option('realty_search_results_thumbnail', $_POST['search_results_thumbnail']);
	update_option('realty_search_results_thumbnail', $_POST['search_results_thumbnail']);
	$return = 'Search Results Updated.';
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;

$default_options =  array (
	  	'price' => '',
	  	'address' => array('street_address', 'suburb','state'),
	  	'property_type' => '',
	  	'headline' => '',
	  	'description' => '150',
	  	'thumbnail' =>'',
	  	'building_size' => array('Sqms', 'Hectares', 'Acres'),
		'available_at' => '',
	 	'maximum_persons' => '',
		'opentimes' => '',
	  	'land_size' => array('Sqms', 'Hectares', 'Acres'),
	  	'primary_contact' => array('name', 'phone', 'email', 'mobile'),
	  	'secondary_contact' => array('name', 'phone', 'email', 'mobile'),
	    'number_of_rooms' => array('bedrooms','bathrooms','carspaces','pool' ),
	 	'auction' => array('auction_date','auction_time'),
		'rates' =>array ('land_tax', 'strata_fees', 'water_rates', 'rates', 'management_fee')
	);

$settings = get_option('realty_search_results_thumbnail');
?>
<style type="text/css">
<!--
/* ie */
* html div.widget-liquid-left-holder,
* html div.widget-liquid-right {
	display: block;
	position: relative;
}

/* 2 column liquid layout */
div.widget-liquid-left-holder {
	float: left;
	clear: left;
	width: 100%;
	margin-right: -310px;
}

div.widget-liquid-left {
	margin-right: 320px;
}

div.widget-liquid-right {
	float: right;
	clear: right;
	width: 300px;
	position: relative;
}

/* pasitioning etc. */
form#widgets-filter {
	position: relative;
}

div#available-widgets-filter {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-color: #C0C0C0;
}

ul#widget-list {
	list-style: none;
	margin: 0;
	padding: 0;
}

ul#widget-list li.widget-list-item {
	padding: .7em 0.9em;
	margin: 0;
	border-bottom: 1px solid;
	border-color: #C0C0C0;
	line-height: 1;
	width: 500px !important;
}

p.widget-title span {
	float: left;
}

p.widget-title a {
	float: right;
	text-decoration: underline;
	border-bottom: none;
	margin-left: 1em;
}

ul#widget-list li.widget-list-item p.widget-title {
	position: relative;
	margin: 0;
	padding: .5em 1em;
	width: 200px;
	float: left;
	font-size: 13px;
	-moz-border-radius: 3px;
	-khtml-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
}

#dragHelper p.widget-title {
	padding: .5em 1em;
	margin: 0;
}

ul#widget-list li.widget-list-item div.widget-description {
	display: block;
	margin: 0 0 0 200px;
	padding: 0 0 0 4em;
	font-size: 11px;
}

ul#widget-list li.widget-list-item ul.widget-control-info {
	display: none;
}

div#sidebar-info {
	padding: 0 1em;
	margin-bottom: 1em;
	font-size: 11px;
}

ul.widget-control-list {
	list-style: none;
	margin: 0;
	padding: 0 1em;
}

div#current-widgets p.submit {
	padding: 1em;
}

li.widget-list-control-item {
	margin: 1em 0;
	-moz-border-radius: 3px;
	-khtml-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
}

li.widget-list-control-item p,
#dragHelper li.widget-list-control-item p,
li.widget-sortable p {
	margin: 0;
	cursor: move;
	font-size: 13px;
	padding: 0.4em 0.8em;
	position: relative;
	-moz-border-radius: 3px;
	-khtml-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
}

.widget-control-save,
.widget-control-remove {
	margin-right: 8px;
	float: left;
	text-decoration: none;
}

li.widget-list-control-item p.widget-title a,
#dragHelper li.widget-list-control-item p.widget-title a,
#draghelper li.widget-list-control-item p.widget-title a:visited {
	right: 1em;
}

li.widget-list-control-item p.widget-title a:hover {
	text-decoration: none;
	border-bottom: none;
}

li.widget-list-control-item div.widget-control {
	border-width: 0 1px 1px;
	border-style: none solid solid;
	border-color: #C0C0C0;
	display: none;
	padding: 15px;
	font-size: 11px;
	position: relative;
}

li.widget-list-control-item div.widget-control p {
	margin: 0 0 1em;
	padding: 0;
}

ul.widget-control-list div.widget-control-actions {
	padding: 0.5em 0 0;
}

.widget-control-edit {
	font-size: 10px;
	font-weight: normal;
}

div#current-widgets {
	padding-top: 1em;
	border-width: 1px 0 0;
	border-style: solid none none;
	border-color: #C0C0C0;
}

#widget-controls .widefat {
	width: 92%;
	padding: 3px;
}

#widget-controls select.widefat {
	width: auto;
	display: block;
}

/* additional 20/2/2012 */
#available-widgets a.widget-action {
    display: block;
}
a.widget-action {
    width: auto !important;
}
.widget-top a.widget-action {
    background: none !important;
}
-->
</style>
<?php
wp_print_scripts('jquery-ui-core');
wp_print_scripts('jquery-ui-sortable');
?><script type="text/javascript">
/* <![CDATA[ */
jQuery(document).ready(function(){
	jQuery(".widget-sortable").sortable({});
	var parent = null;
	jQuery(".widget-control-add").click(function() {
		jQuery(this).hide();
		parent = jQuery(this).parent().parent();
		parent.children(".widget-control-info").clone(true).insertAfter(jQuery("#current-sidebar")).removeClass('widget-control-info').addClass(' widget-control-list');
		return false;
	});
	jQuery(".widget-control-edit").click(function() {
		parent = jQuery(this).parent().parent().parent();//jQuery('#'+ jQuery(this).attr('rel')); 
		//toggleWidgetControl(parent, jQuery(this));
		if(jQuery(this).html() =='Edit'){
			parent.children(".widget-control").show();
			jQuery(this).html('Cancel');
		}else{
			parent.children(".widget-control").hide();	
			jQuery(this).html('Edit');					
		}
		return false;
	});	
	/*jQuery(".widget-control-save").click(function() {
		jQuery('#'+ jQuery(this).attr('rel')).trigger('click');		
		return false;
	});	*/
	jQuery(".widget-control-remove").click(function() {
		parent = jQuery(this).parent().parent().parent();
		jQuery('#' + jQuery(this).attr('rel') + ' .widget-control-add').show();
		parent.remove();
		return false;
	});		
});//ends document).ready
/* ]]> */
</script>
<div class="wrap">
<h2>Results Page </h2>

	<form method="get" action="" id="widgets-filter">

	<div class="widget-liquid-right" id="available-sidebars">
		<p style="font-size:15px;"><label for="sidebar-selector">Current Display</label></p>
		<p>You are displaying <?php echo count($settings); ?> items in your results page.</p>
		<p>You can "Add" items from the left column, make sure you save your changes.</p>

	</div>

	</form>

	<div class="widget-liquid-left-holder" id="widget-content">

		<div class="widget-liquid-left" id="available-widgets">

			
	<ul id="widget-list">
		
		<?php foreach($default_options as $name=>$vars): $caption = $helper->humanize($name); ?>
		<li class="widget-list-item" id="widget-list-item-<?php echo $name ?>">
			<p class="widget-title widget-draggable">

				<span><?php echo $caption; ?></span>
								
				<a href="#" class="widget-action widget-control-add" <?php if(is_array($settings) and array_key_exists($name, $settings)) echo ' style="display:none"'; ?>>Add</a>
				<br class="clear"/>

			</p>

			<ul class="widget-control-info">

				<li class="widget-list-control-item widget-sortable" id="widget-list-control-<?php echo $name ?>">
		<div class="widget-top">
		<p class="widget-title"><span><?php echo $helper->humanize($name); ?></span>
			<?php if(is_array($vars) or !empty($vars)) : ?>
			<a href="#" id="widget-list-edit-<?php echo $name ?>" class="widget-action widget-control-edit">Edit</a>
			<?php else: ?>
			<a href="#" rel="widget-list-item-<?php echo $name ?>" class="widget-action widget-control-remove">Remove</a>
			<input type="hidden" value="" name="search_results_thumbnail[<?php echo $name; ?>]" />
			<?php endif; // if(is_array($vars))?><br class="clear"/>
		</p></div>

		<div class="widget-control">
		<?php if(is_array($vars)):
		 foreach($vars as $var): ?>
			<p><input type="checkbox" value="<?php echo $var; ?>" name="search_results_thumbnail[<?php echo $name; ?>][]" checked="checked" /><label><?php echo $var; ?></label></p>
		<?php endforeach;
		elseif(!empty($vars)): ?>
		 <p><input type="text" value="<?php echo $var; ?>" name="search_results_thumbnail[<?php echo $name; ?>]" /><label><?php echo $var; ?></label></p>
		 <?php endif;?>
			<div class="widget-control-actions">
				<!-- <a href="#" rel="widget-list-edit-<?php echo $name ?>" class="button widget-action widget-control-save edit alignleft">Done</a>-->
				<a href="#" rel="widget-list-item-<?php echo $name ?>" class="button widget-action widget-control-remove alignright">Remove</a>
				<br class="clear"/>
			</div>
		</div>

</li>
			</ul>

						
			<div class="widget-description"></div>

			<br class="clear"/>

		</li>
<?php endforeach; ?>

	</ul>
		</div>
	</div>

	<form method="post" action="" id="widget-controls">
	<div class="widget-liquid-right" id="current-widgets">
		<div id="current-sidebar">
			
	<ul class="widget-control-list widget-sortable">
<?php if(is_array($settings)) foreach($settings as $setting=>$values): ?>
<li id="widget-list-control-<?php echo $setting; ?>" class="widget-list-control-item ">
		<div class="widget-top">
		<p class="widget-title"><span><?php echo $helper->humanize($setting); ?></span>
		<?php if(is_array($values) or !empty($values)) : ?>
			<a href="#" id="widget-list-edit-<?php echo $setting; ?>" class="widget-action widget-control-edit">Edit</a>
			<?php else: ?>
			<a href="#" rel="widget-list-item-<?php echo $setting ?>" class="widget-action widget-control-remove">Remove</a>
			<input type="hidden" value="" name="search_results_thumbnail[<?php echo $setting; ?>]" />
			<?php endif; // if(is_array($vars))?>
			<br class="clear"/>
		</p></div>
		<?php if(is_array($default_options[$setting]) or !empty($values)): ?>
		<div class="widget-control">
<?php
	if(is_array($default_options[$setting])):
		foreach($default_options[$setting] as $var): ?>
		<p><input type="checkbox" name="search_results_thumbnail[<?php echo $setting; ?>][]" value="<?php echo $var; ?>"<?php if(is_array($values) and in_array($var, $values)) echo 'checked="checked" '; ?>/><label><?php echo $var; ?></label></p>
<?php 
		endforeach; 
	elseif(!empty($values)): ?>
		<p><input type="text" name="search_results_thumbnail[<?php echo $setting; ?>]" value="<?php echo $values; ?>" /></p>
<?php 
	endif; //!empty($values) ?>
			<div class="widget-control-actions">
				<a class="button widget-action widget-control-remove alignright" rel="widget-list-item-address" href="#">Remove</a>
				<br class="clear"/>
			</div>
		
		</div>
		<?php endif;//is_array($default_options[$setting] ?>
		</li>
<?php endforeach; //$settings  ?>
	</ul>


		</div>

		<p class="submit"><input type="submit" value="Save Changes" class="button-primary" name="submit"/></p>
	</div>

	</form>
	<br class="clear"/>

</div><!-- ends .wrap -->