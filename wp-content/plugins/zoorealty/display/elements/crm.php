<?php
$path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require($path.'/wp-load.php');
global $realty, $helper, $wpdb;
if(!empty($_GET['property_id']) && !empty($_GET['time'])){
	$blogname=str_replace("&amp;","&",get_option('blogname'));
	$filename=str_replace(" ","_",$blogname)."_Auction";
	$property=$wpdb->get_results("select * from properties where id=".intval($_GET['property_id']),ARRAY_A);
	$property=$property[0];
	if($property['display_address']==1){
		if(!empty($property['unit_number']))$street_address = $property['unit_number'] .'/';
		$street_address .=  $property['street_number'] . ' ' .  $property['street']  ;	
	}
	header("Content-Type: text/x-vCalendar");
	header("Content-Disposition: inline; filename=$filename.vcs");

	$gmt_offset=get_option('gmt_offset');
	$time_diff=$gmt_offset*3600;
	$start_date=date("Ymd\THi00", strtotime($_GET['time'])-$time_diff);
	$subject="$blogname - Auction";
	$suburb=ucwords(strtolower($property['suburb']));

	print "BEGIN:VCALENDAR\n";
	print "VERSION:1.0\n";
	print "PRDID:RBC Web Calendar\n";
	print "METHD:PUBLISH\n";
	print "TZ:-08\n";
	print "BEGIN:VEVENT\n";
	print "DTSTART:".$start_date."\n";
	//print "DTEND:".$end_date."\n";
	print "LOCATION:".$street_address.", ".$suburb."\n";
	print "TRANSP:PAQUE\n";
	print "UUID:".microtime()."\n";
	print "DTSTAMP:".date('Ymd').'T'.date('His')."\n"; // required by Outlook
	print "SUMMARY:".$subject . "\n"; //subject line shows name field
	print "DESCRIPTION;ENCODING=QUOTED-PRINTABLE: " . //description shows all fields
	"Auction Time for $suburb property\n";
	print "BEGIN:VALARM\n";
	print "TRIGGER:PT15M\n";
	print "ACTIN:DISPLAY\n";
	print "END:VALARM\n";
	print "END:VEVENT\n";
	print "END:VCALENDAR\n";
	exit;
}

if(!empty($_GET['property_id']) && empty($_GET['time'])){
	$blogname=str_replace("&amp;","&",get_option('blogname'));
	$filename=str_replace(" ","_",$blogname)."_Inspections";
	$property=$wpdb->get_results("select * from properties where id=".intval($_GET['property_id']), ARRAY_A);
	$property=$property[0];
	if($property['display_address']==1){
		if(!empty($property['unit_number']))$street_address = $property['unit_number'] .'/';
		$street_address .=  $property['street_number'] . ' ' .  $property['street']  ;	
	}
	header("Content-Type: text/x-vCalendar");
	header("Content-Disposition: inline; filename=$filename.vcs");

	$gmt_offset=get_option('gmt_offset');
	$time_diff=$gmt_offset*3600;
	$start_date=date("Ymd\THi00", strtotime($_GET['start_date'])-$time_diff);
	$end_date=date("Ymd\THi00", strtotime($_GET['end_date'])-$time_diff);
	$subject="$blogname - Inspection";
	$suburb=ucwords(strtolower($property['suburb']));

	print "BEGIN:VCALENDAR\n";
	print "VERSION:1.0\n";
	print "PRDID:RBC Web Calendar\n";
	print "METHD:PUBLISH\n";
	print "TZ:-08\n";
	print "BEGIN:VEVENT\n";
	print "DTSTART:".$start_date."\n";
	print "DTEND:".$end_date."\n";
	print "LOCATION:".$street_address.", ".$suburb."\n";
	print "TRANSP:PAQUE\n";
	print "UUID:".microtime()."\n";
	print "DTSTAMP:".date('Ymd').'T'.date('His')."\n"; // required by Outlook
	print "SUMMARY:".$subject . "\n"; //subject line shows name field
	print "DESCRIPTION;ENCODING=QUOTED-PRINTABLE: " . //description shows all fields
	"Inspection Time for $suburb property\n";
	print "BEGIN:VALARM\n";
	print "TRIGGER:PT15M\n";
	print "ACTIN:DISPLAY\n";
	print "END:VALARM\n";
	print "END:VEVENT\n";
	print "END:VCALENDAR\n";
	exit;
}

if(!empty($_GET['user_id'])){
	$user=$wpdb->get_results("select * from users where id=".intval($_GET['user_id']), ARRAY_A);
	$user=$user[0];
	$office=$wpdb->get_results("select * from office where id=".intval($user['office_id']), ARRAY_A);
	$office=$office[0];
	if(empty($user))return;
	$filename=$user['firstname']."_".$user['lastname'];
	$N=$user['lastname'].", ".$user['firstname'];
	$FN=$user['firstname']." ".$user['lastname'];
	$ORG=$office['name'];
	//$TITLE=$user['group'];
	$TITLE=$user['role'];
	$WORK=$user['phone'];
	$CELL=$user['mobile'];
	$FAX=$user['fax'];
	$EMAIL=$user['email'];
	$URL=get_option('siteurl');
	if(!empty($office['unit_number']))$street_address = $office['unit_number'] .'/';
	$street_address .=  $office['street_number'] . ' ' .  $office['street']  ;	
	$state_suburb=$office['suburb'] . ' ' .  $office['state']. ' ' .  $office['zipcode']  ;
	$country=$office['country']  ;
	header("Content-Type: text/x-vcard; charset=utf-8");
	header("Content-Disposition: attachment; filename=$filename.vcf");

	print "BEGIN:VCARD\n";
	print "VERSION:3.0\n";
	print "N:$N\n";
	print "FN:$FN\n";
	print "ORG:$ORG\n";
	print "TITLE:$TITLE\n";
	print "TEL;WORK;VOICE: $WORK\n";
	//print "TEL;HOME;VOICE: 888888\n";
	print "TEL;WORK;FAX: $FAX\n";
	print "TEL;CELL;VOICE: $CELL\n";
	print "URL;WORK: $URL\n";
	print "ADR;WORK:;;$street_address;;$state_suburb;;$country\n";
	print "EMAIL;PREF;INTERNET:$EMAIL\n";
	print "END:VCARD\n";
	exit;

}
?>