<?php
global $helper, $current_user, $realty, $wpdb;
if(!empty(intval($_SESSION['za_user_id'])))$office_id=$wpdb->get_var("select id from office where wp_user_id='".intval($_SESSION['za_user_id'])."' and contributor=1");
else if(current_user_can('administrator')){ $office_id=intval($_GET['office_id']);$is_admin=1;}

if (!$office_id){ ?>
	<script type="text/javascript">
	alert("Please login to access this page.");
	location.href='<?php echo $realty->siteUrl."agent-login/"; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;

if($_GET['task']=='delete' && !empty($_GET['id'])){
	if($wpdb->query("delete from properties where office_id in($office_id) and id=".$_GET['id'])){
		$wpdb->query("delete from property_meta where property_id=".$_GET['id']);
		$wpdb->query("delete from property_features where property_id=".$_GET['id']);
		echo "<div id='success'><span>Listing succesfully deleted.</span></div>";
	}
}

$private_listings=$wpdb->get_var("select contributor from office where id in ($office_id)");

$extra_condition="and office_id in ($office_id)";
$condition['status']='1,2,3,4,5,6';
$condition['all_listings']=1;
$condition['office_id']=$office_id;
$condition['limit']=500;
$condition['id']='';
$condition['property_id']='';
$condition['order']='created_at';
$listings=$realty->properties($condition);

//Active / Expired / Suspended / Audit 
?>

<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/table_sorter.css" type="text/css" media="all" />
<script type="text/javascript">jQuery(function() { jQuery("#properties_table").tablesorter({ widgets: ['zebra'] }); });</script>	

<div class="dashboard-subnav">
    <h4>Create New Listing</h4>
	<div class="nav-sites">     
		<ul>
			<li><a href="<?php echo $realty->siteUrl."dashboard/new-listing/?type=ResidentialSale&office_id=$office_id"; ?>">Residential Sale</a></li>
			<li><a href="<?php echo $realty->siteUrl."dashboard/new-listing/?type=ResidentialLease&office_id=$office_id"; ?>">Residential Lease</a></li>
			<li><a href="<?php echo $realty->siteUrl."dashboard/new-listing/?type=Commercial&office_id=$office_id"; ?>">Commercial</a></li>
			<li><a href="<?php echo $realty->siteUrl."dashboard/new-listing/?type=BusinessSale&office_id=$office_id"; ?>">Business Sale</a></li>
			<li><a href="<?php echo $realty->siteUrl."dashboard/new-listing/?type=ShareAccomodation&office_id=$office_id"; ?>">Share Accomodation</a></li>
		</ul>
	</div>
    <div class="clear"></div>
</div>

<table class="tablesorter" cellpadding="0" cellspacing="0" id="properties_table">
	<thead>
		<tr class="th">
			<th class="th_id">ID</th>
			<th class="th_type">Listing</th>
			<th class="th_address">Address</th>
			<th class="th_suburb">Suburb</th>
			<th class="th_property_type">Type</th>
			<th class="th_status">Status</th>
			<?php if($private_listings==1){ ?>				
			<th class="th_paid">Paid</th>
			<?php } ?>
			<th class="th_edit">Actions</th>
		</tr>
	</thead>
	<?php 
	foreach($listings as $property): 	
	if(!is_array($property)) continue; 
	switch($property['status']){
		case '1':$status='Available';break;
		case '2':$status='Sold';break;
		case '3':$status='Draft';break;
		case '4':$status='Under Offer';break;
		case '5':$status='Withdrawn';break;
		case '6':$status='Leased';break;
	}
	$property['url']=$realty->siteUrl.$property['id']."/";
	?>
	<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
		<td class="td_id">
			<form name="preview_form_<?php echo $property['id']; ?>" id="preview_form_<?php echo $property['id']; ?>" action="<?php echo $property['url']; ?>" method="post" target="_blank">
				<input type="hidden" name="preview" value="1">	
				<a href="javascript:document.forms['preview_form_<?php echo $property['id']; ?>'].submit();" title="View Detail"><?php echo $property['id']; ?></a>
			</form>
		</td>
		<td class="td_type"><?php echo ($property['share'])?'ShareAccomodation':$property['type']; ?></td>
		<td class="td_address"><?php echo ($property['street_address']=='')?'no address available':$property['street_address']; ?></td>
		<td class="td_suburb"><?php echo strtoupper($property['suburb']); ?></td>
		<td class="td_property_type"><?php echo $property['property_type']; ?></td>
		<td class="td_status"><a href="<?php echo $realty->siteUrl.'dashboard/status/?id='.$property['id']."&office_id=".$property['office_id'];?>"><?php echo $status; ?></a></td>	
		<?php if($private_listings==1){ ?>		
		<td class="th_paid">
			<a href="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id'];?>"><?php echo ($property['listingpaid'] != '1')?'Pay':'Paid'; ?></a>
		</td>
		<?php } ?>
		<td class="td_edit">
			<a class="ed_edit" title="Edit" href="<?php echo $realty->siteUrl.'dashboard/detail/?id='.$property['id']."&type=".$property['type']."&office_id=".$property['office_id'];?>" >Edit</a>
			<a class="delete" onClick="return confirm('You are about to delete the this listing.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl.'dashboard/listings/?id='.$property['id']."&task=delete&office_id=".$property['office_id'];?>" >Delete</a>
		</td>
	</tr>
	<?php endforeach; ?>
</table>