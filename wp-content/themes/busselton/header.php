<?php 
if(isset($_GET['main_site'])){ // add this code on the 1st line to save cookie for mobile for the next 1 hour
	setcookie("main_site", 1,time()+(60*60), '/');
	$_COOKIE['main_site']=1;
}// end 
global $options; foreach ($options as $value) { if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); } } ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:fb='http://www.facebook.com/2008/fbml'>
<!--[if lt IE 7 ]><html class="die lt-ie9 ie6" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7 ]><html class="die lt-ie9 ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8 ]><html class="die lt-ie9 ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9 ]><html class="die ie9" <?php language_attributes(); ?>><![endif]-->

<head>

<?php 
$text = $_SERVER['HTTP_USER_AGENT'];
$var[1] = 'Windows CE';
$var[2] = 'Blazer/';
$var[3] = 'Palm';
$var[4] = 'EPOC32-WTL/';
$var[5] = 'Netfront/';
$var[6] = 'Mobile Content Viewer/';
$var[7] = 'PDA';
$var[8] = 'BlackBerry';
$var[9] = 'MOT-';
$var[10] = 'UP.Link/';
$var[11] = 'Smartphone';
$var[12] = 'portalmmm/';
$var[13] = 'Nokia';
$var[14] = 'Symbian';
$var[15] = 'J2ME';
$var[16] = 'Mini/';
$var[17] = 'AU-MIC/';
$var[18] = 'Sharp';
$var[19] = 'SIE-';
$var[20] = 'SonyEricsson';
$var[21] = 'SAMSUNG-';
$var[22] = 'Panasonic';
$var[23] = 'Siemens';
$var[24] = 'Sony';
$var[25] = 'Pocket PC';
$var[26] = 'T-Mobile';
$var[27] = 'Motorola';
$var[28] = 'Samsung';
$var[29] = 'iPhone';
$var[30] = 'iPod';
$var[31] = 'Wap';
$var[33] = 'android';
$var[34] = 'vodafone';
$var[35] = 'ericy';
$var[36] = 'alcatel';
$var[37] = 'au-mic';
$var[38] = 'WinW';
$var[39] = 'philips';
$var[40] = 'sanyo';
$var[41] = 'avantgo';
$var[42] = 'danger';
$var[43] = 'series60';
$var[44] = 'palmsource';
$var[45] = 'rover';
$var[46] = 'ipaq';
$var[47] = 'nitro';
$var[48] = 'midp';
$var[49] = 'cldc';
$var[50] = 'audiovox';
$var[51] = 'LG';
$var[52] = 'Handspring';
global $realty;
if(empty($_COOKIE['main_site']) && is_front_page()){
	foreach($var as $item){
		$ausg = stristr($text, $item);
		if(strlen($ausg)>0 && strpos($text,'MSIE')===false)
		{
		header('location: http://m.busseltonfn.com.au/');
		break;
		}
	}
	?>
	<script type="text/javascript">
	if (navigator.userAgent.match(/iPhone/i)) {
		location.href='<?php echo 'http://m.busseltonfn.com.au/'; ?>';
	}
	</script>
<?php } 

wp_deregister_script( 'l10n' );wp_print_scripts('jquery');wp_head();

if(empty($_COOKIE['main_site']) && is_page('property')){
	global $wp_query;
	$query_vars=$wp_query->query_vars;
	$property_id=$query_vars['property_id'];
	foreach($var as $item){
		$ausg = stristr($text, $item);
		if(strlen($ausg)>0 && strpos($text,'MSIE')===false)
		{			
			header('location: http://m.busseltonfn.com.au/'.$property_id);
			break;
		}
	}
	?>
	<script>
	if (navigator.userAgent.match(/iPhone/i)) {
		location.href='http://m.busseltonfn.com.au/<?php echo $property_id; ?>';
	}
	</script>
<?php } ?>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
    <link rel="shortcut icon" type="image/ico" href="<?php echo bloginfo('template_url'); ?>/favicon.ico?v=1.00" />
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_deregister_script( 'l10n' ); wp_print_scripts('jquery'); wp_head(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-11477462-48']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/ng-template" id="dropwdown.html">
    <button type="button" class="btn btn-default dropdown-toggle light-text" data-toggle="dropdown">
        <span class="selected-text">{{ selectedText }}</span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a ng-repeat="item in items" class="dropdown-option" ng-class="{ 'selected': selectedValue == item.value }" href="javascript:void(0);" ng-click="selectItem(item)" data-value="{{ item.value }}">{{ item.text }}</a>
        </li>
    </ul>
</script>

</head>

<body id="<?php body_css_id(); ?>" <?php body_class(); ?>>

<div id="wrapper">

    <div id="header">

        <div class="headerwrap">

            <h1 class="logo">
            	<a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
                	<img src="<?php bloginfo('template_url'); ?>/img/<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>" />
				</a>
			</h1>

			<?php wp_nav_menu(array('menu'=>'Main Menu', 'container_id'=>'smoothmenu', 'container_class'=>'ddsmoothmenu'));?>

            <div class="office-address">
            	<span><?php echo get_option('header_address'); ?> </span>
                <span><?php echo get_option('header_city'); ?> </span>
                <span><?php echo get_option('header_state'); ?> </span>
                <span><?php echo get_option('header_zip'); ?></span>
                <span class="phone"><?php echo get_option('header_phone'); ?></span>
            </div>

            <div class="social-networks">
				<div class="logotag"></div>
                <?php
                    $networks = array(
                        array("id" => "facebook", "value" => $facebook),
                        array("id" => "twitter", "value" => $twitter),
                        array("id" => "digg", "value" => $digg),
                        array("id" => "myspace", "value" => $myspace),
                        array("id" => "linkedin", "value" => $linkedin),
                        array("id" => "youtube", "value" => $youtube)
                    );
                    if (($facebook != '') || ($twitter != '') || ($digg != '') || ($myspace != '') || ($linkedin != '') || ($youtube != '')) { ?>
                    <ul>
                    <?php foreach ( $networks as $network ) { ?>
                        <?php if ($network['value'] != '') { ?><li><a class="<?php echo $network['id'];?>" href="<?php echo $network['value']; ?>" title="<?php echo $network['id'];?>" target="_blank"></a></li><?php } ?>
                    <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>

	<div id="container">