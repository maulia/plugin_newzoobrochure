?php<?php global $realty;?>
<table class="property" width="765px" border="0" cellspacing="0" cellpadding="0" style="margin: 0 0 18px; padding: 0;font-family: Verdana, Arial, sans-serif;line-height:1.25;">
	<tr>
		<th colspan="2"><img src="<?php echo (get_option('sm_logo'))?get_option('sm_logo'):get_option('logo'); ?>" alt="<?php echo $realty->blogname; ?>" style="max-width:760px;" /></th>
	</tr>
	<tr><td colspan="2"><?php echo $params['extra_message']; ?></td></tr> 
	<?php if(!empty($realty->property)): ?>
	<tr>
		<td style="width: 240px; height: 210px; padding: 20px 0; border-bottom: 1px solid #ccc; vertical-align: top;">
			<div style="width: 200px; height: 151px; float: left; position: relative;">
				<a href="<?php echo $realty->property['url']; ?>" title="<?php echo $realty->property['headline']; ?>" style="text-decoration: none !important;">
				<img src="<?php echo $realty->property['thumbnail']; ?>" alt="<?php echo $realty->property['street_address']; ?>" width="200" height="150" style="background-color: #fff; padding: 9px; border: 1px solid #ccc;" /></a>
			</div>
		</td>

		<td style="width: 545px;height: 210px; padding: 20px 0; border-bottom: 1px solid #ccc; vertical-align: top;">
			<h4 style="font-size:15px; font-weight: normal; color: #18222c; margin: 0; padding: 0 0 3px;"><?php echo strtoupper($realty->property['suburb']); ?></h4>
			<p style="margin:0;padding: 0 0 6px;float: left; font-size: 12px;color: #666;"><?php echo $realty->property['price']; ?>&#8226;&nbsp;</p>
			<p style="margin:0;padding: 0 0 6px; float: left; font-size: 12px;color: #666;"><?php echo $realty->property['property_type']; ?></p>

			<hr style="width: 100%; background-color: #fff; margin: 0; padding: 0; border: 0; clear: both; color: #fff;" />

			<p style="margin:0;padding:0 0 6px; clear: both;font-size: 12px; line-height: 18px; color: #444;">
				<?php echo substr($realty->property['description'], 0, 190); ?> &hellip;<a href="<?php echo $realty->property['url']; ?>" style="color: #444;">View more &raquo;</a>
			</p>

			<ul style="margin: 5px 0 12px 0;padding: 0; text-align: left; list-style: none;font-size: 12px; line-height: 18px;">
				<?php if($realty->property['bedrooms']){ ?><li style="color: #666;"><?php echo $realty->property['bedrooms']; ?> bed</li><?php } ?>
				<?php if($realty->property['bathrooms']){ ?><li style="color: #666;"><?php echo $realty->property['bathrooms']; ?> bath</li><?php } ?>
				<?php if($realty->property['carspaces']){ ?><li style="color: #666;"><?php echo $realty->property['carspaces']; ?> car</li><?php } ?>
			</ul>
			
			<div class="land_building_size">
				<?php if(!empty($realty->property['land_size'])){ ?>
				<p style="margin:0 0 6px;padding: 0; font-size: 12px;color: #777;">Land Size: <span class="land_size" style="color: #404e5e;"><?php echo $realty->property['land_size']." ".$realty->property['land_area_metric']; ?></span></p>
				<?php } ?>
				<?php if(!empty($realty->property['building_size'])){ ?>
				<p style="margin:0;padding: 0; font-size: 12px;color: #777;">Building Size: <span class="building_size" style="color: #404e5e;"><?php echo $realty->property['building_size']." ".$realty->property['floor_area_metric']; ?></span></p>
				<?php } ?>
			</div>
		</td>
	</tr>
<?php endif;// !empty ($realty->property) ?>
</table>