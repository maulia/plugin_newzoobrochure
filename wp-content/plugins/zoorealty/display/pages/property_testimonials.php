<?php
$src=str_replace('wp-content/plugins/Realty/style/stickers/','',$this->settings['property_stickers']['sticker_src']);
$style="style".(str_replace("sticker","",str_replace(".png","",$src))+1);
switch($this->settings['property_stickers']['sticker_colour']){
	case '#2b497d';$colors='dk_blue';break;
	case '#2b7d36';$colors='dk_green';break;
	case '#857a82';$colors='grey';break;
	case '#70c141';$colors='lite_green';break;
	case '#4cb2c2';$colors='lite_blue';break;
	case '#d78c23';$colors='orange';break;
	case '#7e2a1d';$colors='red';break;
	case '#7d5f2b';$colors='tan';break;
	case '#d7ce31';$colors='yellow';break;
}

$property_testimonials=$this->property_testimonials();
if(empty($property_testimonials))return;
foreach($property_testimonials as $item){ ?>
	<div class="testimonial-block">
		<div class="testimonial-top">
			<span class="contact_name"><?php echo $item['first_name']; ?></span>
			<span class="suburb"><?php echo $item['suburb']; ?></span>
		</div>
		<p class="note"><?php echo $item['note']; ?></p>
		<div class="agent-detail">
			<div class="image">
				<a href="<?php echo $item['user_url']; ?>" title="View <?php echo $item['agent_name']; ?>' Profile"><img src="<?php echo $item['agent_photo']; ?>"></a>
			</div>
			<div class="agent_name"><?php echo $item['agent_name']; ?></div>
			<div class="agent_position"><?php echo $item['agent_position']; ?></div>
			<div class="agent_phone"><strong>P: </strong><?php echo $item['agent_phone']; ?></div>
			<div class="agent_mobile"><strong>M : </strong><?php echo $item['agent_mobile']; ?></div>
			<div class="agent_profile_link"><a href="<?php echo $item['user_url']; ?>" title="View <?php echo $item['agent_name']; ?>' Profile">Agent Profile</a></div>
		</div>
		<div class="listing-image">
			<div class="image">
				<a href="<?php echo $item['property_url']; ?>" title="<?php echo htmlspecialchars($item['headline']); ?>"><img src="<?php echo $item['thumbnail']; ?>"></a>
				<?php if ($item['status']=='2'):?>
				<div class="image_overlay">
					<div class="<?php echo "sticker sold ".$colors." ".$style;?>">
						
					</div>
				</div>
				<?php elseif ($item['status']=='6'):?>
				<div class="image_overlay">
					<div class="<?php echo "sticker leased ".$colors." ".$style;?>">
					
					</div>
				</div>
				<?php elseif ($item['status']=='4'):?>
				<div class="image_overlay">
					<div class="<?php echo "sticker under_offer ".$colors." ".$style;?>">
						
					</div>
				</div>
				<?php endif;  ?>
			</div>
		</div>
        <div class="clear"></div>
	</div>
<?php } ?>