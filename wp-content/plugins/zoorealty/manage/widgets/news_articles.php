<?php
$wp_categories = get_categories();
foreach ($wp_categories as $key=>$value)
	$news_categories[$value->term_id] = $value->slug;
?>
<p><label>Number</label> <br><input value="<?php echo $settings['Number']; ?>" name="realty_widget_<?php echo $caption; ?>[Number]" type="text"></p>
<p><label>Title</label><br />
<input value="<?php echo $settings['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Title]" type="text">
</p>
<p><label>Excerpt Size</label><br />
<input value="<?php echo $settings['excerpt_size']; ?>" name="realty_widget_<?php echo $caption; ?>[excerpt_size]" type="text"> characters.
</p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[rotate]" type="checkbox"<?php if($settings['rotate']) echo ' checked="checked"'; ?>> <label>Display with rotate feature</label> </p>
<p><label>Categories to be displayed</label><br />
<?php foreach($news_categories as $term_id=>$slug): ?>
<input type="checkbox"  name="realty_widget_<?php echo $caption; ?>[categories][]" value="<?php echo $term_id; ?>"<?php if(is_array($settings['categories']) and in_array($term_id, $settings['categories'])) echo ' checked="checked"'; ?> /> <?php echo $slug; ?><br />
<?php endforeach; ?>
</p>