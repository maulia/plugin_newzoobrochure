<?php

/* Font color */
if ($block_color[0] != '255' && $block_color[1] != '255' && $block_color[2] != '255') {
    $pdf->SetTextColor(255, 255, 255);
} else {
    $pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
}

/* Header block */
$pdf->SetFillColor($block_color[0], $block_color[1], $block_color[2]);
$pdf->Rect(0, 0, 219, 30, 'F');
$pdf->SetFont('Arial', '', 22);

/* auction text */
if ($auction_date) {
    $pdf->MultiCell(100, 5, "AUCTION", 0, 'L');
}

/* header logo */
$pdf->Image($brochure['logo'], 160, 5, 40, 0, '', '', array('max_width' => 40));
$pdf->SetXY(10, 30);

/* image */
$y = $pdf->getY();
if (!empty($properties_photos)) {
    $photos = array();
    $i = 0;
    foreach ($properties_photos as $photo) {
        $size = ( $i == 0 ) ? 'large' : 'medium';
        $photos[$i] = $photo[$size];
        $i++;
    }

    if (!empty($photos[0])) {  // Main image; The big one		
        $sizes = $wpdb->get_row("SELECT width, height FROM attachments WHERE url='" . $photos[0] . "'");
        $height = $sizes->height;
        $width = $sizes->width;
        if ($height > $width) {
            $pdf->Image($photos[0], 0, 27, 210, 149);
            $y = $y + 149;
        } else {
            $pdf->Image($photos[0], 0, 27, 210, 110);
            $y = $y + 110;
        }
    }

    $y_image = $y - 2;
    if (!empty($photos[1])) {
        $pdf->Image($photos[1], 0, $y_image, 69.5, 55);
        $y = $y + 55;
    }
    if (!empty($photos[2])) {
        $pdf->Image($photos[2], 70.6, $y_image, 69.5, 55);
    }
    if (!empty($photos[3])) {
        $pdf->Image($photos[3], 141.5, $y_image, 69.5, 55);
    }
}

/* address-block */
$pdf->SetFillColor($block_color[0], $block_color[1], $block_color[2]);
$pdf->Rect(0, $y - 4, 210, 10, 'F');

/* address */
if ($block_color[0] == '255' && $block_color[1] == '255' && $block_color[2] == '255') {
    $pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
}
$pdf->SetFont('Helvetica', '', 12);
$pdf->SetXY(10, $y);
$pdf->MultiCell(100, 2, $street_address . " " . $suburb . ", " . $state, 0, 'L');

/* beth, bath, car */
if ($bedrooms || $bathrooms || $carspaces) {

    $pdf->SetFont('Arial', '', 13);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->SetFillColor(0, 0, 0);
    /*
      $beds = floor($bedrooms);
      $baths = floor($bathrooms);
      if( strpos($bedrooms,'.5')!==false ){
      $half_bed='1';
      }
      if(strpos($bathrooms,'.5')!==false){
      $half_bath='1';
      } */
    $beds = $bedrooms;
    $baths = $bathrooms;

    $bed_icon = ( $bed_icon ) ? $bed_icon : $pdf->pluginPath . 'images/icons/bed_icon.png';
    $bath_icon = ( $bath_icon ) ? $bath_icon : $pdf->pluginPath . 'images/icons/bath_icon.png';
    $car_icon = ( $car_icon ) ? $car_icon : $pdf->pluginPath . 'images/icons/car_icon.png';

    if ($beds) {
        $pdf->SetXY(158.5, $y);
        $pdf->MultiCell(100, 2, $beds, 0, 'L');
        $pdf->Image($bed_icon, 150, $y - 2, 0, 0, '', '', array('max_height' => 7));
    }
    if ($baths) {
        $pdf->SetXY(179, $y);
        $pdf->MultiCell(100, 2, $baths, 0, 'L');
        $pdf->Image($bath_icon, 170, $y - 2, 0, 0, '', '', array('max_height' => 7));
    }
    if ($carspaces) {
        $pdf->SetXY(200, $y);
        $pdf->MultiCell(100, 2, $carspaces, 0, 'L');
        $pdf->Image($car_icon, 190, $y - 2.3, 0, 0, '', '', array('max_height' => 7));
    }
}

$y = $pdf->getY();
$y_content = $y + 7;

/* headline */
$pdf->SetTextColor($headline_color[0], $headline_color[1], $headline_color[2]);
$pdf->SetFont('Arial', '', 12);
$pdf->SetXY(10, $y_content);
$head = str_replace("\t", " ", $headline);
$head = preg_replace("[\n\r]", "\n", $headline);
$headline_string = ceil(strlen($head));
if ($headline_string <= 80) {
    $pdf->MultiCell(95, 4, $head, 0, 'J');
} else {
    $newtext = wordwrap(substr($head, 0, 80), 40);
    $pdf->MultiCell(95, 4, $newtext, 0, 'J');
}

/* description */
$y = $pdf->getY();
$y_desc = $y + 2;
$pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
$pdf->SetFont('Helvetica', '', 7);
$pdf->SetXY(10, $y_desc);
$desc = str_replace("\t", " ", $description);
$desc = preg_replace("[\n\r]", "\n", $desc);
$description_row = ceil(strlen($desc) / 40);
$desc_rows = explode("\n", $desc);
foreach ($desc_rows as $key => $value) {
    $y_desc = $pdf->getY();
    $available_row = (270 - $y_desc) / 4;
    $print_row = ceil(strlen($value) / 40);
    if ($available_row > $print_row) {
        $pdf->MultiCell(95, 4, $value, 0, 'J');
    } else {
        $max_desc = $available_row * 90;
        $get_value = strlen($value);
        if ($get_value > $max_desc) {
            $desc_cut = substr($value, 0, $max_desc);
            $pdf->MultiCell(95, 4, $desc_cut, 0, 'J');
            $print_y = ceil(strlen($desc_cut) / 40);
            break;
        }
    }
    $y_desc = $y_desc + $print_y;
}

/* detail */
$pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
$pdf->SetFont('Helvetica', '', 8);
$pdf->SetXY(116.5, $y_content + 12);

$next_detail = $pdf->getY();

$details = array('opentimes' => 'Inspect', 'auction_date' => 'Auction', 'auction_time' => 'Details');
foreach ($details as $key => $values) {
    if (empty($properties[$key]))
        continue;
    switch ($key) {
        case 'auction_date' :
            if ($properties[$key] != '0000-00-00')
                $pdf->print_detail($values, $show_auction_date, $next_detail);
            break;
        case 'auction_time' :
            $pdf->print_detail($values, $auction_time, $next_detail);
            break;
        case 'opentimes':
            if (!empty($opentimes)) {
                $i = 0;
                $detail = '';
                foreach ($opentimes as $key => $opentime) {
                    if (strtotime($opentime['date']) >= time() && $key < 2) {

                        $detail.=date('l, jS F Y', strtotime($opentime['date'])) . " " . date('g:i', strtotime($opentime['start_time'])) . " - " . date('g:i', strtotime($opentime['end_time']));
                        $detail.="\n";
                        $i++;
                    }
                }
                $pdf->print_detail($values, $detail, $next_detail);
                if ($i > 0)
                    $next_detail = $next_detail + (($i - 1) * 4);
                $pdf->Ln();
            }
            break;
        default:
            $pdf->print_detail($values, $properties[$key], $next_detail);
            break;
    }
    $next_detail = $next_detail + 4;
}
$y = $pdf->getY();
$pdf->Ln();
if ($auction_date || $opentimes) {
    $pdf->SetLineWidth(.1);
    $pdf->SetDrawColor($block_color[0], $block_color[1], $block_color[2]);
    $pdf->Line(117, $y + 2, 195, $y + 2);
}

/* detail 2 */
$next_detail = $pdf->getY();
$details = array('tax_rate' => 'Council Rates', 'water_rate' => 'Water Rates', 'condo_strata_fee' => 'Strata Rates');
foreach ($details as $key => $values) {
    if (empty($properties[$key]))
        continue;
    switch ($key) {
        case 'tax_rate': case 'water_rate': case 'condo_strata_fee':
            $key_period = $properties[$key . '_period'];
            switch ($key_period) {
                case 'Per Week': $period = '/weeky (approx)';
                    break;
                case 'Per Month': $period = '/month (approx)';
                    break;
                case 'Per Quarter': $period = ' p/q';
                    break;
                case 'Per Year': $period = '/year (approx)';
                    break;
                default :$period = $key_period;
                    break;
            }
            $pdf->print_detail($values, '$' . number_format($properties[$key], 2) . $period, $next_detail);
            break;
        default:
            $pdf->print_detail($values, $properties[$key], $next_detail);
            break;
    }

    $next_detail = $next_detail + 4;
}

if ($brochure['logo_tba']) {
    $pdf->Image($brochure['logo_tba'], 117, 254.5, 0, 12, '', '', array('max_height' => 12));
    $pdf->SetLineWidth(.1);
    $pdf->SetDrawColor($block_color[0], $block_color[1], $block_color[2]);
    $pdf->Line(117, 267.5, 195, 267.5);
}

/* USER */
$next_detail = $pdf->getY();
$details = array('user' => 'Contact');
foreach ($details as $key => $values) {
    if (empty($properties[$key]))
        continue;
    switch ($key) {
        case 'user';
            $i = 0;
            $pdf->SetXY(116, 267);
            $pdf->SetFont('Helvetica', 'B', 12);
            $pdf->MultiCell(50, 10, $properties['user'][0]['name']);

            $pdf->SetXY(116, 273);
            $pdf->SetFont('Helvetica', '', 9);
            $pdf->MultiCell(50, 10, $properties['user'][0]['mobile']);

            $pdf->SetXY(156, 267);
            $pdf->SetFont('Helvetica', 'B', 12);
            $pdf->MultiCell(50, 10, $properties['user'][1]['name']);

            $pdf->SetXY(156, 273);
            $pdf->SetFont('Helvetica', '', 9);
            $pdf->MultiCell(50, 10, $properties['user'][1]['mobile']);
            if ($i > 0)
                $next_detail = $next_detail + ($i * 4);
            break;
    }
    $next_detail = $next_detail + 4;
}

if ($block_color[0] != '255' && $block_color[1] != '255' && $block_color[2] != '255') {
    $pdf->SetTextColor(255, 255, 255);
} else {
    $pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
}

$pdf->SetFillColor($block_color[0], $block_color[1], $block_color[2]);
$pdf->Rect(0, 284, 210, 14, 'F');
$pdf->SetFont('Arial', '', 11);
$pdf->SetXY(10, 286);
$y = $pdf->getY();
$pdf->Cell(100, 9, $url, 0, 0, 'L', false, $url);

/* florplan */
if (!empty($floorplans[0])) {
    $pdf->SetXY(20, $y);
    $pdf->Floorplan($floorplans[0], 10, 20, 180, true);
    $pdf->Image($brochure['logo_tba'], 160, 5, 0, 14, '', '', array('max_height' => 14));

    /* address */
    if (!empty($street_address || $suburb || $state)) {
        $pdf->SetTextColor($font_color[0], $font_color[1], $font_color[2]);
        $pdf->SetFont('Helvetica', 'B', 12);
        $pdf->SetXY(20, $pdf->GetPaperHeight() - 30);
        $pdf->MultiCell(100, 2, $street_address, 0, 'L');
        $pdf->SetXY(90, $pdf->GetPaperHeight() - 30);
        $pdf->MultiCell(100, 2, $suburb . ", " . $state, 0, 'L');
        $y = $pdf->getY();
        $pdf->Ln();
        $pdf->SetLineWidth(.1);
        $pdf->SetDrawColor($block_color[0], $block_color[1], $block_color[2]);
        $pdf->Line(22, $y + 2, 130, $y + 2);
    }
    $pdf->Image($brochure['logo'], 140, $pdf->GetPaperHeight() - 40, 40, 0, '', '', array('max_width' => 40));
}
?>