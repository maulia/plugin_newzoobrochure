<div class="property_map" id="property_map">
	<div class="block_content">
		<div id="map_canvas">
			<div id="map_loading">Loading...</div>
		</div>
	</div>
	<div class="clearer"></div>
</div>
<div id="property_detail" style="display:none;"></div>
<?php
if (class_exists('zoomap'))
{
	$markers = array();
	foreach ($listings as $property)
	{
		if (!is_array($property))
			continue;
		
		extract($property);
		ob_start();		
		require ('info_window.php');
		$html = ob_get_contents();
		ob_end_clean();
		if (is_numeric($latitude) && is_numeric($longitude))
		{
			$markers[] = array(
				'x' => $latitude,
				'y' => $longitude,
				'html' => $html,
				);
		}
	}
	zoomap::getInstance()->load_map('map_canvas', 0, 0, 8, $markers);
} else {
	die('No Map Available!');
}
?>