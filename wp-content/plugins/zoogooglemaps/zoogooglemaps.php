<?php
/*
Plugin Name: Zoo Google Maps
Plugin URI: http://www.agentpoint.com.au/
Description: Google Maps for Real Estate Sites
Version: 0.9
Author: Agentpoint
*/

define('MAP_ROAD', 1);
define('MAP_SATELLITE', 2);
define('MAP_HYBRID', 3);
define('MAP_TERRAIN', 4);

register_activation_hook(__FILE__, 'zoogooglemaps_activate');

function zoogooglemaps_activate()
{
	$zoomap_map = get_page_by_path('zoomap-map');
	$zoomap_streetview = get_page_by_path('zoomap-streetview');
	
	if (empty($zoomap_map))
	{
		wp_insert_post(array(
			'post_title' => 'Property Map',
			'post_type' => 'page',
			'post_status' => 'publish',
			'post_name' => 'zoomap-map',
			'post_content' => 'This page is a placeholder.',
			));
	}
	
	if (empty($zoomap_streetview))
	{
		wp_insert_post(array(
			'post_title' => 'Street View',
			'post_type' => 'page',
			'post_status' => 'publish',
			'post_name' => 'zoomap-streetview',
			'post_content' => 'This page is a placeholder.',
			));
	}
	
	$tmp = get_option('zoogooglemaps');
	if (empty($tmp))
	{
		$tmp = array(
			'key' => '',
			);

		update_option('zoogooglemaps', $tmp);
	}

	flush_rewrite_rules();
}

if (!class_exists('zoomap'))
{
	class zoomap
	{
		private $options;
		private $maps = 0;
		private $map_markers = array();
		private $init = array();
		private $code = array();
		private $html = array();
		private $vars = array();
		
		private $debug = false;
		private $clustered = false;
		
		public static function getInstance()
		{
			static $instance = null;
			if (null === $instance)
			{
				$instance = new zoomap();
			}

			return $instance;
		}
		
		function __construct()
		{
			$this->options = get_option('zoogooglemaps');
			
			add_action('template_redirect', array($this, 'template_redirect'));
			add_action('generate_rewrite_rules', array($this,'rewrite_rules'));
			add_action('wp_enqueue_scripts', array($this, 'load_scripts'));
			add_action('wp_footer', array($this, 'produce_js'));
			
			add_shortcode('zoomap', array($this, 'shortcode_map'));
		}
		
		function rewrite_rules($wp_rewrite)
		{
			$zoomap_map = get_page_by_path('zoomap-map');
			$zoomap_streetview = get_page_by_path('zoomap-streetview');
			
			$new_rules = array(
				'^([0-9]+)/map$' => 'index.php?page_id='.$zoomap_map->ID.'&property_id='.$wp_rewrite->preg_index(1),
				'^([0-9]+)/streetview$' => 'index.php?page_id='.$zoomap_streetview->ID.'&property_id='.$wp_rewrite->preg_index(1),
				);
			$wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
		}
		
		function load_scripts()
		{
			wp_enqueue_script('zoogooglemaps', plugins_url('zoogooglemaps/zoogooglemaps.js', 'zoogooglemaps'));
		}
		
		function template_redirect()
		{
			if (is_page('zoomap-map') || is_page('zoomap-streetview'))
			{
				add_filter('template_include', array( &$this, 'template_callback' ), 100);
			}
		}
		
		function template_callback()
		{
			global $wp_query;
			$wp_query->is_single = false;
			
			$headless = locate_template('zoogooglemap.php');
			
			if (empty($headless))
				$headless = dirname(__FILE__).'/headless.php';
			
			return $headless;
		}
		
		public function shortcode_map($atts)
		{
			extract(shortcode_atts( array(
				'lat' => -25,
				'lng' => 135,
				'z' => 8,
				'marker' => false,
				'maptype' => MAP_ROAD,
				'float' => false,
				'width' => 320,
				'height' => 240,
				'cluster' => false,
			), $atts));
			
			if (is_numeric($marker))
				$marker = $marker > 0;
			
			$attributes = array();
			$attributes['mapTypeId'] = $maptype;
			$attributes['cluster'] = $cluster;
			
			$this->load_map('zoomap_'.($this->maps + 1), $lat, $lng, $z, $marker, $attributes);
			
			if ($float)
			{
				if ($float == 'right')
				{
					$float = 'float:right;';
				} else {
					$float = 'float:left;';
				}
			} else {
				$float = '';
			}
			
			return "<div id=\"zoomap_{$this->maps}\" class=\"zoogooglemap\" style=\"width:{$width}px;height:{$height}px;$float\">Preparing Map...</div>\n";
		}
		
		public function load_map($id, $x = 0, $y = 0, $z = 8, $markers = false, $attributes = array())
		{
			$this->maps++;
			$this->map_markers[$this->maps] = 0;
			$calculate_bounds = false;
			
			if (!is_array($attributes))
				$attributes = array('mapTypeId' => $attributes);
			if (empty($attributes['mapTypeId']))
				$attributes['mapTypeId'] = MAP_ROAD;
			if (is_int($attributes['mapTypeId']))
				$attributes['mapTypeId'] = $this->getMapType($attributes['mapTypeId']);
			if (empty($attributes['cluster']))
				$attributes['cluster'] = false;
			else
			{
				if (is_string($attributes['cluster']) && substr($attributes['cluster'], 0, 1) == '{')
					$attributes['cluster'] = json_decode($attributes['cluster'], true);
				elseif ($attributes['cluster'] === true)
					$attributes['cluster'] = array();

				if (!isset($attributes['cluster']['image_path']))
					$attributes['cluster']['image_path'] = plugins_url('zoogooglemaps/cimg/m');
				if (!isset($attributes['cluster']['max_zoom']))
					$attributes['cluster']['max_zoom'] = 'null';
				$this->clustered = true;
			}
			
			if ($x == 0)
			{
				$x = -25;
				$y = 135;
				$z = 4;
				$calculate_bounds = true;
			}
			
			$this->vars[] = "map{$this->maps}";
			$this->vars[] = "map{$this->maps}_markers=[]";
			$this->vars[] = "map{$this->maps}_info=[]";
			if ($attributes['cluster'])
				$this->vars[] = "map{$this->maps}_cluster";
			
			$code = "var map{$this->maps}_opts = {center: new google.maps.LatLng($x,$y), zoom: $z";
			foreach ($attributes as $key => $value)
			{
				if (in_array($key, array('loadCallback')))
					continue;
				if (is_bool($value))
					$code .= ", $key: ".($value ? 'true' : 'false');
				else
					$code .= ", $key: $value";
			}
			$code .= '};';
			
			$code .= "map{$this->maps} = new google.maps.Map(document.getElementById('$id'), map{$this->maps}_opts);";
			$code .= "gmap_resize_list.push(map{$this->maps});";
			
			if (!empty($markers))
			{
				if (!is_array($markers))
					$markers = array($markers);
				
				$coordinate_list = array();

				if (!empty($attributes['autoCloseWindow']))
					$code .= "var map{$this->maps}_current_window = false;";
				
				foreach ($markers as $marker)
				{
					$this->map_markers[$this->maps]++;
					$marker_id = $this->map_markers[$this->maps];
					
					$x = (isset($marker['x']) ? $marker['x'] : $x);
					$y = (isset($marker['y']) ? $marker['y'] : $y);
					$img = isset($marker['img']) ? $marker['img'] : (is_string($marker) ? $marker : false);
					$cluster = isset($marker['cluster']) ? $attributes['cluster'] && $marker['cluster'] : $attributes['cluster'];
					
					$coordinate_list[] = "[$x,$y]";
					
					$code .= "var map{$this->maps}_mkr_$marker_id = new google.maps.Marker({position: new google.maps.LatLng($x,$y)".($cluster ? '' : ", map: map{$this->maps}").(is_string($img) ? ", icon: '$img'" : '')."});";
					
					if (!empty($marker['html']))
					{
						$info_id = count($this->info);
						$code .= "var map_info$info_id = new google.maps.InfoWindow({content: document.getElementById('map_infobox_$info_id')});";
						$code .= "google.maps.event.addListener(map{$this->maps}_mkr_$marker_id, 'click', function() {map_info$info_id.open(map{$this->maps},map{$this->maps}_mkr_$marker_id);";
						if (!empty($attributes['autoCloseWindow']))
							$code .= "if (map{$this->maps}_current_window != false) { map{$this->maps}_current_window.close(); } map{$this->maps}_current_window = map_info$info_id;";
						$code .= "});";

						$this->info[$info_id] = $marker['html'];
						$code .= "map{$this->maps}_info.push(map_info$info_id);";
					}
					
					if (!empty($marker['onclick']))
					{
						$code .= "google.maps.event.addListener(map{$this->maps}_mkr_$marker_id, 'click', function() {var current_map = map{$this->maps}; var current_marker = map{$this->maps}_mkr_$marker_id); {$marker['onclick']}});";
					}

					$code .= "map{$this->maps}_markers.push(map{$this->maps}_mkr_$marker_id);";
				}
				
				if ($calculate_bounds)
				{
					$code .= "map{$this->maps}.zoogooglemaps_bounds = [".implode(',', $coordinate_list)."];";
				}
				
				if ($attributes['cluster'])
				{
					$code .= "map{$this->maps}_cluster = new MarkerClusterer(map{$this->maps}, map{$this->maps}_markers, {imagePath: '{$attributes['cluster']['image_path']}', maxZoom: {$attributes['cluster']['max_zoom']}});";
				}
			}
			
			if (!empty($attributes['loadCallback']))
				$code .= "{$attributes['loadCallback']}(map{$this->maps},".json_encode(array('x' => $x, 'y' => $y, 'z' => $z)).");";
			
			$this->code[] = $code;
			
			return "map{$this->maps}";
		}
		
		public function load_streetview($id, $x, $y)
		{
			$this->maps++;
			$this->map_markers[$this->maps] = 0;
			
			$this->vars[] = "streetview{$this->maps}";
			
			$code .= "var streetview{$this->maps}_location = new google.maps.LatLng($x,$y);";
			$code .= "var streetview{$this->maps} = new google.maps.StreetViewPanorama(document.getElementById('$id'), {visible: true, position: streetview{$this->maps}_location});";
			$code .= "gmap_resize_list.push(streetview{$this->maps});";
			$code .= "zoogooglemaps_getpov(streetview{$this->maps}, streetview{$this->maps}_location);";
			
			$this->code[] = $code;
			
			return "streetview{$this->maps}";
		}
		
		function produce_js()
		{
			if ($this->maps == 0)
				return;
			
			if ($this->clustered)
				echo '<script src="'.plugins_url('zoogooglemaps/markerclusterer'.($this->debug ? '' : '_compiled').'.js').'"></script>';
			
			echo '<script type="text/javascript">';
			
			echo "var ".implode(',', $this->vars).";";
			echo "var gmap_resize_list = [];";
			
			if (empty($this->options['key']))
				echo 'zoogooglemaps_init("AIzaSyCeEBmbIGnRwo6MqJVQyhcAq7zaLq9r_PM");';
			else
				echo 'zoogooglemaps_init("'.$this->options['key'].'");';
			
			echo 'function zoogooglemaps() {';
			
			foreach ($this->code as $code)
				echo $code;

			echo 'zoogooglemaps_resize();';
			echo '}</script>';
			
			foreach ($this->html as $html)
				echo $html;

			if (!empty($this->info))
			{
				echo '<div id="hide_info_windows" style="display:none">';
				foreach ($this->info as $id => $info)
				{
					echo '<div id="map_infobox_'.$id.'">'.$info.'</div>';
				}
				echo "</div>";
			}
		}
		
		private function getMapType($maptype)
		{
			$out = '';
			switch ($maptype)
			{
				case MAP_SATELLITE:
					$out = 'SATELLITE';
					break;
				case MAP_HYBRID:
					$out = 'HYBRID';
					break;
				case MAP_TERRAIN:
					$out = 'TERRAIN';
					break;
				case MAP_ROAD:
				default:
					$out = 'ROAD';
					break;
			}
			
			return 'google.maps.MapTypeId.'.$out;
		}
		
		function addCode($code, $html = false)
		{
			if ($html)
				$this->html[] = $code;
			else
				$this->code[] = $code;
		}
	}
	
	$zoomap = zoomap::getInstance();
}
