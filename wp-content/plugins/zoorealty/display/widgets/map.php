<?php if(!$realty->is_property_page || empty($realty->property['latitude']) || empty($realty->property['longitude'])) return; ?>
<?php echo $before_title; ?>Location Map<?php echo $after_title; ?>
<div class="map-wrapper">
    <div id="map_canvas" style="width:<?php echo $settings['width']; ?>px;height:<?php echo $settings['height']; ?>px;"></div>
 <?php
if (class_exists('zoomap'))
{
zoomap::getInstance()->load_map('map_canvas', $realty->property['latitude'], $realty->property['longitude'], 13, true);
} else {
die('No Map Available!');
}
?>
</div>