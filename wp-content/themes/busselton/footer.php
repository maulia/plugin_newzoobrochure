<?php global $options; foreach ($options as $value) { if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); } } ?>

	<div class="clear"></div>
</div><!-- end #container -->

<?php wp_footer(); ?>

<div id="footer">
    <div class="footerwrap">
        <p class="left"><a href="<?php echo get_option('home'); ?>/terms-of-use" title="Terms of Use">Terms of Use</a> | <a href="<?php echo get_option('home'); ?>/privacy-policy" title="Privacy Policy">Privacy Policy</a></p>
        <p class="right">&copy; <?php bloginfo('name'); echo ' '.date('Y'); ?>. Powered by <a href="http://agentpoint.com.au" target="_blank">Agentpoint</a></p> 
        <div class="clear"></div>
	</div>
</div>
<pre><?php global $wp_rewrite; var_dump($wp_rewrite); ?></pre>
</div><!-- end #wrapper -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-174564-119']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>
