<?php
//metadata: access_level=8
/* All files in display/pages will be displayed as options that can be added or deleted. If added, a corresponding page is created in WordPress
 * with the files as its template. If removed, the page will be deleted from the blog.
 * The plugin stores each post_id created by WP mapping them with their file.
 * The plugin contains functions to extract the permalink based on the template filename.
 * */
require_once($realty->path('controller/install.php'));
global $helper, $wpdb;

function page_wp_insert_post($post, $unique=false){
	global $helper;
	if(empty($post['post_type'])) $post['post_type'] = 'page';
	$postID = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => $post['post_type'], 'post_title'=>$helper->humanize($post['post_title']), 'post_content'=>$post['post_content'], 'post_parent'=>$post['post_parent']));

	update_post_meta($postID, '_permanent_name',  $helper->underscore($post['post_title']));
	if(!empty($post['post_template']))update_post_meta($postID, '_wp_page_template', $post['post_template']);
	if(is_array($post['custom'])){
		foreach ($post['custom'] as $meta_key=>$meta_value)update_post_meta($postID, $meta_key, $meta_value);
	}
	return $postID;			
}
	
if(isset($_POST['submit'])){
	$pages=$_POST['page'];
	if(!empty($pages)){
		foreach($pages as $key=>$page){
			if(!empty($page['page_title'])){
				$post_title=strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "_",str_replace("-", "_", $page['page_title']))));
				$post_title=str_replace("__","_",$post_title);
			}
			else { $post_title=$page['post_title']; }
			$post_parent=$page['parent_page'];		
			if(!empty($post_title)){ 
				foreach($default_pages as $page_title=>$post_content){
					if($page_title==$page['post_title'])page_wp_insert_post(array('post_title'=>$post_title, 'post_content'=>$post_content,'post_parent'=>$post_parent), true);		
					//echo "$post_title $post_content $post_parent<br>";
				}					
			}
		}
		$return="Pages created.";
	}
	else $return="No pages created.";
}

if(isset($_POST['submit_type'])){
	$pages=$_POST['page'];
	if(!empty($pages)){
		foreach($pages as $key=>$page){
			$property_type=str_replace(" ","_",str_replace(",","/",str_replace(", ","/",$page['property_type'])));
			$post_title=strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "_",str_replace("-", "_", $page['page_title']))));
			$post_title=str_replace("__","_",$post_title);
			$post_parent=$page['parent_page'];		
			$type=$page['list'];		
			if(!empty($property_type) && !empty($post_title)){
				$list=$property_type;
				if(!empty($type))$list.="/".$type;
				$post_content="[realty_plugin list=$list template=search_results]";
				page_wp_insert_post(array('post_title'=>$post_title, 'post_content'=>$post_content,'post_parent'=>$post_parent), true);
			}
		}
		$return="Pages created.";
	}
	else $return="No pages created.";
}

if(isset($_POST['submit_suburb'])){
	$pages=$_POST['page'];
	if(!empty($pages)){
		foreach($pages as $key=>$page){
			$suburb=str_replace(" ","_",str_replace(",",":",str_replace(", ",":",$page['suburb'])));
			$post_title=strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "_",str_replace("-", "_", $page['page_title']))));
			$post_title=str_replace("__","_",$post_title);
			$post_parent=$page['parent_page'];		
			$type=$page['list'];		
			if(!empty($suburb) && !empty($post_title)){
				$list=$suburb;
				if(!empty($type))$list.=":".$type;
				$post_content="[realty_plugin list=$list template=search_results]";
				page_wp_insert_post(array('post_title'=>$post_title, 'post_content'=>$post_content,'post_parent'=>$post_parent), true);
			}
		}
		$return="Pages created.";
	}
	else $return="No pages created.";
}

if(isset($_POST['submit_update'])){
	$pages=$_POST['page_ids'];
	if(!empty($pages)){
		foreach($pages as $key=>$post_id){
			wp_delete_post($post_id);
			//echo $post_id.'<br>';
		}
		$return="Pages deleted.";
	}
	else $return="No pages deleted.";
}

if(!empty($return)){
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
}

$parent_pages=$wpdb->get_results("select ID, post_title, post_parent from $wpdb->posts where post_status='publish' and post_type='page' and post_parent='0' order by post_title", ARRAY_A);
$pages=$wpdb->get_results("select ID, post_title, post_parent, post_content from $wpdb->posts where post_status='publish' and post_type='page' and post_parent='0' order by post_title", ARRAY_A); 
$existing_pages=array();
$i=0;
foreach( $pages as $page ){	
	$existing_pages[$i]=$page;
	$j=$i;$existing_pages[$j]['display']=1;if(strpos($page['post_content'],'[realty_plugin')===false)$existing_pages[$j]['display']=0;	
	$i++;	
	$children_pages=$wpdb->get_results("select ID, post_title, post_parent,post_content from $wpdb->posts where post_status='publish' and post_type='page' and post_parent=".intval($page['ID'])." order by post_title", ARRAY_A);
	if(!empty($children_pages)){
		foreach( $children_pages as $children_page ){
			if(strpos($children_page['post_content'],'[realty_plugin')!==false)$existing_pages[$j]['display']=1;
			$existing_pages[$i]=$children_page;
			$existing_pages[$i]['display']=1;if(strpos($children_page['post_content'],'[realty_plugin')===false)$existing_pages[$i]['display']=0;	
			$i++;
			$children_pages_2=$wpdb->get_results("select ID, post_title, post_parent,post_content from $wpdb->posts where post_status='publish' and post_type='page' and post_parent=".intval($children_page['ID'])." order by post_title", ARRAY_A);
			if(!empty($children_pages_2)){
				foreach( $children_pages_2 as $children_page_2 ){
					if(strpos($children_page_2['post_content'],'[realty_plugin')!==false)$existing_pages[$j]['display']=1;
					$existing_pages[$i]=$children_page_2;
					$existing_pages[$i]['display']=1;if(strpos($children_page_2['post_content'],'[realty_plugin')===false)$existing_pages[$i]['display']=0;	
					$i++;
				}
			}
		}
	}
}

$office_id= esc_sql(implode(', ', $realty->settings['general_settings']['office_id']));
$property_types=$wpdb->get_results("select distinct property_type from properties where office_id in ($office_id) order by property_type", ARRAY_A);
$suburbs=$wpdb->get_results("select distinct suburb from properties where office_id in ($office_id) order by suburb", ARRAY_A);
?>
<div class="wrap">
	<select id="defaultpage" style="display:none;">
		<?php foreach($default_pages as $post_title=>$post_content){ ?>
		<option value="<?php echo $post_title; ?>"><?php echo $helper->humanize($post_title); ?></option>
		<?php } ?>
	</select>
	<select id="parent_page" style="display:none;">
		<option value="">Main parent</option>
		<?php foreach($parent_pages as $parent_page){ ?>
		<option value="<?php echo $parent_page['ID']; ?>"><?php echo $parent_page['post_title']; ?></option>
		<?php } ?>
	</select>
	<select id="type" style="display:none;">		
		<?php /*<option value="">All Listings</option>*/ ?>
		<option value="sale">For Sale</option>
		<option value="lease">For Lease</option>		
	</select>
	<select id="property_type" style="display:none;">
		<?php foreach($property_types as $item){  ?>
		<option value="<?php echo $item['property_type']; ?>"><?php echo $item['property_type']; ?></option>
		<?php } ?>
	</select>
	<select id="suburb" style="display:none;">
		<?php foreach($suburbs as $item){  ?>
		<option value="<?php echo $item['suburb']; ?>"><?php echo $item['suburb']; ?></option>
		<?php } ?>
	</select>
	
	<h2>Create Default Pages</h2>
	<p><a href="javascript:;" onclick="addPages();">Add New</a></p>	
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col" width="20%">Page</th>
			<th scope="col" width="40%">Title ( Default page title will be the same as chosen page )</th>			
			<th scope="col" width="30%">Parent</th>			
			<th scope="col" width="10%">Remove</th>			
		</tr>
		</thead>
	</table>	
	<input type="hidden" value="0" id="theValue" />
	<form name="" method="post" action="">
	<div id="myDiv"></div>
	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</form>
	
	<h2>Create Property Type Pages</h2>
	<p>
		<a href="javascript:;" onclick="addType('single');">Add Single Property Type Pages</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="javascript:;" onclick="addType('multiple');">Add Multiple Property Type Pages</a>&nbsp;(separate by commas)
	</p>	
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col" width="30%">Property Type</th>
			<th scope="col" width="10%">Listing Type</th>
			<th scope="col" width="30%">Title</th>			
			<th scope="col" width="20%">Parent</th>				
			<th scope="col" width="10%">Remove</th>			
		</tr>
		</thead>
	</table>	
	<input type="hidden" value="0" id="theValueType" />
	<form name="" method="post" action="">
	<div id="myDivType"></div>
	<p><i>All fields are mandatory.</i></p>
	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit_type" /></p> 
	</form>
	
	<h2>Create Suburb Pages</h2>
	<p>
		<a href="javascript:;" onclick="addSuburb('single');">Add Single Suburb Pages</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="javascript:;" onclick="addSuburb('multiple');">Add Multiple Suburb Pages</a>&nbsp;(separate by commas)
	</p>		
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col" width="30%">Suburb</th>
			<th scope="col" width="10%">Listing Type</th>
			<th scope="col" width="30%">Title</th>			
			<th scope="col" width="20%">Parent</th>			
			<th scope="col" width="10%">Remove</th>			
		</tr>
		</thead>
	</table>	
	<input type="hidden" value="0" id="theValueSuburb" />
	<form name="" method="post" action="">
	<div id="myDivSuburb"></div>
	<p><i>All fields are mandatory.</i></p>
	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit_suburb" /></p> 
	</form>
	
	<h2>Existing Pages</h2>
	<p>Check the pages to delete</p>
	<form name="" method="post" action="">
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Delete'); ?>" name="submit_update" /></p> 
		<table class="widefat">
			<thead>
			<tr>
				<th scope="col"><input type="checkbox" value="1" id="all_ids" onClick="check_all();"></th>
				<th scope="col">Title</th>			
				<th scope="col">Permalink</th>			
			</tr>
			</thead>
			<?php $i=0;foreach($existing_pages as $page): if(!is_array($page))continue; $permalink=get_permalink($page['ID']);
			if(!empty($page['display'])){ ?> 
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
				<td>
				<?php if(strpos($page['post_content'],'[realty_plugin')!==false){ ?>
				<span style="margin-left:4px">&nbsp;</span><input type="checkbox" name="page_ids[]" value="<?php echo $page['ID']; ?>">
				<?php } ?>
				</td> 
				<td><?php if($page['post_parent']!='0')echo '<span style="margin-left:10px">-&nbsp;&nbsp;</span>'; ?><a href="<?php echo $permalink; ?>" target="_blank"><?php echo $page['post_title']; ?></a></td>	
				<td><a href="<?php echo $permalink; ?>" target="_blank"><?php echo $permalink; ?></a></td>
			</tr> 
			<?php $i++; } endforeach; ?>
		</table> 	
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Delete'); ?>" name="submit_update" /></p> 
	</form>
	
	<script type="text/javascript">
	var default_page = document.getElementById('defaultpage').innerHTML;
	var parent_page = document.getElementById('parent_page').innerHTML;
	var type = document.getElementById('type').innerHTML;
	var property_type = document.getElementById('property_type').innerHTML;
	var suburb = document.getElementById('suburb').innerHTML;
	
	function addPages() {
	  var ni = document.getElementById('myDiv');
	  var numi = document.getElementById('theValue');
	  var num = (document.getElementById('theValue').value -1)+ 2;
	  numi.value = num;
	  var newdiv = document.createElement('div');
	  var divIdName = 'my'+num+'Div';	  
	  var default_page_name = 'page['+num+'][post_title]';
	  var page_title_name = 'page['+num+'][page_title]';
	  var parent_page_name = 'page['+num+'][parent_page]';
	  newdiv.setAttribute('id',divIdName);
	  newdiv.innerHTML = 	'<table class="widefat">'+
							'<tr>'+
							'<td width="20%"><select name="'+default_page_name+'">'+default_page+'</select></td>'+
							'<td width="40%"><input type="text" size="30" name="'+page_title_name+'"></td>'+
							'<td width="30%"><select name="'+parent_page_name+'">'+parent_page+'</select></td>'+
							'<td><a href=\'#\' onclick=\'removePages("'+divIdName+'")\'>Remove</a></td>'+
							'</tr>'+
							'</table>';
	  ni.appendChild(newdiv);
	}
	
	function removePages(divNum) {
	  var d = document.getElementById('myDiv');
	  var olddiv = document.getElementById(divNum);
	  d.removeChild(olddiv);
	}
	
	function addType(flag) {
	  var ni = document.getElementById('myDivType');
	  var numi = document.getElementById('theValueType');
	  var num = (document.getElementById('theValueType').value -1)+ 2;
	  numi.value = num;
	  var newdiv = document.createElement('div');
	  var divIdName = 'my'+num+'DivType';
	  var property_type_name = 'page['+num+'][property_type]';
	  var page_title_name = 'page['+num+'][page_title]';
	  var parent_page_name = 'page['+num+'][parent_page]';
	  var type_name = 'page['+num+'][list]';
	  newdiv.setAttribute('id',divIdName);
	  if(flag=='multiple'){
		  newdiv.innerHTML = 	'<table class="widefat">'+
								'<tr>'+
								'<td width="30%"><input type="text" size="35" name="'+property_type_name+'"></td>'+
								'<td width="10%"><select name="'+type_name+'">'+type+'</select></td>'+
								'<td width="30%"><input type="text" size="35" name="'+page_title_name+'"></td>'+
								'<td width="20%"><select name="'+parent_page_name+'">'+parent_page+'</select></td>'+
								'<td><a href=\'#\' onclick=\'removeType("'+divIdName+'")\'>Remove</a></td>'+
								'</tr>'+
								'</table>';
	  }
	  else{
		newdiv.innerHTML = 	'<table class="widefat">'+
								'<tr>'+
								'<td width="30%"><select name="'+property_type_name+'">'+property_type+'</select></td>'+
								'<td width="10%"><select name="'+type_name+'">'+type+'</select></td>'+
								'<td width="30%"><input type="text" size="35" name="'+page_title_name+'"></td>'+
								'<td width="20%"><select name="'+parent_page_name+'">'+parent_page+'</select></td>'+
								'<td><a href=\'#\' onclick=\'removeType("'+divIdName+'")\'>Remove</a></td>'+
								'</tr>'+
								'</table>';	  
	  }
	  ni.appendChild(newdiv);
	}
	
	function removeType(divNum) {
	  var d = document.getElementById('myDivType');
	  var olddiv = document.getElementById(divNum);
	  d.removeChild(olddiv);
	}
	
	function addSuburb(flag) {
	  var ni = document.getElementById('myDivSuburb');
	  var numi = document.getElementById('theValueSuburb');
	  var num = (document.getElementById('theValueSuburb').value -1)+ 2;
	  numi.value = num;
	  var newdiv = document.createElement('div');
	  var divIdName = 'my'+num+'DivSuburb';
	  var suburb_name = 'page['+num+'][suburb]';
	  var page_title_name = 'page['+num+'][page_title]';
	  var parent_page_name = 'page['+num+'][parent_page]';
	  var type_name = 'page['+num+'][list]';
	  newdiv.setAttribute('id',divIdName);
	  if(flag=='multiple'){
		  newdiv.innerHTML = 	'<table class="widefat">'+
								'<tr>'+
								'<td width="30%"><input type="text" size="35" name="'+suburb_name+'"></td>'+
								'<td width="10%"><select name="'+type_name+'">'+type+'</select></td>'+
								'<td width="30%"><input type="text" size="35" name="'+page_title_name+'"></td>'+
								'<td width="20%"><select name="'+parent_page_name+'">'+parent_page+'</select></td>'+
								'<td><a href=\'#\' onclick=\'removeSuburb("'+divIdName+'")\'>Remove</a></td>'+
								'</tr>'+
								'</table>';
	  }
	  else{
		newdiv.innerHTML = 	'<table class="widefat">'+
								'<tr>'+
								'<td width="30%"><select name="'+suburb_name+'">'+suburb+'</select></td>'+
								'<td width="10%"><select name="'+type_name+'">'+type+'</select></td>'+
								'<td width="30%"><input type="text" size="35" name="'+page_title_name+'"></td>'+
								'<td width="20%"><select name="'+parent_page_name+'">'+parent_page+'</select></td>'+
								'<td><a href=\'#\' onclick=\'removeSuburb("'+divIdName+'")\'>Remove</a></td>'+
								'</tr>'+
								'</table>';
	  }
	  ni.appendChild(newdiv);
	}
	
	function removeSuburb(divNum) {
	  var d = document.getElementById('myDivSuburb');
	  var olddiv = document.getElementById(divNum);
	  d.removeChild(olddiv);
	}
	
	function check_all(){				
			var max = document.getElementsByName('page_ids[]').length;		
			for(var idx = 0; idx < max; idx++){
				if(eval("document.getElementById('all_ids').checked") == true)document.getElementsByName('page_ids[]')[idx].checked = true;
				else document.getElementsByName('page_ids[]')[idx].checked = false;
			}
		} 
	</script>
<table class="widefat">
<thead><tr>
	<th>Avaialable Page</th>
	<th>Short Codes</th>
</tr></thead>
<tbody>
<tr>
	<td>Search Results</td>
	<td>[realty_plugin template=search_results]</td>
</tr>
<tr>
	<td>Sales</td>
	<td>[realty_plugin list=sale template=search_results]</td>
</tr>
<tr>
	<td>Residential Sales</td>
	<td>[realty_plugin list=ResidentialSale template=search_results]</td>
</tr>
<tr>
	<td>Commercial Sales</td>
	<td>[realty_plugin list=commercial_sale template=search_results]</td>
</tr>
<tr>
	<td>Project Sale</td>
	<td>[realty_plugin list=ProjectSale template=search_results]</td>
</tr>
<tr>
	<td>Business Sale</td>
	<td>[realty_plugin list=BusinessSale template=search_results]</td>
</tr>
<tr>
	<td>Rural</td>
	<td>[realty_plugin list=rural template=search_results]</td>
</tr>
<tr>
	<td>Waterfront</td>
	<td>[realty_plugin list=waterfront template=search_results]</td>
</tr>
<tr>
	<td>Land</td>
	<td>[realty_plugin list=land template=search_results]</td>
</tr>
<tr>
	<td>House For Sale</td>
	<td>[realty_plugin list=house/sale template=search_results]</td>
</tr>
<tr>
	<td>Apartment For Sale</td>
	<td>[realty_plugin list=apartment/sale template=search_results]</td>
</tr>
<tr>
	<td>Lease</td>
	<td>[realty_plugin list=lease template=search_results]</td>
</tr>
<tr>
	<td>Residential Lease</td>
	<td>[realty_plugin list=ResidentialLease template=search_results]</td>
</tr>
<tr>
	<td>Commercial Lease</td>
	<td>[realty_plugin list=commercial_lease template=search_results]</td>
</tr>
<tr>
	<td>Holiday</td>
	<td>[realty_plugin list=HolidayLease template=search_results]</td>
</tr>
<tr>
	<td>House For Lease</td>
	<td>[realty_plugin list=house/lease template=search_results]</td>
</tr>
<tr>
	<td>Apartment For Lease</td>
	<td>[realty_plugin list=apartment/lease template=search_results]</td>
</tr>
<tr>
	<td>opentimes]</td>
	<td>[realty_plugin template=opentimes]</td>
</tr>
<tr>
	<td>Opentimes Sales</td>
	<td>[realty_plugin list=sale template=opentimes]</td>
</tr>
<tr>
	<td>Opentimes Lease</td>
	<td>[realty_plugin list=lease template=opentimes]</td>
</tr>
<tr>
	<td>Sold</td>
	<td>[realty_plugin list=sale template=sold]</td>
</tr>
<tr>
	<td>Leased</td>
	<td>[realty_plugin list=lease template=sold]</td>
</tr>
<tr>
	<td>Forthcoming Auction</td>
	<td>[realty_plugin list=latest_auction template=latest_auction]</td>
</tr>
<tr>
	<td>Team</td>
	<td>[realty_plugin template=team]</td>
</tr>
<tr>
	<td>Executive Team</td>
	<td>[realty_plugin group=Executive template=team]</td>
</tr>
<tr>
	<td>Sales Agent Team</td>
	<td>[realty_plugin group=Sales Agent template=team]</td>
</tr>
<tr>
	<td>Property Manager Team</td>
	<td>[realty_plugin group=Property Manager template=team]</td>
</tr>
<tr>
	<td>Admin Team</td>
	<td>[realty_plugin group=Admin template=team]</td>
</tr>
<tr>
	<td>Other Team</td>
	<td>[realty_plugin group=Other template=team]</td>
</tr>
<tr>
	<td>Advanced Search Results</td>
	<td>[realty_plugin template=advanced_search]</td>
</tr>
</tbody>
</table>
</div>