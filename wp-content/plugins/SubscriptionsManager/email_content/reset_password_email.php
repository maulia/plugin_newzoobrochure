<table class="property" width="765px" border="0" cellspacing="0" cellpadding="0" style="margin: 0 0 18px; padding: 0;font-family: Georgia, Palatino, Palatino Linotype, Times, Times New Roman, serif;line-height:1.25;color: #58585a;">
<tr>
	<th colspan="2" style="padding: 0 0 15px;"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/backgrounds/bg_header_print.jpg" alt="<?php echo $blog_name; ?>" /></th>
</tr>
<tr>
	<td style="font-size:15px;">Your new password is:</td>
</tr>
<tr>
	<td style="font-size:15px;"><?php echo $new_password; ?></td>
</tr>
<tr>
	<td style="font-size:15px;">Please log in here: <a href="<?php echo $this->user_login_page . '?action=login'; ?>" style="color: #007ca5;" title="Log in to view your subscriptions"><?php echo $this->user_login_page . '?action=login'; ?></a>. Be sure to change your password when you log in.</td>
</tr>
</table>
