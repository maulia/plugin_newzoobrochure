<?php 
global $wpdb;
$settings = (empty($_POST['member']))?$realty->settings['team']: $_POST['member'];//Represents the excluded from members.
if(isset($_POST['submit'])):
	$realty->settings['team'] = $settings;
	update_option('realty_team', $settings);
	
	if(empty($_POST['team_not_display']))$_POST['team_not_display']=array();
	if(!update_option('realty_team_not_display',$_POST['team_not_display']))add_option('realty_team_not_display',$_POST['team_not_display']);
	
	if(!empty($_POST['team_ids'])){			
		foreach($_POST['team_ids'] as $position=>$id)://Update teams order
			$display=($_POST['member'][$id]['display']=='')?'0':$_POST['member'][$id]['display'];
			$helper->query("update users set display=$display, position=$position  where id=$id");
		endforeach;
	}
	$return = 'Settings Updated.';
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;

if($_GET['task']=='delete_form' && !empty($_GET['user_id'])){
	$helper->query("delete from users where id=".$_GET['user_id']);
	?><div id="message" class="updated fade"><p><strong>Agent has been deleted.</strong></p></div><?php
}

$users=$helper->get_results("select id as user_id, concat(firstname,' ',lastname) as name, email, office_id, display, is_zoo from users where office_id in($office_id) order by position");	

$authors = $helper->get_results("SELECT ID, user_nicename, display_name FROM $wpdb->users ORDER BY display_name");

$team_not_display=get_option('realty_team_not_display');
if(empty($team_not_display))$team_not_display=array();

wp_print_scripts('jquery-ui-core');
wp_print_scripts('jquery-ui-sortable');	
?>
<form name="themes.php?page=theme-settings&tab=team" method="post" action="" id="elements">
	<div class="leftside">		
		<table class="widefat">
			<thead>
			<tr>				
				<th scope="col">Reorder</th>
				<th scope="col">Agent Name</th>
				<th scope="col">News Articles</th>
				<th scope="col">Not display on team page</th>
				<th scope="col">Display</th>
				<th scope="col">Source</th>
				<th scope="col">Actions</th>
			</tr>
			</thead>
			<tbody id="team_list">
			<?php $i=0;
			foreach($users as $user):if(!is_array($user)) continue; ?> 
				<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 					
					<td style="cursor:pointer"><input type="hidden" name="team_ids[]" value="<?php echo $user['user_id']; ?>" />&Delta;&nabla;</td>
					<td><label><?php echo $user['name']; ?></label></td>
					<td>					
						<select name="member[<?php echo $user['user_id']; ?>][wpUser]">
							<option value="">Select Author Name</option>
							<?php foreach($authors as $author): ?>
							<option value="<?php echo $author['ID']; ?>"<?php if($settings[$user['user_id']]['wpUser']==$author['ID']) echo ' selected="selected"'; ?>><?php echo $author['display_name']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
					<td>
						<input type="checkbox" <?php if (in_array($user['user_id'],$team_not_display)) echo 'checked="checked" '; ?>name="team_not_display[]" value="<?php echo $user['user_id']; ?>">
					</td> 
					<td>						
						<input type="checkbox" <?php if ($user['display']==1) echo 'checked="checked" '; ?>name="member[<?php echo $user['user_id']; ?>][display]" value="1">
					</td> 
					<td><label><?php echo ($user['is_zoo']==1)?'API':'WP'; ?></label></td>
					<td>
						<a class="edit" title="Edit <?php echo $user['name']; ?>" href="themes.php?page=theme-settings&tab=team&action=edit&user_id=<?php echo $user['user_id']; ?>">Edit</a>&nbsp;|&nbsp;
						<a onClick="return confirm('You are about to delete \'<?php echo $user['name']; ?>\' from the team.\n\'OK\' to delete, \'Cancel\' to stop.' );" class="delete" href="themes.php?page=theme-settings&tab=team&task=delete_form&user_id=<?php echo $user['user_id']; ?>">Delete</a>
					</td>
				</tr> 
			<?php $i++;endforeach; ?>
			</tbody>
		 </table> 
		 <p class="submit"><input type="submit" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</div>
			
	<div id="poststuff">
		<div class="postbox">
			<h3>Actions</h3>		
			<div class="inside">
				<p class="submit"><input type="submit" value="<?php _e('Save'); ?>" name="submit" /></p> 
				<p>
				<a href="themes.php?page=theme-settings&tab=team&action=add_new" class="button">Add New</a>
				</p>			
			</div>	
		</div>
	</div>
</form>

<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#team_list").sortable({});
  });
</script>