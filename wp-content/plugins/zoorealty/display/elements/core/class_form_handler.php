<?php
class form_handler{
	function filter_input($table,$post_vars){
		global $wpdb;
		$sql = "SHOW COLUMNS FROM $table";		
		$row=$wpdb->get_col($sql);
		$return=array();
		if($row){
			foreach($row as $key){
				if (array_key_exists($key,$post_vars))
					$return["`$key`"] = "'" . mysql_real_escape_string(trim($post_vars[$key])) . "'";		
			}
		}
		return $return;		
	}//ends function

	function get_where($table, $post_vars){
		global $wpdb;
		$sql = "SHOW KEYS FROM $table";		
		$row=$wpdb->get_results($sql, ARRAY_A);	
		if($row){
			foreach($row as $item){
				$Column_name = 	$item['Column_name'];
				if(array_key_exists($Column_name,$post_vars)):
					$where['where'] = "WHERE `$Column_name`='" . $post_vars[$Column_name] . "'";
					$where['primary_key'] = "`$Column_name`";				
					return $where;
				endif;
			}
		}
		return false;			
	}//ends function

	function insert_form($table, $post_vars){	
		global $wpdb;
		$filtered_input = $this->filter_input($table,$post_vars);
		$fields = implode(", ", array_keys($filtered_input));		
		$values = implode(", ", $filtered_input);		
		$sql = "INSERT INTO $table ( $fields ) VALUES ( $values )";
		return $wpdb->query($sql);
	}//ends function

	function update_form($table, $post_vars){
		global $wpdb;
		$filtered_input = $this->filter_input($table,$post_vars);
		$where = $this->get_where($table, $post_vars);
		if(!$where) return false; // No key was found in the database matching any of the input request variables. Theres nowhere to update
		$primary_key = $where['primary_key'];
		unset($filtered_input[$primary_key]);
		foreach ($filtered_input as $key=>$value)
			$updates .= "$key=$value, ";
		$updates = substr($updates,0,-2);
		$sql = "UPDATE $table SET $updates " . $where['where'];
		$wpdb->query($sql);
		return true;
	}//ends function

	function delete_form($table, $post_vars){
		global $wpdb;
		$where = $this->get_where($table, $post_vars);
		if(!$where) return false;
		$sql = "DELETE FROM $table " . $where['where'];
		$wpdb->query($sql);
		return true;
	}//ends function
	
	
	function upload_file($upfile, $uploads_dir, $resize_to_thumb = 0, $resize_to_medium = 0,$allowed_types = 'jpg,gif,png'){
		$allowed_types = explode(",", $allowed_types);	
		if ( $upfile['size'] <= 0 ):
			$return['return'] = '<p>' . $upfile['name'] . " is empty.</p>"; // If the file has size 0 (is empty), leave immediately	
			return $return;
		endif;
		extract(pathinfo($upfile['name']));
		if ( array_search( strtolower($extension) , $allowed_types ) === false ):// If the file type is not in the list
			$return['return'] = "<p>The type of file ($extension) cannot be uploaded.</p>";
			return $return;
		endif;
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		$this->clean_filename($filename);
		$basename =  "$filename.$extension";
		$final_filename = $uploads_dir . $basename;

		for ($count = 1; file_exists( $final_filename ); $count++): //If the file already exists, it will be uploaded again with the _number appended to it in order to differentiate the file name.	
			$basename = $filename . "_$count" .  '.' . $extension;
			$final_filename = $uploads_dir . $basename;	 			
		endfor;	
		
		// all check has been done, upload the file that meets the requirements
		$tmp_name = $upfile['tmp_name'];
		if ( file_exists($tmp_name) ) :
		
		//$this->resize($tmp_name,400, $final_filename); return; //This will limit every file to 400px width.
	
		// test if the file can be uploaded to the REAL folder from the temporary folder
			if ( move_uploaded_file( $tmp_name , $final_filename ) ) :
				if($resize_to_thumb > 0)$this->resize($final_filename, $resize_to_thumb);		
				if($resize_to_medium > 0)$this->resize_medium($final_filename, $resize_to_medium);		
				$return ['return'] = "<p>$basename was uploaded successfully.</p>";
				$return ['path'] = $basename; 
			else:
				$return['return'] .= "<p>The file could not be saved onto the server. Please check if the upload folders exist and are set to the correct permissions.</p>"; 
			endif;
		else:
			// if it does not exist or that the file is too big for the server to handle, show error message
			$return['return'] .= '<p>Image was rejected by server. Probably for being larger than ' . get_cfg_var('upload_max_filesize') .' B.</p>';
			endif;
		return $return;		
	}//ends function 
	
	function resize($original,$new_width, $thumbnail='') {
		if(empty($thumbnail))
			$thumbnail = $this->get_thumbnail($original); 
		$extension = pathinfo($thumbnail, PATHINFO_EXTENSION);// get information about the original file
		switch ( strtolower ( $extension ) ):
			case 'jpg':
			case 'jpeg':
				$img  = @imagecreatefromjpeg( $original );
				break;
			case 'gif':
				$img  = @imagecreatefromgif( $original );
				break;
			case 'png':
				$img  = @imagecreatefrompng( $original );
				break;
			default:
				return;
			break;
		endswitch;
		
		$ratio = $new_width / imagesx($img ); // So that the image's height is resized proportionally to the width.
		$new_height = (imagesy($img)) * $ratio;		
		$dstim = @imagecreatetruecolor($new_width, $new_height);	
		imagecopyresized($dstim, $img, 0, 0, 0, 0, $new_width, $new_height, imagesx($img), imagesy($img));
				
		switch ( strtolower ( $extension ) ):
			case 'jpg':
			case 'jpeg':
				imagejpeg($dstim, $thumbnail );
				break;
			case 'gif':
				imagegif($dstim, $thumbnail );
				break;
			case 'png':
				imagepng($dstim, $thumbnail );
				break;
			default:
				break;
		endswitch;	
		//chmod( $imgFilenameThumb , 0777 );		
	} // end function

	function get_thumbnail($image_path){ //Returns the thumbnail path of an image by examining the path of the larger image.
		extract(pathinfo($image_path));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		return "$dirname/$filename" ."_thumb.$extension";	
	}//ends function
	
	function resize_medium($original,$new_width, $thumbnail='') {
		if(empty($thumbnail))
			$thumbnail = $this->get_medium($original); 
		$extension = pathinfo($thumbnail, PATHINFO_EXTENSION);// get information about the original file
		switch ( strtolower ( $extension ) ):
			case 'jpg':
			case 'jpeg':
				$img  = @imagecreatefromjpeg( $original );
				break;
			case 'gif':
				$img  = @imagecreatefromgif( $original );
				break;
			case 'png':
				$img  = @imagecreatefrompng( $original );
				break;
			default:
				return;
			break;
		endswitch;
		
		$ratio = $new_width / imagesx($img ); // So that the image's height is resized proportionally to the width.
		$new_height = (imagesy($img)) * $ratio;		
		$dstim = @imagecreatetruecolor($new_width, $new_height);	
		imagecopyresized($dstim, $img, 0, 0, 0, 0, $new_width, $new_height, imagesx($img), imagesy($img));
				
		switch ( strtolower ( $extension ) ):
			case 'jpg':
			case 'jpeg':
				imagejpeg($dstim, $thumbnail );
				break;
			case 'gif':
				imagegif($dstim, $thumbnail );
				break;
			case 'png':
				imagepng($dstim, $thumbnail );
				break;
			default:
				break;
		endswitch;	
		//chmod( $imgFilenameThumb , 0777 );		
	} // end function
	
	function get_medium($image_path){ //Returns the medium path of an image by examining the path of the larger image.
		extract(pathinfo($image_path));
		if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); // filename only since php 5.2
		return "$dirname/$filename" ."_medium.$extension";	
	}

	function format_url(&$url){
		$url = (!empty($url) && strpos($url, 'http://') === false)? 'http://' . $url : $url;
	
	}
	

}//ends class

//$form = new $form_handler;
?>