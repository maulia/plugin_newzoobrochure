<?php

// Sample hook for TBD setting template
add_filter('zoobrochure_settings_tbd', 'add_qrcode_tbd_template');
if (!function_exists('add_qrcode_tbd_template')) {

    /**
     * Filter (add new field) to the TBD template pdf
     * 
     * @since 2.0.0
     * @param array $section
     * @return array
     */
    function add_qrcode_tbd_template($section) {
        $section[] = array(
            'label' => __('QR Code'),
            'help' => 'Please check the checkbox if you want to display QR Code',
            'fields' => array(
                array(
                    'name' => 'qrcode',
                    'type' => 'checkbox',
                    'label' => 'Show QR Code',
                    'help' => ''
                )
            )
        );
        return $section;
    }

}

// Sample hook for TBA setting template
add_filter('zoobrochure_settings_tba', 'add_bottomlogo_tba_template');
if (!function_exists('add_bottomlogo_tba_template')) {

    /**
     * Filter (add new field) to the TBA template pdf
     * 
     * @since 2.0.0
     * @param array $section
     * @return array
     */
    function add_bottomlogo_tba_template($section) {
        $section[] = array(
            'label' => __('Bottom Logo'),
            'help' => 'Add logo for bottom logo',
            'fields' => array(
                array(
                    'name' => 'bottom_logo',
                    'type' => 'upload',
                    'label' => 'Bottom Logo',
                    'help' => 'Please use 960x160 dimension for best result'
                )
            )
        );
        return $section;
    }

}

if (!function_exists('zoobrochure_default_settings')) {

    /**
     * Produce default setting for every template pdf on admin setting
     * 
     * @since 2.0.0
     * @return array
     */
    function zoobrochure_default_settings() {
        $sections = [];
        $sections[] = array(
            'label' => __('General'),
            'help' => 'General option such as Logo and Icon',
            'fields' => array(
                array(
                    'name' => 'logo',
                    'type' => 'upload',
                    'label' => 'Logo',
                    'help' => 'Please use 960x160 dimension for best result'
                ),
                array(
                    'name' => 'bed_icon',
                    'type' => 'upload',
                    'label' => 'Bed Icon',
                    'help' => 'Please use 360x360 dimension for best result'
                ),
                array(
                    'name' => 'bath_icon',
                    'type' => 'upload',
                    'label' => 'Bath Icon',
                    'help' => 'Please use 360x360 dimension for best result'
                ),
                array(
                    'name' => 'car_icon',
                    'type' => 'upload',
                    'label' => 'Car Icon',
                    'help' => 'Please use 360x360 dimension for best result'
                ),
            )
        );
        $sections[] = array(
            'label' => __('Color'),
            'help' => 'To set the color, click in each box and choose your color by click the color wheel in the bottom.',
            'fields' => array(
                array(
                    'name' => 'bg_color',
                    'type' => 'color',
                    'label' => 'Background Color',
                    'help' => ''
                ),
                array(
                    'name' => 'font_color',
                    'type' => 'color',
                    'label' => 'Font Color',
                    'help' => ''
                ),
                array(
                    'name' => 'headline_color',
                    'type' => 'color',
                    'label' => 'Headline Color',
                    'help' => ''
                ),
                array(
                    'name' => 'headline_font_color',
                    'type' => 'color',
                    'label' => 'Headline Font Color',
                    'help' => ''
                ),
                array(
                    'name' => 'block_color',
                    'type' => 'color',
                    'label' => 'Block Color',
                    'help' => ''
                ),
                array(
                    'name' => 'color_preview',
                    'type' => 'color_preview',
                    'label' => '',
                    'help' => '',
                    'args' => ['current_template', 'bg_color', 'font_color', 'headline_color', 'block_color']
                )
            )
        );
        return $sections;
    }

}


add_filter('zoobrochure_template_register', 'zoobrochure_default_templates');

if (!function_exists('zoobrochure_default_templates')) {

    /**
     * Generate default list template TBA & TBD
     * 
     * @since 2.0.0
     * @param array $templates
     * @return array
     */
    function zoobrochure_default_templates($templates) {
        $templates['tba'] = array(
            'name' => 'TBA',
            'screenshot_url' => plugins_url('assets/images/sample/tba.jpg', dirname(dirname(__FILE__))),
            'sample_url' => plugins_url('assets/images/sample/tba.pdf', dirname(dirname(__FILE__))),
        );
        $templates['tbd'] = array(
            'name' => 'TBD',
            'screenshot_url' => plugins_url('assets/images/sample/tbd.jpg', dirname(dirname(__FILE__))),
            'sample_url' => plugins_url('assets/images/sample/tbd.pdf', dirname(dirname(__FILE__)))
        );
        $templates['classic'] = array(
            'name' => 'Classic'
        );
        return $templates;
    }

}
