<?php get_header(); ?>
<div id="content">
<div id="blog-div" class="search">

	<?php if (have_posts()) : ?>

		<h5 class="page_title">Search Results</h5>

		<?php while (have_posts()) : the_post(); ?>

			<div class="post">
				<?php if(function_exists('userphoto_the_author_thumbnail')) { echo '<p class="author_thumb">'; userphoto_the_author_thumbnail(); echo '</p>'; } ?>
				<div class="post_title_date<?php if(!function_exists('userphoto_the_author_thumbnail')) { echo ' no_author_thumb'; } ?>">
					<h2 class="post_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<p class="postdate"><?php the_time('jS F, Y') ?><?php the_tags( ' | <span class="tagged">Tagged: ', ', ', '</span>'); ?></p>
				</div>
				<?php $category = get_the_category(); $category_link = get_category_link( $category[0]->cat_ID ); ?><p class="flag cat-<?php echo $category[0]->category_nicename; ?>"><span class="filed_cats"><a href="<?php echo $category_link; ?>" title="<?php echo $category[0]->cat_name; ?>"><?php echo $category[0]->cat_name; ?></a></span></p>

				<div class="entry">
					<?php the_excerpt_reloaded(30, '<a>', 'excerpt', TRUE, 'more &#187;'); ?>
				</div>

			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<p class="alignleft"><?php next_posts_link('View older posts') ?></p>
			<p class="alignright"><?php previous_posts_link('View newer posts') ?></p>
		<div class="clearer"></div>
		</div>

	<?php else : ?>

		<h2 class="center">No posts found. Try a different search?</h2>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div><!-- end #blog-div -->
	</div><!-- end #content -->

<?php include (TEMPLATEPATH . '/sidebar_blog.php'); ?>

<?php get_footer(); ?>
