<?php
global $helper, $current_user, $realty, $wpdb;
if (!is_user_logged_in())
{
    ?>
    <script type="text/javascript">
        location.href = '<?php echo $realty->siteUrl . 'wp-login.php'; ?>';
    </script>
    <?php die(); 
}

if (!current_user_can('administrator'))
{
    ?>
    <script type="text/javascript">
        location.href = '<?php echo $realty->siteUrl . 'dashboard/listings/'; ?>';
    </script>
    <?php
}
include('menu_admin.php');

$offices = $wpdb->get_results("select id from office order by id", ARRAY_A);
?>
<form name="update_properties" id="update_properties" class="form-profile" action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="block_content">
        <h4>Search By</h4>
        <table>
            <tr>
                <td class="label">Account ID</td>
                <td>
                    <select name="office_id" class="full">
                        <option value="">Please Select</option>
                        <?php
                        foreach ($offices as $office)
                        {
                            ?>
                            <option value="<?php echo $office['id'] ?>" <?php if ($_POST['office_id'] == $office['id']) echo 'selected="selected"'; ?>><?php echo $office['id'] ?></option>
                        <?php } ?>
                    </select>					
                </td>

                <td class="label">Status</td>
                <td>
                    <select name="status" class="full">
                        <option value="">Any</option>
					   <?php
                        $status = array(1 => 'Available', 'Sold', 'Draft', 'Under Offer', 'Withdrawn', 'Leased');
                        foreach ($status as $key => $value)
                        {
                            ?>
                            <option value="<?php echo $key; ?>" <?php if($_POST['status']==$key)echo 'selected="selected"'; ?>><?php echo $value; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
                
            </tr>
            <tr>
                <td class="label">Account Name</td>
                <td>
					<?php
					$offices = $wpdb->get_results("select id, name from office order by name", ARRAY_A);
					?>
                    <select name="officename" class="full">
                        <option value="">Please Select</option>
                        <?php
                        foreach ($offices as $office)
                        {
                            ?>
                            <option value="<?php echo $office['name'] ?>" <?php if ($_POST['officename'] == $office['name']) echo 'selected="selected"'; ?>><?php echo $office['name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
				<td class="label">Paid</td>
                <td>
                    <select name="listingpaid" class="full">
                          <option value="">Any</option>
						  <option value="1" <?php if ($_POST['listingpaid'] == '1') echo 'selected="selected"'; ?>>Yes</option>                           
                          <option value="0" <?php if ($_POST['listingpaid'] == '0') echo 'selected="selected"'; ?>>No</option>                           
                    </select>
                </td>
			</tr>
			<tr>
                <td class="label">Listing ID</td>
                <td>
                    <input type="text" class="full" name="property_id" value="<?php echo $_POST['property_id']; ?>">
                </td>
				<?php /*
				<td class="label">Type</td>
                <td>
                    <select name="listing_type" class="full">
                          <option value="">Any</option>
						  <option value="Standard" <?php if ($_POST['listing_type'] == 'Standard') echo 'selected="selected"'; ?>>Standard</option>
						  <option value="Featured" <?php if ($_POST['listing_type'] == 'Featured') echo 'selected="selected"'; ?>>Featured</option>
						  <option value="Premium" <?php if ($_POST['listing_type'] == 'Premium') echo 'selected="selected"'; ?>>Premium</option>                      
                    </select>
                </td>*/ ?>
            </tr>
        </table>

        <p class="button"><input type="submit" name="submit" value="Search" class="btn"></p>
    </div>
</form>
<div class="clear"></div>

<?php
if (isset($_POST['submit']))
{
    if (!empty($_POST['property_id']))$condition.= "and id like '%" . intval($_POST['property_id']) . "%'";
    if (!empty($_POST['office_id']))$condition.= "and office_id in (" . $_POST['office_id'] . ")";
    if (!empty($_POST['status']))$condition.= "and status in (" . $_POST['status'] . ")";
    if ($_POST['listingpaid']!='')$condition.= "and listingpaid in (" . $_POST['listingpaid'] . ")";
    if ($_POST['listing_type']!='')$condition.= "and listing_type in ('" . $_POST['listing_type'] . "')";
    if (!empty($_POST['email']))$condition.= "and office_id in (select id from office where email='" . $_POST['email'] . "')";
    if (!empty($_POST['officename']))$condition.= "and office_id in (select id from office where name='" . $_POST['officename'] . "')";

    $listings = $wpdb->get_results("select id, office_id, created_at, type, status, listingpaid, street_number, street, suburb from properties where 1 $condition", ARRAY_A);
	
}
?>

<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl; ?>css/table_sorter.css" type="text/css" media="all" />
<script type="text/javascript">
jQuery(function() { 
	jQuery("#properties_table").tablesorter({ 
		widgets: ['zebra'], 
		 dateFormat: 'uk',
		headers: { 0:{ sorter: "shortDate", dateFormat: 'uk'} } 
	}); 
});
</script>	
<table class="tablesorter" cellpadding="0" cellspacing="0" id="properties_table">
    <thead>
        <tr class="th">
            <th class="th_date">Date Added</th>
            <th class="th_id">Listing ID</th>
            <th class="th_id">Account ID</th>            	
            <th class="th_type acc_name">Account Name</th>		
			<th class="th_type">Type</th>			
            <th class="th_address">Address</th>			
            <th class="th_type">Status</th>			
            <th class="th_type paid">Paid</th>			
            <th class="th_edit">Actions</th>
        </tr>
    </thead>
    <?php
    if (!empty($listings))
    {
        foreach ($listings as $property):
            foreach ($offices as $data)
            {
                if ($data['id'] == $property['office_id'])
                {
                    $office = $data;
                }
            }
			switch($property['status']){
				case '1':$status='Available';break;
				case '2':$status='Sold';break;
				case '3':$status='Draft';break;
				case '4':$status='Under Offer';break;
				case '5':$status='Withdrawn';break;
				case '6':$status='Leased';break;
				case '7':$status='Waiting';break;
				case '8':$status='Expired';break;
			}
					$property['url'] = $realty->siteUrl . $property['id'] . "/";
            ?>
            <tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
                <td class="td_date"><?php echo date("d-m-Y", strtotime($property['created_at'])); ?></td>
                <td class="td_id">
                    <form name="preview_form_<?php echo $property['id']; ?>" id="preview_form_<?php echo $property['id']; ?>" action="<?php echo $property['url']; ?>" method="post" target="_blank">
                        <input type="hidden" name="preview" value="1">	
                        <a href="javascript:document.forms['preview_form_<?php echo $property['id']; ?>'].submit();" title="Preview"><?php echo $property['id']; ?></a>
                    </form>
                </td>
                <td class="td_type"><a href="<?php echo $realty->siteUrl . 'dashboard/listings/?office_id=' . $property['office_id']; ?>" title="View System" target="_blank"><?php echo $property['office_id']; ?></a></td>		
                <td class="td_type"><?php echo $office['name']; ?></td>		
                <td class="td_type"><a href="<?php echo $realty->siteUrl.'dashboard/package/?id='.$property['id']."&office_id=".$property['office_id'];?>" title="Change Type" target="_blank"><?php echo ($property['listing_type'])?$property['listing_type']:'Standard'; ?></a></td>		
                <td class="td_address"><?php echo $property['street_number']." ".$property['street']." ".$property['suburb']; ?></td>		
                <td class="td_status"><a href="<?php echo $realty->siteUrl.'dashboard/status/?id='.$property['id']."&office_id=".$property['office_id'];?>" target="_blank"><?php echo $status; ?></a></td>
				<td class="th_paid">
					<a href="<?php echo $realty->siteUrl.'dashboard/payment/?id='.$property['id']."&office_id=".$property['office_id'];?>" target="_blank"><?php echo ($property['listingpaid'] != '1')?'Pay':'Paid'; ?></a>
				</td>		
                <td class="td_edit">
                    <table>
                        <tr>
                            <td>
                                <a class="ed_edit" title="Edit" href="<?php echo $realty->siteUrl . 'dashboard/detail/?id=' . $property['id'] . "&type=" . $property['type'] . "&office_id=" . $property['office_id']; ?>" target="_blank" ><i class="icon-edit-sign"></i>Edit</a>
                            </td>
                            <td>
                                <a class="delete" onClick="return confirm('You are about to delete the this listing.\n\'OK\' to delete, \'Cancel\' to stop.');" href="<?php echo $realty->siteUrl . 'dashboard/listings/?id=' . $property['id'] . "&task=delete&office_id=" . $property['office_id']; ?>"  target="_blank"><i class="icon-remove"></i>Delete</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php
        endforeach;
    }
    ?>
</table>