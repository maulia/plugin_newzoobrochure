<?php //metadata: access_level=1 ?>
<div class="wrap">
<h2>Send Property Alerts Sms</h2>
<p>Check the subscribers then press "Send Sms" button to send property alerts sms.</p>
<form name="form_subscriptions_manager" method="get" target='alerts_content' action="<?php echo $this->form_action; ?>" onsubmit="window.open(this.action, this.target, 'width=900, height=700, resizable=1,scrollbars=1, left=300'); return true;">
	<input type="hidden" name="page" value="SendSms" />
	<input type="hidden" name="task" id="task" value="send_sms" />
	<p><select name="alert">
		<option value="all_alerts">All Alerts</option>
		<?php foreach($list_alerts as $key=>$value): ?>
		<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
		<?php endforeach; ?>
	</select>
	</p>
	<div class="tablenav">
	<?php echo $subscribers['pagination_label']; ?>
	</div>
	<table class="widefat">
	  <thead>
	  <tr>
		<th scope="col"><input type="checkbox" id="all_subscribers" onClick="check_all_suburb();"/></th>   
		<th scope="col"><?php _e('E-mail') ?></th>
		<th scope="col"><?php _e('Name') ?></th>
		<th scope="col"><?php _e('Alert(s)') ?></th>
		<th scope="col"><?php _e('Mobile') ?></th>
	  </tr>
	  </thead>
	  <tbody id="the-list">
	<?php if(!empty($subscribers)) foreach($subscribers as $item): if(!is_array($item)) continue;//pagination_label data ?>
		<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">
		
		<td><input type="checkbox" value="<?php echo $item['subscriber_id']; ?>" name="sbs_ids[]" /></td>
		<td><a href="mailto:<?php echo $item['email']; ?>" title="">Click</a></td>
		<td>	<?php echo $item['first_name'] . ' ' . $item['last_name']; ?></td>
		<td>	<?php echo $item['alert']; ?></td>
		<td>	<?php echo $item['mobile_phone']; ?></td>
		</tr>	
	<?php endforeach; ?>
	  </tbody>
	</table>
	<p><input class="button" value="Send Sms" type="submit"/></p>
</form>
<script>
	function check_all_suburb(){				
		var max = document.getElementsByName('sbs_ids[]').length;		
		for(var idx = 0; idx < max; idx++){
			if(eval("document.getElementById('all_subscribers').checked") == true)document.getElementsByName('sbs_ids[]')[idx].checked = true;
			else document.getElementsByName('sbs_ids[]')[idx].checked = false;
		}
	}   
</script>
<br>
<?php
if(isset($_POST['submit_pass'])){
if(!update_option('sms_username',$_POST['sms_username']))add_option('sms_username',$_POST['sms_username']);
if(!update_option('sms_password',$_POST['sms_password']))add_option('sms_password',$_POST['sms_password']);
if(strlen($_POST['sms_add_text'])<=15){ if(!update_option('sms_add_text',$_POST['sms_add_text']))add_option('sms_add_text',$_POST['sms_add_text']); }
else $message_pass='The maximum of addition text is 15 charcaters';
if(empty($message_pass))$message_pass = 'Settings Updated.';
}
if(!empty($message_pass)):
	?><div id="message_pass" class="updated fade"><p><strong><?php echo $message_pass; ?></strong></p></div><?php
endif;
?>

<form action="" method="post">
<table class="widefat">
<thead>
<tr>
	<th colspan="2">Other Settings</th>
</tr>
</thead>
<tr class="alternate">
	<td>Sms global username</td>
	<td><input type="text" name="sms_username" value="<?php echo get_option('sms_username'); ?>"></td>
</tr>
<tr>
	<td>Sms global pasword</td>
	<td><input type="text" name="sms_password" value="<?php echo get_option('sms_password'); ?>"></td>
</tr>
<tr class="alternate">
	<td>Addition text in sms ( Max 15 characters )</td>
	<td><input type="text" name="sms_add_text" value="<?php echo get_option('sms_add_text'); ?>"></td>
</tr>
</table>
<p><input class="button" name="submit_pass" type="submit" value="Save" name=""  /></p>
</form>
</div>