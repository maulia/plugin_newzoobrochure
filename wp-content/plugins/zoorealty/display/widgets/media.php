<?php 
if(isset($_GET['settings']) && isset($_GET['property_id'])){ //If it came from a popup, include the standard WP and HTML headers
	require_once('../../../../../wp-blog-header.php');
	$settings['mode'] = $_GET['settings'];
	wp_print_scripts('jquery');
	$realty->property();	
?>
<html>
	<head>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" title="Ginga" />
	<title><?php bloginfo('name'); ?> - Media Slideshow</title>
	</head>
	<body id="ssp">
<?php 
}
else{ ?>
	<h2 class="section_title">
	<?php echo ($realty->property['display_address']!='1')?$realty->property['suburb']:$realty->property['street_address'].", ".$realty->property['suburb'].", ".$realty->property['state'].", ".$realty->property['postcode']; ?>
	</h2>
<?php
}
if(!$realty->is_property_page || (empty($realty->property['photos']) && empty( $realty->property['floorplans'])) ) return;
if(!class_exists('photo_manager'))$settings['mode'] = str_replace('ssp','normal', $settings['mode']);
if(!empty($realty->property['photos'])){
	$photos = $realty->property['photos'];
	if(stripos($settings['mode'], 'lightbox') !== false){ ?>
	<script type="text/javascript" src="<?php echo $realty->pluginUrl; ?>js/jquery.prettyPhoto.js?v=0,01"></script>
	<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php echo $realty->pluginUrl.'css/prettyPhoto.css'; ?>?v=0,01" />
	<?php } 

	if($settings['mode']=='thumbnail_lightbox' || $settings['mode']=='thumbnail_lightbox_horizontal'  || $settings['mode']=='thumbnail_ssp_large'): ?>
	<script src="<?php echo $realty->pluginUrl.'display/pages/js/'; echo ($settings['mode']=='thumbnail_lightbox_horizontal')?'scroller_horizontal.js':'scroller_vertical.js'; ?>" type="text/javascript"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("a[rel^='prettyPhoto']").prettyPhoto({allow_resize: false,deeplinking: false,social_tools: false});
		});
	</script>
	<div class="media_div with_scroller">
		<table cellpadding="0" cellspacing="0" class="media_table">
			<tr>
				<td class="td_slider_photo">
					<?php if($settings['mode']=='thumbnail_ssp_large'){?>
					<a href="<?php echo $realty->pluginUrl ."display/widgets/media.php?property_id=" . $realty->property['id'] .'&amp;settings=ssp_large'; ?>" class="popup_window" rel="width=840, height=680" title="View Photo Gallery">
					<?php } else if($settings['mode']=='thumbnail_lightbox' || $settings['mode']=='thumbnail_lightbox_horizontal' ){ ?>
					<?php /*	<a href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[property_photos]" title="View Photo Gallery" id="slide_photo_ref"> */ ?>
					<a href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="prettyPhoto[property_photo]" title="View Photo Gallery">
					<?php } ?>					
					<?php /* <img src="<?php echo $photos[0]['large']; ?>" id="slide_photo"/> */ ?>
					<img src="<?php echo $photos[0]['large']; ?>" id="main_img_"/>
					</a>
					<?php for ($i=1;$i<count($photos);$i++){ ?>
					<a href="<?php echo $photos[$i]['large']; ?>" style="display:none;" onClick="return false;" rel="prettyPhoto[property_photo]"></a>
					<?php } ?>

				</td>				
				<td class="td_photo_scroller">	
					<div id="scroll-container">
					<div id="scroll-content" <?php $width=(count($photos)-1)*135; $height=(count($photos)-1)*114; echo ($settings['mode']=='thumbnail_lightbox_horizontal')?'style="width:'.$width.'px !important;"':'style="height:'.$height.'px !important;"' ; ?>>
						<?php 	
						for ($count=1;$count<count($photos);$count++){
							$photo=$photos[$count]; 
							
							if($settings['mode']=='thumbnail_ssp_large'){ ?>
							<a href="<?php echo $realty->pluginUrl ."display/widgets/media.php?property_id=" . $realty->property['id'] .'&amp;settings=ssp_large'; ?>" class="popup_window" rel="width=840, height=680"><img src="<?php echo $photo['small']; ?>" /></a>
							<?php }else{ ?>
								<? /* <a href="<?php echo $photo['large']; ?>" onclick="return false;" rel="prettyPhoto[property_photos]"><img src="<?php echo $photo['small']; ?>" /></a> */ ?>
								<a href="#" onClick="document.getElementById('main_img_').src='<?php echo $photo['large']; ?>'; return false;"><img src="<?php echo $photo['small']; ?>" /></a>								
								
							<?php }
							
						}						
					?>
					</div>
					<div id="scroll-controls">
						<a href="#" class="up-arrow"></a><a href="#" class="down-arrow"></a>
					</div>
				</div>
				</td>
			</tr>
		</table>
	</div>
	
	<?php elseif($settings['mode']=='crausel_lightbox'): ?>
	<link type="text/css" href="<?php echo $realty->pluginUrl?>display/pages/js/rotate.css" rel="stylesheet" /> 
	<script type="text/javascript" src="<?php echo $realty->pluginUrl?>display/pages/js/jquery.pikachoose.js"></script>
	<script language="javascript">
		jQuery(document).ready(function(){
			jQuery("#pikame").PikaChoose({carousel:true,carouselVertical:true,speed:2000});
		});
	</script>
	<div class="media_div with_scroller">
		<table cellpadding="0" cellspacing="0" class="media_table">
			<tr>
				<td class="td_slider_photo">
					<ul id="pikame" >
						<?php for ($i=0;$i<count($photos);$i++){ $possition=$i+1; ?>
							<li>
							<img src="<?php echo $photos[$i]['large']; ?>" <?php echo ($i==0)?'style="height:520px;width:690px;"':'style="display:none;height:520px;width:690px;"';?>/>
							</li>
						<?php } ?>			
					</ul>
				</td>
			</tr>
		</table>
	</div>
		
	<?php elseif($settings['mode']=='slide_lightbox'):  ?>
	<div class="property-images">
		<div id="main_image">
			<a href="<?php echo $photos[0]['medium']; ?>" onclick="return false;" rel="prettyPhoto[property_photo]" title="View Photo Gallery"><img id="main_img_<?php echo $listing['id']; ?>" src="<?php echo $photos[0]['medium']; ?>"></a>
			<?php for ($i=1;$i<count($photos);$i++){ ?>
			<a href="<?php echo $photos[$i]['medium']; ?>" style="display:none;" onClick="return false;" rel="prettyPhoto[property_photo]"></a>
			<?php } ?>
		</div>
		<div id="wrapper">
				<div id="slideWrap">
				<?php
				$count=0; $page=1; 
				foreach($photos as $photo){      
					if($count%8==0){ echo "<div id='slide_$page' class='slide'>";$page++; } ?>				
					<a href="#" onClick="swapimage('<?php echo $photo['medium']; ?>','<?php echo $listing['id']; ?>');return false;"><img src="<?php echo $photo['small']; ?>"></a>
					<?php if($count%8==7 || $count==(count($photos)-1) ){ echo "</div>";  } 
					$count++; 
				} ?>			
				</div>	
		</div>
		<?php if($page>2){ ?>
		<ul id="slideNav">
			<li id="prevSlide" style="display:none;"><a href="#" class="prev disabled">&lt; previous slide</a></li>
			<?php for($i=1;$i<$page;$i++){ ?>
			<li><a href="#" id="slideNav-<?php echo $i; ?>" onclick="showSlide('<?php echo $i; ?>'); return false;" <?php if($i=='1')echo 'class="selected"';?>><?php echo $i; ?></a></li>
			<?php } ?>
			<li id="nextSlide" style="display:none;"><a href="#" class="next" onclick="nextSlide(); return false;">&gt;</a></li>
		</ul>
		<?php } ?>
		
		<div class="clear"></div>	
    </div>
	
	<script type="text/javascript">	
	function Each(array, block){
		for (var i=0, l = array.length; i<l; i++){
			block(array[i]);
		}
	}
	 
	var currentSlide = 1;
	var maxSlides = 5;
	var animating = false;
	var slideWidth = 164;
	var currentPX = 0;
	 
	function nextSlide(){
		var nextSlideNum = currentSlide + 1;
		if(nextSlideNum >= maxSlides){ toggleButton('next','disable'); }
		else if (nextSlideNum == 2 && currentSlide == 1){ toggleButton('prev','enable'); }
		currentSlide = nextSlideNum;
		if(animating == false){ scroll('right'); }
		return false;
	}
	 
	function prevSlide(){
		var prevSlideNum = currentSlide - 1;
		if(prevSlideNum == 1){ toggleButton('prev','disable'); }
		else if (currentSlide == maxSlides){ toggleButton('next','enable'); }
		currentSlide = prevSlideNum;
		if(animating == false){ scroll('left'); }
		return false;
	}
	 
	function toggleButton(button,state){
		if(button == 'next' && state == 'disable'){
			document.getElementById('nextSlide').innerHTML='<a href="#" class="next disabled">next slide &gt;</a>';
			document.getElementById('nextSlide').removeEventListener('click',nextSlide);
		}
		if(button == 'next' && state == 'enable'){
			document.getElementById('nextSlide').innerHTML='<a href="#" class="next">next slide &gt;</a>';
			document.getElementById('nextSlide').addEventListener('click',nextSlide);
		}
		if(button == 'prev' && state == 'disable'){
			document.getElementById('prevSlide').innerHTML='<a href="#" class="prev disabled">&lt; previous slide</a>';
			document.getElementById('nextSlide').removeEventListener('click',prevSlide);
		}
		if(button == 'prev' && state == 'enable'){
			document.getElementById('prevSlide').innerHTML='<a href="#" class="prev">&lt; previous slide</a>';
			document.getElementById('nextSlide').addEventListener('click',prevSlide);
		}
	}
	 
	function scroll(direction){
		Each(document.getElementById("slideNav").getElementsByTagName("a"), function(as){
			as.removeClassName("selected");
		});
	 
		document.getElementById("slideNav-"+currentSlide).addClassName("selected");
	 
		animating = true;
		var pixelShift = slideWidth * (currentSlide-1);
		var startingSlideNum = currentSlide;
		var Slide_container = document.getElementById('slideWrap');
		var slide=0;
	 
		
		if(direction == 'left'){
			slide=pixelShift-currentPX;
			jQuery(Slide_container).animate({
				left: '-='+slide,
			  }, 300, function() {
			});
		}
		if(direction == 'right'){
			slide=currentPX-pixelShift;
			jQuery(Slide_container).animate({
				left: '+='+slide,
			  }, 300, function() {
 		    });
		}	 
		currentPX = pixelShift;
	}
	 
	function showSlide(num){
		startingSlide = currentSlide;
		currentSlide = num;
	 
		if(num > startingSlide){ scroll('left'); }
		else if (num < startingSlide) { scroll('right'); }
	 
		if (startingSlide == maxSlides && currentSlide == 1){ toggleButton('next','enable'); toggleButton('prev','disable'); }
		else if (currentSlide == maxSlides && startingSlide == 1){ toggleButton('prev','enable'); toggleButton('next','disable'); }
		else if (currentSlide == 1){ toggleButton('prev','disable'); }
		else if (currentSlide == maxSlides){ toggleButton('next','disable'); }
		else if (startingSlide == 1) { toggleButton('prev','enable'); }
		else if (startingSlide == maxSlides) { toggleButton('next','enable'); }
	}
	-->
	
	function swapimage(image, id){
		document.getElementById("main_img_"+id).src=image;
	}
	</script>

	<?php elseif($settings['mode']=='imageviewer'):  ?>
	<link type="text/css" href="<?php echo $realty->pluginUrl?>css/imageviewer.css" rel="stylesheet" /> 
	<script src="<?php echo $realty->pluginUrl?>js/imageviewer.js" type="text/javascript"></script>

	<script type="text/javascript">
	var fadeimages=new Array();
	<?php for ($count=0;$count<count($photos);$count++){ $photo=$photos[$count]; ?>
	fadeimages[<?php echo $count ?>]=["<?php echo $photo['large'] ?>", "", ""]
	<?php } ?>


	var thumbimages=new Array();
	<?php for ($count=0;$count<count($photos);$count++){ $photo=$photos[$count]; ?>
	thumbimages[<?php echo $count ?>]=["<?php echo $photo['small'] ?>","javascript: watchForClick(<?php echo $count ?>);","",""]
	<?php } ?>

	var total_fades = fadeimages.length;
	var total_thumbs = thumbimages.length;
	var totalImages = parseInt(total_fades + total_thumbs);
	var borderimg = "<?php echo $realty->pluginUrl?>css/media/borderimg.gif";

	var preloadedthumbimages=new Array()
	for (p=0;p<total_thumbs;p++) {
		preloadedthumbimages[p]=new Image();
		preloadedthumbimages[p].onload = function() { thumbimagesLoaded++; }
		preloadedthumbimages[p].src=thumbimages[p][0];
	}
	var preloadedfadeimages=new Array()
	for (p=0;p<total_fades;p++) {
		preloadedfadeimages[p]=new Image();
		preloadedfadeimages[p].onload = function() { fadeimagesLoaded++; }
		preloadedfadeimages[p].src=fadeimages[p][0];
	}
	
	jQuery(document).ready(function(){
		init();
	});
	</script>
	<div id="mainpropimageContainer">
		<div id="mainpropimage" style="display: block;">
			<div id="imageload"><img src="<?php echo $realty->pluginUrl?>display/pages/js/loading.gif" border="0" alt="Loading" /></div>
			<div id="imageloadcount"></div>
		</div>
	</div>
	<?php if(!empty($realty->property['latitude']) && !empty($realty->property['longitude'])){ ?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php  echo $realty->settings['general_settings']["google_maps_key"]; ?>" type="text/javascript"></script>	
	<script type="text/javascript">
	//<![CDATA[
		var map;
		var use_gmap = 1;
		var latlng = new GLatLng(<?php echo $realty->property['latitude']; ?>, <?php echo $realty->property['longitude']; ?>);

		function googleLoad() {
		  if (GBrowserIsCompatible()) {
			map = new GMap2(document.getElementById("googlemap_image"));
			map.setCenter(latlng, 15);
			var marker = new GMarker(latlng);
			map.addOverlay(marker);     
			map.addControl(new GSmallMapControl());
			map.removeMapType(G_HYBRID_MAP);
			map.addMapType(G_PHYSICAL_MAP);
			var mapControl = new GMapTypeControl();
			map.addControl(mapControl);
		  }
		}
	//]]>
	</script>
	<div id="googlemap">
		<div id="googlemap_image"></div>
	</div>
	<?php }else{ ?>
	<script type="text/javascript">
	//<![CDATA[
		var use_gmap = 0;		
	//]]>
	</script>
	<?php } ?>
	<div style="clear:both;"></div>
	<div id="propcontainer">
		<div id="imagesMainContainer">
			<div id="propimages" style="width: 830px;">
				<div id="propslide" style="position: absolute; left: 0px; top: 0px; white-space:nowrap;"></div>
			</div>			
		</div>
	</div>		
	
	<?php elseif($settings['mode']=='rw_crausel'):  ?>	
	<div class="gallery">
		<div class="gThumbs">
			<ul>
				<?php foreach($photos as $photo){ ?>
				<li class="gThumbImg"><img src="<?php echo $photo['small']; ?>" /><span></span></li>
				<?php } ?>
			</ul>
        </div>
        <div id="hero-container" class="gMain">
			<a href="#" class="rwOHero-prev">Previous</a>
			<a href="#" class="rwOHero-next">Next</a>
			<ul>
				<?php foreach($photos as $photo){ ?>
				<li class="gMainImg"><img src="<?php echo $photo['large']; ?>" load-src="<?php echo $photo['large']; ?>" style="margin-left:<?php echo image_ratio($photo['large'], 520, 390); ?>;" /></li>
				<?php } ?>
			</ul>
		</div>
        <div class="clear"></div>
	</div>
	<link type="text/css" href="<?php echo $realty->pluginUrl?>css/rwmin.css?v=0.03" rel="stylesheet" />
	<script src="<?php echo $realty->pluginUrl?>js/rwmin.js?v=0.02" type="text/javascript"></script>
	
	<?php 
	else: 
		global $photo_manager;
		if(!isset($photo_manager))$photo_manager = new photo_manager;
		wp_print_scripts('jquery-ui-core');wp_print_scripts('jquery-ui-tabs');
		?>	
		<div class="clearer"></div>
		<script type="text/javascript">
		/* <![CDATA[ */

		jQuery(document).ready(function(){		
			jQuery("#SSP_content").tabs();
		});//ends document).ready
		/* ]]> */
		</script>
		<div id="flashcontent"> 
			<div id="SSP_content">	
				<ul>
					<li class="flashpic"><a href="#photoshow_wrap"><span>Photos</span></a></li>
				<?php if(($realty->property['Video'] !=false)): ?>		
					<li class="flashvid"><a href="#videoshow_wrap"><span>Video</span></a></li>		
				<?php endif; ?>
				<?php if(($realty->property['floorplans'] !=false)): ?>		
					<li class="flashplan"><a href="#floorplans_wrap"><span>Floorplans</span></a></li>					
				<?php endif; ?>	
				</ul>	
				<?php
				$media_settings = $realty->settings['media_slideshow'];
				$gallery = array('title' => 'Photos of ' . $realty->property['street_address'], 
				  'settings' => $media_settings,
							'albums' => array(array(
							'title' => 'Photos of ' . $realty->property['street_address'], 
							'description'=>addslashes(htmlentities($realty->property['headline'])),
							'thumbnail' => $realty->property['photos'][0]['tiny'],
							'photos'=> array()
											)));
				$photos = $realty->property['photos'];
				foreach($photos as $photo):
					$gallery['albums'][0]['photos'][] = array('path' =>$photo['large'], 'thumbnail' =>$photo['small'], 'title'=>htmlentities($photo['name']));
				endforeach;
				$photo_manager->ssp($gallery, "photoshow", null,'ssp'); //Call tg_ssp for thumbgrid

				?>
			</div><!--#SSP_content-->
		</div><!-- end #flashcontent -->
	<?php endif;	
}
if(isset($_GET['settings']) && isset($_GET['property_id'])){ //If it came from a popup?>
</body>
</html>
<?php } ?>