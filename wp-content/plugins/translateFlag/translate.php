<?php
/*
Plugin Name: Language Translator
Plugin URI: http://lancelhoff.com
Description: Adds a multi-language sidebar widget which enables visitors to translate your blog in their own language.
Author: Lancelhoff
Version: 1.0
Author URI: http://lancelhoff.com

The Original creator of this awesome plugin is Trevor Creech of http://trevorcreech.com/. I simply fixed the plugin making it compatible with WordPress 2.2 or greater.
*/
$langnames = Array('en'=>'English','fr'=>'French', 'de'=>'German', 'es'=>'Spanish', 'it'=>'Italian', 'zh-CN'=>'Chinese', 'ar'=>'Arabic', 'ru'=>'Russian', 'ms'=>'Malaysian','iw'=>'Hebrew','el'=>'Greek');
$langmap = array('en'=>array('ar','zh-CN','it','es','de','fr','el'), 'fr'=>array('en','de'), 'de'=>array('en','fr'), 'es'=>array('en'), 'it'=>array('en'),'zh-CN'=>array('en'), 'ar'=>array('en'),'ru'=>array('en'),'ms'=>array('en'),'iw'=>array('en'),'el'=>array('en'));

function ltranslate($orig_lang = 'en', $en_flag = 'uk') {
global $langnames, $langmap;
	$availablelangs = $langmap[$orig_lang];
	$url = 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		?>
		<script type="text/javascript">
		//<![CDATA[
		document.write('<?php foreach($availablelangs as $langcode) {
			if($langcode != $orig_lang)
				$full_url = 'http://www.google.com/translate?hl='.$orig_lang.'&amp;ie=UTF8&amp;langpair='.$orig_lang.'%7C'.$langcode.'&amp;u='.$url;
			widget_ltranslate_print_flag($langcode,$full_url,$en_flag); 
		}
		//Print its own flag
		widget_ltranslate_print_flag($orig_lang,$url,$en_flag); 
		?>');
		//]]>
		</script>
		<noscript>
		<?php foreach($availablelangs as $langcode) {
			if($langcode != $orig_lang)
				$full_url = 'http://www.google.com/translate?hl='.$orig_lang.'&amp;ie=UTF8&amp;langpair='.$orig_lang.'%7C'.$langcode.'&amp;u='.$url;
			widget_ltranslate_print_flag($langcode,$full_url,$en_flag); 
		}
		//Print its own flag
		widget_ltranslate_print_flag($orig_lang,$url,$en_flag); 
		echo '</noscript>';

} 

function widget_ltranslate_init() {

	
	function widget_ltranslate_noframes() {
		?>
		<script type="text/javascript">
		//<![CDATA[
		if ((top != self) && location.href.match(/^http:\/\/[\d]+\.[\d]+\.[\d]+\.[\d]+\/translate_c/))
		{
			if ( navigator.appName == 'Netscape' && parseInt(navigator.appVersion) == 6 ) {
			top.location.replace(location.href);
			} else {
				top.location = location.href;
			}
		}
		//]]>
		</script>
		<?php
	}
	
	function widget_ltranslate($args) {
		global $langnames,$langmap;
		extract($args);
		
		$options = get_option('widget_ltranslate');
		if ('' == $options['orig-lang'])
			$options['orig-lang'] = 'en';
		$title = $options['title'];
		

		echo $before_widget;
		//echo $before_title . $title . $after_title;
		echo "<div class=\"ltranslate\"><label>Translate this site:</label>";
		ltranslate($options['orig-lang'],$options['en-flag']);
		echo "</div>";
		echo $after_widget;
	}
	
	function widget_ltranslate_control() {
		global $langnames;
		$options = get_option('widget_ltranslate');
		if ( !is_array($options) )
			$options = array('title'=>'Translate', 'orig-lang'=>'en', 'en-flag' => 'uk');			
		if ( $_POST['ltranslate-submit'] ) {
			$options['title'] = strip_tags(stripslashes($_POST['ltranslate-title']));
			$options['orig-lang'] = strip_tags(stripslashes($_POST['ltranslate-orig-lang']));
			$options['en-flag'] = strip_tags(stripslashes($_POST['ltranslate-en-flag']));
			update_option('widget_ltranslate', $options);
		}

		$title = htmlspecialchars($options['title'], ENT_QUOTES);

		echo '<p style="text-align:right;"><label for="ltranslate-title">Title: <input style="width: 200px;" id="ltranslate-title" name="ltranslate-title" type="text" value="'.$title.'" /></label></p>';
		echo '<p style="text-align:right; float:right;">Blog Language: <br />';
		foreach($langnames as $langcode=>$langname)
		{
			$checked = '';			
			if ($langcode == $options['orig-lang'])
				$checked = 'checked="checked "';
			echo '<input type="radio" id="ltranslate-orig-lang" name="ltranslate-orig-lang" ' . $checked . 'value="'.$langcode.'"> '.$langname.'<br>';
		}
		
		echo '<p style="text-align:left; float:left;">English Flag: <br />';
		$en_flags = array('uk'=>'United Kingdom','us'=>'United States', 'ca'=>'Canada');
		foreach($en_flags as $flagcode => $flagname)
		{
			$checked = '';			
			if ($flagcode == $options['en-flag'])
				$checked = 'checked="checked "';
			if ( file_exists(dirname(__FILE__) . "/flags/flag_$flagcode.png") )
				$image = str_replace(str_replace('\\', '/', ABSPATH), get_settings('siteurl').'/', str_replace('\\', '/', dirname(__FILE__))) . "/flags/flag_$flagcode.png";
			else
				$image = get_settings('siteurl')."/wp-includes/images/flags/flag_$flagcode.png";
			echo '<input type="radio" id="ltranslate-en-flag" name="ltranslate-en-flag" ' . $checked . 'value="'.$flagcode.'"><img src="'.$image.'" alt="'.$flagname.'" /><br>';
		}
		echo '<input type="hidden" id="ltranslate-submit" name="ltranslate-submit" value="1" />';
	}
	
	function widget_ltranslate_print_flag($langcode, $full_url,$en_flag = 'uk')
	{
		global $langnames;
		if('en' == $langcode)
		{
			$imagecode = $en_flag;
		}
		else
		{
			$imagecode = $langcode;
		}
						
		if ( file_exists(dirname(__FILE__) . "/flags/flag_$imagecode.png") )
				$image = str_replace(str_replace('\\', '/', ABSPATH), get_settings('siteurl').'/', str_replace('\\', '/', dirname(__FILE__))) . "/flags/flag_$imagecode.png";
			else
				$image = get_settings('siteurl')."/wp-includes/images/flags/flag_$imagecode.png";
			
			echo '<a class="noicon" title="Translate this site: '.$langnames[$langcode].'" style="margin-left:1px; margin-right:1px;" href="'.$full_url.'"><img src="'.$image.'" alt="'.$langnames[$langcode].'" /></a>';
	}

		
	if (function_exists('register_sidebar_widget'))
	{
		register_sidebar_widget('ltranslate', 'widget_ltranslate');
		register_widget_control('ltranslate', 'widget_ltranslate_control', 250, 300);
	}
}

add_action('plugins_loaded', 'widget_ltranslate_init');
add_action('wp_head', 'widget_ltranslate_noframes');

?>