<?php

//save payment
global $wpdb;
$query = $wpdb->prepare( "insert into property_payment(propertyid, listingfeepaid,upgradelisting,datepaid,reference) values (%d,'%s','%s',now(),'%s')", $_REQUEST["propertyid"], $_REQUEST["paylistingtype"],$_REQUEST["payupgradetype"],$authorizeID );
$wpdb->query( $query );

foreach ($_REQUEST as $k=>$v) 
{
   $message .= $k . ":" . $v .", ";
} 
$subject = "paypal transaction";

//mail( "ian@agentpoint.com.au", $subject, $message ,$header);

//update property based on the payment
switch($_REQUEST["paylistingtype"])
{
	case "oneoff":
		$query = $wpdb->prepare( "update properties set active = 1,listingfeepaid=1, active_until = '0000-00-00'	where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "1month":
		$query = $wpdb->prepare( "update properties set active = 1,listingfeepaid=1, active_until =  DATE_ADD( NOW( ) , INTERVAL 1 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "3month":
		$query = $wpdb->prepare( "update properties set active = 1,listingfeepaid=1, active_until =  DATE_ADD( NOW( ) , INTERVAL 3 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "6month":
		$query = $wpdb->prepare( "update properties set active = 1,listingfeepaid=1, active_until =  DATE_ADD( NOW( ) , INTERVAL 6 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "12month":
		$query = $wpdb->prepare( "update properties set active = 1,listingfeepaid=1, active_until =  DATE_ADD( NOW( ) , INTERVAL 12 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
}

switch($_REQUEST["payupgradetype"])
{
	case "oneoff":
		$query = $wpdb->prepare( "update properties set active = 1,upgradelistingpaid=1, featured_until = '0000-00-00'	where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "1month":
		$query = $wpdb->prepare( "update properties set active = 1, upgradelistingpaid=1,featured_until =  DATE_ADD( NOW( ) , INTERVAL 1 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "3month":
		$query = $wpdb->prepare( "update properties set active = 1,upgradelistingpaid=1, featured_until =  DATE_ADD( NOW( ) , INTERVAL 3 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "6month":
		$query = $wpdb->prepare( "update properties set active = 1,upgradelistingpaid=1, featured_until =  DATE_ADD( NOW( ) , INTERVAL 6 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
	case "12month":
		$query = $wpdb->prepare( "update properties set active = 1,upgradelistingpaid=1, featured_until =  DATE_ADD( NOW( ) , INTERVAL 12 
MONTH ) where id = %d",$_REQUEST["propertyid"]);
		$wpdb->query( $query );
		break;
}


//update featured property
if($_REQUEST["payupgradetype"] == "oneoff" || $_REQUEST["payupgradetype"] == "1month" || $_REQUEST["payupgradetype"] == "3month" || $_REQUEST["payupgradetype"] == "6month" || $_REQUEST["payupgradetype"] == "12month" )
{
	global $wpdb;
	$blogname = $wpdb->get_var("select blog_name from properties where id = ".$_REQUEST["propertyid"]);
	$blogid= $wpdb->get_var("select blog_id from wp_blogs where domain like '%".$blogname .".reallistings.com.au%'");
	
	$ifexist = $wpdb->get_var("select id from featured_listings where blog_id = ".$blogid);
	if($ifexist == "")
	{
		//does not exist
		$query = $wpdb->prepare( "insert into featured_listings(blog_id,featured_listings_ids) values (%d,'%s')", $blogid, $_REQUEST["propertyid"]);
	}
	else
	{
		//exist and get featured listing
		$wpdb->query("update featured_listings set featured_listings_ids = CONCAT(featured_listings_ids,',".$_REQUEST["propertyid"]."') where id=".$ifexist);
		
	}
}
	//update featured property in realty_featured_listings
?>