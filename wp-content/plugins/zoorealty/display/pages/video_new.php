<?php
$condition=$_GET;
$condition['limit']='100';
$condition['display_agent']='1';
$condition['video_type']=(!isset($_GET['video_type']))?get_option( 'video_type' ):$_GET['video_type'];
global $helper, $post;

$video_types=array('sold_property_videos','current_property_videos','agent_profile_videos','other_videos');

if(is_null($listings))$listings = $this->properties_video($condition);
$queryString = "?" .  preg_replace('/(&?status=)([\w]+&?)/is', '', urldecode($_SERVER['QUERY_STRING']));

$featured_ids=get_option('video_featured_ids');
if(!empty($featured_ids)){
	$featured_id=implode(",",$featured_ids);
	$featured_listings = $this->properties_video(array("condition"=>"property_id IN ($featured_id)", 'agent_ids'=>$featured_id, 'admin_ids'=>$featured_ids,'display_agent'=>'1'));
}
$url=get_permalink($post->ID);
if($listings['results_count'] < 1){ ?>
<p class="no_properties_error">We are sorry, no video were found to match this criteria. Click <a href="<?php echo $url; ?>">here</a> to see all videos.</p>
<?php 
} 
else{ 

?>
	<link rel="stylesheet" href="<?php echo $this->paths['theme']; ?>on-demand/video.css" type="text/css" media="screen" />
	<script type='text/javascript' src='<?php echo $this->paths['theme']; ?>on-demand/jquery.easing.js?ver=3.1.2'></script>
	<script type='text/javascript' src='<?php echo $this->paths['theme']; ?>on-demand/jquery.coda.js?ver=3.1.2'></script>
	<script type='text/javascript' src='<?php echo $this->paths['theme']; ?>on-demand/jquery.equal.js?ver=3.1.2'></script>
	<script type='text/javascript' src='<?php echo $this->paths['theme']; ?>on-demand/fancybox/jquery.fancybox.js?ver=3.1.2'></script>
	<?php if($featured_listings['results_count'] > 0 ){ ?>
	<div id="featured-content">
		<div id="featured-posts">
			<div id="coda-nav">
				<div class="coda-slider" id="slider">
					<?php 
					foreach($featured_listings as $listing):
					if(!is_array($listing)) continue; 					
					?>
					<div class="panel">
						<div class="featured-post">
							
							<a class="inline" href="#video-<?php echo $listing['id']; ?>" title="<?php echo $listing['headline']; ?>">
								<img width="560" height="315" src="<?php echo $listing['large']; ?>" class="attachment-featured wp-post-image"/>									
							</a>						
							<div class="instant">
								<div id="video-<?php echo $listing['id']; ?>" class="instant-view">
									<div class='video-embed'><?php display_video($listing);?></div>							
								</div>
							</div>	
							
							<?php /* display directly
							<div style="float:left;"><?php display_video($listing,'550','305');?></div>*/ ?>	
							
							<div class="featured-post-description">
								<h2><a href="headline" title="<?php echo $listing['url']; ?>"><?php echo $listing['headline']; ?></a></h2>								
								<?php if($listing['admin']!='1'){ ?>
									<p><?php echo $helper->_substr($listing['description'],300); ?> &#8230;</p>
									<a class="continue" href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>">Continue Reading</a>
								<?php } else { ?>
									<p><?php echo $listing['description']; ?></p>
								<?php } ?>
							</div>
						</div><!-- featured-post -->
					</div> <!-- panel -->		
					<?php endforeach; ?>
				</div> <!-- coda-slider -->
			</div> <!-- coda-nav -->
		</div> <!-- featured-posts -->
	</div> <!-- featured-content -->
	<?php } ?>
	
	<div id="post-container">
		<div id="posts">
			<?php if(!empty($condition['video_type']))echo '<h2 class="video_title">'.$helper->humanize($condition['video_type']).'</h2>'; ?>
            <div class="video_type_link">
                <ul>
                    <li><a href="<?php echo $url; ?>?video_type=" <?php if($condition['video_type']=='')echo 'class="current"'; ?>>All Videos</a></li>
					<?php foreach($video_types as $video_type){ ?>
                    <li><a href="<?php echo $url; ?>?video_type=<?php echo $video_type; ?>" <?php if($condition['video_type']==$video_type)echo 'class="current"'; ?>><?php echo $helper->humanize($video_type); ?></a></li>
                    <?php } ?>
                </ul>
            </div>
			<?php 
			foreach($listings as $listing):
			if(!is_array($listing)) continue; 					
			if(!empty($listing['youtube_id']) && $listing['video_status']==NULL){	
				$image_url=str_replace("youtube_id",$listing['youtube_id'],"http://img.youtube.com/vi/youtube_id/2.jpg");
			}
			else $image_url=$this->paths['theme'].'on-demand/images/place_tv.jpg';
			$listing['street_suburb']=($listing['street_address']=='')?$listing['suburb']:$listing['street_address'].', '.$listing['suburb'];
			?>
			<div id="post-<?php echo $listing['id'];?>" class="post-<?php echo $listing['id'];?> post type-post status-publish format-standard hentry category-more-videos category-sample-videos gallery-item">			
				<div class="post-thumbnail">
					<a class="thumbnail-frame-video inline" href="#video-<?php echo $listing['id'];?>" title="<?php echo $listing['headline'];?>"><!-- nothing to see here --></a>
						<img width="180" height="130" src="<?php echo $listing['thumb'];?>" class="attachment-post-thumbnail wp-post-image"/>	
				</div>
				
				<?php if(is_array($featured_ids) && !in_array($listing['id'],$featured_ids)){ ?>
				<div class="instant">
					<div id="video-<?php echo $listing['id'];?>" class="instant-view">
						<div class='video-embed'>
							<div class='video-embed'><?php display_video($listing);?></div>					
						</div>		
					</div>
				</div>			
				<?php } ?>
				
				<?php if($listing['admin']=='1' && !empty($listing['headline'])){ ?>
				<h2><?php echo $listing['headline'];?></h2>
				<?php } ?>
				
				<?php if($listing['properties']=='1'){ ?>
				<h2><a href="<?php echo $listing['url'];?>" title="<?php echo $listing['headline'];?>"><?php echo $listing['street_suburb'];?></a></h2>
				<ul class="post-meta">
					<li class="headline"><?php echo $listing['headline'];?></li>					
					<li class="price">
						<?php 
						if (!$listing["is_auction"] && $listing["status"]!='2'): echo $listing['price'];
						elseif($listing["status"]=='2'): echo ($listing['display_sold_price'])? $listing['last_price']:'Undisclosed'; 
						else :
							echo "Auction: ";
							if(!empty($listing["auction_place"]))echo $listing["auction_place"].", ";							
							echo $listing["auction_date"]; 
							if(!empty($listing["auction_time"]))echo " at ".$listing["auction_time"];
						endif; 
						?>					
					</li>					
				</ul>
				<?php } ?>
				
				<?php if($listing['agent']=='1'){ ?>
				<h2><a href="<?php echo $listing['url'];?>" title="<?php echo $listing['headline'];?>"><?php echo $listing['headline'];?></a></h2>
				<ul class="post-meta">
					<li class="headline"><?php echo $listing['position_for_display'];?></li>		
				</ul>
				<?php } ?>
			</div> <!-- gallery-item -->		
			<?php endforeach; ?>
		</div> <!-- posts -->		
		<div id="sidebar">
			<?php dynamic_sidebar('Video Sidebar'); ?>
		</div>
	</div> <!-- post-container -->
			
<?php }
function display_video($listing, $width='490', $height='275' ){
	global $realty;
	$iframe_width=$width+10;
	$iframe_height=$height+10;
	if(!empty($listing['embed_code'])){
		$embed_code=$listing['embed_code'];
		if(strpos($embed_code,'width="')!==false){
			$cut_off=strpos($embed_code,'width="')+7;
			$start_part=substr($embed_code,0,$cut_off);
			$end_part=substr($embed_code,$cut_off);
			$cut_off=strpos($end_part,'"');			
			$embed_code=$start_part.$width.substr($end_part,$cut_off); // combine start part , new width and end part
		}
		if(strpos($embed_code,'height="')!==false){
			$cut_off=strpos($embed_code,'height="')+8;
			$start_part=substr($embed_code,0,$cut_off);
			$end_part=substr($embed_code,$cut_off);
			$cut_off=strpos($end_part,'"');
			
			$embed_code=$start_part.$height.substr($end_part,$cut_off); // combine start part , new width and end part
		}
		echo $embed_code ;
	}
	if(!empty($listing['youtube_id']) && $listing['video_status']==NULL){
	?>
	 <object width="<?php echo $width; ?>" height="<?php echo $height; ?>">
		<param name="movie" value="<?php echo str_replace("youtube_id",$listing['youtube_id'],"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>"></param>
		<param name="allowFullScreen" value="true"></param>
		<param name="allowscriptaccess" value="always"></param>
		<param name="wmode" value="transparent"></param>
		<embed src="<?php echo str_replace("youtube_id",$listing['youtube_id'],"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $width; ?>" height="<?php echo $height; ?>" wmode="transparent"></embed>
	</object>
	<?php 	 }	
	if(!empty($listing['youtube_id']) && $listing['video_status']=='0'){ ?>
		<iframe width="<?php echo $iframe_width; ?>" height="<?php echo $iframe_height; ?>" scrolling="no" frameborder="0" src="<?php echo $realty->pluginUrl."display/elements/video.php?width=$width&height=$height&property_id=".$listing['id']; ?>">Your browser does not support inline frames or is currently configured not to display inline frames.</iframe>
		<?php
	}
	if (!empty($listing['video_url'])){ // agent video ?>
		<iframe width="500" height="285" scrolling="no" frameborder="0"  src="<?php echo $realty->pluginUrl."display/elements/video.php?width=$width&height=$height&user_id=".$listing['id']; ?>">Your browser does not support inline frames or is currently configured not to display inline frames.</iframe>
	<?php }
	if (!empty($listing['video_embed'])){ // agent video ?>
	<object width="<?php echo $width; ?>" height="<?php echo $height; ?>">
		<param name="movie" value="<?php echo str_replace("v=","v/",str_replace("watch_popup?","",$listing['video_embed'])); ?>"></param>
		<param name="allowFullScreen" value="true"></param>
		<param name="allowscriptaccess" value="always"></param>
		<param name="wmode" value="transparent"></param>
		<embed src="<?php echo str_replace("v=","v/",str_replace("watch_popup?","",$listing['video_embed'])); ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $width; ?>" height="<?php echo $height; ?>" wmode="transparent"></embed>
	</object>
	<?php } 
}
?>