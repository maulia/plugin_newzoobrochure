<script type="text/javascript">
// <![CDATA[
function open_query_form(element){
	if(element.value != 'sale' && element.value != 'lease')
		return;
	if(element.checked==1)
		document.getElementById(element.value + '_search').style.display='block';
	else
		document.getElementById(element.value + '_search').style.display='none';
}
// ]]>
</script>
<?php if (!empty($content))echo "$content"; ?>
<?php if (!empty($return)) echo "<div id='return'>$return</div>"; ?>

<?php if($_GET['action']=='forgot_password'): ?>
<p class="login"><a href="?action=login" title="Log In">Log In</a></p>
<p style="padding: 0 0 9px;">Enter your e-mail address and we will send you a new password.</p>
<form method="post" name="forgot_password" action="">
	<input type="hidden" name="task" value="forgot_password" />
	<label>E-mail<br /><input type="text" name="email" class="searchbox" style="margin: 0 0 18px;" value="<?php echo $subscriber['email']; ?>" size="45" onClick="if(this.value=='enter email address') this.value='';" /></label>
	<div class="clearer"></div>

	<div class="buttons signup_button">
		<p class="email_btn"><a class="btn" href="javascript:document.forgot_password.submit();">Submit</a></p>
	</div>
</form>

<?php elseif(!$this->is_logged_in()): ?>
<form method="post" name="login_form_email_subscription" action="" class="contact_form">
	<input type="hidden" name="task" value="login" />
	<ol class="cf-ol">
		<li><label><span>E-mail</span></label><input type="text" name="email" class="searchbox" size="45" value="<?php echo $subscriber['email']; ?>" onClick="if(this.value=='enter email address') this.value='';" /></li>
		<?php if (get_option('is_use_password')=='1'): ?>
		<li><label><span>Password</span></label><input type="password" name="password" class="searchbox"  size="45" /></li>
		<?php endif ; ?>
	</ol>
	<div class="buttons signup_button">
		<p class="button email_btn"><a class="btn" href="javascript:document.login_form_email_subscription.submit();">Submit</a></p>
	</div>
</form>

<?php if (get_option('is_use_password')=='1'): ?>
<p class="forgot_password_link" style="margin: 18px 0 0;">Forgot your password? <a href="?action=forgot_password" title="Forgot your password?">Click here.</a></p>
<?php endif ; ?>

<?php else: ?>
<h3>Your Subscriptions Details</h3>
<form method="post" action="" name="form_email_subscription" class="contact_form form_email_subscription">
	<input type="hidden" name="subscriber_id" value="<?php echo $subscriber['subscriber_id']; ?>" />
	<input type="hidden" name="task" value="register_or_edit" />

	<div id="user_management_page">	
		<div id="manage_profile">
			<ol class="cf-ol">
			<?php if(get_option('subscribe_title')=='1') : ?>
			<li><label><span>Title</span></label>
			<select name="title">
				<option value="">Select</option>		
				<option value="Miss"<?php if($subscriber['title']=='Miss') echo ' selected="selected"'; ?>>Miss</option>
				<option value="Mr"<?php if($subscriber['title']=='Mr') echo ' selected="selected"'; ?>>Mr</option>
				<option value="Mrs"<?php if($subscriber['title']=='Mrs') echo ' selected="selected"'; ?>>Mrs</option>
				<option value="Ms"<?php if($subscriber['title']=='Ms') echo ' selected="selected"'; ?>>Ms</option>
				<option value="Dr"<?php if($subscriber['title']=='Dr') echo ' selected="selected"'; ?>>Dr</option>
			</select></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_first_name')=='1') : ?>
			<li><label><span>First Name *</span></label><input type="text" name="first_name" class="searchbox" size="45" value="<?php echo $subscriber['first_name']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_last_name')=='1') : ?>
			<li><label><span>Last Name *</span></label><input type="text" name="last_name" class="searchbox" size="45" value="<?php echo $subscriber['last_name']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_email')=='1') : ?>
			<li><label><span>E-mail *</span></label><input type="text" name="email" class="searchbox" size="45" value="<?php echo $subscriber['email']; ?>" onClick="if(this.value=='enter email address') this.value='';" /></li>
			<?php endif; ?>
			
			<?php if (get_option('is_use_password')=='1'): ?>
			<li><label><span>Password</span></label><input type="password" name="password" class="searchbox"  size="45" value=""/></li>
			<li><label><span>Verify Password</span></label><input type="password" name="verify_password" class="searchbox" value="" /></li>
			<?php endif ; ?>

			<?php if(get_option('subscribe_home_phone')=='1') : ?>	
			<li><label><span>Home Number</span></label><input type="text" name="home_phone" class="searchbox" size="45" value="<?php echo $subscriber['home_phone']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_mobile_phone')=='1') : ?>
			<li><label><span>Mobile Number</span></label><input type="text" name="mobile_phone" class="searchbox" size="45" value="<?php echo $subscriber['mobile_phone']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_work_phone')=='1') : ?>
			<li><label><span>Work Number</span></label><input type="text" name="work_phone" class="searchbox" size="45" value="<?php echo $subscriber['work_phone']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_fax')=='1') : ?>
			<li><label><span>Fax Number</span></label><input type="text" name="fax" class="searchbox" size="45" value="<?php echo $subscriber['fax']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_country')=='1') : ?>			
				<li><label><span>Country</span></label>
				<select id="country" name="country">
					<option value="Australia">Australia</option>
					<?php foreach ($list_countries as $item): ?>	
						<option value="<?php echo $item; ?>" <?php if($item == $subscriber['country']) echo 'selected="selected"'; ?>><?php echo $item; ?></option>
					<?php endforeach; ?>
					</select></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_referrer')=='1') : ?>	
				<li><label><span>How did you find us?</span></label>
				<select name="referrer">
					<option value="">Please select</option>
				<?php foreach($all_list as $item_value): ?>
					<option value="<?php echo $item_value['name']; ?>"<?php if ($subscriber['referrer'] == $item_value['name']) echo 'selected="selected"'; ?>><?php echo $item_value['name']; ?></option>
				<?php endforeach; ?>
				</select></li>
			<?php endif; ?>
			
			
			<li class="site_alerts">
			<?php if(get_option('subscribe_property_alert')=='1') : ?>	
			<label><span>Property Alerts</span></label>
				<ul class="prop_alerts_ul">
				<?php $property_alert=array();$property_alert=array_diff($list_alerts, $news_alerts);?>
				<?php foreach($property_alert as $key=>$item_value):?>
					<li><input type="checkbox" class="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?> onclick="open_query_form(this)" /> <?php echo $item_value; ?></li>
					<?php echo $this->get_form_query_string($key);
			endforeach; ?>
				</ul>
			<div class="clearer"></div>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_news_alert')=='1') : ?>	
			<label><span>News Alerts</span></label>
				<ul class="news_alerts_ul">
				<?php $news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);?>
				<?php foreach($news_alert as $key=>$item_value):?>
					<li><input type="checkbox" class="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?>><?php echo $item_value; ?></li>
				<?php endforeach; ?>
				</ul>
			<div class="clearer"></div>
			<?php endif; ?>
			</li>
				
			<?php if(get_option('subscribe_property_address')=='1') : ?>
			<li><label><span>Property Address</span></label><input type="text" name="property_address" class="searchbox" size="45" value="<?php echo $subscriber['property_address']; ?>" /></li>
			<?php endif; ?>
			
			<?php if(get_option('subscribe_comments')=='1') : ?>
			<li><label><span>Comments</span></label><textarea name="comments" cols="42"><?php echo $subscriber['comments']; ?></textarea></li>
			<?php endif; ?>
			
			<li class="li--unsubscribe"><input type="checkbox" class="checkbox" name="unsubscribe" onclick="if(confirm('You are about to delete your account.\n\'OK\' to delete, \'Cancel\' to stop.' )) {document.form_email_subscription.submit();}" value="unsubscribe" /> Unsubscribe</li>
			</ol>
		</div><!-- end #manage_profile-->
	</div><!-- end #user_management_page-->
	<input type="hidden" name="user_type" value="subscribe">
	<div class="clearer"></div>
	<div class="buttons signup_button">
	   <p class="button email_btn"><input class="btn" type="submit" value="Submit">
	   <a class="button" href="?action=logout" title="Log Out">Log Out</a></p>
	</div>
</form>
<?php endif; //$this->is_logged_in()) ?>