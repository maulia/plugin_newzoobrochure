<?php $listings = $params;  global $realty, $helper; ?>
<div id="list_format">
<table class="property" width="100%" border="0" cellspacing="0" cellpadding="0">
<?php			
foreach($listings as $listing):
	if(!is_array($listing)) continue; //It will stop in the last index of the sql result because it will be the number of rows returned			
	$printedTd = false;
	$count = 0;
	?>
	<tr <?php echo ($alt_class = ($alt_class==' class="alt"') ? '':' class="alt"'); ?>>
		<td class="listCont">
    	<div class="listWrap">
		<?php
		 if(is_array($realty->settings['search_results'])) 
			foreach($realty->settings['search_results'] as $key=>$values):
				if($key =='thumbnail'):					
					if( $count > 0 ):
						echo '</div>';
						$printedTd = false;
					endif;
				?>
					<div class="image">
				<?php
					elseif($key !='thumbnail' and !$printedTd):
				?>	
					<div class="tdContent">
				<?php 
					$printedTd = true;
				endif;	
				switch($key):
					case 'thumbnail': 
					if(!empty($listing['embed_code'])){
						$embed_code=$listing['embed_code'];
						
						$cut_off=strpos($embed_code,'width="')+7;
						$start_part=substr($embed_code,0,$cut_off);
						$end_part=substr($embed_code,$cut_off);
						$cut_off=strpos($end_part,'"');
						
						$embed_code=$start_part.'490'.substr($end_part,$cut_off); // combine start part , new width and end part
						
						echo $embed_code ;
					}
					if(!empty($listing['youtube_id']) && $listing['video_status']==NULL){
					?>
					 <object width="490" height="265">
						<param name="movie" value="<?php echo str_replace("youtube_id",$listing['youtube_id'],"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>"></param>
						<param name="allowFullScreen" value="true"></param>
						<param name="allowscriptaccess" value="always"></param>
                        <param name="wmode" value="transparent"></param>
						<embed src="<?php echo str_replace("youtube_id",$listing['youtube_id'],"http://www.youtube.com/v/youtube_id?hl=en&fs=1"); ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="490" height="265" wmode="transparent"></embed>
					</object>
					 <?php 	 }	
					 if(!empty($listing['youtube_id']) && $listing['video_status']=='0'){ ?>							
							<script type="text/javascript">
							AC_FL_RunContent( 'codebase','http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0','width','490','height','265','id','exhibition-stand','src','http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer','flashvars','file=<?php echo $listing['youtube_id']; ?>&amp;image=<?php echo $listing['large']; ?>','quality','high','bgcolor','#1E1E20','name','exhibition-stand','pluginspage','http://www.macromedia.com/go/getflashplayer','allowscriptaccess','always','allowfullscreen','true','movie','http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer' ); //end AC code
							</script>
							<noscript>
							  <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="350" height="270" id="exhibition-stand">
								<param name="movie" value="http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer.swf" />
								<param name="quality" value="high" />
								<param name="bgcolor" value="#1E1E20" />
								<param name="allowscriptaccess" value="always" />
								<param name="allowfullscreen" value="true" />
								<param name="FlashVars" value="file=<?php echo $listing['youtube_id']; ?>&amp;image=<?php echo $listing['large']; ?>" />
                                <param name="wmode" value="transparent"></param>
								<embed src="http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer.swf" width="490" height="265" FlashVars="file=<?php echo $listing['youtube_id']; ?>&amp;image=<?php echo $listing['large']; ?>" quality="high" bgcolor="#1E1E20" name="exhibition-stand"  type="application/x-shockwave-flash"  pluginspage="http://www.macromedia.com/go/getflashplayer" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" /></embed>
							  </object>
							</noscript>
						<?php
					}
					if (!empty($listing['video_url'])){ // agent video ?>
						<script type="text/javascript">
							AC_FL_RunContent( 'codebase','http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0','width','490','height','265','id','exhibition-stand','src','http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer','flashvars','file=<?php echo $listing['video_url']; ?>&amp;image=<?php echo $listing['large']; ?>','quality','high','bgcolor','#1E1E20','name','exhibition-stand','pluginspage','http://www.macromedia.com/go/getflashplayer','allowscriptaccess','always','allowfullscreen','true','movie','http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer' ); //end AC code
							</script>
							<noscript>
							  <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="350" height="270" id="exhibition-stand">
								<param name="movie" value="http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer.swf" />
								<param name="quality" value="high" />
								<param name="bgcolor" value="#1E1E20" />
								<param name="allowscriptaccess" value="always" />
								<param name="allowfullscreen" value="true" />
								<param name="FlashVars" value="file=<?php echo $listing['video_url']; ?>&amp;image=<?php echo $listing['large']; ?>" />
                                <param name="wmode" value="transparent"></param>
								<embed src="http://wpc.023B.edgecastcdn.net/80023B/lightboxfilms.com.au/website/mediaplayer/mediaplayer.swf" width="490" height="265" FlashVars="file=<?php echo $listing['video_url']; ?>&amp;image=<?php echo $listing['large']; ?>" quality="high" bgcolor="#1E1E20" name="exhibition-stand"  type="application/x-shockwave-flash"  pluginspage="http://www.macromedia.com/go/getflashplayer" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" /></embed>
							  </object>
						</noscript>
					<?php }
					if (!empty($listing['video_embed'])){ // agent video ?>
					<object width="490" height="265">
						<param name="movie" value="<?php echo str_replace("v=","v/",str_replace("watch_popup?","",$listing['video_embed'])); ?>"></param>
						<param name="allowFullScreen" value="true"></param>
						<param name="allowscriptaccess" value="always"></param>
						<param name="wmode" value="transparent"></param>
						<embed src="<?php echo str_replace("v=","v/",str_replace("watch_popup?","",$listing['video_embed'])); ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="490" height="265" wmode="transparent"></embed>
					</object>
					<?php }
					break;
					case 'address':
						if(is_array($values)) foreach($values as $addressPart)${$addressPart} = $listing[$addressPart];
						$street_address=ucwords(strtolower($street_address));
						$address='';
						if(!empty($street_address))$address=$street_address.", ";
						if(!empty($suburb))$address.=$suburb.", ";
						if(!empty($state))$address.=$state.", "; ?>
						<p class="street-address"><a href="<?php echo $listing['url']; ?>" title="<?php echo $listing['headline']; ?>"><?php echo substr($address,0,-2) ;?></a></p>						<?php
					break;
					case 'description':
					if($listing['admin']!='1'){ ?>
					<p class="description"><?php echo $helper->_substr($listing['description'], $values); ?>&hellip;<a href="<?php echo $listing['url']; ?>" title="View Property">More &raquo;</a></p>
					<?php }
					else { ?>
					<p class="description"><?php echo $listing['description']; ?></p>
					<?php }
					break;
					case 'building_size':
						if(is_array($listing[$key]) and is_array($values) and !empty($listing[$key]) and $listing['is_for_sale']):?>
						<div class="land_building_size">
							<p>Building Size: 
							<?php foreach($values as $unit): ?>	<span class="building_size"><?php echo str_replace('Sqms', 'm2 (approx.)',$listing[$key][$unit]); ?></span>
							<?php endforeach;?>
							</p>
						</div>
						<?php
						endif;						
					break;
					case 'land_size':						
						if(is_array($listing[$key]) and is_array($values) and !empty($listing[$key]) and $listing['is_for_sale']):?>
						<div class="land_building_size">
							<p>Land Size: 
							<?php foreach($values as $unit): ?>	<span class="land_size"><?php echo str_replace('Sqms', 'm2 (approx.)',$listing[$key][$unit]); ?></span>
							<?php endforeach;?>
							</p>
						</div>
						<?php
						endif;						
					break;
					case 'current_rent':?>
						<p class="current_rent">Current Rent : <?php echo '$'.number_format($listing['current_rent']); ?></p>
					<?php
					break;
					case 'auction':
						if (!empty($listing[$key]) && $listing[$key]!='00:00:00'):	?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php
						endif;
					break;
					case 'price':
						if (!$listing["is_auction"] && $listing["status"]!='2'):?>
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
						<?php elseif($listing["status"]=='2'): ?>
							<p class="<?php echo $key; ?>"><?php echo ($listing['display_sold_price'])? $listing['last_price']:'Undisclosed'; ?></p>
						<?php else : ?>
							<p class="<?php echo $key; ?>">Auction: <?php 
								if(!empty($listing["auction_place"]))echo $listing["auction_place"].", ";							
								echo $listing["auction_date"]; 
								if(!empty($listing["auction_time"]))echo " at ".$listing["auction_time"]; ?>
							</p>
						<?php endif;
					break;
					case 'action_buttons': ?>
						<div class="more-buttons">
							<ul class="action-buttons">
								<?php 
								foreach($values as $param){
									switch($param){
										case 'photos':
											$photo = $realty->photos($listing['id'], "photos", array('800x600'));
											if(!empty($photo)){
												$photos = array();
												$j='0';$k='0';$l='0';
												for ($i=0;$i<count($photo);$i++){
													if($photo[$i]['small']!=""){ $photos[$j]['small']=$photo[$i]['small']; $j++; }
													if($photo[$i]['medium']!=""){ $photos[$k]['medium']=$photo[$i]['medium']; $k++; }
													if($photo[$i]['large']!=""){ $photos[$l]['large']=$photo[$i]['large']; $l++; }
												} 
												 ?>
												<li class="button photo_btn">
													<a class="link-lightbox view_photos" href="<?php echo $photos[0]['large']; ?>" onclick="return false;" rel="lightbox[photo_gallery<?php echo $listing['id']?>]" title="View SLIDE SHOW">SLIDE SHOW</a>
													<?php for ($i=1;$i<count($photos);$i++){ ?>
													<a href="<?php echo $photos[$i]['large']; ?>" onclick="return false;" rel="lightbox[photo_gallery<?php echo $listing['id']?>]"></a>
													<?php } ?>
												</li>				
											<?php
											}
										break;
										case 'floorplans':
											$floorplans = $realty->photos($listing['id'], "floorplans", array('800x600'));	
											if(!empty($floorplans)){
												$floorplan = array();
												$j='0';$k='0';$l='0';
												for ($i=0;$i<count($floorplans);$i++){
													if($floorplans[$i]['small']!=""){ $floorplan[$j]['small']=$floorplans[$i]['small']; $j++; }
													if($floorplans[$i]['medium']!=""){ $floorplan[$k]['medium']=$floorplans[$i]['medium']; $k++; }
													if($floorplans[$i]['large']!=""){ $floorplan[$l]['large']=$floorplans[$i]['large']; $l++; }
												}
												 ?>
												<li class="button floorplan">
													<a class="link-lightbox view_photos" href="<?php echo $floorplan[0]['large']; ?>" onclick="return false;" rel="lightbox[floorplan<?php echo $listing['id']?>]" title="View FLOOR PLAN">FLOOR PLAN</a>
													<?php for ($i=1;$i<count($floorplan);$i++){ ?>
													<a href="<?php echo $floorplan[$i]['large']; ?>" onclick="return false;" rel="lightbox[floorplan<?php echo $listing['id']?>]"></a>
													<?php } ?>
												</li>					
											<?php
											}
										break;
										case 'map':
											if(!empty($listing['latitude']) && !empty($listing['longitude'])){ ?>								
												<li class="button map">
													<a class="link-lightbox" href="#" onclick="openbox('Map', 'filter_map', 'box_map', 'boxtitle_map', 'load_form_map', '<?php echo $realty->pluginUrl.'display/elements/lightbox_map.php?property_id='.$listing['id']; ?>')" title="MAP">MAP</a>
												</li>								
											<?php }
										break;																				
									}
								} ?>
							</ul>
						</div>
					<?php
					break;
					case 'number_of_rooms': 
						if ( !$listing['is_land'] and is_array($values) and !empty($values)): 
						?>
						
						<div class="rooms-wrap">							
							<ul class="rooms">
							<?php  
							foreach($values as $param):
								if(!empty($listing[$param])): ?>
								<li class="<?php echo $param; ?>">
									<span class="room_count"><?php echo $listing[$param]; ?></span> <span class="room_type"><?php //echo $param; ?></span>
								</li>
								<?php
								endif;
							endforeach; ?>
							</ul>
						</div>
						<?php 
						endif;	
					break;
					case 'primary_contact': 
						$user = $realty->user($listing['user_id']);
						if(!empty($user)):?>
						<p>Contact</p>
						<?php
							foreach($values as $param):
								if(!empty($user[$param])): ?>
								<p>
								<span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $user[$param]; ?>"><?php echo $user[$param]; ?></a>
								<?php else: ?>
								<?php echo $user[$param]; ?>
								<?php endif; ?>
								</span>
								</p>
								<?php
								endif;
							endforeach;
						endif;						
					break;
					case 'opentimes':
						$opentimes = $this->opentimes($listing['id']); 
						if(!empty($opentimes)) :?>	
						<p><span>Open</span> Times</p>
						<ul>
						<?php foreach($opentimes as $opentime): ?>
							<li><span class="open_date"><strong>
							<?php echo date('l, j M Y', strtotime($opentime['date'])); ?></strong></span><br />
							<span class="open_time"><?php echo $opentime['start_time']; ?> - <?php echo $opentime['end_time']; ?></span></li>
						<?php endforeach; ?>
						</ul>
						<?php
						endif;
					break;
					case 'available_at':
						if(!empty($listing[$key]) && $listing[$key]!='0000-00-00'):	?>	
							<p>Available on: </p>						
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
						endif;					
					break;	
					case 'secondary_contact':
						if(!empty($listing['secondary_user']) && $listing['secondary_user']!= $listing['user_id'])$secondary_user = $this->user($listing['secondary_user']);
						if(!empty($secondary_user)):
							foreach($values as $param):
								if(!empty($user[$param])):?>
								<p><span class="<?php echo $param ?>Label"><?php echo $helper->humanize($param); ?>:</span> <span class="<?php echo $param; ?>Value">
								<?php if($param=='email'): ?><a href="mailto:<?php echo $secondary_user[$param]; ?>"><?php echo $secondary_user[$param]; ?></a>
								<?php else: ?>
								<?php echo $secondary_user[$param]; ?>
								<?php endif; ?>
								</span>
								</p>
								<?php
								endif;
							endforeach;
						endif;					
					break;							
					default:
						if(!empty($listing[$key])):		?>							
							<p class="<?php echo $key; ?>"><?php echo $listing[$key]; ?></p>
							<?php
						endif;
					break;
				endswitch;
				if($key =='thumbnail')
					echo '</div><div class="clear"></div><!--ends .listWrap--></td>';
				++$count;
			endforeach; 
			if($key !='thumbnail') ?>
				</div><!--ends .tdContent-->
            
</td>
</tr>
<?php endforeach; ?>	
</table>
</div>