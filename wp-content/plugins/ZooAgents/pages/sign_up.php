<h2>Set up an Account</h2>
<?php
global $wpdb, $realty;
$private_listing = get_option('private_listing');
if($private_listing!=1)return;

if(isset($_POST['submit'])){
	extract ($_POST);	
	if(empty($firstname))$error.="Please fill your first name<br/>";
	if(empty($lastname))$error.="Please fill your last name<br/>";
	if(empty($user_email))$error.="Please fill your email<br/>";
	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $user_email))$error.="Invalid email format.<br/>";
	if(empty($password))$error.="Please fill your password<br/>";
	if(empty($verify_password))$error.="Please verify your password<br/>";
	if($password != $verify_password)$error.="Password not match. Please verify your password.<br/>";
	if($_SESSION['security_code'] != $_POST['securitycode'] && !empty($_SESSION['security_code'] ))$error.="Wrong spam code<br/>";
	if( $_SESSION['security_code'] == $_POST['securitycode'] && !empty($_SESSION['security_code'] ) )unset($_SESSION['security_code']);
	if(!empty($error))echo "<p class='error'>$error</p>";
	
	if(empty($error)){		
		$check_email=$wpdb->get_var("select id from agent_account where email='$user_email'");
		if($check_email)$error.="Email already exist.<br/>";
		
		if(!empty($error))echo "<p class='error'>$error</p>";
		else{
			$_POST['email'] = $user_email;	
			$_POST['ip_address']=$ip_address=esc_sql ($_SERVER['REMOTE_ADDR']);
			$_POST['created_at'] =  date("Y-m-d h:i:s");
			$this->insert_form( 'agent_account', $_POST );	
			
			$id=$wpdb->get_var("select id from agent_account where email='$user_email'");	
			if($id){
				$office_id=$wpdb->get_var("select max(id) from office");
				$_POST['office_id']=$_POST['id']=$office_id=($office_id<100000)?100000:$office_id+1;
				$_POST['name']=trim($firstname." ".$lastname);
				$_POST['email'] = $user_email;		
				$_POST['contributor']=1;
				$_POST['is_zoo']=0;
				$_POST['display']=1;
				$_POST['wp_user_id']=$id;
				
				$this->insert_form( 'office', $_POST );
				
				$user_id=$wpdb->get_var("select max(id) from users");
				$_POST['id']=$user_id=($user_id<100000)?100000:$user_id+1;
				$this->insert_form( 'users', $_POST );
				
				$settings=get_option('realty_general_settings');
				if(!in_array($_POST['office_id'], $settings['office_id']))array_push($settings['office_id'], $_POST['office_id']);
				update_option('realty_general_settings', $settings);
				
				$login=$realty->siteUrl.'agent-login/';
				$subject="[".$this->blogname."] Your username and password";
				$message="Thanks for signing up to ".$this->blogname.". You can edit your account or create a listing at $login. Below is your login detail:\n\rUsername: $user_email\n\rPassword: $password\n\r\n\rBest Regards,\n\r\n\r".$this->blogname;				
				$this->text_email($user_email, $subject, $message, '', '', false);
				
				$za_email = get_option('za_email');
				if(!empty($za_email)){
					$subject="[".$this->blogname."] new private listing registration";
					$message="Name: $firstname  $lastname\n\rEmail: $user_email\n\rPhone: $phone\n\r\n\rAddress\n\rUnit Number: $unit_number\n\rStreet Number: $street_number\n\rStreet: $street\n\rSuburb: $suburb\n\rState: $state\n\rPostcode: $zipcode\n\r\n\rIp Address: $ip_address\n\r\n\rSent out from ".$realty->siteUrl;
					$this->text_email($za_email, $subject, $message, '', '', false);
				}
				echo "<p class='success'>Thanks for signing up your login details have been emailed to you.</p>";
				$success=1;
				$_POST=array();
			}
		}
	}
}
if($success!=1){
?>
<form id="setupform" method="post" action="" onsubmit="return validate_form();">
	<div class="block_content form-agent">		
		<p><label >First name <span class="red">*</span></label> <input name="firstname" type="text"  value="<?php echo $_POST['firstname']; ?>" maxlength="200" /></p>
		<p><label >Last name <span class="red">*</span></label> <input name="lastname" type="text"  value="<?php echo $_POST['lastname']; ?>" maxlength="200" /></p>
		<p><label >Email <span class="red">*</span></label> <input name="user_email" type="text"  value="<?php echo $_POST['user_email']; ?>" maxlength="200" /></p>
		<p><label >Password <span class="red">*</span></label> <input name="password" type="password"  value="" maxlength="200" /></p>
		<p><label >Verify Password <span class="red">*</span></label> <input name="verify_password" type="password"  value="" maxlength="200" /></p>
		<p><label >Telephone</label> <input name="phone" type="text"  value="<?php echo $_POST['phone']; ?>" maxlength="200" /></p>
		<br/>
		
		<h2>Address</h2>		
		<p><label >Unit number</label> <input name="unit_number" type="text"  value="<?php echo $_POST['unit_number']; ?>" maxlength="200" /></p>
		<p><label >Street number</label> <input name="street_number" type="text"  value="<?php echo $_POST['street_number']; ?>" maxlength="200" /></p>
		<p><label >Street</label> <input name="street" type="text"  value="<?php echo $_POST['street']; ?>" maxlength="200" /></p>
		<p><label >Suburb</label> <input name="suburb" type="text"  value="<?php echo $_POST['suburb']; ?>" maxlength="200" /></p>       
		<p><label >State</label> 
		<select name="state" >
			<option value="">Please Select</option>
			<option value="ACT" <?php if($_POST['state']=='ACT')echo 'selected="selected"'; ?>>ACT</option>
			<option value="NSW" <?php if($_POST['state']=='NSW')echo 'selected="selected"'; ?>>NSW</option>
			<option value="NT" <?php if($_POST['state']=='NT')echo 'selected="selected"'; ?>>NT</option>
			<option value="QLD" <?php if($_POST['state']=='QLD')echo 'selected="selected"'; ?>>QLD</option>
			<option value="SA" <?php if($_POST['state']=='SA')echo 'selected="selected"'; ?>>SA</option>
			<option value="TAS" <?php if($_POST['state']=='TAS')echo 'selected="selected"'; ?>>TAS</option>
			<option value="VIC" <?php if($_POST['state']=='VIC')echo 'selected="selected"'; ?>>VIC</option>
			<option value="WA" <?php if($_POST['state']=='WA')echo 'selected="selected"'; ?>>WA</option>
		</select>
		</p>
		<p><label >Postcode</label> <input name="zipcode" type="text"  value="<?php echo $_POST['zipcode']; ?>" maxlength="200" /></p>		
		<input type="hidden" name="country" value="Australia">
		<br/>
		
		<p><label >Spam Code <span class="red">*</span></label>		
		<img id="captcha-image" src="<?php echo $this->pluginUrl.'includes/CaptchaSecurityImages.php?width=100&height=40&characters=5'; ?>" alt="Anti-Spam Image" style="vertical-align:top;" /><span class="reload-captcha"><a href="javascript:captchas_image_reload('captcha-image')" class='reload-image' title="Reload Image">Reload Image</a></span>
		<br/><br/><label>Enter the above code</label><input type="text" name="securitycode" id="securitycode" size="20" />
	
		</p>			
		<div class="clear"></div> 
		<br/>
		<input type="submit" name="submit" class="button" value="Register" /></div>
	<div class="clear"></div>
</form>
	
<script type="text/javascript">
  function captchas_image_reload (imgId) 
  {
	var image_url = document.getElementById(imgId).src;
	image_url+= "&";
	document.getElementById(imgId).src = image_url;
  }
</script>
<?php } ?>