<?php
global $helper, $current_user, $realty, $wpdb;
if (!is_user_logged_in()){ ?>
	<script type="text/javascript">
	location.href='<?php echo $realty->siteUrl.'wp-login.php'; ?>'; 
	</script>
<?php die();  }
include('menu.php');

$condition=$_GET;
if(current_user_can('administrator'))$office_id=$_GET['office_id'];
else $office_id=$wpdb->get_var("select office_id from users where email='".$current_user->user_email."' and contributor=1");

if(!$office_id)return;

$extra_office_id=$wpdb->get_var("select extra_office_id from office where id=$office_id");
if(!empty($extra_office_id))$office_id.=",".$extra_office_id;

if($_GET['task']=='delete' && !empty($_GET['id'])){	
	if($wpdb->query("delete from office where main_office_id in($office_id) and id=".$_GET['id'])){
		// delete office page
		$short_code="<!--realty_plugin: office_id=".$_GET['id']." template:office-->";
		$postID=$wpdb->get_var("select ID from $wpdb->posts where post_content='$short_code'");		
		if($postID)wp_delete_post( $postID, true ); 
		
		$user_ids=$wpdb->get_col("select id from users where office_id=".$_GET['id']);
		if($user_ids){
			$user_ids=implode(",",$user_ids);
			$wpdb->query("delete from users where id in($user_ids)");
			$wpdb->query("update properties set user_id='' where user_id in ($user_ids)");
			$wpdb->query("update properties set secondary_user='' where secondary_user in ($user_ids)");
			$wpdb->query("update properties set third_user='' where third_user in ($user_ids)");
			$wpdb->query("update properties set fourth_user='' where fourth_user in ($user_ids)");
		}
		echo "<div id='success'><span>Office succesfully deleted.</span></div>";
	}
}
if($_GET['x']==1){	echo "<div id='return'><span>New office successfully created.</span></div>";}
$offices=$wpdb->get_results("select * from office where main_office_id in($office_id) order by name", ARRAY_A);

?>
<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="<?php echo $this->pluginUrl;?>css/table_sorter.css" type="text/css" id="" media="print, projection, screen" />
<script type="text/javascript">	
jQuery(function() {
	jQuery("#properties_table").tablesorter({     widgets: ['zebra']     });
});	
</script>	
<p class="add-agent-link"><a href="<?php echo $realty->siteUrl."dashboard/new-office/?office_id=$office_id"; ?>">Add New Office</a></p>
<table class="tablesorter tableOffice" cellpadding="0" cellspacing="0" id="properties_table">
	<thead>
		<tr class="th">
			<th class="th_id">ID</th>
			<th class="th_name">Name</th>
			<th class="th_email">Email</th>						
			<th class="th_phone">Phone</th>			
			<th class="th_edit">Edit</th>
			<th class="th_delete">Delete</th>
		</tr>
	</thead>
	<?php 
	foreach($offices as $office): ?>
	<tr class="<?php echo $odd_class = empty($odd_class) ? 'alt ' : ''; ?>">
		<td class="td_id"><?php echo $office['id']; ?></td>
		<td class="td_name"><?php echo $office['name']; ?></td>		
		<td class="td_email"><?php echo $office['email']; ?></td>
		<td class="td_phone"><?php echo $office['phone']; ?></td>
		<td class="td_edit"><a class="ed_edit" href="<?php echo $realty->siteUrl.'dashboard/edit-office/?id='.$office['id'];?>">Edit</a></td>
		<td class="td_delete">
			<a class="delete" onClick="return confirm('You are about to delete the this office including all agents belong to this office. The agents will be removed from listings when deleted. \n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl.'dashboard/offices/?task=delete&id='.$office['id'];?>">Delete</a>
		</td>
	</tr>
	<?php endforeach; ?>
</table>