jQuery(document).ready(function ($) {
    $('.color-picker-selector').wpColorPicker();

    $(document).on('click', '.media-upload-selector', function (e) {
        e.preventDefault();
        var target = $(this).data('selector-target');
        // Create a new media frame
        var frame = wp.media({
            title: 'Select or Upload Media',
            button: {
                text: 'Submit'
            },
            multiple: false  // Set to true to allow multiple files to be selected
        });
        // When an image is selected in the media frame...
        frame.on('select', function () {
            // Get media attachment details from the frame state
            var attachment = frame.state().get('selection').first().toJSON();
            // Send the attachment id to our hidden input
            $('input[name="' + target + '"]').val(attachment.url);
        });
        // Finally, open the modal on click
        frame.open();
    });

    $(document).on('click', '[data-preview-color-trigger="1"]', function (e) {
        var args = $(this).data('preview-args');
        var option_name = $(this).data('preview-option-name');
        var query = {};
        $.each(args, function (i, v) {
            query[v] = $('input[name="' + option_name + '[' + v + ']' + '"]').val();
        });
        var url = $(this).data('preview-url') + $.param(query);
        window.open(url, '_blank');
    });
});