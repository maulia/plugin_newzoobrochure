<?php

defined('ABSPATH') or die('No script kiddies please!');

class zoobrochure_template_classic extends zoobrochure_template {

    public function render() {
        global $wpdb;

        $property = $this->property;
        $setting = $this->setting;

        $this->pdf->SetAutoPageBreak(True, 5); //Earlier 40
        $this->pdf->AliasNbPages();
        $this->pdf->AddPage();

        // Color
        $text_red = $setting['font_color'][0];
        $text_green = $setting['font_color'][1];
        $text_blue = $setting['font_color'][2];

        $headline_red = $setting['headline_color'][0];
        $headline_green = $setting['headline_color'][1];
        $headline_blue = $setting['headline_color'][2];

        $headline_font_red = $setting['headline_font_color'][0];
        $headline_font_green = $setting['headline_font_color'][1];
        $headline_font_blue = $setting['headline_font_color'][2];

        $block_red = $setting['block_color'][0];
        $block_green = $setting['block_color'][1];
        $block_blue = $setting['block_color'][2];

        $bg_red = $setting['bg_color'][0];
        $bg_green = $setting['bg_color'][1];
        $bg_blue = $setting['bg_color'][2];

        $logo = $setting['logo'];

        /* image */
        $y = 20;
        if (!empty($property['photos'])) {
            $photos = array();
            $i = 0;
            foreach ($property['photos'] as $photo) {
                $size = ($i == 0) ? 'large' : 'medium';
                $photos[$i] = array($photo[$size], $photo[$size . '_width'], $photo[$size . '_heigth']);
                $i++;
            }

            $this->pdf->SetFillColor($bg_red, $bg_green, $bg_blue);

            if (!empty($photos[0])) {  // Main image; The big one
                list($src, $width, $height) = $photos[0];
                if ($height > $width)
                    $this->pdf->Image($src, 20, 10, 170, 150);
                else
                    $this->pdf->Image($src, 20, 10, 170);
                $this->pdf->Rect(0, 9, 210, 8, 'F');
                $this->pdf->Rect(190, 10, 20, 125, 'F');
                $this->pdf->Rect(0, 120, 210, 200, 'F');
            }
            $y = 130;

            if (!empty($photos[1])) { // 1st thumb
                list($src, $width, $height) = $photos[1];
                if ($height > $width)
                    $this->pdf->Image($src, 0, 125, 110, 90);
                else
                    $this->pdf->Image($src, 0, 125, 110);
            }
            if (!empty($photos[2])) { // 2nd thumb
                list($src, $width, $height) = $photos[2];
                if ($height > $width)
                    $this->pdf->Image($src, 115.5, 125, 110, 90);
                else
                    $this->pdf->Image($src, 115.5, 125, 110);
            }

            if (!empty($photos[1])) {
                $y = 205;
                $this->pdf->Rect(0, 120, 20, 90, 'F');
                $this->pdf->Rect(190, 120, 20, 90, 'F');
                $this->pdf->Rect(0, 196, 210, 120, 'F');
            }
        }

        /* address */

        $this->pdf->SetTextColor($headline_font_red, $headline_font_green, $headline_font_blue);
        $this->pdf->SetFillColor($headline_red, $headline_green, $headline_blue);
        $this->pdf->Rect(20, $y - 4, 95, 15, 'F');
        $this->pdf->SetFont('Helvetica', 'B', 17);
        $this->pdf->SetXY(23, $y);
        $this->pdf->MultiCell(100, 2, $property['suburb'] . ", " . $property['state'], 0, 'L');
        $this->pdf->SetFont('Helvetica', '', 13);
        $this->pdf->SetXY(23, $y + 5);
        $this->pdf->MultiCell(100, 2, $property['street_address'], 0, 'L');

        /* beth, bath, car */
        $this->pdf->SetFillColor($headline_red, $headline_green, $headline_blue);
        $this->pdf->SetFont('Helvetica', 'B', 33);
        $this->pdf->Rect(116.5, $y - 4, 11, 15, 'F');
        $beds = floor($property['bedrooms']);
        $baths = floor($property['bathrooms']);
        if (strpos($property['bedrooms'], '.5') !== false) {
            $half_bed = '1';
        }
        if (strpos($property['bathrooms'], '.5') !== false) {
            $half_bath = '1';
        }
        $this->pdf->SetXY(118, $y + 1);
        $this->pdf->MultiCell(100, 2, $beds, 0, 'L');
        $this->pdf->Rect(129, $y - 4, 11, 15, 'F');
        $this->pdf->SetXY(130.5, $y + 1);
        $this->pdf->MultiCell(100, 2, $baths, 0, 'L');
        $this->pdf->Rect(141.5, $y - 4, 11, 15, 'F');
        $this->pdf->SetXY(143, $y + 1);
        $this->pdf->MultiCell(100, 2, $property['carspaces'], 0, 'L');

        $this->pdf->SetTextColor($headline_font_red, $headline_font_green, $headline_font_blue);
        $this->pdf->SetFont('Helvetica', '', 10);
        $this->pdf->SetXY(117.5, $y + 7);
        $this->pdf->MultiCell(100, 2, 'BED', 0, 'L');
        $this->pdf->SetXY(129, $y + 7);
        $this->pdf->MultiCell(100, 2, 'BATH', 0, 'L');
        $this->pdf->SetXY(142.5, $y + 7);
        $this->pdf->MultiCell(100, 2, 'CAR', 0, 'L');

        if ($half_bed == '1' || $half_bath == '1') {
            $this->pdf->SetDrawColor(255, 255, 255);
            $this->pdf->SetTextColor($headline_red, $headline_green, $headline_blue);
            if ($half_bed == '1') {
                $this->pdf->Rect(124.5, $y - 3, 1, 3, 'DF'); // vertikal
                $this->pdf->Rect(123.5, $y - 2, 3, 1, 'DF'); // horizontal
                $this->pdf->SetDrawColor(53, 53, 55);
                $this->pdf->Line(124.6, $y - 2, 125.4, $y - 2);
                $this->pdf->Line(124.6, $y - 1, 125.4, $y - 1);
            }
            $this->pdf->SetDrawColor(255, 255, 255);
            if ($half_bath == '1') {
                $this->pdf->Rect(137, $y - 3, 1, 3, 'DF'); // vertikal
                $this->pdf->Rect(136, $y - 2, 3, 1, 'DF'); // horizontal
                $this->pdf->SetDrawColor(53, 53, 55);
                $this->pdf->Line(137.1, $y - 2, 137.9, $y - 2);
                $this->pdf->Line(137.1, $y - 1, 137.9, $y - 1);
            }
        }

        $logo_wp_content = WP_CONTENT_DIR . end(explode('wp-content', $logo));
        $this->pdf->Image($logo_wp_content, 154, $y, 36);


        /* headline */
        $this->pdf->SetTextColor($text_red, $text_green, $text_blue);
        $this->pdf->SetFont('Helvetica', 'B', 10);
        $this->pdf->SetXY(20, $y + 16);
        $this->pdf->MultiCell(95, 4, $property['headline'], 0, 'L');
        $headline_row = ceil(strlen($property['headline']) / 48);
        $next = $y + 16 + ($headline_row * 4);

        /* description */
        $this->pdf->SetFont('Helvetica', '', 8);
        $this->pdf->SetTextColor($text_red, $text_green, $text_blue);
        $this->pdf->SetXY(20, $this->pdf->getY());
        $desc = str_replace("\t", " ", $property['description']);
        $desc = preg_replace("[\t\n\r]", "\n", $desc);
        $desc_rows = explode("\n", $desc);
        foreach ($desc_rows as $desc_row) {
            $y_desc = $this->pdf->GetY();
            if ($y_desc <= 275) {
                $this->pdf->SetXY(20, $y_desc);
                $this->pdf->MultiCell(95, 4, trim($desc_row), 0, 'L');
            }
        }

        /* price */
        $this->pdf->SetFont('Helvetica', 'B', 10);
        $this->pdf->SetTextColor($text_red, $text_green, $text_blue);
        $this->pdf->SetXY(116.5, $y + 16);
        if (!empty($property['auction_date'])) {
            $auction_date = 'Auction: ' . date("D, j M Y", strtotime($property['auction_date']));
            if (!empty($property['auction_time']) && $property['auction_time'] != '00:00:00')
                $auction_date .= " @" . date('g:ia', strtotime($property['auction_time']));
            if (!empty($property['auction_place']))
                $auction_date .= "\n" . $property['auction_place'];
            $this->pdf->MultiCell(60, 4, $auction_date, 0, 'L');
        }
        else {
            $this->pdf->MultiCell(60, 4, $property['price'], 0, 'L');
        }

        /* detail */
        $this->pdf->SetTextColor($text_red, $text_green, $text_blue);
        $next_detail = $this->pdf->getY();
        $details = array('tax_rate' => 'Council Rates', 'condo_strata_fee' => 'Water Rates', 'opentimes' => 'View', 'user' => 'Contact', 'property_type' => 'Type', 'available_at' => 'Date Available', 'sold_at' => 'Sold Date', 'leased_at' => 'Leased Date', 'land_size' => 'Land', 'bond' => 'Bond', 'year_built' => 'Year Built');
        foreach ($details as $key => $values) {
            if (empty($property[$key]))
                continue;
            switch ($key) {
                case 'sold_at' : case 'leased_at' : case 'available_at' :
                    if ($property[$key] != '0000-00-00')
                        $this->print_detail($values, date("d/m/Y", strtotime($property[$key])), $next_detail);
                    break;
                case 'sold_price' :case 'leased_price' :case 'bond' :
                    $this->print_detail($values, "$" . $property[$key], $next_detail);
                    break;
                case 'opentimes':
                    $i = 0;
                    $detail = '';
                    foreach ($property['opentimes'] as $opentime) {
                        $detail .= date('D, j M Y', strtotime($opentime['date'])) . " @" . date('g:i a', strtotime($opentime['start_time'])) . " - " . date('g:i a', strtotime($opentime['end_time']));
                        $detail .= "\n";
                        $i++;
                    }
                    $this->pdf->print_detail($values, $detail, $next_detail);
                    if ($i > 0)
                        $next_detail = $next_detail + (($i - 1) * 4);
                    break;
                case 'user';
                    $detail = $property['user'][0]['name'];
                    $i = 0;
                    if (!empty($property['user'][0]['mobile'])) {
                        $detail .= "\n" . $property['user'][0]['mobile'];
                        $i++;
                    }
                    if (!empty($property['user'][1]['name'])) {
                        $detail .= "\n" . $property['user'][1]['name'];
                        $i++;
                    }
                    if (!empty($property['user'][1]['mobile'])) {
                        $detail .= "\n" . $property['user'][1]['mobile'];
                        $i++;
                    }
                    $this->print_detail($values, $detail, $next_detail);
                    if ($i > 0)
                        $next_detail = $next_detail + ($i * 4);
                    break;
                case 'land_size';
                    $this->print_detail($values, $property[$key] . $property['land_area_metric'], $next_detail);
                    break;
                case 'tax_rate': case 'condo_strata_fee':
                    $key_period = $property[$key . '_period'];
                    switch ($key_period) {
                        case 'Per Week': $period = '/weeky (approx)';
                            break;
                        case 'Per Month': $period = '/month (approx)';
                            break;
                        case 'Per Quarter': $period = '/qtr (approx)';
                            break;
                        case 'Per Year': $period = '/year (approx)';
                            break;
                        default :$period = $key_period;
                            break;
                    }
                    $this->print_detail($values, '$' . number_format($property[$key]) . $period, $next_detail);
                    break;
                default:
                    $this->print_detail($values, $property[$key], $next_detail);
                    break;
            }
            $next_detail = $next_detail + 4;
        }

        $this->pdf->SetFont('Helvetica', 'B', 8);
        $this->pdf->SetXY(116.5, $next_detail);
        $this->pdf->MultiCell(60, 4, get_option('siteurl'), 0, 'L');

        /* florplan */
        if (!empty($property['floorplans'])) {
            $floorplans = array();
            $i = 0;
            foreach ($property['floorplans'] as $floorplan) {
                $floorplans[$i] = $floorplan['large'];
                $i++;
            }
        }

        if (!empty($floorplans[0])) {
            $this->pdf->AddPage();
            $this->pdf->Image($floorplans[0], 20, 20, 170, 240);

            $this->pdf->SetFont('Helvetica', '', 7);
            $this->pdf->SetXY(20, 257);
            $this->pdf->MultiCell(100, 2, "Plans shown are only indicative of layout. Dimensions are approximate.", 0, 'L');
            $this->pdf->SetTextColor($headline_font_red, $headline_font_green, $headline_font_blue);
            $this->pdf->SetFillColor($headline_red, $headline_green, $headline_blue);
            $this->pdf->Rect(20, 261, 170, 15, 'F');
            $this->pdf->SetFont('Helvetica', 'B', 17);
            $this->pdf->SetXY(23, 265);
            $this->pdf->MultiCell(100, 2, $property['suburb'] . ", " . $property['state'], 0, 'L');
            $this->pdf->SetFont('Helvetica', '', 13);
            $this->pdf->SetXY(23, 270);
            $this->pdf->MultiCell(100, 2, $property['street_address'], 0, 'L');
        }
    }

    function print_detail($title, $value, $y) {
        $this->pdf->SetFont('Helvetica', 'B', 8);
        $this->pdf->SetXY(116.5, $y);
        $this->pdf->MultiCell(60, 4, $title . ":", 0, 'L');
        $this->pdf->SetFont('Helvetica', '', 8);
        $this->pdf->SetXY(138, $y);
        $this->pdf->MultiCell(60, 4, $value, 0, 'L');
    }

}
