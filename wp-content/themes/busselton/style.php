<?php
global $options;
foreach ($options as $value) {if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }}

header("Content-type: text/css");
?>
h1.logo a, h1.logo a img { width: <?php echo $logo_width; ?>px; height: <?php echo $logo_height; ?>px; }

/*  
Theme Name: Carbon
Theme URI: http://demo.thatid.com/carbon/
Description: Carbon theme.
Version: 1.1
Author: Agentpoint
Author URI: http://agentpoint.com.au/
*/

/* -------------------------------------------------------------- 
FIXES

1.1:	Updated includes/nav-menu.php to show dropdown arrows only when user chooses to show dropdown menus in the theme options page
		Added B2 Links Page to functions.php
		Sidebar menu added; required change to get_absolute_ancestor() function and added Theme option to choose if user wants it or not
		Added stripslashes to Theme Options input box values in functions.php

-------------------------------------------------------------- */

/* -------------------------------------------------------------- 
  
   Reset.css
   * Resets default browser CSS styles.
   
   Original by Erik Meyer:
   * http://meyerweb.com/eric/thoughts/2007/05/01/reset-reloaded/
   
-------------------------------------------------------------- */

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, font, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	font-weight: inherit;
	font-style: inherit;
	font-size: 100%;
	font-family: inherit;
	vertical-align: baseline;
}

/***** Links *****/
.enhanced_active{ 
color: #6CC417 !important;
}
a:link, a:visited 	{ text-decoration: none; -webkit-transition: color .4s ease-in-out;}
a:hover, a:focus, a:active { text-decoration: underline; }

:active, :hover { outline: 0; }

h2.post_title a { text-decoration: none; }

/***** Buttons *****/
p.button, p.cf-sb, p.quick_search_btn p, p.submit_btn { width: 80px; height: 23px; background-image: url(images/buttons/btn_left.png); background-repeat: no-repeat; background-position: 0 0; padding: 0; -webkit-transition: background-color .4s ease-in-out; }
	#property_tools p, #add_to_favs p, p.photo_btn, p.view_larger_btn { width: 140px; }
p:hover, p:focus, p:active { -webkit-transition: background-color .4s ease-in-out; }

a.btn, input.btn, input.Buttons, input.sendbutton {
	width: 80px; height: 21px; background: url(images/buttons/btn_right.png) no-repeat 100% 0; padding: 0 0 4px; border: 0; display: block; cursor: pointer;
}
	#property_tools p a.btn, #property_tools p input.btn, #add_to_favs p a.btn, p.photo_btn a.btn, p.view_larger_btn a.btn { width: 140px; padding: 1px 0 3px; overflow: hidden; }
	input.btn, input.Buttons, input.sendbutton { height: 23px; }

p.email_agent_btn { width: 90px; background: none; }
p.email_agent_btn a { width: 90px; height: 23px; background: url(images/buttons/btn_90.png) no-repeat 0 0; text-decoration: none; -webkit-transition: background-color .4s ease-in-out;}
span.help_tab { width: 90px; height: 18px; background: url(images/buttons/btn_90.png) no-repeat 0 0; text-decoration: none; -webkit-transition: background-color .4s ease-in-out;}

p.big { width: 288px; height: 58px; background: none; }
	#sidebar p.big { position: relative; left: -4px; }
	p#watch_podcast { background: url(images/buttons/btn_podcast_trans.png) no-repeat 0 0; }
p.big a { width: 280px; height: 50px; padding: 4px; display: block; text-indent: -9999px; }

#switch_buttons { float: right; margin: -30px 0 0; }
#switch_buttons p { width: auto; float: left; margin: 0 0 0 5px; padding: 0; }
#switch_buttons p a { width: auto; padding: 0 10px 4px; }

/***** Tabbed sections *****/
ul.shadetabs, ul.ui-tabs-nav, ul#listing_tabs { height: 18px; margin: 0 0 15px; padding: 0; border-bottom: 1px solid; clear: both; list-style: none; }
	#content ul.shadetabs { width: 300px; margin: 0; border: 0; }
	.realty_widget_enchanced_search ul.shadetabs { margin: 0 0 10px; }
	#flashcontent ul.ui-tabs-nav { margin: 0; }
	#content .realty_widget_sales_data ul.shadetabs { width: 620px; }
	ul#listing_tabs { margin: 0; border: 0; }
ul.shadetabs li, ul.ui-tabs-nav li, ul#listing_tabs li { height: 18px; background: url(images/buttons/btn_left.png) no-repeat 0 0; margin: 0 3px 0 0; padding: 0; float: left; -webkit-transition: background-color .4s ease-in-out; }
ul.shadetabs li a, ul.ui-tabs-nav li a, ul#listing_tabs li a { height: 14px; background: url(images/buttons/btn_right.png) no-repeat 100% 0; padding: 0 10px 4px; display: block; float: left; }
ul.shadetabs li a:hover, ul.shadetabs li a.selected, ul.ui-tabs-nav li.ui-tabs-selected a, ul#listing_tabs li.current a {  }
.ui-tabs-hide, .ui-tabs-hide { display: none; }


/***** Global Classes *****/

hr { width: 100%; margin: 0 0 25px; border: 0; border-top: 2px solid; clear: both; }

.clearer, .clear { clear:both; }

/* Clearfix hack */
.clearfix:after {content: ".";
	display: block;
	height: 0;
	clear: both;
	visibility: hidden;}
.clearfix {display: inline-block;}
/* Hides from IE-mac \*/
* html .clearfix { height: 1%;}
.clearfix {display: block;}
/* End hide from IE-mac */


.float-left, .alignleft 	{ float:left; }
.float-right, .alignright 	{ float:right; }
.alignnone { float: none; }
.aligncenter { margin: 0 auto; display: block; } /* Need to check on this. */

.text-left 		{ text-align:left; }
.text-right 	{ text-align:right; }
.text-center 	{ text-align:center; }
.text-justify 	{ text-align:justify; }
.uppercase		{ text-transform: uppercase; }
.capitalize		{ text-transform: capitalize; }
	i.capitalize { font-style: normal; }
.letter-spacing{ letter-spacing: 1px; }

.bold 			{ font-weight:bold; }
.italic 		{ font-style:italic; }
.underline 		{ border-bottom:1px solid; }
.highlight 		{ background:#ffc; }
.position_inside { list-style-position: inside; }

img { border:3px solid; }
.post .entry img { width: 220px !important; height: auto !important; margin:0 15px 16px 0; padding: 0; border: 0; float: left; }
.post img.size-thumbnail { width: 140px !important; height: auto !important; }
.post img.size-medium { width: 300px !important; height: auto !important; }
.post img.size-large { width: 460px !important; height: auto !important; margin: 0 0 25px; }
.post img.size-full { width: 620px !important; height: auto !important; margin: 0 0 25px; }

.post .wp-caption { max-width: 538px !important; border: 1px solid; }
	.post .wp-caption.alignleft { margin: 0 18px 3px 0; }
	.post .wp-caption.alignright { margin: 0 0 3px 18px; }
	.post .wp-caption.aligncenter { margin: 0 auto 18px; }
.post .entry .wp-caption img { margin: 5px 0 5px 5px; }
	.post .entry .wp-caption img.size-large { margin: 2px 0 5px 5px; clear: none; }
	.post .entry .wp-caption img.size-full { width: 528px !important; margin: 0; }
#content .post p.wp-caption-text { margin: 0 5px 6px !important; padding: 0; clear: both; }

.post .entry img.alignleft 		{ float:left;margin:0 15px 16px 0; }
.post .entry img.alignright 		{ float:right;margin:4px 0 16px 15px; }

.nomargin		{ margin:0 !important; }
.nopadding 		{ padding:0 !important; }
.noborder		{ border: 0; }
.noindent 		{ margin-left:0;padding-left:0; }
.nobullet 		{ list-style:none;list-style-image:none; }
.nodisplay, .hide, .hidden	{ display: none; }

blockquote,q { quotes:none; }
blockquote 	{ background: url(images/quote.gif) no-repeat 0 10px; margin-left: 5px; margin-right: 19px; padding:8px 15px 0; border-width: 1px; border-style: solid; }

p.requi { margin: -24px 0 0; float: right; text-align: right; }

/**************************************************** B2 - Layout Styles ****************************************************/

/* global settings
---------------------------------------------------------- */
html, body {height: 100%;}
body { background: url(images/backgrounds/bg_body.png) repeat-x 0 0; }
	body#team_page_popup, body#email_subscribe, body#ssp, body#print-property { background: none; }
		body#ssp { height: auto; padding: 20px 0 0 20px; }
body > #wrapper {height: auto; min-height: 100%; }
#wrapper { width: 1008px; background: url(images/backgrounds/bg_wrapper.png) repeat-y 0 center; margin: 0 auto; }
	body#print-property #wrapper { width: 750px; background: none; }
#main_body { width: 940px; padding: 0 34px 20px; }
	body#team_page_popup #main_body { width: 760px; padding: 20px; }
	body#email_subscribe #main_body { width: auto; padding: 10px 20px; }
	body#print-property #main_body { width: 750px; padding: 0; }
#container { width: 100%; background-image: url(images/backgrounds/bg_container.gif); background-repeat: repeat-y; background-position: 0 0; margin: 25px 0; }

/***** Forms *****/
form 			{ margin:0; padding:0; }
	form.contact_form { margin: 0 0 25px; padding: 0 0 25px; border-bottom: 2px solid; }
input, textarea { margin: 0; padding: 3px 2px; border: 1px solid; }
	input.radio, input.checkbox { width: auto !important; background-color: transparent; padding: 0; border: 0; vertical-align: middle; }
	#sidebar form input#s { width: 272px; margin: 10px 0; }
select { margin: 0; padding: 3px 2px; }
body.fourOfour input { margin: 0; }

body#email_subscribe p { margin: 0 0 10px; padding: 0; }
body#email_subscribe input.textbox, body#email_subscribe textarea { width: 200px; }

form.contact_form li.subscribe_country select, form.contact_form li.subscribe_referrer select, form.contact_form li.property_type select { width: 280px; }
	form.contact_form li.property_type select { margin-bottom: 5px; }
form.contact_form input.searchbox, form.contact_form textarea { width: 280px; }

/***** Tables *****/
/* tables still need 'cellspacing="0"' in the markup */
table.footer_links_table tbody td { border: 0 !important;}
table.footer_links_table{ border: 0 !important; float: center; position:relative; align:center;}
table {
	margin:0 0 28px;
	border-top-width: 1px; border-top-style: solid;
	border-right-width: 1px; border-right-style: solid;
	border-collapse: separate;
	border-spacing: 0;
}
caption { text-align: center; }
th, td {
	border-bottom: 1px solid;
	border-left: 1px solid;
	vertical-align: top; }

thead th, tfoot th { padding: 10px 5px; text-align: center; }
	thead th { vertical-align: bottom; }
thead th:first-child, tfoot th:first-child { padding: 10px 5px 10px 10px; text-align: right; }

tbody th { padding: 5px 5px 5px 10px; text-align: right; }
tbody td { padding: 5px; text-align: left; }
	table.comparable_sales_table th{ background-color : #FFFFFF; } 
	table.comparable_sales_table thead th{ padding: 10px 5px; text-align: center; } 
	table.comparable_sales_table tr.alt { background-color : #99CCFF;}
	table.comparable_sales_table tr.no_alt { background-color : ##95B9C7;}
	table.sales_data_table tbody td { padding: 5px 3px; }
	table.sales_data_table tbody td:first-child { text-align: right; }
	
/* header
---------------------------------------------------------- */
#header 	{ width: 100%; height: 80px; padding: 10px 0 0 0; border-bottom: 10px solid; clear: both; position: relative; }
.headerwrap { width: 990px; height: 80px; margin: 0 auto; }

h1.logo			{ width: 320px; margin: 10px 0 0; float: left; }
h1.logo a 		{ margin: 0; display: block; overflow: hidden;  }
h1.logo a img 	{ background: transparent; padding: 0; border: 0;  }

/* nav
---------------------------------------------------------- */
#nav { width: 620px; height: 80px; margin: 0; padding: 0; position: absolute; bottom: -10px; right: 0; z-index: 99; }
#nav ul { margin: 0; padding: 0; float: right; list-style: none; }
#nav ul li { margin: 0; padding-left: 1px; float: left; }
#nav ul li.page_item { background: none !important; padding: 0; }
	#nav ul li:first-child { margin: 0; }
#nav ul li a { height: 23px; padding: 32px 10px 15px 20px; float: left; display: block; position: relative; text-align: right; text-decoration: none; -webkit-transition: color .4s linear; } /* padding altered in type2.css */
#nav ul li a span.span_div { width: 100%; height: 70px; border-right-width: 1px; border-right-style: solid; text-indent: -9999px; position: absolute; top: 0; left: -1px; z-index: 1; }
	#nav ul li:first-child a span.span_div { border-left-width: 1px; border-left-style: solid; }

#nav ul li a, #nav ul li a:hover, #nav ul li a:active, #nav ul li.current_page_item a { border-bottom: 10px solid; -webkit-transition: color .4s ease-in-out;}

#nav ul li span.ddarrow { width: 10px; height: 5px; background: url(images/icons/dd_arrow.gif) no-repeat 100% 0; margin: 7px 0 0; padding-left: 5px; float: right; display: block; text-indent: -9999px; }

/* drop down menus */
#nav ul li ul { width: 140px; height: auto; background: none; margin: 80px 0 -1px 0; padding: 0; border: 0; border-bottom: 1px solid; display:none; position:absolute; z-index: 99; }
#nav ul li:hover ul {  }
#nav ul li.current_page_parent ul { margin-left: 0; }

#nav ul li ul li { height: auto; margin: 0; padding: 0; float:none; clear:both; }
* html #nav ul li ul li { height: 22px; }
#nav ul li ul li a { width: 100px; height: auto; padding: 5px 20px; display: block; clear:both; border: 0; border-top: 1px solid; text-align: left; -webkit-transition: none; }
	#nav ul li ul li:first-child a { border: 0; }
	#nav ul li ul li.current_page_item a { border-bottom: 0; }
#nav ul li ul li a:hover { border-bottom: 0; text-decoration: underline; }

#nav ul li ul li ul { width: 140px; margin: 0; padding:0; border: 0; border-top: 1px solid; position:relative; clear:both; }
#nav ul li ul li ul li { margin:-2px 0 0 0; float: none; border:0; display:block; }
#nav ul li ul li ul li a { padding: 5px 20px 5px 30px; }

/* Banner and contact info
---------------------------------------------------------- */
#banner { width: 938px; height: 100px; padding: 60px 0 50px; border-left: 1px solid; border-right: 1px solid; }
.bannerwrap { width: 100%; height: 100px; background: url(images/banner/bg_banner_gradient.png) no-repeat 0 0; }
.banner_text { width: 550px; height: 75px; margin: 14px 22px 11px 46px; padding: 0; float: left; display: block; text-indent: -9999px; }
p.big_button { margin: 22px 0 0; float: left; }
p.big_button a img { border: 0 !important; }

#contact_info { width: 100%; height: 12px; margin: 0; padding: 9px 0; }
#contact_info p { margin: 0 auto; }

/* extra
---------------------------------------------------------- */
#extra 		{ width: 100%; }
.extrawrap { width: 910px; height: auto; margin: 0 auto; padding: 25px 15px 32px; }

#footer_nav { width: 720px; float: left; }
#footer_nav ul { margin: 0; padding: 0; list-style: none; }
#footer_nav ul li { width: 90px; margin-right: 10px; padding: 0; float: left; }
	#footer_nav ul li.page_item_top a { margin-bottom: 4px !important; display: block; }
#footer_nav ul li ul {  }
#footer_nav ul li ul li { width: auto; margin: 0; float:none; clear:both; }
#footer_nav ul li.page_item_top ul li a { margin: 0 !important; }
#footer_nav span { display: none; }

#extra_links { width: 180px; padding-left: 9px; border-left-width: 1px; border-left-style: solid; float: left; }

/* footer
---------------------------------------------------------- */
#footer 	{ width: 100%; height: 75px; background-image: url(../images/backgrounds/bg_footer.gif); background-repeat: repeat-x; background-position: 0 0; margin: 0 auto; }
.footerwrap { width: 920px; height: 35px; margin: 0 auto; padding: 10px 10px 0; }
.footerwrap p.credit { margin: 10px 0 0; padding: 0; }
.footerwrap p.agentpoint { margin: 0; padding: 0; border: 0; float: right; }
.footerwrap p.agentpoint img { padding: 0; border: 0; }

.footerwrap {  }

/* content
---------------------------------------------------------- */
#content 	{ width: 620px; margin: 0; padding: 0; float: right; }
	body#print-property #content { width: 430px; }

	/* body#home #content { width: 990px; } */

/***** Posts *****/
.post { margin-bottom: 11px; border-bottom: 1px solid; }

p.author_thumb { width: 60px; height: 57px; padding: 4px 0 0; float: left; }
p.author_thumb img { width: 50px; height: 50px; border: 0; }

p.author_pic { width: 110px; height: 110px; padding: 0 5px 6px 0; float: right; }
p.author_pic img { width: 105px; height: 105px; border: 0; }

.post_title_date	{ width: 400px; margin: 3px 20px 10px 0; float: left; }
	.post_title_date.no_author_thumb { width: 100% !important; margin-bottom: 9px; }
.post_title_date h2	{ padding: 0; }
p.postdate { width: 400px; margin: 0; padding: 0; }
.flag { margin: 10px 0 0; padding: 0; float: left; text-align: center; text-transform: uppercase; }
.flag a { width: 140px; height: 23px; background: url(images/backgrounds/bg_cat.png) repeat-x 0 0; padding: 2px 0 0; display: block; text-shadow: 1px 1px 1px #aaa; }

.entry				{ width: 100%; margin-bottom: 14px; clear: both; }
#blog-div.home .entry p { padding: 0; }
#content p.tagged_single { padding: 0; }

/* Posts Page */
p.postmetadata { width: 595px; height: 18px; clear: both; }
p.postmetadata a, .more-link {  }
	span.post_filed_cats { float: left; }

.navigation { height: 20px; margin: 0 0 25px; padding: 6px 5px 5px; clear: both; }

#blog-div.single #content .navigation p { padding-bottom: 4px; }

#blog-div .navigation p a { padding: 2px 4px; }
#blog-div.single .navigation p a { padding: 0; }

/***** Comments *****/
/* Comment List */
div#comments { margin: 0 0 25px; padding: 0; clear: both; }
div#comments .navigation { display: none; }

#content ul.commentlist { margin: 16px 0 0; padding: 0; list-style: none; }
#content ul.commentlist li { margin: 0 0 12px; padding: 8px 5px 0; border-top-width: 2px; border-top-style: solid; clear: both; }
ul.commentlist li.alt { border: 0; }

.comment-author { width: 95px; margin: 0 20px 11px 0; padding: 5px 0 7px; float: left; }
.comment-author img { margin: 0 0 5px; }
#content .comment-author p { padding: 0; }

.commentcontent { width: 480px; padding: 0; float: left; }

span.says { display: none; }

/* Leave a Reply */
#commentform-container { width: 620px; margin-bottom: 29px; padding: 0 0 9px; clear: both; }
#content #commentform-container h4 { margin: 4px 0 1px; }
#commentform-container form input.textbox	{ width: 305px; }
#commentform-container form textarea { width: 460px; height: 180px; margin: 0; }

/* Related Posts (after Leave a Reply) */
#wp_related_posts { width: 620px; margin-bottom: 30px; padding: 16px 0 0; }
#wp_related_posts h3 { margin-left: 15px !important; }

/* Where did they go from here */
#wherego_related { margin-bottom: 5px; padding-top: 20px; padding-bottom: 16px; }

/* Sociable */
div.sociable { width: 620px; margin: 6px 0 3px; padding: 8px 0 23px; border-top-width: 2px; border-top-style: solid; clear: both; }
.sociable_tagline { display: none; }
.sociable span { display: block; }
#content .sociable ul {
	height: 24px;
	margin: 0 !important;
	padding: 0 !important;
	list-style: none;
}
.sociable ul li {
	margin: 0 5px 0 0;
	float: left;
}
.sociable ul li:before { content: ""; }
.post .entry .sociable img {
	float: none;
	width: 24px !important;
	height: 24px !important;
	border: 0;
	margin: 0;
	padding: 0;
}

.sociable-hovers {
	opacity: .4;
	-moz-opacity: .4;
	filter: alpha(opacity=40);
}
.sociable-hovers:hover {
	opacity: 1;
	-moz-opacity: 1;
	filter: alpha(opacity=100);
}

/* AGENTS LIST AND AGENT PAGE
---------------------------------------------------------- */
#agents_list {  }
#agents_list .agent_item { margin: 0 0 25px; padding: 0 0 25px; border-bottom: 1px solid; }
#agents_list .imageCont, #agent .imageCont { margin: 0 20px 25px 0; float: left; }
#agents_list .imageCont img, #agent .imageCont img { width: 294px; height: auto; }

#agents_list .descCont, #agent .descCont { width: 300px; height: 144px; float: left; }
#agents_list .descCont ul, #agent .descCont ul { margin: 0; padding: 0 0 20px; list-style: none; }
#agents_list .descCont ul li, #agent .descCont ul li { margin: 0; padding: 0; }
		#agent .descCont ul li.agent_twitter, #agent .descCont ul li.agent_facebook, #agent .descCont ul li.agent_linkedin,
		#agents_list .descCont ul li.agent_twitter, #agents_list .descCont ul li.agent_facebook, #agents_list .descCont ul li.agent_linkedin { padding: 0 0 0 22px; }

#agents_list p { clear: both; }

#agent p.agent_description { width: 300px; margin-right: 20px; float: left; }
#agent #formpart { float: left; }
#agent p.agent_testimonials { clear: both; }

/* HOME
---------------------------------------------------------- */
#current_Sales, #current_Rentals, .realty_widget_featured_listings, #featured_sale, #featured_Rentals, #news_articles, #content #cloud_suburb, #content #cloud_sales, #content #cloud_lease, #sold_Sold, sold_Leased, #week_House { width: 620px; margin: 25px 0; clear: both; }

#featured_sale{margin:0 0 25px;}
.feature_photo { width: 300px; height: 225px; }
.feature_photo img { width: 294px; height: 219px; }

.realty_widget_featured_listings ul { margin: 0; padding: 0; list-style: none; }
.realty_widget_featured_listings li{float:left;}
.realty_widget_featured_listings li.feature1{margin:0 20px 0 0;}

.cloud {  }
#content .cloud .block_content { width: 600px; padding: 10px; }
.cloud p { padding: 0; }
	#tag_search .cloud p { padding: 0 0 10px; }
.cloud a { margin-right: 5px; }
#content p.selections_note, #content p.cloud_btn { display: none; }

.content_block .latest_listings .photo { width: 140px; height: 105px; margin: 0 20px 20px 0; float: left; }
.content_block .latest_listings .photo p { padding: 0; }
	.content_block .latest_listings .photo.photo3, .content_block .latest_listings .photo.photo7, .content_block .latest_listings .photo.photo11 { margin: 0 0 20px; }
.content_block .latest_listings .photo img { width: 134px; height: 99px; }

/* property search map */
.map_container { width: 580px; height: 270px; background: url(images/backgrounds/bg_gradient.gif) repeat-x 0 0; margin: 0 0 25px; padding: 20px; }
.map_container form { width: 270px; margin: 10px 0 0; float: left; }
.map_container label.instruction { width: 270px; }
.map_container form select { width: 270px; margin: 20px 0 10px; }
.map_container img { border: 0; }

.sliced-map { margin-left: 20px; float: left; position: relative; }
span.state_count { display: block; position: absolute; font-size: 0.75em; font-weight: bold;}
span.span_TAS {right: 85px; top: 240px}
span.span_ACT { top: 195px; left:340px; }
span.span_NSW { top: 185px; left:267px; }
span.span_QLD { top: 45px; left:240px; }
span.span_NT { top: 85px; left:125px; }
span.span_SA { top: 165px; left:120px; }
span.span_WA { top: 50px; left:20px; }
span.span_VIC { top: 220px; left:240px; }

/* SIDEBARS
---------------------------------------------------------- */
#sidebar { width: 280px; margin: 0 40px 0 0; float: left; }

.side_block { width: 280px; margin: 0 0 20px; padding: 0; border-top: 1px solid; float: left; }
#sidebar .side_block h3 { margin: 0; padding: 0 0 2px; border-bottom: 1px solid; }
#sidebar .side_block ul { padding: 10px 0 1px; }
.side_block .block_content ul { list-style: none; }
.side_block .block_content ul li { background: url(images/backgrounds/bg_arrow.gif) no-repeat 0 center; padding-left: 12px; -webkit-transition: background-color .4s ease-in-out; }
span.block_top {  }
.block_content {  }
span.block_bottom { height: 20px; display: block; clear: both; }

.side_block.alt { margin-right: 0; }
.side_block.widget { width: 280px; }
.widget_tag_cloud div { padding: 10px 0; }

/***** Subscribe block *****/
#subscribe_block { width: 250px; background: url(images/backgrounds/bg_stripe.gif) repeat 0 0; padding: 15px; }
#subscribe_block .side_block { width: 250px; margin: 0 0 18px; padding: 0; border: 0; float: none; }
#subscribe_block ul { width: 250px; margin: 0; padding: 10px 0; border-top: 1px solid; list-style: none; }
#subscribe_block #subscribe_rss ul li, #subscribe_block #subscribe_email ul li { background: none !important; padding-left: 0; clear: both; }
#subscribe_block #subscribe_rss ul li span { width: 18px; height: 18px; background: url(images/icons/icon_rss.png) no-repeat 0 0; margin: 0 4px 9px 0; float: left; display: block; }
#subscribe_block ul li a {  } /* Sales Alert */
#subscribe_block ul li a {  } /* Rentals Alert */
#subscribe_block #subscribe_email ul li span { width: 18px; height: 18px; background: url(images/icons/icon_email.png) no-repeat 0 0; margin: 0 4px 9px 0; float: left; display: block; }

/* Follow buttons */
#subscribe_block #follow.side_block { margin: 0; }
ul#follow_buttons { margin: 0; padding: 0; overflow: auto; }
ul#follow_buttons li { width: 50px; height: 50px; margin: 0 15px 20px 0; padding-left: 0; float: left; }
	ul#follow_buttons li.network4 { margin-right: 0; }
ul#follow_buttons li a { width: 50px; height: 50px; float: left; display: block; text-indent: -9999px; }

/* Recent Sales, Recent Lease */
#side_realty_widget_sold_listings-30.side_block { margin: 0; }
#sold_listings { width: 280px; margin: 0; padding: 0; }
#sold_listings .block_content { margin: 0 0 20px; }
#sold_listings table { margin: 8px 0 0; padding: 0; border: 0; }
#sold_listings td { padding: 2px 5px 2px 0; border: 0; border-bottom: 1px solid; }
#sold_listings td.street_address, #sold_listings td.sold_at { display: none; }
	#sold_listings td.suburb { width: 110px; }
	#sold_listings td.property_type { width: 60px; }
	#sold_listings td.bedrooms { width: 10px; text-align: right; }
	#sold_listings td.bathrooms { width: 10px; text-align: right; }
	#sold_listings td.carspaces { width: 10px; text-align: right; }
	#sold_listings td.last_price { width: 60px; text-align: right; padding-right: 0; }
#sold_listings p.button { margin: 10px 0 0; clear: both; }

/* Team Member */
#team_member .block_content { margin: 10px 0; }
#team_member img { width: 56px; height: auto; margin: 0 0 6px; }
#team_member .block_content .agent_item { width: 62px; float: left; margin-right: 10px; }
	#team_member .block_content .agent_item4 { margin: 0; }
#team_member .block_content .imageCont { clear: both; }
#team_member .block_content .descCont { clear: both; }
#team_member .tdContent { clear: both; }
#team_member .tdContent p { padding: 0 0 10px; }

/* Share and Enjoy */
#share_enjoy {  }
	body#property #share_enjoy { width: 140px;margin:20px 0;float: right; }
#share_enjoy a { text-decoration: none; }
	body#property #share_enjoy a { width: 42px; margin-right: 3px; display: block; overflow: hidden; font-size: 10px; text-align: center; }
table.share_enjoy { margin: 10px 0 0; padding: 0; border: 0; }
table.share_enjoy td { width: 33%; border: 0; padding: 0 0 5px; }
table.share_enjoy img { width: 16px; height: 16px; margin: 0 5px 0 0; border: 0; vertical-align: top; }
	body#property #share_enjoy table.share_enjoy img { margin: 0 auto; display: block; clear: both; }

.realty_widget_property_features{float: left;}
body#property .realty_widget_sales_data {margin:20px 0 0;}

/* Similar Listings */
#sidebar .realty_widget_similar_listings { width: 280px; }

.sim_content .block_content { margin: 10px 0 0; }
.sim_content .image 			{ width: 130px; height: 100px; margin: 0 10px 10px 0; float: left; position: relative; }
.sim_content .image img 		{ width: 124px; height: 94px; }
.sim_content .image_overlay 	{ position: absolute; top: 0; left: 0; }
.sim_content .image_overlay img { width: 124px; height: 94px; padding: 0; border: 0; background: transparent; }

.sim_content .similar_property_info { width: 140px; margin: 6px 0 0; float: left; }
.similar_property_info ul { margin: 0; padding: 0 !important; list-style: none; }
.similar_property_info ul li { background: none !important; margin: 0; padding: 0 !important; }

#other_properties .block_content { width: 260px; margin: 0 0 20px; padding: 10px; }
#other_properties p { padding: 0; }
#other_properties a { margin-right: 5px; }

.sim_content .block { width: 300px; margin: 0 20px 10px 0; float: left; }
	.realty_widget_sales_data .sim_content .block { width: 290px; }
	.realty_widget_property_research_sidebar .sim_content .block { width: 280px; float: none; }
.sim_content .block2, .sim_content .block4 { margin: 0; clear: right; }


/* PROPERTY
---------------------------------------------------------- */
/***** Quick Search *****/
#quick_search { width: 280px; }

ul.qs-ul { width: 280px; margin: 0; padding: 10px 0 0; list-style:none; }
ul.qs-ul li { width: 280px; background: none !important; padding: 0; clear: both; }

.search_selection { padding-bottom: 10px; }
.search_selection input.radio { margin-right: 5px; }

ul.qs-ul .property_type 			{ width: 280px; }
ul.qs-ul .property_type select 	{ width: 280px; margin: 0 0 10px; }

.enterSuburb 				{ width: 172px; margin-bottom: 10px; float: left; }
.enterSuburb input 		{ width: 159px; padding: 4px 2px 0; }

.prices { width: 135px; margin: 0 0 8px; float: left; }
	.price_min { margin-right: 10px; }
.prices select { width: 135px; }

.numberofrooms { width: 135px; margin: 0 0 10px 0; float: left; }
	.bedrooms { margin-right: 10px; }
.numberofrooms label { padding-bottom: 3px; }
.numberofrooms select { width: 135px; }

.suburb_select select, .states_select select { width: 280px; margin-bottom: 5px; clear: both; }

/***** Sorter and Search filter *****/	
p.number_properties		{ float: left; }
.sorter_search_quick 	{ float:right; }

#search_quick 			{ float: right; }
#search_quick select 	{  }

/*.search_results_view_option { width: 620px; height: 40px; clear: both; }
.search_results_view_option p { margin-right: 10px; float: left; }*/

/***** Page toolbar *****/
.page_toolbar 	{ width: 620px; margin-bottom: 12px; padding: 4px 0; border-top-width: 1px; border-top-style: solid; border-bottom-width: 2px; border-bottom-style: solid; clear: both; }
.page_toolbar p { padding: 0; text-align: center }
.page_prev 		{ float: left; margin-right: 10px; }
.page_prev a 	{ background: url(images/icons/icon_prev.gif) no-repeat 0 center; margin: 0; padding: 0 0 0 7px; }
.page_next 		{ margin-left: 10px;float: left; }
.page_next a 	{ background: url(images/icons/icon_next.gif) no-repeat right center; margin: 0; padding: 0 7px 0 0; }
.page_numbers	{ float: left; overflow: hidden; }
.page_numbers a	{ height: 8px; margin: 0 6px 0 1px; padding: 0; }
.page_prev a:hover, .page_next a:hover, .page_numbers a:hover,
.page_prev a:active, .page_next a:active, .page_numbers a:active {  }
.page_numbers a.current_page, .page_numbers a:hover {  }

/**** Property Search tables ****/
table.property 					{ width: 620px; margin: 0; padding: 0; border: 0; }
table.property td 				{ border: 0; border-bottom: 20px solid; }

td.imageCont 						{ width: 220px; padding: 0; }
td.imageCont .image 				{ width: 220px; height: 165px; margin-left: 20px; }
td.imageCont .image img 		{ width: 214px; height: 159px; }
td.imageCont .image_overlay img { width: 214px; height: 159px; margin: -80px 0 0 -103px; }

td.thumbCont						{ width: 300px; padding: 0; }
	td.thumbCont.alt				{ padding-right: 20px; }
td.thumbCont .image 				{ width: 300px; height: 225px;  }
td.thumbCont .image img 		{ width: 294px; height: 219px; }
td.thumbCont .image_overlay img { width: 214px; height: 159px; margin: -80px 0 0 -103px; }

td.descCont 						{ background: url(images/backgrounds/bg_td_descCont.gif) repeat-x 0 0; padding: 10px 0 0 0; }
	#list_format  td .tdContent { width: 380px; height: 155px; }
	#list_format  td .tdContent { width: 380px; height: 155px; }

td .tdContent { position: relative; }
	td.thumbCont .tdContent { width: 280px; height: 65px; padding: 35px 10px 0; clear: both; }

/***** Image overlays *****/
.image 				{ float: left; position: relative; }
.image_overlay 	{ width: 100%; }
.image_overlay img { background: transparent; margin: 0; padding: 0; border: 0; position: absolute; top: 50%; left: 50%; }
.image_overlay span { width: 125px; margin: -20px 0 0 -64px; display: block; position: absolute; left: 50%; top: 50%; color: #fff; font-size: 3em; line-height: 1em; font-weight: bold; text-align: center; }
	.image_overlay .sold span { font-size: 4em; }
	.image_overlay .leased span { margin: -18px 0 0 -63px; font-size: 2.75em; }
	.image_overlay .under_offer span { margin-top: -29px; line-height: .98em; }
	.image_overlay .style1.sold span { margin-top: -20px; font-size: 3.25em; }
	.image_overlay .style1.under_offer span { margin: -28px 0 0 -62px; font-size: 2.25em; }
	.image_overlay .style3 span { margin: -15px 0 0 -64px; font-size: 2em; text-transform: uppercase; }
	.image_overlay .style3.sold span {  margin: -16px 0 0 -63px; font-size: 2.5em; }
	.image_overlay .style3.under_offer span { margin: -23px 0 0 -62px; }
	.image_overlay .style3.leased span { margin: -14px 0 0 -63px; font-size: 2em; }
	.image_overlay .style5 img { top: 52%; }
	.image_overlay .style5.sold span { margin: -24px 0 0 -64px; }
	.image_overlay .style5.under_offer span { margin: -28px 0 0 -62px; font-size: 2.25em; }

ul.rooms { margin: 0; padding: 0; float: left; list-style: none; }
	#list_format ul.rooms { position: absolute; bottom: 4px; right: 1px; }
	#thumbnail_format ul.rooms { position: absolute; top: 7px; right: 1px; }
ul.rooms li { width: 38px; background: none; margin: 0 3px 0 0; padding: 0; float: left; }
ul.rooms li span { float: left; }
ul.rooms li span.room_count { padding-top: 1px; }
ul.rooms li span.room_type { width: 18px; height: 18px; background-repeat: no-repeat; background-position: center -41px; margin-left: 3px; float: left; display: block; text-indent: -9999px; }
ul.rooms li.bedrooms span.room_type { background-image: url(images/icons/beds.gif); }
ul.rooms li.bathrooms span.room_type { background-image: url(images/icons/baths.gif); }
ul.rooms li.carspaces span.room_type { background-image: url(images/icons/cars.gif); }

	#content #list_format p.property_type { padding: 0; position: absolute; bottom: 4px; left: 10px; }
	#content #list_format td.descCont p.price { padding: 0; position: absolute; bottom: 4px; left: 160px; }
	
	#content #thumbnail_format p.property_type { padding: 0; position: absolute; top: 7px; left: 10px; }
	#content #thumbnail_format td.thumbCont p.price { padding: 0; position: absolute; top: 7px; left: 95px; }

td.thumbCont p.suburb { border-top: 1px solid; }

.land_building_size 			{  }
.land_building_size p 		{ padding: 0 !important; }
.land_building_size p span { margin-left: 2px; padding-left: 4px; border-left: 1px solid; }
.land_building_size p span:first-child { margin: 0; padding: 0; border: 0; }

/***** Open for Inspection tables *****/
table.date_container			{ width: 620px; border: 0; border-bottom: 1px solid; }
table.date_container tr.alt { border-top: 1px solid; }
table.date_container tr:hover	{  }
table.date_container th,
table.date_container td			{ border: 0; vertical-align: top; text-align: left; }

table.date_container th		{ padding: 0; }
td.time_cell					{ width: 140px; padding-left: 0; }
td.suburb_cell					{ width: 155px; }
td.address_cell				{ width: 255px; }
td.property_link				{ width: 115px; }


/**** Property Page ****/
/* Property Page specific sidebar widgets */
#details ,#rental_season{ width: 250px; background: url(images/backgrounds/bg_stripe.gif) repeat 0 0; padding: 15px; }
#details table, #rental_season table{ margin: 10px 0 0; border: 0; }
#details table td, #rental_season table  td{ margin: 0; padding: 0 0 8px; border: 0; }
#details table td.field , #rental_season table td.field{  }
#details .block_content, #rental_season .block_content { padding: 0; border-top: 1px solid; border-bottom: 1px solid; border-bottom-color:#C7C7C7;
border-top-color:#FFFFFF;}
#details .block_content h5 , #rental_season .block_content h5{  }
span.dates_view {  }
#details .block_bottom , #rental_season .block_bottom { border-top: 1px solid; padding: 6px 0 0; }
#details .block_bottom p , #rental_season .block_bottom p { margin: 0; padding: 0; }
#details .block_bottom p span , #rental_season .block_bottom p span{ width: 3px; height: 5px; background: url(images/icons/icon_prev.gif) no-repeat 0 center; margin:7px 7px 0 0; float: left; display: block; }
#details .block_bottom p a , #rental_season .block_bottom p a { text-decoration: none; }

.realty_widget_stamp_duty_and_mortgage_calculator { width: 280px; background: url(images/backgrounds/bg_stripe.gif) repeat 0 0; }
.realty_widget_stamp_duty_and_mortgage_calculator h3 { display: none; }

.realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs { 
	width: 280px; height: 27px; padding: 0 !important; border: 0;
}
.realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs li {
	width: 140px; height: 27px; background: none; margin: 0; padding: 0; float: left; -webkit-transition: background-color .4s ease-in-out;
}
.realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs li a { 
	width: 130px; height: 19px; background: none; background-image: url(images/backgrounds/bg_calc_shadetab.png); background-repeat: no-repeat; background-position: 0 bottom; padding: 4px 5px; display: block; float: left; }
	
.realty_widget_stamp_duty_and_mortgage_calculator ul.shadetabs li.ui-state-active a, .realty_widget_stamp_duty_and_mortgage_calculator ul.ui-tabs-nav li.ui-tabs-selected a, .realty_widget_stamp_duty_and_mortgage_calculator ul.ui-tabs-nav li.ui-tabs-selected, .realty_widget_stamp_duty_and_mortgage_calculator ul.ui-tabs-nav li.ui-state-default {
	background: none !important; text-decoration: none;
}

table.calc_table { margin: 5px 5px 0; border: 0; }
table.calc_table td { border: 0; }
table.calc_table td.calc_field { font-size: 11px; }
table.calc_table td.calc_value { padding-bottom: 2px; }
table.calc_table td.calc_value input { width: 144px; }
table.calc_table td.calc_value select { width: 152px; }
table.calc_table td.calc_btn { padding: 15px 5px 10px; }
table.calc_table td.calc_btn p { margin: 0 10px 0 0; padding: 0; float: left; }
#calc_results, #calc_result { padding: 0 10px; }

#agentinfo_contactform.side_block { width: 280px; float: right; }
#agentinfo_contactform .block_content { margin: 5px 0; }
.agent_info { margin: 0 0 10px; }
.agent_info p { padding: 0; }
.agent_contact_info { padding-bottom: 10px; float: left; }
p.agent_photo, p.agent_photo img { width: 82px; }
	p.agent_photo { margin: 0 15px 10px 0; float: left; }
	p.agent_photo img {  }
span.agent_company_name {  }

/*ul.contact_agent_fields { margin: 0 0 20px; padding: 0; list-style: none; }
.side_block ul.contact_agent_fields li { background: none; margin: 0 0 8px; padding: 0; }
.side_block ul.contact_agent_fields li:hover { background: none; }
ul.contact_agent_fields li label { width: 79px; padding: 0 5px 0 0; float: left; text-align: right; }
ul.contact_agent_fields li label span { width: 79px; float: left; display: block; }
ul.contact_agent_fields li input, ul.contact_agent_fields li textarea { width: 187px; }

/* Property page specific content widgets */
.property_block { margin: 0 0 20px; }
#property_description { width: 460px; margin: 0 20px 0 0; float: left; }
.realty_widget_action_buttons, #add_to_favs, #rental_application { width: 140px; }
	.realty_widget_action_buttons.property_block { margin: 0; float: left; }
.realty_widget_action_buttons h4 { display: none; }
#property_tools p, #add_to_favs p, p.photo_btn { margin-bottom: 12px; }

.media_div img { margin-bottom: 10px; }
a.thumbimage { width: 193px; height: 146px; margin: 0 11px 10px 0; border: 0; float: left; display: block; }
	a.imagecount4, a.imagecount8, a.imagecount12, a.imagecount16, a.imagecount20, a.imagecount24 { margin-right: 0; }
a.thumbimage img { width: 187px; height: 140px; }

.media_div.with_scroller { width: 600px; padding: 10px; clear: both; }
.media_div.with_scroller img { margin: 0; }
table.media_table { margin: 0; border: 0; border: 1px solid; }
table.media_table td { padding: 0; border: 0; }
table.media_table img { border: 0; }
table.media_table td.td_slider_photo { border-right: 1px solid; }
td.td_photo_scroller { width: 450px; }
img#slide_photo { width: 450px; height: 338px; }
img#slide_photo_large { width: 800px; height: 638px; }
td.td_photo_scroller { width: 150px; }
td.td_photo_scroller img { width: 147px; height: 105px; border-bottom: 1px solid; }

#scroll-container {
overflow: hidden;
width: 147px;
height: 318px;
position: relative;
margin-bottom: -20px;
padding-bottom: 20px;
}
#scroll-content {
position: relative;
top: 0;
}#scroll-controls {
position: absolute;
bottom: 0;
left: 0;
height: 20px;
width: 147px;
}
#scroll-controls a.up-arrow {
display: block;
width: 19px;
height: 11px;
background: transparent url(images/icons/arrow_up.png) no-repeat scroll left top;
position: absolute;
top: 6px;
left: 45px;
}
#scroll-controls a.up-arrow:hover {
background: transparent url(images/icons/arrow_up.png) no-repeat scroll left top;
}
#scroll-controls a.down-arrow {
display: block;
width: 19px;
height: 11px;
background: transparent url(images/icons/arrow_down.png) no-repeat scroll left top;
position: absolute;
top: 6px;
right: 42px;
}
#scroll-controls a.down-arrow:hover {
background: transparent url(images/icons/arrow_down.png) no-repeat scroll left top;
}

#flashcontent { width: 620px; margin-bottom: 25px; position: relative; }
	body#ssp #flashcontent { width: 800px; margin: 0; }
/*span.help_tab { padding: 0; position: absolute; top: 0; right: 0; cursor: pointer; }
p#help_img { width: 620px; height: 142px; background-color: #fff; margin: 0; padding: 0; position: absolute; top: 25px; left: 0; z-index: 99; }
p#help_img img { width: 614px; height: 141px; }*/
span.help_tab { display: none; }

#SSP_content { width: 620px; }
	body#ssp #SSP_content { width: 800px; }
#photoshow_wrap {  }

.listing_photos { margin: 0 0 25px; }
.listing_photos #main_photo img { width: 620px; height: auto; }

#open_times { display: none; }

#property_features { width: 430px; padding: 4px 15px 8px; clear: both; }
#property_features p { padding: 0; }
#property_features small { padding: 0 1px; vertical-align: middle; font-size: 1.5em; }
#property_features p span:last-child small { display: none; }

/***** MAPS *****/
.property_map .block_content { margin: 10px 0 15px; position: relative; }
#map_canvas {   height: 380px ; border: 3px solid; }
	body#print-property #map_canvas {  }
#street_view_button{ width:6em; margin: 0 0 3px; padding:4px 0 5px; border: 1px solid; cursor:pointer; position: absolute; top: 0; left: 34px; }

#map_suburb { width: 300px; margin: 10px; float: right; }
#map_suburb span { margin: 0 10px 10px 0;padding:0;}
.gsmsc-mapDiv { height: 275px !important; }
.gsmsc-idleMapDiv { height : 275px !important; }
div.gsmsc-idleMapDiv span, div.gsmsc-mapDiv span { display: none; }

#walk_score { width: 600px !important; margin: 0 0 25px; }

/* Property Stats table/chart below map */

#property_stats { width: 600px; margin: -1px 0 25px; padding: 10px; border: 1px solid; clear: both; }
#data_table_div { width: 280px; margin: 0 10px 0 0; float: left;  }
#content #property_stats table.DataTable { width: 280px; margin: 0 0 5px; border-bottom: 0; }
#content #property_stats table tbody td { padding: 0 5px; }
#content #property_stats table tr.DataTableHeader td { padding: 5px; }
#content #property_stats table tr.DataTableSubHeader td {  }
#content #property_stats table tr.DataTableFooter td {  }
#data_table_div small { line-height: 14px; }
#property_stats .prop_stats_chart { width: 280px !important; float: left; }
#content #property_stats table.LargeDataTable { width: 100%; }

table.results { width: 600px; }
table.sales_data_table { width: 620px; }
table.comparable_sales_table td.yield, table.comparable_sales_table td.sale_price, table.comparable_sales_table td.net_rental_pa, table.comparable_sales_table td.date{ text-align: center; }
table.sales_data_table td.yield, table.sales_data_table td.trend, table.sales_data_table td.auction, table.sales_data_table td.days, table.sales_data_table td.price, table.sales_data_table td.growth, table.sales_data_table td.discount { text-align: center; }
table.sales_data_table td.postcode, table.sales_data_table td.type, table.sales_data_table td.map, table.sales_data_table td.view { text-align: center; }

/***** Location Information *****/
#side_realty_widget_location_info-9 { width: 620px; clear: both; }

	.suburb_profile_banner p { width: 200px; height: 10px; position: absolute; top: 17px; right: 5px; }
		.suburb_profile_banner p a { width: 200px; height: 10px; display: block; text-indent: -9999px; }
	
	.location_content {  }
	p.location_main_image { width: 140px; height: 105px; margin: 13px 20px 25px 0; padding: 0; float: left; }
		p.location_main_image img { width: 134px; height: 99px; }
	p.location_info { margin: 14px 0 0; padding: 0; float: left; }
	p.location_info span {  }
	p.location_description {  }

/* Contact Forms
---------------------------------------------------------- */
ol.cf-ol { margin: 0 0 25px; padding: 0; list-style: none; }
ol.cf-ol li { background: none; margin: 0 0 15px; padding: 0; clear: both; }
ol.cf-ol li label { width: 120px; margin: 0 10px 0 0; float: left; text-align: right; }
ol.cf-ol li label span { width: 120px; display: block; }
ol.cf-ol input.checkbox { margin-right: 5px; }
.signup_button p.email_btn { position: relative; left: 130px; }
textarea#comments { margin: 0; clear: none; }

#formpart { width: 260px; padding: 15px 10px 10px; }
#formpart form { width: 260px; margin: 0; border: 0; }
#formpart ol.cf-ol li { clear: both; }
#formpart ol.cf-ol li label { width: 90px; }
#formpart ol.cf-ol li label span { width: 90px; }
#formpart input, #formpart textarea { width: 150px; }
#formpart select { width: 155px; }
#formpart p.requi { width: 90px; float: left; }
#formpart .signup_button p.email_btn { position: relative; top: 0; left: 100px; }

li.site_alerts ul { width: 325px; margin: 0; padding: 0; float: left; display: block; list-style: none; }
	li.site_alerts ul.prop_alerts_ul { margin-bottom: 10px; }
li.site_alerts ul li { margin: 0 0 5px; padding: 0; }

/* Print Property
------------------------------------------------------------------*/
body#print-property #container { background: none; }
body#print-property #content { padding: 0 0 0 19px; border-left: 1px solid #ddd; }
#page_nav_buttons { font-weight: bold; text-align: right; position: absolute; top: 40px; right: 28px; }
body#print-property #property_description { width: 430px; margin: 0 0 25px; float: none; }
body#print-property #property_images_medium { width: 430px; height: 340px; margin: 0 0 15px; }
body#print-property #property_images_medium .photo { width: 210px; height: 158px; margin: 0 10px 10px 0; float: left; }
	body#print-property #property_images_medium .photo0, body#print-property #property_images_medium .photo2 { margin-right: 0; }
body#print-property #property_images_medium .photo img { width: 208px; height: 156px; border: 1px solid #333; }
body#print-property #map_canvas { width: 428px !important; height: 321px !important; border: 1px solid #333; }
body#print-property #sidebar { margin: 0 20px 0 0; }
body#print-property #details { width: 248px; background: url(images/backgrounds/bg_stripe.gif) repeat 0 0; padding: 15px; border: 1px solid #ddd; }

/* lightbox
------------------------------------------------------------------*/
#lightbox					{ width: 100%; position: absolute; left: 0; z-index: 100; text-align: center; line-height: 0; }
#lightbox a, #lightbox a img, #lightbox img { border: 0; padding: 0; }

#outerImageContainer		{width: 250px; height: 250px; background-color: #fff; margin: 0 auto; border: 4px solid #8b8b8b; border-bottom: 0; position: relative; }
#imageContainer				{ padding: 10px; }

#loading					{ width: 100%; height: 25%; position: absolute; top: 40%; left: 0%; text-align: center; line-height: 0; }
#hoverNav					{ width: 100%; height: 100%; position: absolute; top: 0; left: 0; z-index: 10; }
#imageContainer>#hoverNav	{ left: 0;}
#hoverNav a{ outline: none;}

#prevLink, #nextLink		{ width: 49%; height: 100%; background: transparent url(images/lightbox/blank.gif) no-repeat; /* Trick IE into showing hover */
	display: block; }
#prevLink 					{ background: url(images/lightbox/prevlabel.gif) left 15% no-repeat; left: 0; float: left;}
#nextLink 					{ background: url(images/lightbox/nextlabel.gif) right 15% no-repeat; right: 0; float: right;}
#prevLink:hover, #prevLink:visited:hover {  }
#nextLink:hover, #nextLink:visited:hover {  }

#imageDataContainer			{ width: 100%; background-color: #fff; margin: 0 auto; border: 4px solid #8b8b8b; border-top: 0; font: 10px Verdana, Helvetica, sans-serif; line-height: 1.4em; overflow: auto; }

#imageData					{	padding:0 10px; color: #666; }
#imageData #imageDetails	{ width: 70%; float: left; text-align: left; }	
#imageData #caption			{ font-weight: bold;	}
#imageData #numberDisplay	{ display: block; clear: left; padding-bottom: 1.0em;	}			
#imageData #bottomNavClose	{ width: 66px; float: right;  padding-bottom: 0.7em;	}	
		
#overlay					{ width: 100%; height: 500px; position: absolute; top: 0; left: 0; z-index: 90; }

/* LIGHTBOX ENDS */

/* View Property in map resutl 
------------------------------------------------------------------*/
#property_detail{		height: 500px !important;		}

/* calendar
------------------------------------------------------------------*/
img.tcalIcon {
	cursor: pointer;
	margin-left: 1px;
	vertical-align: middle;
}
div#tcal {
	position: absolute;
	visibility: hidden;
	z-index: 100;
	width: 158px;
	padding: 2px 0 0 0;
}
div#tcal table {
	width: 100%;
	border: 1px solid silver;
	border-collapse: collapse;
	margin: 0px; padding: 0px;
	background-color: white;
}
div#tcal table td img { border:0 none;}
div#tcalShade table td img { border:0 none;}
.numberofrooms img { border:0 none;}
div#tcal table.ctrl {
	border-bottom: 0;
}
div#tcal table.ctrl td {
	width: 15px;
	height: 20px;
}
div#tcal table.ctrl th {
	background-color: white;
	color: black;
	border: 0;
}
div#tcal th {
	border: 1px solid silver;
	border-collapse: collapse;
	text-align: center;
	padding: 3px 0;
	font-family: tahoma, verdana, arial;
	font-size: 10px;
	background-color: gray;
	color: white;
}
div#tcal td {
	border: 0;
	border-collapse: collapse;
	text-align: center;
	padding: 2px 0;
	font-family: tahoma, verdana, arial;
	font-size: 11px;
	width: 22px;
	cursor: pointer;
}
div#tcal td.othermonth {
	color: silver;
}
div#tcal td.weekend {
	background-color: #ACD6F5;
}
div#tcal td.today {
	border: 1px solid red;
}
div#tcal td.selected {
	background-color: #FFB3BE;
}
iframe#tcalIF {
	position: absolute;
	visibility: hidden;
	z-index: 98;
	border: 0;
}
div#tcalShade {
	position: absolute;
	visibility: hidden;
	z-index: 99;
}
div#tcalShade table {
	border: 0;
	border-collapse: collapse;
	width: 100%;
	height: 20px;
}
div#tcalShade table td {
	border: 0;
	border-collapse: collapse;
	padding: 0;
}
/* calendar END
------------------------------------------------------------------*/

/* availability widget -- holidays rental enquiru form
------------------------------------------------------------------*/
.holiday_enquiry_fields {  }
.holiday_enquiry_fields p { margin: 0 0 10px; padding: 0; clear: both; }
.holiday_enquiry_fields p label { width: 85px; margin: 0; float: left; }
.holiday_enquiry_fields p label span { width: 85px; display: block; }
	.holiday_enquiry_fields p.spam_question label { width: 115px; }
	.holiday_enquiry_fields p.spam_question label span { width: 115px; }
.holiday_enquiry_fields p input, .holiday_enquiry_fields textarea { width: 132px; }
	.holiday_enquiry_fields p input { padding: 2px !important; }
	.holiday_enquiry_fields textarea { margin-bottom: 2px; }
	
.holiday_enquiry_fields .nights p { width: 100px; padding: 0 5px; float: left; clear: none; }
	input#first_night, input#last_night { width: 80px !important; margin-right: 5px; float: left; }
	span#reset_first_night, span#reset_last_night { float: left; background: #ededed; }
	.holiday_enquiry_fields p.spam_question input { width: 102px; margin-bottom: 15px; }
	
.side_block .block_content #legend ul li {padding:0;}
#sidebar #legend ul li { padding:0;}
#calendars { width: 100%; margin-bottom: 10px; background-color:#fff; }
#legend { margin-bottom: 10px; padding: 5px; border: 1px solid #343434; }
#legend ul { padding: 0; list-style: none; }
#legend ul li { width: 50%; float: left; }
#legend ul li span { width: 14px; height: 16px; margin: 0 5px 5px 0; border: 1px solid #434343; float: left; display: block; text-align: center; }

select.calendar_select { width: 196px; float: left; }
span#prev_month { width: 12px; padding-left: 3px; float: left; display: block; }
span#next_month { width: 12px; padding-left: 3px; float: left; display: block; }

.calendar_table { width: 100%; }
.calendar_table th { text-align: center; }
.calendar_cells td { background-color:#dcdcdc; border: 1px solid #434343; color:#fff; text-align: center; }
.calendar_cells a { padding: 2px; display: block; text-decoration:none; color: #434343; }
.calendar_cells .today { font-weight:bold; color: #343434; }
.available {  }
.normal { background: #dcdcdc; color: #000; }
.peak a, .peak{ background: #ff0000; color: #fff; }
.calendar_table .week_days{  }
.hidden { display: none; }

#prev_month, #next_month{ background: #dcdcdc; color: #000; }

.toggle_dates, #prev_month, #next_month, #reset_first_night, #reset_last_night{ cursor: pointer; }
.mid a, .mid { background:#CCFFFF;color: #000;}
.high a, .high {background:#E0F98D;color: #000;}
.not_available a, .not_available{background: #dcdcdc; color: #fff;}

/*label {display:block;margin:2px;} */
.form {padding:0px;margin:0px;background-color:#EDECDC;}
.form li {width:190px;margin:3px;padding:5px 5px 5px 30px;list-style:none;position:relative;}
*html .form li {left:-15px;}
.form li img {position:absolute;left:5px;}
.form .error {border:1px solid #A90000;padding:4px 4px 4px 29px;background-color:#F8E5E5;}
.form .success {border:1px solid #74F019;padding:4px 4px 4px 29px;background-color:#DEF8CA;}
.form .selected {border:1px solid #1AA8E1;padding:4px 4px 4px 29px;}
#login_table .pad {padding:15px;}
.form input.login {padding:2px 7px;width:auto;}
.form input {width:180px;}

.validate .error {border:1px solid #A90000;padding:0 0 5px 20px;background: #F8E5E5 url(images/form/exclamation.gif) no-repeat; }
.loading{background: url(images/form/loading.gif) no-repeat; padding:0 0 5px 20px}
.validate .error_message{font-size:10px;}
.validate .success {border:1px solid #74F019;background:#DEF8CA url(images/form/accept.gif) no-repeat; padding:0 0 5px 20px}
.validate .selected {}
.toggle_dates { color: #ed1849; }

/* availability widget -- holidays rental enquiru form END
------------------------------------------------------------------*/