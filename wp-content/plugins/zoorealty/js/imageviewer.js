var slideshow_width='569px'; //SET IMAGE WIDTH
var slideshow_height='371px'; //SET IMAGE HEIGHT
var thumbimage_width=73; //SET IMAGE WIDTH
var thumbimage_height=60; //SET IMAGE HEIGHT
var pause=5000; //SET PAUSE BETWEEN SLIDE (3000=3 seconds)
var restarea=6 //1) width of the "neutral" area in the center of the gallery in px
var maxspeed=6 //2) top scroll speed in pixels. Script auto creates a range from 0 to top speed.

var menu_width;
var curpos=10;
var degree=10;
var curcanvas="canvas0";
var curimageindex=0;
var nextimageindex=1;
var previmageindex=0;
var scrollDone=0;
var temp;
var timeloop;
var timeloop2;
var stageimage=0;
var scrollborder;
var scroller;
var actualwidth;
var sliderUp;
var sliderUp2
var sliderDown;
var sliderDown2;
var borderUp;
var borderDown;
var scrollyReset;
var movestate;
var iedom=document.all||document.getElementById;
var scrollspeed=5;
var movestate="";
var fadeimagesLoaded=0;
var thumbimagesLoaded=0;
var cross_scroll;
var loadedyes=0;
var Swirl=0;
var fadeDone=0;
var thumbsDone=0;
var imageWait;
var fadewait;
var faderDrawn = 0;
var allImagesLoaded = 0;
var borderGoto;
var borderSPos=0;
var sliderGoto;
var sliderSPos=0;
var allFade;
var allThumn;
var waitforscroller;
var totalLoaded;

var ie4=document.all
var dom=document.getElementById

// Fader

function fadepic(){
if (curpos<100){
curpos+=10
if (tempobj.filters)
tempobj.filters.alpha.opacity=curpos
else if (tempobj.style.MozOpacity)
tempobj.style.MozOpacity=curpos/101
}
else{
clearInterval(dropslide)
nextcanvas=(curcanvas=="canvas0")? "canvas0" : "canvas1"
tempobj=ie4? eval("document.all."+nextcanvas) : document.getElementById(nextcanvas)
tempobj.innerHTML=insertimage(nextimageindex)
nextimageindex=(nextimageindex<fadeimages.length-1)? nextimageindex+1 : 0
var tempobj2=ie4? eval("document.all."+nextcanvas) : document.getElementById(nextcanvas)
tempobj2.style.visibility="hidden"
timeloop2=setTimeout("sliderScroll()",pause)
}
}

function insertimage(i) {
	var tempcontainer = fadeimages[i][1]!=""? '<a href="'+fadeimages[i][1]+'" target="'+fadeimages[i][2]+'">' : "";
	tempcontainer += '<img src="'+fadeimages[i][0]+'" border="0">';
	tempcontainer = fadeimages[i][1]!=""? tempcontainer+'</a>' : tempcontainer;
	return tempcontainer;
}

function rotateimage() {
	if(ie4 || dom) {
		resetit(curcanvas);
		var crossobj = tempobj = ie4? eval("document.all."+curcanvas) : document.getElementById(curcanvas);
		crossobj.style.zIndex++;
		tempobj.style.visibility = "visible";
		var temp = 'setInterval("fadepic()",50)';
		dropslide = eval(temp);
		curcanvas = (curcanvas=="canvas0")? "canvas1" : "canvas0";
	}
	else {
		document.images.defaultslide.src = fadeimages[curimageindex][0];
		curimageindex = (curimageindex<fadeimages.length-1)? curimageindex+1 : 0;
	}
}

function sliderScroll() {
	waitforscroller = setInterval("scrollFinished()",20);
	curimageindex=(curimageindex<fadeimages.length-1)? curimageindex+1 : 0;
	if(curimageindex == 0) {
		stageimage = fadeimages.length - 1;
	}
	else {
		stageimage = curimageindex - 1;
	}
	scrollborder = eval(document.getElementById("imageborder"));
	scroller = eval(document.getElementById("propslide"));
//	actualwidth=scroller.offsetWidth;
	borderSPos = parseInt(scrollborder.style.left);
	borderGoto = parseInt(83 * stageimage);
	sliderSPos = parseInt(scroller.style.left);
	sliderGoto = parseInt("-"+parseInt(83 * stageimage)) + 332;
	if(parseInt(scrollborder.style.left)<=(actualwidth-83)) {
		if(window.borderDown2) clearInterval(borderDown2);
		if(window.borderDown) clearTimeout(borderDown);
		borderDown2 = setInterval("borderSDown()",2);
	}
	if((parseInt(scrollborder.style.left) > 0 && parseInt(scroller.style.left)>(menu_width-actualwidth)) || (stageimage == 0 && parseInt(scroller.style.left)<0)) {
		if(window.sliderUp2) clearInterval(sliderUp2);
		if(window.sliderUp) clearTimeout(sliderUp);
		sliderUp2 = setInterval("sliderSUp()",2);
	}
}

function scrollFinished() {
	if(parseInt(scrollborder.style.left) == borderGoto) {
		clearInterval(waitforscroller);
		rotateimage();
	}
}

function borderSDown() {
	if(borderSPos > borderGoto) {
		if(parseInt(scrollborder.style.left) > borderGoto) {
			var borderPxToScroll = 5;
		}
		else {
			var borderPxToScroll = (borderGoto) - parseInt(scrollborder.style.left);
		}
		scrollborder.style.left = parseInt(scrollborder.style.left) - borderPxToScroll + "px";
		borderSPos = parseInt(scrollborder.style.left);
		borderDown = setTimeout("borderSDown()",1);
	}
	else if(borderSPos < borderGoto) {
		if(parseInt(scrollborder.style.left) < borderGoto) {
			var borderPxToScroll = 1;
		}
		else {
			var borderPxToScroll = parseInt(scrollborder.style.left) - (borderGoto);
		}
		scrollborder.style.left = parseInt(scrollborder.style.left) + borderPxToScroll + "px";
		borderSPos = parseInt(scrollborder.style.left);
		borderDown = setTimeout("borderSDown()",1);
	}
	else {
//		if(window.borderDown) clearTimeout(borderDown);
		if(window.borderDown2) clearInterval(borderDown2);
	}
}

function sliderSUp() {
	
	if(sliderSPos < sliderGoto && movestate == "" && parseInt(scroller.style.left) < 0) {
		var sliderDir = "Up";
		if(parseInt(scroller.style.left) < sliderGoto) {
			var sliderPxToScroll = 5;
		}
		else {
			var sliderPxToScroll = (sliderGoto) + parseInt(scroller.style.left);
		}
		scroller.style.left = parseInt(scroller.style.left) + sliderPxToScroll + "px";
		sliderSPos = parseInt(scroller.style.left);
		sliderUp = setTimeout("sliderSUp()",1);
	}
	else if(sliderSPos > sliderGoto && movestate == "" && parseInt(scroller.style.left) > (menu_width-actualwidth)) {
		var sliderDir = "Down";
		if(parseInt(scroller.style.left) >= sliderGoto) {
			var sliderPxToScroll = 1;
		}
		else {
			var sliderPxToScroll = parseInt(scroller.style.left) - (sliderGoto);
		}
		scroller.style.left = parseInt(scroller.style.left) - sliderPxToScroll + "px";
		sliderSPos = parseInt(scroller.style.left);
		sliderUp = setTimeout("sliderSUp()",1);
	}
	else if(movestate != "") {
		if(window.sliderUp2) clearInterval(sliderUp2);
		sliderUp2 = setInterval("sliderSUp()",1);
	}
	else {
//			if(window.sliderUp) clearTimeout(sliderUp);
			if(window.sliderUp2) clearInterval(sliderUp2);
	}
}

function resetit(what) {
	curpos = 10;
	var crossobj = ie4? eval("document.all."+what) : document.getElementById(what);
	if(crossobj.filters) {
		crossobj.filters.alpha.opacity=curpos;
	}
	else if(crossobj.style.MozOpacity) {
		crossobj.style.MozOpacity=curpos/101;
	}
}

function faderStart() {
	var crossobj = ie4? eval("document.all."+curcanvas) : document.getElementById(curcanvas);
	crossobj.innerHTML = insertimage(curimageindex);
	sliderScroll();
}

function watchForClick(newImageNum) {
	curimageindex = newImageNum;
	nextimageindex=(curimageindex<fadeimages.length-1)? curimageindex+1 : 0;
	clearInterval(temp);
	clearInterval(timeloop);
	clearTimeout(timeloop2);
	clearInterval(dropslide);
	if(window.waitforscroller) clearInterval(waitforscroller);
	faderStart();
}


// Vertical Thumb Scroller
function drawThumbImages() {
	var tempth = ie4? eval("document.all.propslide") : document.getElementById("propslide");
	var propslideimages = '<div id="imageborder" style="left: 0px; top: 0px; "><img src="'+borderimg+'"></div>\n';
	for(i = 0;i < thumbimages.length;i++) {
		propslideimages += '<div id="smallpropimageContainer"><div id="smallpropimage"><a href="'+thumbimages[i][1]+'" border="0"><img src="' + thumbimages[i][0] + '"></a></div></div>\n';
	}
	tempth.innerHTML = propslideimages;
}

function drawFader() {
	if(faderDrawn == 0) {
		document.getElementById("mainpropimage").innerHTML = '<div style="position:relative;width:'+slideshow_width+';height:'+slideshow_height+';overflow:hidden"><div  id="canvas0" style="position:absolute;width:'+slideshow_width+';height:'+slideshow_height+';top:0;left:0;filter:alpha(opacity=10);-moz-opacity:10"></div><div id="canvas1" style="position:absolute;width:'+slideshow_width+';height:'+slideshow_height+';top:0;left:0;filter:alpha(opacity=10);-moz-opacity:10;visibility: hidden"></div></div>';
		faderDrawn = 1;
	}
}

function moveleft() {
	if(loadedyes) {
		movestate="left";
		if(iedom&&parseInt(cross_scroll.style.left)>(menu_width-actualwidth)) {
			cross_scroll.style.left=parseInt(cross_scroll.style.left)-scrollspeed+"px";
		}
	}
	lefttime=setTimeout("moveleft()",5);
}

function moveright() {
	if(loadedyes) {
		movestate="right";
		if(iedom&&parseInt(cross_scroll.style.left)<0) {
			cross_scroll.style.left=parseInt(cross_scroll.style.left)+scrollspeed+"px";
		}
	}
	righttime=setTimeout("moveright()",5);
}

function ietruebody() {
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;
}

function getposOffset(what, offsettype) {
	var totaloffset = (offsettype=="left")? what.offsetLeft: what.offsetTop;
	var parentEl = what.offsetParent;
	while(parentEl != null) {
		totaloffset = (offsettype == "left")? totaloffset + parentEl.offsetLeft : totaloffset + parentEl.offsetTop;
		parentEl = parentEl.offsetParent;
	}
	return totaloffset;
}

function motionengine(e) {
	var dsocx=(window.pageXOffset)? pageXOffset: ietruebody().scrollLeft;
	var dsocy=(window.pageYOffset)? pageYOffset : ietruebody().scrollTop;
	var curposy=window.event? event.clientX : e.clientX? e.clientX: "";
	curposy -= mainobjoffset-dsocx;
	var leftbound = (menu_width-restarea)/2;
	var rightbound = (menu_width+restarea)/2;
	if(curposy > rightbound) {
		scrollspeed = (curposy-rightbound)/((menu_width-restarea)/2) * maxspeed;
		if(window.righttime) {
			clearTimeout(righttime);
		}
		if(movestate != "left") {
			moveleft();
		}
	}
	else if(curposy < leftbound) {
		scrollspeed = (leftbound-curposy) / ((menu_width-restarea)/2) * maxspeed;
		if(window.lefttime) {
				clearTimeout(lefttime);
		}
		if(movestate!="right") {
			moveright();
		}
	}
	else {
		scrollspeed=0;
	}
}

function contains_ns6(a, b) {
	while (b.parentNode) {
		if ((b = b.parentNode) == a) {
			return true;
		}
	}
	return false;
}

function stopmotion(e){
	if((window.event&&!crossmain.contains(event.toElement)) || (e && e.currentTarget && e.currentTarget!= e.relatedTarget && !contains_ns6(e.currentTarget, e.relatedTarget))) {
		if(window.righttime) {
				clearTimeout(righttime);
		}
		if(window.lefttime) {
			clearTimeout(lefttime);
		}
		movestate="";
	}
}

function fillup() {
	if(iedom) {
		crossmain=document.getElementById? document.getElementById("propimages") : document.all.propimages;
		menu_width= parseInt(crossmain.offsetWidth);
		mainobjoffset=getposOffset(crossmain, "left");
		cross_scroll=document.getElementById? document.getElementById("propslide") : document.all.propslide;
		actualwidth= (thumbimage_width + 10) * thumbimages.length;
		crossmain.onmousemove=function(e) {
			motionengine(e);
		}
		crossmain.onmouseout=function(e) {
			stopmotion(e);
		}
	}
	if(window.opera) {
		cross_scroll.style.left=menu_width-actualwidth+'px';
		setTimeout('cross_scroll.style.left=0', 10);
	}
	loadedyes=1;
}

// Initalisation .. All required to make sure images are loaded prio to anything being displayed.

function init() {
	imageWait = setInterval("waitForLoad()",500);
	allThumb = setInterval("thumbpreloaderCheck()",100);
	allFade = setInterval("fadepreloaderCheck()",100);
	if(use_gmap == 1) {
		googleLoad();		
	}
}

function fadepreloaderCheck() {
	if(fadeimagesLoaded == fadeimages.length) {
		clearInterval(allFade);
		fadeDone = 1;
	}
}

function thumbpreloaderCheck() {
	if(thumbimagesLoaded == thumbimages.length) {
		clearInterval(allThumb);
		thumbsDone = 1;
	}
}

function waitForLoad() {
	if(thumbsDone == 1 && fadeDone == 1) {
		clearInterval(imageWait);
		finalInit();
	}
	else {
		pleaseWait();
	}
}

function pleaseWait() {
	totalLoaded = thumbimagesLoaded + fadeimagesLoaded;
	var htmlcode = totalLoaded + ' of ' + totalImages + ' images loaded...';
	BetterInnerHTML(document.getElementById("imageloadcount"),htmlcode);
}

function finalInit() {
	drawThumbImages();
	drawFader();
	fillup();
	faderStart();
}

// JavaScript Document
function BetterInnerHTML(element, HTML, clearfirst) {

	// load the HTML as XML
	function Load(xmlString) {
		var xml;
		if (typeof DOMParser != "undefined") xml = (new DOMParser()).parseFromString(xmlString, "application/xml");
		else {
			var ieDOM = ["MSXML2.DOMDocument", "MSXML.DOMDocument", "Microsoft.XMLDOM"];
			for (var i = 0; i < ieDOM.length && !xml; i++) {
				try { xml = new ActiveXObject(ieDOM[i]); xml.loadXML(xmlString); }
				catch(e) {}
			}
		}
		return xml;
	}

	// recursively copy the XML into the DOM
	function Copy(domNode, xmlDoc, level) {

		if (typeof level == "undefined") level = 1;
		if (level > 1) {

			if (xmlDoc.nodeType == 1) {

				// element node
				var thisNode = document.createElement(xmlDoc.nodeName);

				// attributes
				for (var a = 0, attr = xmlDoc.attributes.length; a < attr; a++) {
					var aName = xmlDoc.attributes[a].name, aValue = xmlDoc.attributes[a].value, evt = (aName.substr(0,2) == "on");
					if (!evt) {
						switch (aName) {
							case "class": thisNode.className = aValue; break;
							case "for": thisNode.htmlFor = aValue; break;
							default: thisNode.setAttribute(aName, aValue);
						}
					}
				}

				// append node
				domNode = domNode.appendChild(thisNode);

				// attach event
				if (evt) domNode[aName] = function() { eval(aValue); };
			}
			else if (xmlDoc.nodeType == 3) {
				// text node
				var text = (xmlDoc.nodeValue ? xmlDoc.nodeValue : "");
				var test = text.replace(/^\s*|\s*$/g, "");
				if (test.indexOf("<!--") != 0 && test.indexOf("-->") != (test.length - 3)) domNode.appendChild(document.createTextNode(text));
			}
		}

		// do child nodes
		for (var i = 0, j = xmlDoc.childNodes.length; i < j; i++) Copy(domNode, xmlDoc.childNodes[i], level+1);
	}

	// load the XML and copies to DOM
	HTML = "<root>"+HTML+"</root>";
	var xmlDoc = Load(HTML);
	if (element && xmlDoc) {
		if (clearfirst != false) while (element.lastChild) element.removeChild(element.lastChild);
		Copy(element, xmlDoc.documentElement);
	}
}
//window.onload=init;