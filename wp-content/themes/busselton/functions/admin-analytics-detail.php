	<h2><?php echo $title; ?></h2>
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col">Date</th>
			<th scope="col" style="text-align: center">Num<br/>Hits</th>
			<th scope="col" style="text-align: center">Num<br/>Emails</th>
			<th scope="col" style="text-align: center">Brochure<br/>Views</th>
			<th scope="col" style="text-align: center">Email to<br/>Friend</th>
			<th scope="col" style="text-align: center">Email to<br/>Self</th>
			<th scope="col" style="text-align: center">Share on<br/>Twitter</th>
			<th scope="col" style="text-align: center">Share on<br/>Facebook</th>
			<th scope="col" style="text-align: center">Shortlisted</th>
		</tr>
		</thead>
		<tbody id="the-list">
			<?php if(!empty($properties)){
			 $i=0;foreach ($properties as $property): if(!is_array($property))continue; ?>
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
			<td><?php echo date("l, F d, Y", strtotime($property['visited_at'])); ?></td>
			<td style="text-align: center"><?php echo $property['visit']; ?> </td>
			<td style="text-align: center"><?php echo $property['enquiry']; ?></td>
			<td style="text-align: center"><?php echo $property['brochure']; ?></td>
			<td style="text-align: center"><?php echo $property['email_friend']; ?></td>
			<td style="text-align: center"><?php echo $property['email_self']; ?></td>
			<td style="text-align: center"><?php echo $property['share_on_twitter']; ?></td>
			<td style="text-align: center"><?php echo $property['share_on_facebook']; ?></td>
			<td style="text-align: center"><?php echo $property['favorite']; ?></td>
			</tr>
			<?php $i++; endforeach; } ?>
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
			<td>TOTAL</td>
			<td style="text-align: center"><?php echo ($total['visit']=='')?'0':$total['visit']; ?></td>
			<td style="text-align: center"><?php echo ($total['enquiry']=='')?'0':$total['enquiry']; ?></td>
			<td style="text-align: center"><?php echo ($total['brochure']=='')?'0':$total['brochure']; ?></td>
			<td style="text-align: center"><?php echo ($total['email_friend']=='')?'0':$total['email_friend']; ?></td>
			<td style="text-align: center"><?php echo ($total['email_self']=='')?'0':$total['email_self']; ?></td>
			<td style="text-align: center"><?php echo ($total['share_on_twitter']=='')?'0':$total['share_on_twitter']; ?></td>
			<td style="text-align: center"><?php echo ($total['share_on_facebook']=='')?'0':$total['share_on_facebook']; ?></td>
			<td style="text-align: center"><?php echo ($total['favorite']=='')?'0':$total['favorite']; ?></td>
			</tr>
		</tbody>
	</table>
	<p><a href="javascript:history.go(-1);"><?php _e('&laquo; Back'); ?></a></p>