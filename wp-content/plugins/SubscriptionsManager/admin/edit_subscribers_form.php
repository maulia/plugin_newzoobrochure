<script type="text/javascript">
//<!--
function open_query_form(element){
	if(element.value != 'sale' && element.value != 'lease')
		return;
	if(element.checked==1)
		document.getElementById(element.value + '_search').style.display='block';
	else
		document.getElementById(element.value + '_search').style.display='none';
}

//-->
</script>
<style type="text/css">
	.select_suburb{
		height:auto !important;
	}
</style>
<div class="wrap">
<h3><a href="<?php echo $this->form_action; ?>"><?php _e('&laquo; Back to Subscribers List'); ?></a></h3>
<h2><?php _e('Add/Edit Subscribers'); ?></h2>

<form name="" id="your-profile" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="task" value="<?php echo $_GET['action']; ?>" />
<input type="hidden" name="subscriber_id" value="<?php echo $subscriber['subscriber_id']; ?>" />

<p class="submit"><input type="submit" value="<?php _e('Save &raquo;') ?>" name="update_subscriber" /></p>

<table class="form-table">
	<tr>
	<th><label>Title</label></th>
	<td><div class="name_title">
	<select name="title">
		<option value="">Title</option>
		<option value="Miss"<?php if($subscriber['title']=='Miss') echo ' selected="selected"'; ?>>Miss</option>
		<option value="Mr"<?php if($subscriber['title']=='Mr') echo ' selected="selected"'; ?>>Mr</option>
		<option value="Mrs"<?php if($subscriber['title']=='Mrs') echo ' selected="selected"'; ?>>Mrs</option>
		<option value="Ms"<?php if($subscriber['title']=='Ms') echo ' selected="selected"'; ?>>Ms</option>
		<option value="Dr"<?php if($subscriber['title']=='Dr') echo ' selected="selected"'; ?>>Dr</option>
	</select>
</div><!--class_title--></td>
</tr>	 
	 
<tr>
	<th><label>E-mail *</label></th>
	<td><input type="text" name="email" size="33" class="searchbox" value="<?php echo $subscriber['email']; ?>" onClick="if(this.value=='enter email address') this.value='';" /></td>
</tr>

<tr>
	<th><label>First Name *</label></th>
	<td><input type="text" name="first_name" size="33" class="searchbox" value="<?php echo $subscriber['first_name']; ?>" /></td>
</tr>

<tr>
	<th><label>Last Name *</label></th>
	<td><input type="text" name="last_name" size="33" class="searchbox" value="<?php echo $subscriber['last_name']; ?>" /></td>
</tr>
<tr>
	<th><label>Home Number</label></th>
	<td><input type="text" name="home_phone" size="33" class="searchbox" value="<?php echo $subscriber['home_phone']; ?>" /></td>
</tr>
<tr>
	<th><label>Mobile Number</label></th>
	<td><input type="text" name="mobile_phone" size="33" class="searchbox" value="<?php echo $subscriber['mobile_phone']; ?>" /></td>
</tr>
  
<tr>
	<th><label>Country</label></th>
	<td><select id="country" name="country">
			<option value="Australia">Australia</option>
			<?php foreach ($list_countries as $item): ?>	
				<option value="<?php echo $item; ?>" <?php if($item == $subscriber['country']) echo 'selected="selected"'; ?>><?php echo $item; ?></option>
			<?php endforeach; ?>
			</select></td>
</tr>

<tr>
	<th><label>How did you find us?</label></th>
	<td>		<select name="referrer">
			<option value="">How you heard about us?</option>
		<?php foreach($all_list as $item_value): ?>
			<option value="<?php echo $item_value['name']; ?>"<?php if ($subscriber['referrer'] == $item_value['name']) echo 'selected="selected"'; ?>><?php echo $item_value['name']; ?></option>
		<?php endforeach; ?>

		</select>
</td>
</tr>
<tr>
	<th><label>Property Address</label></th>
	<td><input type="text" name="property_address" class="searchbox" value="<?php echo $subscriber['property_address']; ?>" size="78"/></td>
</tr>
<tr>
	<th><label>Comments</label></th>
	<td><textarea name="comments" cols="64"><?php echo $subscriber['comments']; ?></textarea></td>
</tr>
<tr>
	<th><label><?php _e('Property Alert'); ?></label></th>
	<td><div class="site_alerts">
		<?php $property_alert=array();$property_alert=array_diff($list_alerts, $news_alerts);?>
		<?php $news_alert=array();$news_alert=array_diff($list_alerts, $all_alerts);?>
		<?php foreach($property_alert as $key=>$item_value):?>
			<span><input type="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?> onclick="open_query_form(this)">
			<?php echo $item_value; ?></span><br />
			<?php echo $this->get_form_query_string($key);
	endforeach; ?>
	</div></td>
</tr>
<?php if(!empty($news_alert)): ?>
<tr>
	<th><label><?php _e('News Alert'); ?></label></th>
	<td><div class="site_alerts">
		<?php foreach($news_alert as $key=>$item_value):?>
			<span><input type="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?>>
			<?php echo $item_value; ?></span><br />
		<?php	endforeach; ?>
	</div></td>
</tr>
<?php endif; ?>
</table>

<br clear="all" />

<input type="hidden" name="keep_informed" value="<?php echo $subscriber['keep_informed'];?>">

<p class="submit"><input type="submit" value="<?php _e('Save &raquo;') ?>" name="update_subscriber" /></p>
</form>

</div><!--.wrap-->