<?php

defined('ABSPATH') or die('No script kiddies please!');

class zoobrochure_template_tba extends zoobrochure_template {

    public function render() {
        // Read Setting & Property
        $setting = $this->setting;
        $property = $this->property;

        // Add page
        $this->pdf->SetAutoPageBreak(True, 1);
        $this->pdf->AliasNbPages();
        $this->pdf->AddPage();

        // Set Background
        $this->pdf->SetFillColor($setting['bg_color'][0], $setting['bg_color'][1], $setting['bg_color'][2]);
        $this->pdf->Rect(0, 0, $this->pdf->GetPageWidth(), 300, 'F');

        /* Font color */
        if ($setting['block_color'][0] != '255' && $setting['block_color'][1] != '255' && $setting['block_color'][2] != '255') {
            $this->pdf->SetTextColor(255, 255, 255);
        } else {
            $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        }

        /* Header block */
        $this->pdf->SetFillColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
        $this->pdf->Rect(0, 0, 219, 30, 'F');
        $this->pdf->SetFont('Arial', '', 22);

        /* auction text */
        if ($property['auction_date']) {
            $this->pdf->MultiCell(100, 5, "AUCTION", 0, 'L');
        }

        /* header logo */
        $this->pdf->Image($setting['logo'], 160, 5, 40, 0, '', '', array('max_width' => 40));
        $this->pdf->SetXY(10, 30);

        /* image */
        $y = $this->pdf->getY();
        $property_photos = $property['photos'];
        if (!empty($property_photos)) {
            $photos = array();
            $i = 0;
            foreach ($property_photos as $photo) {
                $size = ( $i == 0 ) ? 'large' : 'medium';
                $photos[$i] = array($photo[$size], $photo[$size . '_width'], $photo[$size . '_heigth']);
                $i++;
            }

            if (!empty($photos[0][0])) {  // Main image; The big one		
                list($src, $width, $height) = $photos[0];
                if ($height > $width) {
                    $this->pdf->Image($photos[0][0], 0, 27, 210, 149);
                    $y = $y + 149;
                } else {
                    $this->pdf->Image($photos[0][0], 0, 27, 210, 110);
                    $y = $y + 110;
                }
            }

            $y_image = $y - 2;
            if (!empty($photos[1][0])) {
                $this->pdf->Image($photos[1][0], 0, $y_image, 69.5, 55);
                $y = $y + 55;
            }
            if (!empty($photos[2][0])) {
                $this->pdf->Image($photos[2][0], 70.6, $y_image, 69.5, 55);
            }
            if (!empty($photos[3][0])) {
                $this->pdf->Image($photos[3][0], 141.5, $y_image, 69.5, 55);
            }
        }

        /* address-block */
        $this->pdf->SetFillColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
        $this->pdf->Rect(0, $y - 4, 210, 10, 'F');

        /* address */
        if ($setting['block_color'][0] == '255' && $setting['block_color'][1] == '255' && $setting['block_color'][2] == '255') {
            $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        }
        $this->pdf->SetFont('Helvetica', '', 12);
        $this->pdf->SetXY(10, $y);
        $this->pdf->MultiCell(100, 2, $property['street_address'] . " " . $property['suburb'] . ", " . $property['state'], 0, 'L');

        /* beth, bath, car */
        if ($property['bedrooms'] || $property['bathrooms'] || $property['carspaces']) {

            $this->pdf->SetFont('Arial', '', 13);
            $this->pdf->SetTextColor(255, 255, 255);
            $this->pdf->SetFillColor(0, 0, 0);

            $beds = $property['bedrooms'];
            $baths = $property['bathrooms'];
            $carspaces = $property['carspaces'];

            $bed_icon = $setting['bed_icon'];
            $bath_icon = $setting['bath_icon'];
            $car_icon = $setting['car_icon'];

            if ($beds) {
                $this->pdf->SetXY(158.5, $y);
                $this->pdf->MultiCell(100, 2, $beds, 0, 'L');
                $this->pdf->Image($bed_icon, 150, $y - 2, 0, 0, '', '', array('max_height' => 7));
            }
            if ($baths) {
                $this->pdf->SetXY(179, $y);
                $this->pdf->MultiCell(100, 2, $baths, 0, 'L');
                $this->pdf->Image($bath_icon, 170, $y - 2, 0, 0, '', '', array('max_height' => 7));
            }
            if ($carspaces) {
                $this->pdf->SetXY(200, $y);
                $this->pdf->MultiCell(100, 2, $carspaces, 0, 'L');
                $this->pdf->Image($car_icon, 190, $y - 2.3, 0, 0, '', '', array('max_height' => 7));
            }
        }

        $y = $this->pdf->getY();
        $y_content = $y + 7;

        /* headline */
        $this->pdf->SetTextColor($setting['headline_color'][0], $setting['headline_color'][1], $setting['headline_color'][2]);
        $this->pdf->SetFont('Arial', '', 12);
        $this->pdf->SetXY(10, $y_content);
        $head = str_replace("\t", " ", $property['headline']);
        $head = preg_replace("[\n\r]", "\n", $property['headline']);
        $headline_string = ceil(strlen($head));
        if ($headline_string <= 80) {
            $this->pdf->MultiCell(95, 4, $head, 0, 'J');
        } else {
            $newtext = wordwrap(substr($head, 0, 80), 40);
            $this->pdf->MultiCell(95, 4, $newtext, 0, 'J');
        }

        /* description */
        $y = $this->pdf->getY();
        $y_desc = $y + 2;
        $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        $this->pdf->SetFont('Helvetica', '', 8);
        $this->pdf->SetXY(10, $y_desc);
        $desc = str_replace("\t", " ", $property['description']);
        $desc = preg_replace("[\n\r]", "\n", $desc);
        $description_row = ceil(strlen($desc) / 40);
        $desc_rows = explode("\n", $desc);
        foreach ($desc_rows as $key => $value) {
            $print_y = 0;
            $y_desc = $this->pdf->getY();
            $available_row = (270 - $y_desc) / 4;
            $print_row = ceil(strlen($value) / 40);
            if ($available_row > $print_row) {
                $this->pdf->MultiCell(95, 4, $value, 0, 'J');
            } else {
                $max_desc = $available_row * 90;
                $get_value = strlen($value);
                if ($get_value > $max_desc) {
                    $desc_cut = substr($value, 0, $max_desc);
                    $this->pdf->MultiCell(95, 4, $desc_cut, 0, 'J');
                    $print_y = ceil(strlen($desc_cut) / 40);
                    break;
                }
            }
            $y_desc = $y_desc + $print_y;
        }

        /* detail */
        $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        $this->pdf->SetFont('Helvetica', '', 8);
        $this->pdf->SetXY(116.5, $y_content + 12);

        $next_detail = $this->pdf->getY();



        $details = array('opentimes' => 'Inspect', 'auction_date' => 'Auction', 'auction_time' => 'Details');
        foreach ($details as $key => $values) {
            if (empty($property[$key]))
                continue;
            switch ($key) {
                case 'auction_date' :
                    if ($property[$key] != '0000-00-00')
                        $this->print_detail($values, $show_auction_date, $next_detail);
                    break;
                case 'auction_time' :
                    $this->print_detail($values, $auction_time, $next_detail);
                    break;
                case 'opentimes':
                    if (!empty($opentimes)) {
                        $i = 0;
                        $detail = '';
                        foreach ($opentimes as $key => $opentime) {
                            if (strtotime($opentime['date']) >= time() && $key < 2) {

                                $detail .= date('l, jS F Y', strtotime($opentime['date'])) . " " . date('g:i', strtotime($opentime['start_time'])) . " - " . date('g:i', strtotime($opentime['end_time']));
                                $detail .= "\n";
                                $i++;
                            }
                        }
                        $this->print_detail($values, $detail, $next_detail);
                        if ($i > 0)
                            $next_detail = $next_detail + (($i - 1) * 4);
                        $this->pdf->Ln();
                    }
                    break;
                default:
                    $this->print_detail($values, $property[$key], $next_detail);
                    break;
            }
            $next_detail = $next_detail + 4;
        }
        $y = $this->pdf->getY();
        $this->pdf->Ln();
        if ($property['auction_date'] || $property['opentimes']) {
            $this->pdf->SetLineWidth(.1);
            $this->pdf->SetDrawColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
            $this->pdf->Line(117, $y + 2, 195, $y + 2);
        }

        /* detail 2 */
        $next_detail = $this->pdf->getY();
        $details = array('tax_rate' => 'Council Rates', 'water_rate' => 'Water Rates', 'condo_strata_fee' => 'Strata Rates');
        foreach ($details as $key => $values) {
            if (empty($property[$key]))
                continue;
            switch ($key) {
                case 'tax_rate': case 'water_rate': case 'condo_strata_fee':
                    $key_period = $property[$key . '_period'];
                    switch ($key_period) {
                        case 'Per Week': $period = '/weeky (approx)';
                            break;
                        case 'Per Month': $period = '/month (approx)';
                            break;
                        case 'Per Quarter': $period = ' p/q';
                            break;
                        case 'Per Year': $period = '/year (approx)';
                            break;
                        default :$period = $key_period;
                            break;
                    }
                    $this->print_detail($values, '$' . number_format($property[$key], 2) . $period, $next_detail);
                    break;
                default:
                    $this->print_detail($values, $property[$key], $next_detail);
                    break;
            }

            $next_detail = $next_detail + 4;
        }

        if ($setting['bottom_logo']) {
            $this->pdf->Image($setting['bottom_logo'], 117, 254.5, 0, 12, '', '', array('max_height' => 12));
            $this->pdf->SetLineWidth(.1);
            $this->pdf->SetDrawColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
            $this->pdf->Line(117, 267.5, 195, 267.5);
        }

        /* USER */
        $next_detail = $this->pdf->getY();
        $details = array('user' => 'Contact');
        foreach ($details as $key => $values) {
            if (empty($property[$key]))
                continue;
            switch ($key) {
                case 'user';
                    $i = 0;
                    $this->pdf->SetXY(116, 267);
                    $this->pdf->SetFont('Helvetica', 'B', 12);
                    $this->pdf->MultiCell(50, 10, $property['user'][0]['name']);

                    $this->pdf->SetXY(116, 273);
                    $this->pdf->SetFont('Helvetica', '', 9);
                    $this->pdf->MultiCell(50, 10, $property['user'][0]['mobile']);

                    if (isset($property['user'][1])) {
                        $this->pdf->SetXY(156, 267);
                        $this->pdf->SetFont('Helvetica', 'B', 12);
                        $this->pdf->MultiCell(50, 10, $property['user'][1]['name']);

                        $this->pdf->SetXY(156, 273);
                        $this->pdf->SetFont('Helvetica', '', 9);
                        $this->pdf->MultiCell(50, 10, $property['user'][1]['mobile']);
                    }
                    if ($i > 0)
                        $next_detail = $next_detail + ($i * 4);
                    break;
            }
            $next_detail = $next_detail + 4;
        }

        if ($setting['block_color'][0] != '255' && $setting['block_color'][1] != '255' && $setting['block_color'][2] != '255') {
            $this->pdf->SetTextColor(255, 255, 255);
        } else {
            $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        }

        $this->pdf->SetFillColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
        $this->pdf->Rect(0, 284, 210, 14, 'F');
        $this->pdf->SetFont('Arial', '', 11);
        $this->pdf->SetXY(10, 286);
        $y = $this->pdf->getY();
        $url = strtolower(str_replace(array('http://', 'https://'), array('', ''), get_option('siteurl')));
        $this->pdf->Cell(100, 9, $url, 0, 0, 'L', false, $url);

        /* florplan */
        $floorplans = array();
        if (!empty($property['floorplans'])) {
            $i = 0;
            foreach ($property['floorplans'] as $floorplan) {
                $floorplans[$i] = $floorplan['large'];
                $i++;
            }
        }

        if (!empty($floorplans[0])) {
            $this->pdf->SetXY(20, $y);
            $this->pdf->Floorplan($floorplans[0], 10, 20, 180, true);
            $this->pdf->Image($setting['bottom_logo'], 160, 5, 0, 14, '', '', array('max_height' => 14));

            /* address */
            $street_address = $property['street_address'];
            $suburb = $property['suburb'];
            $state = $property['state'];
            if (!empty($street_address || $suburb || $state)) {
                $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
                $this->pdf->SetFont('Helvetica', 'B', 12);
                $this->pdf->SetXY(20, $this->pdf->GetPaperHeight() - 30);
                $this->pdf->MultiCell(100, 2, $street_address, 0, 'L');
                $this->pdf->SetXY(90, $this->pdf->GetPaperHeight() - 30);
                $this->pdf->MultiCell(100, 2, $suburb . ", " . $state, 0, 'L');
                $y = $this->pdf->getY();
                $this->pdf->Ln();
                $this->pdf->SetLineWidth(.1);
                $this->pdf->SetDrawColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
                $this->pdf->Line(22, $y + 2, 130, $y + 2);
            }
            $this->pdf->Image($setting['logo'], 140, $this->pdf->GetPaperHeight() - 40, 40, 0, '', '', array('max_width' => 40));
        }
    }

    public function print_detail($title, $value, $y) {
        $this->pdf->SetFont('Helvetica', 'B', 8);
        $this->pdf->SetXY(116.5, $y);
        $this->pdf->MultiCell(60, 4, $title . ":", 0, 'L');
        $this->pdf->SetFont('Helvetica', '', 8);
        $this->pdf->SetXY(138, $y);
        $this->pdf->MultiCell(60, 4, $value, 0, 'L');
    }

}
