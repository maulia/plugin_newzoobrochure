<?php
	$property = $this->property($_GET['property_id']);
	if(empty($property))echo '<p>Property not found.</p>';
?>
<div id="page_nav_buttons">
	<div id="print_page"><a href="javascript:window.print();">Print this page</a></div>
	<div id="close_page"><a href="javascript:window.close();">Close this page</a></div>
</div>

<div id="content">
	<?php //desc & features ; ?>
	<div id="property_description">
		<h2><?php echo $this->property['street_address']; ?></h2>
		<h4><?php echo $this->property['headline']; ?></h4>
		<p class="property_description"><?php echo nl2br($this->property['description']); ?></p>
	</div>

	<?php 
	//images
	if(!empty($this->property['photos'])){
		$photos = $this->property['photos'];
		?>
		<div id="property_images_medium">
			<div class="main_photo_overlay_medium">
				<?php for ($i=0;$i<count($photos);$i++){ if($i==4)break;?>
				<div class="photo photo<?php echo $i; ?>"><img src="<?php echo $photos[$i]['medium']; ?>"/></div>
				<?php } ?>
				<div class="clear"></div>
			</div>	
		</div>
	<?php } ?>

	<?php 
	//map
	if(!empty($this->property['latitude']) and !empty($this->property['longitude'])){ 
	$longitude=$this->property['latitude'].",".$this->property['longitude'];
	$g_url = "http://maps.googleapis.com/maps/api/staticmap?center=$longitude&zoom=13&size=450x350&markers=color:red|$longitude&format=jpg&sensor=false"; 
	 ?>
     <div class="property_map"><h4>Property Map: <?php echo $property['street_address']; ?></h4><img src="<?php echo $g_url; ?>" alt="map" /></div>
	 <?php } ?>
</div>

<div id="sidebar" class="sidebar">	
	<!--PROPERTY DETAILS -->
	<?php $settings = $this->settings['widgets']['property_table']; ?>
	<div class="side_block realty_widget_property_table">
		<div id="details">
			<h3>Property Details</h3>
			<div class="block_content">
				<table class="property_details_table" width="100%" cellpadding="0" cellspacing="0">
				<?php  
				foreach($settings as $key=>$values):
					if( !is_array($values) || !$values['display']  || empty($this->property[$key]) || $key =='street_address' || $key=='headline' || $key=='floorplans') continue;
					switch($key):
						case 'lease_plus_another' :case 'lease_option' : case 'floorplans':case 'brochure':case 'virtual_tour': case 'ext_link_1':case 'ext_link_2':break;
						case 'land_size': case 'building_size':case 'office_area':case 'warehouse_area':case 'retail_area':case 'other_area':
							if($key=='land_size')$unit_size='land_area_metric';
							if($key=='building_size')$unit_size='floor_area_metric';
							if($key=='office_area')$unit_size='office_area_metric';
							if($key=='warehouse_area')$unit_size='warehouse_area_metric';
							if($key=='retail_area')$unit_size='retail_area_metric';
							if($key=='other_area')$unit_size='other_area_metric';

							?>
							<tr class="side_land_size">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value">
									<span><?php echo $this->property[$key]." ".$this->property[$unit_size]; ?></span>
								</td>
							</tr>	
							<?php
						break;
						case 'sold_at' : case 'leased_at' : case 'available_at' :
							if ($this->property[$key]!='0000-00-00' && $this->property[$key]!=""):
							?>
								<tr class="<?php echo $key; ?>">
									<td class="field"><?php echo $values['title']; ?> </td>
									<td class="value"><?php echo date("jS F Y", strtotime($this->property[$key])); ?>
									</td>
								</tr>	
							<?php
							endif;
							break;
						case 'approximate_stock_value' : case 'annual_turnover': case 'annual_gross_profit': case 'annual_ebit':  ?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo '$'.number_format($this->property[$key]); ?>
								</td>
							</tr>	
						<?php
						break;
						case 'current_rent' : case 'current_outgoings':  
							if($key=='current_rent')$included_tax='current_rent_include_tax';
							if($key=='current_outgoings')$included_tax='current_outgoings_include_tax';
							?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo '$'.number_format($this->property[$key]); if($this->property[$included_tax]=='1')echo " (included tax)"; ?>
								</td>
							</tr>	
						<?php
						break;
						case 'sold_price' :case 'leased_price' :
							if($this->property['display_sold_price']){ ?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo '$'.number_format($this->property[$key]); ?>
								</td>
							</tr>	
						<?php
							}
						break;					
						case 'lease_option' : 
							if($this->property['lease_option']!='Please select'){ ?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value">
								<?php 
								echo str_replace(" Years","",$this->property['lease_option']);
								if($this->property['lease_plus_another']!='Please select')echo "*".str_replace(" Years","",$this->property['lease_plus_another'])." Years"; 
								?>
								</td>
							</tr>	
						<?php
							}
						break;
						case 'premise' : case 'franchise' : case 'parking_spaces' : 
							if($this->property[$key]!='Please select'){ ?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo $this->property[$key];?></td>
							</tr>	
						<?php
							}
						break;
						case 'lease_commencement' : case 'lease_end':?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo date("d/m/Y",strtotime($this->property[$key])); ?>
								</td>
							</tr>	
						<?php
						break;
						case 'return_percent': ?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo $this->property[$key]."%"; ?>
								</td>
							</tr>	
						<?php
						break;
						case 'auction_time' :
							if (!empty($this->property[$key]) && $this->property[$key]!='00:00:00'):
							?>
								<tr class="<?php echo $key; ?>">
									<td class="field"><?php echo $values['title']; ?> </td>
									<td class="value"><?php echo date("g:ia",strtotime($this->property[$key])); ?></td>
								</tr>	
							<?php
							endif;
							break;
						case 'auction_date' :
							if (!empty($this->property[$key]) && $this->property[$key]!='0000-00-00' && $this->property['status'] != '2' &&  $this->property['status'] != '6' ):
							?>
								<tr class="<?php echo $key; ?>">
									<td class="field"><?php echo $values['title']; ?><span class="colon" style="display:none;"> :</span></td>
									<td class="value"><?php echo date('l jS F Y',strtotime($this->property['auction_date']));?></td>
								</tr>	
							<?php
							endif;
						break;
						case 'estimate_rental_return' :case 'tax_rate': case 'condo_strata_fee':
							$key_period=$this->property[$key.'_period'];
							switch($key_period){
							case 'Per Week': $period=' pw';break;
							case 'Per Month': $period=' pm';break;
							case 'Per Quarter': $period=' pq';break;
							case 'Per Year': $period=' py';break;
							default :
							$period=$key_period;
							break;
							}
							?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo '$'.number_format($this->property[$key]).$period; ?>
								</td>
							</tr>	
							<?php
						break;						
						case 'type':
							switch($this->property[$key]){
							case "BusinessSale":
								$item='Business Sale';
							break;
							case "HolidayLease":
								 $item='Holiday Lease';
							break;
							case "ProjectSale":
								$item='Project Sale';
							break;	
							case "ResidentialLease":
								$item='Residential Lease';
							break;
							case "ResidentialSale":
								$item='Residential Sale';
							break;
							default:
								$item=$this->property[$key];
							break;
							}
							?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo $item; ?>
								</td>
							</tr>
							<?php
						break;								
						default:
							?>
							<tr class="<?php echo $key; ?>">
								<td class="field"><?php echo $values['title']; ?> </td>
								<td class="value"><?php echo $this->property[$key]; ?>
								</td>
							</tr>
							<?php
						break;
					endswitch;
				endforeach; ?>					
				</table>
			</div><!-- end .block_content -->
		</div><!-- end #details -->
	</div><!-- end .side_block.realty_widget_property_table -->
	<!-- END PROPERTY DETAILS -->

	<?php if(!empty($this->property['itemized_features'])){?>
	<div class="side_block">
		<h3>Property Features</h3>
		<div class="block_content"><ul class="features_summary"><?php foreach($this->property['itemized_features'] as $feature)echo "<li>$feature</li>";?></ul></div>
	</div>
	<?php }; ?>
	
	<div id="agentinfo_contactform" class="side_block">
		<h3>Contact Details</h3>
		<div class="block_content">
			<div id="agentinfo">
				<?php 
				$settings=$this->settings['widgets']['contact_agent'];
				for($i=0;$i<=1;$i++){
					$user = $this->property['user'][$i]; 
					if(empty($user)) continue;
					?>	
					<div id="agent<?php echo $i; ?>" class="agent_info">
						<?php foreach($settings as $key=>$values):if(empty($user[$key])) continue;
						switch($key):
						case 'photo': ?>
						<p class="agent_photo">
						<img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo']; ?>" />
						</p>
						<div class="agent_contact_info"><!-- photo always has to be the first in the list -->
						<?php break; ?>
							
						<?php case 'name':?>
						<?php if($settings['not_agent_link']!='1'): ?>
						<h4><a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><?php echo $user[$key]; ?></a></h4>
						<?php else:?>
						<h4><?php echo $user[$key]; ?></h4>
						<?php endif;break; ?>		
						<?php case 'business_phone':?>
								<p><strong>P:</strong>&nbsp;<?php echo $user[$key]; ?></p>
						<?php break;?>	
						<?php case 'mobile':?>
								<p><strong>M:</strong>&nbsp;<?php echo $user[$key]; ?></p>
						<?php break;?>	
						<?php case 'fax':?>
								<p><strong>F:</strong>&nbsp;<?php echo $user[$key]; ?></p>
						<?php break;?>	
						<?php default:?>
								<p><strong><?php echo $key; ?></strong>&nbsp;<?php echo $user[$key]; ?></p>
						<?php break;?>			
						<?php endswitch; ?>	
						<?php endforeach; ?>	
						</div>
						<div class="clearer"></div>
					</div><!-- #<?php echo $i; ?> ends-->

				<?php } ?>
			</div><!-- end #agentinfo -->
		</div><!-- ends .block_content -->
	</div><!-- ends .agentinfo_contactform -->
	
</div><!-- end #sidebar -->