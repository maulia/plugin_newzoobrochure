<?php

function zoosuperapi_insert_property(&$log, $property, $options)
{
	global $wpdb;

	$site_url = get_option('siteurl');

	if (!isset($options['save_property_data']))
		$options['save_property_data'] = 0;
	if (!isset($options['remove_sold']))
		$options['remove_sold'] = 0;
	if (!isset($options['remove_leased']))
		$options['remove_leased'] = 0;
	if (!isset($options['remove_withdrawn']))
		$options['remove_withdrawn'] = 0;
	if (!isset($options['remove_property_ids']))
		$options['remove_property_ids'] = array();
	else
		$options['remove_property_ids'] = array_map('trim', explode(',', $options['remove_property_ids']));

	if (empty($property['id']))
	{
		$log[] = 'No data transmitted? ID was blank';
		return false;
	}
	
	$table_cache = zoosuperapi_get_tables();

	$columns = $table_cache['properties_columns'];

	if (!empty($property['deleted_at']) || ($options['remove_sold'] && $property['status'] == 2) ||  ($options['remove_leased'] && $property['status'] == 3) || ($options['remove_withdrawn'] && $property['status'] == 4) || in_array($property['id'], $options['remove_property_ids']))
	{
		$log[] = "Deleting property {$property['id']}";

		delete_images($property['id']);

		$wpdb->query("DELETE FROM property_features WHERE property_id = ".$property['id']);
		$wpdb->query("DELETE FROM property_meta WHERE property_id = ".$property['id']);
		$wpdb->query("DELETE FROM property_testimonials WHERE property_id = ".$property['id']);
		$wpdb->query("DELETE FROM opentimes WHERE property_id = ".$property['id']);
		
		if ($table_cache['notes_exists'])
			$wpdb->query("DELETE FROM notes WHERE property_id = ".$property['id']);
		
		if ($table_cache['property_exports_exists'])
			$wpdb->query('DELETE FROM property_exports WHERE property_id = '.$property['id']);

		if ($table_cache['extras_exists'])
			$wpdb->query("DELETE FROM extras WHERE property_id = ".$property['id']);

		if ($table_cache['extra_details_exists'])
			$wpdb->query("DELETE FROM extra_details WHERE property_id = ".$property['id']);

		if ($table_cache['property_video_exists'])
			$wpdb->query("DELETE FROM property_video WHERE property_id = ".$property['id']);
		
		if ($table_cache['custom_properties_exists'])
			$wpdb->query('DELETE FROM custom_properties WHERE id = '.$property['id']);

		$wpdb->query("DELETE FROM properties WHERE id = ".$property['id']);

		return true;
	}

	$data = filter_columns($columns, $property);

	if (isset($data['status']))
	{
		if ($data['status'] == 3)
			$data['status'] = 6;
		elseif ($data['status'] == 4)
			$data['status'] = 5;
		elseif ($data['status'] == 5)
			$data['status'] = 4;
		elseif ($data['status'] == 6)
			$data['status'] = 3;
	}

	$existing_property = $wpdb->get_row('SELECT * FROM properties WHERE id = '.$property['id'], ARRAY_A);

	if (!$existing_property)
	{
		$log[] = 'Property does not exist';

		zoosuperapi_insert('properties', $data);

		if (!$wpdb->rows_affected > 0)
		{
			$log[] = 'Insert Failed';
			return false;
		}

		//zoosuperapi_make_dir(ZOOSUPERAPI_UPLOADDIR.'/property/'.$property['id']);

		$log[] = 'Property created';
	} else {
		$skip_fields = explode(',', $options['skip_fields_property']);

		$log[] = "Property already exists.";
		$log[] = "Last updated at {$existing_property['updated_at']}";

		foreach ($skip_fields as $field)
		{
			$field = trim($field);
			unset($data[$field]);
		}

		zoosuperapi_update('properties', $data, array('id' => $property['id']));

		$log[] = 'Property updated';
	}

	$mr = new MultiRequests();

	if (isset($property['attachments']))
	{
		$columns = $table_cache['attachments_columns'];
		
		$delete = array();
		$existing_attachments = $wpdb->get_results('SELECT * FROM attachments WHERE is_user = 0 AND parent_id = '.$property['id'], ARRAY_A);

		foreach ($existing_attachments as $attachment)
			$delete[$attachment['id']] = true;

		$log[] = count($existing_attachments).' existing attachments';
		$log[] = count($property['attachments']).' attachments sent';

		$a_inserted = 0;
		$a_updated = 0;
		$a_deleted = 0;
		$a_downloaded = 0;

		foreach ($property['attachments'] as $attachment)
		{
			$found = false;
			$updated = false;
			$download = false;
			$download_url = false;
			$basename = strtolower(basename($attachment['url']));
			$new_url = $options['save_property_data'] ? ZOOSUPERAPI_UPLOADURL.'/property/'.substr($basename, 0, 2).'/'.$attachment['id'].'_'.$basename : $attachment['url'];
			$new_path = $options['save_property_data'] ? ZOOSUPERAPI_UPLOADDIR.'/property/'.substr($basename, 0, 2).'/'.$attachment['id'].'_'.$basename : false;
			$attachment_id = $attachment['id'];
			unset($attachment['id']);

			if ($options['save_property_data'])
			{
				$download_url = $attachment['url'];
				$attachment['url'] = $new_url;
			}

			foreach ($existing_attachments as $existing_attachment)
			{
				if (
					$existing_attachment['type'] == $attachment['type'] &&
					$existing_attachment['description'] == $attachment['description'] &&
					$existing_attachment['position'] == $attachment['position'] &&
					$existing_attachment['url'] == $new_url &&
					strtotime($existing_attachment['updated_at']) == strtotime($attachment['updated_at'])
					)
				{
					$found = $existing_attachment['id'];
					$updated = false;
					$delete[$existing_attachment['id']] = false;
					break;
				} elseif (
					$existing_attachment['type'] == $attachment['type'] &&
					$existing_attachment['description'] == $attachment['description'] &&
					$existing_attachment['position'] == $attachment['position']
					)
				{
					$found = $existing_attachment['id'];
					$updated = true;
					$delete[$existing_attachment['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('attachments', filter_columns($columns, $attachment));
				if ($wpdb->rows_affected > 0)
				{
					$a_inserted++;
					$download = true;
				} else {
					var_dump($wpdb->func_call, mysql_error());
				}
			} elseif ($found && $updated) {
				delete_images(array($found), false);
				
				zoosuperapi_update('attachments', filter_columns($columns, $attachment), array('id' => $found));
				$a_updated++;
				$download = true;
			}

			if ($found && $new_path && !file_exists($new_path)) {
				$download = true;
			}

			if ($download && $new_path)
			{
				$mr->request($download_url, array(), 'store_img', array('path' => $new_path));
				$a_downloaded++;
			}
		}

		$imgs = array();
		foreach ($delete as $attachment_id => $process)
		{
			if ($process)
			{
				$imgs[] = $attachment_id;
				$a_deleted++;
			}
		}
		delete_images($imgs);

		$log[] = "Attachments: Inserted $a_inserted, updated $a_updated, deleted $a_deleted, downloaded $a_downloaded";
	}

	if (isset($property['property_features']))
	{
		$delete = array();
		$existing_features = $wpdb->get_results('SELECT id, feature FROM property_features WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_features as $feature)
			$delete[$feature['id']] = true;

		$log[] = count($existing_features).' existing features';
		$log[] = count($property['property_features']).' features sent';

		foreach ($property['property_features'] as $feature)
		{
			$found = false;

			foreach ($existing_features as $existing_feature)
			{
				if ($existing_feature['feature'] == $feature)
				{
					$found = true;
					$delete[$existing_feature['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('property_features', array(
					'property_id' => $property['id'],
					'feature' => $feature
					));
			}
		}

		foreach ($delete as $feature_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM property_features WHERE id = '.$feature_id);
			}
		}
	} else {
		$wpdb->query('DELETE FROM property_features WHERE property_id = '.$property['id']);
	}

	$valid_meta = array('vendor_first_name', 'vendor_last_name', 'vendor_phone', 'agent_user_id', 'normal_season_period', 'new_project_rea', 'created_at', 'updated_at', 'unavailable_date', 'property_url', 'duplication', 'land_depth', 'land_width', 'ext_link_1', 'ext_link_2', 'price2_include_tax', 'save_status', 'additional_notes', 'bond', 'cleaning_fee', 'date_of_completion', 'energy_efficiency_rating', 'energy_star_rating', 'garage_area', 'house_depth', 'number_of_floors', 'porch_terrace_area', 'year_built', 'tax_rate', 'tax_rate_period', 'condo_strata_fee', 'condo_strata_fee_period', 'water_rate', 'water_rate_period', 'estimate_rental_return_period', 'land_area_metric', 'floor_area_metric', 'porch_area_metric', 'garage_area_metric', 'house_width_metric', 'virtual_tour', 'mid_season_period', 'high_season_period', 'peak_season_period', 'all_the_building', 'all_the_floor', 'annual_ebit', 'annual_gross_profit', 'annual_outgoings', 'annual_outgoings_include_tax', 'annual_turnover', 'approximate_stock_value', '
building_name', 'business_name', 'current_leased', 'current_outgoings', 'current_outgoings_include_tax', 'current_rent', 'current_rent_include_tax', 'design_type', 'development_name', 'franchise', 'franchise_levies', 'franchise_royalties', 'lease_commencement', 'lease_end', 'lease_option', 'lease_plus_another', 'outlets', 'outgoings_paid_by_tenant', 'parking_comments', 'parking_spaces', 'parking', 'patron_capacity', 'premise', 'rent_review', 'return_percent', 'staff_casual', 'staff_full_time', 'staff_part_time', 'zoning', 'development_website', 'number_of_properties', 'project_design_type_name', 'new_development_category_name', 'project_development_name', 'project_style_name', 'office_area_metric', 'warehouse_area_metric', 'retail_area_metric', 'other_area_metric', 'virtual_tour2', 'virtual_tour3', 'ext_link_3', 'price_include_tax', 'rent_type', 'lease_further_option', 'start_date', 'carspace_from', 'size_to', 'bedroom_from', 'listing_privacy', 'bathroom_to', 'size_from', 'carspace_to', 'completion_date', '
bathroom_from', 'architect_name', 'bedroom_to', 'distance', 'property_logo');
	$property_meta = array();
	foreach ($property as $key => $value)
	{
		if (in_array($key, $valid_meta))
			$property_meta[$key] = $value;
	}

	$delete = array();
	$existing_meta = array();
	$result = $wpdb->get_results('SELECT meta_id, meta_key, meta_value FROM property_meta WHERE property_id = '.$property['id'], ARRAY_A);

	foreach ($result as $meta)
	{
		$delete[$meta['meta_key']] = $meta['meta_id'];
		$existing_meta[$meta['meta_key']] = $meta;
	}

	foreach ($property_meta as $key => $value)
	{
		if ($key == 'price_per')
		{
			switch ($value)
			{
				case 0:
					$value = 'Monthly';
					break;
				case 1:
					$value = 'Weekly';
					break;
				case 2:
					$value = 'Quarterly';
					break;
				case 3:
					$value = 'Yearly';
					break;
			}
		}

		if (!isset($existing_meta[$key]))
		{
			if (!empty($value))
			{
				zoosuperapi_insert('property_meta', array(
					'property_id' => $property['id'],
					'meta_key' => $key,
					'meta_value' => $value,
					));
			}
		} elseif ($existing_meta[$key]['meta_value'] != $value) {
			if (!empty($value))
			{
				zoosuperapi_update('property_meta', array('meta_value' => $value), array('meta_id' => $existing_meta[$key]['meta_id']));
				$delete[$key] = 0;
			}
		} else {
			$delete[$key] = 0;
		}
	}

	foreach ($delete as $meta_key => $id)
	{
		if ($id > 0)
		{
			$wpdb->query('DELETE FROM property_meta WHERE meta_id = '.$id);
		}
	}

	if (isset($property['property_testimonials']))
	{
		$delete = array();
		$existing_testimonials = $wpdb->get_results('SELECT * FROM property_testimonials WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_testimonials as $testimonial)
			$delete[$testimonial['id']] = true;

		foreach ($property['property_testimonials'] as $testimonial)
		{
			$found = false;

			foreach ($existing_testimonials as $existing_testimonial)
			{
				if (
					$existing_testimonial['first_name'] == $testimonial['first_name'] &&
					$existing_testimonial['last_name'] == $testimonial['last_name'] &&
					$existing_testimonial['note'] == $testimonial['note'] &&
					$existing_testimonial['date'] == $testimonial['date']
					)
				{
					$found = true;
					$delete[$existing_testimonial['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('property_testimonials', array(
					'property_id' => $property['id'],
					'contact_name' => trim($testimonial['first_name'].' '.$testimonial['last_name']),
					'first_name' => $testimonial['first_name'],
					'last_name' => $testimonial['last_name'],
					'note' => $testimonial['note'],
					'date' => $testimonial['date'],
					));
			}
		}

		foreach ($delete as $testimonial_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM property_testimonials WHERE id = '.$testimonial_id);
			}
		}
	} else {
		$wpdb->query('DELETE FROM property_testimonials WHERE property_id = '.$property['id']);
	}

	if (strtolower($property['type']) == 'newdevelopment')
	{
		if (isset($property['new_development_details']) && !empty($property['new_development_details']) && $table_cache['new_development_details_exists'])
		{
			$existing_new_development_details = $wpdb->get_row('SELECT id FROM new_development_details WHERE new_development_id = '.$property['id'], ARRAY_A);

			$columns = $table_cache['new_development_details_columns'];
			unset($property['new_development_details']['id']);
			$property['new_development_details'] = filter_columns($columns, $property['new_development_details']);

			if (empty($existing_new_development_details) && !empty($property['new_development_details']))
			{
				zoosuperapi_insert('new_development_details', $property['new_development_details']);
			} elseif (!empty($property['new_development_details'])) {
				zoosuperapi_update('new_development_details', $property['new_development_details'], array('id' => $existing_new_development_details['id']));
			}
		} elseif ($table_cache['new_development_details_exists']) {
			$wpdb->query('DELETE FROM new_development_details WHERE property_id = '.$property['id']);
		}
	}
	
	if (strtolower($property['type']) == 'livestocksale')
	{
		if (isset($property['livestock_sale_details']) && !empty($property['livestock_sale_details']) && $table_cache['livestock_sale_details_exists'])
		{
			$existing_livestock_sale_details = $wpdb->get_row('SELECT id FROM livestock_sale_details WHERE livestock_sale_id = '.$property['id'], ARRAY_A);

			$columns = $table_cache['livestock_sale_details_columns'];
			unset($property['livestock_sale_details']['id']);
			$property['livestock_sale_details'] = filter_columns($columns, $property['livestock_sale_details']);

			if (empty($existing_livestock_sale_details) && !empty($property['livestock_sale_details']))
			{
				zoosuperapi_insert('livestock_sale_details', $property['livestock_sale_details']);
			} elseif (!empty($property['livestock_sale_details'])) {
				zoosuperapi_update('livestock_sale_details', $property['livestock_sale_details'], array('id' => $existing_livestock_sale_details['id']));
			}
		} elseif ($table_cache['livestock_sale_details_exists']) {
			$wpdb->query('DELETE FROM livestock_sale_details WHERE livestock_sale_id = '.$property['id']);
		}
	}
	
	if (strtolower($property['type']) == 'clearingsale')
	{
		if (isset($property['clearing_sale_details']) && !empty($property['clearing_sale_details']) && $table_cache['clearing_sale_details_exists'])
		{
			$existing_clearing_sale_details = $wpdb->get_row('SELECT id FROM clearing_sale_details WHERE clearing_sale_id = '.$property['id'], ARRAY_A);

			$columns = $table_cache['clearing_sale_details_columns'];
			unset($property['clearing_sale_details']['id']);
			$property['clearing_sale_details'] = filter_columns($columns, $property['clearing_sale_details']);

			if (empty($existing_clearing_sale_details) && !empty($property['clearing_sale_details']))
			{
				zoosuperapi_insert('clearing_sale_details', $property['clearing_sale_details']);
			} elseif (!empty($property['clearing_sale_details'])) {
				zoosuperapi_update('clearing_sale_details', $property['clearing_sale_details'], array('id' => $existing_clearing_sale_details['id']));
			}
		} elseif ($table_cache['clearing_sale_details_exists']) {
			$wpdb->query('DELETE FROM clearing_sale_details WHERE clearing_sale_id = '.$property['id']);
		}
	}
	
	if (strtolower($property['type']) == 'clearingsalesevent')
	{
		if (isset($property['clearing_sales_event_details']) && !empty($property['clearing_sales_event_details']) && $table_cache['clearing_sales_event_details_exists'])
		{
			$existing_clearing_sales_event_details = $wpdb->get_row('SELECT id FROM clearing_sales_event_details WHERE clearing_sales_event_id = '.$property['id'], ARRAY_A);

			$columns = $table_cache['clearing_sales_event_details_columns'];
			unset($property['clearing_sales_event_details']['id']);
			$property['clearing_sales_event_details'] = filter_columns($columns, $property['clearing_sales_event_details']);

			if (empty($existing_clearing_sales_event_details) && !empty($property['clearing_sales_event_details']))
			{
				zoosuperapi_insert('clearing_sales_event_details', $property['clearing_sales_event_details']);
			} elseif (!empty($property['clearing_sales_event_details'])) {
				zoosuperapi_update('clearing_sales_event_details', $property['clearing_sales_event_details'], array('id' => $existing_clearing_sales_event_details['id']));
			}
		} elseif ($table_cache['clearing_sales_event_details_exists']) {
			$wpdb->query('DELETE FROM clearing_sales_event_details WHERE clearing_sales_event_id = '.$property['id']);
		}
	}
	
	if (!$table_cache['property_exports_exists'] || count($table_cache['property_exports_columns']) != 5)
	{
		$table = 'CREATE TABLE property_exports (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  property_id int(10) unsigned NOT NULL,
  feed_id int(10) unsigned NOT NULL,
  external_id varchar(32) DEFAULT NULL,
  last_report int(10) unsigned DEFAULT NULL,
  PRIMARY KEY  (id),
  KEY property_id (property_id),
  KEY feed_id (feed_id)
)';

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($table);
		
		$table_cache = zoosuperapi_get_tables(true);
	}
	
	if (isset($property['property_exports']) && $table_cache['property_exports_exists'])
	{
		$delete = array();
		$existing_property_exports = $wpdb->get_results('SELECT * FROM property_exports WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_property_exports as $property_export)
			$delete[$property_export['id']] = true;

		foreach ($property['property_exports'] as $property_export)
		{
			$found = false;

			foreach ($existing_property_exports as $existing_property_export)
			{
				if (
					$existing_property_export['property_id'] == $property_export['property_id'] &&
					$existing_property_export['feed_id'] == $property_export['feed_id'] &&
					$existing_property_export['external_id'] == $property_export['external_id'] &&
					$existing_property_export['last_report'] == $property_export['last_report']
					)
				{
					$found = true;
					$delete[$existing_property_export['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('property_exports', array(
					'property_id' => $property['id'],
					'feed_id' => $property_export['feed_id'],
					'external_id' => $property_export['external_id'],
					'last_report' => $property_export['last_report'],
					));
			}
		}

		foreach ($delete as $property_export_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM property_exports WHERE id = '.$property_export_id);
			}
		}
	} elseif ($table_cache['property_exports_exists']) {
		$wpdb->query('DELETE FROM property_exports WHERE property_id = '.$property['id']);
	}

	if (isset($property['extras']) && !empty($property['extras']) && $table_cache['extras_exists'])
	{
		$existing_extras = $wpdb->get_row('SELECT id FROM extras WHERE property_id = '.$property['id'], ARRAY_A);

		$columns = $table_cache['extras_columns'];
		unset($property['extras']['id']);
		$property['extras'] = filter_columns($columns, $property['extras']);

		if (empty($existing_extras))
		{
			zoosuperapi_insert('extras', $property['extras']);
		} else {
			zoosuperapi_update('extras', $property['extras'], array('id' => $existing_extras['id']));
		}
	} elseif ($table_cache['extras_exists']) {
		$wpdb->query('DELETE FROM extras WHERE property_id = '.$property['id']);
	}

	if (isset($property['extra_details']) && $table_cache['extra_details_exists'])
	{
		$delete = array();
		$existing_extra_details = $wpdb->get_results('SELECT * FROM extra_details WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_extra_details as $extra_detail)
			$delete[$extra_detail['id']] = true;

		foreach ($property['extra_details'] as $extra_detail)
		{
			$found = false;

			foreach ($existing_extra_details as $existing_extra_detail)
			{
				if (
					$existing_extra_detail['field_value'] == $extra_detail['field_value'] &&
					$existing_extra_detail['updated_at'] == $extra_detail['updated_at'] &&
					$existing_extra_detail['extra_detail_name_id'] == $extra_detail['extra_detail_name_id']
					)
				{
					$found = true;
					$delete[$existing_extra_detail['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('extra_details', array(
					'property_id' => $property['id'],
					'field_value' => $extra_detail['field_value'],
					'created_at' => $extra_detail['created_at'],
					'updated_at' => $extra_detail['updated_at'],
					'extra_detail_name_id' => $extra_detail['extra_detail_name_id'],
					'office_id' => $extra_detail['office_id'],
					));
			}
		}

		foreach ($delete as $extra_detail_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM extra_details WHERE id = '.$extra_detail_id);
			}
		}
	} elseif ($table_cache['extra_details_exists']) {
		$wpdb->query('DELETE FROM extra_details WHERE property_id = '.$property['id']);
	}

	if (isset($property['extra_options']) && $table_cache['extra_options_exists'])
	{
		$delete = array();
		$existing_extra_options = $wpdb->get_results('SELECT * FROM extra_options WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_extra_options as $extra_option)
			$delete[$extra_option['id']] = true;

		foreach ($property['extra_options'] as $extra_option)
		{
			$found = false;

			foreach ($existing_extra_options as $existing_extra_option)
			{
				if (
					$existing_extra_option['id'] == $extra_option['id'] &&
					$existing_extra_option['option_title'] == $extra_option['option_title'] &&
					$existing_extra_option['updated_at'] == $extra_option['updated_at'] &&
					$existing_extra_option['option_order'] == $extra_option['option_order']
					)
				{
					$found = true;
					$delete[$existing_extra_option['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('extra_options', array(
					'id' => $extra_option['id'],
					'property_id' => $property['id'],
					'option_title' => $extra_option['option_title'],
					'created_at' => $extra_option['created_at'],
					'updated_at' => $extra_option['updated_at'],
					'option_order' => $extra_option['option_order'],
					'office_id' => $extra_option['office_id'],
					));
			}
		}

		foreach ($delete as $extra_option_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM extra_options WHERE id = '.$extra_option_id);
			}
		}
	} elseif ($table_cache['extra_options_exists']) {
		$wpdb->query('DELETE FROM extra_options WHERE property_id = '.$property['id']);
	}

	if (isset($property['opentimes']))
	{
		$delete = array();
		$existing_opentimes = $wpdb->get_results('SELECT * FROM opentimes WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_opentimes as $opentime)
			$delete[$opentime['id']] = true;

		foreach ($property['opentimes'] as $opentime)
		{
			$found = false;

			foreach ($existing_opentimes as $existing_opentime)
			{
				if (
					$existing_opentime['date'] == $opentime['date'] &&
					$existing_opentime['start_time'] == $opentime['start_time'] &&
					$existing_opentime['end_time'] == $opentime['end_time']
					)
				{
					$found = true;
					$delete[$existing_opentime['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('opentimes', array(
					'property_id' => $property['id'],
					'date' => $opentime['date'],
					'start_time' => $opentime['start_time'],
					'end_time' => $opentime['end_time'],
					));
			}
		}

		foreach ($delete as $opentime_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM opentimes WHERE id = '.$opentime_id);
			}
		}
	} else {
		$wpdb->query('DELETE FROM opentimes WHERE property_id = '.$property['id']);
	}

	if (isset($property['property_video']) && $table_cache['property_video_exists'])
	{
		$columns = $table_cache['property_video_columns'];
		$delete = array();
		$existing_videos = $wpdb->get_results('SELECT * FROM property_video WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_videos as $video)
			$delete[$video['id']] = true;

		foreach ($property['property_video'] as $video)
		{
			$found = false;

			foreach ($existing_videos as $existing_video)
			{
				if (
					$existing_video['youtube_id'] == $video['youtube_id'] &&
					$existing_video['embed_code'] == $video['embed_code'] &&
					$existing_video['title'] == $video['title']
					)
				{
					$found = true;
					$delete[$existing_video['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$row = filter_columns($columns, array(
					'property_id' => $property['id'],
					'youtube_id' => $video['youtube_id'],
					'embed_code' => $video['embed_code'],
					'title' => $video['title'],
					'status' => $video['status'],
					));
				if (!empty($row))
					zoosuperapi_insert('property_video', $row);
			}
		}

		foreach ($delete as $video_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM property_video WHERE id = '.$video_id);
			}
		}
	} elseif ($table_cache['property_video_exists']) {
		$wpdb->query('DELETE FROM property_video WHERE property_id = '.$property['id']);
	}

	if ($table_cache['pds_exists'] && $table_cache['pds_investment_exists'])
	{
		$pds_columns = $table_cache['pds_columns'];
		$pds_investment_columns = $table_cache['pds_investment_columns'];

		$property['pds'] = filter_columns($pds_columns, $property['pds']);

		$existing_pds = $wpdb->get_results('SELECT * FROM pds WHERE property_id = '.$property['id'], ARRAY_A);

		if (empty($existing_pds) && !empty($property['pds']))
		{
			zoosuperapi_insert('pds', $property['pds']);
		} elseif (!empty($existing_pds) && empty($property['pds'])) {
			$wpdb->query('DELETE FROM pds WHERE property_id = '.$property['id']);
		} elseif (!empty($property['pds'])) {
			zoosuperapi_update('pds', $property['pds'], array('property_id' => $property['id']));
		}
		
		$delete = array();
		$existing_pds_investment = $wpdb->get_results('SELECT * FROM pds_investment WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_pds_investment as $pds_investment)
			$delete[$pds_investment['id']] = true;

		foreach ($property['pds_investment'] as $inv)
		{
			$inv = filter_columns($pds_investment_columns, $inv);
			
			if (isset($delete[$inv['id']]))
			{
				$delete[$pds_investment['id']] = false;

				zoosuperapi_update('pds_investment', $inv, array('id' => $inv['id']));
			} else {
				zoosuperapi_insert('pds_investment', $inv);
			}
		}

		foreach ($delete as $pds_investment_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM pds_investment WHERE id = '.$pds_investment_id);
			}
		}
	}
	
	if ($property['type'] == 'HolidayLease')
	{
		if (isset($property['holiday_lease_rates']) && $table_cache['holiday_lease_rates_exists'])
		{
			$delete = array();
			$existing_holiday_lease_rates = $wpdb->get_results('SELECT * FROM holiday_lease_rates WHERE holiday_lease_id = '.$property['id'], ARRAY_A);

			foreach ($existing_holiday_lease_rates as $holiday_lease_rate)
				$delete[$holiday_lease_rate['id']] = true;

			foreach ($property['holiday_lease_rates'] as $holiday_lease_rate)
			{
				$found = false;

				foreach ($existing_holiday_lease_rates as $existing_holiday_lease_rate)
				{
					if (
						$existing_holiday_lease_rate['title'] == $holiday_lease_rate['title'] &&
						$existing_holiday_lease_rate['starts_at'] == $holiday_lease_rate['starts_at'] &&
						$existing_holiday_lease_rate['ends_at'] == $holiday_lease_rate['ends_at'] &&
						$existing_holiday_lease_rate['per_night'] == $holiday_lease_rate['per_night'] &&
						$existing_holiday_lease_rate['per_week'] == $holiday_lease_rate['per_week'] &&
						$existing_holiday_lease_rate['min_los'] == $holiday_lease_rate['min_los']
						)
					{
						$found = true;
						$delete[$existing_holiday_lease_rate['id']] = false;
						break;
					}
				}

				if (!$found)
				{
					zoosuperapi_insert('holiday_lease_rates', array(
						'holiday_lease_id' => $property['id'],
						'title' => $holiday_lease_rate['title'],
						'starts_at' => $holiday_lease_rate['starts_at'],
						'ends_at' => $holiday_lease_rate['ends_at'],
						'per_night' => $holiday_lease_rate['per_night'],
						'per_week' => $holiday_lease_rate['per_week'],
						'min_los' => $holiday_lease_rate['min_los'],
						));
				}
			}

			foreach ($delete as $holiday_lease_rate_id => $process)
			{
				if ($process)
				{
					$wpdb->query('DELETE FROM holiday_lease_rates WHERE id = '.$holiday_lease_rate_id);
				}
			}
		} elseif ($table_cache['holiday_lease_rates_exists']) {
			$wpdb->query('DELETE FROM holiday_lease_rates WHERE holiday_lease_id = '.$property['id']);
		}
		
		if (isset($property['holiday_lease_bookings']) && $table_cache['holiday_lease_bookings_exists'])
		{
			$delete = array();
			$existing_holiday_lease_bookings = $wpdb->get_results('SELECT * FROM holiday_lease_bookings WHERE holiday_lease_id = '.$property['id'], ARRAY_A);

			foreach ($existing_holiday_lease_bookings as $holiday_lease_booking)
				$delete[$holiday_lease_booking['id']] = true;

			foreach ($property['holiday_lease_bookings'] as $holiday_lease_booking)
			{
				$found = false;

				foreach ($existing_holiday_lease_bookings as $existing_holiday_lease_booking)
				{
					if (
						$existing_holiday_lease_booking['arrives'] == $holiday_lease_booking['arrives'] &&
						$existing_holiday_lease_booking['departs'] == $holiday_lease_booking['departs'] &&
						$existing_holiday_lease_booking['first_name'] == $holiday_lease_booking['first_name'] &&
						$existing_holiday_lease_booking['last_name'] == $holiday_lease_booking['last_name'] &&
						$existing_holiday_lease_booking['email'] == $holiday_lease_booking['email'] &&
						$existing_holiday_lease_booking['updated_at'] == $holiday_lease_booking['updated_at']
						)
					{
						$found = true;
						$delete[$existing_holiday_lease_booking['id']] = false;
						break;
					}
				}

				if (!$found)
				{
					zoosuperapi_insert('holiday_lease_bookings', array(
						'holiday_lease_id' => $property['id'],
						'arrives' => $holiday_lease_booking['arrives'],
						'departs' => $holiday_lease_booking['departs'],
						'first_name' => $holiday_lease_booking['first_name'],
						'last_name' => $holiday_lease_booking['last_name'],
						'email' => $holiday_lease_booking['email'],
						'updated_at' => $holiday_lease_booking['updated_at'],
						));
				}
			}

			foreach ($delete as $holiday_lease_booking_id => $process)
			{
				if ($process)
				{
					$wpdb->query('DELETE FROM holiday_lease_bookings WHERE id = '.$holiday_lease_booking_id);
				}
			}
		} elseif ($table_cache['holiday_lease_bookings_exists']) {
			$wpdb->query('DELETE FROM holiday_lease_bookings WHERE holiday_lease_id = '.$property['id']);
		}
	}
	
	if (isset($property['specific_field_values']) && $table_cache['specific_field_values_exists'])
	{
		$columns = $table_cache['specific_field_values_columns'];
		$delete = array();
		$existing_specfieldvals = $wpdb->get_results('SELECT * FROM specific_field_values WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_specfieldvals as $specfieldval)
			$delete[$specfieldval['id']] = true;

		foreach ($property['specific_field_values'] as $specfieldval)
		{
			$found = false;

			foreach ($existing_specfieldvals as $existing_specfieldval)
			{
				if (
					$existing_specfieldval['dropdown_id'] == $specfieldval['dropdown_id'] &&
					$existing_specfieldval['field_id'] == $specfieldval['field_id'] &&
					$existing_specfieldval['updated_at'] == $specfieldval['updated_at']
					)
				{
					$found = true;
					$delete[$existing_specfieldval['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$row = filter_columns($columns, array(
					'property_id' => $property['id'],
					'dropdown_id' => $specfieldval['dropdown_id'],
					'field_id' => $specfieldval['field_id'],
					'created_at' => $specfieldval['created_at'],
					'updated_at' => $specfieldval['updated_at'],
					));
				if (!empty($row))
					zoosuperapi_insert('specific_field_values', $row);
			}
		}

		foreach ($delete as $specfieldval_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM specific_field_values WHERE id = '.$specfieldval_id);
			}
		}
	} elseif ($table_cache['specific_field_values_exists']) {
		$wpdb->query('DELETE FROM specific_field_values WHERE property_id = '.$property['id']);
	}
	
	if (isset($property['specific_field_dropdowns']) && $table_cache['specific_field_dropdowns_exists'])
	{
		$columns = $table_cache['specific_field_dropdowns_columns'];
		$existing_specfielddrops = $wpdb->get_results('SELECT * FROM specific_field_dropdowns', ARRAY_A);

		foreach ($property['specific_field_dropdowns'] as $specfielddrop)
		{
			$found = false;
			$update = false;

			foreach ($existing_specfielddrops as $existing_specfielddrop)
			{
				if (
					$existing_specfielddrop['id'] == $specfielddrop['id'] &&
					$existing_specfielddrop['field_id'] == $specfielddrop['field_id'] &&
					$existing_specfielddrop['name'] == $specfielddrop['name'] &&
					$existing_specfielddrop['provided_id'] == $specfielddrop['provided_id'] &&
					$existing_specfielddrop['param'] == $specfielddrop['param'] &&
					$existing_specfielddrop['updated_at'] == $specfielddrop['updated_at']
					)
				{
					$found = true;
					break;
				} elseif ($existing_specfielddrop['id'] == $specfielddrop['id']) {
					$update = true;
					break;
				}
			}

			$row = filter_columns($columns, array(
				'id' => $specfielddrop['id'],
				'field_id' => $specfielddrop['field_id'],
				'name' => $specfielddrop['name'],
				'provided_id' => $specfielddrop['provided_id'],
				'param' => $specfielddrop['param'],
				'created_at' => $specfielddrop['created_at'],
				'updated_at' => $specfielddrop['updated_at'],
				));
			if (!$found && !$update)
			{
				if (!empty($row))
					zoosuperapi_insert('specific_field_dropdowns', $row);
			} elseif ($update) {
				if (!empty($row))
					zoosuperapi_update('specific_field_dropdowns', $row, array('id' => $specfielddrop['id']));
			}
		}
	}
	
	if (isset($property['specific_fields']) && $table_cache['specific_fields_exists'] && !empty($property['specific_fields']))
	{
		$columns = $table_cache['specific_fields_columns'];
		$existing_specfieldss = $wpdb->get_results('SELECT * FROM specific_fields', ARRAY_A);

		foreach ($property['specific_fields'] as $specfields)
		{
			$found = false;
			$update = false;

			foreach ($existing_specfieldss as $existing_specfields)
			{
				if (
					$existing_specfields['id'] == $specfields['id'] &&
					$existing_specfields['name'] == $specfields['name'] &&
					$existing_specfields['xml_field'] == $specfields['xml_field'] &&
					$existing_specfields['multi'] == $specfields['multi']
					)
				{
					$found = true;
					break;
				} elseif ($existing_specfields['id'] == $specfields['id']) {
					$update = true;
					break;
				}
			}

			$row = filter_columns($columns, array(
				'id' => $specfields['id'],
				'name' => $specfields['name'],
				'xml_field' => $specfields['xml_field'],
				'developer_id' => $specfields['developer_id'],
				'multi' => $specfields['multi'],
				));
			if (!$found && !$update)
			{
				if (!empty($row))
					zoosuperapi_insert('specific_fields', $row);
			} elseif ($update) {
				if (!empty($row))
					zoosuperapi_update('specific_fields', $row, array('id' => $specfields['id']));
			}
		}
	}
	
	if (isset($property['land_home_designs']) && $table_cache['land_home_designs_exists'])
	{
		$columns = $table_cache['land_home_designs_columns'];
		$delete = array();
		$existing_houselands = $wpdb->get_results('SELECT * FROM land_home_designs WHERE home_design_id = '.$property['id'], ARRAY_A);

		foreach ($existing_houselands as $houseland)
			$delete[$houseland['id']] = true;

		foreach ($property['land_home_designs'] as $houseland)
		{
			$found = false;

			foreach ($existing_houselands as $existing_houseland)
			{
				if ($existing_houseland['land_id'] == $houseland)
				{
					$found = true;
					$delete[$existing_houseland['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$row = filter_columns($columns, array(
					'home_design_id' => $property['id'],
					'land_id' => $houseland,
					));
				if (!empty($row))
					zoosuperapi_insert('land_home_designs', $row);
			}
		}

		foreach ($delete as $houseland_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM land_home_designs WHERE id = '.$houseland_id);
			}
		}
	} elseif ($table_cache['land_home_designs_exists']) {
		$wpdb->query('DELETE FROM land_home_designs WHERE home_design_id = '.$property['id']);
	}
	
	if (isset($property['rental_seasons']) && $table_cache['rental_seasons_exists'])
	{
		$columns = $table_cache['rental_seasons_columns'];
		$delete = array();
		$existing_rentalseasons = $wpdb->get_results('SELECT * FROM rental_seasons WHERE property_id = '.$property['id'], ARRAY_A);
		
		if (in_array('property_id', $columns))
		{
			$table = 'CREATE TABLE rental_seasons (
  id int(11) NOT NULL AUTO_INCREMENT,
  season enum(\'high\',\'mid\',\'normal\',\'peak\') DEFAULT NULL,
  start_date date DEFAULT NULL,
  end_date date DEFAULT NULL,
  minimum_stay varchar(10) DEFAULT NULL,
  property_id int(11) DEFAULT NULL,
  position int(11) DEFAULT NULL,
  PRIMARY KEY  (id),
  KEY property_id (property_id)
)';

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta($sql);
			
			$columns = zoosuperapi_get_columns('rental_seasons');
			
			update_option('zoosuperapi_table_cache', '');
		}

		foreach ($existing_rentalseasons as $rentalseason)
			$delete[$rentalseason['id']] = true;

		foreach ($property['rental_seasons'] as $rentalseason)
		{
			$found = false;

			foreach ($existing_rentalseasons as $existing_rentalseason)
			{
				if (
					$existing_rentalseason['season'] == $rentalseason['season'] &&
					$existing_rentalseason['start_date'] == $rentalseason['start_date'] &&
					$existing_rentalseason['end_date'] == $rentalseason['end_date'] &&
					$existing_rentalseason['minimum_stay'] == $rentalseason['minimum_stay'] &&
					$existing_rentalseason['property_id'] == $rentalseason['holiday_lease_id'] &&
					$existing_rentalseason['position'] == $rentalseason['position']
				)
				{
					$found = true;
					$delete[$existing_rentalseason['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				$rentalseason['property_id'] = $rentalseason['holiday_lease_id'];
				$row = filter_columns($columns, $rentalseason);
				if (!empty($row))
					zoosuperapi_insert('rental_seasons', $row);
			}
		}

		foreach ($delete as $rentalseason_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM rental_seasons WHERE id = '.$rentalseason_id);
			}
		}
	} elseif ($table_cache['rental_seasons_exists']) {
		$columns = $table_cache['rental_seasons_columns'];
		
		if (in_array('property_id', $columns))
			$wpdb->query('DELETE FROM rental_seasons WHERE property_id = '.$property['id']);
	}
	
	if (isset($property['notes']) && $table_cache['notes_exists'])
	{
		$delete = array();
		$existing_notes = $wpdb->get_results('SELECT * FROM notes WHERE property_id = '.$property['id'], ARRAY_A);

		foreach ($existing_notes as $note)
			$delete[$note['id']] = true;

		$log[] = count($existing_notes).' existing notes';
		$log[] = count($property['notes']).' notes sent';

		foreach ($property['notes'] as $note)
		{
			$found = false;

			foreach ($existing_notes as $existing_note)
			{
				if (
					$existing_note['contact_id'] == $note['contact_id'] &&
					$existing_note['property_id'] == $note['property_id'] &&
					$existing_note['agent_id'] == $note['agent_id'] &&
					$existing_note['note_type_id'] == $note['note_type_id'] &&
					$existing_note['source_id'] == $note['source_id'] &&
					$existing_note['address'] == $note['address'] &&
					$existing_note['description'] == $note['description'] &&
					$existing_note['note_date'] == $note['note_date'] &&
					$existing_note['updated_at'] == $note['updated_at']
					)
				{
					$found = true;
					$delete[$existing_note['id']] = false;
					break;
				}
			}

			if (!$found)
			{
				zoosuperapi_insert('notes', $note);
			}
		}

		foreach ($delete as $note_id => $process)
		{
			if ($process)
			{
				$wpdb->query('DELETE FROM notes WHERE id = '.$note_id);
			}
		}
	} elseif ($table_cache['notes_exists']) {
		$wpdb->query('DELETE FROM notes WHERE property_id = '.$property['id']);
	}
	
	if (isset($property['custom_fields']) && !empty($property['custom_fields']) && $table_cache['custom_fields_exists'] && $table_cache['custom_values_exists'] && $table_cache['custom_properties_exists'])
	{
		$log[] = count($property['custom_fields']).' custom fields sent';
	
		$field_ids = array(-1);
		$fields = array();
		$fields_exist = array();
		foreach ($property['custom_fields'] as $row)
		{
			if (!in_array($row['field_id'], $field_ids))
			{
				$field_ids[] = intval($row['field_id']);
				$fields[intval($row['field_id'])] = $row;
			}
		}
		
		$results = $wpdb->get_results("SELECT id, updated_at FROM custom_fields WHERE id IN (".implode(',', $field_ids).")", ARRAY_A);
		foreach ($results as $row)
		{
			$new = $fields[$row['id']];
			if (strtotime($row['updated_at']) < strtotime($new['field_updated_at']))
			{
				zoosuperapi_update('custom_fields', array(
					'developer_id' => isset($new['developer_id']) ? $new['developer_id'] : null,
					'agent_id' => isset($new['agent_id']) ? $new['agent_id'] : null,
					'office_id' => isset($new['office_id']) ? $new['office_id'] : null,
					'field_name' => isset($new['field_name']) ? $new['field_name'] : null,
					'xml_name' => isset($new['xml_name']) ? $new['xml_name'] : null,
					'field_type' => isset($new['field_type']) ? $new['field_type'] : 0,
					'position' => isset($new['position']) ? $new['position'] : 0,
					'updated_at' => isset($new['field_updated_at']) ? $new['field_updated_at'] : '',
					), array('id' => $row['id']));
			}
			
			unset($fields[$row['id']]);
		}
		
		foreach ($fields as $new)
		{
			zoosuperapi_insert('custom_fields', array(
				'id' => $new['field_id'],
				'developer_id' => isset($new['developer_id']) ? $new['developer_id'] : null,
				'agent_id' => isset($new['agent_id']) ? $new['agent_id'] : null,
				'office_id' => isset($new['office_id']) ? $new['office_id'] : null,
				'field_name' => isset($new['field_name']) ? $new['field_name'] : null,
				'xml_name' => isset($new['xml_name']) ? $new['xml_name'] : null,
				'field_type' => isset($new['field_type']) ? $new['field_type'] : 0,
				'position' => isset($new['position']) ? $new['position'] : 0,
				'created_at' => isset($new['field_created_at']) ? $new['field_created_at'] : '',
				'updated_at' => isset($new['field_updated_at']) ? $new['field_updated_at'] : '',
				));
		}
		
		unset($field_ids, $fields, $fields_exist);
		
		$value_ids = array(-1);
		$values = array();
		$values_exist = array();
		foreach ($property['custom_fields'] as $row)
		{
			if (!in_array($row['value_id'], $value_ids))
			{
				$value_ids[] = intval($row['value_id']);
				$values[intval($row['value_id'])] = $row;
			}
		}
		
		$results = $wpdb->get_results("SELECT id, updated_at FROM custom_values WHERE id IN (".implode(',', $value_ids).")", ARRAY_A);
		foreach ($results as $row)
		{
			$new = $values[$row['id']];
			if (strtotime($row['updated_at']) < strtotime($new['value_updated_at']))
			{
				zoosuperapi_update('custom_values', array(
					'value' => isset($new['value']) ? $new['value'] : null,
					'updated_at' => isset($new['value_updated_at']) ? $new['value_updated_at'] : '',
					), array('id' => $row['id']));
			}
			
			unset($values[$row['id']]);
		}

		foreach ($values as $new)
		{
			zoosuperapi_insert('custom_values', array(
				'id' => $new['value_id'],
				'field_id' => isset($new['field_id']) ? $new['field_id'] : null,
				'value' => isset($new['value']) ? $new['value'] : null,
				'created_at' => isset($new['value_created_at']) ? $new['value_created_at'] : '',
				'updated_at' => isset($new['value_updated_at']) ? $new['value_updated_at'] : '',
				));
		}
		
		unset($value_ids, $values, $values_exist);
		
		$ids = array();
		foreach ($property['custom_fields'] as $row)
		{
			$col = $wpdb->get_var("SELECT id FROM custom_properties WHERE property_id = ".$property['id'].' AND field_id = '.$row['field_id'].' AND value_id = '.$row['value_id'], ARRAY_A);
			
			if (empty($col))
			{
				zoosuperapi_insert('custom_properties', array(
					'property_id' => $property['id'],
					'field_id' => isset($row['field_id']) ? $row['field_id'] : null,
					'value_id' => isset($row['value_id']) ? $row['value_id'] : null,
					'created_at' => isset($row['created_at']) ? $row['created_at'] : '',
					'updated_at' => isset($row['updated_at']) ? $row['updated_at'] : '',
					));
				$ids[] = $wpdb->insert_id;
			} else {
				$ids[] = $col;
			}
		}
		
		if (count($ids) > 0)
			$wpdb->query('DELETE FROM custom_properties WHERE property_id = '.$property['id'].' AND id NOT IN ('.implode(',', $ids).')');
	} elseif ($table_cache['custom_properties_exists']) {
		$wpdb->query('DELETE FROM custom_properties WHERE property_id = '.$property['id']);
	}

	while ($mr->active_requests() > 0)
	{
		$mr->wait();
	}

	return true;
}
