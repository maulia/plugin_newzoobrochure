<?php
 $path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
require($path.'/wp-blog-header.php');
 extract($_POST);
 global $realty, $helper;
 switch($task):
	case 'validate_available_date':	
		$mininum_day="-".$min_day." days";
		if(empty($first_night)):
			$return['success'] = false;
			$return['msg'] = 'First night cannot be empty.';
		//elseif(strtotime($first_night) <= strtotime($last_night)):	
		elseif(empty($last_night) && !empty($first_night)):$return['success'] = true;
		elseif(strtotime($first_night) > strtotime($mininum_day, strtotime($last_night))): //strtotime("-5 days",$timestamp); 
			$return['success'] = false;
			$return['msg'] = 'First night must be at least '.$min_day.' nights before last night.';
		else:
			$return['success'] = true;
			//$return['success'] = false;
			//$return['msg'] = 'First night cannot be after last night.';
		endif;
	break;
	case 'spam_question':	
		$validated_spam = $helper->isSpam($spam_question,$spam_question_id);
		if(empty($validated_spam)):
			$return['success'] = true; 
		else:
			$return['success'] = false;
			$return['msg'] = $validated_spam;
		endif;
	break;
	case 'children_age':
		if($children_number >0 and empty($children_age) ):
			$return['success'] = false;
			$return['msg'] = "Please enter the children age.";
		else:
			$return['success'] = true; 
		endif;
	break;
	case 'process_contact_form':
		$return['msg'] = $realty->process_contact_form($_POST);
	break;
	default:
		$return['success'] = false;
	break;
endswitch;

// output correct header
$xhr = isset($_SERVER['HTTP_X_REQUESTED_WITH']) and (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
header('Content-Type: ' . ($xhr ? 'application/json' : 'text/plain'));
 
echo $helper->json_encode($return); 
?>