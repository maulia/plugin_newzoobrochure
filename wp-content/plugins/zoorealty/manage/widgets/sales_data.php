<p><label>Please check the options that you want to display in Sales Data widget.</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[sales_data]" type="checkbox"<?php if($settings['sales_data']) echo ' checked="checked"'; ?>> <label>Sales Data</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[suburb_profile]" type="checkbox"<?php if($settings['suburb_profile']) echo ' checked="checked"'; ?>> <label>Suburb Profile</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[sold_and_leased]" type="checkbox"<?php if($settings['sold_and_leased']) echo ' checked="checked"'; ?>> <label>Recent Sale / Leased</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[similar_listings]" type="checkbox"<?php if($settings['similar_listings']) echo ' checked="checked"'; ?>> <label>Similar Listings</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[walkability]" type="checkbox"<?php if($settings['walkability']) echo ' checked="checked"'; ?>> <label>Walkability</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[map]" type="checkbox"<?php if($settings['map']) echo ' checked="checked"'; ?>> <label>Map</label> </p>

<p>Below are the options for Similar Listings</p>
<p><label>Number</label> <br><input value="<?php echo $settings['limit']; ?>" name="realty_widget_<?php echo $caption; ?>[limit]" type="text"></p>
<p><label>Title</label> <br><input value="<?php echo $settings['Title']; ?>" name="realty_widget_<?php echo $caption; ?>[Title]" type="text"></p>
<p><label>Price Range (Criteria: returns current property price + price range or current property price - price range )</label> <br><input value="<?php echo $settings['price_range']; ?>" name="realty_widget_<?php echo $caption; ?>[price_range]" type="text"></p>

<h4>Search listings with the same:</h4>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][type]" type="checkbox"<?php if($settings['display']['type']) echo ' checked="checked"'; ?>> <label>Type</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][suburb]" type="checkbox"<?php if($settings['display']['suburb']) echo ' checked="checked"'; ?>> <label>Suburb</label> </p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][property_type]" type="checkbox"<?php if($settings['display']['property_type']) echo ' checked="checked"'; ?>> <label>Property Type</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][bedrooms]" type="checkbox"<?php if($settings['display']['bedrooms']) echo ' checked="checked"'; ?>> <label>Bedrooms</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][bathrooms]" type="checkbox" <?php if($settings['display']['bathrooms']) echo 'checked="checked"'; ?>> <label>Bathrooms</label></p>
<p><input value="1" name="realty_widget_<?php echo $caption; ?>[display][carspaces]" type="checkbox"<?php if($settings['display']['carspaces']) echo ' checked="checked"'; ?>> <label>Carspaces</label></p>

<p>Below are the options for map</p>
<p><input type ="radio" value="street_view" name="realty_widget_<?php echo $caption; ?>[map_mode]"<?php if($settings['map_mode']=='street_view') echo ' checked="checked"'; ?>> Street View<br />
<input type ="radio" value="normal" name="realty_widget_<?php echo $caption; ?>[map_mode]"<?php if($settings['map_mode']=='normal') echo ' checked="checked"'; ?>> Normal<br/>
<label>Width</label> <br><input value="<?php echo $settings['map_width']; ?>" name="realty_widget_<?php echo $caption; ?>[map_width]" type="text"><br/>
<label>Height</label> <br><input value="<?php echo $settings['map_height']; ?>" name="realty_widget_<?php echo $caption; ?>[map_height]" type="text"><br/>
</p>

<p>Below are the options for walk score</p>
<p><label>Width</label> <br><input value="<?php echo $settings['width']; ?>" name="realty_widget_<?php echo $caption; ?>[width]" type="text"></p>
<p><label>Iframe CSS</label> <br><input value="<?php echo $settings['iframe_css']; ?>" name="realty_widget_<?php echo $caption; ?>[iframe_css]" type="text"></p>
<p><label>Map Frame Color</label> <br><input value="<?php echo $settings['map_frame_color']; ?>" name="realty_widget_<?php echo $caption; ?>[map_frame_color]" type="text"></p>
<p><label>Score Color</label> <br><input value="<?php echo $settings['score_color']; ?>" name="realty_widget_<?php echo $caption; ?>[score_color]" type="text"></p>
<p><label>Headline Color</label> <br><input value="<?php echo $settings['headline_color']; ?>" name="realty_widget_<?php echo $caption; ?>[headline_color]" type="text"></p>
<p><label>Category Color</label> <br><input value="<?php echo $settings['category_color']; ?>" name="realty_widget_<?php echo $caption; ?>[category_color]" type="text"></p>
<p><label>Result Color</label> <br><input value="<?php echo $settings['result_color']; ?>" name="realty_widget_<?php echo $caption; ?>[result_color]" type="text"></p>