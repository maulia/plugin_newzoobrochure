<?php

$functions_path = TEMPLATEPATH . '/functions/';
$themename = get_option('stylesheet');
$options = array(
    array(
        "name" => "General",
        "type" => "open"),
    array(
        "name" => "Header Logo",
        "id" => "logo",
        "type" => "media_upload",
        ),
    array(
        "name" => "Footer Logo",
        "id" => "logo_footer",
        "type" => "media_upload",
        ),
    array(
        "name" => "Mobile Url",
        "id" => "mobile_url",
        "type" => "text"),
    array(
        "name" => "Ipad Url",
        "id" => "ipad_url",
        "type" => "text"),
    array(
        "name" => "Google Analytic Code",
        "id" => "ga_code",
        "type" => "text"),
    array(
        "type" => "close"),
    // array(
    //     "name" => "Contact Information Mackay",
    //     "type" => "open"),
    // array(
    //     "name" => "Company",
    //     "id" => "header_office_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Address",
    //     "id" => "header_address_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "City",
    //     "id" => "header_city_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Suburb",
    //     "id" => "header_suburb_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "State",
    //     "id" => "header_state_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Postal Code",
    //     "id" => "header_zip_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Phone",
    //     "id" => "header_phone_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Mobile",
    //     "id" => "header_mobile_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Fax",
    //     "id" => "header_fax_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Email",
    //     "id" => "header_email_mackay",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "type" => "close"),
    // array(
    //     "name" => "Contact Information Townsville",
    //     "type" => "open"),
    // array(
    //     "name" => "Company",
    //     "id" => "header_office",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Address",
    //     "id" => "header_address",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "City",
    //     "id" => "header_city",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Suburb",
    //     "id" => "header_suburb",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "State",
    //     "id" => "header_state",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Postal Code",
    //     "id" => "header_zip",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Phone",
    //     "id" => "header_phone",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Mobile",
    //     "id" => "header_mobile",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Fax",
    //     "id" => "header_fax",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Email",
    //     "id" => "header_email",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "type" => "close"),

    // array(
    //     "name" => "Contact Information Brisbane",
    //     "type" => "open"),
    // array(
    //     "name" => "Company",
    //     "id" => "header_office_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Address",
    //     "id" => "header_address_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "City",
    //     "id" => "header_city_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Suburb",
    //     "id" => "header_suburb_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "State",
    //     "id" => "header_state_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Postal Code",
    //     "id" => "header_zip_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Phone",
    //     "id" => "header_phone_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Mobile",
    //     "id" => "header_mobile_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Fax",
    //     "id" => "header_fax_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "name" => "Email",
    //     "id" => "header_email_brisbane",
    //     "type" => "text",
    //     "std" => "",
    //     "description" => ""),
    // array(
    //     "type" => "close"),

    array(    
        "name" => "Contact Information",
        "type" => "open"),
    array(
        "name" => "Address",
        "id" => "header_address",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "City",
        "id" => "header_city",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "State",
        "id" => "header_state",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "Postal Code",
        "id" => "header_zip",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "Phone",
        "id" => "header_phone",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "Mobile",
        "id" => "header_mobile",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "Fax",
        "id" => "header_fax",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "name" => "Email",
        "id" => "header_email",
        "type" => "text",
        "std" => "",
        "description" => ""),
    array(
        "type" => "close"),
    array(
        "name" => "Social Networking",
        "type" => "open"),
    array(
        "name" => "Facebook",
        "id" => "facebook",
        "type" => "text",
        "std" => ""),
    array(
        "name" => "Twitter",
        "id" => "twitter",
        "type" => "text",
        "std" => ""),
    array(
        "name" => "Instagram",
        "id" => "instagram",
        "type" => "text",
        "std" => ""),
    array(
        "name" => "LinkedIn",
        "id" => "linkedin",
        "type" => "text",
        "std" => ""),
    array(
        "name" => "YouTube",
        "id" => "youtube",
        "type" => "text",
        "std" => ""),
    // array(
    //     "name" => "Display RSS Feed",
    //     "id" => "display_rss",
    //     "std" => "",
    //     "status" => 'checked',
    //     "type" => "checkbox",
    // ),
    array(
        "type" => "close"),
   //  array(
   //      "name" => "Homepage Footer",
   //      "type" => "open"),
   //  array(
   //      "name" => "Home Career Image",
   //      "id" => "career_image",
   //      "type" => "media_upload",
   //      ),
   // array(
   //      "name" => "Our People Image",
   //      "id" => "ourpeople_image",
   //      "type" => "media_upload",
   //      ),
   //  array(
   //      "name" => "Explore Townsville",
   //      "id" => "townsville_image",
   //      "type" => "media_upload",
   //      ),
   //  array(
   //      "type" => "close"),
    array(
        "type" => "slideshow"),
    array(
        "type" => "close"),
    /*
    array(
        "name" => "Homepage Video",
        "type" => "open"),
    array(
        "name" => "",
        "id" => "homepage_video",
        "type" => "video",
        ),
    array(
        "type" => "close")
    */
    /*
    array(
        "name" => "Company Profile",
        "type" => "open"),
    array(
        "name" => "Company Profile",
        "id" => "company_profile",
        "type" => "textarea",
        "std" => ""),
    array(
        "name" => "Company Profile Link",
        "id" => "company_profile_link",
        "type" => "input",
        "std" => ""),
    array(
        "type" => "close"),
    */
);
// function my_menu_notitle( $menu ){
  // return $menu = preg_replace('/ title=\"(.*?)\"/', '', $menu );

// }
// add_filter( 'wp_nav_menu', 'my_menu_notitle' );
// add_filter( 'wp_page_menu', 'my_menu_notitle' );
// add_filter( 'wp_list_categories', 'my_menu_notitle' );
require_once ($functions_path . 'admin-init.php');
require_once ($functions_path . 'widget.php');
require_once ($functions_path . 'post.php');
require_once ($functions_path . 'custom-login.php');
require_once ($functions_path . 'others.php');

add_filter('gettext', 'howdy_message', 10, 3);
?>
