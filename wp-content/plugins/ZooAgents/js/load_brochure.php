<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');
global $helper, $realty, $wpdb;
$id=$_GET['id'];
$office_id=$_GET['office_id'];
$brochures=$realty->brochure($id);
if(!empty($brochures)){ 
	foreach($brochures as $key=>$brochure):
	$ids=$wpdb->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='brochure'", ARRAY_A);
	$ids=$ids[0]['ids'];
	?>
	<li>						
		<input type="hidden" name="brochure_ids[]" value="<?php echo $ids; ?>">
		<a href="<?php echo $brochure['url']; ?>" target="blank"><?php echo $brochure['description']; ?></a>
		<a class="delete-pdf" onClick="return confirm('You are about to delete the this brochure.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=brochure&office_id=$office_id&task=delete&id=$id&ids=$ids";?>">Delete</a>
		<div class="clear"></div>
	</li>
	<?php endforeach; 
}
?>