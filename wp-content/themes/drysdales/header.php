<?php

// set cookie for ipad mobile redirect
if(isset($_GET['main_site'])) {
  setcookie("main_site", 1,time()+(60*60), '/');
  $_COOKIE['main_site']=1;
}
if(isset($_GET['classic_site'])) {
  setcookie("main_site", 1,time()+(60*60), '/');
  $_COOKIE['classic_site']=1;
}

global $realty, $options;
foreach ($options as $value) {
    if (get_option($value['id']) === FALSE) {
        $$value['id'] = $value['std'];
    } else {
        $$value['id'] = get_option($value['id']);
    }
}
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

    <head profile="http://gmpg.org/xfn/11">

        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="<?php bloginfo('name'); ?>" />
        <meta name="copyright" content="<?php bloginfo('name'); ?>" />
        <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />

        <title><?php //bloginfo('name'); ?> <?php //if (is_single()) { ?> &raquo; Blog Archive <?php// } ?> <?php //wp_title(); ?></title>
        <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('stylesheet_url'); ?>?v=1.00" />
        <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/media.css?v=1.00" />
        <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie-only.css" />
        <![endif]-->
        <link rel="shortcut icon" type="image/ico" href="<?php echo bloginfo('template_url'); ?>/favicon.ico?v=1.01" />
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php 
          if(empty($_COOKIE['main_site']) && !empty($mobile_url)){
            $text = $_SERVER['HTTP_USER_AGENT'];
            $var[1] = 'Windows CE';
            $var[2] = 'Blazer/';
            $var[3] = 'Palm';
            $var[4] = 'EPOC32-WTL/';
            $var[5] = 'Netfront/';
            $var[6] = 'Mobile Content Viewer/';
            $var[7] = 'PDA';
            $var[8] = 'BlackBerry';
            $var[9] = 'MOT-';
            $var[10] = 'UP.Link/';
            $var[11] = 'Smartphone';
            $var[12] = 'portalmmm/';
            $var[13] = 'Nokia';
            $var[14] = 'Symbian';
            $var[15] = 'J2ME';
            $var[16] = 'Mini/';
            $var[17] = 'AU-MIC/';
            $var[18] = 'Sharp';
            $var[19] = 'SIE-';
            $var[20] = 'SonyEricsson';
            $var[21] = 'SAMSUNG-';
            $var[22] = 'Panasonic';
            $var[23] = 'Siemens';
            $var[24] = 'Sony';
            $var[25] = 'Pocket PC';
            $var[26] = 'T-Mobile';
            $var[27] = 'Motorola';
            $var[28] = 'Samsung';
            $var[29] = 'iPhone';
            $var[30] = 'iPod';
            $var[31] = 'Wap';
            $var[33] = 'android';
            $var[34] = 'vodafone';
            $var[35] = 'ericy';
            $var[36] = 'alcatel';
            $var[37] = 'au-mic';
            $var[38] = 'WinW';
            $var[39] = 'philips';
            $var[40] = 'sanyo';
            $var[41] = 'avantgo';
            $var[42] = 'danger';
            $var[43] = 'series60';
            $var[44] = 'palmsource';
            $var[45] = 'rover';
            $var[46] = 'ipaq';
            $var[47] = 'nitro';
            $var[48] = 'midp';
            $var[49] = 'cldc';
            $var[50] = 'audiovox';
            $var[51] = 'LG';
            $var[52] = 'Handspring';
                if(is_front_page()){
                  foreach($var as $item){
                    $ausg = stristr($text, $item);
                    if(strlen($ausg)>0 && strpos($text,'MSIE')===false)
                    {
                    ?>
                    <script type="text/javascript">
                      location.href='<?php echo $mobile_url; ?>';
                    </script>
                    <?php
                    }
                  }
                }
                elseif(is_page('property')){
                  global $wp_query;
                  $query_vars=$wp_query->query_vars;
                  $property_id=$query_vars['property_id'];
                  foreach($var as $item){
                    $ausg = stristr($text, $item);
                    if(strlen($ausg)>0 && strpos($text,'MSIE')===false)
                    {     
                      ?>
                      <script type="text/javascript">
                        location.href='<?php echo $mobile_url.'/'.$property_id.'/'; ?>';
                      </script>
                      <?php
                    }
                  }
                }
              }
              //ipad redirection
              if(empty($_GET['classic_site']) && !empty($ipad_url)){
                $ausg = stristr($text, 'iPad');
                if(strlen($ausg)>0 && strpos($text,'MSIE')===false){
                  if(is_front_page()){
                    ?>
                    <script type="text/javascript">
                      location.href='<?php echo $ipad_url; ?>';
                    </script>
                    <?php
                  }
                  elseif(is_page('property')){
                    global $wp_query;
                    $query_vars=$wp_query->query_vars;
                    $property_id=$query_vars['property_id'];
                    ?>
                    <script type="text/javascript">
                      location.href='<?php echo $ipad_url.'/'.$property_id.'/'; ?>';
                    </script>
                    <?php
                  }
                } 
              } ?>

        <?php if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { ?>
            <!-- IE < 9. Oswald replacement-->
            <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/ie.css?v=0.01" />
            <?php } ?>

            <?php
            wp_deregister_script('l10n');
            wp_print_scripts('jquery');
            ?>

            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.cycle2.js"></script>
            <?php if (is_page('home')) { ?>
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.1.3.js"></script>
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.cycle.all.min.js"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('.slideshow').after('<div id="slide-paging">').cycle({
                            fx: 'fade', timeout: 5000, delay: 3000, pager: '#slide-paging'
                        });

                         jQuery('.agent_slideshow').after('<div id="slide-paging">').cycle({
                            fx: 'fade', timeout: 3000, delay: 2000, pager: '#slide-paging'
                        });
                    });
                </script>
        		
            <?php } ?>
            <?php if (is_page('property')) {?>

            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.magnific-popup.js"></script>
            <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/magnific-popup.css ?>" />  
            <?php } ?>

            <?php wp_head(); ?>
	<?php /*
            
	*/?>
    
    <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/update.css?v=1.00" />
    <script type="text/javascript">
        jQuery(window).on('ready resize', function(){
            if(jQuery('#thumbnail_format .image').length) {
            var t=jQuery('#thumbnail_format .image'),w=t.width(),h=t.height(w/1.5);
            }	
        });
    </script>
	
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('p:empty').remove();
			jQuery('#agent #list_format .image:parent').each(function () {
			jQuery(this).insertBefore(jQuery(this).prev('#agent #list_format .details'));
		});
		});
    </script>
    
    </head>

    <body id="<?php body_css_id(); ?>" <?php body_class(); ?>>

        <div class="wrap">

            <div id="header">

                <div id="headerwrap">
                        <div class="subtitle"><?php //if(is_page('home')) { echo"<h1>";bloginfo('name');echo "</h1>"; } ?></div>
                        <a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?> <?php bloginfo('description'); ?>">
                               <!-- <h1> --><div class="logo">
                            <?php /* <img src="<?php bloginfo('url') ?>/wp-content/uploads/homepage_images/<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" /> */ ?>
                            <img src="<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" />
                         </div><!-- </h1> -->
                        </a>
                   

                    <?php
                    /*
                      <div class="site-description">
                      <p class="tagline"><?php
                      if (!empty($header_office)) {
                      echo $header_office;
                      } else {
                      bloginfo('description');
                      }
                      ?></p>
                      <p class="address">
                      <span><?php echo $header_address; ?>, </span>
                      <span><?php echo $header_city; ?>, </span>
                      <span><?php echo $header_state; ?>, </span>
                      <span><?php echo $header_zip; ?></span><br>
                      <span>P: <?php echo $header_phone; ?></span>
                      <span>F: <?php echo $header_fax; ?></span>
                      </p>
                      </div>
                     */
                    ?>
                    <div class="site-description">
                            <div class="col-md-3 sections footer-icons">
                              <div class="social left">
                                <?php include('includes/follow.php'); ?>
                              </div>
                              <!--/.social-->
                            </div>
                            <div class="search-content"> <?php /*
                                <div class="reit"><img src="<?php echo site_url(); ?>/wp-content/themes/rwacton/images/reit-logo.jpg?v=1.01"/></div>
                                <div class="search-form">
                                    <?php get_search_form(); ?>
                                </div> */ 

				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Quick Search Sidebar')) {
                
            			}
				?>
								<div class="clear"></div>
                            </div>
                    </div>
                </div><div class="clear"></div>
                <div class="toggle-menu menu-icon hidden"><span class="icon-arrow-down"></span> Menu </div>
                <nav class="main-nav" role="navigation">
                <div id="nav">
					<?php/* if (isset($_GET['test']) && $_GET['test'] == 1) { ?>
                    <?php wp_nav_menu(array('menu' => 'Main Nav', 'container_class' => 'nav', 'after' => '<span class="separator"></span>')); ?>
					<?php } else { ?>
                    <?php wp_nav_menu(array('menu' => 'Menu test', 'container_class' => 'nav', 'after' => '<span class="separator"></span>')); ?>
					<?php } */?>
					<?php //wp_nav_menu(array('menu' => 'Main Nav', 'container_class' => 'nav', 'after' => '<span class="separator"></span>')); ?>
					<?php wp_nav_menu(array('theme_location'=>'primary', 'container_class' => 'nav', 'after' => '<span class="separator"></span>')); ?>
                </div>
              </nav>

            </div><!-- end #header -->
			<div class="red-space"><div class="clear"></div></div>

            <div id="main_body">
		  <div class="content">
