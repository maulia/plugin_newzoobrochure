<?php 
$sociable_known_sites = Array(

	'BlinkList' => Array(
		'favicon' => 'blinklist.png',
		'url' => 'http://www.blinklist.com/index.php?Action=Blink/addblink.php&amp;Url=PERMALINK&amp;Title=TITLE',
	),

	'Brightkite' => Array(
		'favicon' => 'brightkite.png',
		'url' => 'http://www.brightkite.com/',
	),
	
	'del.icio.us' => Array(
		'favicon' => 'delicious.png',
		'url' => 'http://delicious.com/post?url=PERMALINK&amp;title=TITLE&amp;notes=EXCERPT',
	),

	'Design Float' => Array(
		'favicon' => 'design_float.png',
		'url' => 'http://www.designfloat.com/submit.php?url=PERMALINK&amp;title=TITLE',
	),

	'Digg' => Array(
		'favicon' => 'digg.png',
		'url' => 'http://digg.com/submit?phase=2&amp;url=PERMALINK&amp;title=TITLE&amp;bodytext=EXCERPT',
		'description' => 'Digg',
	),
	
	'Dopplr' => Array(
		'favicon' => 'dopplr.png',
		'url' => 'http://www.dopplr.com/',
	),

	'Email' => Array(
		'favicon' => 'email.png',
		'url' => 'mailto:?subject=TITLE&amp;body=PERMALINK',
		'awesm_channel' => 'mailto',
		'description' => __('E-mail this story to a friend!','sociable'),
	),

	'Facebook' => Array(
		'favicon' => 'facebook.png',
		'awesm_channel' => 'facebook-post',
		'url' => 'http://www.facebook.com/share.php?u=PERMALINK&amp;t=TITLE',
	),

	'Feed' => Array(
		'favicon' => 'feed.png',
		'url' => 'FEEDLINK',
	),
	
	'Flickr' => Array(
		'favicon' => 'flickr.png',
		'url' => 'http://www.flickr.com/',
	),
	
	'FriendFeed' => Array(
		'favicon' => 'friendfeed.png',
		'url' => 'http://www.friendfeed.com/share?title=TITLE&amp;link=PERMALINK',
	),
	
	'Furl' => Array(
		'favicon' => 'furl.png',
		'url' => 'http://www.furl.com/',
	),

	'Gamespot' => Array(
		'favicon' => 'gamespot.png',
		'url' => 'http://www.gamespot.com/',
	),

	'Lastfm' => Array(
		'favicon' => 'lastfm.png',
		'url' => 'http://www.lastfm.com/',
	),
	
	'LinkedIn' => Array(
		'favicon' => 'linkedin.png',
		'url' => 'http://www.linkedin.com/shareArticle?mini=true&amp;url=PERMALINK&amp;title=TITLE&amp;source=BLOGNAME&amp;summary=EXCERPT',
	),

	'Magnolia' => Array(
		'favicon' => 'magnolia.png',
		'url' => 'http://www.magnolia.com/',
	),
	
	'Mixx' => Array(
		'favicon' => 'mixx.png',
		'url' => 'http://www.mixx.com/submit?page_url=PERMALINK&amp;title=TITLE',
	),
	
	'MySpace' => Array(
		'favicon' => 'myspace.png',
		'awesm_channel' => 'myspace',
		'url' => 'http://www.myspace.com/Modules/PostTo/Pages/?u=PERMALINK&amp;t=TITLE',
	),

	'NewsVine' => Array(
		'favicon' => 'newsvine.png',
		'url' => 'http://www.newsvine.com/_tools/seed&amp;save?u=PERMALINK&amp;h=TITLE',
	),

	'Posterous' => Array(
		'favicon' => 'posterous.png',
		'url' => 'http://posterous.com/share?linkto=PERMALINK&amp;title=TITLE&amp;selection=EXCERPT',
	),
	
	'Reddit' => Array(
		'favicon' => 'reddit.png',
		'url' => 'http://reddit.com/submit?url=PERMALINK&amp;title=TITLE',
	),

	'Sphere' => Array(
		'favicon' => 'sphere.png',
		'url' => 'http://www.sphere.com/search?q=sphereit:PERMALINK&amp;title=TITLE',
	),

	'Sphinn' => Array(
		'favicon' => 'sphinn.png',
		'url' => 'http://sphinn.com/index.php?c=post&m=submit&link=PERMALINK',
	),

	'Stumble' => Array(
		'favicon' => 'stumble.png',
		'url' => 'http://www.stumbleupon.com/submit?url=PERMALINK&amp;title=TITLE',
	),

	'Technorati' => Array(
		'favicon' => 'technorati.png',
		'url' => 'http://technorati.com/faves?add=PERMALINK',
	),
	
	'Tripadvisor' => Array(
		'favicon' => 'tripadvisor.png',
		'url' => 'http://tripadvisor.com/',
	),

	'Tumblr' => Array(
		'favicon' => 'tumblr.png',
		'url' => 'http://www.tumblr.com/share?v=3&amp;u=PERMALINK&amp;t=TITLE&amp;s=EXCERPT',
	),

	'Twitter' => Array(
		'favicon' => 'twitter.png',
		'awesm_channel' => 'twitter',
		'url' => 'http://twitter.com/home?status=TITLE%20-%20PERMALINK',
	),

	'Vimeo' => Array(
		'favicon' => 'vimeo.png',
		'url' => 'http://www.vimeo.com/',
	),
	
	'Youtube' => Array(
		'favicon' => 'youtube.png',
		'url' => 'http://www.youtube.com/',
	),
);
//print_r ($sociable_known_sites);
?>

<table cellspacing="0" class="widefat fixed" border="0" id="table-dnd-photos">
<?php foreach($sociable_known_sites as $key=>$values):?>	
	<tr class="dragHandle" style="cursor:move;"><td ><input value="1" name="realty_widget_<?php echo $caption; ?>[display][<?php echo $key; ?>]" type="checkbox"<?php if($settings['display'][$key]) echo ' checked="checked"'; ?>><img src="<?php echo get_option('siteurl').'/wp-content/plugins/Realty/manage/widgets/images/'.$values['favicon'];?> "><?php echo $key; ?></td></tr>
<?php endforeach; ?>
	</table>
<input type="hidden" name="realty_widget_<?php echo $caption; ?>[temp]">