<?php
global $themename, $shortname, $options, $realty;
$images_location = get_option( 'siteurl' ) . "/wp-content/uploads/homepage_images/";
$videos_location = get_option( 'siteurl' ) . '/wp-content/uploads/homepage_videos/';
?>
<form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" enctype="multipart/form-data">
    <input type="hidden" name="zoo-settings-submit" value="Y" />
    <div class="leftside">

        <?php
        foreach ( $options as $value ) {
            switch ( $value['type'] ) {

                case "open":
                    ?>
                    <table class="widefat maintable" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="3" scope="col" class="manage-column column-title"><?php echo $value['name']; ?></th>
                            </tr>
                        </thead>
                        <?php
                        break;

                    case "close":
                        ?>
                    </table>
                    <?php
                    break;

                case "title":
                    ?>
                    <tr><td colspan="3"><h3><?php echo $value['name']; ?></h3></td></tr>

                    <?php
                    break;

                case 'text':
                    ?>

                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" <?php if ( empty( $value['description'] ) ) echo 'colspan="2"'; ?>><input class="widefat" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php
                            if ( get_option( $value['id'] ) != "" ) {
                                echo get_option( $value['id'] );
                            } else {
                                echo $value['std'];
                            }
                            ?>" /></td>
                            <?php
                            if ( !empty( $value['description'] ) ) {
                                ?><td class="instruction"><?php echo $value['description']; ?></td><?php } ?>
                    </tr>

                    <?php
                    break;

                case 'image':
                    ?>

                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" colspan="2"><img src="<?php bloginfo( 'template_directory' ); ?>/img/<?php
                            if ( get_option( $value['id'] ) != "" ) {
                                echo stripslashes( get_option( $value['id'] ) );
                            } else {
                                echo stripslashes( $value['std'] );
                            }
                            ?>"  /></td>
                    </tr>

                    <?php
                    break;

                case 'textarea':
                    ?>

                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" colspan="2">
                            <textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="80" rows="100"><?php
                                if ( get_option( $value['id'] ) != "" ) {
                                    echo get_option( $value['id'] );
                                } else {
                                    echo $value['std'];
                                }
                                ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="instruction" colspan="3"><?php echo stripslashes( str_replace( '\\', '', $value['description'] ) ); ?></td>
                    </tr>

                    <?php
                    break;

                case 'select':
                    ?>
                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" <?php if ( empty( $value['description'] ) ) echo 'colspan="2"'; ?>>
                            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php
                                foreach ( $value['options'] as $option ) {
                                    ?><option<?php
                                    if ( get_option( $value['id'] ) == $option ) {
                                        echo ' selected="selected"';
                                    } elseif ( get_option( $value['id'] ) == '' && $option == $value['std'] ) {
                                        echo ' selected="selected"';
                                    }
                                    ?>><?php echo $option; ?></option><?php } ?></select>
                        </td>
                        <?php
                        if ( !empty( $value['description'] ) ) {
                            ?><td class="instruction"><?php echo $value['description']; ?></td><?php } ?>
                    </tr>

                    <?php
                    break;

                case "checkbox":
                    ?>
                    <tr>

                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" <?php if ( empty( $value['description'] ) ) echo 'colspan="2"'; ?>>
                            <?php
                            if ( get_option( $value['id'] ) != "" ) {
                                $status = get_option( $value['id'] );
                            } else {
                                $status = $value['std'];
                            }
                            ?>
                            <input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php
                            if ( get_option( $value['id'] ) != "" ) {
                                echo get_option( $value['id'] );
                            } else {
                                echo $value['std'];
                            }
                            ?>" <?php
                                   if ( $status == 1 ) {
                                       echo 'checked';
                                   }
                                   ?>/>
                        </td>
                        <?php
                        if ( !empty( $value['description'] ) ) {
                            ?><td class="instruction"><?php echo $value['description']; ?></td><?php } ?>
                    </tr>

                    <?php
                    break;

                case "radio":
                    ?>
                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field followme" colspan="2">
                            <?php
                            foreach ( $value['options'] as $option ) {
                                ?>
                                <label class="<?php echo $option; ?>"><span><?php echo $option; ?></span><input name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" class="<?php echo $value['type']; ?>" value="<?php echo $option; ?>"<?php
                                    if ( get_option( $value['id'] ) == $option ) {
                                        echo ' checked="checked"';
                                    } elseif ( get_option( $value['id'] ) == '' && $option == $value['std'] ) {
                                        echo ' checked="checked"';
                                    }
                                    ?> /></label>
                                                                                                                <?php } ?>
                            <div style="clear:both"></div></td>
                    </tr>

                    <?php
                    break;

                case "input":
                    ?>
                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" <?php if ( empty( $value['description'] ) ) echo 'colspan="2"'; ?>>
                            <input class="widefat" type="<?php echo $value['type']; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="<?php
                            if ( get_option( $value['id'] ) != "" ) {
                                echo stripslashes( get_option( $value['id'] ) );
                            } else {
                                echo stripslashes( $value['std'] );
                            }
                            ?>" />
                        </td>
                        <?php
                        if ( !empty( $value['description'] ) ) {
                            ?><td class="instruction"><?php echo $value['description']; ?></td><?php } ?>
                    </tr>		 
                    <?php
                    break;

                // case 'image_upload':
                    ?>

                    <?php /*  <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" colspan="2">
                            <?php
                            $image_value = get_option( $value['id'] );
                            $image_upload = ( $image_value != "") ? stripslashes( $image_value ) : stripslashes( $value['std'] );
                            if ( $image_upload != '' ):
                                ?> 
                                <img src="<?php echo $images_location . $image_upload; ?>"><br/>
                            <?php endif; ?>
                            <input type="file" name="<?php echo $value['id']; ?>" value="">	
                        </td>
                    </tr> */ ?>

                    <?php
                    // break;
                case 'video':
                    ?>

                    <tr>
                        <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
                        <td class="field" colspan="2">
                            <div style="display: none;"><?php print_r( $value['id'] ); ?></div>
                            <?php
                            $video = get_option( $value['id'] );
                            $video = ( $video != "") ? stripslashes( $video ) : stripslashes( $value['std'] );
                            if ( $video != '' ):
                                ?>
                                <div id="video_container">Loading the player ...</div>
                                <script src="<?php echo get_template_directory_uri() . '/js/jwplayer/jwplayer.js'; ?>"></script>
                                <script src="<?php echo get_template_directory_uri() . '/js/jwplayer/jwplayer.html5.js'; ?>"></script>
                                <script type="text/javascript">
                                    jwplayer("video_container").setup({
                                        flashplayer: "<?php echo get_template_directory_uri() . '/js/jwplayer/jwplayer.flash.swf'; ?>",
                                        file: "<?php echo $videos_location . $video; ?>",
                                        height: 180,
                                        width: 272
                                    });
                                </script>
                                <?php
                            /* ?> 
                              <script type="text/javascript" src="<?php echo $realty->pluginUrl; ?>js/flowplayer-3.2.6.min.js"></script>
                              <script src="<?php echo $realty->pluginUrl; ?>js/flowplayer.ipad-3.2.2.min.js"></script>
                              <a href="<?php echo $videos_location . $video; ?>" style="display:block;width:272px;height:180px" id="ipad"></a>
                              <script>
                              flowplayer("ipad", "<?php echo $realty->pluginUrl; ?>js/flowplayer.controls-3.2.5.swf", {
                              clip: {
                              autoPlay: false,
                              autoBuffering: true
                              }
                              });
                              </script>
                              <?php */
                            /* <div id="video_container">Loading the player ...</div>
                              <script type="text/javascript">
                              jwplayer("video_container").setup({
                              flashplayer: "http://www.videoproductions.com.au/video/player.swf",
                              file: "<?php echo $videos_location.$top_right_video; ?>",
                              skin: "http://www.videoproductions.com.au/video/newtubedark.xml",
                              height: 180,
                              width: 272
                              });
                              </script> */
                            endif;
                            ?>
                            <input type="file" name="<?php echo $value['id']; ?>" value="">	
                        </td>
                    </tr>
                    <?php
                    if ( !empty( $value['description'] ) ) {
                        ?>
                        <tr>
                            <td class="instruction" colspan="3"><?php echo stripslashes( str_replace( '\\', '', $value['description'] ) ); ?></td>
                        </tr>
                        <?php
                    }
                    break;
                    case 'media_upload': ?>
        <tr>
            <th class="rowheader"><strong><?php echo $value['name']; ?></strong></th>
            <td class="field" colspan="2">
            <div class="uploader">
                <?php
                if($image=get_option( $value['id'])){
                ?>
                <img src="<?php echo $image; ?>" style="max-width:500px"><br/>
                <?php
                } ?>
                <input id="<?php echo $value['id']; ?>" name="<?php echo $value['id']; ?>" value="<?php echo $image; ?>" type="text" style="width:500px" />
                <input id="<?php echo $value['id']; ?>_button" class="button" name="<?php echo $value['id']; ?>_button" type="text" value="Upload" />
            </div> 
            </td>
        </tr>
        <?php
        break;
        case 'slideshow':
        $homepage_images=get_option('homepage_images');
        if(!$homepage_images)$homepage_images=array();
        ?>
        <table class="widefat maintable" cellspacing="0">
            <thead>
            <tr>
                <th colspan="3" scope="col" class="manage-column column-title">Homepage Images Slider (Drag and drop to reorder)<a class="button right" href="javascript:addSlide();" title="Add New Image"><i class="fa fa-plus-circle"></i> Add New Image</a></th>
            </tr>
            </thead> 
            <tr>
                <td colspan="3">
                    <div id="photo_list">
                    <?php 
                    if(!empty($homepage_images)){
                        $theValue=0;
                        foreach($homepage_images as $item){ 
                            $theValue++;
                            $divIdName='my'.$theValue.'photo_list';
                            $image_id='homepage_images_image_'.$theValue;
                            ?>
                            <div id="<?php echo $divIdName; ?>">
                                <table class="widefat maintable">
                                <tr>
                                    <td class="rowheader"><?php echo $theValue; ?>. Image</td>
                                    <td><?php if($item['image']){   ?>
                                        <img src="<?php echo $item['image']; ?>" style="max-width:500px"><br/>
                                        <?php } ?>
                                        <div class="uploader">
                                        <input id="<?php echo $image_id; ?>" name="homepage_images[<?php echo $theValue; ?>][image]" value="<?php echo $item['image']; ?>" type="text" style="width:500px" />
                                        <input id="<?php echo $image_id; ?>_button" class="button" name="<?php echo $image_id; ?>_button" type="text" value="Upload" />
                                        </div>
                                    </td>
                                </tr>
                                <tr><td class="rowheader">Title</td><td><textarea class="small_textarea" name="homepage_images[<?php echo $theValue; ?>][text]" style="height:100px"><?php echo str_replace("\\","",stripslashes($item['text'])); ?></textarea></td></tr>
                                <tr><td class="rowheader">Link</td><td><input class="widefat" type="text" name="homepage_images[<?php echo $theValue; ?>][link]" value="<?php echo $item['link']; ?>"  ></td></tr>
                                <tr><td colspan="2"><a class="del" href="javascript:removeSlide('<?php echo $divIdName; ?>');" title="Delete"><i class="fa fa-minus-circle"></i> Delete</a></td></tr>
                                </table>
                            </div>              
                        <?php }
                    } ?>

                    </div>
                </td>
            </tr>
            
        </table>
        <?php   
        break;
            }
        }
        
        /*
        // Slideshow image 
        $photos = get_option( 'homepage_images' );
        $themes_folder = get_bloginfo( 'stylesheet_directory' ) . '/';
        $uploaded_path = ABSPATH . 'wp-content/uploads/slideshow/';
        $photos_upload_url = get_option( 'siteurl' ) . '/wp-content/uploads/slideshow/';
        wp_print_scripts( 'jquery-ui-core' );
        wp_print_scripts( 'jquery-ui-sortable' );
        <link rel="stylesheet" href="<?php echo $themes_folder; ?>uploadify/uploadify.css" type="text/css" media="all" /> 
        <script type="text/javascript" src="<?php echo $themes_folder; ?>uploadify/swfobject.js"></script>
        <script type="text/javascript" src="<?php echo $themes_folder; ?>uploadify/jquery.uploadify.v2.1.4.js"></script>
        <script type="text/javascript"> -->
                    var uploader = '<?php echo $themes_folder; ?>uploadify/uploadify.swf';
                    var script_photo = '<?php echo $themes_folder; ?>uploadify/uploadify_photo.php';
                    var photos_upload_path = '<?php echo $uploaded_path; ?>';
                    var load_photo = '<?php echo $themes_folder . "uploadify/load_photo.php?key="; ?>';
                    var reorder_photo = '<?php echo $themes_folder . "uploadify/reorder_photo.php"; ?>';
                    var button_image = '<?php echo $themes_folder; ?>uploadify/select_files.png';
                    var button_width = '110';
                    var button_height = '26';
                    var cancel_image = '<?php echo $themes_folder; ?>uploadify/cancel.png';
                    jQuery(function() {
                        jQuery('#photo_upload').uploadify({
                            'uploader': uploader,
                            'script': script_photo,
                            'buttonImg': button_image,
                            'width': button_width,
                            'height': button_height,
                            'cancelImg': cancel_image,
                            'rollover': true,
                            'folder': photos_upload_path,
                            'scriptData': {
                                'z_authentication': 1
                            },
                            'multi': true,
                            'auto': true,
                            'fileExt': '*.jpg;*.gif:;*.png',
                            'fileDesc': 'Image Files (.JPG, .GIF, .PNG)',
                            'queueID': 'photo_queue',
                            'queueSizeLimit': 30,
                            'simUploadLimit': 30,
                            'removeCompleted': true,
                            'onSelect': function(event, data) {
                                jQuery('#photo_message').text(data.filesSelected + ' files have been added to the queue.');
                            },
                            'onComplete': function(event, ID, fileObj, response, data) {
                                jQuery('#photo_list').load(load_photo);
                            },
                            'onAllComplete': function(event, data) {
                                jQuery('#photo_message').text(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
                            },
                            'onQueueFull': function(event, queueSizeLimit) {
                                jQuery('#photo_message').text("Maximum upload of photos is 30.");
                                return false;
                            }
                        });
                        function slideout() {
                            setTimeout(function() {
                                jQuery("#response").slideUp("slow", function() {
                                });

                            }, 2000);
                        }
                        jQuery("#response").hide();
                        jQuery(function() {
                            jQuery("#photo_list").sortable({
                                opacity: 0.8,
                                cursor: 'move',
                                update: function() {
                                    document.getElementById('overlay').style.visibility = 'visible';
                                    var order = jQuery(this).sortable("serialize");
                                    jQuery.post(reorder_photo, order, function(theResponse) {
                                        jQuery("#response").html(theResponse);
                                        jQuery("#response").slideDown('slow');
                                        slideout();
                                        jQuery('#photo_list').load(load_photo);
                                        document.getElementById('overlay').style.visibility = 'hidden';
                                    });
                                }
                            });
                        });
                    });
                    function delete_photo(key) {
                        jQuery('#photo_list').load(load_photo + key);
                    } 
        </script> 
        <div id="response"> </div>
        <div id="overlay"></div>
        <table class="widefat maintable" cellspacing="0">
            <thead>
                <tr>
                    <th colspan="3" scope="col" class="manage-column column-title">Homepage Images (Drag and drop to reorder)</th>
                </tr>
            </thead> 
            <tr>
                <td colspan="3">
                    <ul class="image_list" id="photo_list"> 
                        if ( !empty( $photos ) ) {
                            foreach ( $photos as $key => $photo ):
                                if ( !empty( $photo['large'] ) ) {
                                    ?>
                                    <li id="arrayorder_<?php echo $key ?>">
                                        <img src="<?php echo $photos_upload_url . $photo['thumbnail']; ?>"/>
                                        <a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.');" href="javascript:delete_photo('<?php echo $key; ?>');" title="Delete Image"></a>
                                        <br/><span>Text: <input type="text" name="homepage_images[<?php echo $key ?>][text]" value="<?php echo $photo['text']; ?>"></span>
                                        <br/><span>Link: <input type="text" name="homepage_images[<?php echo $key ?>][link]" value="<?php echo $photo['link']; ?>"></span>
                                        <input type="hidden" name="homepage_images[<?php echo $key ?>][large]" value="<?php echo $photo['large']; ?>">
                                        <input type="hidden" name="homepage_images[<?php echo $key ?>][thumbnail]" value="<?php echo $photo['thumbnail']; ?>">
                                    </li> 
                                    <?php
                                } endforeach;
                        }
                        ?>

                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="photo-upload-box">
                        <div id="photo_message" class="upload_message">Select some files to upload:</div>
                        <div id="photo_queue" class="custom-queue"></div>
                        <div class="photo-upload-btn"><input id="photo_upload" type="file" name="Filedata" /></div>
                    </div>
                </td>
            </tr>
        </table> */ ?>
        <script type="text/javascript">
        jQuery(document).ready(function(){
            var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
             
            jQuery('.uploader .button').click(function(e) {
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(this);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;
                wp.media.editor.send.attachment = function(props, attachment){
                if ( _custom_media ) {
                jQuery("#"+id).val(attachment.url);
                } else {
                return _orig_send_attachment.apply( this, [props, attachment] );
                };
                }
                 
                wp.media.editor.open(button);
                return false;
                });
             
            jQuery('.add_media').on('click', function(){
                _custom_media = false;
            });
            
            jQuery("#photo_list").sortable();
        }); 
        
        var theValue=<?php echo count($homepage_images); ?>;
        
        function addSlide() {
            var ni = document.getElementById('photo_list');
            theValue++;
            var newdiv = document.createElement('div');
            var divIdName = 'my'+theValue+'photo_list';   
            var image_name = 'homepage_images['+theValue+'][image]';
            var image_id = 'homepage_images_image_'+theValue;
            var text_name = 'homepage_images['+theValue+'][text]';
            var link_name = 'homepage_images['+theValue+'][links]';
            newdiv.setAttribute('id',divIdName);
            newdiv.innerHTML =  '<table class="widefat maintable">'+
                                '<tr><td class="rowheader">'+theValue+'. Image</td>'+
                                '<td><div class="uploader"><input id="'+image_id+'" name="'+image_name+'" type="text" style="width:500px" />'+
                                '<input id="'+image_id+'_button" class="button" name="'+image_id+'_button" type="text" value="Upload" /></div></td></tr>'+
                                '<tr><td class="rowheader">Title</td><td><textarea class="small_textarea" name="'+text_name+'" style="height:100px"></textarea></td></tr>'+
                                '<tr><td class="rowheader">Link</td><td><input class="widefat" type="text" name="'+link_name+'"  ></td></tr>'+
                                '<tr><td colspan="2"><a class="del" href="javascript:removeSlide(\''+divIdName+'\');" title="Delete">'+
                                '<i class="fa fa-minus-circle"></i> Delete</a></td></tr>'+                              
                                '</table>';
            ni.appendChild(newdiv);     
            
            
            jQuery('.uploader .button').click(function(e) {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(this);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment){
            if ( _custom_media ) {
            jQuery("#"+id).val(attachment.url);
            } else {
            return _orig_send_attachment.apply( this, [props, attachment] );
            };
            }
             
            wp.media.editor.open(button);
            return false;
            });
            
            jQuery("#photo_list").sortable();
        }

        function removeSlide(divNum) {
            var d = document.getElementById('photo_list');
            var olddiv = document.getElementById(divNum);
            d.removeChild(olddiv);
            theValue--;
        }   

    </script>

        <p class="submit">
            <input name="save" type="submit" value="Save changes" class="button button-primary" />
        </p>
        <?php  ?>
    </div>

    <div id="poststuff">
        <div class="postbox">
            <h3>Update Data</h3>

            <div class="inside">
                <p class="submit" style="text-align:center;">
                    <input name="save" type="submit" value="Save changes" class="button button-primary" />
                </p>

            </div>

            <div class="clear"></div>
        </div>
    </div>
</form>
