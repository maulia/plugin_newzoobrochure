<div class="wrap">	
	<h2>Offices Data</h2>
	<p><a href="<?php echo $this->form_action; ?>&amp;action=new_property_site"  class="button"><?php _e('Add New Offices'); ?></a></p>
	<table class="widefat">		
		<thead>
			<tr>
			<th>Office ID</th>
			<th>Access Key</th>
			<th>Private Key</th>
			<th>Action</th>
			</tr>
		</thead>
			
		<?php		
		if(!empty($property_xml)):
		foreach($property_xml as $property): ?>
		<tr class="<?php $odd_class = (empty($odd_class))? 'alternate': '' ; echo $odd_class; ?>">
			<td><?php echo $property['office_id'];?></td>
			<td><?php echo $property['access_key'];?></td>
			<td><?php echo $property['private_key'];?></td>
			<td><a class="delete" onClick="return confirm('You are about to delete the this property xml.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $this->form_action; ?>&amp;task=delete_property_site&amp;id=<?php echo  $property['id']; ?>">Delete</a>
			<a href="<?php echo $this->form_action; ?>&amp;action=edit_property_site&amp;id=<?php echo $property['id']; ?>">Edit</a>	
			<input type="hidden" id="id" value="<?php echo $property['id'];?>"></td>
		</tr>
		<?php endforeach; endif; ?>	
	</table>	
</div>