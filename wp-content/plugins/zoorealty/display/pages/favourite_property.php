<?php
$condition=$_GET;
global $helper, $favourite;
if($favourite->fav_string() > 0){ $query = $favourite->fav_string(); }
else { return print '<p class="no_properties_error">We are sorry, no property listings were saved as your favourites.</p>'; }
$ids=explode(",",$query);
$condition['id']=$ids;

if(is_null($listings))$listings = $this->properties($condition);

$queryString = "?" .  preg_replace('/(&?order=)(&?limit=)([\w]+&?)/is', '', urldecode($_SERVER['QUERY_STRING']));
if($listings['results_count'] < 1)return print '<p class="no_properties_error">We are sorry, no property listings were found to match this criteria, please check back again soon or contact us with your requirements.</p>';

if($this->settings['general_settings']['search_results_display']=='thumb' || $this->settings['general_settings']['search_results_display']=='')$default='0';
if($this->settings['general_settings']['search_results_display']=='list')$default='1';
if($this->settings['general_settings']['search_results_display']=='map')$default='2';

$view=0;
if($this->settings['general_settings']['display_search_result_in_thumbnail_format']=='1')$view++;
if($this->settings['general_settings']['not_display_search_result_in_list_format']!='1')$view++;
if($this->settings['general_settings']['display_map_in_search_result']=='1')$view++;
 ?>
<script type="text/javascript">var siteurl="<?php echo get_option('siteurl'); ?>";</script>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>js/lightbox.js?v=0.03"></script>
<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php echo $this->pluginUrl.'css/lightbox.css'; ?>?v=0,01" />

<div id="search_results_container">
	<div id="search_results">
		<div class="link-back"><a href="javascript:javascript:history.go(-1)">Back</a></div>
		<div id="sorter_pagination">
			
			<p class="number_properties">
				There <?php echo ($listings['results_count'] == 1) ? 'is': 'are'; ?> <strong><?php echo $listings['results_count']; ?></strong>
				<?php echo "$types "; ?><?php echo ($listings['results_count'] == 1) ? 'property': 'properties'; ?> saved as your favourites.
			</p>
			<div class="sorter_search_quick">
				<select name="" onchange="window.location=this.value">
					<option value="#">Sort by:</option>
					 <option value="<?php echo $queryString . "&amp;order=price";?>"<?php if($this->params['order'] =='price' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='price') echo ' selected="selected"';?>>Price</option>
					<option value="<?php echo $queryString. "&amp;order=suburb";?>"<?php if($this->params['order'] =='suburb' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='suburb') echo ' selected="selected"';?>>Suburb</option>
					<option value="<?php echo $queryString. "&amp;order=created_at";?>"<?php if($this->params['order'] =='created_at' && empty($_GET['order'])) echo ' selected="selected"';if($_GET['order']=='created_at') echo ' selected="selected"';?>>Date Added</option>
				</select>		
				<?php if($this->settings['general_settings']['display_pdf_button_in_search_result']=='1'){ ?>
				<p class="pdf btn"><a href="javascript:void(0)" onclick="return window.open('<?php echo $this->pluginUrl."display/elements/search_brochure.php?fav=1"; ?>', 'Brochure', 'width=820, height=960,resizable=1,scrollbars=1'); return false;" title="Print Results" class="btn">Print Results</a></p>
				<?php } ?>
				<div class="clear"></div>
			</div>	
			
			<?php if($view>1){ 
			wp_print_scripts('jquery-ui-core');wp_print_scripts('jquery-ui-tabs');		
			
			if($this->settings['general_settings']['display_map_in_search_result']=='1'):
				foreach($listings as $property):  
					if(!is_array($property)) continue; 
					if(!empty($longitude) && !empty($latitude)) break; 
					if(!empty($property['latitude']) && !empty($property['longitude']) && empty($longitude) && empty($latitude)){
					$latitude=$property['latitude'];
					$longitude=$property['longitude'];
					}
				endforeach;
				if(!empty($latitude) && !empty($longitude))$display_map='1';else $display_map='0';
				if($display_map=='1'){
			?>
			<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php  echo $this->settings['general_settings']["google_maps_key"]; ?>" type="text/javascript"></script>	
			<?php } endif; ?>
			
			<input type="hidden" id="thumb_tab" value="0" >
			<input type="hidden" id="list_tab" value="1" >	
			<input type="hidden" id="map_tab" value="2" >

			<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#search_results").tabs({selected:<?php echo ($_COOKIE['active_tab']=='')?$default:$_COOKIE['active_tab'];?>});
				<?php if($this->settings['general_settings']['display_map_in_search_result']=='1'):?>
				jQuery("#map_tabs").click(function(){
					map.checkResize();
					map.returnToSavedPosition();
				});
				<?php endif; ?>
			});
			</script>
			
			<div id="search_results_tab">
				<ul class="shadetabs search_results_view_option">
					<li class="thumbnail_format" <?php if($this->settings['general_settings']['display_search_result_in_thumbnail_format']!='1') echo 'style="display:none !important;"';?> ><a href="#thumbnail_format" onClick="active_tab('thumb');" title="View Thumbnails Mode">Thumbnails</a></li>
					<li class="list_format" <?php if($this->settings['general_settings']['not_display_search_result_in_list_format']=='1') echo 'style="display:none !important;"';?>><a href="#list_format" onClick="active_tab('list');" title="View List Mode">List</a></li>
					<li class="map_search_result"  <?php if($display_map!='1') echo 'style="display:none !important;"';?>><a href="#map_search_result" id="map_tabs" onClick="active_tab('map');" title="View Map Mode">Map</a></li>
				</ul>
			</div>
			<?php } 
		
		echo $listings['pagination_above']; ?>

		</div> <!-- #sorter_pagination -->	
		<?php $this->element('listings', $listings); 

		if($display_map=='1'):?>
		<div id="map_search_result">
		<?php include(dirname(dirname(__FILE__)).'/elements/map_search_results.php'); ?>
		</div>
		<?php endif; 

		if(!empty($listings['pagination_below'])): ?>
		<div id='bottom_pagination'><?php echo $listings['pagination_below']; ?></div>
		<?php endif;?>
	</div>
</div>
<script>
<?php if($view>1){ ?>
function active_tab(tab){
	var index;
	if(tab=="thumb")index=document.getElementById('thumb_tab').value;
	else if(tab=="list")index=document.getElementById('list_tab').value;
	else if(tab=="map")index=document.getElementById('map_tab').value;
	var url="<?php echo $this->pluginUrl; ?>display/elements/active_tab.php?tab="+escape( index );
	jQuery.get(url);
}
<?php } ?>
</script>