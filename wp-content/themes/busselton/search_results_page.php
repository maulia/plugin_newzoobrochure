<?php
/*
Template Name: Search Results
*/
global $options;
foreach ($options as $value) {if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }}
get_header();
global $realty;?>

<div id="sidebar"><?php dynamic_sidebar('Tag Search'); ?></div>

<div id="content" class="narrowcolumn">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="page" id="page-<?php the_ID(); ?>">
		<h1 class="page-title"><?php if ( is_single() || is_page() ) { ?><?php $BodyTitle = get_post_meta($post->ID, 'BodyTitle', true);  if ($BodyTitle) { echo $BodyTitle;  } else { the_title(); } } ?></h1>
		<div class="entry"><?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?></div>
	</div>
	<?php endwhile; endif; ?>
</div>

<?php get_footer(); ?>