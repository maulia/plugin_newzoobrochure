<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
}
$menupages = explode(',',$main_menu);

global $wp_query;
if( empty($wp_query->post->post_parent) ) {
$parent = $wp_query->post->ID;
} else {
$parent = $wp_query->post->post_parent;
} if ( ($wp_query->is_posts_page) || ( is_home() || is_category() || is_archive() || is_search() || is_single() || is_date() ) ) { 
$parent = get_option('page_for_posts'); 
}

foreach ($menupages as $menupage) {
$redirect = get_post_meta($menupage, 'redirect_value', $single = true);
	if (empty($redirect)) { 
		global $wpdb;		 
		 $redirect = $wpdb->get_var("SELECT `post_parent` FROM $wpdb->posts WHERE `ID`= $menupage");		 
		 if($redirect == 0) { //Return from the recursion with the title of the absolute ancestor post.
			$redirect = $wpdb->get_var("SELECT `post_name` FROM $wpdb->posts WHERE `ID`= $menupage"); }
	}
$nav_text = get_post_meta($menupage, 'nav_text_value', $single = true);
if ($nav_text != '' ) {
	$nav_text = "<small>" . $nav_text . "</small>";
}
$children = wp_list_pages("title_li=&child_of=$menupage&echo=0");

//if (($children != '') && ($drop_down_menu != "")) { $ddarrow = '<span class="ddarrow">&nbsp;</span>'; $ddarrowitem = ' ddarrowitem'; } else { $ddarrow = ''; }

if ($parent == $menupage){ $parent_page = " current_page_ancestor current_page_parent"; } else { $parent_page = ""; }
echo '<li class="page_item page-item-'.$menupage . $parent_page.' page_item_top'.'"><a href="'.get_option('home').'/'.$redirect.'/" title="'.get_the_title($menupage).'">'.get_the_title($menupage).$nav_text."</a>\n";

$kids = explode('</li>',$children);
if ( ($drop_down_menu != "") && ( ($repeat_main_item != "") || (count($kids) > 1)) ) {
echo "<ul>\n";
if ($repeat_main_item != "") {
echo wp_list_pages("title_li=&include=$menupage&echo=0");
}
echo $children;
echo "</ul>\n";
}
echo "</li>\n";
}

?>