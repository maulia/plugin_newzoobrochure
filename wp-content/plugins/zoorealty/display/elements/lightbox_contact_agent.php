<?php 
require_once('../../../../../wp-load.php');
global $realty;
?>
<div class="lightbox-wrap">
<h2>Email Agent</h2>
<div id="return"></div>
<ol class="cf-ol">
	<li><label><span>First Name *</span></label>
	<input type="text" class="textbox" id="cfirst_name" name="first_name" value="<?php echo $_POST['first_name']; ?>" /></li>
	<li><label><span>Last Name *</span></label>
	<input type="text" class="textbox" id="clast_name" name="last_name" value="<?php echo $_POST['last_name']; ?>" /></li>	
	<li><label><span>Telephone</span></label>
	<input type="text" class="textbox" id="chome_phone" name="home_phone" value="<?php echo $_POST['home_phone']; ?>" /></li>
	<li><label><span>Mobile</span></label>
	<input type="text" class="textbox" id="cmobile_phone" name="mobile_phone" value="<?php echo $_POST['mobile_phone']; ?>" /></li>
	<li><label><span>Email  *</span></label>
	<input type="text" class="textbox" id="cemail" name="email" value="<?php echo $_POST['email']; ?>" /></li>
	<li class="contact_message"><label><span>Comments</span></label>
	<textarea name="comments" id="ccomments" cols="22" rows="3"><?php echo $_POST['comments']; ?></textarea></li>
	<li><label><span>Spam Code</span></label>
		<img id="captcha-image" src="<?php echo $realty->pluginUrl.'display/elements/CaptchaSecurityImages.php?width=100&height=40&characters=5'; ?>" alt="Anti-Spam Image" style="vertical-align:top;" /><span class="reload-captcha"><a href="javascript:captchas_image_reload('captcha-image')" class='btn' title="Reload Image">Reload Image</a></span>
			<br/><br/><label>Enter the above code</label><input type="text" name="securitycode" id="securitycode" size="20" />
	</li>
    <li><label>&nbsp;</label><a href="javascript:void(0)" onClick="send_enquiry()" class="btn">Submit</a><div class="clear"></div></li>
</ol>
</div>