<?php
//metadata: access_level=8
$colours = array('#e43ab7', '#857a82', '#0b090b', '#f68231', '#ba7b4f', '#c9ee8b', '#67941e', '#1e7394', '#8b1e94'
,'#ee3917', '#23ee17', '#1732ee');
$identifier = substr(pathinfo(__FILE__, PATHINFO_BASENAME), 0, -4); //The name of the file is used as unique identifier for options in the plugin.
$stickers = $helper->getTemplates($realty->path($realty->paths['stickers']));
if(isset($_POST['submit'])):
	if(empty($_POST['sticker']))$_POST['sticker']=array ();
	update_option('realty_featured_listings', $_POST['sticker']);
	$return = 'Settings Updated.';
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
$realty->element('add_tag_script');
$settings = get_option('realty_featured_listings');

?>
<div class="wrap">
<form name="" method="post" action="" id="elements">
<h2>Featured Listings</h2> 

<table class="widefat"> 
<tr class="alternate">
	<td>
	<label>Featured Sale IDs</label><br/>
	<?php realty_add_tag_box('sticker[featured_listings][sale][]', $settings['featured_listings']['sale'] ); ?>	</td>
</tr>
<tr>
	<td>
	<label>Featured Lease IDs</label>	<br/>
	<?php realty_add_tag_box('sticker[featured_listings][lease][]', $settings['featured_listings']['lease'] ); ?>	</td>
	</td>
</tr>
<tr class="alternate">
	<td>	
	<label>House of the Week ID</label><br/>
	<?php realty_add_tag_box('sticker[properties_of_the_week][house][]', $settings['properties_of_the_week']['house'] ); ?>	</td>
	</td>
</tr>
<tr>
	<td>
	<label>Apartment of the Week ID</label>	<br/>
		<?php realty_add_tag_box('sticker[properties_of_the_week][apartment][]', $settings['properties_of_the_week']['apartment'] ); ?>	</td>
	</td>
</tr>
<tr class="alternate">
	<td>
	<label>Rental of the Week ID</label><br/>
		<?php realty_add_tag_box('sticker[properties_of_the_week][rental][]', $settings['properties_of_the_week']['rental'] ); ?>	</td>
	</td>
</tr>
</table> 
 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->