<?php

/**********************************************************************************************************************************************
Start of Theme Options Page
***********************************************************************************************************************************************/
$functions_path = TEMPLATEPATH . '/functions/';
$themename = get_option('stylesheet');
$options = array (
	array(    
		"name" => "Logo",
		"type" => "open"),
	array(
		"name" => "Logo Image",
		"id" => "logo",
		"type" => "text",
		"std" => "logo_carbon.gif",
		"description" => "Image file name, with extension."),
	array(
		"name" => "Image Preview",
		"id" => "logo",
		"type" => "image",
		"std" => "logo.png"),
	array(
		"type" => "close"),

	array(    
		"name" => "Contact Information",
		"type" => "open"),
	array(
		"name" => "Address",
		"id" => "header_address",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "City",
		"id" => "header_city",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "State",
		"id" => "header_state",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Postal Code",
		"id" => "header_zip",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Phone",
		"id" => "header_phone",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Fax",
		"id" => "header_fax",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"name" => "Email",
		"id" => "header_email",
		"type" => "text",
		"std" => "",
		"description" => ""),
	array(
		"type" => "close"),

	array(    
		"name" => "Social Networking",
		"type" => "open"),
	array(    
		"name" => "Choose the button type.",
		"type" => "title"),
	array(
		"name" => "Type",
		"id" => "button_type",
		"std" => "circle",
		"type" => "radio",
		"options" => array("badge","circle","flower","flower2","house")),
	array(    
		"name" => "Enter links to your social networking sites.",
		"type" => "title"),
	array(
		"name" => "Facebook",
		"id" => "facebook",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "Twitter",
		"id" => "twitter",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "Digg",
		"id" => "digg",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "MySpace",
		"id" => "myspace",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "LinkedIn",
		"id" => "linkedin",
		"type" => "input",
		"std" => ""),
	array(
		"name" => "YouTube",
		"id" => "youtube",
		"type" => "input",
		"std" => ""),
	array(
		"type" => "close"),

);
require_once ($functions_path . 'admin-init.php');			
require_once ($functions_path . 'widget.php');			
require_once ($functions_path . 'post.php'); 		
require_once ($functions_path . 'custom-login.php'); 		
require_once ($functions_path . 'others.php');	
?>