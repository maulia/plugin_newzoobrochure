<tr>
	<td align="center" colspan="2" style="background:<?php echo $sm_heading_color; ?>; border-top:solid 1px #fff; color:#<?php echo $sm_heading_font_color; ?>">
		<h4 style="margin:10px 0; padding:0; font-size:16px; "><?php echo $site_alerts[$alert]; ?></h4>
	</td>
</tr>
<?php
$count = 0;
foreach($opendates as $date=>$times):
	$count++; /* Array of dates */  ?>
<tr>
<td colspan="2">
<table width="100%" cellspacing="0" cellpadding="0" summary="Open Times for <?php echo $key; ?>" style="font-size:12px;">
	<tr>
		<th colspan="4" style="font-weight: bold; text-transform: uppercase; padding: 10px 0;text-align:left; border-bottom:dotted 1px #ddd; border-top:dotted 1px #ddd;"><?php echo date('l, jS F Y', strtotime($date)); ?></th>
	</tr>
	<?php
		foreach($times as $property):/* Array of properties within each time */
		$opentimes = $realty->opentimes($property['id']," AND opentimes.date='$date'"); 
		//echo $date,$property['id'];?>
	<tr style="<?php echo ($odd_class = ($odd_class=='background-color: #fff;') ? 'background: #f6f5f1;':'background-color: #fff;'); ?>">
		<td style="width:150px; padding:5px 0; vertical-align: top;"><?php foreach($opentimes as $opentime): ?>
			<?php echo date('g:ia', strtotime($opentime['start_time'])) . ' - ' .date('g:ia', strtotime($opentime['end_time'])) . "<br />";  
		endforeach; ?>
	</td>
		<td style="width:150px; padding:5px 0; vertical-align: top;"><?php echo $property['suburb']; ?></td>
		<td style="width:300px; padding:5px 0; vertical-align: top;"><?php echo $property['street_address']; ?></td>	
		<td style="padding:5px 0; vertical-align: top;"><a title="View Property" href="<?php echo $property['url']; ?>" style="color:#8e855e; text-decoration:none;">View Property <span style="font-family:Georgia, 'Times New Roman', Times, serif">&#187;</span></a></td>
	</tr>
<?php 
	
endforeach; ?>
</table><!--date_container_-->
</td>
</tr>
<?php endforeach; // properties_date ?>
