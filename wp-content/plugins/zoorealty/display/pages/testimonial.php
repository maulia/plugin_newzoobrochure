<?php 
global $helper, $wpdb;

$user_id=$this->params['user_id'];
if(empty($user_id)){
	$office_id = implode(',', $this->settings['general_settings']['office_id']);
	$user_id="select id from users where office_id in ($office_id)";			
}
$users=$wpdb->get_col("SELECT distinct user_id FROM testimonials WHERE `user_id`in (".esc_sql($user_id).")");
foreach($users as $user_id){
	$user_name=$wpdb->get_var("SELECT CONCAT(firstname, ' ', lastname) as name FROM users WHERE `id` in (".intval($user_id).")");
	echo "<h3>$user_name"."'s Testimonials</h3>";
	$testimonials=$wpdb->get_results("SELECT `content`, `user_name` FROM testimonials WHERE `user_id` in (".intval($user_id).")", ARRAY_A);
	foreach($testimonials as $testimonial){
		echo "<p>".nl2br($testimonial['content'])."<br>- ".$testimonial['user_name']."</p><br/>";
	}
}
?>