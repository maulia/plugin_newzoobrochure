function getTop() {
	// get the top of the content
	var top = $('#scroll-content').css('top');
	return trimPx(top);
}

function getHeight(id) {
	// get the height, including padding
	var height = $(id).height();
	var paddingTop = trimPx($(id).css("padding-top"));
	var paddingBottom = trimPx($(id).css("padding-bottom"));

	return height + paddingTop + paddingBottom;
}

function trimPx(value) {
	// remove "px" from values
	var pos = value.indexOf("px");
	if (pos != 0)
		return parseInt(value.substring(0, pos));
	else
		return 0;
}

var container;
var content;
var hidden;	// # of pixels hidden by the container

function setScrollerDimensions() {
	container = getHeight("#scroll-container");
	content = getHeight("#scroll-content");
	hidden = content - container;
}

function resetScroller() {
	setScrollerDimensions();
	$('#scroll-content').css('top', 0);
}

$(document).ready(function() {

	setScrollerDimensions();

	$('#scroll-controls a.up-arrow').click(function() {
		return false;
	});

	$('#scroll-controls a.down-arrow').click(function() {
		return false;
	});

	$('#scroll-controls a.down-arrow').hover(
		function() {
			if (hidden > 0) {
				var current = getTop();
				$('#scroll-content').animate({ top: -hidden }, Math.abs(current - hidden) * 5);
			}
		},
		function() {
			$('#scroll-content').stop();
		}
	);

	$('#scroll-controls a.up-arrow').hover(
		function() {
			if (hidden > 0) {
				var current = getTop();
				$('#scroll-content').animate({ top: "0" }, Math.abs(current) * 5);
			}
		},
		function() {
			$('#scroll-content').stop();
		}
	);
});