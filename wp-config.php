<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'andriani');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.z]hHFrsjokn_NUDngV#^+-{C~jvAlHU9Z#{x?9}Jb`0|c::x|&=12}~TQ[KZ/t+');
define('SECURE_AUTH_KEY',  '.2h_gP_oGQ0$DpU*uq#?p%I&PuR=g#M.iz{>@ncQe]{_v-RUM=M>Wm%aY7%E6`v-');
define('LOGGED_IN_KEY',    '@L/@5$ea{C|!8HY9X4dY=}9xrNTnvKF.)Rg`hcth5URbySP3rn<QyG9=oKvi7FYs');
define('NONCE_KEY',        '$?Q;sI|c4W.F{`0l245ba@-d#:_5%l^_|nhk|w{^4FxWC1dHCoKhBfEv+F_c))`+');
define('AUTH_SALT',        'Ax))i$tudgDO%S&:G9?^sJ7mWu(+NQ5>I:pk/{F@*2`2W]E8/3 gg@%B_xKWe@|0');
define('SECURE_AUTH_SALT', 'C%FI^^,&B*E|@Y,+LnEx1vOQp^&OJmYXG;h;sjB].]-7PW ++(.e2hXU-C(h&ViV');
define('LOGGED_IN_SALT',   '[a?24+RTX)-r)A1OkNjT$Xhk-@+M&+N=|oDT:JG(1DPGOf#v1U<-IbXrEd.-C1[T');
define('NONCE_SALT',       'RNG^B{-7I<D6F<Q+@iCf9{nMk<%!#8l@O^wZ4Lf#j/|2-~Y4~:u(7bjhb1%<{662');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

// define('FS_METHOD', 'direct');