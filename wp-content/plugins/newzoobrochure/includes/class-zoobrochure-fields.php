<?php
defined('ABSPATH') or die('No script kiddies please!');

/**
 * Generate field for admin page
 * 
 * @author Jony <jony@agentpoint.com.au>
 * @package zoobrochure
 * @subpackage zoobrochure_fields
 * @since 2.0.0
 */
class zoobrochure_fields {

    public $args = [];
    public $option_name;
    public $option = [];

    /**
     * Initialize group name and setting name
     * 
     * @since 2.0.0
     */
    public function __construct($settings = []) {
        $this->set_settings($settings);
        $this->option = get_option($this->option_name, []);
    }

    /**
     * Setter for argument
     * 
     * @since 2.0.0
     * @param array $args
     * @return void
     */
    public function set_args($args = []) {
        if (!empty($args)) {
            $this->args = wp_parse_args($args, ['type' => null, 'name' => null, 'std' => null]);
        }
    }

    /**
     * Setter for setting zoobrochure_field it self
     * 
     * @since 2.0.0
     * @param array $settings
     * @return void
     */
    public function set_settings($settings = []) {
        foreach ($settings as $proprety => $setting) {
            $this->{$proprety} = $setting;
        }
    }

    /**
     * Getter for name of option
     * 
     * @since 2.0.0
     * @return string
     */
    public function get_option_name() {
        return esc_attr($this->option_name);
    }

    /**
     * Getter for name of field, we format field name using option name ($this->option_name)
     * 
     * @since 2.0.0
     * @param string $name
     * @return string
     */
    public function get_field_name($name) {
        return esc_attr(sprintf('%s[%s]', $this->option_name, $name));
    }

    /**
     * Getter for value of field, we read value of field by check existng key on $this->option
     * 
     * @since 2.0.0
     * @param string $name
     * @return mixed
     */
    public function get_field_value($name) {
        return isset($this->option[$name]) ? $this->option[$name] : $this->args['std'];
    }

    public function get_field_args() {
        return $this->args['args'];
    }

    /**
     * Create field Upload Media
     * 
     * @since 2.0.0
     * @return string HTML markup
     */
    private function field_type_upload() {
        ob_start();

        $value = $this->get_field_value($this->args['name']);
        $name = $this->get_field_name($this->args['name']);

        if (!empty($value)) {
            ?>
            <img style="max-width:300px;" src="<?php echo $value; ?>" alt="<?php echo $name; ?>" title="<?php echo $name; ?>">
            <div class="clear"></div>
            <?php
        }
        ?>

        <input type="text" name="<?php echo $this->get_field_name($this->args['name']); ?>" value="<?php echo $value; ?>" class="regular-text" />
        <button type="button" name="trigger" value="1" class="button media-upload-selector" data-selector-target="<?php echo $this->get_field_name($this->args['name']); ?>">Upload</button>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    /**
     * Create field Color Picker
     * 
     * @since 2.0.0
     * @return string HTML markup
     */
    private function field_type_color() {
        ob_start();
        ?>
        <input type="text" name="<?php echo $this->get_field_name($this->args['name']); ?>" value="<?php echo $this->get_field_value($this->args['name']); ?>" class="color-picker-selector" />
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    /**
     * Create field Template Chooser
     * 
     * @since 2.0.0
     * @return string HTML markup
     */
    private function field_type_template() {
        ob_start();
        $template = $this->args['templates'];
        $field_name = $this->get_option_name();
        $field_value = get_option($field_name);
        foreach ($template as $name => $option) {
            ?>
            <input type="radio" name="<?php echo $field_name; ?>" value="<?php echo $name; ?>" <?php checked($name, $field_value); ?> />
            <label><?php echo $option['name']; ?></label>
            <?php
            if (!empty($option['screenshot_url'])) {
                ?>
                <a href="<?php echo $option['sample_url']; ?>" target="_blank">
                    <img style="max-width:150px;" src="<?php echo $option['screenshot_url']; ?>" alt="<?php echo $option['name']; ?>" title="<?php echo $option['name']; ?>">
                </a>
                <?php
            }
        }
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    /**
     * Create field Checkbox
     * 
     * @since 2.0.0
     * @return string HTML markup
     */
    private function field_type_checkbox() {
        ob_start();
        $value = $this->get_field_value($this->args['name']);
        ?>
        <input type="checkbox" name="<?php echo $this->get_field_name($this->args['name']); ?>" value="1" <?php echo checked(1, $value); ?> />
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    private function field_type_color_preview() {
        global $wpdb;
        $property_id = $wpdb->get_var('select id from properties where status = 1 order by id desc limit 1');
        $url = site_url(sprintf('brochure-%d.pdf?zoobrochure_preview=1&', $property_id));
        ob_start();
        ?>
        <button type="button" class="button" name="<?php echo $this->get_field_name($this->args['name']); ?>" data-preview-color-trigger="1" data-preview-args="<?php echo esc_attr(json_encode($this->get_field_args())); ?>" data-preview-option-name="<?php echo esc_attr($this->get_option_name()); ?>" data-preview-url="<?php echo esc_url($url); ?>">Preview Color Template</button>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    /**
     * Main method that use to render each field
     * 
     * @since 2.0.0
     * @return boolean
     */
    public function render() {
        $method = sprintf('field_type_%s', $this->args['type']);

        if (!method_exists($this, $method)) {
            echo 'Field type ' . $this->args['type'] . ' doesn\'t exist';
            return false;
        }


        echo $this->$method();
        if (!empty($this->args['help'])) {
            echo sprintf('<p class="description">%s</p>', $this->args['help']);
        }
        return true;
    }

}
