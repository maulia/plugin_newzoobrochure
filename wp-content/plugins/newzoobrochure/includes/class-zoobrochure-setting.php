<?php

defined('ABSPATH') or die('No script kiddies please!');

/**
 * Setting zoobrochure for admin page
 * 
 * @author Jony <jony@agentpoint.com.au>
 * @package zoobrochure
 * @subpackage zoobrochure_setting
 * @since 2.0.0
 */
class zoobrochure_setting {

    private static $instance = null;
    public $page;
    public $section;
    public $option_group;
    public $option_name;
    public $setting;
    public $templates = [];

    /**
     * Initialize main setting
     * 
     * @since 2.0.0
     */
    public function __construct() {
        // Setup settings
        $this->set_option_group('zoobrochure_setting');
        $this->set_option_name('zoobrochure_setting');

        // Call the setting after plugins loaded to ensure can be add_filter properly 
        add_action('after_setup_theme', array($this, 'set_template_list'));
        add_action('after_setup_theme', array($this, 'set_settings'));

        // Do administration options
        add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('admin_init', array($this, 'register_setting'));
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    /**
     * Main zoobrochure_setting Instance.
     *
     * Ensures only one instance of zoobrochure_setting is loaded or can be loaded.
     *
     * @since 2.0.0
     * @static
     * @return object zoobrochure_setting Main instance
     */
    public static function instance() {
        if (null == self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Produce list of brochure template
     * 
     * @since 2.0.0
     * @param boolean $name_only Set to be TRUE if only want the name, default is false
     * @return array Template list
     */
    public function get_template_list($name_only = false) {
        if (true == $name_only) {
            $templates = [];
            foreach ((array) $this->templates as $name => $option) {
                $templates[] = $name;
            }
            return $templates;
        }
        return $this->templates;
    }

    /**
     * Generate current zoobrochure setting/options
     * 
     * @since 2.0.0
     * @param string $name
     * @return array|boolean Will return setting as array and false if failure
     */
    public function get_settings($name = '') {
        if (!empty($name)) {
            return isset($this->setting[$name]) ? $this->setting[$name] : false;
        }
        return $this->setting;
    }

    /**
     * Produce every single field name that related with current template
     * 
     * @since 2.0.0
     * @param string $name
     * @return boolean|array
     */
    public function get_default_formated_setting($name = '') {
        if (empty($name) || !isset($this->setting[$name])) {
            return false;
        }
        $setting = [];
        foreach ($this->setting[$name] as $section) {
            foreach ($section['fields'] as $field) {
                $setting[$field['name']] = null;
            }
        }
        return $setting;
    }

    /**
     * Add javascript to admin page, such as color picker and setting.js that use to trigger input element
     * 
     * @since 2.0.0
     * @return void
     */
    public function enqueue_scripts() {
        wp_enqueue_style('wp-color-picker');

        wp_enqueue_script('jquery');
        wp_enqueue_script('wp-color-picker');
        wp_enqueue_media();

        wp_register_script('zoobrochure-setting', plugins_url('assets/js/setting.js', dirname(__FILE__)), array('jquery'), null);
        wp_enqueue_script('zoobrochure-setting');
    }

    /**
     * Create admin menu page for zoobrochure setting
     * 
     * @since 2.0.0
     * @return void
     */
    public function admin_menu() {
        add_menu_page(__('Zoo Brochure'), __('Zoo Brochure'), 'manage_options', 'zoobrochure-setting', array($this, 'main_setting_page'));
        add_submenu_page('zoobrochure-setting', __('Templates'), __('Templates'), 'manage_options', 'zoobrochure-setting', array($this, 'main_setting_page'));

        // Get active template 
        $template = get_option('zoobrochure_current_template', false);

        // Only show setting on current active template
        if ($template) {
            $list = $this->get_template_list();
            $option = isset($list[$template]) ? $list[$template] : ['name' => 'Unknown'];
            add_submenu_page('zoobrochure-setting', __('Settings (Template ' . $option['name'] . ')'), __('Settings'), 'manage_options', 'zoobrochure-setting-' . $template, array($this, 'template_setting_page'));
        }

        // Disable Load All setting
        // foreach ($this->get_template_list() as $name => $option) {
        //    add_submenu_page('zoobrochure-setting', __('Zoo Brochure (Template ' . $option['name'] . ')'), __('Template ' . $option['name']), 'manage_options', 'zoobrochure-setting-' . $name, array($this, 'template_setting_page'));
        // }
    }

    /**
     * Register setting for every section and field
     * 
     * @since 2.0.0
     * @return void
     */
    public function register_setting() {
        $templates = $this->get_template_list();

        // Register option name for Main setting
        $option_name = 'zoobrochure_current_template';
        register_setting('zoobrochure-setting-template', $option_name);
        $page = 'zoobrochure-setting';
        $section = 'zoobrochure-general';
        add_settings_section($section, __('Available Template'), function() {
            echo 'Please choose one template';
        }, $page);
        add_settings_field(1, __('Template'), array($this, 'callback_field'), $page, $section, array('type' => 'template', 'name' => 'template', 'templates' => $this->get_template_list(), 'option_name' => $option_name));


        // Register option name for every single template
        foreach ($templates as $name => $option) {
            register_setting(sprintf('%s_%s', $this->option_group, $name), sprintf('%s_%s', $this->option_name, $name));
        }
        $settings = $this->get_settings();
        foreach ($settings as $name => $sections) {
            $this->set_page('zoobrochure-setting-' . $name);
            foreach ($sections as $section_id => $section) {

                // Section Template
                $this->set_section($name . '_' . $section_id);
                add_settings_section($this->section, $section['label'], array($this, 'callback_section'), $this->page);

                // Set every single field
                foreach ($section['fields'] as $id => $field) {
                    $field['option_name'] = sprintf('%s_%s', $this->option_name, $name);
                    add_settings_field($id, $field['label'], array($this, 'callback_field'), $this->page, $this->section, $field);
                }
            }
        }
    }

    /**
     * Display admin setting page
     * 
     * @since 2.0.0
     * @return void
     */
    public function main_setting_page() {
        echo '<div class="wrap">';
        echo sprintf('<h1>%s</h1>', esc_html(get_admin_page_title()));
        settings_errors();
        echo '<form action="options.php" method="post">';
        settings_fields('zoobrochure-setting-template');
        do_settings_sections('zoobrochure-setting');
        submit_button();
        echo '</form>';
        echo '</div>';
    }

    /**
     * Display admin setting page
     * 
     * @since 2.0.0
     * @return void
     */
    public function template_setting_page() {
        echo '<div class="wrap">';
        echo sprintf('<h1>%s</h1>', esc_html(get_admin_page_title()));
        settings_errors();
        echo '<form action="options.php" method="post">';
        $screen_id = get_current_screen()->id;
        $settings = $this->get_settings();
        foreach ($settings as $name => $sections) {
            if ('zoo-brochure_page_zoobrochure-setting-' . $name == $screen_id) {
                settings_fields(sprintf('%s_%s', $this->option_group, $name));
                echo '<input type="hidden" name="zoobrochure_setting_' . $name . '[current_template]" value="' . $name . '">';
                do_settings_sections('zoobrochure-setting-' . $name);
            }
        }
        submit_button();
        echo '</form>';
        echo '</div>';
    }

    /**
     * Maping inputed option to the correct format of setting
     * 
     * @since 2.0.0
     * @return array
     */
    public function format_setting($settings) {
        $o_settings = [];
        foreach ($settings as $name => $sections) {
            $o_sections = [];
            foreach ($sections as $section) {
                $o_section = wp_parse_args($section, array('label' => null, 'help' => null, 'fields' => []));
                $fields = [];
                foreach ($section['fields'] as $field) {
                    $fields[] = wp_parse_args($field, array('name' => null, 'type' => null, 'label' => null, 'std' => null, 'help' => null, 'option_name' => null, 'args' => []));
                }
                $o_section['fields'] = $fields;
                $o_sections[] = $o_section;
            }
            $o_settings[$name] = $o_sections;
        }
        return $o_settings;
    }

    /**
     * Setter for page name
     * 
     * @since 2.0.0
     * @param string $name
     * @return void
     */
    private function set_page($name) {
        $this->page = $name;
    }

    /**
     * Setter for settings
     * 
     * @since 2.0.0
     * @param string $name
     * @return void
     */
    public function set_settings() {
        $settings = [];
        foreach ($this->get_template_list() as $name => $option) {
            $settings[$name] = apply_filters('zoobrochure_settings_' . $name, zoobrochure_default_settings());
        }
        $this->setting = $this->format_setting($settings);
    }

    /**
     * Setter for section name
     * 
     * @since 2.0.0
     * @param string $name
     * @return void
     */
    private function set_section($name) {
        $this->section = $name;
    }

    /**
     * Setter for option name
     * 
     * @since 2.0.0
     * @param string $name
     * @return void
     */
    private function set_option_name($name) {
        $this->option_name = $name;
    }

    /**
     * Setter for option group name
     * 
     * @since 2.0.0
     * @param string $name
     * @return void
     */
    private function set_option_group($name) {
        $this->option_group = $name;
    }

    /**
     * Setter for template list
     * 
     * @since 2.0.0
     * @return void
     */
    public function set_template_list() {
        $templates = apply_filters('zoobrochure_template_register', []);
        $template_list = [];
        foreach ($templates as $name => $option) {
            $default = [
                'name' => null,
                'screenshot_url' => plugins_url('assets/images/noimage.png', dirname(__FILE__)),
                'sample_url' => null
            ];
            $template_list[$name] = wp_parse_args($option, $default);
        }
        $this->templates = $template_list;
    }

    /**
     * Call method that used to render form
     * 
     * @since 2.0.0
     * @param array $args
     * @return void
     */
    public function callback_field($args) {
        $field = new zoobrochure_fields(array('option_name' => $args['option_name']));
        $field->set_args($args);
        $field->render();
    }

    /**
     * Used to render section text help
     * 
     * @since 2.0.0
     * @param array $args
     * @return void
     */
    public function callback_section($args) {
        list($name, $section_id) = explode('_', $args['id']);
        echo $this->setting[$name][$section_id]['help'];
    }

}
