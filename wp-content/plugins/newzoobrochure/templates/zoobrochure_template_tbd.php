<?php

defined('ABSPATH') or die('No script kiddies please!');

class zoobrochure_template_tbd extends zoobrochure_template {

    public function render() {
        global $wpdb;

        $setting = $this->setting;
        $property = $this->property;

        // Add page
        $this->pdf->SetAutoPageBreak(True, 1);
        $this->pdf->AliasNbPages();
        $this->pdf->AddPage();

        $this->pdf->SetFillColor($setting['bg_color'][0], $setting['bg_color'][1], $setting['bg_color'][2]);
        $this->pdf->Rect(0, 0, $this->pdf->GetPageWidth(), 300, 'F');

        $this->pdf->AddFont('RaineHorne-Light', '', 'rainehornelight-webfont.php');
        $this->pdf->AddFont('RaineHorne-Regular', '', 'rainehorneregular-webfont.php');

        /* Font color */
        if ($setting['block_color'][0] != '255' && $setting['block_color'][1] != '255' && $setting['block_color'][2] != '255') {
            $this->pdf->SetTextColor(255, 255, 255);
        } else {
            $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        }

        /* image */
        $y = $this->pdf->getY();
        $property_photos = $property['photos'];
        if (!empty($property_photos)) {
            $photos = array();
            $i = 0;
            foreach ($property_photos as $photo) {
                $size = ( $i == 0 ) ? 'large' : 'medium';
                $photos[$i] = array($photo[$size], $photo[$size . '_width'], $photo[$size . '_heigth']);
                $i++;
            }

            if (!empty($photos[0][0])) {  // Main image; The big one		
                list($src, $width, $height) = $photos[0][0];
                if ($height > $width) {
                    $this->pdf->Image($photos[0][0], 0, 0, 210, 179);
                    $y = $y + 179;
                } else {
                    $this->pdf->Image($photos[0][0], 0, 0, 210, 140);
                    $y = $y + 140;
                }
            }

            $y_image = $y - 7;
            if (!empty($photos[1][0])) {
                $this->pdf->Image($photos[1][0], 0, $y_image, 68.5, 45);
                $y = $y + 44;
            }
            if (!empty($photos[2][0])) {
                $this->pdf->Image($photos[2][0], 70.6, $y_image, 68.5, 45);
            }
            if (!empty($photos[3][0])) {
                $this->pdf->Image($photos[3][0], 141.5, $y_image, 68.5, 45);
            }
        }

        /* beth, bath, car */
        $bedrooms = $property['bedrooms'];
        $bathrooms = $property['bathrooms'];
        $carspaces = $property['carspaces'];
        if (!empty($bedrooms || $bathrooms || $carspaces)) {

            $this->pdf->SetFont('RaineHorne-Regular', '', 14);
            $this->pdf->SetTextColor(66, 69, 74);
            $this->pdf->SetFillColor(0, 0, 0);

            $beds = $property['bedrooms'];
            $baths = $property['bathrooms'];
            $carspaces = $property['carspaces'];

            $bed_icon = $setting['bed_icon'];
            $bath_icon = $setting['bath_icon'];
            $car_icon = $setting['car_icon'];

            if ($beds) {
                $this->pdf->SetXY(70, $y);
                $this->pdf->MultiCell(100, 2, $beds, 0, 'L');
                $this->pdf->Image($bed_icon, 77.5, $y - 2, 0, 0, '', '', array('max_height' => 7));
            }
            if ($baths) {
                $this->pdf->SetXY(95, $y);
                $this->pdf->MultiCell(100, 2, $baths, 0, 'L');
                $this->pdf->Image($bath_icon, 102.5, $y - 2, 0, 0, '', '', array('max_height' => 7));
            }
            if ($carspaces) {
                $this->pdf->SetXY(120, $y);
                $this->pdf->MultiCell(100, 2, $carspaces, 0, 'L');
                $this->pdf->Image($car_icon, 127.5, $y - 2, 0, 0, '', '', array('max_height' => 7));
            }
            $y = $y + 10;
        }

        /* company info */
        $this->pdf->SetTextColor($setting['headline_color'][0], $setting['headline_color'][1], $setting['headline_color'][2]);
        $this->pdf->SetFont('RaineHorne-Regular', '', 14);
        $this->pdf->SetXY(9, $y);
        $this->pdf->MultiCell(63, 2, $property['office']['name'], 0, 'L');
        $this->pdf->SetXY(9, $y + 6);
        $this->pdf->MultiCell(63, 2, $property['office']['phone'], 0, 'L');

        /* detail */
        $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        $details = array('user' => 'Contact', 'property_type' => 'Type', 'land_size' => 'Land');

        foreach ($details as $key => $values) {
            if ($this->pdf->getY() >= 265)
                break;
            if (empty($property[$key]))
                continue;
            switch ($key) {
                case 'user';
                    $detail = $property['user'][0]['name'] . "\n";
                    if (!empty($property['user'][0]['mobile'])) {
                        $detail .= "" . $property['user'][0]['mobile'] . "\n";
                        $i++;
                    }
                    if (!empty($property['user'][1]['name'])) {
                        $detail .= $property['user'][1]['name'] . "\n";
                        $i++;
                    }
                    if (!empty($property['user'][1]['mobile'])) {
                        $detail .= "" . $property['user'][1]['mobile'];
                        $i++;
                    }
                    $this->print_details($values, $detail, $this->pdf->getY() + 8);
                    break;
                case 'land_size';
                    $this->print_details($values, $property[$key] . $property['land_area_metric'], $this->pdf->getY() + 8);
                    break;
                default:
                    $this->print_details($values, $property[$key], $this->pdf->getY() + 8);
                    break;
            }
        }

        /* address */
        $this->pdf->SetTextColor($setting['headline_color'][0], $setting['headline_color'][1], $setting['headline_color'][2]);
        $this->pdf->SetFont('RaineHorne-Regular', '', 14);
        $this->pdf->SetXY(70, $y);
        $street_address = $property['street_address'];
        $suburb = $property['suburb'];
        $state = $property['state'];
        $this->pdf->MultiCell(100, 2, $street_address . " " . $suburb . ", " . $state, 0, 'L');

        /* headline */
        $this->pdf->SetTextColor($setting['headline_color'][0], $setting['headline_color'][1], $setting['headline_color'][2]);
        $this->pdf->SetFont('RaineHorne-Regular', '', 14);
        $this->pdf->SetXY(70, $y + 5);
        $headline = $property['headline'];
        $head = str_replace("\t", " ", $headline);
        $head = preg_replace("[\n\r]", "\n", $headline);
        $headline_string = ceil(strlen($head));
        if ($headline_string <= 80) {
            $this->pdf->MultiCell(95, 4, $head, 0, 'J');
        } else {
            $newtext = wordwrap(substr($head, 0, 80), 40);
            $this->pdf->MultiCell(95, 4, $newtext, 0, 'J');
        }

        $y = $this->pdf->getY();
        $y_content = $y + 3;
        /* description */
        $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        $this->pdf->SetFont('RaineHorne-Regular', '', 9);
        $this->pdf->SetXY(70, $y_content);
        $description = $property['description'];
        $desc = str_replace("\t", " ", $description);
        $desc = preg_replace("[\n\r]", "\n", $desc);
        $description_row = ceil(strlen($desc) / 48);
        $desc_rows = explode("\n", $desc);
        $display_qrcode = $setting['qrcode'];
        foreach ($desc_rows as $key => $value) {
            $y = $this->pdf->getY();
            $this->pdf->SetXY(70, $y);
            $available_row = (260 - $y) / 4;
            $print_row = ceil(strlen($value) / 48);
            if ($available_row > $print_row) {
                if (!empty($display_qrcode)) {
                    $this->pdf->MultiCell(105, 4, $value, 0, 'J');
                } else {
                    $this->pdf->MultiCell(125, 4, $value, 0, 'J');
                }
            } else {
                $max_desc = $available_row * 85;
                $get_value = strlen($value);
                if ($get_value > $max_desc) {
                    $desc_cut = substr($value, 0, $max_desc);
                    if (!empty($display_qrcode)) {
                        $this->pdf->MultiCell(105, 4, $desc_cut, 0, 'J');
                    } else {
                        $this->pdf->MultiCell(125, 4, $desc_cut, 0, 'J');
                    }
                    break;
                }
            }
        }

        /* qrcode */
        $y = $this->pdf->getY();
        if (!empty($display_qrcode)) {
            $this->pdf->Image($this->pdf->GetQRCode($property['url']), 178, $y_content, 25, 25, 'PNG');
        }

        $this->pdf->SetFillColor($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
        $setting['block_color'] = array($setting['block_color'][0], $setting['block_color'][1], $setting['block_color'][2]);
        // set alpha to semi-transparency
        $this->pdf->SetAlpha(0.75);
        $this->pdf->RoundedRect(0, 275, 210, 30, 150, '1000', "F", null, $setting['block_color']);
        $this->pdf->RoundedRect(35, 275, 220, 30, 115, '0001', "F", null, $setting['block_color']);

        $this->pdf->Image($setting['logo'], 10, 278, 40, 0, '', '', array('max_width' => 40));

        $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
        $this->pdf->SetFont('RaineHorne-Regular', '', 11);
        $this->pdf->SetXY(135, 283);
        $url = strtolower(str_replace(array('http://', 'https://'), array('', ''), get_option('siteurl')));
        $this->pdf->Cell(100, 9, $url, 0, 0, 'L', false, $url);

        /* florplan */
        $floorplans = array();
        if (!empty($property['floorplans'])) {
            $i = 0;
            foreach ($property['floorplans'] as $floorplan) {
                $floorplans[$i] = $floorplan['large'];
                $i++;
            }
        }
        if (!empty($floorplans[0])) {
            $this->pdf->Floorplan($floorplans[0], 10, 20, 180, true);

            /* address */
            if (!empty($street_address || $suburb || $state)) {
                $this->pdf->SetTextColor($setting['font_color'][0], $setting['font_color'][1], $setting['font_color'][2]);
                $this->pdf->SetFont('RaineHorne-Regular', '', 14);
                $this->pdf->SetXY(20, $this->pdf->GetPaperHeight() - 50);
                $this->pdf->MultiCell(100, 2, $street_address . " " . $suburb . ", " . $state, 0, 'L');
            }
            $this->pdf->Image($setting['logo'], 140, $this->pdf->GetPaperHeight() - 55, 40, 0, '', '', array('max_width' => 40));
            $this->pdf->SetFont('Helvetica', '', 7);
            $this->pdf->SetXY(20, $this->pdf->GetPaperHeight() - 40);
            $this->pdf->MultiCell(100, 2, "Plans shown are only indicative of layout. Dimensions are approximate.", 0, 'L');
        }
    }

    function print_details($title, $value, $y) {
        $this->pdf->SetFont('RaineHorne-Regular', '', 10);
        $this->pdf->SetXY(9, $y);
        $this->pdf->MultiCell(80, 4, $title . " ", 0, 'L');
        if (!empty($value)) {
            $this->pdf->SetFont('RaineHorne-Light', '', 10);
            $this->pdf->SetXY(33, $y);
            $this->pdf->MultiCell(36, 4, $value, 0, 'L');
        }
    }

}
