<?php
global $options;
foreach ($options as $value) {
    if (get_option($value['id']) === FALSE) {
        $$value['id'] = $value['std'];
    } else {
        $$value['id'] = get_option($value['id']);
    }
}
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

    <head profile="http://gmpg.org/xfn/11">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53157558-1', 'auto');
  ga('send', 'pageview');

</script>

        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="<?php bloginfo('name'); ?>" />
        <meta name="copyright" content="<?php bloginfo('name'); ?>" />
        <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />

        <title><?php //bloginfo('name'); ?> <?php if (is_single()) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
         <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/media.css" />
        <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('stylesheet_url'); ?>?v=1.00" />
        <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie-only.css" />
        <![endif]-->
        <link rel="shortcut icon" type="image/ico" href="<?php echo bloginfo('template_url'); ?>/favicon.ico?v=1.01" />
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

        <?php if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { ?>
            <!-- IE < 9. Oswald replacement-->
            <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_directory'); ?>/css/ie.css?v=0.01" />
            <?php } ?>

            <?php
            wp_deregister_script('l10n');
            wp_print_scripts('jquery');
            ?>

            <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>

            <?php if (is_page('home')) { ?>
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.1.3.js"></script>
                <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.cycle.all.min.js"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('.slideshow').after('<div id="slide-paging">').cycle({
                            fx: 'fade', timeout: 5000, delay: 3000, pager: '#slide-paging'
                        });

                         jQuery('.agent_slideshow').after('<div id="slide-paging">').cycle({
                            fx: 'fade', timeout: 3000, delay: 2000, pager: '#slide-paging'
                        });
                    });
                </script>
            <?php } ?>

            <?php wp_head(); ?>
	<?php /*
            
	*/?>
    </head>

    <body id="<?php body_css_id(); ?>" <?php body_class(); ?>>

        <div class="wrap">

            <div id="header">

                <div id="headerwrap">
                        <div class="subtitle"><?php //if(is_page('home')) { echo"<h1>";bloginfo('name');echo "</h1>"; } ?></div>
                        <a href="<?php echo get_option('home'); ?>/" title="<?php bloginfo('name'); ?> <?php bloginfo('description'); ?>">
                               <h1><div class="logo">
                            <img src="<?php bloginfo('url') ?>/wp-content/uploads/homepage_images/<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" />
                         </div></h1>
                        </a>
                   

                    <?php
                    /*
                      <div class="site-description">
                      <p class="tagline"><?php
                      if (!empty($header_office)) {
                      echo $header_office;
                      } else {
                      bloginfo('description');
                      }
                      ?></p>
                      <p class="address">
                      <span><?php echo $header_address; ?>, </span>
                      <span><?php echo $header_city; ?>, </span>
                      <span><?php echo $header_state; ?>, </span>
                      <span><?php echo $header_zip; ?></span><br>
                      <span>P: <?php echo $header_phone; ?></span>
                      <span>F: <?php echo $header_fax; ?></span>
                      </p>
                      </div>
                     */
                    ?>
                    <div class="site-description">
                            <div class="search-content"> <?php /*
                                <div class="reit"><img src="<?php echo site_url(); ?>/wp-content/themes/rwacton/images/reit-logo.jpg?v=1.01"/></div>
                                <div class="search-form">
                                    <?php get_search_form(); ?>
                                </div> */ 

				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Quick Search Sidebar')) {
                
            			}
				?>
								<div class="clear"></div>
                            </div>
                    </div>
                </div>

                <div id="nav">
                    <?php wp_nav_menu(array('menu' => 'Main Nav', 'container_class' => 'nav', 'after' => '<span class="separator"></span>')); ?>
                </div>

            </div><!-- end #header -->
			<div class="red-space"><div class="clear"></div></div>

            <div id="main_body">
		  <div class="content">