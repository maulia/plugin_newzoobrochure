<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');
global $helper, $zoo_agents, $realty, $wpdb;

if (!empty($_FILES) && (is_user_logged_in() || $this->is_logged_in()) && intval($_REQUEST['id'])) {
	$id= intval($_REQUEST['id']);
	$uploaded_url=$realty->siteUrl.'wp-content/uploads/';
	$brochures_upload_url=$uploaded_url."brochures/$id/";

	$tempFile = $_FILES['Brochuredata']['tmp_name'];
	$targetPath = $_REQUEST['folder'];
	
	extract(pathinfo( $_FILES['Brochuredata']['name']));
	if(!isset($filename))$filename=str_replace(".".$extension,"",$basename);				
	$filename=sanitize_title($filename);
	$basename =  $_FILES['Brochuredata']['name'];
	$final_filename = $targetPath . $basename;
	$large_url = $brochures_upload_url . $basename;	

	for ($count = 1; file_exists( $final_filename ); $count++): 
		$basename = $filename . "_$count" .  '.' . $extension;
		$final_filename = $targetPath . $basename;	 			
		$large_url = $brochures_upload_url . $basename;	 			
	endfor;	
	if($zoo_agents->save_file($tempFile, $final_filename)){						
		$position=$wpdb->get_var("select max(position) from attachments where parent_id=$id and type='brochure'");
		if($position=='')$position=0;
		else $position=$position+1;				
		
		$zoo_agents->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'brochure','description'=>$basename,'url'=>$large_url, 'position'=>$position));
	}	
	
	echo $final_filename;
}
?>