<?php require_once('../../../../../wp-blog-header.php');
global $wpdb;
$testimonials = $wpdb->get_col("SELECT `content` FROM testimonials WHERE `user_id` =".intval($_GET['user_id']));
?>
<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
}
?>

<HTML>
<HEAD>
<TITLE>Testimonials</TITLE>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" title="Ginga" />
<link rel="stylesheet" type="text/css" media="screen, projection" href="<?php bloginfo('template_url'); ?>/styles/type/<?php echo $fonts; ?>.css" />
	<style type="text/css">
		p, ul, ol, cite { color: #<?php echo $main_text_color; ?>; }
		h2, h3, h4, blockquote p, blockquote strong, h5, h6, legend, blockquote ol, blockquote ul { color: #<?php echo $page_title_color; ?>; }
		a, a:link, a:visited { color: #<?php echo $link_color; ?>; }
	</style>

</HEAD>
<body id="team_page_popup">
	<div id="main_body">
	<h2 class="search_team_member_name">Testimonials</h2>
	<div class="search_team_member_testimonial">
	<ul>
		<?php foreach($testimonials as $item): ?>
		<li><?php echo $item; ?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	</div>
</BODY>
</HTML>