<?php
$property_types=$helper->get_column("select distinct name from property_types where category in ('Commercial') order by name");
?>
<form name="update_properties" id="update_properties" action="" method="post" autocomplete="off">
	<div class="content-left-column">
		<input type="hidden" name="type" value="Commercial">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div id="address_div" class="block_content">
			<h4>Property Address</h4>
			<table>
				<tr>
					<td class="label"><strong>Country<span class="red"> *</span></strong></td>
					<td>
						<select name="country" id="country" class="full">
						<?php foreach($countries as $country){ ?>
						<option value="<?php echo $country; ?>" <?php if($property['country']==$country)echo 'selected="selected"'; ?>><?php echo $country; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Suburb<span class="red"> *</span></strong></td>
					<td><input onkeyup="reloads_addres(this.value);" type="text" name="suburb" id="suburb" class="full" value="<?php echo $property['suburb']; ?>">
					<div class="suggestionsBoxSuburb" id="suggestionsBoxSuburb" style="display: none;">
						<div class="autoSuggestionsListSuburb" id="autoSuggestionsListSuburb">
							&nbsp;
						</div>
					</div>  
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Region<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="town_village" name="town_village"  value="<?php echo $property['town_village']; ?>">
						<span id="town_village_span"><?php echo $property['town_village']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>State<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="state" name="state"  value="<?php echo $property['state']; ?>">
						<span id="state_span"><?php echo $property['state']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Postcode<span class="red"> *</span></strong></td>
					<td>
						<input type="hidden" id="postcode" name="postcode"  value="<?php echo $property['postcode']; ?>">
						<span id="postcode_span"><?php echo $property['postcode']; ?></span>
					</td>
				</tr>
				<tr>
					<td class="label">Unit Number</td>
					<td><input type="text" name="unit_number" class="full"  value="<?php echo $property['unit_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Street Number<span class="red"> *</span></strong></td>
					<td><input type="text" id="street_number" name="street_number" class="full"  value="<?php echo $property['street_number']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Street Name/Type<span class="red"> *</span></strong></td>
					<td><input type="text" id="street" name="street" class="full"  value="<?php echo $property['street']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Display Address:</td>
					<td>
						<input type="radio" name="display_address" <?php if ($property['display_address']=='1' || !isset($property['display_address'])) echo 'checked="checked"'; ?> value="1">All Address Details
						<span style="margin-left:10px"><input type="radio" name="display_address" <?php if ($property['display_address']=='0') echo 'checked="checked"'; ?> value="0">Suburb Only</span>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="datas_div" class="block_content">
			<h4>Property Data</h4>
			<table>
				<tr>
					<td class="label"><strong>Property Type:<span class="red"> *</span></strong></td>
					<td colspan="2">
						<select name="property_type" class="full" onChange="change_area(this.value)">
						<option value="">Please Select</option>
						<?php foreach($property_types as $property_type){ ?>
							<option value="<?php echo $property_type; ?>" <?php if($property_type==$property['property_type']) echo 'selected="selected"'; ?>><?php echo $property_type; ?></option>
						<?php } ?>
						</select>					
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Sale/Lease:<span class="red"> *</span></strong></td>
					<td>
						<select name="deal_type" class="full">
							<option value="">Please Select</option>
							<option value="Sale" <?php if($property['deal_type']=='Sale')echo 'selected="selected"'; ?>>Sale</option>
							<option value="Lease" <?php if($property['deal_type']=='Lease')echo 'selected="selected"'; ?>>Lease</option>
							<option value="Both" <?php if($property['deal_type']=='Both')echo 'selected="selected"'; ?>>Both</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Exclusivity:*:<span class="red"> *</span></strong></td>
					<td>
						<select name="method_of_sale" class="full">
							<option value="">Please Select</option>
							<?php foreach($method_of_sales as $method_of_sale){ ?>
							<option value="<?php echo $method_of_sale ; ?>" <?php if($property['method_of_sale']==$method_of_sale)echo 'selected="selected"'; ?>><?php echo $method_of_sale ; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Authority:*:<span class="red"> *</span></strong></td>
					<td>
						<select name="authority" class="full">
							<option value="">Please Select</option>
							<?php foreach($authorities as $authority){ ?>
							<option value="<?php echo $authority ; ?>" <?php if($property['authority']==$authority)echo 'selected="selected"'; ?>><?php echo $authority ; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<?php /*<tr>
					<td class="label">Unique Property ID#:</td>
					<td><input type="text" class="full"  name="unique_id" value="<?php echo $property['unique_id']; ?>"></td>
				</tr>*/ ?>
				<tr>
					<td class="label">Building Name:</td>
					<td><input type="text" class="full"  name="building_name" value="<?php echo $property['building_name']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Property Headline:<span class="red"> *</span></strong></td>
					<td><input type="text" class="full" name="headline" value="<?php echo $property['headline']; ?>"></td>
				</tr>
				<tr>
					<td class="label"><strong>Property Description:<span class="red"> *</span></strong></td>
					<td><textarea name="description" cols="39" rows="15"><?php echo $property['description']; ?></textarea></td>
				</tr>
				<tr>
					<td class="label">Year built:</td>
					<td><input type="text" class="half" name="year_built"  value="<?php echo $property['year_built']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Highlight 1:</td>
					<td><input type="text" class="full" name="highlight1" value="<?php echo $property['highlight1']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Highlight 2:</td>
					<td><input type="text" class="full" name="highlight2" value="<?php echo $property['highlight2']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Highlight 3:</td>
					<td><input type="text" class="full" name="highlight3" value="<?php echo $property['highlight3']; ?>"></td>
				</tr>
			</table>
		</div>
	
		<div id="eer_div"class="block_content">
			<h4>Energy Efficiency Rating </h4>
			<table>
				<tr>
					<td class="label">Select Rating:</td>
					<td>
						<select name="energy_efficiency_rating" class="full">
							<option value="">Please Select</option>
							<?php foreach($eer as $i){ ?>
							<option value="<?php echo $i; ?>" <?php if($property['energy_efficiency_rating']==$i)echo 'selected';?>><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="map_div"class="block_content">
			<h4>Mapping Coordinates</h4>
			<p><strong><a class="submit_geocode" href="javascript:submit_geocode();">Set Map Location</a></strong></p>
			<p>
				<strong>Longitude:<span id="longitude_span" class="value"><?php echo ($property['longitude']=='')?'&nbsp':$property['longitude']; ?></span></strong>
				<strong>Latitude:<span id="latitude_span" class="value"><?php echo ($property['latitude']=='')?'&nbsp':$property['latitude']; ?></span></strong>
				<input type="hidden" name="longitude" id="longitude" value="<?php echo $property['longitude']; ?>">
				<input type="hidden" name="latitude" id="latitude" value="<?php echo $property['latitude']; ?>">
			</p>
			<div class="map-wrapper">
                <div id="map_canvas" style="width:380px !important;height:350px !important;"></div>
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <?php if(!empty($property['latitude'])){ ?>
                <script type="text/javascript">
                jQuery(document).ready(function () {
                    load();
                    });	
                </script>
                <?php } ?>
            </div>
		</div>

	</div>

	<div class="content-right-column">
		<div id="areas_div" class="block_content">
			<h4>Building Areas (Whole Numbers Only)</h4>
			<table>
				<tr>
					<td class="label" id="land_area_field"><?php echo ($property['property_type']=='Land' || $property['property_type']=='Farmland')?'<strong>Land Area:<span class="red"> *</span></strong>':'Land Area'; ?></td>
					<td><input type="text" class="half"  name="land_area" value="<?php if(!empty($property['land_area']))echo $property['land_area']; ?>"></td>
					<td>
						<select name="land_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['land_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label" id="floor_area_field"><?php echo($property['property_type']=='Land' || $property['property_type']=='Farmland' || $property['type']!='Commercial')?'Floor Area':'<strong>Floor Area:<span class="red"> *</span></strong>'; ?></td>
					<td><input type="text"  class="half" name="floor_area" value="<?php if(!empty($property['floor_area']))echo $property['floor_area']; ?>"></td>
					<td>
						<select name="floor_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['floor_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Office Area: </td>
					<td><input type="text" class="half"  name="office_area" value="<?php if(!empty($property['office_area']))echo $property['office_area']; ?>"></td>
					<td>
						<select name="office_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['office_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Warehouse Area:</td>
					<td><input type="text"  class="half" name="warehouse_area" value="<?php if(!empty($property['warehouse_area']))echo $property['warehouse_area']; ?>"></td>
					<td>
						<select name="warehouse_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['warehouse_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Retail Area: </td>
					<td><input type="text" class="half"  name="retail_area" value="<?php if(!empty($property['retail_area']))echo $property['retail_area']; ?>"></td>
					<td>
						<select name="retail_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['retail_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Other Area:</td>
					<td><input type="text"  class="half" name="other_area" value="<?php if(!empty($property['other_area']))echo $property['other_area']; ?>"></td>
					<td>
						<select name="other_area_metric">
							<option value="">Please Select</option>
							<?php foreach($metrics as $metric){ ?>
							<option value="<?php echo $metric; ?>" <?php if($property['other_area_metric']==$metric)echo 'selected';?>><?php echo $metric; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Area Range (m2):</td>
					<td><input type="text" class="half" name="area_range" value="<?php if(!empty($property['area_range'])) { echo $property['area_range']; } else { echo 'From'; } ?>" onclick="if(this.value=='From') this.value='';"></td>
                    <td><input type="text" class="half" name="area_range_to" value="<?php if(!empty($property['area_range_to'])) { echo $property['area_range_to']; } else { echo 'To'; } ?>" onclick="if(this.value=='To') this.value='';"></td>
				</tr>
				<tr>
					<td class="label">Whole or Part</td>
					<td colspan="2">
						<span class="span-break"><input <?php if($property['all_the_building']==0)echo 'checked="checked"'; ?> name="all_the_building" value="0" type="radio"> Part Building</span>
						<span class="span-break"><input <?php if($property['all_the_building']==1)echo 'checked="checked"'; ?> name="all_the_building" value="1" type="radio"> Whole Building</span>
						<span class="span-break"><input <?php if($property['all_the_building']==2)echo 'checked="checked"'; ?> name="all_the_building" value="2" type="radio"> Not applicable</span>
					</td>	
				</tr>
				<tr>
					<td class="label">All the Floor: </td>
					<td colspan="2">
						<input <?php if($property['all_the_floor']==1)echo 'checked="checked"'; ?> name="all_the_floor" value="1" type="checkbox"> 
					</td>	
				</tr>
				<tr>
					<td class="label">Zoning: </td>
					<td colspan="2">
						<input value="<?php echo $property['zoning']; ?>" class="full" name="zoning" type="text"> 
					</td>	
				</tr>
				<tr>
					<td class="label">Number of Car Spaces: </td>
					<td colspan="2">
						<input value="<?php echo $property['carport_spaces']; ?>" class="half" name="carport_spaces" type="text"> 
					</td>	
				</tr>
				<tr>
					<td class="label">Parking Comments: </td>
					<td colspan="2">
						<input value="<?php echo $property['parking']; ?>" class="full" name="parking" type="text"> 
					</td>	
				</tr>
				<tr>
					<td class="label">Tenancy: </td>
					<td colspan="2">
						 <select name="occupancy" class="full">
							<option value="">Please select</option>
							<option value="0" <?php if($property['occupancy']==0) echo 'selected="selected"'; ?>>Vacant Possession</option>
							<option value="1" <?php if($property['occupancy']==1) echo 'selected="selected"'; ?>>Tenanted Investment</option>
						</select> 
					</td>	
				</tr>
			</table>
		</div>
		
		<div id="others_div" class="block_content">
        	<h4>Property Url & Media</h4>
			<table>
				<?php /*<tr>
					<td class="label">Property Url:</td>
					<td><input type="text" class="full"  name="property_url" value="<?php echo $property['property_url']; ?>"></td>
				</tr>*/ ?>
				<tr>
					<td class="label">Virtual Tour 1 Url:</td>
					<td><input type="text" class="full"  name="virtual_tour" value="<?php echo $property['virtual_tour']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Virtual Tour 2 Url:</td>
					<td><input type="text" class="full"  name="ext_link_1" value="<?php echo $property['ext_link_1']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Video Url:</td>
					<td><input type="text" class="full"  name="ext_link_2" value="<?php echo $property['ext_link_2']; ?>"></td>
				</tr>
			</table>
		</div>
		
		<div id="price_div" class="block_content">			
			<h4>Pricing</h4>
			<h5 class="sub-title">Sale Details (*if Sale)</h5>
			<table class="table-sale-detail">
				<tr>
					<td class="label">Sale Price:</td>
					<td colspan="2"><input type="text" class="half" name="price" value="<?php if(!empty($property['price']))echo $property['price']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Tax:</td>
					<td colspan="2">
						<span class="span-break"><input <?php if($property['price_include_tax']==2)echo 'checked="checked"'; ?> name="price_include_tax" value="2" type="radio"> Exclusive</span>
						<span class="span-break"><input <?php if($property['price_include_tax']==3)echo 'checked="checked"'; ?> name="price_include_tax" value="3" type="radio"> Inclusive</span>
						<span class="span-break"><input <?php if($property['price_include_tax']==1)echo 'checked="checked"'; ?> name="price_include_tax" value="1" type="radio"> Not applicable</span>
					</td>	
				</tr>
				<tr>
					<td class="label">Sale Price Range:</td>
					<td><input type="text" class="half" name="sale_price_from" value="<?php if(!empty($property['sale_price_from'])) { echo $property['sale_price_from']; } else { echo 'From'; } ?>" onclick="if(this.value=='From') this.value='';"></td>
					<td><input type="text" class="half" name="sale_price_to" value="<?php if(!empty($property['sale_price_to'])) { echo $property['sale_price_to']; } else { echo 'To'; } ?>" onclick="if(this.value=='To') this.value='';"></td>
				</tr>
				<tr>
					<td class="label">Return % pa:</td>
					<td colspan="2"><input type="text"  class="half" name="return_percent" value="<?php echo $property['return_percent']; ?>"></td>
				</tr>
			</table>
			
			<h5 class="sub-title">Lease Details (*if Lease)</h5>
			<table>
				<tr>
					<td class="label">Rent pa:</td>
					<td colspan="2"><input type="text"  class="half"  name="current_rent" value="<?php echo $property['current_rent']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Tax:</td>
					<td colspan="2">
						<span class="span-break"><input <?php if($property['price_include_tax']==2)echo 'checked="checked"'; ?> name="price_include_tax" value="2" type="radio"> Exclusive</span>
						<span class="span-break"><input <?php if($property['price_include_tax']==3)echo 'checked="checked"'; ?> name="price_include_tax" value="3" type="radio"> Inclusive</span>
						<span class="span-break"><input <?php if($property['price_include_tax']==1)echo 'checked="checked"'; ?> name="price_include_tax" value="1" type="radio"> Not applicable</span>
					</td>	
				</tr>
				<tr>
					<td class="label">Rental psm/pa:</td>
					<td><input type="text" class="half" name="rental_price_from" value="<?php if(!empty($property['rental_price_from'])) { echo $property['rental_price_from']; } else { echo 'From'; } ?>" onclick="if(this.value=='From') this.value='';"></td>
					<td><input type="text" class="half" name="rental_price_to" value="<?php if(!empty($property['rental_price_to'])) { echo $property['rental_price_to']; } else { echo 'To'; } ?>" onclick="if(this.value=='To') this.value='';"></td>
				</tr>
				<tr>
					<td class="label">Plus Outgoings: </td>
					<td colspan="2"><input <?php if($property['outgoings_paid_by_tenant']==1)echo 'checked="checked"'; ?> name="outgoings_paid_by_tenant" value="1" type="checkbox"></td>	
				</tr>
				<tr>
					<td class="label">Annual Outgoings:</td>
					<td colspan="2"><input type="text" class="half"  name="annual_outgoings" value="<?php echo $property['annual_outgoings']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Outgoings Paid by:</td>
					<td colspan="2">
						<input <?php if($property['outgoings_paid_by']==0)echo 'checked="checked"'; ?> name="outgoings_paid_by" value="0" type="radio"> Exclusive &nbsp;
						<input <?php if($property['outgoings_paid_by']==1)echo 'checked="checked"'; ?> name="outgoings_paid_by" value="1" type="radio"> Inclusive
					</td>	
				</tr>
				<tr>
					<td class="label">Lease End: </td>
					<td colspan="2"><input type="text" class="full" id="lease_end" name="lease_end" value="<?php if(!empty($property['lease_end']) && $property['lease_end']!='0000-00-00')echo date("d-m-Y",strtotime($property['lease_end'])); ?>"></td>
				</tr>
				<tr>
					<td class="label">Further Option: </td>
					<td colspan="2"><input type="text" class="full" name="lease_further_option" value="<?php echo $property['lease_further_option']; ?>"></td>
				</tr>
			</table>
        </div>
        
        <div class="block_content">
			<h4>Displaying Price</h4>
			<table>
				<tr>
					<td class="label">Display Above Price:</td>
					<td colspan="2"><input type="radio" name="display_price" <?php if ($property['display_price']=='1' || !isset($property['display_price'])) echo 'checked="checked"'; ?> value="1"></td>	
				</tr>
				<tr>
					<td class="label">Display text in place of Price:</td>
					<td><input type="radio" name="display_price" <?php if ($property['display_price']=='2') echo 'checked="checked"'; ?> value="2"></td>
					<td><input type="text"  class="full display-price"  name="display_price_text" value="<?php echo $property['display_price_text']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Not display Price:</td>
					<td><input type="radio" name="display_price" <?php if ($property['display_price']=='0') echo 'checked="checked"'; ?> value="0"></td>
					<td>&nbsp;</td>
				</tr>
			</table>
        </div>

        <div class="block_content">
			<h4>Event Date & Time </h4>
			<table>
				<tr>
					<td class="label">Event Place:</td>
					<td colspan="3"><input class="full" type="text" name="auction_place" value="<?php echo $property['auction_place']; ?>"></td>
				</tr>
				<tr>
					<td class="label">Event Date: /Time:</td>
					<td><input class="half" type="text"  id="auction_date" name="auction_date" value="<?php if(!empty($property['auction_date']) && $property['auction_date']!='0000-00-00')echo date("d-m-Y",strtotime($property['auction_date'])); ?>"></td>
					<td>&nbsp;@&nbsp;</td>
					<td><input type="text"  class="half" id="auction_time" name="auction_time" value="<?php if(!empty($property['auction_time']) && $property['auction_time']!='00:00:00')echo date("H:i",strtotime($property['auction_time'])); ?>"></td>
				</tr>
			</table>				
		</div>
		
		<div id="users_div" class="block_content">
			<h4>Property Contacts </h4>
			<table>
				<tr>
					<td class="label"><strong>Primary Contact:<span class="red"> *</span></strong></td>
					<td>
						<select name="user_id" class="full">
							<option value="">Please Select</option>
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['user_id']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Secondary Contact:</td>
					<td>
						<select name="secondary_user" class="full">
							<option value="">Please Select</option>	
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['secondary_user']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Third Contact:</td>
					<td>
						<select name="third_user" class="full">
							<option value="">Please Select</option>
							<?php foreach($users as $user){ ?>
							<option value="<?php echo $user['id']; ?>" <?php if($property['third_user']==$user['id'])echo 'selected="selected"'; ?>><?php echo $user['name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>				
			</table>
		</div>
	</div>
    
    <div class="clear"></div>
    
   <div class="clear"></div>	
     <div class="block_content">
		<input type="submit" name="submit" value="Save & Next" class="btn" id="submit_btn" >
	</div>
</form>