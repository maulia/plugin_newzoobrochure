<style type="text/css">

.tagchecklist {
width:300px;

}
.tagchecklist span{
	margin-right: 25px;
	float: left;
	font-size: 11px;
	line-height: 1.8em;
	white-space: nowrap;
}
.tagchecklist span a {
	background: url(<?php echo  $realty->siteUrl;?>wp-admin/images/xit.gif) no-repeat;
	margin: 6px 0pt 0pt -5px;
	cursor: pointer;
	width: 10px;
	height: 10px;
	display: block;
	float: left;
	text-indent: -9999px;
	overflow: hidden;
	position: absolute;
}

.tagchecklist a:hover {
	background: url(<?php echo $realty->siteUrl;?>wp-admin/images/xit.gif) no-repeat -10px 0;
}
.tag_icon:hover {
	background: url(<?php echo $realty->siteUrl;?>wp-admin/images/xit.gif) no-repeat -10px 0;
}
.tag_icon {
	background: url(<?php echo  $realty->siteUrl;?>wp-admin/images/xit.gif) no-repeat;
	margin: 6px 0pt 0pt -9px;
	width: 10px;
	height: 10px;
	overflow: hidden;
	position: absolute;
	cursor: pointer;
	text-indent: -9999px;
}
.tag_span{
	margin-right: 25px;
	font-size: 11px;
	line-height: 1.8em;
	white-space: nowrap;
}
#tag_span_a{
	margin-right: 25px;
}
</style>
<script type="text/javascript">
/* <![CDATA[ */

jQuery(document).ready(function(){
	jQuery(".add_item").unbind('click').click(function() {
		var values_array = jQuery(this).prev().val().split(";");
		for(i=0;i<values_array.length;i++) {
	   	  values_array[i] = values_array[i].replace(/^\s+|\s+$/g, '');
		  if(values_array[i] !='')
		  	jQuery(this).next().append('<span><input type="hidden" name="'+jQuery(this).attr('rel') +'" value="'+values_array[i]
		  	+'" /><a href="#" onclick="javascript:jQuery(this).parent().remove();return false;" title="Delete">x</a>&nbsp;'+values_array[i] +'</span>');
		  }//ends for values_array
		return false;
	});

});//ends document).ready
/* ]]> */
</script>

<?php if(!function_exists('realty_add_tag_box')):
function realty_add_tag_box($name, $settings ){ ?>



<input type="text" name="" value="" />	
<a class="button add_item" href="#" title="Add" rel="<?php echo $name; ?>">Add</a>
<p class="tagchecklist">
	<?php if(is_array($settings)) foreach($settings as $item): ?>
	<span>
	<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $item; ?>" />
	<a href="#" onclick="javascript:jQuery(this).parent().remove();return false;" title="Delete" >x</a>&nbsp;<?php echo "   ".$item; ?></span>		
	<?php endforeach; ?>
</p>

<?php }// ends function 
endif;
?>