<?php
//metadata: access_level=1
//$colours = array('#e43ab7', '#857a82', '#0b090b', '#f68231', '#ba7b4f', '#c9ee8b', '#67941e', '#1e7394', '#8b1e94','#ee3917', '#23ee17', '#1732ee');
$colours = array('#7e2a1d', '#d78c23', '#d7ce31', '#70c141', '#2b7d36', '#4cb2c2', '#2b497d', '#7d5f2b');
$identifier = substr(pathinfo(__FILE__, PATHINFO_BASENAME), 0, -4); //The name of the file is used as unique identifier for options in the plugin.
$stickers = $helper->getTemplates($realty->path($realty->paths['stickers']));

if(isset($_POST['submit'])):
	update_option('realty_property_stickers', $_POST['sticker']);
	$return = 'Settings Updated.';
endif;
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
$realty->element('add_tag_script');
$settings = get_option('realty_property_stickers');

?>
<div class="wrap">
<form name="" method="post" action="" id="elements">
<h2>Property Stickers</h2> 
<p>Where you choose sold, leased and under offer stickers for your property.</p>
<table class="widefat"> 
	<tr class="alternate">
<th scope="row">Sticker Colour</th>
<td>
	<table class="color-palette">
	<tbody><tr >
		<?php foreach($colours as $colour): ?>	
		<td style="background-color: <?php echo $colour; ?>;">&nbsp;<input type="radio" value="<?php echo $colour; ?>" name="sticker[sticker_colour]"<?php if($colour == $settings['sticker_colour']) echo ' checked="checked"'; ?> /></td>
		<?php endforeach; ?>
		</tr>
	</tbody></table>
</td>
</tr>


<tr>
<th scope="row">Sticker Text</th>
	<td>
		<label>Sold</label>
		<input type="text" name="sticker[sticker_text][sold]" value="<?php echo $settings['sticker_text']['sold']; ?>" class="smaller-text" />
		<label>Leased</label>
		<input type="text" name="sticker[sticker_text][leased]" value="<?php echo $settings['sticker_text']['leased']; ?>" class="smaller-text" />
		<label>Under Offer</label>
		<input type="text" name="sticker[sticker_text][under_offer]" value="<?php echo $settings['sticker_text']['under_offer']; ?>" class="smaller-text" />
		</td>
</tr>

<tr class="alternate">
<th scope="row">Sticker Format</th>
	<td>
	<?php if(is_array($stickers)) foreach($stickers as $filename=>$path): $path = $realty->relativePath($path); //For each sticker image the stickers folder ?>
	<span class="left-align">
	<input type="radio" value="<?php echo $path; ?>" name="sticker[sticker_src]"<?php if($path == $settings['sticker_src']) echo ' checked="checked"'; ?> /><img src="<?php echo $realty->path($path, 'url'); ?>" width="100px" title="<?php echo $filename; ?>" alt="<?php echo $filename; ?>" />
	</span>
	<?php endforeach; ?>
	</td>
</tr>
</table>



 
 <p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
</form>
</div><!-- ends .wrap -->