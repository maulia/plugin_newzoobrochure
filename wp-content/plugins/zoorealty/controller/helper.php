<?php
class helper extends my_db{

	function wp_insert_post($post, $unique=false){
		if(empty($post['post_type'])) $post['post_type'] = 'page';
		if($unique && $this->getPost($this->underscore($post['post_title'])) != false)return;
		$postID = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => $post['post_type'], 'post_title'=>$this->humanize($post['post_title']), 'post_content'=>$post['post_content'], 'post_parent'=>$post['post_parent']));
		update_post_meta($postID, '_permanent_name',  $this->underscore($post['post_title']));
		if(!empty($post_template))update_post_meta($postID, '_wp_page_template',  $post['post_template']);
		if(is_array($post['custom'])){
			foreach ($post['custom'] as $meta_key=>$meta_value)update_post_meta($postID, $meta_key, $meta_value);
		}
		return $postID;		
	}

	function replaceSlashes($file){
		return str_replace('\\', '/', $file);
	}
	
	function getTemplates($dir, $type ='', $excludedExtensions= array('db')){
		$paths = glob($dir . "*$type" );
		foreach($paths as $path):
			$details = pathinfo($path);
			//For some reason $template[$details['filename']]  is not working on triproom
			if(!in_array($details['extension'], $excludedExtensions))
				$template[substr($details['basename'], 0, -(strlen($details['extension']) +1))] = $this->replaceSlashes($path); 
		endforeach;
	return $template;		
	}

	function convertUnit( $value, $unit = "sq"){
		switch ($unit):
			case 'Hectares': // Hectares
				$multiplier = 0.0001;
			break;
			case 'Acres': // Acres
				$multiplier = 0.0002471;
			break;
			case 'sqf': //Square feet
				$multiplier = 10.7639104;
			break;
			case 'sq': // Squares
				$multiplier = 0.1076391;
			break;
			default:
				$multiplier = 1;
			break;
		endswitch;	
		return number_format($value * $multiplier, 2);
	}//ends function

	function get_converted_sizes($value, $units = array('Acres', 'Hectares')){
		$value = (float) $value;	
		if(is_array($units) and $value>0) foreach ($units as $unit)	
			$sizes[$unit] = $this->convertUnit($value, $unit) . " $unit";
		return $sizes;
	}

	function _substr($string, $max_chars ){
    	$string = str_replace(']]>', ']]&gt;', $string);
 		 if ((strlen($string)>$max_chars) && ($space = strpos($string, " ", $max_chars )))
        	$string = substr($string, 0, $space); //Only split if the string is larger than the max number of chars. Also, split on the next white space.
		return $string;
	}

	function getPost($permanent_name){
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare("SELECT `ID` FROM $wpdb->posts LEFT JOIN $wpdb->postmeta ON `ID`=`post_id` WHERE `meta_key`='_permanent_name' AND `meta_value`= %s LIMIT 1", $permanent_name));
	}
	
	function wp_post_name(){
		global $wp_query;		
		$page_obj = $wp_query->get_queried_object(); 
		return $page_obj->post_name;
	}

	function underscore($camelCasedWord) {
		return strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "", $camelCasedWord)));
	}

	function humanize($lowerCaseAndUnderscoredWord) {
		return ucwords(str_replace("_", " ", $this->underscore($lowerCaseAndUnderscoredWord)));
	}

	function camelize($lowerCaseAndUnderscoredWord) {
		$string =str_replace(" ", "", ucwords(str_replace("_", " ", $lowerCaseAndUnderscoredWord)));
		$string{0} = strtolower($string{0});
		return $string;
	}

	function isSpam($answer, $question_id){
		return $this->spam_questions[$question_id]['answer'] != strtolower(trim($answer));
	}

	function send_email($to='',$subject='', $html_content='', $text_content='', $from='', $bcc='', $flag=true) { 
		global $realty;
		# Setup mime boundary
		$mime_boundary = 'Multipart_Boundary_x'.md5(time()).'x';

		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\r\n";
		$headers .= "Content-Transfer-Encoding: 7bit\r\n";

		$body	 = "This is a multi-part message in mime format.\n\n";

		# Add in plain text version
		$body	.= "--$mime_boundary\n";
		$body	.= "Content-Type: text/plain; charset=\"charset=us-ascii\"\n";
		$body	.= "Content-Transfer-Encoding: 7bit\n\n";
		$body	.= $text_content;
		$body	.= "\n\n";

		# Add in HTML version
		$body	.= "--$mime_boundary\n";
		$body	.= "Content-Type: text/html; charset=\"UTF-8\"\n";
		$body	.= "Content-Transfer-Encoding: 7bit\n\n";
		$body	.= $html_content;
		$body	.= "\n\n";

		# Attachments would go here
		# But this whole email thing should be turned into a class to more logically handle attachments, 
		# this function is fine for just dealing with html and text content.

		# End email
		$body	.= "--$mime_boundary--\n"; # <-- Notice trailing --, required to close email body for mime's

		# Finish off headers
		if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
		if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\r\n";
		if($flag){
			if(empty($bcc)) $bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
			if(!empty($bcc)) $bcc = "bcc: $bcc\r\n";		
		}
		//$headers .= "From: $from\r\n";
		$headers .= "$from\r\n";
		$headers .= "$bcc\r\n";
		$headers .= "X-Sender-IP: $_SERVER[SERVER_ADDR]\r\n";
		$headers .= 'Date: '.date('n/d/Y g:i A')."\r\n";

		# Mail it out
		return mail($to, $subject, $body, $headers);
	}
	
	function email($to, $subject, $message, $from='', $bcc='', $flag=true){
		global $realty;
		//On netregistry emails, messages from non existing email addresses are deleted, so we shouldn't rely on users emails.
		if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
		if(empty($from)) $from = get_option('admin_email');
		if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\r\n";		
		if($flag){
		if(empty($bcc)) $bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
		if(!empty($bcc)) $bcc = "bcc: $bcc\r\n";		
		}	
		return @wp_mail($to, $subject, $message, $from . $bcc . "Content-Type: text/html\n");		
	}
	
	function text_email($to, $subject, $message, $from='', $bcc='', $flag=true){
		global $realty;
		if(empty($from)) $from = $realty->settings['general_settings']['emails_sent_from'];
		if(!empty($from)) $from = "From: \"$realty->blogname\" <$from>\r\n";		
		if($flag){
		if(empty($bcc)) $bcc = $realty->settings['general_settings']['bcc_all_mail_to'];
		if(!empty($bcc)) $bcc = "bcc: $bcc\r\n";		
		}	
		return @wp_mail($to, $subject, $message, $from . $bcc . "Content-Type: text/plain\n");		
	}
	
	function isValidEmail($email) {		
		if(!filter_var($email, FILTER_VALIDATE_EMAIL))return false;
		else return true;
	}
	
	function spamQuestion(){
		$this->spam_questions = array(
		array('question'=>'Is Fire Hot or Cold?','answer'=>'hot'),
		array('question'=>'Is Water Wet or Dry?','answer'=>'wet'),
		array('question'=>'Is Paper Thin or Thick?','answer'=>'thin')
		);
		$random_index = rand(0,count($this->spam_questions) - 1);
		$spam_question = $this->spam_questions[$random_index];
		$spam_question['question_id'] = $random_index;
		return $spam_question;
	}

	function paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $realty;
		//Retrieve how many links to show in each page
		$max_page_links = 	$realty->settings['general_settings']['number_of_page_links_on_pagination'];
		$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
	
		//$current_query =  "/" . $_SERVER['QUERY_STRING']; p($_SERVER['REQUEST_URI']);
		
		$prev_page = $page - 1;
		$next_page = $page + 1;
		if($next_page == 1) $next_page = 2;
		ob_start();
			require($realty->path('display/elements/pagination.php'));
		$return = ob_get_clean();
		return $return;
	}
	
	function ajax_paginate($limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $realty;
		//Retrieve how many links to show in each page
		$max_page_links = 	$realty->settings['general_settings']['number_of_page_links_on_pagination'];
		$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
	
		//$current_query =  "/" . $_SERVER['QUERY_STRING']; p($_SERVER['REQUEST_URI']);
		
		$prev_page = $page - 1;
		$next_page = $page + 1;		
		if($next_page == 1) $next_page = 2;
		ob_start();
			require($realty->path('display/elements/ajax_pagination.php'));
		$return = ob_get_clean();
		return $return;
	}
	
	function team_paginate($status,$user_id, $limit = 1,$page = 1, $results_count = 0, $sql = ''){
		global $realty;
		//Retrieve how many links to show in each page
		$max_page_links = 	$realty->settings['general_settings']['number_of_page_links_on_pagination'];
		$page_link = $page - floor($max_page_links / 2); //Make the current page always stay in the middle
		if($page_link <1)
			$page_link = 1;
		$total_pages = ceil($results_count / $limit);
	
		//$current_query =  "/" . $_SERVER['QUERY_STRING']; p($_SERVER['REQUEST_URI']);
		
		$prev_page = $page - 1;
		$next_page = $page + 1;		
		if($next_page == 1) $next_page = 2;
		ob_start();
			require($realty->path('display/elements/team_pagination.php'));
		$return = ob_get_clean();
		return $return;
	}
	
	function replacePageLabel($replacement){
		 if(!empty($_SERVER['QUERY_STRING'])):
			$queryString ='?' . $_SERVER['QUERY_STRING'];
			$url = substr($_SERVER['REQUEST_URI'], 0, -strlen($queryString));
		else:
			$url = $_SERVER['REQUEST_URI'];
		endif;
		$newUrl = preg_replace('/\/([-+]?\\d+)\//is', "/$replacement/", $url, 1, $replacesMade);
		if($replacesMade < 1) //If no replacements were made, it was the first page, just add it.
			$newUrl .= "$replacement/";
		$newUrl .= $queryString; 
		return $newUrl;
	}
	
	function isAssoc($arr){
	    return array_keys($arr) != range(0, count($arr) - 1);
	}
	
	function sanitizeUrl($param){	
		$prev_url = urldecode($_SERVER['QUERY_STRING']);
		$param_value = explode('=', $param);
		if($param_value[1] == 'all'): //E.g. state=all
			//$new_url = preg_replace('/(&?'.preg_quote($param_value[0]) .'=)([\w]+&?)/is', '', $prev_url);
			$new_url = preg_replace('/(&?'.preg_quote($param_value[0]) .'=)(([\w\s[\/\]])+)/', '', $prev_url);
		elseif(empty($prev_url)):		
			$new_url = $param;
		else: //Otherwise, we need to do some more checking.
			if(strpos($param, '[]') === false){ //If the paramater is not an array. E.g. state=3
				$new_url = (strpos($prev_url, $param_value[0] ) === false)? $prev_url. '&amp;' .$param: preg_replace('/(' . $param_value[0] . '=)([-+]?\\d+)/is', $param, $prev_url);
			}else{//E.g. state[]=3
				//If the param doesn't exist in the url yet, add it
				$new_url =(strpos($prev_url, $param) === false)?
				$prev_url . '&amp;' .$param:str_replace(array( "&$param", "$param&", $param),'', $prev_url);				
			}
		endif;
		$new_url= str_replace("&enchanced[]=","",$new_url);
		$new_url = preg_replace('/(page=)([-+]?\\d+)/is', '', $new_url); 
		if(!empty($new_url))
			return '?' . $new_url;
	}
	
	function sanitize_url($param){
		//echo($param);
		$prev_url = urldecode($_SERVER['QUERY_STRING']);
		if(strpos($prev_url, $param) === false)://If the param doesn't exist in the url yet, add it
			$new_url = (empty($prev_url))?$param:$prev_url . '&amp;' .$param;
		else: //$new_url = preg_replace('/(&' . preg_quote($param) .')/is', '', $prev_url);
			$new_url = str_replace(array( "&$param", "$param&", $param),'', $prev_url);
		endif;
		$new_url = preg_replace('/(page=)([-+]?\\d+)/is', '', $new_url);
		if(!empty($new_url))
			return '?' . $new_url;
	
	}
	
	function validUrl($str){
		return ( ! preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $str)) ? FALSE : TRUE;
	}
	
	function recursive_array_search($needle,$haystack) { 
			/*http://au.php.net/array_search
			 * tony dot peter at wanadoo dot fr
			 */
		foreach($haystack as $key=>$value) { 
			$current_key=$key; 
			if($needle===$value OR (is_array($value) && $this->recursive_array_search($needle,$value))) { 
				return $current_key; 
			} 
		} 
		return false; 
	} 
	
	function isLocalhost(){ // Determines if this script is running on an Apache localhost or on the server. Useful for testing on Windows platforms, since the htaccess redirection does not work
		return strpos($_SERVER['SERVER_NAME'],"127.0.0.1") or strpos($_SERVER['SERVER_NAME'],"localhost") !==false;
	}
	
	function writeFile($filename,$contents){
		// Let's make sure the file exists and is writable first.
	//	if (is_writable($filename)):
		   if (!$handle = fopen($filename, 'w')):
				return false;
				exit;
		   endif;
		
		   // Write $somecontent to our opened file.
		   if (fwrite($handle, $contents) === FALSE):
			   return false;
			   exit;
		  	endif;
		
		   return true;		
		   fclose($handle);		
		/*else:
		   return "The file $filename is not writable";
		endif;	*/
	}

	function parse_query_string($url, $qmark=true){
    if ($qmark) {
        $pos = strpos($url, "?");
        if ($pos !== false) {
            $url = substr($url, $pos + 1);
        }
    }
    if (empty($url))
        return false;
    $tokens = explode("&", $url);
    $urlVars = array();
    foreach ($tokens as $token) {
        $value = $this->string_pair($token, "=", "");
        if (preg_match('/^([^\[]*)(\[.*\])$/', $token, $matches)) {
            $this->parse_query_string_array($urlVars, $matches[1], $matches[2], $value);
        } else {
            $urlVars[urldecode($token)] = urldecode($value);
        }
    }
    return $urlVars;
}

	function parse_query_string_array(&$result, $k, $arrayKeys, $value){
	    if (!preg_match_all('/\[([^\]]*)\]/', $arrayKeys, $matches))
	        return $value;
	    if (!isset($result[$k])) {
	        $result[urldecode($k)] = array();
	    }
	    $temp =& $result[$k];
	    $last = urldecode(array_pop($matches[1]));
	    foreach ($matches[1] as $k) {
	        $k = urldecode($k);
	        if ($k === "") {
	            $temp[] = array();
	            $temp =& $temp[count($temp)-1];
	        } else if (!isset($temp[$k])) {
	            $temp[$k] = array();
	            $temp =& $temp[$k];
	        }
	    }
	    if ($last === "") {
	        $temp[] = $value;
	    } else {
	        $temp[urldecode($last)] = $value;
	    }
	}
	
	function json_encode($data) {
		if( !function_exists('json_encode') ){
		require_once( dirname(__FILE__). '/JSON.php' );
		$json = new Services_JSON();
		return( $json->encode($data) );
		}else
		return json_encode( $data );
	}

	function string_pair(&$a, $delim='.', $default=false){
	    $n = strpos($a, $delim);
	    if ($n === false)
	        return $default;
	    $result = substr($a, $n+strlen($delim));
	    $a = substr($a, 0, $n);
	    return $result;
	}

	function table_colums($table){
		global $wpdb;
		return $wpdb->get_col("SHOW COLUMNS FROM $table");
	}
}//ends class

if(!function_exists('p')){
	function p($var, $terminate = false, $export = false, $output = true){
		if(!$output)
			ob_start();
		echo '<pre>';
		($export)?var_export($var): var_dump($var);
		echo '</pre>';

		if(!$output) 
			return ob_get_clean();
		if($terminate)
			exit(0);	
	}
}

if(!function_exists('bf_smart_multiwidget_update')){
	function bf_smart_multiwidget_update($id_prefix, $options, $post, $sidebar, $option_name = ''){
		global $wp_registered_widgets;
		static $updated = false; 
		// get active sidebar
		$sidebars_widgets = wp_get_sidebars_widgets();
		if ( isset($sidebars_widgets[$sidebar]) )
			$this_sidebar =& $sidebars_widgets[$sidebar];
		else
			$this_sidebar = array(); 
		// search unused options
		foreach ( $this_sidebar as $_widget_id ) {
			if(preg_match('/'.$id_prefix.'-([0-9]+)/i', $_widget_id, $match)){
				$widget_number = $match[1];
 
				// $_POST['widget-id'] contain current widgets set for current sidebar
				// $this_sidebar is not updated yet, so we can determine which was deleted
				if(!in_array($match[0], $_POST['widget-id'])){
					unset($options[$widget_number]);
				}
			}
		} 
		// update database
		if(!empty($option_name)){
			update_option($option_name, $options);
			$updated = true;
		} 
		// return updated array
		return $options;
	}
}
?>