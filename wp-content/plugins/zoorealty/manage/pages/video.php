<?php
//metadata: access_level=8
global $helper, $wpdb;

if(isset($_POST['submit'])):
	$videos=$_POST['videos'];
	$settings=array();
	if(!empty($videos)){
		$i=0;
		foreach($videos as $key=>$video){
			if(!empty($video['youtube_id']) || !empty($video['embed_code'])){
				$video['embed_code']=stripslashes(str_replace('\\','',$video['embed_code']));
				$video['description']=stripslashes(str_replace('\\','',$video['description']));
				$video['headline']=stripslashes(str_replace('\\','',$video['headline']));
				$settings[$i]=$video;
				$i++;
			}			
		}		
	}
	//print_r ($settings);
	if(!update_option('realty_videos',$settings))add_option('realty_videos',$settings);
	$return="Videos updated.";
endif;
$settings = (empty($_POST['videos']))?get_option('realty_videos'): $_POST['videos'];
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
?>
<div class="wrap">	
	<h2>Place Corporate Video</h2>
	<p><a href="javascript:;" onclick="addPages('','');">Add New</a></p>	
	<input type="hidden" value="0" id="theValue" />
	<form name="" method="post" action="">
	<div id="myDiv">	
		<?php if(!empty($settings)){
			foreach($settings as $item){ 
				$theValue++;
				$divIdName='my'.$theValue.'Div';
			?>
			<script>document.getElementById('theValue').value='<?php echo $theValue; ?>';</script>
			<div id="<?php echo $divIdName; ?>">
				<table class="widefat">
					<thead>
					<tr>
						<th scope="col"><?php echo "Video No. $theValue"; ?></th>
						<th scope="col"><a href='#' onclick='removePages("<?php echo $divIdName; ?>")'>Remove</a></th>			
					</tr>
					</thead>
					<tr>
						<td>YouTube ID</td>					
						<td><input type="text" class="widefat" name="videos[<?php echo $theValue; ?>][youtube_id]" value="<?php echo $item['youtube_id']; ?>"></td>					
					</tr>
					<tr>
						<td>Embed Code</td>					
						<td><textarea class="widefat" name="videos[<?php echo $theValue; ?>][embed_code]" cols="100" rows="10"><?php echo stripslashes(str_replace('\\','',$item['embed_code'])); ?></textarea></td>					
					</tr>
					<tr>
						<td>Headline</td>					
						<td><input type="text" class="widefat" name="videos[<?php echo $theValue; ?>][headline]" value="<?php echo stripslashes(str_replace('\\','',$item['headline'])); ?>"></td>					
					</tr>
					<tr>
						<td>Description</td>					
						<td><textarea class="widefat" name="videos[<?php echo $theValue; ?>][description]" cols="100" rows="10"><?php echo stripslashes(str_replace('\\','',$item['description'])); ?></textarea></td>					
					</tr>
				</table>
			</div>
			<?php } 
		} ?>
	</div>
	
	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</form>		
	
	<br/>
<?php
$video_types=array('sold_property_videos','current_property_videos','agent_profile_videos','other_videos');
if(isset($_POST['submit_front'])):
	if(!update_option('video_type',$_POST['video_type']))add_option('video_type',$_POST['video_type']);
	$return_front='Setting Updated.';
endif;

if(!empty($return_front)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return_front; ?></strong></p></div><?php
endif;

$default_video_type=get_option( 'video_type' );
?>	
	<form name="" method="post" action="">
		<table class="widefat">			
			<tr>
				<td>Default Video Tab</td>					
				<td>
					<select name="video_type">
						<option value="">All Videos</option>
						<?php foreach($video_types as $video_type): ?>
						 <option value="<?php echo $video_type; ?>" <?php if($default_video_type==$video_type) echo 'selected'; ?>><?php echo $helper->humanize($video_type); ?></option>
						<?php endforeach; ?>
					</select>
				</td>					
			</tr>
		</table> 	
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit_front" /></p> 
	</form>		
	
	<Br/>
<?php
if(is_null($listings))$listings = $realty->properties_video(array('limit'=>'100','display_agent'=>'1'));
if(isset($_POST['submit_featured'])):
	$featured_ids=$_POST['featured_ids'];
	if(!update_option('video_featured_ids',$featured_ids))add_option('video_featured_ids',$featured_ids);
	$return_featured='Featured Videos Saved.';
endif;
$featured_ids=get_option('video_featured_ids');
if(!is_array($featured_ids))$featured_ids=array();
if(!empty($return_featured)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return_featured; ?></strong></p></div><?php
endif;

?>
	<h2>Featured Videos</h2>
	<form name="" method="post" action="">
		<table class="widefat">
			<thead>
			<tr>
				<th scope="col"></th>			
				<th scope="col">Listing ID</th>			
				<th scope="col">Address</th>	
			<?php /*	<th scope="col">Price</th>	*/ ?>
				<th scope="col">Status</th>	
				<th scope="col">Agent ID</th>	
				<th scope="col">Name</th>	
				<th scope="col">Video No</th>	
				<th scope="col">Video Headline</th>	
			</tr>
			</thead>
			<?php 
			$i=0;
			foreach($listings as $listing): if(!is_array($listing))continue;			
			switch($listing['status']){
				case '1':$status="Available";break;
				case '2':$status="Sold";break;
				case '4':$status="Under Offer";break;
				case '6':$status="Leased";break;				
			}				
			?> 
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
				<td><span style="margin-left:4px">&nbsp;</span><input type="checkbox" name="featured_ids[]"  <?php if (in_array($listing['id'], $featured_ids)) echo 'checked="checked" '; ?>  value="<?php echo $listing['id']; ?>"></td> 
				<td><?php if($listing['properties']=='1')echo $listing['id']; ?></td>	
				<td><?php if($listing['properties']=='1'){ ?><a href="<?php echo $listing['url']; ?>"><?php if($listing['properties']=='1')echo $listing['street_address'].' '.$listing['suburb']; ?></a><?php } ?></td>	
				<?php /*<td><?php if($listing['properties']=='1')echo $listing['price']; ?></td>	*/ ?>
				<td><?php if($listing['properties']=='1')echo $status; ?></td>	
				<td><?php if($listing['agent']=='1')echo $listing['id']; ?></td>	
				<td><?php if($listing['agent']=='1'){ ?><a href="<?php echo $listing['url']; ?>"><?php echo $listing['headline']; ?></a><?php } ?></td>	
				<td><?php if($listing['admin']=='1')echo $listing['id']; ?></td>	
				<td><?php if($listing['admin']=='1')echo $listing['headline']; ?></td>	
			</tr> 
			<?php $i++; endforeach; ?>
		</table> 	
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit_featured" /></p> 
	</form>		
	<script type="text/javascript">	
	function addPages(youtube_id, embed_code) {
	  var ni = document.getElementById('myDiv');
	  var numi = document.getElementById('theValue');
	  var num = (document.getElementById('theValue').value -1)+ 2;
	  numi.value = num;
	  var newdiv = document.createElement('div');
	  var divIdName = 'my'+num+'Div';	  
	  var youtube_id_name = 'videos['+num+'][youtube_id]';
	  var embed_code_name = 'videos['+num+'][embed_code]';
	  var headline_name = 'videos['+num+'][headline]';
	  var desc_name = 'videos['+num+'][description]';
	  newdiv.setAttribute('id',divIdName);
	  newdiv.innerHTML = 	'<table class="widefat">'+
							'<thead><tr>'+
							'<th scope="col">Video No.'+num+'</th>'+
							'<th scope="col"><a href=\'#\' onclick=\'removePages("'+divIdName+'")\'>Remove</a></th>'+
							'</tr></thead>'+
							'<tr>'+
								'<td>YouTube ID</td>'+			
								'<td><input type="text" class="widefat" name="'+youtube_id_name+'" value="'+escape(youtube_id)+'"></td>'+					
							'</tr>'+
							'<tr>'+
								'<td>Embed Code</td>'+			
								'<td><textarea class="widefat" name="'+embed_code_name+'" cols="100" rows="10">'+embed_code+'</textarea></td>'+					
							'</tr>'+
							'<tr>'+
								'<td>Headline</td>'+				
								'<td><input type="text" class="widefat" name="'+headline_name+'"></td>'+					
							'</tr>'+
							'<tr>'+
								'<td>Description</td>'+					
								'<td><textarea class="widefat" name="'+desc_name+'" cols="100" rows="10"></textarea></td>'+					
							'</tr>'+
							'</table>';
	  ni.appendChild(newdiv);
	}
	
	function removePages(divNum) {
	  var d = document.getElementById('myDiv');
	  var olddiv = document.getElementById(divNum);
	  d.removeChild(olddiv);
	}	
	</script>	
</div><!-- ends .wrap -->