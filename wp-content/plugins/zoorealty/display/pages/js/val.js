new function() {
         jQuery.fn.validate = {
        init: function(o) {
        if(jQuery(o).hasClass('string')) this.validate_string(o);
         if(jQuery(o).hasClass('email'))  { this.validate_email(o) };
         if(jQuery(o).hasClass('digit'))  this.validate_digit(o);
		 if(jQuery(o).hasClass('spam_question') || jQuery(o).hasClass('children_age') ) doValidate(o);
         if(jQuery(o).attr("id") =="first_night") doValidate(o);
        },
        validate_string: function(o) {
          var valid_chars = /[(\*\(\)\[\]\+\.\,\/\?\:\;\'\"\`\~\\#\$\%\^\&\<\>)+]/;
          if(o.value.length < o.size) { //Validate for a minimum size string based on the size of the input box
          	doError(o,'This field cannot be empty.');          
           }else if (!o.value.match(valid_chars)) {
             doSuccess(o);
            } else {
             doError(o,'No special characters allowed');
            };
        },
        validate_digit: function(o) {
        	//if (o.value.match(/([0-9]{4,10})(\s*)/g) == null) 
        	if (o.value.match(/[^a-zA-Z]([0-9]{8,})/) == null)  
        		doError(o,'Phone number is invalid');
    		else
        		doSuccess(o);
        },
        validate_email: function(o) {
          var email  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
           if (o.value.match(email)) {
              doSuccess(o);
            } else {
              doError(o,'Not a valid email');
            };
        },

        dob: function(o) {
          var dob  = /(0[1-9]|1[012])+\/(0[1-9]|[12][0-9]|3[01])+\/(19|20)\d\d/;
            if (o.value.match(dob)) {
              doSuccess(o);
            } else {
              doError(o,'not a valid date');
            };
        }
     };
     //private helper, validates each type after check
     function doValidate(o) {
        	jQuery(o).parent().addClass("loading");
        	var params = jQuery(".validate :input").serialize() +'&task=' +jQuery(o).attr("class");
        	jQuery.post(jQuery(".validate").attr("action"), params, function(json) {
                  	eval("var args = " + json);
                        if (args.success == true)
                        	doSuccess(o);
                        else
                        	doError(o,args.msg);                  	
                  });//ends $.post
    };
};
	function submit_form(){
		var params = jQuery(".validate :input").serialize() +'&task=process_contact_form';
        jQuery.post(jQuery(".validate").attr("action"), params, function(json) {
        	eval("var args = " + json);
           jQuery(".return").html(args.msg);                   	
                  });//ends $.post
	}
      function doSuccess(o) {
     		jQuery(o).parent().removeClass("loading");
			jQuery(o).parent().removeClass("error");    
     		jQuery(o).parent().addClass("success");			
			jQuery(o).next().html(''); //Removes the error message to the first span or element after the input box
    		
      }
     function doError(o,m) {
		//$('#' + o.name+ '_img').html('<img src="images/exclamation.gif" border="0" style="float:left;" />');
		jQuery(o).parent().removeClass("loading");
		jQuery(o).parent().addClass("error");
		jQuery(o).next().html(m); //Adds the error message to the first span or element after the input box
		jQuery(o).parent().removeClass("success");
		
     }
	jQuery(document).ready(function(){
	 jQuery(".validate :input").blur(function() {
	          jQuery(this).validate.init(this);
	  });	  
	  jQuery(".validate :input").parent().mouseover(function() { //Gets the first element containing each input from a form with class validate
	          jQuery(this).addClass("selected");
	  });
	  jQuery(".validate :input").parent().mouseout(function() {
	          jQuery(this).removeClass("selected");
	  });
	  
	jQuery(".validate").submit( function () {		
		jQuery(".validate :input").trigger("blur");
		if(jQuery(".validate :input").parent().hasClass('error')){	
			jQuery(".return").html('Please correct the errors above before continuing.');		
		}else{
			jQuery(".return").html('  ');		
			jQuery(".return").addClass("loading");
			submit_form();
			jQuery(".return").removeClass("loading");
			jQuery(".validate").get(0).reset();
		}
		return false;
	}); //ends $(".validate").submit

});//ends $(document).ready