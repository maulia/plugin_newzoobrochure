<div id="sidebar">
<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
} if ($sidebar_menu != '') {
 global $wp_query;
if( empty($wp_query->post->post_parent) ) {
$parent = $wp_query->post->ID;
$title = $wp_query->post->post_title;
$parent_id = get_absolute_ancestor($post->ID);
} else {
$parent = $wp_query->post->post_parent;
$parent_id = get_absolute_ancestor($post->ID);
$title = get_the_title($parent_id);
} ?>
<?php if(wp_list_pages("child_of=$parent&echo=0" ) && !is_page('property') && !is_page('print-property')) { ?>
<div id="side_nav" class="side_block">
	<h3><?php echo $title; ?> Menu</h3>
	<ul>
		<?php wp_list_pages("include=$parent_id&title_li="); ?>
		<?php wp_list_pages("child_of=$parent_id&title_li=" ); ?>
	</ul>
</div>
<?php } }
		if (is_page('home')) { 
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Side') ) {} 
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar') ) {} 
			//include('includes/follow.php');
		} elseif (is_home() || is_category() || is_archive() || is_tag() || is_search() || is_single() || is_date()) {
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar') ) {} 
			include('includes/follow.php');
		} elseif (is_page('property')) {
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Page Side') ) {}
		} elseif (is_page('property-research')) {
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Research Sidebar') ) {}
		} elseif (is_page('print-property')) { 		
		}  else {
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Tag Search') ) {} 
			include('includes/follow.php');
		} ?>
	</div><!-- end #sidebar -->