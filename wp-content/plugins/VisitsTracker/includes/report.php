<div class="wrap">
<h2>Visits' Report</h2>
<?php 
if(isset($visits['pagination_label'])){ ?>
<div class="tablenav">
<?php echo $visits['pagination_label']; ?>
</div>
<?php } ?>
<table class="widefat">
	<thead>
	<tr>
		<th scope="col" style="text-align: center">Property ID</th>
        <th scope="col">Suburb</th>
        <th scope="col">Address</th>
        <th scope="col" width="90" style="text-align: center">Week</th>
        <th scope="col" style="text-align: center">Month</th>
		<th scope="col" style="text-align: center">Total</th>
		<!--<th scope="col" style="text-align: center">Total Wespac</th>-->
	</tr>
	</thead>
	<tbody id="the-list">
		<?php foreach ($visits as $visit): if(!is_array($visit))continue; ?>
		<tr>
		<th scope="col" style="text-align: center"><a href="<?php echo $visit['url']; ?>" title="<?php echo $visit['address']; ?>"><?php echo $visit['property_id']; ?></a></th>
        <th scope="col"><?php echo $visit['suburb']; ?></th>
        <th scope="col"><?php echo $visit['address']; ?></th>
        <th scope="col" width="90" style="text-align: center"><?php echo $visit['week_total']; ?> </th>
        <th scope="col" style="text-align: center"><?php echo $visit['month_total']; ?></th>
		<th scope="col" style="text-align: center"><?php echo $visit['grand_total']; ?></th>
		<!--<th scope="col" style="text-align: center"><?php echo $visit['counter']; ?></th>-->
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php if(isset($visits['pagination_label'])){ ?>
<div class="tablenav">
<?php echo $visits['pagination_label']; ?>
</div>
<?php } ?>
</div>