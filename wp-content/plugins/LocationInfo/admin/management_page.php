<div class="wrap">
	<h2><?php echo sprintf(__('%s Location'),$form_title); ?></h2>

	<h3><a href="<?php echo $this->form_action; ?>"><?php echo sprintf(__('&laquo; Back to Location List')); ?></a></h3>

	<form name="" action="<?php if ($task == 'insert_form') echo $this->form_action; ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="project_id" value="<?php echo $_GET['project_id']; ?>" />
		<input type="hidden" name="task" value="<?php echo $task; ?>" />
		<input type="hidden" name="page_id" value="<?php echo $location['page_id']; ?>" />
		<input type="hidden" id="user-id" name="user_ID" value="<?php global $user_ID; echo (int) $user_ID ?>" />
		<p class="submit"><input type="submit" value="<?php echo sprintf(__('%s Location &raquo;'),$form_title); ?>" name="" /></p>
		<div id="poststuff">
			<div id="post-body">

			<div id="locationdiv" class="stuffbox">
				<h3><?php _e('Location Information'); ?></h3>
				<div class="inside">
					<table class="form-table" style="width: 100%;" cellspacing="2" cellpadding="5">

						<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Suburb') ?></label></th>
							<td><input type="text" name="suburb" id="suburb" value="<?php echo $location['suburb']; ?>" /></td>
						</tr>
							<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Population') ?></label></th>
							<td><input type="text" name="population" id="population" value="<?php echo $location['population']; ?>" /></td>
						</tr>
							<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Postcode') ?></label></th>
							<td><input type="text" name="postcode" id="postcode" value="<?php echo $location['postcode']; ?>" /></td>
						</tr>
							<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Council') ?></label></th>
							<td><input type="text" name="council" id="council" value="<?php echo $location['council']; ?>" /></td>
						</tr>
							<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Link') ?></label></th>
							<td><input type="text" name="link" value="<?php echo $location['link']; ?>" /></td>
						</tr>
							<tr class="form-field">
							<th valign="top"  scope="row"><label><?php _e('Description') ?></label></th>
							<td><?php
					$args = array("textarea_name" => "description");
					wp_editor( $location['description'], "content", $args );
					?></td>
						</tr>
					</table>
				</div>
			</div>
			
			<br class="clear">
			<?php
			wp_enqueue_script('media-upload'); 
			wp_enqueue_media(); 	
			wp_print_scripts('jquery-ui-core'); 
			wp_print_scripts('jquery-ui-sortable'); 
			$photos=$location['photos'];
			?>
			<table class="widefat maintable" cellspacing="0">
				<input type="hidden" name="delete_photo" value="1">
				<thead>
				<tr>
					<th colspan="2" scope="col" class="manage-column column-title">Homepage Images (Drag and drop to reorder)<a class="button right" href="javascript:addSlide();" title="Add New Image"><i class="fa fa-plus-circle"></i> Add New Image</a></th>
				</tr>
				</thead> 
				<tr>
					<td colspan="2">
						<div class="image_list" id="photo_list">
						<?php 					
						if(!empty($photos)){
							$theValue=0;
							foreach($photos as $key=>$item){ 
								$theValue++;
								$divIdName='my'.$theValue.'photo_list';
								$image_id='photos'.$theValue;
								if($item['path']){	?>
								<div id="<?php echo $divIdName; ?>">
									<img src="<?php echo $item['path']; ?>" style="max-width:200px"><br/>								
									<div class="uploader">
									<input id="<?php echo $image_id; ?>" name="photos[<?php echo $theValue; ?>][path]" value="<?php echo $item['path']; ?>" type="text" style="width:500px" />
									<input id="<?php echo $image_id; ?>_button" class="button" name="<?php echo $image_id; ?>_button" type="text" value="Upload" /> <a class="del" href="javascript:removeSlide('<?php echo $divIdName; ?>');" title="Delete" class="button">Delete</a>
									</div>
									
								</div>
								<?php }
							} 
						} ?>
						</div>
					</td>
				</tr>
			</table>	
			<p class="submit"><input type="submit" value="<?php echo sprintf(__('%s Location &raquo;'),$form_title); ?>" name="" /></p>
		
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		var _custom_media = true,
		_orig_send_attachment = wp.media.editor.send.attachment;
		 
		jQuery('.uploader .button').click(function(e) {
			var send_attachment_bkp = wp.media.editor.send.attachment;
			var button = jQuery(this);
			var id = button.attr('id').replace('_button', '');
			_custom_media = true;
			wp.media.editor.send.attachment = function(props, attachment){
			if ( _custom_media ) {
			jQuery("#"+id).val(attachment.url);
			} else {
			return _orig_send_attachment.apply( this, [props, attachment] );
			};
			}
			 
			wp.media.editor.open(button);
			return false;
			});
		 
		jQuery('.add_media').on('click', function(){
			_custom_media = false;
		});
		
		jQuery("#photo_list").sortable();
	}); 
	
	var theValue=<?php echo count($photos); ?>;
	
	function addSlide() {
		var ni = document.getElementById('photo_list');
		theValue++;
		var newdiv = document.createElement('div');
		var divIdName = 'my'+theValue+'photo_list';	  
		var image_name = 'photos['+theValue+'][path]';
		var image_id = 'photos'+theValue;
		newdiv.setAttribute('id',divIdName);
		newdiv.innerHTML = 	'<div class="uploader"><input id="'+image_id+'" name="'+image_name+'" type="text" style="width:500px" />'+
							'<input id="'+image_id+'_button" class="button" name="'+image_id+'_button" type="text" value="Upload" /> '+
							'<a class="del" href="javascript:removeSlide(\''+divIdName+'\');" title="Delete" class="button">Delete</a></div>';
		ni.appendChild(newdiv);		
		
		
		jQuery('.uploader .button').click(function(e) {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = jQuery(this);
		var id = button.attr('id').replace('_button', '');
		_custom_media = true;
		wp.media.editor.send.attachment = function(props, attachment){
		if ( _custom_media ) {
		jQuery("#"+id).val(attachment.url);
		} else {
		return _orig_send_attachment.apply( this, [props, attachment] );
		};
		}
		 
		wp.media.editor.open(button);
		return false;
		});
		
		jQuery("#photo_list").sortable();
	}

	function removeSlide(divNum) {
		var d = document.getElementById('photo_list');
		var olddiv = document.getElementById(divNum);
		d.removeChild(olddiv);
		theValue--;
	}	

</script>