<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require($path.'/wp-load.php');
global $helper, $realty, $wpdb;
$value = esc_sql(trim($_REQUEST['q']));
$office_id= implode(",",$realty->settings['general_settings']['office_id']);
$states = $wpdb->get_col("select distinct state from properties where office_id in (".esc_sql($office_id).")");
$state = implode("','",$states);
if(empty($value))$value='default';
$results=array();
if($value=='default'){
	$sql="select distinct state , (Case When state='NSW' Then 'NSW (New South Wales)'
			When state='SA' Then 'SA (South Australia)'
			When state='VIC' Then 'VIC (Victoria)'
			When state='QLD' Then 'QLD (Queensland)'
			When state='WA' Then 'WA (Western Australia)'
			When state='TAS' Then 'TAS (Tasmania)'
			When state='ACT' Then 'ACT (Australian Capital Territory)'
			ELSE state END)as state_name from suburb_region where state in ('$state') order by state limit 10";
	$check_like_state=$wpdb->get_results($sql, ARRAY_A);
	if(!empty($check_like_state)){
		foreach($check_like_state as $item):
			$results[]=$item['state_name'];
		endforeach;
		
	}
}
elseif(is_numeric($value)){	
	$check_like_postcode=$wpdb->get_results("select distinct state, suburb, postcode from suburb_region where postcode like '".intval($value)."%' and state in ('$state') order by suburb limit 10", ARRAY_A);
	if(!empty($check_like_postcode)){
		foreach($check_like_postcode as $item):
			$results[]=$item['suburb'].", ".$item['state'].", ".$item['postcode'];
		endforeach;
	}
	/*
	$check_like_street=$helper->get_results("select distinct street_number, unit_number, street, state, suburb, postcode from properties where (street_number like '$value%' or unit_number like '$value%') and status in (1,4) and office_id in ($office_id) order by street, suburb limit 10");
	if(!empty($check_like_street)){
		foreach($check_like_street as $item):
			$street='';
			if(!empty($item['unit_number']))$street=$item['unit_number'];
			if(!empty($item['street_number']))$street=($street=='')?$item['street_number']:"$street/".$item['street_number'];
			if(!empty($item['street']))$street=($street=='')?$item['street']:"$street ".$item['street'];
			$results[]=$street.", ".$item['suburb'].", ".$item['state'].", ".$item['postcode'];
		endforeach;
	}
	*/
}
else{
	$sql="select distinct state , (Case When state='NSW' Then 'NSW (New South Wales)'
			When state='SA' Then 'SA (South Australia)'
			When state='VIC' Then 'VIC (Victoria)'
			When state='QLD' Then 'QLD (Queensland)'
			When state='WA' Then 'WA (Western Australia)'
			When state='TAS' Then 'TAS (Tasmania)'
			When state='ACT' Then 'ACT (Australian Capital Territory)'
			ELSE state END)as state_name from suburb_region where state like '".esc_sql($value)."%' and status in (1,4) and office_id in ($office_id) order by state limit 10";
	$check_like_state=$wpdb->get_results($sql, ARRAY_A);
	if(!empty($check_like_state)){
		foreach($check_like_state as $item):
			$results[]=$item['state_name'];
		endforeach;
	}
	
	$check_like_region=$wpdb->get_results("select distinct state, town_village from suburb_region where town_village like '".esc_sql($value)."%' and state in ('$state')  order by town_village limit 10", ARRAY_A);
	if(!empty($check_like_region)){
		foreach($check_like_region as $item):
			$results[]=$item['town_village'].", ".$item['state'];
		endforeach;
	}	
	
	$check_like_suburb=$helper->get_results("select distinct state, suburb from suburb_region where suburb like '".esc_sql($value)."%' and state in ('$state')  order by suburb limit 10", ARRAY_A);
	if(!empty($check_like_suburb)){
		foreach($check_like_suburb as $item):
			$results[]=$item['suburb'].", ".$item['state'];
		endforeach;
	}	
	/*
	$check_like_street=$helper->get_results("select distinct street, state, suburb, postcode from properties where street like '$value%' and status in (1,4) and office_id in ($office_id) order by street, suburb limit 10");
	if(!empty($check_like_street)){
		foreach($check_like_street as $item):
			$results[]=$item['street'].", ".$item['suburb'].", ".$item['state'].", ".$item['postcode'];
		endforeach;
	}	
	*/
}
echo json_encode($results);

if( !function_exists('json_encode') ){
	function json_encode($data) {	
		require_once( dirname(__FILE__). '/JSON.php' );
		$json = new Services_JSON();
		return( $json->encode($data) );	
	}
}
?>