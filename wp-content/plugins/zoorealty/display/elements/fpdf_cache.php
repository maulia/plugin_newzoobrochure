<?php
// Nick's FPDF Cache

if (!function_exists('wp_upload_dir'))
	die('FPDF Cache requires Wordpress');

class FPDF_Cache extends FPDF
{
	private $rungc = false;
	
	function Image($file, $x=null, $y=null, $w=0, $h=0, $type='', $link='', $placeholder='')
	{
		if(empty($type)){
			$type = image_type_to_extension(exif_imagetype($file));
			if (preg_match('/jpg|jpeg/i',$type))$type='jpg';
		}
		if(strtolower($type)!='jpg' && $placeholder!='' )$file=$placeholder;
		$orig_file = $file;
		
		
		if (strtolower(substr($file, 0, 7)) == 'http://')
		{
			$upload_dir = wp_upload_dir();
			$path = $upload_dir['basedir'].'/fpdf_cache/';

			if (!file_exists($path))
				mkdir($path);
			
			if (!$this->rungc && rand(1, 50) == 1)
			{
				$files = scandir($path);
				
				foreach ($files as $file)
				{
					if (filemtime($path.$file) < time() - 604800)
						@unlink($path.$file);
				}
				
				$this->rungc = true;
			}
			
			$cache_filename = $path.'cache_'.md5($file);

			if (file_exists($cache_filename) && filesize($cache_filename) == 0)
				@unlink($cache_filename);
			
			if (!file_exists($cache_filename))
			{
				$data = file_get_contents($orig_file);
				
				if (empty($data))
					return false;
				
				file_put_contents($cache_filename, $data);
			}
			
			$file = $cache_filename;
		}
		
		parent::Image($file, $x, $y, $w, $h, $type, $link);
	}
}
