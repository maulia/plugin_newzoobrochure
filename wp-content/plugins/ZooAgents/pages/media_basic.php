<?php
if (!is_user_logged_in())return;
global $helper, $current_user, $realty;
include('menu.php');

$office_id=$helper->get_var("select office_id from users where email='".$current_user->user_email."' and contributor=1");

if(!$office_id)return;

$extra_condition="and office_id in ($office_id)";
$id=$_GET['id'];

$property=$helper->get_results("select * from properties where id = $id $extra_condition");
$property=$property[0];
if(empty($property))return print "Listing ID $id is not exist";

include('menu_listings.php');

$uploaded_path=ABSPATH . 'wp-content/uploads/zooproperty_api/attachments/';
$uploaded_url=$realty->siteUrl.'wp-content/uploads/zooproperty_api/attachments/';
$photos_upload_path=$uploaded_path."photos/$id/";
$photos_upload_url=$uploaded_url."photos/$id/";
$floorplans_upload_path=$uploaded_path."floorplans/$id/";
$floorplans_upload_url=$uploaded_url."floorplans/$id/";
$brochures_upload_path=$uploaded_path."brochures/$id/";
$brochures_upload_url=$uploaded_url."brochures/$id/";

wp_print_scripts('jquery-ui-core');
wp_print_scripts('jquery-ui-sortable');		
		
if(!@file_exists($photos_upload_path)){ @mkdir($photos_upload_path, 0777); authorized_dir($photos_upload_path); }
if(!@file_exists($floorplans_upload_path)){ @mkdir($floorplans_upload_path, 0777);authorized_dir($floorplans_upload_path); }
if(!@file_exists($brochures_upload_path)){ @mkdir($brochures_upload_path, 0777);authorized_dir($brochures_upload_path); }

if($_GET['task']=='delete' && !empty($id)){
	$row=$helper->get_results("select url from attachments where id in(".$_GET['ids'].")");	
	if(!empty($row)){
		foreach($row as $key=>$item){
			if($_GET['type']=='photo')$url=$photos_upload_path.str_replace($photos_upload_url,"",$item['url']);
			else if($_GET['type']=='floorplan')$url=$floorplans_upload_path.str_replace($floorplans_upload_url,"",$item['url']);
			else if($_GET['type']=='brochure')$url=$brochures_upload_path.str_replace($brochures_upload_url,"",$item['url']);
			@unlink($url);
		}
	}
	$helper->query("delete from attachments where id in(".$_GET['ids'].")");
	$row=$helper->get_results("select distinct position from attachments where parent_id=$id and is_user=0 and type='".$_GET['type']."' order by position");
	if(!empty($row)){
		foreach($row as $key=>$item){
			$helper->query("update attachments set position=$key where parent_id=$id and is_user=0 and position=".$item['position']." and type='".$_GET['type']."'");
		}
	}
	echo "<div id='success'><span>".$_GET['type']." deleted.</span></div>";
}

if(isset($_POST['submit'])){
	$photo_upfile = $_FILES['photo']; 
	$photo_upfile = $this->build_input_array($photo_upfile,'size');	
	if(!empty($photo_upfile)){
		foreach($photo_upfile as $photo){
			if($photo['size'] <= 0) continue;	
			if(strpos($photo['type'],'jpeg')===false )$return.="Image `".$photo['name']."` failed to save. Only allow type .jpg<br/>";
			else {
				extract(pathinfo($photo['name']));
				if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 				
				$this->clean_filename($filename);
				$basename =  "$filename.$extension";
				$final_filename = $photos_upload_path . $basename;
				$original_url = $photos_upload_url . $basename;	

				for ($count = 1; file_exists( $final_filename ); $count++): 
					$basename = $filename . "_$count" .  '.' . $extension;
					$final_filename = $photos_upload_path . $basename;	 			
					$original_url = $photos_upload_url . $basename;	 			
				endfor;	
			
				if($this->save_file($photo['tmp_name'], $final_filename)){
					$this->resize($final_filename, 200, 'thumbnail');		
					$this->resize($final_filename, 400, 'medium');		
					$this->resize($final_filename, 800, 'large');		
					extract(pathinfo($final_filename));
					if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
					$thumbnail_url=$photos_upload_url.$filename."_thumb.$extension";
					$medium_url=$photos_upload_url.$filename."_medium.$extension";
					$large_url=$photos_upload_url.$filename."_large.$extension";
					
					$position=$helper->get_var("select max(position) from attachments where parent_id=$id and type='photo'");
					if($position=='')$position=0;
					else $position=$position+1;				
													
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'photo','description'=>'thumb','url'=>$thumbnail_url, 'position'=>$position));
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'photo','description'=>'medium','url'=>$medium_url, 'position'=>$position));
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'photo','description'=>'large','url'=>$large_url, 'position'=>$position));	
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'photo','description'=>'original','url'=>$original_url, 'position'=>$position));					
				}	
			}
		}
	}
	if(!empty($_POST['photo_ids'])){
		foreach($_POST['photo_ids'] as $key=>$ids){
			if($ids)$helper->query("update attachments set position=$key where id in($ids)");
		}
	}	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else echo "<div id='success'><span>Photos updated</span></div>";
}

if(isset($_POST['submit_floor'])){
	$floorplan_upfile = $_FILES['floorplan']; 
	$floorplan_upfile = $this->build_input_array($floorplan_upfile,'size');	
	if(!empty($floorplan_upfile)){
		foreach($floorplan_upfile as $floorplan){
			if($floorplan['size'] <= 0) continue;	
			if(strpos($floorplan['type'],'jpeg')===false && strpos($floorplan['type'],'gif')===false)$return.="Image `".$floorplan['name']."` failed to save. Only allow type .jpg and .gif<br/>";
			else{
				extract(pathinfo($floorplan['name']));
				if(!isset($filename))$filename=str_replace(".".$extension,"",$basename);				
				$this->clean_filename($filename);
				$basename =  "$filename.$extension";
				$final_filename = $floorplans_upload_path . $basename;
				$original_url = $floorplans_upload_url . $basename;	

				for ($count = 1; file_exists( $final_filename ); $count++): 
					$basename = $filename . "_$count" .  '.' . $extension;
					$final_filename = $floorplans_upload_path . $basename;	 			
					$original_url = $floorplans_upload_url . $basename;	 			
				endfor;	
				if($this->save_file($floorplan['tmp_name'], $final_filename)){
					$this->resize($final_filename, 200, 'thumbnail');		
					$this->resize($final_filename, 400, 'medium');		
					$this->resize($final_filename, 800, 'large');	
					extract(pathinfo($final_filename));
					if(!isset($filename))$filename=str_replace(".".$extension,"",$basename); 
					$thumbnail_url=$floorplans_upload_url.$filename."_thumb.$extension";
					$medium_url=$floorplans_upload_url.$filename."_medium.$extension";
					$large_url=$floorplans_upload_url.$filename."_large.$extension";
					
					$position=$helper->get_var("select max(position) from attachments where parent_id=$id and type='floorplan'");
					if($position=='')$position=0;
					else $position=$position+1;				
													
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'thumb','url'=>$thumbnail_url, 'position'=>$position));
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'medium','url'=>$medium_url, 'position'=>$position));
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'large','url'=>$large_url, 'position'=>$position));
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'floorplan','description'=>'original','url'=>$original_url, 'position'=>$position));
				}
			}			
		}		
	}	
	if(!empty($_POST['floorplan_ids'])){
		foreach($_POST['floorplan_ids'] as $key=>$ids){
			if($ids)$helper->query("update attachments set position=$key where id in($ids)");
		}
	}	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else echo "<div id='success'><span>Floorplans updated</span></div>";
}

if(isset($_POST['submit_brochure'])){
	$brochure_upfile = $_FILES['brochure']; 
	$brochure_upfile = $this->build_input_array($brochure_upfile,'size');	
	if(!empty($brochure_upfile)){
		foreach($brochure_upfile as $brochure){
			if($brochure['size'] <= 0) continue;	
			if(strpos($brochure['type'],'pdf')===false)$return.="File `".$brochure['name']."` failed to save. Only allow type .pdf<br/>";
			else{
				extract(pathinfo($brochure['name']));
				if(!isset($filename))$filename=str_replace(".".$extension,"",$basename);				
				$this->clean_filename($filename);
				$basename =  $brochure['name'];
				$final_filename = $brochures_upload_path . $basename;
				$large_url = $brochures_upload_url . $basename;	

				for ($count = 1; file_exists( $final_filename ); $count++): 
					$basename = $filename . "_$count" .  '.' . $extension;
					$final_filename = $brochures_upload_path . $basename;	 			
					$large_url = $brochures_upload_url . $basename;	 			
				endfor;	
				if($this->save_file($brochure['tmp_name'], $final_filename)){								
					$position=$helper->get_var("select max(position) from attachments where parent_id=$id and type='brochure'");
					if($position=='')$position=0;
					else $position=$position+1;				
					
					$this->insert_form("attachments",array('parent_id'=> $id,'is_user'=>0,'type'=>'brochure','description'=>$basename,'url'=>$large_url, 'position'=>$position));
				}	
			}
		}		
	}	
	if(!empty($_POST['brochure_ids'])){
		foreach($_POST['brochure_ids'] as $key=>$ids){
			if($ids)$helper->query("update attachments set position=$key where id in($ids)");
		}
	}	
	if(!empty($return))echo "<div id='return' class='red'>$return</div>";
	else echo "<div id='success'><span>Brochures updated</span></div>";
}

$photos=$realty->photos($id,'photos');
$floorplans=$realty->photos($id,'floorplans');
$brochures=$realty->brochure($id);
?>
<link rel="stylesheet" href="<?php echo $this->pluginUrl.'css/prettyPhoto.css'; ?>" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<?php echo $this->pluginUrl.'js/jquery.prettyPhoto.js'; ?>" type="text/javascript" charset="utf-8"></script>

<div class="listing-media">		
	<p>Click, drag and drop images to change the order, please press the `Save` button after images have been reorded. To delete an image press the red cross in the corner of the image.</p>	
    <form name="update_properties" id="update_properties" action="<?php echo $realty->siteUrl."dashboard/media/?id=$id";?>" method="post" autocomplete="off" enctype="multipart/form-data">
        <div id="photo_div" class="block_content">
            <h4>Photos</h4>
            <p class="info">File type allowed .jpg.</p>
            <ul class="image_list">
                <?php 
                if(!empty($photos)){ 
                    foreach($photos as $key=>$photo):
                    $ids=$helper->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='photo'");
                    $ids=$ids[0]['ids'];
                    ?>
                    <li>			
                        <input type="hidden" name="photo_ids[]" value="<?php echo $ids; ?>">
                        <a href="<?php echo $photo['large']; ?>" rel="prettyPhoto[photo]"><img src="<?php echo $photo['small']; ?>"/></a>
                        <a class="delete-image" onClick="return confirm('You are about to delete the this photo.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=photo&task=delete&id=$id&ids=$ids";?>" title="Delete Image"></a>
                    </li>
                    <?php endforeach; 
                }	?>
            </ul>
            <div class="clear"></div>
            <div id="files_list" class="to-upload"></div>
            <div class="button">
                <span><input type="file" id="photo" name="photo[]" size="99" /></span>
                <span class="last"><input type='submit' name='submit' value='Save' class='btn'/></span>
                <div class="clear"></div>
            </div>
        </div>
    </form>		
    
    <form name="update_properties" id="update_properties" action="<?php echo $realty->siteUrl."dashboard/media/?id=$id";?>" method="post" autocomplete="off" enctype="multipart/form-data">
        <div id="floorplan_div" class="block_content">
            <h4>Floorplans</h4>
            <p class="info">File type allowed .jpg & .gif.</p>
            <ul class="image_list">
                <?php 
                if(!empty($floorplans)){ 
                    foreach($floorplans as $key=>$floorplan):
                    $ids=$helper->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='floorplan'");
                    $ids=$ids[0]['ids'];
                    ?>
                    <li>			
                        <input type="hidden" name="floorplan_ids[]" value="<?php echo $ids; ?>">
                        <a href="<?php echo $floorplan['large']; ?>" rel="prettyPhoto[floorplan]"><img src="<?php echo $floorplan['small']; ?>"/></a>
                        <a class="delete-image" onClick="return confirm('You are about to delete the this floorplan.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=floorplan&task=delete&id=$id&ids=$ids";?>" title="Delete Image"></a>
                    </li>
                    <?php endforeach; 
                }	?>
            </ul>
            <div class="clear"></div>
            <div id="floorplan_list" class="to-upload"></div>
            <div class="button">
                <span><input type="file" id="floorplan" name="floorplan[]" size="99" /></span>
                <span class="last"><input type='submit' name='submit_floor' value='Save' class='btn'/></span>
                <div class="clear"></div>
            </div>
        </div>
    </form>
    
    <form name="update_properties" id="update_properties" action="<?php echo $realty->siteUrl."dashboard/media/?id=$id";?>" method="post" autocomplete="off" enctype="multipart/form-data">
        <div id="brochure_div" class="block_content">
            <h4>Brochures</h4>
            <p class="info">File type allowed .pdf.</p>
            <ul id="brochures">
                <?php 
                if(!empty($brochures)){ 
                    foreach($brochures as $brochure):
                    $ids=$helper->get_results("select GROUP_CONCAT(`id`) as ids from attachments where parent_id=$id and is_user=0 and position=$key and type='brochure'");
                    $ids=$ids[0]['ids'];
                    ?>
                    <li>						
                        <input type="hidden" name="brochure_ids[]" value="<?php echo $ids; ?>">
                        <a href="<?php echo $brochure['url']; ?>" target="blank"><?php echo $brochure['description']; ?></a>
                        <a class="delete-pdf" onClick="return confirm('You are about to delete the this brochure.\n\'OK\' to delete, \'Cancel\' to stop.' );" href="<?php echo $realty->siteUrl."dashboard/media/?type=brochure&task=delete&id=$id&ids=$ids";?>">Delete</a>
                    	<div class="clear"></div>
                    </li>
                    <?php endforeach; 
                }	?>
            </ul>
            <div id="brochure_list" class="to-upload"></div>
            <div class="button">
                <span><input type="file" id="brochure" name="brochure[]" size="99" /></span>
                <span class="last"><input type='submit' name='submit_brochure' value='Save' class='btn'/></span>
                <div class="clear"></div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo $this->pluginUrl;?>js/media.js"></script>