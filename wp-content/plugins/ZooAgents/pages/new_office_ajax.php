<form id="new_office_ajax" class="form-profile" action="" onsubmit="return false;">
	<div class="content-left-column">
		<div class="block_content profile-block">
			<h4>New Office</h4>
			<table>
				<tr>
					<td class="label"><strong>Name<span class="red"> *</span></strong></td>
					<td>
						<input type="text" class="full" id="office_name" name="office_name">						
					</td>
				</tr>
				<tr>
					<td class="label"><strong>State</strong></td>
					<td>
						<input type="text" class="full" id="state" name="state">
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Suburb</strong></td>
					<td>
						<input type="text" class="full" id="suburb" name="suburb">
					</td>
				</tr>				
				<tr>
					<td class="label"><strong>Postcode</strong></td>
					<td>
						<input type="text" class="full" id="postcode" name="zipcode">
					</td>
				</tr>
				<tr>
					<td class="label"><strong>Country</strong></td>
					<td>
						<input type="text" class="full" id="country" name="country">
					</td>
				</tr>
				<tr>
					<td class="label">Unit Number</td>
					<td><input type="text" name="unit_number" class="full"></td>
				</tr>
				<tr>
					<td class="label">Street Number</td>
					<td><input type="text" id="street_number" name="street_number" class="full"></td>
				</tr>
				<tr>
					<td class="label">Street Name/Type</td>
					<td><input type="text" id="street" name="street" class="full"></td>
				</tr>
				<tr>
					<td class="label"><strong>Email<span class="red"> *</span></strong></td>
					<td>
						<input type="text" class="full" id="office_email" name="email">
					</td>
				</tr>
				<tr>
					<td class="label">Phone</td>
					<td><input type="text" name="phone" class="full"></td>
				</tr>
				<tr>
					<td class="label">Fax</td>
					<td><input type="text" id="fax" name="fax" class="full"></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="content-right-column">
		<div id="map_div" class="block_content">
			<h4>Mapping Coordinates</h4>
			<p><strong><a class="submit_geocode" href="javascript:submit_geocode();">Set Map Location</a></strong></p>
			<p>
				<strong>Longitude:<span id="longitude_span" class="value">&nbsp;</span></strong>
				<strong>Latitude:<span id="latitude_span" class="value">&nbsp;</span></strong>
				<input type="hidden" name="longitude" id="longitude" value="">
				<input type="hidden" name="latitude" id="latitude" value="">
			</p>
			<div class="map-wrapper">
				<div id="map_canvas"></div>				
			</div>
		</div>  
	</div>    
	<div class="clear"></div>
    <div class="block_content profile-save">
        <h4>Save</h4>
        <p class="button"><input type="submit" name="submit" value="Save" class="btn"></p>
        <p class="button"><input type="button" name="cancel" value="Cancel" onClick="closeoffice();"></p>
        <div class="clear"></div>
    </div>
</form>	

<script type="text/javascript">
jQuery('#new_office_ajax').submit(function() {
	if(jQuery("#office_name").val()=='')alert('Please fill office name.');
	else if(jQuery("#office_email").val()=='')alert('Please fill office email.');
	else{
		var query = jQuery(this).serialize();
		jQuery.get(new_office_url+query, function(data) {
			alert("New Office successfully created");
			closeoffice();
			jQuery("#office_id").html(data);
		});	
		
	}
	return false;
});
				
/* map */
function submit_geocode(){
	var geocoder = new google.maps.Geocoder();
	var street_number=jQuery('#street_number').val();
	var street=jQuery('#street').val();
	var suburb=jQuery('#suburb').val();
	var state=jQuery('#state').val();
	var postcode=jQuery('#postcode').val();				
	var address = "";

	if(street_number!='')address=street_number+" ";
	if(street!='')address=address+street+" ";
	if(suburb!='')address=address+suburb+" ";
	if(state!='')address=address+state+" ";
	if(postcode!='')address=address+postcode+" ";
	address=address.substr(0,address.length-1); 
	geocoder.geocode( { 'address': address}, function(results, status) {

	if (status == google.maps.GeocoderStatus.OK) {
		var latitude = results[0].geometry.location.lat();
		var longitude = results[0].geometry.location.lng();
		document.getElementById('latitude').value=latitude;
		document.getElementById('latitude_span').innerHTML=latitude;
		document.getElementById('longitude').value=longitude;
		document.getElementById('longitude_span').innerHTML=longitude;
		load();
	}
		
	}); 
}
var myLatLng;
function load() {
	var latitude=jQuery('#latitude').val();		
	var longitude=jQuery('#longitude').val();		
	myLatLng = new google.maps.LatLng(latitude,longitude);
	var myOptions = {
		zoom: 14,
		center:myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};	
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);	
	var Marker = new google.maps.Marker({  position: myLatLng, map: map });
}
</script>