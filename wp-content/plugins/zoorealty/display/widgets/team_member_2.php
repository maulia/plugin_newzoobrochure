<div id="team_member">
	<?php echo $before_title; ?><span>Team</span> Member<?php echo $after_title; ?>
	<div class="block_content">
		<?php
		global $realty;
		$users = $realty->users('','',$settings['Number']);
		foreach($users as $user):	
			if(!is_array($user)) continue;
			?>
			<div class="agent_item agent_item<?php echo $count; ?>">
				<?php if($settings['image']=='1' || isset($settings['image'])) : ?>
				<div class="imageCont">
					<div class="image">
						<?php if($settings['image_mode']=='landscape') : ?>
							<a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo_landscape']; ?>" /></a>

						<?php else: ?>
							<a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo']; ?>" /></a>					
						<?php endif; ?>
					</div>
				</div>
				<?php endif; ?>
				<div class="descCont">
					<div class="tdContent">
					<?php if($settings['name']=='1' || isset($settings['name'])) : ?>
					<p class="agent_name"><span><a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><?php echo $user['name']; ?></a></span></p>
					<?php endif; ?>
					<?php if($settings['telephone']=='1' || isset($settings['telephone'])) : ?>
					<?php if (!empty($user['business_phone'])) { ?><p class="agent_phone"><strong>P:</strong><span>&nbsp;<?php echo $user['business_phone']; ?></span></p><?php } ?>
					<?php endif; ?>
					<?php if($settings['mobile']=='1' || isset($settings['mobile'])) : ?>
					<?php if (!empty($user['mobile'])) { ?><p class="agent_mobile"><strong>M:</strong><span>&nbsp;<?php echo $user['mobile']; ?></span></p><?php } ?>
					<?php endif; ?>
					<?php if($settings['fax']=='1' || isset($settings['fax'])) : if (!empty($user['fax'])) { ?>
					<p class="agent_fax"><strong>F:</strong><span>&nbsp;<?php echo $user['fax']; ?></span></p>
					<?php } endif;?>
					<?php if($settings['role']=='1' || isset($settings['role'])) :if (!empty($user['position_for_display'])) { ?>
					<p class="agent_role"><strong>Role:</strong><span>&nbsp;<?php echo $user['position_for_display']; ?></span></p>
					<?php } endif;?>
					</div><!--ends .tdContent-->
				</div>
			</div>
		<?php endforeach; ?>
		<div class="clearer"></div>
		<p class="button team_member_btn"><a href="<?php echo $realty->team_url;?>" class="btn">View All</a></p>
	</div>
</div>