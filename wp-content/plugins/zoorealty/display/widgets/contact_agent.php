<?php if(!$realty->is_property_page) return; /*check if this is the property page before displaying the widget */ ?>
<div id="agentinfo_contactform">
    <h2 class="section_title">Make an Enquiry</h2>
	<div id="agentinfo">
	<?php 
	for($i=0;$i<=2;$i++){
		$user = $realty->property['user'][$i]; 
		if(empty($user)) continue;
		?>

		<div id="agent_<?php echo $i; ?>" class="agent_info">
			<?php if($settings['photo']): 
			if($settings['image_mode']=='landscape') : ?>
				<p class="agent_photo">
					<?php $agent_image = explode('/',$user['photo_landscape']); if (in_array('default_avatar.jpg', $agent_image)) { ?>
						<a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile" class="agent-image-nolandscape"></a>
					<?php } else { ?>
						<a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo_landscape']; ?>" /></a>
					<?php } ?>
				</p>
			<?php else: ?>
				<p class="agent_photo">
				<a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><img alt="<?php echo $user['name']; ?>" src="<?php echo $user['photo']; ?>" /></a>
				</p>
			<?php endif; endif; ?>
			<div class="agent_contact_info">
				<?php foreach($settings as $key=>$values):if(empty($user[$key])) continue;
				switch($key): 
				 case 'photo_landscape':case 'photo': break ; 
				 case 'name':
				 if($settings['not_agent_link']!='1'): ?>
				<h4><a href="<?php echo $user['url']; ?>" title="View <?php echo $user['name']; ?>'s Profile"><?php echo $user[$key]; ?></a></h4>
				<?php else:?>
				<h4><?php echo $user[$key]; ?></h4>
				<?php endif; 
				 break;
				 case 'business_phone':?>
				<p class="agent_phone"><strong>P:</strong><span>&nbsp;<?php echo $user[$key]; ?></span></p>
				<?php break;
				 case 'mobile':?>
				<p class="agent_mobile"><strong>M:</strong><span>&nbsp;<?php echo $user[$key]; ?></span></p>
				<?php break; 
				case 'email':?>
				<p class="agent_email"><strong>E:</strong><span>&nbsp;<a href="mailto:<?php echo $user[$key]; ?>" title="Email Agent"><?php echo $user[$key]; ?></a></span></p>
				<?php break;	
				case 'fax':?>
				<p class="agent_mobile"><strong>F:</strong><span>&nbsp;<?php echo $user[$key]; ?></span></p>
				<?php break;
				case 'download_vcard':?>
				<p class="agent_vcard"><strong>Download Vcard: </strong><span><a href="<?php echo $user['vcard_path']; ?>" target="_newtab"><?php echo ($user['vcard_title']=='')?'No Title':$user['vcard_title']; ?></a></span></p>
				<?php break;
				 default:?>
				<p><strong><?php echo $key; ?></strong><span>&nbsp;<?php echo $user[$key]; ?></span></p>
				<?php break;
				 endswitch; 
				 endforeach; ?>
				 
				<?php if($settings['not_contact_form']!='1'){ ?>
				<p class="email_agent">	<a href="<?php echo $realty->pluginUrl; ?>display/elements/lightbox_contact_agent.php?ajax=true&width=600&height=430"  rel="ajax" title="Email Agent">Email Agent</a></p>
				<?php } ?>
	
			</div><!-- end .agent_contact_info -->
			<div class="clearer"></div>
		</div><!-- #agent_<?php echo $i; ?> ends-->
		<?php } ?>
	</div><!-- end #agentinfo -->

<div class="clearer"></div>
</div><!-- ends #agentinfo_contactform -->

<script type="text/javascript">
function send_enquiry(){
	var code = jQuery("#securitycode").val();
	var comments = document.getElementById('ccomments').value;	
	var first_name = jQuery("#cfirst_name").val();
	var last_name = jQuery("#clast_name").val(); 
	var email = jQuery("#cemail").val();
	var mobile_phone = jQuery("#cmobile_phone").val();
	var home_phone = jQuery("#chome_phone").val();
	if(first_name=='First Name' || first_name=='')alert("You have entered an invalid first name");
	else if(last_name=='Last Name' || last_name=='')alert("You have entered an invalid last name");
	else if ((email.indexOf('@') < 0) || ((email.charAt(email.length-4) != '.') && (email.charAt(email.length-3) != '.')))alert("You have entered an invalid email address. Please try again.");
	else{
	var url    = "<?php echo $realty->pluginUrl."display/pages/js/contact_agent.php?property_id=".$realty->property['id']."&user_id=".$realty->property['user_id']."&user_id1=".$realty->property['secondary_user']; ?>&code="+escape( code )+"&comments="+escape( comments )+"&first_name="+escape( first_name )+"&last_name="+escape( last_name )+"&email="+escape( email )+"&mobile_phone="+escape( mobile_phone )+"&home_phone="+escape( home_phone );
		jQuery('#return').load(url);
		//alert('Email was successfully sent.'); 
	}	
	return false; 
}
function captchas_image_reload (imgId) 
{
var image_url = document.getElementById(imgId).src;
image_url+= "&";
document.getElementById(imgId).src = image_url;
}
</script>