<?php
$detail['ProjectSale']="
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='date_of_completion' LIMIT 1) AS date_of_completion,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='garage_area' LIMIT 1) AS garage_area,	
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='garage_area_metric' LIMIT 1) AS garage_area_metric,	
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='porch_terrace_area' LIMIT 1) AS porch_terrace_area,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='porch_terrace_area_metric' LIMIT 1) AS porch_terrace_area_metric,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='land_depth' LIMIT 1) AS land_depth,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='land_depth_metric' LIMIT 1) AS land_depth_metric,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='land_width' LIMIT 1) AS land_width,	
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='land_width_metric' LIMIT 1) AS land_width_metric,	
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='project_development_name' LIMIT 1) AS project_development_name,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='new_development_category_name' LIMIT 1) AS new_development_category_name,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='project_design_type_name' LIMIT 1) AS project_design_type_name,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $tbl_properties.id and meta_key='project_style_name' LIMIT 1) AS project_style_name,
	(SELECT meta_value FROM property_meta WHERE property_meta.property_id = $id and meta_key='estimate_rental_return_period' LIMIT 1) AS estimate_rental_return_period,	
	";
?>