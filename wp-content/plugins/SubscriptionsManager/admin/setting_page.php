<?php //metadata: access_level=8 
global $helper, $wpdb; wp_enqueue_script('media-upload');	wp_enqueue_media(); ?>
<div class="wrap">
	<h2><?php _e('Settings'); ?></h2>
	<form method="post">
		<input type="hidden" value="reinstall" name="task" />
		<p><input onclick="return confirm('Are you sure you want to reinstall table ?\nAll subscribers data will be clear.\n\'OK\' to confirm, \'Cancel\' to stop.' );" type="submit" class="button-primary" value="<?php _e('Reinstall Table'); ?>"/><p>
	</form>
	<?php
	if(isset($_POST['submit_pass'])){
		//list refferer
		$list=array();
		$list=explode(",",$_POST['list_referrer']);
		$wpdb->query("delete from list_referrers");
		for($i=0;$i<count($list);$i++){
			$wpdb->query("insert into list_referrers values('','".ucwords(strtolower(trim(esc_sql($list[$i]))))."')");
		}
		
		if(!update_option('subscription_manager_included_categories',$_POST['included_categories']))add_option('subscription_manager_included_categories',$_POST['included_categories']);
		if(!update_option('is_use_password',$_POST['is_use_password']))add_option('is_use_password',$_POST['is_use_password']);
		if(!update_option('spam_filter',$_POST['spam_filter']))add_option('spam_filter',$_POST['spam_filter']);
		if(!update_option('sm_days_limit',$_POST['sm_days_limit']))add_option('sm_days_limit',$_POST['sm_days_limit']);
		if(!update_option('included_new_alert',$_POST['included_new_alert']))add_option('included_new_alert',$_POST['included_new_alert']);
		
		if(!empty($_POST['sm_property_types'])){
			$_POST['sm_property_types']=str_replace(", ",",",$_POST['sm_property_types']);
			$_POST['sm_property_types']=explode(",",$_POST['sm_property_types']);
			sort($_POST['sm_property_types']);
			$_POST['sm_property_types']=array_unique($_POST['sm_property_types']);
			$_POST['sm_property_types']=ucwords(strtolower(implode(", ",$_POST['sm_property_types'])));
		}		
		if(!update_option('sm_property_types',$_POST['sm_property_types']))add_option('sm_property_types',$_POST['sm_property_types']);
		
		if(!empty($_POST['sm_suburbs'])){
			$_POST['sm_suburbs']=str_replace(", ",",",$_POST['sm_suburbs']);
			$_POST['sm_suburbs']=explode(",",$_POST['sm_suburbs']);
			sort($_POST['sm_suburbs']);
			$_POST['sm_suburbs']=array_unique($_POST['sm_suburbs']);
			$_POST['sm_suburbs']=ucwords(strtolower(implode(", ",$_POST['sm_suburbs'])));
		}		
		if(!update_option('sm_suburbs',$_POST['sm_suburbs']))add_option('sm_suburbs',$_POST['sm_suburbs']);
		
		if(!update_option('sm_email_user_format',$_POST['sm_email_user_format']))add_option('sm_email_users_format',$_POST['sm_email_users_format']);
		if(!update_option('sm_email_admin_format',$_POST['sm_email_admin_format']))add_option('sm_email_admin_format',$_POST['sm_email_admin_format']);
		if(!update_option('sm_logo',$_POST['sm_logo']))add_option('sm_logo',$_POST['sm_logo']);
		if(!update_option('sm_bed',$_POST['sm_bed']))add_option('sm_bed',$_POST['sm_bed']);
		if(!update_option('sm_bath',$_POST['sm_bath']))add_option('sm_bath',$_POST['sm_bath']);
		if(!update_option('sm_cars',$_POST['sm_cars']))add_option('sm_cars',$_POST['sm_cars']);
		if(!update_option('sm_bg_color',$_POST['sm_bg_color']))add_option('sm_bg_color',$_POST['sm_bg_color']);
		if(!update_option('sm_heading_color',$_POST['sm_heading_color']))add_option('sm_heading_color',$_POST['sm_heading_color']);
		$message_pass = 'Settings Updated.';
	}
	$sm_days_limit=get_option('sm_days_limit');
	$included_new_alert=get_option('included_new_alert');
	$is_use_password=get_option('is_use_password');
	$spam_filter=get_option('spam_filter');
	$sm_email_user_format=get_option('sm_email_user_format');
	$sm_email_admin_format=get_option('sm_email_admin_format');
	$sm_logo=get_option('sm_logo');
	$sm_bed=get_option('sm_bed');
	$sm_bath=get_option('sm_bath');
	$sm_cars=get_option('sm_cars');
	$sm_bg_color=get_option('sm_bg_color');
	$sm_heading_color=get_option('sm_heading_color');
	$list_alerts = $this->get_site_alerts();
	$all_alerts = $this->get_site_alerts(true);
	$news_alerts = $this->get_news_alerts();
	$all_list=$this->all_list_referrers();
	foreach($all_list as $item_value)$all_list_referer.=$item_value['name'].", ";  
	$all_list_referer=substr($all_list_referer,0,-2);
	
	$suburbs=get_option('sm_suburbs');
	if(empty($suburbs)){
		$office_id =esc_sql(implode(', ', $realty->settings['general_settings']['office_id']));
		$list_suburbs = $wpdb->get_col("select distinct suburb from properties where suburb!='' and office_id IN ($office_id) order by suburb ");
		$suburbs=implode(", ",$list_suburbs);	
	}
	
	$property_types=get_option('sm_property_types');
	if(empty($property_types)){
		$property_types="Acreage/Semi-Rural, Alpine, Apartment, Block of Units, Flat, House, Land, Semi Detached, Serviced Apartment, Studio, Terrace, Townhouse, Unit, Villa, Warehouse, Retirement, Rural";
	}
	if(!empty($message_pass)):
		?><div id="message_pass" class="updated fade"><p><strong><?php echo $message_pass; ?></strong></p></div><?php
	endif;
	?>
	<form  action="" method="post">
		<table class="widefat">
			<tr class="alternate">
				<td>Property Alerts</td>
				<td>
					<ul class="site_alerts">
						<?php foreach($all_alerts as $key=>$item_value):?>
							<li><input type="checkbox" value="<?php echo $item_value; ?>" name="included_categories[<?php echo $key; ?>]"<?php if( is_array($list_alerts) and array_key_exists($key,$list_alerts) ) echo ' checked="checked"'; ?>><?php echo $item_value; ?></li>
						<?php endforeach; ?>
					</ul>
				</td>
			</tr>
			<tr>
				<td>News Alerts</td>
				<td>
					<ul class="site_alerts">
						<?php foreach($news_alerts as $key=>$item_value):?>
							<li><input type="checkbox" value="<?php echo $item_value; ?>" name="included_categories[<?php echo $key; ?>]"<?php if( is_array($list_alerts) and array_key_exists($key,$list_alerts) ) echo ' checked="checked"'; ?>><?php echo $item_value; ?></li>
						<?php endforeach; ?>
					</ul>
				</td>
			</tr>
			<tr class="alternate">
				<td>How did you find us?</td>
				<td><textarea  name="list_referrer" class="widefat"><?php echo $all_list_referer;?></textarea></td>
			</tr>
			<tr>
				<td>Property Type List</td>
				<td><textarea name="sm_property_types" class="widefat" rows="5"><?php echo $property_types;?></textarea></td>
			</tr>
			<tr>
				<td>Suburb List</td>
				<td><textarea name="sm_suburbs" class="widefat" rows="5"><?php echo $suburbs;?></textarea></td>
			</tr>
			<tr class="alternate">
				<td>Display listing in the past n days ( eg: 1 for 1 days, 2 for 2 days, 7 for 7 days, etc)</td>
				<td><input type="text" name="sm_days_limit" value="<?php echo $sm_days_limit ?>"></td>
			</tr>
			<tr>
				<td>Send News and Property Alerts in the same email</td>
				<td>
					<input type="radio" name="included_new_alert" <?php if ($included_new_alert=='1') echo 'checked="checked"'; ?> value="1">Yes
					<span style="margin-left:10px"><input type="radio" name="included_new_alert" <?php if ($included_new_alert=='') echo 'checked="checked"'; ?> value="">No</span>
				</td>
			</tr>
			<tr class="alternate">
				<td>Use password in user authentication and user registration</td>
				<td>
					<input type="radio" name="is_use_password" <?php if ($is_use_password=='1') echo 'checked="checked"'; ?> value="1">Yes
					<span style="margin-left:10px"><input type="radio" name="is_use_password" <?php if ($is_use_password=='') echo 'checked="checked"'; ?> value="">No</span>
				</td>
			</tr>
			<tr>
				<td>Include Captcha Spam Filter</td>
				<td><input type="radio" name="spam_filter" <?php if ($spam_filter=='1') echo 'checked="checked"'; ?> value="1">Yes
				<span style="margin-left:10px"><input type="radio" name="spam_filter" <?php if ($spam_filter=='') echo 'checked="checked"'; ?> value="">No</span></td>
			</tr>
			<tr class="alternate">
				<td>Format Email to Users</td>
				<td>
					<select name="sm_email_user_format">
						<option value="html" <?php if ($sm_email_user_format=='html') echo 'selected="selected"'; ?>>HTML</option>
						<option value="text" <?php if ($sm_email_user_format=='text') echo 'selected="selected"'; ?>>Plain Text</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Format Email to Admin</td>
				<td>
					<select name="sm_email_admin_format">
						<option value="html" <?php if ($sm_email_admin_format=='html') echo 'selected="selected"'; ?>>HTML</option>
						<option value="text" <?php if ($sm_email_admin_format=='text') echo 'selected="selected"'; ?>>Plain Text</option>
					</select>
				</td>
			</tr>
			<tr class="alternate">
				<td>Logo</td>
				<td>		
					<div class="uploader">
						<?php
						if($sm_logo){
						?>
						<img src="<?php echo $sm_logo; ?>" style="max-width:500px"><br/>
						<?php
						} ?>
						<input id="sm_logo" name="sm_logo" value="<?php echo $sm_logo; ?>" type="text" style="width:400px" />
						<input id="sm_logo_button" class="button" name="sm_logo_button" type="text" value="Upload" />
					</div> 					
				</td>
			</tr>
			<tr>
				<td>Bed icons</td>
				<td>		
					<div class="uploader">
						<?php
						if($sm_bed){
						?>
						<img src="<?php echo $sm_bed; ?>" style="max-width:500px"><br/>
						<?php
						} ?>
						<input id="sm_bed" name="sm_bed" value="<?php echo $sm_bed; ?>" type="text" style="width:400px" />
						<input id="sm_bed_button" class="button" name="sm_bed_button" type="text" value="Upload" />
					</div> 	
				</td>
			</tr>
			<tr class="alternate">
				<td>Bath icons </td>
				<td>		
					<div class="uploader">
						<?php
						if($sm_bath){
						?>
						<img src="<?php echo $sm_bath; ?>" style="max-width:500px"><br/>
						<?php
						} ?>
						<input id="sm_bath" name="sm_bath" value="<?php echo $sm_bath; ?>" type="text" style="width:400px" />
						<input id="sm_bath_button" class="button" name="sm_bath_button" type="text" value="Upload" />
					</div> 	
				</td>
			</tr>
			<tr>
				<td>Cars icons</td>
				<td>		
					<div class="uploader">
						<?php
						if($sm_cars){
						?>
						<img src="<?php echo $sm_cars; ?>" style="max-width:500px"><br/>
						<?php
						} ?>
						<input id="sm_cars" name="sm_cars" value="<?php echo $sm_cars; ?>" type="text" style="width:400px" />
						<input id="sm_cars_button" class="button" name="sm_cars_button" type="text" value="Upload" />
					</div> 	
				</td>
			</tr>
			<tr class="alternate">
				<td>Background Color <em>( in hex format, e.g : #ffffff )</em></td>
				<td><input type="text" name="sm_bg_color" value="<?php echo $sm_bg_color;?>"><?php if(!empty($sm_bg_color))echo "<span style='width:25px;border:1px solid;background-color:$sm_bg_color !important;'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"; ?></td>
				
			</tr>
			<tr>
				<td>Heading Color <em>( in hex format, e.g : #ffffff )</em></td>
				<td><input type="text" name="sm_heading_color" value="<?php echo $sm_heading_color;?>"><?php if(!empty($sm_heading_color))echo "<span style='width:25px;border:1px solid;background-color:$sm_heading_color !important;'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"; ?></td>
			</tr>
		</table>
		<p><input class="button" name="submit_pass" type="submit" value="Save" name=""  /></p>
	</form>
	<br/>
	
	<?php
	if(isset($_POST['submit_page'])){
		
		foreach($this->pages as $post_title=>$post_content):
			foreach($this->display_element_page as $value=>$item):
				$arr = $post_title.'_'.$value;		
				if(!update_option($post_title.'_'.$value,$_REQUEST[$arr]))	add_option($post_title.'_'.$value,$_REQUEST[$arr]);
			endforeach;
		endforeach;
		$request = $_POST + $_GET ; //var_dump($request);
		extract($request);
		
		$add_subscribers='';
			if(!empty($subscribe_add_subscribers))$add_subscribers = explode(",",$subscribe_add_subscribers);
			if(!update_option('subscribe_add_subscribers',$add_subscribers))add_option('subscribe_add_subscribers',$add_subscribers);
			
			$add_subscribers='';
			if(!empty($contact_us_add_subscribers))$add_subscribers = explode(",",$contact_us_add_subscribers);
			if(!update_option('contact_us_add_subscribers',$add_subscribers))add_option('contact_us_add_subscribers',$add_subscribers);
				
			$add_subscribers='';
			if(!empty($sales_appraisal_add_subscribers))$add_subscribers = explode(",",$sales_appraisal_add_subscribers);
			if(!update_option('sales_appraisal_add_subscribers',$add_subscribers))add_option('sales_appraisal_add_subscribers',$add_subscribers);
			
			$add_subscribers='';
			if(!empty($management_appraisal_add_subscribers))$add_subscribers = explode(",",$management_appraisal_add_subscribers);
			if(!update_option('management_appraisal_add_subscribers',$add_subscribers))add_option('management_appraisal_add_subscribers',$add_subscribers);		

		$return_page = 'Pages Updated.';
	}
	if(!empty($return_page)){
		?><div id="message" class="updated fade"><p><strong><?php echo $return_page; ?></strong></p></div><?php
	}
	?>

	<h2>Wordpress Pages</h2>
	<form name="form_setting_page_manager" action="" method="post">
		<input type="hidden" value="" name="task" />
		<table class="widefat">
			<thead><tr>
				<?php foreach($this->pages as $post_title=>$post_content):?>
				<th scope="col" width="25%"><?php echo $helper->humanize($post_title); ?></th>
				<?php endforeach; ?>
			</tr>
			</thead>
			<tbody>
			<tr>
				<?php foreach($this->pages as $post_title=>$post_content):?>
				<td>
				<?php $i=0;foreach($this->display_element_page as $value=>$item): if($post_title=='subscribe' && $value=='issubscribe')continue;?>
				<p <?php if ($i%2==0) echo 'class="alternate"'; ?>><input type="checkbox" value='1' name="<?php echo $post_title.'_'.$value; ?>" <?php if(get_option($post_title.'_'.$value)=='1') echo "checked"; ?>><?php echo $item; ?></p>
				<?php $i++;endforeach; ?>
				<p <?php if ($i%2==0) echo 'class="alternate"'; ?>>Email (emails separated by comma)</p>
				<textarea name="<?php echo $post_title;?>_add_subscribers" cols="25" rows="3"><?php echo (get_option($post_title.'_add_subscribers')=='')?'':implode(",",get_option($post_title.'_add_subscribers')); ?></textarea>
				</td>
				<?php endforeach; ?>
			</tr></tbody>
		</table>
		<p><input class="button" name="submit_page" type="submit" value="Save" name=""  /></p>
	</form>

	<?php
	function page_wp_insert_post($post, $unique=false){
		global $helper;
		if(empty($post['post_type'])) $post['post_type'] = 'page';
		$postID = wp_insert_post(array( 'post_status' => 'publish', 'post_type' => $post['post_type'], 'post_title'=>$helper->humanize($post['post_title']), 'post_content'=>$post['post_content'], 'post_parent'=>$post['post_parent']));

		update_post_meta($postID, '_permanent_name',  $helper->underscore($post['post_title']));
		if(!empty($post['post_template']))update_post_meta($postID, '_wp_page_template', $post['post_template']);
		if(is_array($post['custom'])){
			foreach ($post['custom'] as $meta_key=>$meta_value)update_post_meta($postID, $meta_key, $meta_value);
		}
		return $postID;		
	}
		
	if(isset($_POST['submit'])){
		$pages=$_POST['page'];
		if(!empty($pages)){
			foreach($pages as $key=>$page){
				if(!empty($page['page_title'])){
					$post_title=strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', str_replace(" ", "_",str_replace("-", "_", $page['page_title']))));
					$post_title=str_replace("__","_",$post_title);
				}
				else { $post_title=$page['post_title']; }
				$post_parent=$page['parent_page'];		
				if(!empty($post_title)){ 
					foreach($this->pages as $page_title=>$post_content){
						if($page_title==$page['post_title'])page_wp_insert_post(array('post_title'=>$post_title, 'post_content'=>$post_content,'post_parent'=>$post_parent), true);		
					}					
				}
			}
			$return="Pages created.";
		}
		else $return="No pages created.";
	}

	if(isset($_POST['submit_update'])){
		$pages=$_POST['page_ids'];
		if(!empty($pages)){
			foreach($pages as $key=>$post_id){
				wp_delete_post($post_id);				
			}
			$return="Pages deleted.";
		}
		else $return="No pages deleted.";
	}

	if(!empty($return)){
		?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
	}

	$parent_pages=$wpdb->get_results("select ID, post_title, post_parent from $wpdb->posts where post_status='publish' and post_type='page' and post_parent='0' order by post_title", ARRAY_A);
	$pages=$wpdb->get_results("select ID, post_title, post_parent, post_content from $wpdb->posts where post_status='publish' and post_type='page' and post_parent='0' order by post_title", ARRAY_A); 
	$existing_pages=array();
	$i=0;
	foreach( $pages as $page ){	
		$existing_pages[$i]=$page;
		$j=$i;$existing_pages[$j]['display']=1;if(strpos($page['post_content'],'[subscriptions_manager_plugin:')===false)$existing_pages[$j]['display']=0;	
		$i++;	
		$children_pages=$wpdb->get_results("select ID, post_title, post_parent,post_content from $wpdb->posts where post_status='publish' and post_type='page' and post_parent=".$page['ID']." order by post_title", ARRAY_A);
		if(!empty($children_pages)){
			foreach( $children_pages as $children_page ){
				if(strpos($children_page['post_content'],'[subscriptions_manager_plugin:')!==false)$existing_pages[$j]['display']=1;
				$existing_pages[$i]=$children_page;
				$existing_pages[$i]['display']=1;if(strpos($children_page['post_content'],'[subscriptions_manager_plugin:')===false)$existing_pages[$i]['display']=0;	
				$i++;
				$children_pages_2=$wpdb->get_results("select ID, post_title, post_parent,post_content from $wpdb->posts where post_status='publish' and post_type='page' and post_parent=".$children_page['ID']." order by post_title", ARRAY_A);
				if(!empty($children_pages_2)){
					foreach( $children_pages_2 as $children_page_2 ){
						if(strpos($children_page_2['post_content'],'[subscriptions_manager_plugin:')!==false)$existing_pages[$j]['display']=1;
						$existing_pages[$i]=$children_page_2;
						$existing_pages[$i]['display']=1;if(strpos($children_page_2['post_content'],'[subscriptions_manager_plugin:')===false)$existing_pages[$i]['display']=0;	
						$i++; 
					}
				}
			}
		}
	}
	?>
	<select id="defaultpage" style="display:none;">
		<?php foreach($this->pages as $post_title=>$post_content){ ?>
		<option value="<?php echo $post_title; ?>"><?php echo $helper->humanize($post_title); ?></option>
		<?php } ?>
	</select>
	<select id="parent_page" style="display:none;">
		<option value="">Main parent</option>
		<?php foreach($parent_pages as $parent_page){ ?>
		<option value="<?php echo $parent_page['ID']; ?>"><?php echo $parent_page['post_title']; ?></option>
		<?php } ?>
	</select>
	
	<br/>	
	<h2>Create Default Pages</h2>
	<p><a href="javascript:;" onclick="addPages();">Add New</a></p>	
	<table class="widefat">
		<thead>
		<tr>
			<th scope="col" width="20%">Page</th>
			<th scope="col" width="40%">Title ( Default page title will be the same as chosen page )</th>			
			<th scope="col" width="30%">Parent</th>			
			<th scope="col" width="10%">Remove</th>			
		</tr>
		</thead>
	</table>	
	<input type="hidden" value="0" id="theValue" />
	<form name="" method="post" action="">
	<div id="myDiv"></div>
	<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</form>
	
	<h2>Existing Pages</h2>
	<p>Check the pages to delete</p>
	<form name="" method="post" action="">
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Delete'); ?>" name="submit_update" /></p> 
		<table class="widefat">
			<thead>
			<tr>
				<th scope="col"><input type="checkbox" value="1" id="all_ids" onClick="check_all();"></th>
				<th scope="col">Title</th>			
				<th scope="col">Permalink</th>			
			</tr>
			</thead>
			<?php $i=0;foreach($existing_pages as $page): if(!is_array($page))continue; $permalink=get_permalink($page['ID']);
			if(!empty($page['display'])){ ?> 
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
				<td>
				<?php if(strpos($page['post_content'],'[subscriptions_manager_plugin:')!==false){ ?>
				<span style="margin-left:4px">&nbsp;</span><input type="checkbox" name="page_ids[]" value="<?php echo $page['ID']; ?>">
				<?php } ?>
				</td> 
				<td><?php if($page['post_parent']!='0')echo '<span style="margin-left:10px">-&nbsp;&nbsp;</span>'; ?><a href="<?php echo $permalink; ?>" target="_blank"><?php echo $page['post_title']; ?></a></td>	
				<td><a href="<?php echo $permalink; ?>" target="_blank"><?php echo $permalink; ?></a></td>
			</tr> 
			<?php $i++; } endforeach; ?>
		</table> 	
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Delete'); ?>" name="submit_update" /></p> 
	</form>
	
	<script type="text/javascript">
	jQuery(document).ready(function(){
		var _custom_media = true,
		_orig_send_attachment = wp.media.editor.send.attachment;
		 
		jQuery('.uploader .button').click(function(e) {
			var send_attachment_bkp = wp.media.editor.send.attachment;
			var button = jQuery(this);
			var id = button.attr('id').replace('_button', '');
			_custom_media = true;
			wp.media.editor.send.attachment = function(props, attachment){
			if ( _custom_media ) {
			jQuery("#"+id).val(attachment.url);
			} else {
			return _orig_send_attachment.apply( this, [props, attachment] );
			};
			}
			 
			wp.media.editor.open(button);
			return false;
			});
		 
		jQuery('.add_media').on('click', function(){
			_custom_media = false;
		});
		
	}); 
	
	var default_page = document.getElementById('defaultpage').innerHTML;
	var parent_page = document.getElementById('parent_page').innerHTML;
	
	function addPages() {
	  var ni = document.getElementById('myDiv');
	  var numi = document.getElementById('theValue');
	  var num = (document.getElementById('theValue').value -1)+ 2;
	  numi.value = num;
	  var newdiv = document.createElement('div');
	  var divIdName = 'my'+num+'Div';	  
	  var default_page_name = 'page['+num+'][post_title]';
	  var page_title_name = 'page['+num+'][page_title]';
	  var parent_page_name = 'page['+num+'][parent_page]';
	  newdiv.setAttribute('id',divIdName);
	  newdiv.innerHTML = 	'<table class="widefat">'+
							'<tr>'+
							'<td width="20%"><select name="'+default_page_name+'">'+default_page+'</select></td>'+
							'<td width="40%"><input type="text" size="30" name="'+page_title_name+'"></td>'+
							'<td width="30%"><select name="'+parent_page_name+'">'+parent_page+'</select></td>'+
							'<td><a href=\'javascript:void(0)\' onclick=\'removePages("'+divIdName+'")\'>Remove</a></td>'+
							'</tr>'+
							'</table>';
	  ni.appendChild(newdiv);
	}
	
	function removePages(divNum) {
	  var d = document.getElementById('myDiv');
	  var olddiv = document.getElementById(divNum);
	  d.removeChild(olddiv);
	}
	</script>
	
	
	<h2>Short Codes Pages</h2>
	<table class="widefat">
		<thead>
		<tr>
			<th>Page</th>
			<th>Short Codes</th>
		</tr>
		</thead>
		<tr>
			<td>Subscribe</td>
			<td>[subscriptions_manager_plugin: template:subscribe]</td>
		</tr>
		<tr>
			<td>Sales Appraisal</td>
			<td>[subscriptions_manager_plugin: template:sales_appraisal]</td>
		</tr>
		<tr>
			<td>Management Appraisal</td>
			<td>[subscriptions_manager_plugin: template:management_appraisal]</td>
		</tr>
		<tr>
			<td>Contact Us</td>
			<td>[subscriptions_manager_plugin: template:contact_us]</td>
		</tr>
		<tr>
			<td>Unsubscribe</td>
			<td>[subscriptions_manager_plugin: template:unsubscribe]</td>
		</tr>
	</table>
	
	<br/>
	<h2>Cron Jobs (Run at 23:45)</h2>
	<table class="widefat">
		<thead><tr>
			<th>Task</th>
			<th>Minute *</th>
			<th>Hour *</th>
			<th width="50">Day of the Month *</th>
			<th>Month *</th>
			<th width="50">Day of the Week *</th>
			<th>Command *</th>
		</tr>
		</thead>
		<tr>
			<td>cPanel</td>
			<td>0</td><td>10</td><td>*</td><td>*</td><td>*</td><td><?php echo $this->plugin_url.'/cron_job.php'; ?></td>
		</tr>
	</table>
</div>