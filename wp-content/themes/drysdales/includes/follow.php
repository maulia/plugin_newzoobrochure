<?php 
global $options; 
foreach ($options as $value) { 
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); } 
} 
?>

<?php
$networks = array(
	// array("id" => "facebook", "value" => $facebook, "text" => 'Join our Facebook Fan Club'),
	// array("id" => "twitter", "value" => $twitter, "text" => 'Follow us on Twitter and receive property updates and news and media releases'),
	// array("id" => "linkedin", "value" => $linkedin, "text" => 'Join us on Linkedin'),
    // array("id" => "instagram", "value" => $instagram, "text" => 'Join us on Instagram'),
	// array("id" => "youtube", "value" => $youtube, "text" => 'View our channel at Youtube')
    array("id" => "facebook", "value" => $facebook),
    array("id" => "twitter", "value" => $twitter),
    array("id" => "linkedin", "value" => $linkedin),
    array("id" => "instagram", "value" => $instagram),
    array("id" => "youtube", "value" => $youtube)
);

// if (($facebook != '') || ($twitter != '') || ($digg != '') || ($myspace != '') || ($linkedin != '') || ($youtube != '')) { 
if (($facebook != '') || ($twitter != '') || ($instagram != '') || ($linkedin != '') || ($youtube != '')) { ?>

<div id="follow" class="block side_block">
    <?php /*  <h4 class="title">Follow</h4> */ ?>
    <div class="wrap">
    	<ul>
        <?php foreach ($networks as $network) { 
			if ($network['value'] != '') { 
				// echo '<li class="'.$network['id'].'"><a href="'.$network['value'].'" target="_blank" title="'.$network['id'].'">'.$network['text'].'</a></li>';?>
                <li class="<?php echo $network['id'];?> network<?php echo $count;?> left">
            <a href="<?php echo $network['value']; ?>" title="<?php echo $network['id'];?>" class="<?php echo $network['id']; ?>" target="_blank"><span class="circle"><i class="fa fa-<?php echo $network['id'];?>"></i></span></a></li>
            <?php
			} 
		} ?>
        </ul>
	</div>
</div>
<?php }
/*
    $networks = array(
        array("id" => "facebook", "value" => $facebook),
        array("id" => "twitter", "value" => $twitter),
        array("id" => "digg", "value" => $digg),
        array("id" => "myspace", "value" => $myspace),
        array("id" => "linkedin", "value" => $linkedin),
        array("id" => "instagram", "value" => $instagram),
        array("id" => "google-plus", "value" => $googleplus),
        array("id" => "youtube", "value" => $youtube)
    );
if (($facebook != '') || ($twitter != '') || ($digg != '') || ($myspace != '') || ($linkedin != '') || ($youtube != '')) { 
    if (($facebook != '') || ($twitter != '') || ($instagram != '') || ($linkedin != '') || ($youtube != '')) { ?>
<div id="follow" class="side_block"> 
    <h3>Follow</h3> 
    <ul id="follow_buttons">
    <?php $count=1; foreach ( $networks as $network ) { ?>
        <?php if ($network['value'] != '') { ?>
        <li class="<?php echo $network['id'];?> network<?php echo $count;?>">
            <a href="<?php echo $network['value']; ?>" title="<?php echo $network['id'];?>" class="<?php echo $network['id']; ?>" target="_blank"><span class="circle"><i class="fa fa-<?php echo $network['id'];?>"></i></span></a></li><?php } ?>
        <?php $count++; } ?>
    </ul> 
</div> 
<?php } ?>
<div id="subscribe_rss" class="block side_block">
    <h4 class="title">Subscribe</h4>
    <div class="wrap">
        <p>Stay informend subscribe to our weekly updates.</p>
        <ul>
            <li class="email_news"><a href="<?php echo get_option('home'); ?>/subscribe" title="News Alert">News &amp; Listing Alerts</a></li>
            <li class="rss_sales"><a href="<?php bloginfo('url'); ?>/?rss=sale" title="RSS Sales">RSS Sales</a></li>
            <li class="rss_rentals"><a href="<?php bloginfo('url'); ?>/?rss=lease" title="RSS Rentals">RSS Rentals</a></li>
            <li class="rss_news"><a href="<?php bloginfo('rss2_url'); ?>" title="RSS News">RSS News</a></li>
        </ul>
    </div>
</div> 
<?php } ?>
 <?php
         if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Static Sidebar')) {
            
         }
    */    
?>
