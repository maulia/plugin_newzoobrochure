<?php 
	$pages = array(5,6,13,147,2); 
	echo '<ul class="nav-bottom">';
	foreach ($pages as $page) {
		echo '<li class="page_item page-item-'.$page.' li_parent"><a class="a_parent" href="'.get_permalink($page).'">'.get_the_title($page).'</a>';
		$page_child = wp_list_pages("child_of=$page&echo=0&title_li=");
		if ($page_child) { echo '<ul>'.$page_child.'</ul>'; }
		echo '</li>';
	}
	echo '</ul>';
?>
