<?php $sociable_known_sites = Array(

	'BlinkList' => Array(
		'favicon' => 'blinklist.png',
		'url' => 'http://www.blinklist.com/index.php?Action=Blink/addblink.php&amp;Url=PERMALINK&amp;Title=TITLE',
	),

	'Brightkite' => Array(
		'favicon' => 'brightkite.png',
		'url' => 'http://www.brightkite.com/',
	),
	
	'del.icio.us' => Array(
		'favicon' => 'delicious.png',
		'url' => 'http://delicious.com/post?url=PERMALINK&amp;title=TITLE&amp;notes=EXCERPT',
	),

	'Design Float' => Array(
		'favicon' => 'design_float.png',
		'url' => 'http://www.designfloat.com/submit.php?url=PERMALINK&amp;title=TITLE',
	),

	'Digg' => Array(
		'favicon' => 'digg.png',
		'url' => 'http://digg.com/submit?phase=2&amp;url=PERMALINK&amp;title=TITLE&amp;bodytext=EXCERPT',
		'description' => 'Digg',
	),
	
	'Dopplr' => Array(
		'favicon' => 'dopplr.png',
		'url' => 'http://www.dopplr.com/',
	),

	'Email' => Array(
		'favicon' => 'email.png',
		'url' => 'mailto:?subject=TITLE&amp;body=PERMALINK',
		'awesm_channel' => 'mailto',
		'description' => __('E-mail this story to a friend!','sociable'),
	),

	'Facebook' => Array(
		'favicon' => 'facebook.png',
		'awesm_channel' => 'facebook-post',
		'url' => 'http://www.facebook.com/share.php?u=PERMALINK&amp;t=TITLE',
	),

	'Feed' => Array(
		'favicon' => 'feed.png',
		'url' => 'FEEDLINK',
	),
	
	'Flickr' => Array(
		'favicon' => 'flickr.png',
		'url' => 'http://www.flickr.com/',
	),
	
	'FriendFeed' => Array(
		'favicon' => 'friendfeed.png',
		'url' => 'http://www.friendfeed.com/share?title=TITLE&amp;link=PERMALINK',
	),
	
	'Furl' => Array(
		'favicon' => 'furl.png',
		'url' => 'http://www.furl.com/',
	),

	'Gamespot' => Array(
		'favicon' => 'gamespot.png',
		'url' => 'http://www.gamespot.com/',
	),

	'Lastfm' => Array(
		'favicon' => 'lastfm.png',
		'url' => 'http://www.lastfm.com/',
	),
	
	'LinkedIn' => Array(
		'favicon' => 'linkedin.png',
		'url' => 'http://www.linkedin.com/shareArticle?mini=true&amp;url=PERMALINK&amp;title=TITLE&amp;source=BLOGNAME&amp;summary=EXCERPT',
	),

	'Magnolia' => Array(
		'favicon' => 'magnolia.png',
		'url' => 'http://www.magnolia.com/',
	),
	
	'Mixx' => Array(
		'favicon' => 'mixx.png',
		'url' => 'http://www.mixx.com/submit?page_url=PERMALINK&amp;title=TITLE',
	),
	
	'MySpace' => Array(
		'favicon' => 'myspace.png',
		'awesm_channel' => 'myspace',
		'url' => 'http://www.myspace.com/Modules/PostTo/Pages/?u=PERMALINK&amp;t=TITLE',
	),

	'NewsVine' => Array(
		'favicon' => 'newsvine.png',
		'url' => 'http://www.newsvine.com/_tools/seed&amp;save?u=PERMALINK&amp;h=TITLE',
	),

	'Posterous' => Array(
		'favicon' => 'posterous.png',
		'url' => 'http://posterous.com/share?linkto=PERMALINK&amp;title=TITLE&amp;selection=EXCERPT',
	),
	
	'Reddit' => Array(
		'favicon' => 'reddit.png',
		'url' => 'http://reddit.com/submit?url=PERMALINK&amp;title=TITLE',
	),

	'Sphere' => Array(
		'favicon' => 'sphere.png',
		'url' => 'http://www.sphere.com/search?q=sphereit:PERMALINK&amp;title=TITLE',
	),

	'Sphinn' => Array(
		'favicon' => 'sphinn.png',
		'url' => 'http://sphinn.com/index.php?c=post&m=submit&link=PERMALINK',
	),

	'Stumble' => Array(
		'favicon' => 'stumble.png',
		'url' => 'http://www.stumbleupon.com/submit?url=PERMALINK&amp;title=TITLE',
	),

	'Technorati' => Array(
		'favicon' => 'technorati.png',
		'url' => 'http://technorati.com/faves?add=PERMALINK',
	),
	
	'Tripadvisor' => Array(
		'favicon' => 'tripadvisor.png',
		'url' => 'http://tripadvisor.com/',
	),

	'Tumblr' => Array(
		'favicon' => 'tumblr.png',
		'url' => 'http://www.tumblr.com/share?v=3&amp;u=PERMALINK&amp;t=TITLE&amp;s=EXCERPT',
	),

	'Twitter' => Array(
		'favicon' => 'twitter.png',
		'awesm_channel' => 'twitter',
		'url' => 'http://twitter.com/home?status=TITLE%20-%20PERMALINK',
	),

	'Vimeo' => Array(
		'favicon' => 'vimeo.png',
		'url' => 'http://www.vimeo.com/',
	),
	
	'Youtube' => Array(
		'favicon' => 'youtube.png',
		'url' => 'http://www.youtube.com/',
	),
);
?>
<?php /* if (!empty($settings['display'])): ?>
<?php echo $before_title; ?><span>Share</span> and Enjoy<?php echo $after_title; ?>
<div id="share_enjoy">
<table cellpadding="0" cellspacing="0" class="share_enjoy">
<?php unset($settings['temp']); $i=0;
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
$permalink=curPageURL();
global $post;
foreach($settings as $type=>$values):
	foreach($values as $key=>$value):
		foreach($sociable_known_sites as $item=>$item_value):
			if($key==$item){$url=$item_value['url'];$favicon=$item_value['favicon'];}
			$url=str_replace("TITLE",$post->post_title,$url);
			$url=str_replace("PERMALINK",$permalink,$url);
		endforeach;
		if ($i %3== 0): ?>
<tr<?php echo ($alt_class = ($alt_class==' class="alt"') ? '':' class="alt"'); ?>>
		<?php endif; ?>
<td><span><a href="<?php echo $url;?>" target='_newtab'><img src="<?php echo get_option('siteurl').'/wp-content/plugins/Realty/manage/widgets/images/'.$favicon;?>" /><?php echo $key; ?></a></span></td>
	<?php if ($i %3== 2): ?>
	</tr>
	<?php endif; ?>
<?php $i++;endforeach;endforeach;echo "</table></div>";endif; */ ?> 

<?php if (!empty($settings['display'])): ?>
	<?php echo $before_title; ?><span>Share</span> and Enjoy<?php echo $after_title; ?>
    <div id="share_enjoy">
		<?php unset($settings['temp']);
        function curPageURL() {
         $pageURL = 'http';
         if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
         $pageURL .= "://";
         if ($_SERVER["SERVER_PORT"] != "80") {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
         } else {
          $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
         }
         return $pageURL;
        }
        $permalink=curPageURL();
        global $post;
        foreach($settings as $type=>$values):
            foreach($values as $key=>$value):
                foreach($sociable_known_sites as $item=>$item_value):
                    if($key==$item){$url=$item_value['url'];$favicon=$item_value['favicon'];}
                    $url=str_replace("TITLE",$post->post_title,$url);
                    $url=str_replace("PERMALINK",$permalink,$url);
                endforeach;
		?>
        <p>
            <span><img src="<?php echo get_option('siteurl').'/wp-content/plugins/Realty/manage/widgets/images/'.$favicon;?>" /></span>
            <span><a href="<?php echo $url;?>" target='_newtab'><?php echo $key; ?></a></span>
        </p>
        <?php endforeach;endforeach; ?>
        <div class="clear"></div>
    </div>
<?php endif; ?>