<div class="wrap">
<h2>Page' Report</h2>

<table class="widefat">
	<thead>
	<tr>
		<th scope="col" style="text-align: center" width="500">Link</th>
		<th scope="col" style="text-align: center">Total</th>
	</tr>
	</thead>
	<tbody id="the-list">
		<tr>
        <th scope="col" width="90" style="text-align: center">www.westpac.com.au at investassured.com.au/about-the-portfolio/ </th>
		<th scope="col" style="text-align: center"><?php echo get_option('counter_westpac'); ?></th>
		</tr>
	</tbody>
</table>

<h2>Visits' Report</h2>

<table class="widefat">
	<thead>
	<tr>
		<th scope="col" style="text-align: center">Property ID</th>
        <th scope="col">Suburb</th>
        <th scope="col">Address</th>
        <th scope="col" width="90" style="text-align: center">Week</th>
        <th scope="col" style="text-align: center">Month</th>
		<th scope="col" style="text-align: center">Total</th>
		<th scope="col" style="text-align: center">Total Wespac</th>
	</tr>
	</thead>
	<tbody id="the-list">
		<?php foreach ($visits as $visit): ?>
		<tr>
		<th scope="col" style="text-align: center"><a href="<?php echo $visit['url']; ?>" title="<?php echo $visit['address']; ?>"><?php echo $visit['property_id']; ?></a></th>
        <th scope="col"><?php echo $visit['suburb']; ?></th>
        <th scope="col"><?php echo $visit['address']; ?></th>
        <th scope="col" width="90" style="text-align: center"><?php echo $visit['week_total']; ?> </th>
        <th scope="col" style="text-align: center"><?php echo $visit['month_total']; ?></th>
		<th scope="col" style="text-align: center"><?php echo $visit['grand_total']; ?></th>
		<th scope="col" style="text-align: center"><?php echo $visit['counter']; ?></th>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>