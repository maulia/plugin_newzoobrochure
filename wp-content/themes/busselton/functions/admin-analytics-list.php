<?php
if(isset($_POST['submit'])):
	$office_ids=$_POST['office_ids'];
	if(!update_option('office_ids_report',$office_ids))add_option('office_ids_report',$office_ids);
	$return='Offices Saved.';
endif;
if(isset($_POST['submit_emails'])):
	if(!update_option('emails_to_summary_report', explode(",", $_POST['emails_to_summary_report'])))add_option('emails_to_summary_report', explode(",", $_POST['emails_to_summary_report']));
	$return='Email Saved.';
endif;
$emails_to_summary_report=get_option('emails_to_summary_report');
if(!is_array($emails_to_summary_report))$emails_to_summary_report=array();
$office_ids=get_option('office_ids_report');
if(!is_array($office_ids))$office_ids=array();
if(!empty($return)):
	?><div id="message" class="updated fade"><p><strong><?php echo $return; ?></strong></p></div><?php
endif;
?>		
<div class="leftside">
	<form method="post" action="themes.php?page=theme-settings&tab=analytics">
		<table class="widefat">
			<thead>
			<tr>
				<th scope="col"><input type="checkbox" value="1" id="all_ids" onClick="check_all();"></th>
				<th scope="col">Office ID</th>			
				<th scope="col">Name</th>			
				<th scope="col">Address</th>	
				<th scope="col">Report</th>	
			</tr>
			</thead>
			<?php $i=0;foreach($offices as $office): if(!is_array($office))continue;  ?> 
			<tr <?php if ($i%2==0) echo 'class="alternate"'; ?>> 
				<td><span style="margin-left:4px">&nbsp;</span><input type="checkbox" name="office_ids[]"  <?php if (in_array($office['id'], $office_ids)) echo 'checked="checked" '; ?>  value="<?php echo $office['id']; ?>"></td> 
				<td><?php echo $office['id']; ?></td>	
				<td><?php echo $office['name']; ?></td>	
				<td><?php echo $office['address']; ?></td>	
				<td>
					<a href="themes.php?page=theme-settings&tab=analytics&action=view_weekly&id=<?php echo $office['id']; ?>">View Weekly Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="themes.php?page=theme-settings&tab=analytics&action=view_summary&id=<?php echo $office['id']; ?>">View Summary Report</a>
				</td>
			</tr> 
			<?php $i++;endforeach; ?>
		</table> 	
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save'); ?>" name="submit" /></p> 
	</form>

	<script src="<?php echo $zoo_analytics->plugin_url; ?>admin/base_datepicker.js" type="text/javascript"></script>
	<link href="<?php echo $zoo_analytics->plugin_url; ?>admin/calender.css" media="screen" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
	var calendar_img="<?php echo $zoo_analytics->plugin_url; ?>admin/cal.gif";
	</script>
	<form name="form_dynamic_report" id="form_dynamic_report" action="" method="get">
		<input type="hidden" name="page" value="<?php echo $_GET['page']; ?>">
		<input type="hidden" name="tab" value="<?php echo $_GET['tab']; ?>">
		<input type="hidden" id="dr_action" name="action" value="<?php echo ($_GET['task']=='')?'view_report':''; ?>">
		<input type="hidden" id="dr_task" name="task" value="<?php echo $_GET['task']; ?>">
		<table class="widefat">
			<thead>
			<tr>
				<th scope="col" width="15%">Date From</th>			
				<th scope="col" width="15%">Date To</th>			
				<th scope="col">Action</th>	
			</tr>
			</thead>
			<tr> 
				<td><input type="text" id="date_from" name="date_from" size="10" value="<?php echo $_GET['date_from']; ?>"/></td>	
				<td><input type="text" id="date_to" name="date_to" size="10" value="<?php echo $_GET['date_to']; ?>"/></td>	
				<td>
					<select id="action_form" onchange="process_dynamic_report(this.value)">
						<option value="view_report">View Report</option>
						<option value="send_report_email" <?php if($_GET['task']=='send_report_email')echo 'selected=selected'; ?>>Sent Report to Certain Email</option>
						<option value="send_report_agent" <?php if($_GET['task']=='send_report_agent')echo 'selected=selected'; ?>>Sent Report to Agent</option>
					</select>
					<input id="dr_email" <?php if($_GET['task']!='send_report_email')echo 'style="display:none;"'; ?> type="text" name="email" size="75" value="<?php echo $_GET['email']; ?>"/>
				</td>
			</tr> 
		</table> 	
		<p class="submit"><input type="submit" class="button-primary" value="GO" name="submit" /></p> 
	</form>

	<form action="themes.php?page=theme-settings&tab=analytics" method="post">
		<table class="widefat maintable" cellspacing="0">
			<thead>
			<tr>
				<th colspan="3" scope="col" class="manage-column column-title">Settings</th>
			</tr>
			</thead> 
			<tr>
				<th class="rowheader"><strong>Emails to notify</strong></th>
				<td class="field" colspan="2">
					<input type="text" name="emails_to_summary_report" value="<?php echo implode(",", $emails_to_summary_report); ?>" size="75">
				</td>
			</tr>
		</table>
		<p class="submit">
		<input name="submit_emails" type="submit" value="Save changes" />
		</p>
	</form>
</div>

<div id="poststuff">
	<div class="postbox">
	<h3>Actions</h3>

	<div class="inside">
	<p>
		<a href="themes.php?page=theme-settings&tab=analytics&task=send_weekly_reports" class="button">Send Weekly Reports</a>
	</p>
	<p>
		<a href="themes.php?page=theme-settings&tab=analytics&task=send_summary_reports" class="button">Send Summary Reports</a>
	</p>
	</div>

	<div class="clear"></div>
	</div>
</div>

<script type="text/javascript">
function check_all(){				
	var max = document.getElementsByName('office_ids[]').length;		
	for(var idx = 0; idx < max; idx++){
		if(eval("document.getElementById('all_ids').checked") == true)document.getElementsByName('office_ids[]')[idx].checked = true;
		else document.getElementsByName('office_ids[]')[idx].checked = false;
	}
} 

function process_dynamic_report(task){		
	if(task=='view_report'){
		document.getElementById('dr_action').value='view_report';
		document.getElementById('dr_task').value='';
		document.getElementById('dr_email').style.display='none';			
	}
	if(task=='send_report_email'){
		document.getElementById('dr_action').value='';
		document.getElementById('dr_task').value='send_report_email';				
		document.getElementById('dr_email').style.display='';				
	}
	if(task=='send_report_agent'){
		document.getElementById('dr_action').value='';
		document.getElementById('dr_task').value='send_report_agent';
		document.getElementById('dr_email').style.display='none';		
	}			
} 
</script>