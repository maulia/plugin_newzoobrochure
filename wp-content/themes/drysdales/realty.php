<?php
/* Template Name: Realty */
global $options, $realty;
foreach ( $options as $value ) {
    if ( get_option( $value['id'] ) === FALSE ) {
        $$value['id'] = $value['std'];
    } else {
        $$value['id'] = get_option( $value['id'] );
    }
}
get_header();
?>
<div class="content">
    <?php /* <div class="homepage-header">
        <div class="homepage-header-wrap">
            <div class="homepage-image">
                <?php (dynamic_sidebar( 'Feature Slider' )) ? '' : ''; ?>
            </div>
            <div class="homepage-search">
                <?php get_sidebar(); ?>
            </div>
            <div class="clear"></div>
        </div>
        
    </div> */ ?>
    <div class="search_wrap">
        <h2>Property Search</h2>
        <div class="register">
        	<a href="<?php bloginfo( 'url' ); ?>/subscribe">register to view new listings</a>
        </div>
        <div class="clear"></div>
        <?php (dynamic_sidebar( 'Tag Search' )) ? '' : ''; ?>
    </div>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="post<?php echo (is_page( 'team' ) || is_page( 'about-us' )) ? ' agents' : ''; ?>" id="post-<?php the_ID(); ?>">

                <?php
                if ( (is_page( 'team' ) || is_page( 'about-us' )) && $_GET['u'] ) {
                    global $realty;
                    $user = $realty->user( $_GET['u'], true );
                    ?>
                    <h1 class="page-title"><?php echo $user['name']; ?><span class="agent-title"><?php echo $user['position_for_display']; ?></span></h1>
                    <?php
                } 
				else if (is_page('buying') || is_page('buying-mackay')) { 
					$text_buying = 'Buying';
					if (isset($_GET['office_id']) && $_GET['office_id'] == '13952') $text_buying .= ' Mackay';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '13607') $text_buying .= ' Townsville';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '14366') $text_renting .= ' Brisbane';
				?>
					<h1 class="page-title"><?php echo $text_buying; ?>.</h1>
				<?php 
				}
				else if (is_page('renting') || is_page('renting-mackay') || is_page('renting-brisbane')) { 
					$text_renting = 'Renting';
					if (isset($_GET['office_id']) && $_GET['office_id'] == '13952') $text_renting .= ' Mackay';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '13607') $text_renting .= ' Townsville';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '14366') $text_renting .= ' Brisbane';
				?>
					<h1 class="page-title"><?php echo $text_renting; ?>.</h1>
				<?php 
				}
				else {
                    ?>
                    <h1 class="page-title"><?php the_title(); ?></h1>
                <?php } ?>

                <div class="entry">
				<?php 
				if (is_page('buying') || is_page('buying-mackay')) { 
					$text = 'Townsville & Mackay';
					if (isset($_GET['office_id']) && $_GET['office_id'] == '13952') $text = 'Mackay';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '13607') $text = 'Townsville';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '14366') $text = 'Brisbane';
				?>
					<h2>Buying Real Estate Railway Estate <?php echo $text; ?>.</h2>
				<?php 
				}
				else if (is_page('renting') || is_page('renting-mackay') || is_page('renting-brisbane')) { 
					$text = 'Townsville & Mackay';
					if (isset($_GET['office_id']) && $_GET['office_id'] == '13952') $text = 'Mackay';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '13607') $text = 'Townsville';
					else if (isset($_GET['office_id']) && $_GET['office_id'] == '14366') $text = 'Brisbane';
				?>
					<h2>Property Management Railway Estate <?php echo $text; ?>.</h2>
				<?php 
				}
				?>
				<?php the_content( '<p class="serif">Read the rest of this page &raquo;</p>' ); ?>
				</div>
            </div>
            <?php
        endwhile;
    endif;
    ?>
</div>

<?php
//get_sidebar(); 
get_footer();
?>
