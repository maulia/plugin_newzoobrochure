<?php //metadata: access_level=8 ?>
<script type="text/javascript">
//<!--
function open_query_form(element){
	if(element.value != 'sale' && element.value != 'lease')
		return;
	if(element.checked==1)
		document.getElementById(element.value + '_search').style.display='block';
	else
		document.getElementById(element.value + '_search').style.display='none';
}

//-->
</script>
<div class="wrap">
<form name="form_subscriptions_manager" method="post" action="<?php echo $this->form_action; ?>">
<input type="hidden" name="task" id="task" value="form_subscriptions_manager" />
<h2><?php _e('Quick Add'); ?></h2>
<?php if (get_option('is_use_password')=='1'): ?>
<p style="font-size:10px;">Users added here will be issued with the default password (<?php echo $this->default_password; ?>).</p>
<?php endif ;?>
E-mails (comma separated) <br />
<textarea name="add_subscribers" cols="80"><?php echo $add_subscribers; ?></textarea><br class="clearer"/><br class="clearer"/>
<!--<input size="80" type="text" name="add_subscribers" value="<?php echo $add_subscribers; ?>" />-->
<div class="site_alerts">
		<?php foreach($list_alerts as $key=>$item_value):?>
			<span><input type="checkbox" value="<?php echo $key; ?>" name="alert[]"<?php if(isset($this->subscriber[$key])) echo ' checked="checked"'; ?> onclick="open_query_form(this)">
			<?php echo $item_value; ?></span><br />
			<?php echo $this->get_form_query_string($key);
	endforeach; ?>
</div>
<br class="clearer"/>
<input class="button" type="submit" value="Add" name="" />
</form>
</div>