<script type="text/javascript">
	var siteurl="<?php echo get_option('siteurl'); ?>";
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->pluginUrl; ?>map/map.css" media="screen, projection, print"/>
<script type="text/javascript" src="http://www.google.com/jsapi?key=<?php  echo $this->settings['general_settings']["google_maps_key"]; ?>"></script>
<script type="text/javascript">google.load("maps","2");</script>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>map/markermanager_packed.js"></script>
<script type="text/javascript" src="<?php echo $this->pluginUrl; ?>map/jquery.hub.mapsearch.js"></script>
<input type="hidden" id="siteurl" value="<?php echo get_option('siteurl'); ?>">
<script type="text/javascript">
	//Initialize google map			
	jQuery(document).ready(function(){
		jQuery('#map').hubMapSearch({
			listingTable:'#mapResultsTableBody',
			searchForm: '#filter'
		});
		<?php if(!empty($_GET)){ ?>
		document.getElementById('submit_search').click();
		<?php } ?>	
	});
	function suburb_type(state) {
		var siteurl=jQuery('#siteurl').val();	
		var url = siteurl + "/wp-content/plugins/Realty/display/pages/js/quick_search_ajax.php?cat=suburb2&state="+escape( state );
		jQuery('#select_suburb').load(url);	
	}
	</script>	
</script>
<div id="search" class="thickBorderBottom">
	<h2>Search</h2>
    <div id="searchStuff" class="thickBorderBottom">
		<?php
		$list=($_GET['list']=='')?$this->params['list']:$_GET['list'];
		if(empty($list))$list='sale';
		$type = (strpos($list, 'lease') !== false)? 'lease': 'sale';
		$price_options = ($type=='sale' || $type=='listing_sold')?
		array(
		"500000"=>"500k","750000"=>"750k","1000000"=>"1m","2000000"=>"2m","5000000"=>"5m"):
		array(
		"150"=>"150 pw","200"=>"200 pw","250"=>"250 pw","300"=>"300 pw","350"=>"350 pw","400"=>"400 pw","450"=>"450 pw","500"=>"500 pw","600"=>"600 pw","700"=>"700 pw","800"=>"800 pw","900"=>"900 pw","1000"=>"1,000 pw","1500"=>"1,500 pw","2000"=>"2,000 pw","5000"=>"5,000 pw","10000"=>"10,000 pw"
		);
		$max_price = end($price_options);

		if($this->renderer=='sold')$status='2,6';
		$property_types = $this->property_search('property_type',$list,'',$status);
		$suburbs = $this->property_search('suburb',$list,$_GET['state'],$status);
		$states = array('ACT','NSW','QLD','SA','TAS','VIC','WA');
		?>
		<div id="filterContainer">
			<div class="fastfind">
				<script language="javascript" type="text/javascript">
					function clearText() {
						if(document.fastfind.keywords.value  == "Property ID, Postcode, Street, Suburb")
						document.fastfind.keywords.value="";
					}
				</script>
				<form name="fastfind" method="get" action="" id="fastfind">
					<p>
						<label for="qsInput">Fast Find</label>
						<input name="keywords" type="text"  id="qsinputbox" value="<?php echo ($_GET['keywords']=='')?'Property ID, Postcode, Street, Suburb':$_GET['keywords'] ; ?>" onclick="Javascript:clearText();"/>
						<input type="submit" value="Go" class="button go" />
					</p>
				</form>
				<div class="clearer"></div>
			</div>
			<div id="gmapsearch">
				<form id="filter" name="filter" action="" onsubmit="return false;">
					<input name="keywords" type="hidden" value="<?php echo $_GET['keywords']; ?>"/>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td class="opt1">
								<table cellpadding="0" cellspacing="0">							  
									<tr>
										<td width="100">State</td>
										<td>
											<select name="state" id="select_state" onChange="suburb_type(this.value);">
												<option value="">Any</option>
												<?php foreach ($states as $state):  ?>
													<option value="<?php echo $state; ?>"<?php if($state==$_GET['state']) echo ' selected="selected"'; ?>><?php echo $state; ?></option>
												<?php endforeach; ?>
											</select>
										</td>
									</tr>
									<?php if(strpos($this->params['list'],'/')===false): ?>
									<tr>
										<td width="100">Type</td>
										<td>
											<select name="property_type" id="select_property_type">
												<option value="">Any</option>
												<?php foreach($property_types as $property_type): if(!is_array($property_type)) continue;	?>
												<option value="<?php echo $property_type['tag']; ?>"<?php if( $property_type['tag']==$_GET['property_type']) echo ' selected="selected"'; ?>><?php echo $property_type['tag']; ?></option>
												<?php endforeach; ?>
											</select>
										</td>
									</tr>
                                    <tr>
                                    	<td colspan="2">&nbsp;</td>
                                    </tr>
									<?php endif; ?>									
								</table>
							</td>
							<td class="opt2">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td width="75" valign="top">Suburb</td>
										<td>
											<select name="suburb" id="select_suburb">
												<option value="">Any</option>
												<?php foreach ($suburbs as $suburb): if(!is_array($suburb)) continue; ?>
													<option value="<?php echo $suburb['tag']; ?>"<?php if($suburb['tag']==$_GET['suburb']) echo ' selected="selected"'; ?>><?php echo $suburb['tag']; ?></option>
												<?php endforeach; ?>
											</select>
										</td>
									</tr>
									<tr>
										<td width="100">From</td>
										<td>
											<select name="price_min" id="price_min">
												<option value="">Price</option>
												<?php foreach($price_options as $price_option=>$price_value):  ?>
												<option value="<?php echo $price_option; ?>"<?php if( $price_option ==$_GET['price_min']) echo ' selected="selected"'; ?>><?php echo $realty->params['default_currency_code'].$price_value; if($price_value ==$max_price) echo '+'; ?></option>
												<?php endforeach;?>
											</select>
										</td>
									</tr>
									<tr>
										<td width="100">To</td>
										<td>
											<select name="price_max" id="price_max">
												<option value="">Price</option>
												<?php foreach($price_options as $price_option=>$price_value): ?>
												<option value="<?php echo $price_option; ?>"<?php if( $price_option ==$_GET['price_max']) echo ' selected="selected"'; ?>><?php echo $realty->params['default_currency_code'].$price_value; if($price_value ==$max_price) echo '+'; ?></option>
												<?php endforeach;?>
											</select>
										</td>
									</tr>							
								</table>
							</td>
							<td class="opt3">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td width="100">Bed</td>
										<td>
											<select name="bedrooms" class="bedrooms">
												<option value="">Any</option>
												<?php for ( $i = 1 ; $i <= 5; ++$i ) : ?>
												<option value="<?php echo $i ?>"<?php if( $i==$_GET['bedrooms']) echo ' selected="selected"'; ?>><?php echo $i;?>+</option>
												<?php endfor;	?>
											</select>
										</td>
									</tr>
									<tr>
										<td width="100">Bath</td>
										<td>
											<select name="bathrooms" class="bathrooms">
												<option value="">Any</option>
												<?php for ( $i = 1 ; $i <= 5; ++$i ) : ?>
												<option value="<?php echo $i ?>"<?php if( $i==$_GET['bathrooms']) echo ' selected="selected"'; ?>><?php echo $i;?>+</option>
												<?php endfor;	?>
											</select>
										</td>
									</tr>
									<tr>
										<td width="100">Cars</td>
										<td>
											<select name="carspaces" class="carspaces">
												<option value="">Any</option>
												<?php for ( $i = 1 ; $i <= 5; ++$i ) : ?>
												<option value="<?php echo $i ?>"<?php if( $i==$_GET['carspaces']) echo ' selected="selected"'; ?>><?php echo $i;?>+</option>
												<?php endfor;	?>
											</select>
										</td>
									</tr>							
								</table>
							</td>
							<?php /* <td class="optSubmit"><input type="submit" id="submit_search" value="Search" class="button"/></td> */ ?>
						</tr>
					</table>
                    <div class="optSubmit"><input type="submit" id="submit_search" value="Search" class="button"/></div>
                    <div class="clearer"></div>
				</form>
            </div>

		</div>
	</div>	
	<h2>Map Search</h2>

	<div class="clearer"></div>
	<!--gmap wraps the map and sidebar-->
	<div id="gmapwrapper">		
		<div class="loading"> <!--This empty container for loading message-->
			<div id="map"></div>
			<div id="mapInstructions">
				<div class="backgroundOverlay"></div>
				<div class="instructionsText">
					<div class="top"><p class="left"></p><p class="centre"></p><p class="right"></p></div>

						<div class="main"><div class="instructionContent">
							<h4>Instructions</h4>
							<p> 1) To view properties on Map Search, select your search criteria above and click "search".<br/><br/>
								2) To view more property details, click the icons on the map.
							</p>
						</div>
					</div>
					<div class="bottom"><p class="left"></p><p class="centre"></p><p class="right"></p></div>

				</div>
			</div>
		</div>
		<div id="gmapsidebar">	
			<div class="property">
				<p class="property-image"><img src="<?php echo $this->pluginUrl; ?>map/empty.gif" alt="Property Image" id="sidebar_image" style="display: none"/></p>
                <h3 id="sidebar_address"></h3>
				<p>                     
					<span id="sidebar_property_type"></span>	
					<span id="sidebar_price"></span>
                </p>
				<p id="sidebar_features"> 
                    <span id="sidebar_featurebed"></span>	
                    <span id="sidebar_featurebath"></span>
                    <span id="sidebar_featurecar"></span>
                </p>				
				<p id="sidebar_description"></p>
				<input type="hidden" name="property_lat" id="property_lat"></input>
				<input type="hidden" name="property_lng" id="property_lng"></input>
			</div>
		</div>
		<div class="clearer"></div>
	</div>		

    <!--gmap legend-->

	<div class="gmaplegend">
		<ul>
			<li class="first"><img src="<?php echo $this->pluginUrl; ?>map/marker-residential.gif" alt="Residential" />Residential</li>
			<li><img src="<?php echo $this->pluginUrl; ?>map/marker-commercial.gif" alt="Commercial" />Commercial</li>
			<li><img src="<?php echo $this->pluginUrl; ?>map/marker-business.gif" alt="Business" />Business</li>
			<li><img src="<?php echo $this->pluginUrl; ?>map/marker-project.gif" alt="Project" />Project</li>			
		</ul>
	</div>	
    
	<!--bottom container to display the property list-->
    
	<div id="resultsTable">
		<table id="mapResultsTable">				
			<thead>
				<tr>				
					<th>&nbsp;</th>	
					<th>Address</th>					
					<th>Price</th>

					<th>Bed</th>
					<th>Bath</th>
					<th>Parking</th>
					<th>Property Type</th>
				</tr>
			</thead>
			<tbody id="mapResultsTableBody">

				<tr>			
					<td></td>		
					<td></td>					
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>

		</table>		
	</div>
	
	<div class="clearer"></div>
</div>