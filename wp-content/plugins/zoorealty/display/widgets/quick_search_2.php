<?php
if(!empty($_GET['list']))$list=$_GET['list'];
elseif(!empty($realty->params['list']))$list=$realty->params['list'];
else $list='sale';

$type = (strpos($list, 'lease') !== false || strpos($list, 'Lease') !== false)? 'lease': 'sale';

if($list!='commercial_lease')$price_options = $settings[$type];
else{
	$price_options=array('500:500', '1000:1,000', '2000:2,000', '3000:3,000', '4000:4,000', '5000:5,000', '6000:6,000', '7000:7,000', '8000:8,000', '9000:9,000', '10000:10,000');
}
$max_price = end($price_options);

if($realty->renderer=='sold'){ $status='2,6'; }
else {
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']=='1')$status='1,2,4,6';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']=='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']!='1')$status='1,2,4';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']=='1')$status='1,4,6';
	if($realty->settings['general_settings']['display_sold_properties_in_search_result']!='1' && $realty->settings['general_settings']['display_leased_properties_in_search_result']!='1')$status='1,4';
}


$property_types = $realty->property_search('property_type',$list,'',$status);
$suburbs = $realty->property_search('suburb',$list,'',$status);

global $post;
$post_action=($realty->params['list']=='')?get_option('siteurl').'/search-results/':get_permalink($post->ID);
?>
<div id="quick_search">
	<h2 class="section_title">Quick Search</h2>
	<form id="search_quick" name="search_quick" action="<?php echo $post_action; ?>" method="get">		
		<fieldset>
			<div class="search_quick-wrap">
				<ul class="qs-ul">					
					<li class="search_property_types">
						<li class="location">
							<div class="city">
								<label>Town</label>
								<div class="suburb_select">
									<div  id="suburb_list" onClick="expand_suburb();">	
										<input id='suburb' value="<?php if (empty($_GET['suburb']))echo "All Suburbs"; if(count($_GET['suburb'])=='1')echo $_GET['suburb'][0];if(count($_GET['suburb'])>1)echo implode(", ",$_GET['suburb']); ?>" readonly="true" /> 
									</div>
		                        
									<div id="select_suburb">
										<p><input type="checkbox" class="checkbox" value="1" id="all_suburb" onClick="check_all_suburb();" <?php if(empty($_GET['suburb']))echo 'checked="checked"';?>> All </p>
										<?php foreach($suburbs as $suburb){ if(!is_array($suburb)) continue;?>
										<p><input name="suburb[]" type="checkbox" class="checkbox" value="<?php echo $suburb['tag']; ?>" <?php if(is_array($_GET['suburb'])){ if(in_array($suburb['tag'],$_GET['suburb'])) echo 'checked="checked"'; }?> onClick="check_suburb('<?php echo $suburb['tag']; ?>');"> <?php echo $suburb['tag']; ?></p>
										<?php } ?>
									</div>
								</div>
							</div>
						</li>
						<li class="property_type"><label>Type</label>
						<select name="property_type" id="select_property_type" <?php if(strpos(strtolower($list),'business')!==false ){ ?>onChange="load_category(this.value);" <?php } ?>>
							<option value=""><?php echo(strpos(strtolower($list),'business')===false )?'Property Type':'Business Type'; ?></option>
							<?php foreach($property_types as $property_type): if(!is_array($property_type)) continue; ?>
							<option value="<?php echo $property_type['tag']; ?>"<?php if( $property_type['tag']==$_GET['property_type']) echo ' selected="selected"'; ?>><?php echo $property_type['tag']; ?></option>
							<?php endforeach; ?>
						</select>
					</li>
					</li>
				  <?php 
					if(strpos(strtolower($list),'business')!==false ){ 
						$categories = $realty->property_search('category',$list,'',$status);
					?>
					<li>		
						<select name="category" id="select_category">
							<option value="">Business Category</option>
							<?php foreach($categories as $category): if(!is_array($category)) continue; ?>
							<option value="<?php echo $category['tag']; ?>"<?php if( $category['tag']==$_GET['category']) echo ' selected="selected"'; ?>><?php echo $category['tag']; ?></option>
							<?php endforeach; ?>
						</select>
					</li>		
					<?php }	?>				
					<li id="price_for_sale" class="price">
					<div><label>Price</label>
						<div class="price_min prices">
							<select name="price_min">
								<option value=""><?php echo($list =='commercial_lease')?'Rental P.A Min':'Min Price'; ?></option>
								<?php foreach($price_options as $price_option): 
								$delimiter = strpos($price_option, ':'); 
								$value = substr($price_option, 0, $delimiter); ?>
								<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_min']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1);// if($price_option ==$max_price) echo '+'; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					
						<div class="price_max prices">
							<select name="price_max">
								<option value=""><?php echo($list =='commercial_lease')?'Rental P.A Max':'Max Price'; ?></option>
								<?php foreach($price_options as $price_option): 
								$delimiter = strpos($price_option, ':'); 
								$value = substr($price_option, 0, $delimiter); ?>
								<option value="<?php echo $value; ?>"<?php if( $value ==$_GET['price_max']) echo ' selected="selected"'; ?>>$<?php echo substr($price_option, $delimiter+1); if($price_option ==$max_price) echo '+'; ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					</li>			
					<?php if($list!='BusinessSale' && strpos(strtolower($list),'commercial')===false){ ?>
					<li class="house_rooms" id="house_rooms">
					<label>Min Bedroom</label>
						<div class="bedrooms numberofrooms">
							<select name="bedrooms" class="bedrooms">
								<option value="">Beds</option>
								<?php for ( $i = 1 ; $i <= $settings['max_beds']; ++$i ) : ?>
								<option value="<?php echo $i ?>"<?php if( $i==$_GET['bedrooms']) echo ' selected="selected"'; ?>><?php echo $i; if ($i == $settings['max_beds']) echo '+';?></option>
								<?php endfor;	?>
							</select>
						</div>
					</li>
					<?php } ?>
				</ul>
				<p class="instruction">To select multiple suburbs, hold the Ctrl key and click.</p>
				<p class="button quick_search_btn"><input type="submit" value="Search" class="btn" /></p>

				<div class="clear"></div>
			</div>
			
		</fieldset>
	</form>
	<div class="clearer"></div>
</div>

<div class="clearer"></div>
<script type="text/javascript">
function showtab(type) {
	var url2 = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?type="+escape( type )+"&cat=property_type";	
	jQuery('#select_property_type').load(url2);
	
	var url = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?type="+escape( type )+"&cat=price";
	jQuery('#price_for_sale').load(url);
	
	var url1 = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?type="+escape( type )+"&cat=suburb2";
	jQuery('#select_suburb').load(url1);	
}

function load_category(property_type){
	var url = "<?php echo $realty->pluginUrl; ?>display/pages/js/quick_search_ajax.php?property_type="+escape( property_type )+"&cat=category";
	jQuery('#select_category').load(url);
}
 
document.onclick = function (e) {
	e = e || event;
	var target = e.target || e.srcElement;
	var select_suburb = document.getElementById("select_suburb");
	var suburb_list = document.getElementById("suburb_list");
	do {
		if (select_suburb == target || suburb_list == target) {
			return;
		}
		target = target.parentNode;
	} while (target);
	select_suburb.style.display = "none";
}

function expand_suburb(){
	if(jQuery("#select_suburb").is(":visible") == true )  jQuery("#select_suburb").hide(); 
	else  jQuery("#select_suburb").show(); 
}

function check_all_suburb(){		
if(eval("document.getElementById('all_suburb').checked") == true)
var max = document.getElementsByName('suburb[]').length;		
for(var idx = 0; idx < max; idx++){document.getElementsByName('suburb[]')[idx].checked = false;}
document.getElementById('suburb').value="All";
} 

function check_suburb(value){		
var flag='0';
var count='0';
var temp='';
var max = document.getElementsByName('suburb[]').length;
if(eval("document.getElementById('all_suburb').checked") == true)flag='1';					
if(flag=='1'){
	document.getElementById('all_suburb').checked = false;
	for(var idx = 0; idx < max; idx++){
		if(document.getElementsByName('suburb[]')[idx].value!=value)document.getElementsByName('suburb[]')[idx].checked = false;
		else document.getElementsByName('suburb[]')[idx].checked = true;
	}
}
for(var idx = 0; idx < max; idx++){
	if(document.getElementsByName('suburb[]')[idx].checked == true){ count++; temp=temp + document.getElementsByName('suburb[]')[idx].value + ", " ; }
}
if(count=='0') { document.getElementById('suburb').value="All";document.getElementsByName('all_suburb').checked = true; }
else document.getElementById('suburb').value=temp.substr(0,(temp.length)-2);
}
</script>