<?php 
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
}
get_header();
global $realty;
?>

<!-- HOMEPAGE -->
<?php if (is_page('home')) { ?>
	<div class="home-top">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Top') ) : ?><?php endif; ?>
        <div class="ads">
            <a class="top" href="<?php echo get_option('home'); ?>/selling/sale-appraisal/"><img src="<?php bloginfo('template_url'); ?>/images/backgrounds/free-market.png"/></a>
            <a class="top alt" href="<?php echo get_option('home'); ?>/team"><img src="<?php bloginfo('template_url'); ?>/images/backgrounds/our-staff.png"/></a>
            <a class="bottom" href="<?php echo get_option('home'); ?>/buying"><img src="<?php bloginfo('template_url'); ?>/images/backgrounds/buying.png"/></a>
            <a class="bottom alt" href="<?php echo get_option('home'); ?>/renting"><img src="<?php bloginfo('template_url'); ?>/images/backgrounds/renting.png"/></a>
        </div>
		<div class="clear"></div>
    </div>
   <div class="home-middle">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Middle') ) : ?><?php endif; ?>
        <div class="clear"></div>
    </div>
    <div class="home-bottom">
    	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Bottom') ) : ?><?php endif; ?>
        <div class="link-box-new">
        	<? /* <div class="link-subscribe"><p><a href="<?php echo get_option('siteurl').'/buy/subscribe/'; ?>">Subscribe to Property & News Alerts</a></p></div>
            <div class="link-appraisal"><p><a href="<?php echo get_option('siteurl').'/list/sales-appraisal/'; ?>">Request a Free Market Appraisal</a></p></div> */?>
            <a href="<?php echo get_option('home'); ?>/subscribe/" class="newsletter"></a>          
            <a href="<?php echo get_option('home'); ?>/contact-us/" class="contact"></a>
        </div>
        <div class="clear"></div>
    </div>

<?php } ?>
<!-- END HOMEPAGE -->

<?php if (!is_page('print-property') && !is_page('home')) { ?>
<!-- END CONTENT -->
<?php  get_sidebar();  } ?>

<?php if (!is_page('print-property') && !is_page('home')) { ?>
<!-- CONTENT -->
<div id="content" class="narrowcolumn"><?php } ?>

	<?php if (is_page('property')) { ?>
        <div class="page"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Property Page Main') ) : ?><?php endif; ?></div><!-- .post -->
    <?php } else { // All other pages will use this content and the sidebar.php file in the theme that uses the default registered sidebar. ?>
	<?php if (!is_page('print-property') && !is_page('home') || is_page('print-property')) { ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="page" id="page-<?php the_ID(); ?>">
                        <h1 class="page-title"><?php if (is_single() || is_page()) { $BodyTitle = get_post_meta($post->ID, 'BodyTitle', true); if ($BodyTitle) { echo $BodyTitle; } else { the_title(); } } ?></h1>
                        <div class="entry">
                            <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
                        </div><!-- end .entry -->
                    </div><!-- end .post -->
            <?php endwhile; endif; ?>
    	<?php } ?>
	<?php } ?>
    
<?php if (!is_page('print-property') && !is_page('home')) { ?>
</div>
<!-- END CONTENT -->
<?php /* get_sidebar(); */ } ?>
<?php get_footer(); ?>
